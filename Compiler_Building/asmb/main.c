#include <stdio.h>
#include <string.h>
#include <mcheck.h>
#include <stdlib.h>

unsigned long asmb_callchecking(unsigned long *x, size_t n, unsigned long y,  
				unsigned long *z);

int test(unsigned long *x, size_t n, unsigned long y,  
	 unsigned long *a, unsigned long rx)
{
  int i;
  int f;
  unsigned long r[n+2];
  unsigned long ra;
  for (i=0; i<n+2; i++)
    r[i]=0;
  printf("calling asmb(%p, %lu, %lu, %p)\n", x, n, y, a);
  for (i=0; i<n; i++)
    printf(" x[%d] = %lu\n", i, x[i]);
  ra = asmb_callchecking(x,n,y,r+1);
  for (i=0, f=1; i<n+2; i++)
    if (r[i]!=a[i]) {
      printf("\n %p: %lu, erwartet: %lu",r+i,r[i],a[i]);
      f=0;
    }
  if (ra!=rx) {
    printf("\n Rest: %lu, erwartet: %lu",ra,rx);
    f=0;
  }
  if (f)
    printf("ok");
  printf("\n");
  return f;
}

int main()
{
  unsigned long x0[]={};
  unsigned long a0[]={0,0};
  unsigned long x1[]={9,12};
  unsigned long a1[]={0,2,3,0};
  unsigned long x2[]={0x0L, 0x8L, 0xCL};
  unsigned long a2[]={0, 0x200000000L, 0x300000000L, 0x0L, 0};
  unsigned long x3[]={0x9260351BC013DFA0L, 0xEB8BC94E83E4C30L, 0x354CD39CA707E0BAL};
  unsigned long a3[]={0, 0x4D6AC0F7EF7E68FAL, 0x3E6643906DFED2FDL, 0x0L, 0};
  unsigned long x4[]={0xF386B81E0FECB5E0L, 0x4749F3A87A293D58L, 0x2DFFC07AE1C81E80L, 0xC4466501EB846103L, 0xF5243AA3861AE820L, 0x9E15F7FC6058077FL, 0xACE352FF07F14BD3L};
  unsigned long a4[]={0, 0x10B9705F792D2C17L, 0xDD01BBDA7CBD6CCAL, 0xA8F0357114F115E8L, 0x3CCB787B9EE75741L, 0x51655513ACA69EDDL, 0xC54A42EE7F72C57DL, 0x2L, 0};

  int f;
  f  = test(x0,0,9,a0,0);
  f &= test(x1,2,4,a1,1);
  f &= test(x2,3,0x400000000L,a2,0);
  f &= test(x3,3,0xdaab40b04479c810L,a3,0);
  f &= test(x4,7,0x3e6643906dfed2fd,a4,0x9A50FD4B64B4525L);
  if (!f)
    fprintf(stdout,"\nTest failed.\n");
  else
    fprintf(stdout,"\nTest succeeded.\n");
  return !f;
}
