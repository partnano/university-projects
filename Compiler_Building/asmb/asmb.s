	.file	"asmb.c"
	.text
	.globl	asmb
	.type	asmb, @function
asmb:
.LFB13:
	.cfi_startproc
	/* 
		%rdi = *x
		%rsi = n
		%rdx = y
		%rcx = *z
	*/
	xor		%rax, %rax /* 0 */

	/* check if n < 1 and if so, skip long div */
	cmpq	$1, %rsi
	jb		end

		movq	%rdx, %r8  /* y to r8 */
		movq	%rcx, %r9  /* z to r9 */
		movq	%rsi, %rcx /* n to rcx (loop counter) */
		xor		%rdx, %rdx /* 0 */ 

		divdiv:	

			/* get second 8-byte number (x1 in x2x1 / y) */
			/* x2 is already in rdx (remainder of previous div or 0) */
			movq	-8(%rdi, %rcx, 8), %rax
			div 	%r8
			movq	%rax, -8(%r9, %rcx, 8)

		loop	divdiv

		/* move remainder of div to rax and end program */
		movq	%rdx, %rax

	end:
		ret

	.cfi_endproc
.LFE13:
	.size	asmb, .-asmb
	.ident	"GCC: (GNU) 7.3.0"
	.section	.note.GNU-stack,"",@progbits
