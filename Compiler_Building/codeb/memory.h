#ifndef COMPILERBAU_MEMORY_H
#define COMPILERBAU_MEMORY_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <stdint.h>

#include "symtable.h"

#define NUM_REGISTERS 9
#define MAX_PARAMS 6

typedef enum 
{
	rdi = 0, // arg 1
	rsi = 1, // arg 2
	rdx = 2, // arg 3
	rcx = 3, // arg 4
	r8 = 4, // arg 5
	r9 = 5, // arg 6
	rax = 6,
	r10 = 7,
	r11 = 8,
	none = -1
} memreg;

typedef struct 
{
	char const* id;
	int64_t value;
} *Variable;

typedef struct Register 
{
	bool immutable;
	bool reserved;
	char const* id;
} Register;

typedef struct Struct 
{
	char const* name;
	symtable *fields;
} *Struct;

typedef struct StructList StructList;

struct StructList 
{
	Struct *str;
	StructList *next;
};

typedef struct fn_scope 
{
	Register registers[NUM_REGISTERS];
	StructList *structs;
} *FunctionScope;

FunctionScope create_scope(StructList* structs);

void register_param(char const* id, FunctionScope scope);
memreg get_free_register(FunctionScope scope);

void reserve(memreg reg, FunctionScope scope);
void release(memreg reg, FunctionScope scope);

memreg id_to_reg(char const* id, FunctionScope scope);

char* print_reg(memreg reg);

Struct create_struct(char const* name, symtable* fields);

StructList* add_struct(Struct* str, StructList* list);
StructList* merge_lists(StructList* l1, StructList* l2);
int get_offset(char const* symbol, StructList* list);
int get_sizeof(char const* symbol, StructList* list);

#endif
