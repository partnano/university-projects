%{
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "symtable.h"
#include "memory.h"
#include "tree.h"
#include "codegen.h"

extern void burm_reduce(NODEPTR_TYPE bnode, int goalnt);
extern void burm_label(NODEPTR_TYPE p);

int yylex();
void yyerror(char *s);
%}

%token STRUCT END RETURN IF THEN ELSE WHILE DO VAR OR NOT SIZEOF DEF

%union {
	long num;
	char *id;
}

%token <num> NUMBER
%token <id>  ID

@autoinh var_pass field_pass structname_pass scope
@autosyn node

@attributes
{
    char *name;
} ID

@attributes
{
    int64_t value;
} NUMBER

@attributes 
{ 
    char *name;
    symtable *ids;
    Struct str;
} Structdef

@attributes
{
    symtable *id_scope;
} Ids

@attributes
{
    symtable *structname_scope;
    symtable *field_scope;
    FunctionScope scope;
} Funcdef

@attributes
{
    symtable *structname_forw;
    symtable *field_forw;
    symtable *structname_back;
    symtable *field_back;
    StructList *structlist_forw;
    StructList *structlist_back;
} Program

@attributes
{
    symtable *structnames;
    symtable *fields;
    symtable *variables;
    FunctionScope scope;
} Pars

@attributes
{
    symtable *structnames;
    symtable *fields;
    symtable *variables;
    FunctionScope scope;
    char *label;
    char *loop_label;
} Stats

@attributes
{
    symtable *structnames;
    symtable *fields;
    symtable *variables;
    symtable *new_variable;
    Tree node;
    FunctionScope scope;
    char *loop_label;
} Stat

@attributes
{
    symtable *var_pass;
    symtable *field_pass;
    symtable *structname_pass;
    Tree node;
    FunctionScope scope;
} Expr Term PlusTerm MultTerm NegatedTerm OrTerm SmallEqTerm

@attributes
{
    symtable *var_pass;
    symtable *field_pass;
    symtable *structname_pass;
    FunctionScope scope;
} Exprs

@attributes
{
    symtable *var_pass;
    symtable *field_pass;
    symtable *structname_pass;
    FunctionScope scope;
    Tree node;
} Lexpr ValDefs

@traversal @preorder PRE
@traversal @postorder POST
@traversal @preorder CODE
@traversal @postorder POSTCODE

%%

Start:
    Program
    @{
        @i @Program.structname_forw@ = NULL;
        @i @Program.field_forw@ = NULL;
        @i @Program.structlist_forw@ = NULL;
    @}
    ;

Program: 
        @{
            @i @Program.structname_back@ = NULL;
            @i @Program.field_back@ = NULL;
            @i @Program.structlist_back@ = NULL;
        @}
    |   Funcdef ';' Program
        @{
            @i @Program.0.structname_back@ = @Program.1.structname_back@;
            @i @Program.0.field_back@ = @Program.1.field_back@;
            @i @Program.0.structlist_back@ = @Program.1.structlist_back@;

            @i @Program.1.structname_forw@ = @Program.0.structname_forw@;
            @i @Program.1.field_forw@ = @Program.0.field_forw@;
            @i @Program.1.structlist_forw@ = @Program.0.structlist_forw@;

            @i @Funcdef.structname_scope@ = merge(@Program.1.structname_forw@, @Program.0.structname_back@);
            @i @Funcdef.field_scope@ = merge(@Program.1.field_forw@, @Program.0.field_back@);

            @i @Funcdef.scope@ = create_scope(merge_lists(@Program.0.structlist_back@, @Program.1.structlist_forw@));
        @}
    |   Structdef ';' Program
        @{
            /* check if structdef correct */
            @PRE checkif_nodouble(add_symbol(@Structdef.name@, @Structdef.ids@));

            /* for every struct that is further ahead in the code (therefore "comes backwards") */
            @PRE checkif_unused(@Structdef.name@, @Program.1.structname_back@);

            @i @Program.0.structname_back@ = add_symbol(@Structdef.name@, @Program.1.structname_back@);
            @i @Program.0.field_back@ = merge(@Structdef.ids@, @Program.1.field_back@);
            @i @Program.0.structlist_back@ = add_struct(&@Structdef.str@, @Program.1.structlist_back@);

            @POST checkif_nodouble(@Program.0.field_back@);

            /* for every struct that is further behind in the code (therefore "comes forwards") */
            @PRE checkif_unused(@Structdef.name@, @Program.0.structname_forw@);

            @i @Program.1.structname_forw@ = add_symbol(@Structdef.name@, @Program.0.structname_forw@);
            @i @Program.1.field_forw@ = merge(@Structdef.ids@, @Program.0.field_forw@);
            @i @Program.1.structlist_forw@ = add_struct(&@Structdef.str@, @Program.0.structlist_forw@);

            @POST checkif_nodouble(@Program.1.field_forw@);
        @}
    ;

Structdef:
        STRUCT ID '=' Ids END
        @{
            @i @Structdef.name@ = @ID.name@;
            @i @Structdef.ids@ = @Ids.id_scope@;
            @i @Structdef.str@ = create_struct(@ID.name@, @Ids.id_scope@);
        @}
    ;

Ids:

    @{
        @i @Ids.id_scope@ = NULL;
    @}    
|   ID Ids
    @{
        @PRE checkif_unused(@ID.name@, @Ids.1.id_scope@);

        @i @Ids.0.id_scope@ = add_symbol(@ID.name@, @Ids.1.id_scope@);
    @}
;

Funcdef:
        ID '(' Pars ')' Stats END
        @{
            @i @Pars.structnames@ = @Funcdef.structname_scope@;
            @i @Pars.fields@ = @Funcdef.field_scope@;

            @i @Stats.structnames@ = @Funcdef.structname_scope@;
            @i @Stats.fields@ = @Funcdef.field_scope@;
            @i @Stats.variables@ = @Pars.variables@;
            @i @Stats.label@ = NULL;
            @i @Stats.loop_label@ = NULL;

            @CODE generate_function_header(@ID.name@);
            @POSTCODE generate_function_footer();            
        @}
    ;

Pars:
        
        @{
            @i @Pars.variables@ = NULL;
        @}
    |   Pars ',' ID
        @{
            @PRE checkif_unused(@ID.name@, @Pars.1.variables@);
            @PRE checkif_unused(@ID.name@, @Pars.0.structnames@);
            @PRE checkif_unused(@ID.name@, @Pars.0.fields@);
        
            @i @Pars.0.variables@ = add_symbol(@ID.name@, @Pars.1.variables@);
            @i @Pars.1.fields@ = @Pars.0.fields@;
            @i @Pars.1.structnames@ = @Pars.0.structnames@;

            @POST register_param(@ID.name@, @Pars.scope@);
        @}
    |   ID
        @{
            @PRE checkif_unused(@ID.name@, @Pars.0.structnames@);
            @PRE checkif_unused(@ID.name@, @Pars.0.fields@);

            @i @Pars.variables@ = add_symbol(@ID.name@, NULL);

            @POST register_param(@ID.name@, @Pars.scope@);
        @}
    ;

Stats:

        @{
            @CODE if(@Stats.loop_label@) fprintf(stdout, "\tjmp %s \n", @Stats.loop_label@);
            @CODE if(@Stats.label@) fprintf(stdout, "\t%s: \n", @Stats.label@);
        @}
    |   Stat ';' Stats
        @{
            @i @Stat.variables@ = @Stats.0.variables@;
            @i @Stat.fields@ = @Stats.0.fields@;
            @i @Stat.structnames@ = @Stats.0.structnames@;

            @i @Stats.1.variables@ = merge(@Stat.new_variable@, @Stats.0.variables@);
            @i @Stats.1.fields@ = @Stat.fields@;
            @i @Stats.1.structnames@ = @Stat.structnames@;
            @i @Stats.1.label@ = @Stats.0.label@;
            @i @Stats.1.loop_label@ = @Stats.0.loop_label@;

            @CODE if(@Stat.loop_label@) fprintf(stdout, "\t%s: \n", @Stat.loop_label@);
            @CODE burm_label(@Stat.node@); burm_reduce(@Stat.node@, 1);
            //@POST print_tree(@Stat.node@); printf("\n\n");
        @}
    ;

Stat:
        RETURN Expr
        @{
            @i @Expr.var_pass@ = @Stat.variables@;
            @i @Expr.field_pass@ = @Stat.fields@;
            @i @Expr.structname_pass@ = @Stat.structnames@;

            @i @Stat.new_variable@ = NULL;
            
            @i @Stat.node@ = add_node(T_RET, @Expr.node@, NULL, 0, NULL, @Stat.scope@, NULL);

            @i @Stat.loop_label@ = NULL;
        @}
    |   IF Expr THEN Stats END
        @{
            @i @Expr.var_pass@ = @Stat.variables@;
            @i @Expr.field_pass@ = @Stat.fields@;
            @i @Expr.structname_pass@ = @Stat.structnames@;

            @i @Stats.variables@ = @Stat.variables@;
            @i @Stats.fields@ = @Stat.fields@;
            @i @Stats.structnames@ = @Stat.structnames@;
            @i @Stats.label@ = generate_new_if_label();
            @i @Stats.loop_label@ = NULL;

            @i @Stat.new_variable@ = NULL;
            @i @Stat.loop_label@ = NULL;

            @i @Stat.node@ = add_node(T_IFTHEN, @Expr.node@, NULL, 0, NULL, @Stat.scope@, @Stats.label@);
        @}
    |   IF Expr THEN Stats ELSE Stats END
        @{
            @i @Expr.var_pass@ = @Stat.variables@;
            @i @Expr.field_pass@ = @Stat.fields@;
            @i @Expr.structname_pass@ = @Stat.structnames@;

            @i @Stats.0.variables@ = @Stat.variables@;
            @i @Stats.0.fields@ = @Stat.fields@;
            @i @Stats.0.structnames@ = @Stat.structnames@;
            @i @Stats.0.label@ = generate_new_if_label();
            @i @Stats.0.loop_label@ = NULL;

            @i @Stats.1.variables@ = @Stat.variables@;
            @i @Stats.1.fields@ = @Stat.fields@;
            @i @Stats.1.structnames@ = @Stat.structnames@;
            @i @Stats.1.label@ = NULL;
            @i @Stats.1.loop_label@ = NULL;

            @i @Stat.new_variable@ = NULL;
            @i @Stat.loop_label@ = NULL;

            @i @Stat.node@ = add_node(T_IFTHENELSE, @Expr.node@, NULL, 0, NULL, @Stat.scope@, @Stats.0.label@);
        @}
    |   WHILE Expr DO Stats END
        @{
            @i @Expr.var_pass@ = @Stat.variables@;
            @i @Expr.field_pass@ = @Stat.fields@;
            @i @Expr.structname_pass@ = @Stat.structnames@;

            @i @Stats.variables@ = @Stat.variables@;
            @i @Stats.fields@ = @Stat.fields@;
            @i @Stats.structnames@ = @Stat.structnames@;
            @i @Stats.loop_label@ = generate_new_while_label();
            @i @Stats.label@ = generate_new_while_label();

            @i @Stat.new_variable@ = NULL;
            @i @Stat.loop_label@ = @Stats.loop_label@;

            @i @Stat.node@ = add_node(T_WHILE, @Expr.node@, NULL, 0, NULL, @Stat.scope@, @Stats.label@);
        @}
    |   VAR ID DEF Expr
        @{
            @PRE checkif_unused(@ID.name@, @Stat.variables@);

            @i @Expr.var_pass@ = @Stat.variables@;
            @i @Expr.field_pass@ = @Stat.fields@;
            @i @Expr.structname_pass@ = @Stat.structnames@;

            @i @Stat.new_variable@ = add_symbol(@ID.name@, NULL);
            @i @Stat.loop_label@ = NULL;

            @i @Stat.node@ = add_node(T_DEF, @Expr.node@, NULL, 0, @ID.name@, @Stat.scope@, NULL);
        @}
    |   ValDefs Expr
        @{
            @i @ValDefs.var_pass@ = @Stat.variables@;
            @i @ValDefs.field_pass@ = @Stat.fields@;
            @i @ValDefs.structname_pass@ = @Stat.structnames@;

            @i @Expr.var_pass@ = @Stat.variables@;
            @i @Expr.field_pass@ = @Stat.fields@;
            @i @Expr.structname_pass@ = @Stat.structnames@;

            @i @Stat.new_variable@ = NULL;
            @i @Stat.loop_label@ = NULL;

            @i @Stat.node@ = add_node(T_ASSIGN, @Expr.node@, @ValDefs.node@, 0, NULL, @Stat.scope@, NULL);
        @}
    |   Expr
        @{
            @i @Expr.var_pass@ = @Stat.variables@;
            @i @Expr.field_pass@ = @Stat.fields@;
            @i @Expr.structname_pass@ = @Stat.structnames@;

            @i @Stat.new_variable@ = NULL;
            @i @Stat.loop_label@ = NULL;

            @i @Stat.node@ = NULL;
        @}
    ;

ValDefs:
        Lexpr DEF
        @{
            @i @ValDefs.node@ = add_node(T_VALDEF, @Lexpr.node@, add_node(T_CONST, NULL, NULL, 0, NULL, @ValDefs.scope@, NULL), 
                                         0, NULL, @ValDefs.scope@, NULL);
        @}
    |   ValDefs Lexpr DEF
        @{
            @i @ValDefs.node@ = add_node(T_VALDEF, @Lexpr.node@, @ValDefs.1.node@, 0, NULL, @ValDefs.scope@, NULL);
        @}
    ;

Lexpr:
        ID
        @{
            /* can only be a variable */
            @PRE checkif_used(@ID.name@, @Lexpr.var_pass@);

            @i @Lexpr.node@ = add_node(T_VAR, NULL, NULL, 0, @ID.name@, @Lexpr.scope@, NULL);
        @}
    |   Term '.' ID
        @{
            @PRE checkif_used(@ID.name@, @Lexpr.field_pass@);

            @i @Term.var_pass@ = @Lexpr.var_pass@;
            @i @Lexpr.node@ = add_node(T_FIELD, @Term.node@, NULL, 0, @ID.name@, @Lexpr.scope@, NULL);
        @}
    ;

Term:
        '(' Expr ')'
    |   SIZEOF ID
        @{
            @PRE checkif_used(@ID.name@, @Term.structname_pass@);
            
            @i @Term.node@ = add_node(T_SIZEOF, NULL, NULL, 0, @ID.name@, @Term.scope@, NULL);
        @}
    |   NUMBER
        @{
            @i @Term.node@ = add_node(T_CONST, NULL, NULL, @NUMBER.value@, NULL, @Term.scope@, NULL);
        @}
    |   Term '.' ID
        @{
            @PRE checkif_used(@ID.name@, @Term.0.field_pass@);

            @i @Term.1.var_pass@ = @Term.0.var_pass@;
            @i @Term.0.node@ = add_node(T_FIELD, @Term.1.node@, NULL, 0, @ID.name@, @Term.scope@, NULL);
        @}
    |   ID
        @{
            @PRE checkif_used(@ID.name@, @Term.var_pass@);
            @i @Term.node@ = add_node(T_VAR, NULL, NULL, 0, @ID.name@, @Term.scope@, NULL);
        @}
    |   ID '(' ')'
        @{
            @i @Term.node@ = NULL;
        @}
    |   ID '(' Exprs ')'
        @{
            @i @Term.node@ = NULL;
        @}
    ;

Expr:
        NegatedTerm
    |   PlusTerm
    |   Term '-' Term
        @{
            @i @Expr.node@ = add_node(T_SUB, @Term.0.node@, @Term.1.node@, 0, NULL, @Term.scope@, NULL);
        @}
    |   MultTerm
    |   OrTerm
    |   SmallEqTerm
    |   Term
    ;

NegatedTerm:
        NOT Term
        @{
            @i @NegatedTerm.node@ = add_node(T_NOT, @Term.node@, NULL, 0, NULL, @Term.scope@, NULL);;
        @}
    |   '-' Term
        @{
            @i @NegatedTerm.node@ = add_node(T_MINUS, @Term.node@, NULL, 0, NULL, @Term.scope@, NULL);
        @}
    |   NOT NegatedTerm
        @{
            @i @NegatedTerm.node@ = add_node(T_NOT, @NegatedTerm.1.node@, NULL, 0, NULL, @NegatedTerm.scope@, NULL);;
        @}
    |   '-' NegatedTerm
        @{
            @i @NegatedTerm.node@ = add_node(T_MINUS, @NegatedTerm.1.node@, NULL, 0, NULL, @NegatedTerm.scope@, NULL);
        @}
    ;

PlusTerm:
        Term '+' Term
        @{
            @i @PlusTerm.node@ = add_node(T_ADD, @Term.0.node@, @Term.1.node@, 0, NULL, @Term.scope@, NULL);
        @}
    |   PlusTerm '+' Term
        @{
            @i @PlusTerm.node@ = add_node(T_ADD, @PlusTerm.1.node@, @Term.node@, 0, NULL, @Term.scope@, NULL);
        @}
    ;

MultTerm:
        Term '*' Term
        @{
            @i @MultTerm.node@ = add_node(T_MUL, @Term.0.node@, @Term.1.node@, 0, NULL, @Term.scope@, NULL);
        @}
    |   MultTerm '*' Term
        @{
            @i @MultTerm.node@ = add_node(T_MUL, @MultTerm.1.node@, @Term.node@, 0, NULL, @Term.scope@, NULL);
        @}
    ;

OrTerm:
        Term OR Term
        @{
            @i @OrTerm.node@ = add_node(T_OR, @Term.0.node@, @Term.1.node@, 0, NULL, @Term.scope@, NULL);
        @}
    |   OrTerm OR Term
        @{
            @i @OrTerm.node@ = add_node(T_OR, @OrTerm.1.node@, @Term.node@, 0, NULL, @Term.scope@, NULL);
        @}
    ;

SmallEqTerm:
        Term '<' Term
        @{
            @i @SmallEqTerm.node@ = add_node(T_BELOW, @Term.0.node@, @Term.1.node@, 0, NULL, @Term.scope@, NULL);
        @}
    |   Term '=' Term
        @{
            @i @SmallEqTerm.node@ = add_node(T_EQUAL, @Term.0.node@, @Term.1.node@, 0, NULL, @Term.scope@, NULL);
        @}
    ;

Exprs:
        Expr ',' Exprs
    |   Expr
    ;

%%

// stuff from lex that yacc needs to know about:
extern int yylex();
extern int yyparse();
extern FILE *yyin;

int main (void)
{
    do {
		yyparse();
	} while (!feof(yyin));

    return 0;
}

void yyerror(char *s) {
	printf("parse error (%s)\n", s);
	exit(2);
}