#ifndef NANO_SYM
#define NANO_SYM

typedef struct symtable symtable;

struct symtable {
	char* symbol;
	symtable *next;
};

symtable* add_symbol  (char *symbol, symtable *table);
symtable* merge       (symtable *t1, symtable *t2);
void checkif_unused   (char *symbol, symtable *table);
void checkif_used     (char *symbol, symtable *table);
void checkif_nodouble (symtable *table);
void symprint         (symtable *table);

int find_offset (char const* symbol, symtable *table);
int get_num (symtable *table);

#endif