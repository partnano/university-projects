#include "codegen.h"

int64_t cmp_counter = 0; // counter for comp labels

void generate_function_header (char* name) 
{
	fprintf(stdout, ".globl\t%s\n", name);
	fprintf(stdout, ".type\t%s, @function\n", name);
	fprintf(stdout, "%s:\n", name);
}

void generate_function_footer () 
{
    fprintf(stdout, "\tretq\n");
}


memreg get_source (Tree node)
{
    memreg source = none;

    if(node->result != none)
        source = node->result;

    switch (node->op)
    {
        case T_CONST:
        {
            // check if there is a free register left, otherwise fail
            source = get_free_register(node->scope);
            if (source == none)
            {
                fprintf(stderr, "Expression too deep.");
                exit(3);
            }

            // if there is, move constant to register
            fprintf(stdout, "\tmovq $%lli, %s\n", node->value, print_reg(source));
            break;
        }
        case T_VAR:
        {
            source = id_to_reg(node->id, node->scope);
            break;
        }        
        default:
            break;
    }

    reserve(source, node->scope);
    return source;
}

memreg get_target (Tree node)
{
    // check if there is a free register left
    memreg target = get_free_register(node->scope);
    if (target == none)
    {
        fprintf(stderr, "Expression too deep.");
        exit(3);
    }

    if(node->result != none)
        target = node->result;

    switch (node->op)
    {
        case T_CONST:
        {
            // if there is, move constant to register
            fprintf(stdout, "\tmovq $%lli, %s\n", node->value, print_reg(target));
            break;
        }
        case T_VAR:
        {
            memreg orig = id_to_reg(node->id, node->scope);
            fprintf(stdout, "\tmovq %s, %s\n", print_reg(orig), print_reg(target));
            node->result = target;
            break;
        }
        default:
        {
            break;
        }
    }

    reserve(target, node->scope);
    return target;    
}

void generate_return (Tree node)
{
	if (node->left->result != none) 
    {
		fprintf(stdout, "\tmovq %s, %rax", print_reg(node->left->result));
		fprintf(stdout, " #return\n");
        generate_function_footer();
		release(node->left->result, node->left->scope);
		return;
	}

    switch (node->left->op) 
    {
        case T_CONST:
            fprintf(stdout, "\tmovq $%lli, %rax \n", node->left->value);
            generate_function_footer();
            break;

        case T_VAR:
            fprintf(stdout, "\tmovq %s, %rax \n", print_reg(id_to_reg(node->left->id, node->left->scope)));
            generate_function_footer();
            break;

        default:
            break;
    }
}

void generate_op (Tree node, char* op)
{   
    memreg source = get_source(node->left);  // source because it can't change the param register
    memreg target = get_target(node->right); // target because it can change the param register

    if (source != none) // target can't be none
        fprintf(stdout, "\t%s %s, %s \n", op, print_reg(source), print_reg(target));

    // new (relevant) value is now in target, source can be used again
    node->result = target;
    reserve(target, node->scope);
    release(source, node->scope);
}

// same as generate_op, just switched nodes
void generate_sub (Tree node)
{
    memreg source = get_source(node->right);  // source because it can't change the param register
    memreg target = get_target(node->left); // target because it can change the param register

    if (source != none) // target can't be none
        fprintf(stdout, "\tsubq %s, %s \n", print_reg(source), print_reg(target));

    // new (relevant) value is now in target, source can be used again
    node->result = target;
    reserve(target, node->scope);
    release(source, node->scope);
}

void generate_negation(Tree node)
{
    release(node->result, node->scope);
    memreg target = get_target(node->left);

    fprintf(stdout, "\tnegq %s \n", print_reg(target));
    node->result = target;
}

void generate_not(Tree node)
{
    release(node->result, node->scope);
    memreg target = get_target(node->left);

    fprintf(stdout, "\tnotq %s \n", print_reg(target));
    node->result = target;
}

void generate_cmp (Tree node, char* cmp)
{
    release(node->result, node->scope);
    memreg s1 = get_source(node->left);  // source because it can't change the param register
    memreg s2 = get_source(node->right); // target because it can change the param register
    memreg final  = get_target(node);

    fprintf(stdout, "\tmovq $1, %s \n", print_reg(final));
    fprintf(stdout, "\tcmpq %s, %s \n", print_reg(s2), print_reg(s1)); // asm is strange (this in the wrong order cost me 10%)
    fprintf(stdout, "\tj%s ___%lli \n", cmp, cmp_counter);
    fprintf(stdout, "\t\tmovq $0, %s \n", print_reg(final));
    fprintf(stdout, "\t___%lli: \n", cmp_counter);

    cmp_counter++;
    release(s1, node->scope);
    node->result = final;
}

void generate_field_access (Tree node)
{
    release(node->result, node->scope);
    memreg address = get_source(node->left);
    memreg target  = get_target(node);
    int offset = get_offset(node->id, node->scope->structs);

    fprintf(stdout, "\tmovq %i(%s), %s \n", offset, print_reg(address), print_reg(target));

    reserve(target, node->scope);
    node->result = target;
}

void generate_sizeof (Tree node)
{
    release(node->result, node->scope);
    memreg target = get_target(node);
    int size = get_sizeof(node->id, node->scope->structs);
    
    fprintf(stdout, "\tmovq $%d, %s #sizeof\n", size, print_reg(target));

    reserve(target, node->scope);
    node->result = target;
}

// labels
int if_label_counter = 0;
char* if_label_prefix = "__IFTHEN_";

char* generate_new_if_label ()
{
    char str[12];
    sprintf(str, "%d", if_label_counter++);

    char* output = (char *) malloc(sizeof(char) * 21);
    strcat(output, if_label_prefix);
    strcat(output, str);

    return output;
}

void generate_ifthen (Tree node)
{
    release(node->result, node->scope);
    memreg source = get_source(node->left);

    // prep test
    memreg comp = get_free_register(node->scope);
    fprintf(stdout, "\tmovq $1, %s \n", print_reg(comp));

    // check if odd
    fprintf(stdout, "\ttestq %s, %s \n", print_reg(source), print_reg(comp));
    fprintf(stdout, "\tjz %s \n", node->label);

    reserve(source, node->scope);
}

// labels
int while_label_counter = 0;
char* while_label_prefix = "__WHILE_";

char* generate_new_while_label ()
{
    char str[12];
    sprintf(str, "%d", while_label_counter++);

    char* output = (char *) malloc(sizeof(char) * 20);
    strcat(output, while_label_prefix);
    strcat(output, str);

    return output;
}

void generate_while (Tree node)
{
    release(node->result, node->scope);
    memreg source = get_source(node->left);

    // prep test
    memreg comp = get_free_register(node->scope);
    reserve(comp, node->scope);
    fprintf(stdout, "\tmovq $1, %s \n", print_reg(comp));

    // check if odd
    fprintf(stdout, "\ttestq %s, %s \n", print_reg(source), print_reg(comp));
    fprintf(stdout, "\tjz %s \n", node->label);

    release(comp, node->scope);
    reserve(source, node->scope);
}

void generate_while_start (Tree node)
{
    if(node->label)
        printf("\t%s: \n", node->label);

    //node->label = NULL;
}

void generate_def (Tree node)
{
    memreg source = get_source(node->left);
    memreg target = get_free_register(node->scope);

    node->scope->registers[target].immutable = true;
    node->scope->registers[target].id = node->id;

    fprintf(stdout, "\tmovq %s, %s # vardef \n", print_reg(source), print_reg(target));

    release(source, node->scope);
}

void generate_ass (Tree node)
{
    release(node->result, node->scope);
    memreg to_assign = get_target(node->left);

    // traverse tree to get assignments
    Tree current_node = node->right;
    do
    {
        Tree lexpr = current_node->left;

        if (lexpr->op == T_VAR)
        {
            fprintf(stdout, "\tmovq %s, %s \n", print_reg(to_assign), print_reg(id_to_reg(lexpr->id, lexpr->scope)));
        }
        else if (lexpr->op == T_FIELD)
        {
            int offset = get_offset(lexpr->id, node->scope->structs);
            memreg address = get_source(lexpr->left);
            printf("\tmovq %s, %i(%s) \n", print_reg(to_assign), offset, print_reg(address));
        }
        else
        {
            fprintf(stderr, "invalid assignment");
            exit(3);
        }

        current_node = current_node->right;
    } while (current_node->op == T_VALDEF);

    // not needed anymore
    release(to_assign, node->scope);
}