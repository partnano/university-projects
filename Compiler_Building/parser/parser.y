%{
#include <stdlib.h>
#include <stdio.h>

int yylex();
void yyerror(char *s);
%}

%token STRUCT END RETURN IF THEN ELSE WHILE DO VAR OR NOT SIZEOF DEF

%union {
	long num;
	char *id;
}

%token <num> NUMBER
%token <id>  ID

%%

Program: 
        Funcdef ';' Program   { printf("funcdef (program) start\n"); }
    |   Funcdef ';'           { printf("funcdef (program) start\n"); }
    |   Structdef ';' Program { printf("structdef (program) start\n"); }
    |   Structdef ';'         { printf("structdef (program) start\n"); }
    ;

Structdef:
        STRUCT ID '=' Ids END { printf("structdef\n"); }
    ;

Ids:
        %empty
    |   Ids ID
    ;

Funcdef:
        ID '(' Pars ')' Stats END { printf("funcdef\n"); }
    ;

Pars:
        %empty
    |   Pars ',' ID
    |   ID
    ;

Stats:
        %empty
    |   Stats Stat ';'
    ;

Stat:
        RETURN Expr                         { printf("return Expr\n"); }
    |   IF Expr THEN Stats END              { printf("if then end\n"); }
    |   IF Expr THEN Stats ELSE Stats END   { printf("if then else end\n"); }
    |   WHILE Expr DO Stats END             { printf("while do end\n"); }
    |   VAR ID DEF Expr                     { printf("var id := Expr\n"); }
    |   ValDefs Expr                        { printf("id1 := id2 := ... := Expr\n"); }
    |   Expr                                { printf("id1;\n"); }
    ;

ValDefs:
        Lexpr DEF
    |   ValDefs Lexpr DEF
    ;

Expr:
        NegatedTerm
    |   PlusTerm
    |   Term '-' Term
    |   MultTerm
    |   OrTerm
    |   SmallEqTerm
    |   Term            { printf("Term\n"); }
    ;

NegatedTerm:
        NOT Term        { printf("not Term\n"); }
    |   '-' Term        { printf("not Term\n"); }
    |   NOT NegatedTerm
    |   '-' NegatedTerm
    ;

PlusTerm:
        Term '+' Term       { printf("Term + Term\n"); }
    |   PlusTerm '+' Term   { printf("Term + Term\n"); }
    ;

MultTerm:
        Term '*' Term       { printf("Term * Term\n"); }
    |   MultTerm '*' Term   { printf("Term * Term\n"); }
    ;

OrTerm:
        Term OR Term        { printf("Term or Term\n"); }
    |   OrTerm OR Term      { printf("Term or Term\n"); }
    ;

SmallEqTerm:
        Term '<' Term       { printf("Term < Term\n"); }
    |   Term '=' Term       { printf("Term = Term\n"); }
    ;

Term:
        '(' Expr ')'
    |   SIZEOF ID
    |   NUMBER
    |   Term '.' ID         
    |   ID
    |   ID '(' ')'
    |   ID '(' Exprs ')'
    ;

Lexpr:
        ID
    |   Term '.' ID
    ;

Exprs:
        Exprs ',' Expr
    |   Expr
    ;

%%

// stuff from lex that yacc needs to know about:
extern int yylex();
extern int yyparse();
extern FILE *yyin;

int main (void)
{
    do {
		yyparse();
	} while (!feof(yyin));

    return 0;
}

void yyerror(char *s) {
	printf("parse error (%s)\n", s);
	exit(2);
}