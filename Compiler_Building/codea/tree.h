#ifndef COMPILERBAU_TREE_H
#define COMPILERBAU_TREE_H

#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <stdint.h>

#include "memory.h"

#ifndef _IBURG
typedef struct burm_state *STATEPTR_TYPE;
#endif

enum 
{
	T_CONST=10,
	T_VAR=11,
	T_FIELD=12,
	T_ADD=13,
	T_RET=14,
	T_MUL=15,
	T_MINUS=16,
	T_NOT=17,
	T_SUB=18,
	T_OR=19,
	T_BELOW=20,
	T_EQUAL=21,
	T_SIZEOF=22
};

typedef int64_t Const;
typedef char const* Identifier;

typedef struct tree {
	int op;
	struct tree* left;
	struct tree* right;
	
	FunctionScope scope;

	Const value; 		// constant value
	bool valid;			// if value is valid (mostly for multiplication)
	Identifier id; 		// variable name
	memreg result; 		// holds results from former calculations

	STATEPTR_TYPE state;
} *NODEPTR_TYPE, *Tree;

#define OP_LABEL(p) (p->op)
#define STATE_LABEL(p) ((p)->state)
#define LEFT_CHILD(p) ((p)->left)
#define RIGHT_CHILD(p) ((p)->right)
#define PANIC printf

Tree add_node(int op, Tree left, Tree right, int value, char* id, FunctionScope scope);
void print_tree(Tree t);

#endif