#include "memory.h"

FunctionScope create_scope (StructList* structs)
{
    FunctionScope scope = malloc(sizeof(struct fn_scope));
    memset(scope, 0x0, sizeof(struct fn_scope));

    scope->structs = structs;

    return scope;
}

void reserve (memreg reg, FunctionScope scope)
{
    scope->registers[reg].reserved = 1;
}

void release (memreg reg, FunctionScope scope)
{
    if (!scope->registers[reg].immutable)
        scope->registers[reg].reserved = 0;
}

void register_param (char const* id, FunctionScope scope)
{
    for (int i = 0; i < MAX_PARAMS; i++)
    {
        if (scope->registers[i].reserved)
            continue;

        scope->registers[i].id = id;
        scope->registers[i].reserved = 1;
        scope->registers[i].immutable = 1;

        //printf("PARAM: %s, %d \n", id, i);

        return;
    }

    fprintf(stderr, "Too many parameters!");
    exit(3);
}

memreg get_free_register (FunctionScope scope)
{
    for (int i = 0; i < NUM_REGISTERS; i++)
    {
        if (!scope->registers[i].reserved && !scope->registers[i].immutable)
            return i;
    }

    return none;
}

memreg id_to_reg (char const* id, FunctionScope scope)
{
    if (scope == NULL)
        return none;

    for (int i = 0; i < NUM_REGISTERS; i++)
    {
        if (strcmp(scope->registers[i].id, id) == 0)
            return i;
    }

    return none;
}

char* print_reg (memreg reg)
{
    switch (reg) 
    {
		case rax: return "%rax";
		case rcx: return "%rcx";
		case rdx: return "%rdx";
		case rsi: return "%rsi";
		case rdi: return "%rdi";
		case r8:  return "%r8";
		case r9:  return "%r9";
		case r10: return "%r10";
		case r11: return "%r11";
		default:  return NULL;
	}
}

Struct create_struct (char const* name, symtable* fields)
{
    Struct str = malloc(sizeof(struct Struct));
    memset(str, 0x0, sizeof(struct Struct));
    
    str->name = name;
    str->fields = fields;

    return str;
}

StructList* add_struct (Struct* str, StructList* list)
{
    StructList *new_node = malloc(sizeof(struct StructList));
    new_node->str = str;

    if (0 == list)
        new_node->next = 0;
    else
        new_node->next = list;

    return new_node;
}

StructList* merge_lists (StructList* l1, StructList* l2) 
{
    StructList* new_list = NULL;

    while (l1 != NULL) {
        new_list = add_struct(l1->str, new_list);
        l1 = l1->next;
    }

    while (l2 != NULL) {
        new_list = add_struct(l2->str, new_list);
        l2 = l2->next;
    }

    return new_list;
}

int get_offset(char const* symbol, StructList* list)
{
    while (list != NULL)
    {
        Struct str = *list->str;

        int off = find_offset(symbol, str->fields);
        if (off != -1)
            return off * 8;

        list = list->next;
    }
    
    return -1;
}

int get_sizeof(char const* symbol, StructList* list)
{
    while (list != NULL)
    {
        Struct str = *list->str;

        if (strcmp(symbol, str->name) == 0)
        {
            int num = get_num(str->fields);
            return num * 8; // in byte
        }
    }
    
    return -1;
}