#ifndef COMPILERBAU_CODEGEN_H
#define COMPILERBAU_CODEGEN_H

#include "tree.h"

void generate_function_header(char* name);
void generate_function_footer();

void generate_return(Tree node);
void generate_negation(Tree node);
void generate_not(Tree node);
void generate_cmp(Tree node, char* cmp);
void generate_field_access(Tree node);
void generate_sizeof(Tree node);
void generate_sub(Tree node);

memreg get_target(Tree node);
memreg get_source(Tree node);
void generate_op(Tree node, char* op);

#endif