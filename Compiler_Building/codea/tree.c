#include "tree.h"

Tree add_node (int op, Tree left, Tree right, int value, char* id, FunctionScope scope)
{
    Tree new_tree = malloc(sizeof(struct tree));
    
    OP_LABEL(new_tree) = op;
    LEFT_CHILD(new_tree) = left;
    RIGHT_CHILD(new_tree) = right;
    new_tree->id = id;
    new_tree->scope = scope;
	new_tree->result = -1;

    new_tree->value = value;
	if (value)
		new_tree->valid = true;

    return new_tree;
}

void print_tree(Tree t) 
{

	switch (t->op) 
    {
		case T_RET:
			printf("RET");
			break;

		case T_CONST:

			printf("%lli", t->value);
			break;

		case T_VAR:

			printf("%s", t->id);
			break;
        
        case T_ADD:
            printf("+");
            break;

		case T_MUL:
			printf("*");
			break;

		case T_MINUS:
			printf("NEG");
			break;

		case T_NOT:
			printf("NOT");
			break;

		case T_SUB:
			printf("-");
			break;

		case T_OR:
			printf("or");
			break;

		case T_BELOW:
			printf("<");
			break;

		case T_EQUAL:
			printf("=");
			break;

		case T_FIELD:
			printf("field");
			break;

		case T_SIZEOF:
			printf("sizeof");
			break;

		default:
			printf("Unsupported");
			break;
	}

	printf("(");
	if(t->left != NULL)
		print_tree(t->left);
	
	printf(",");
	if(t->right != NULL)
		print_tree(t->right);
	
	printf(")");

	return;
}