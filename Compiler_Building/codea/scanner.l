%{
void yyerror(char *s);
#include <string.h>
#include "y.tab.h"
#include "symtable.h"
#include "tree.h"
%}

%%

struct                  return STRUCT;
end                     return END;
return                  return RETURN;
if                      return IF;
then                    return THEN;
else                    return ELSE;
while                   return WHILE;
do                      return DO;
var                     return VAR;
or                      return OR;
not                     return NOT;
sizeof                  return SIZEOF;
:=                      return DEF;

[a-zA-Z][_a-zA-z0-9]*   yylval.id  = yytext; return ID; @{ @ID.name@ = strdup(yytext);  @}
[0-9a-fA-F]+H           yylval.num = strtol(yytext, NULL, 16); return NUMBER; @{ @NUMBER.value@ = yylval.num;  @}
[0-9]+                  yylval.num = strtol(yytext, NULL, 10); return NUMBER; @{ @NUMBER.value@ = yylval.num;  @}
\-\-.*                  ;

";"                     return ';';
"="                     return '=';
"("                     return '(';
")"                     return ')';
","                     return ',';
"."                     return '.';
"+"                     return '+';
"-"                     return '-';
"*"                     return '*';
"<"                     return '<';

[ \n\t]                 ;
.                       printf("invalid character: %s\n", yytext); exit(1);

%%