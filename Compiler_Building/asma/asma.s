	.file	"asma.c"
	.text
	.globl	asma
	.type	asma, @function
asma:
.LFB0:
	.cfi_startproc
	/* 
		(64 bit each)
		%rdi = x0 
		%rsi = x1
		%rdx = x2
		%rcx = y
		%r8  = z (address)

		x2x1 / y -> z1, r1
		r1x0 / y -> z0, r0
		
		result = z1z0, r0
	*/

	/* move x1 to %rax and do x2x1 / y */
	/* quotient (z1) in %rax, remainder (r1) in %rdx */
	movq	%rsi, %rax
	div		%rcx

	movq	%rax, %r9 /* move z1 to %r9 */
	
	/* move x0 to correct register and do r1x0 / y */
	/* quotient (z0) in %rax, remainder (r0) in %rdx */
	movq	%rdi, %rax
	div		%rcx

	/* move results to correct addresses / registers */
	movq	%rax, (%r8)
	movq	%r9, 8(%r8)
	movq	%rdx, %rax

	ret
	.cfi_endproc
.LFE0:
	.size	asma, .-asma
	.ident	"GCC: (GNU) 7.3.0"
	.section	.note.GNU-stack,"",@progbits
