unsigned long asma (unsigned long x0, unsigned long x1, unsigned long x2, 
                    unsigned long y, unsigned long *z)
{
    /* 
		(64 bit each)
		%rdi = x0 
		%rsi = x1
		%rdx = x2

		%rcx = y
		%r8  = z (address)

		x2x1 / y -> z1, r1
		r1x0 / y -> z0, r0
		
		result = z1z0, r0
	*/
    x0 = x1 / y;
    return x0;
}