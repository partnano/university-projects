#include <stdio.h>
#include <string.h>
#include <mcheck.h>
#include <stdlib.h>

unsigned long asma_callchecking(unsigned long xx0, unsigned long xx1,  
				unsigned long xx2, unsigned long y,
				unsigned long *z);

int test(unsigned long x0, unsigned long x1, unsigned long x2, unsigned long y,
	 unsigned long r0, unsigned long r1, unsigned long rest)
{
  unsigned long r[]={0,0,0,0};
  unsigned long expected[]={0,r0,r1,0};
  int f;
  int i;

  printf("calling asma(0x%lx,0x%lx,0x%lx,%p,z) ", x0, x1, x2, y, r+1);
  unsigned long rest1 = asma_callchecking(x0,x1,x2,y,r+1);
  for (i=0, f=1; i<4; i++)
    if (r[i]!=expected[i]) {
      printf("\n %p: 0x%lx, erwartet: 0x%lx",r+i,r[i],expected[i]);
      f=0;
    }
  if (rest1 != rest) {
    printf("\nrest: 0x%lx, erwartet: 0x%lx",rest1, rest);
    f=0;
  }
  if (f)
    printf("\nrest: 0x%lx, erwartet: 0x%lx",rest1, rest);
    printf("ok");
  printf("\n");
  return f;
}

int main()
{
  int f;

  f  = test(2,3,4,8,0x6000000000000000UL,0x8000000000000000,2);
  f &= test(0x4d6ac0f7ef7e68faL,0x3e6643906dfed2fdL,0x9260351bc013dfa0L,
	    0xdaab40b04479c810L,
	    0xEC00D4B334E0FD6L,0xAB5D713112B271C0L,
	    0x7B48CCF56A183B9AL);
  if (!f)
    fprintf(stdout,"\nTest failed.\n");
  else
    fprintf(stdout,"\nTest succeeded.\n");
  return !f;
}
