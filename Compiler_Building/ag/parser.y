%{
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "symtable.h"

int yylex();
void yyerror(char *s);
%}

%token STRUCT END RETURN IF THEN ELSE WHILE DO VAR OR NOT SIZEOF DEF

%union {
	long num;
	char *id;
}

%token <num> NUMBER
%token <id>  ID

@attributes
{
    char *name;
} ID

@attributes 
{ 
    char *name;
    symtable *ids; 
} Structdef

@attributes
{
    symtable *scope;
} Ids

@attributes
{
    symtable *structname_scope;
    symtable *field_scope;
} Funcdef

@attributes
{
    symtable *structname_forw;
    symtable *field_forw;
    symtable *structname_back;
    symtable *field_back;
} Program

@attributes
{
    symtable *structnames;
    symtable *fields;
    symtable *variables;
} Stats Pars

@attributes
{
    symtable *structnames;
    symtable *fields;
    symtable *variables;
    symtable *new_variable;
} Stat

@autoinh var_pass field_pass structname_pass

@attributes
{
    symtable *var_pass;
    symtable *field_pass;
    symtable *structname_pass;
} Exprs Expr Lexpr ValDefs Term NegatedTerm PlusTerm MultTerm OrTerm SmallEqTerm

@traversal @preorder PRE
@traversal @postorder POST

%%

Start:
    Program
    @{
        @i @Program.structname_forw@ = NULL;
        @i @Program.field_forw@ = NULL;
    @}
    ;

Program: 
        @{
            @i @Program.structname_back@ = NULL;
            @i @Program.field_back@ = NULL;
        @}
    |   Funcdef ';' Program
        @{
            @i @Program.0.structname_back@ = @Program.1.structname_back@;
            @i @Program.0.field_back@ = @Program.1.field_back@;

            @i @Program.1.structname_forw@ = @Program.0.structname_forw@;
            @i @Program.1.field_forw@ = @Program.0.field_forw@;

            @i @Funcdef.structname_scope@ = merge(@Program.1.structname_forw@, @Program.0.structname_back@);
            @i @Funcdef.field_scope@ = merge(@Program.1.field_forw@, @Program.0.field_back@);
        @}
    |   Structdef ';' Program
        @{
            /* check if structdef correct */
            @PRE checkif_nodouble(add_symbol(@Structdef.name@, @Structdef.ids@));

            /* for every struct that is further ahead in the code (therefore "comes backwards") */
            @PRE checkif_unused(@Structdef.name@, @Program.1.structname_back@);

            @i @Program.0.structname_back@ = add_symbol(@Structdef.name@, @Program.1.structname_back@);
            @i @Program.0.field_back@ = merge(@Structdef.ids@, @Program.1.field_back@);

            @POST checkif_nodouble(@Program.0.field_back@);

            /* for every struct that is further behind in the code (therefore "comes forwards") */
            @PRE checkif_unused(@Structdef.name@, @Program.0.structname_forw@);

            @i @Program.1.structname_forw@ = add_symbol(@Structdef.name@, @Program.0.structname_forw@);
            @i @Program.1.field_forw@ = merge(@Structdef.ids@, @Program.0.field_forw@);

            @POST checkif_nodouble(@Program.1.field_forw@);
        @}
    ;

Structdef:
        STRUCT ID '=' Ids END
        @{
            @i @Structdef.name@ = @ID.name@;
            @i @Structdef.ids@ = @Ids.scope@;
        @}
    ;

Funcdef:
        ID '(' Pars ')' Stats END
        @{
            @i @Pars.structnames@ = @Funcdef.structname_scope@;
            @i @Pars.fields@ = @Funcdef.field_scope@;

            @i @Stats.structnames@ = @Funcdef.structname_scope@;
            @i @Stats.fields@ = @Funcdef.field_scope@;
            @i @Stats.variables@ = @Pars.variables@;
        @}
    ;

Ids:

        @{
            @i @Ids.scope@ = NULL;
        @}    
    |   ID Ids
        @{
            @PRE checkif_unused(@ID.name@, @Ids.1.scope@);

            @i @Ids.0.scope@ = add_symbol(@ID.name@, @Ids.1.scope@);
        @}
    ;

Pars:
        
        @{
            @i @Pars.variables@ = NULL;
        @}
    |   Pars ',' ID
        @{
            @PRE checkif_unused(@ID.name@, @Pars.1.variables@);
            @PRE checkif_unused(@ID.name@, @Pars.0.structnames@);
            @PRE checkif_unused(@ID.name@, @Pars.0.fields@);
        
            @i @Pars.0.variables@ = add_symbol(@ID.name@, @Pars.1.variables@);
            @i @Pars.1.fields@ = @Pars.0.fields@;
            @i @Pars.1.structnames@ = @Pars.0.structnames@;
        @}
    |   ID
        @{
            @PRE checkif_unused(@ID.name@, @Pars.0.structnames@);
            @PRE checkif_unused(@ID.name@, @Pars.0.fields@);

            @i @Pars.variables@ = add_symbol(@ID.name@, NULL);
        @}
    ;

Stats:

    |   Stat ';' Stats
        @{
            @i @Stat.variables@ = @Stats.0.variables@;
            @i @Stat.fields@ = @Stats.0.fields@;
            @i @Stat.structnames@ = @Stats.0.structnames@;

            @i @Stats.1.variables@ = merge(@Stat.new_variable@, @Stats.0.variables@);
            @i @Stats.1.fields@ = @Stat.fields@;
            @i @Stats.1.structnames@ = @Stat.structnames@;
        @}
    ;

Stat:
        RETURN Expr
        @{
            @i @Expr.var_pass@ = @Stat.variables@;
            @i @Expr.field_pass@ = @Stat.fields@;
            @i @Expr.structname_pass@ = @Stat.structnames@;

            @i @Stat.new_variable@ = NULL;
        @}
    |   IF Expr THEN Stats END
        @{
            @i @Expr.var_pass@ = @Stat.variables@;
            @i @Expr.field_pass@ = @Stat.fields@;
            @i @Expr.structname_pass@ = @Stat.structnames@;

            @i @Stats.variables@ = @Stat.variables@;
            @i @Stats.fields@ = @Stat.fields@;
            @i @Stats.structnames@ = @Stat.structnames@;

            @i @Stat.new_variable@ = NULL;
        @}
    |   IF Expr THEN Stats ELSE Stats END
        @{
            @i @Expr.var_pass@ = @Stat.variables@;
            @i @Expr.field_pass@ = @Stat.fields@;
            @i @Expr.structname_pass@ = @Stat.structnames@;

            @i @Stats.0.variables@ = @Stat.variables@;
            @i @Stats.0.fields@ = @Stat.fields@;
            @i @Stats.0.structnames@ = @Stat.structnames@;

            @i @Stats.1.variables@ = @Stat.variables@;
            @i @Stats.1.fields@ = @Stat.fields@;
            @i @Stats.1.structnames@ = @Stat.structnames@;

            @i @Stat.new_variable@ = NULL;
        @}
    |   WHILE Expr DO Stats END
        @{
            @i @Expr.var_pass@ = @Stat.variables@;
            @i @Expr.field_pass@ = @Stat.fields@;
            @i @Expr.structname_pass@ = @Stat.structnames@;

            @i @Stats.variables@ = @Stat.variables@;
            @i @Stats.fields@ = @Stat.fields@;
            @i @Stats.structnames@ = @Stat.structnames@;

            @i @Stat.new_variable@ = NULL;
        @}
    |   VAR ID DEF Expr
        @{
            @PRE checkif_unused(@ID.name@, @Stat.variables@);

            @i @Expr.var_pass@ = @Stat.variables@;
            @i @Expr.field_pass@ = @Stat.fields@;
            @i @Expr.structname_pass@ = @Stat.structnames@;

            @i @Stat.new_variable@ = add_symbol(@ID.name@, NULL);
        @}
    |   ValDefs Expr
        @{
            @i @ValDefs.var_pass@ = @Stat.variables@;
            @i @ValDefs.field_pass@ = @Stat.fields@;
            @i @ValDefs.structname_pass@ = @Stat.structnames@;

            @i @Expr.var_pass@ = @Stat.variables@;
            @i @Expr.field_pass@ = @Stat.fields@;
            @i @Expr.structname_pass@ = @Stat.structnames@;

            @i @Stat.new_variable@ = NULL;
        @}
    |   Expr
        @{
            @i @Expr.var_pass@ = @Stat.variables@;
            @i @Expr.field_pass@ = @Stat.fields@;
            @i @Expr.structname_pass@ = @Stat.structnames@;

            @i @Stat.new_variable@ = NULL;
        @}
    ;

ValDefs:
        Lexpr DEF
    |   ValDefs Lexpr DEF
    ;

Lexpr:
        ID
        @{
            /* can only be a variable */
            @PRE checkif_used(@ID.name@, @Lexpr.var_pass@);
        @}
    |   Term '.' ID
        @{
            @PRE checkif_used(@ID.name@, @Lexpr.field_pass@);

            @i @Term.var_pass@ = @Lexpr.var_pass@;
        @}
    ;

Term:
        '(' Expr ')'
    |   SIZEOF ID
        @{
            @PRE checkif_used(@ID.name@, @Term.structname_pass@);
        @}
    |   NUMBER
    |   Term '.' ID
        @{
            @PRE checkif_used(@ID.name@, @Term.0.field_pass@);

            @i @Term.1.var_pass@ = @Term.0.var_pass@;
        @}
    |   ID
        @{
            @PRE checkif_used(@ID.name@, @Term.var_pass@);
        @}
    |   ID '(' ')'
    |   ID '(' Exprs ')'
    ;

Expr:
        NegatedTerm
    |   PlusTerm
    |   Term '-' Term
    |   MultTerm
    |   OrTerm
    |   SmallEqTerm
    |   Term
    ;

NegatedTerm:
        NOT Term
    |   '-' Term
    |   NOT NegatedTerm
    |   '-' NegatedTerm
    ;

PlusTerm:
        Term '+' Term
    |   PlusTerm '+' Term
    ;

MultTerm:
        Term '*' Term
    |   MultTerm '*' Term
    ;

OrTerm:
        Term OR Term
    |   OrTerm OR Term
    ;

SmallEqTerm:
        Term '<' Term
    |   Term '=' Term
    ;

Exprs:
        Expr ',' Exprs
    |   Expr
    ;

%%

// stuff from lex that yacc needs to know about:
extern int yylex();
extern int yyparse();
extern FILE *yyin;

int main (void)
{
    do {
		yyparse();
	} while (!feof(yyin));

    return 0;
}

void yyerror(char *s) {
	printf("parse error (%s)\n", s);
	exit(2);
}