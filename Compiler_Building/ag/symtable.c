#include "symtable.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

symtable* add_symbol (char *symbol, symtable *table)
{
    symtable *new_tab = malloc(sizeof(symtable));
    new_tab->symbol = symbol;

    if (0 == table)
        new_tab -> next = 0;
    else
        new_tab -> next = table;

    return new_tab;
}

symtable* find (char *symbol, symtable *table)
{
    while (table != NULL)
    {
        if(strcmp(symbol, table->symbol) == 0)
            return table;
        else
            table = table->next;
    }

    return NULL;
}

symtable* merge (symtable* t1, symtable* t2) {
    symtable* new_tab = NULL;

    while (t1 != NULL) {
        new_tab = add_symbol(t1->symbol, new_tab);
        t1 = t1->next;
    }

    while (t2 != NULL) {
        new_tab = add_symbol(t2->symbol, new_tab);
        t2 = t2->next;
    }

    return new_tab;
}

void symprint(symtable *table) {
	printf("\n--TABLE START--\n");
	while(table != NULL) 
    {
		printf("%s\n", table->symbol);
		table = table->next;
	}
	printf("-- TABLE END --\n");
}

void checkif_unused (char *symbol, symtable *table)
{
    if (find(symbol, table) != NULL)
    {
        fprintf(stderr, "Duplicate symbol %s\n", symbol);
        symprint(table);
        exit(3);
    }
}

void checkif_used (char *symbol, symtable *table)
{
    if (find(symbol, table) == NULL)
    {
        fprintf(stderr, "Couldn't find symbol %s\n", symbol);
        symprint(table);
        exit(3);
    }
}

void checkif_nodouble (symtable *table)
{
    while (table != NULL)
    {
        checkif_unused(table->symbol, table->next);
        table = table->next;
    }
}