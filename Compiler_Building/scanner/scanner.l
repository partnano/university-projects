%{
#define ERROR -1
#define KEYWORD 1
#define IDENTIFIER 2
#define DECNUMBER 3
#define HEXNUMBER 4
#define SYMBOL 5
#define COMMENT 6
%}

kw  struct|end|return|if|then|else|while|do|var|or|not|sizeof

%%
{kw}                    return KEYWORD;
[a-zA-Z][_a-zA-z0-9]*   return IDENTIFIER;
[0-9a-fA-F]+H           return HEXNUMBER;
[0-9]+                  return DECNUMBER;
\-\-.*                  return COMMENT;
:=                      return SYMBOL;
[;=\(\),\.\+\-\*\<]     return SYMBOL;
[ \n\t]                 ;
.                       return ERROR;
%%

int main (void)
{
    // yay, a print right here killed my entire submission
    int ret_value = yylex();
    
    while (ret_value)
    {
        long cast;
        switch(ret_value)
        {   
            case KEYWORD:
                printf("%s\n", yytext);
                break;
            
            case IDENTIFIER:
                printf("id %s\n", yytext);
                break;
            
            case DECNUMBER:
                cast = strtol(yytext, NULL, 10);
                printf("num %d\n", cast);
                break;
            
            case HEXNUMBER:
                cast = strtol(yytext, NULL, 16);
                printf("num %d\n", cast);
                break;
            
            case SYMBOL:
                printf("%s\n", yytext);
                break;
            
            case ERROR:
                printf("unexpected character %s\n", yytext);
                exit(1);

            default:
                // ignore everything else
                break;

        }
        
        ret_value = yylex();
    }

    return 0;
}