typedef struct burm_state *STATEPTR_TYPE;

#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include "tree.h"

static int reg_no=0;
int getreg(void)
{
   return reg_no++;
}

/* symbols below must also be defined
   definition in tree.h!!!
*/

#ifndef ALLOC
#define ALLOC(n) malloc(n)
#endif

#ifndef burm_assert
#define burm_assert(x,y) if (!(x)) { extern void abort(void); y; abort(); }
#endif

#define burm_expr_NT 1
#define burm_reg_NT 2
#define burm_con_NT 3
int burm_max_nt = 3;

struct burm_state {
	int op;
	STATEPTR_TYPE left, right;
	short cost[4];
	struct {
		unsigned burm_expr:1;
		unsigned burm_reg:3;
		unsigned burm_con:2;
	} rule;
};

static short burm_nts_0[] = { burm_reg_NT, 0 };
static short burm_nts_1[] = { burm_reg_NT, burm_reg_NT, 0 };
static short burm_nts_2[] = { 0 };
static short burm_nts_3[] = { burm_con_NT, 0 };
static short burm_nts_4[] = { burm_con_NT, burm_con_NT, 0 };

short *burm_nts[] = {
	0,	/* 0 */
	burm_nts_0,	/* 1 */
	burm_nts_1,	/* 2 */
	burm_nts_2,	/* 3 */
	burm_nts_2,	/* 4 */
	burm_nts_3,	/* 5 */
	burm_nts_4,	/* 6 */
	burm_nts_2,	/* 7 */
};

char burm_arity[] = {
	0,	/* 0 */
	2,	/* 1=ADD */
	0,	/* 2=CONST */
	0,	/* 3=VAR */
};

static short burm_decode_expr[] = {
	0,
	1,
};

static short burm_decode_reg[] = {
	0,
	2,
	3,
	4,
	5,
};

static short burm_decode_con[] = {
	0,
	6,
	7,
};

int burm_rule(STATEPTR_TYPE state, int goalnt) {
	burm_assert(goalnt >= 1 && goalnt <= 3, PANIC("Bad goal nonterminal %d in burm_rule\n", goalnt));
	if (!state)
		return 0;
	switch (goalnt) {
	case burm_expr_NT:
		return burm_decode_expr[state->rule.burm_expr];
	case burm_reg_NT:
		return burm_decode_reg[state->rule.burm_reg];
	case burm_con_NT:
		return burm_decode_con[state->rule.burm_con];
	default:
		burm_assert(0, PANIC("Bad goal nonterminal %d in burm_rule\n", goalnt));
	}
	return 0;
}

static void burm_closure_reg(STATEPTR_TYPE, int);
static void burm_closure_con(STATEPTR_TYPE, int);

static void burm_closure_reg(STATEPTR_TYPE p, int c) {
	if (c + 1 < p->cost[burm_expr_NT]) {
		p->cost[burm_expr_NT] = c + 1;
		p->rule.burm_expr = 1;
	}
}

static void burm_closure_con(STATEPTR_TYPE p, int c) {
	if (c + 1 < p->cost[burm_reg_NT]) {
		p->cost[burm_reg_NT] = c + 1;
		p->rule.burm_reg = 4;
		burm_closure_reg(p, c + 1);
	}
}

STATEPTR_TYPE burm_state(int op, STATEPTR_TYPE left, STATEPTR_TYPE right) {
	int c;
	STATEPTR_TYPE p, l = left, r = right;

	if (burm_arity[op] > 0) {
		p = (void *)ALLOC(sizeof *p);
		burm_assert(p, PANIC("ALLOC returned NULL in burm_state\n"));
		p->op = op;
		p->left = l;
		p->right = r;
		p->rule.burm_expr = 0;
		p->cost[1] =
		p->cost[2] =
		p->cost[3] =
			32767;
	}
	switch (op) {
	case 1: /* ADD */
		assert(l && r);
		{	/* con: ADD(con,con) */
			c = l->cost[burm_con_NT] + r->cost[burm_con_NT] + 0;
			if (c + 0 < p->cost[burm_con_NT]) {
				p->cost[burm_con_NT] = c + 0;
				p->rule.burm_con = 1;
				burm_closure_con(p, c + 0);
			}
		}
		{	/* reg: ADD(reg,reg) */
			c = l->cost[burm_reg_NT] + r->cost[burm_reg_NT] + 1;
			if (c + 0 < p->cost[burm_reg_NT]) {
				p->cost[burm_reg_NT] = c + 0;
				p->rule.burm_reg = 1;
				burm_closure_reg(p, c + 0);
			}
		}
		break;
	case 2: /* CONST */
		{
			static struct burm_state z = { 2, 0, 0,
				{	0,
					2,	/* expr: reg */
					1,	/* reg: CONST */
					0,	/* con: CONST */
				},{
					1,	/* expr: reg */
					3,	/* reg: CONST */
					2,	/* con: CONST */
				}
			};
			return &z;
		}
	case 3: /* VAR */
		{
			static struct burm_state z = { 3, 0, 0,
				{	0,
					2,	/* expr: reg */
					1,	/* reg: VAR */
					32767,
				},{
					1,	/* expr: reg */
					2,	/* reg: VAR */
					0,
				}
			};
			return &z;
		}
	default:
		burm_assert(0, PANIC("Bad operator %d in burm_state\n", op));
	}
	return p;
}

#ifdef STATE_LABEL
static void burm_label1(NODEPTR_TYPE p) {
	burm_assert(p, PANIC("NULL tree in burm_label\n"));
	switch (burm_arity[OP_LABEL(p)]) {
	case 0:
		STATE_LABEL(p) = burm_state(OP_LABEL(p), 0, 0);
		break;
	case 1:
		burm_label1(LEFT_CHILD(p));
		STATE_LABEL(p) = burm_state(OP_LABEL(p),
			STATE_LABEL(LEFT_CHILD(p)), 0);
		break;
	case 2:
		burm_label1(LEFT_CHILD(p));
		burm_label1(RIGHT_CHILD(p));
		STATE_LABEL(p) = burm_state(OP_LABEL(p),
			STATE_LABEL(LEFT_CHILD(p)),
			STATE_LABEL(RIGHT_CHILD(p)));
		break;
	}
}

STATEPTR_TYPE burm_label(NODEPTR_TYPE p) {
	burm_label1(p);
	return STATE_LABEL(p)->rule.burm_expr ? STATE_LABEL(p) : 0;
}

NODEPTR_TYPE *burm_kids(NODEPTR_TYPE p, int eruleno, NODEPTR_TYPE kids[]) {
	burm_assert(p, PANIC("NULL tree in burm_kids\n"));
	burm_assert(kids, PANIC("NULL kids in burm_kids\n"));
	switch (eruleno) {
	case 5: /* reg: con */
	case 1: /* expr: reg */
		kids[0] = p;
		break;
	case 6: /* con: ADD(con,con) */
	case 2: /* reg: ADD(reg,reg) */
		kids[0] = LEFT_CHILD(p);
		kids[1] = RIGHT_CHILD(p);
		break;
	case 7: /* con: CONST */
	case 4: /* reg: CONST */
	case 3: /* reg: VAR */
		break;
	default:
		burm_assert(0, PANIC("Bad external rule number %d in burm_kids\n", eruleno));
	}
	return kids;
}

int burm_op_label(NODEPTR_TYPE p) {
	burm_assert(p, PANIC("NULL tree in burm_op_label\n"));
	return OP_LABEL(p);
}

STATEPTR_TYPE burm_state_label(NODEPTR_TYPE p) {
	burm_assert(p, PANIC("NULL tree in burm_state_label\n"));
	return STATE_LABEL(p);
}

NODEPTR_TYPE burm_child(NODEPTR_TYPE p, int index) {
	burm_assert(p, PANIC("NULL tree in burm_child\n"));
	switch (index) {
	case 0:	return LEFT_CHILD(p);
	case 1:	return RIGHT_CHILD(p);
	}
	burm_assert(0, PANIC("Bad index %d in burm_child\n", index));
	return 0;
}

#endif
void burm_reduce(NODEPTR_TYPE bnode, int goalnt)
{
  int ruleNo = burm_rule (STATE_LABEL(bnode), goalnt);
  short *nts = burm_nts[ruleNo];
  NODEPTR_TYPE kids[100];
  int i;

  if (ruleNo==0) {
    fprintf(stderr, "tree cannot be derived from start symbol");
    exit(1);
  }
  burm_kids (bnode, ruleNo, kids);
  for (i = 0; nts[i]; i++)
    burm_reduce (kids[i], nts[i]);    /* reduce kids */

#if DEBUG
  printf ("%s", burm_string[ruleNo]);  /* display rule */
#endif

  switch (ruleNo) {
  case 1:
 printf("\treturn r%d\n",bnode->reg_no);
    break;
  case 2:
 bnode->reg_no=getreg(); printf("\tr%d = r%d + r%d\n",bnode->reg_no,LEFT_CHILD(bnode)->reg_no,RIGHT_CHILD(bnode)->reg_no);
    break;
  case 3:
 bnode->reg_no=getreg(); printf("\tr%d = var %s\n",bnode->reg_no,bnode->id);
    break;
  case 4:
 bnode->reg_no=getreg(); printf("\tr%d = cons %d\n",bnode->reg_no,bnode->number);
    break;
  case 5:
 bnode->reg_no=getreg(); printf("\tr%d = cons %d\n",bnode->reg_no,bnode->number);
    break;
  case 6:
 bnode->number = LEFT_CHILD(bnode)->number + RIGHT_CHILD(bnode)->number;
    break;
  case 7:
 bnode->number = bnode->number;
    break;
  default:    assert (0);
  }
}
