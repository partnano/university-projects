#ifndef __TREE_H__
#define __TREE_H__


typedef struct tree {
	int op;                    /* node type */
	struct tree *kids[2];      /* successor nodes */
	                           /* attributes of node (depending on type) */
      	int reg_no;                /* register number for reg */    
       	int number;                /* constant value for con */ 
        char *id;                  /* variable name */
        struct burm_state* state;       /* state variable for BURG */
} *NODEPTR_TYPE, *Tree;

#define LEFT_CHILD(p) ((p)->kids[0])
#define RIGHT_CHILD(p) ((p)->kids[1])
#define PANIC printf
#define STATE_LABEL(p) ((p)->state)
#define OP_LABEL(p) ((p)->op)

enum { ADD=1,          /* add node */
       CONST=2,        /* constant */
       VAR=3           /* variable */
     };

#endif
