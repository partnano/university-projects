%{
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tree.h"
#include "y.tab.h"
%}
%%
[ \n\t]
[a-zA-Z_][a-zA-Z_0-9]*	{yylval.id = strdup(yytext); return(VARIABLE);}
[0-9]*			{yylval.number = atoi(yytext); return(CONSTANT);}
.			return(yytext[0]);
