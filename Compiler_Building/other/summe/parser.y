%{
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "tree.h"

/* generate tree node for BURG */
Tree gen_node(int op, Tree left, Tree right, int number, char *id) 
{
    Tree t = malloc(sizeof(struct tree));
    OP_LABEL(t) = op;
    LEFT_CHILD(t) = left;
    RIGHT_CHILD(t) = right;
    t->number = number;
    t->id = id;
    return t;
}

%}
%union{
   char *id;        /* identifier of a variable */
   int  number;     /* number */
   Tree node;       /* tree node pointer */
}
%token <number>  CONSTANT 
%token <id>      VARIABLE
%type  <node>    expr
%start start 
%left '+'
%%
start : expr          { /* generate code for input */
  	                if (burm_label($1) == 0)
                  	   fprintf(stderr, "no cover\n");
			else 
		           burm_reduce($1, 1);
		      }
      ;

expr  : expr '+'expr  { $$ = gen_node(ADD,$1,$3,0,NULL); }
      | CONSTANT      { $$ = gen_node(CONST,NULL,NULL,$1,NULL); }
      | VARIABLE      { $$ = gen_node(VAR,NULL,NULL,0,$1); }
      | '(' expr ')'  { $$ = $2; } 
      ;

%% 
int yyerror(char *s) 
{
  fprintf(stderr,"%s\n",s);
}

int main()
{
 return yyparse(); 
}
