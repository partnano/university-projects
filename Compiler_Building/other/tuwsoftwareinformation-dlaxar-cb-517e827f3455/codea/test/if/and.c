#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

uint64_t nd(uint64_t a, uint64_t b, uint64_t c);

int main() {
	uint64_t val;

	val = nd(3, 2, 1);

	printf("nd(3, 2, 1) = %llu == 0\n", val);
	if(val != 0) {
		return 1;
	}

	val = nd(1, 1, 1);

	printf("nd(1, 1, 1) = %llu == 1\n", val);
	if(val != 1) {
		return 2;
	}
}

