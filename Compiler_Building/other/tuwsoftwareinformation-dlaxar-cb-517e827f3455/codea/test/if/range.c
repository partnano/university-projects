#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

uint64_t range(uint64_t a);

int main() {

	uint64_t val;


	printf("range(200) = %llu == 1\n", val = range(200));
	if(val != 1) {
		return 1;
	}

	printf("range(201) = %llu == 0\n", val = range(201));
	if(val != 0) {
		return 2;
	}

	printf("range(400) = %llu == 0\n", val = range(400));
	if(val != 0) {
		return 3;
	}

	printf("range(500) = %llu == 1\n", val = range(500));
	if(val != 1) {
		return 4;
	}

	return 0;
}

