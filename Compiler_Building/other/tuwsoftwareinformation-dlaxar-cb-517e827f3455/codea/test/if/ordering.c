#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

uint64_t ordering(uint64_t a, uint64_t b, uint64_t c);

int main() {

	uint64_t val;

	printf("ordering(1, 2, 1) = %llu == 0\n", val = ordering(1, 2, 1));
	if(val != 0) {
		return 3;
	}

	printf("ordering(3, 2, 1) = %llu == 1\n", val = ordering(3, 2, 1));
	if(val != 1) {
		return 4;
	}

	return 0;
}

