#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

uint64_t parity(uint64_t a);

int main() {

	uint64_t val;


	printf("parity(0) = %llu == 0\n", val = parity(0));
	if(val != 0) {
		return -1;
	}

	printf("parity(1) = %llu == 1\n", val = parity(1));
	if(val != 1) {
		return 1;
	}

	printf("parity(2) = %llu == 0\n", val = parity(2));
	if(val != 0) {
		return 2;
	}

	printf("parity(3) = %llu == 1\n", val = parity(3));
	if(val != 1) {
		return 3;
	}

	printf("parity(4) = %llu == 0\n", val = parity(4));
	if(val != 0) {
		return 4;
	}

	printf("parity(5) = %llu == 1\n", val = parity(5));
	if(val != 1) {
		return 5;
	}

	printf("parity(6) = %llu == 0\n", val = parity(6));
	if(val != 0) {
		return 6;
	}

	printf("parity(7) = %llu == 1\n", val = parity(7));
	if(val != 1) {
		return 7;
	}

	printf("parity(8) = %llu == 0\n", val = parity(8));
	if(val != 0) {
		return 8;
	}

	printf("parity(9) = %llu == 1\n", val = parity(9));
	if(val != 1) {
		return 9;
	}

	printf("parity(10) = %llu == 0\n", val = parity(10));
	if(val != 0) {
		return 10;
	}

	printf("parity(11) = %llu == 0\n", val = parity(11));
	if(val != 0) {
		return 11;
	}

	return 0;
}

