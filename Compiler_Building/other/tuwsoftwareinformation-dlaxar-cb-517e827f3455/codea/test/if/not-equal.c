#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

uint64_t notEqual();

int main() {
	uint64_t val = notEqual();

	printf("notEqual() = %llu == 0\n", val);
	return val;
}

