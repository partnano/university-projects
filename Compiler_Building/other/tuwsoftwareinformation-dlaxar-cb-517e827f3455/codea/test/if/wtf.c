#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

uint64_t wtf(uint64_t a, uint64_t b, uint64_t c);
uint64_t notzero(uint64_t a);

int main() {

	uint64_t val;

	printf("notzero(1) = %llu == 1\n", val = notzero(1));
	if(val != 1) {
		return 3;
	}

	printf("notzero(0) = %llu == 0\n", val = notzero(0));
	if(val != 0) {
		return 4;
	}

	printf("wtf(1, 1, 2) = %llu == 1\n", val = wtf(1, 1, 2));
	if(val != 1) {
		return 5;
	}

	printf("wtf(1, 2, 2) = %llu == 0\n", val = wtf(1, 2, 2));
	if(val != 0) {
		return 6;
	}

	printf("wtf(2, 1, 2) = %llu == 0\n", val = wtf(2, 1, 2));
	if(val != 0) {
		return 7;
	}

	printf("wtf(0, 0, 1) = %llu == 0\n", val = wtf(0, 0, 1));
	if(val != 0) {
		return 8;
	}
	return 0;
}

