#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

uint64_t n();

int main() {
	uint64_t val;

	val = n();

	printf("n() = %llu == 0\n", val);
	if(val != 0) {
		return 1;
	}
}

