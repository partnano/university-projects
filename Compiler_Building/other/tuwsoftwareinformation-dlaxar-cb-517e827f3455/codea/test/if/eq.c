#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

uint64_t eq(uint64_t a, uint64_t b, uint64_t c);

int main() {

	uint64_t val;

	printf("eq(255, 255, 255) = %llu == 0\n", val = eq(255, 255, 255));
	if (val != 0) {
		return 1;
	}

	printf("eq(255, 200, 255) = %llu == 1\n", val = eq(255, 200, 255));
	if (val != 1) {
		return 2;
	}

	printf("eq(255, 200, 244) = %llu == 1\n", val = eq(255, 200, 244));
	if (val != 1) {
		return 3;
	}

	printf("eq(255, 255, 255) = %llu == 1\n", val = eq(255, 255, 0));
	if (val != 1) {
		return 4;
	}
	return 0;
}