#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

uint64_t many(uint64_t one,
              uint64_t two,
              uint64_t three,
              uint64_t four,
              uint64_t five,
              uint64_t six,
              uint64_t seven,
uint64_t eight);

int main() {

	uint64_t val;


	printf("many(1, 2, 3, 4, 5, 6, 7, 8) = %llu == 15\n", val = many(1, 2, 3, 4, 5, 6, 7, 8));
	if (val != 15) {
		return 1;
	}

	return 0;
}

