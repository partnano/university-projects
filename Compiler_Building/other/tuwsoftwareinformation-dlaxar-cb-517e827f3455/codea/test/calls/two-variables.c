#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

uint64_t fn(uint64_t a, uint64_t b);

int main() {
	return fn(1, 2)-3;
}