#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

uint64_t callC(uint64_t arg, uint64_t b);

void cprint(uint64_t arg) {
	printf("%llu\n", arg);
}

int main() {
	uint64_t code = callC(255, 1);

	return code - 255;
}