#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

uint64_t fn(uint64_t a);

int main() {
	return fn(255) - 255;
}