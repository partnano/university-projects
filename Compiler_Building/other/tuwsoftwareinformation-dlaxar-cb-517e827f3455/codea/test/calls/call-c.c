#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

void callC(uint64_t arg);
void callCB(uint64_t arg, uint64_t, uint64_t, uint64_t, uint64_t, uint64_t, uint64_t, uint64_t, uint64_t);
int called = 11;
int calledB = 15;

void cprint(uint64_t arg) {
	called = (int) strtol("50", NULL, 10);
	called = arg;
	printf("%llu\n", arg);
}

void cprintB(uint64_t arg) {
	calledB = (int) strtol("50", NULL, 10);
	calledB = arg;
	printf("%llu\n", arg);
}

int main() {
	printf("Text0\n");
	callC(255);
	printf("Text1\n");
	callCB(255, 1, 2, 3, 4, 5, 6, 7, 8);
	printf("Text2\n");

	return called-255 + calledB-255;
}