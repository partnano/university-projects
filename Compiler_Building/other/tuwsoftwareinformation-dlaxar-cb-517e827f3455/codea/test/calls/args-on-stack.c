#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

uint64_t fn(uint64_t, uint64_t, uint64_t, uint64_t, uint64_t, uint64_t, uint64_t, uint64_t);
void cprint(uint64_t a) {
	printf("%llu\n", a);
}

int main() {
	return fn(1, 2, 3, 4, 5, 6, 7, 8);
}