#include <stdio.h>
#include <stdint.h>

unsigned long long assign(unsigned long long x);

int main() {
	unsigned long long val = assign(255);
	printf("assign(255) = %llu == 255\n", val);
	if(val != 255) {
		return 1;
	}

	return 0;
}