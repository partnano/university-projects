#include <stdio.h>
#include <stdint.h>

unsigned long long assign();

int main() {
	unsigned long long val = assign();
	printf("assign() = %llu == 255\n", val);
	if(val != 255) {
		return 1;
	}

	return 0;
}