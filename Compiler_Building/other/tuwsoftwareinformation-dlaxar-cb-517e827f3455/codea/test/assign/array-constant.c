#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

uint64_t reassign(uint64_t* array, uint64_t value);

int main() {
	uint64_t* array = malloc(sizeof(uint64_t) * 5);

	reassign(array, 255);

	uint64_t val = array[1];
	printf("reassign array[1] = %llu == 255\n", val);
	if(val != 255) {
		return 1;
	}

	return 0;
}