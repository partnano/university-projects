#include <stdio.h>
#include <stdint.h>

unsigned long long reassign(unsigned long long x, unsigned long long y);

int main() {
	unsigned long long val = reassign(255, 20);
	printf("reassign(255, 20) = %llu == 255\n", val);
	if(val != 255) {
		return 1;
	}

	return 0;
}