#include <stdio.h>
#include <stdint.h>

unsigned long long reassign(unsigned long long x);

int main() {
	unsigned long long val = reassign(50);
	printf("reassign(50) = %llu == 255\n", val);
	if(val != 255) {
		return 1;
	}

	return 0;
}