#include <printf.h>
#include <stdint.h>

extern int add(unsigned long a, unsigned long b);
extern int sub(unsigned long a, unsigned long b);
extern int sub2(unsigned long a, unsigned long b, unsigned long c);
extern int mult(unsigned long a, unsigned long b);
extern int samevar(unsigned long a, unsigned long b);
extern int comb(unsigned long a, unsigned long b, unsigned long c);

int main(int argc, char const* argv[]) {
	printf("add(1, 3) = %d == 4\n", add(1, 3));
	if(add(1, 3) != 4) {
		return 1;
	}

	printf("> %d == 2\n", sub2(8, 4, 2));


	printf("sub(1, 3) = %d == -2\n", sub(1, 3));
	if(sub(1, 3) != -2) {
		return 2;
	}

	printf("mult(50, 3) = %d == 150\n", mult(50, 3));
	if(mult(50, 3) != 150) {
		return 2;
	}

	printf("samevar(1, 3) = %d == 7\n", samevar(1, 3));
	if(samevar(1, 3) != 7) {
		return 3;
	}

	printf("comb(1, 2, 3) = %d == 0\n", comb(1, 2, 3));
	if(comb(1, 2, 3) != 0) {
		return 4;
	}

	return 0;
}