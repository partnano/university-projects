#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

uint64_t first(uint64_t* array);
uint64_t second(uint64_t* array);

int main(int argc, char const* argv[]) {
	uint64_t a[5];
	a[0] = 1;
	a[1] = 2;
	a[2] = 3;
	a[3] = 4;
	a[4] = 5;

	printf("first(a) = %llu == 1\n", first(a));
	if(first(a) != 1) {
		return 1;
	}

	printf("second(a) = %llu == 2\n", second(a));
	if(second(a) != 2) {
		return 2;
	}

	return 0;
}