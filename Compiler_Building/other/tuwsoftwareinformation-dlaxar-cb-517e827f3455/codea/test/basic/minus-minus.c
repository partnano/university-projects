#include <printf.h>
#include <stdint.h>

extern int64_t f(int64_t);

int main(int argc, char const* argv[]) {
	printf("%lld == 4\n", f(4));
	return f(4) - 4;
}