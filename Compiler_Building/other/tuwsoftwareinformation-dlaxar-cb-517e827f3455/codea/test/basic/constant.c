#include <printf.h>
#include <stdint.h>

extern int f(void);

int main(int argc, char const* argv[]) {
	printf("%d == 4\n", f());
	return f() - 4;
}