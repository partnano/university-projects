#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

uint64_t m();

int main(int argc, char const* argv[]) {
	printf("m() = %llu == 1,208\n", m());
	return m() - 1208;
}
