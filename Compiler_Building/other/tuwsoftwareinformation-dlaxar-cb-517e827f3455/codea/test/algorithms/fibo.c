#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

uint64_t fibo(uint64_t x);

int main() {
	return fibo(5) - 120;
}