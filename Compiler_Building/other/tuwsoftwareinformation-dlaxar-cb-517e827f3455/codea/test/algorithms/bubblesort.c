#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

void bubblesort(uint64_t* array, uint64_t len);

int main() {

	const size_t ARRAYLEN = 12;

	uint64_t array[] = {
			20,
	        21,
	        40,
	        1,
	        50,
	        17,
	        3,
	        100,
	        98,
	        89,
	        70,
	        3
	};

	for(size_t i = 0; i < ARRAYLEN; i++) {
		printf("%llu ", array[i]);
	}
	printf("\n");

	bubblesort(array, ARRAYLEN);

	// ensure monotony
	uint64_t max = array[0];
	for(size_t i = 0; i < ARRAYLEN; i++) {
		printf("%llu ", array[i]);
		if(max <= array[i]) {
			max = array[i];
		} else {
			return -1;
		}
	}
	printf("\n");

}