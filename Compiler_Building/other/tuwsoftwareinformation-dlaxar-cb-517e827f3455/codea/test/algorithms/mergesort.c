#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

uint64_t mymergesort(uint64_t* array, uint64_t* array2, uint64_t len);
uint64_t divide(uint64_t a, uint64_t b);
void cprint(uint64_t begin, uint64_t middle, uint64_t end) {
	printf("Splitting %llu %llu in half: %llu\n", begin, end, middle);
}

void cprintSorted(uint64_t begin, uint64_t end) {
	printf("Sorted: %llu %llu\n", begin, end);
}

int main() {

	const size_t ARRAYLEN = 12;

	printf("%d/%d = %llu\n", 13, 6, divide(0 + ARRAYLEN, 2));

	uint64_t array[] = {
			20,
			21,
			40,
			1,
			50,
			17,
			3,
			100,
			98,
			89,
			70,
			3
	};

	uint64_t array2[12];

	for(size_t i = 0; i < ARRAYLEN; i++) {
		printf("%llu ", array[i]);
	}
	printf("\n");

	uint64_t code = mymergesort(array, array2, ARRAYLEN);

	printf("Returned\n");

	// ensure monotony
	uint64_t max = array2[0];
	for(size_t i = 0; i < ARRAYLEN; ++i) {
		printf("%llu ", array2[i]);
		if(max <= array2[i]) {
			max = array2[i];
		} else {
			return code != 0 ? code : 255;
		}
	}
	printf("\n");

}