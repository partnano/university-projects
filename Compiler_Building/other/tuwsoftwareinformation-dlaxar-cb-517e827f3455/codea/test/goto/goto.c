#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

uint64_t go();

int main() {
	uint64_t val = go();

	printf("go() = %llu == 255\n", val);
	if(val != 255) {
		return 1;
	}

	return 0;
}