#ifndef DLAXAR_CB_TREE_H
#define DLAXAR_CB_TREE_H

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "terminals.h"
#include "memory.h"

#ifndef _NODEPTR_TYPE_DEF
#define _NODEPTR_TYPE_DEF
typedef struct tree_node* NODEPTR_TYPE;
#endif

#ifndef _BURG_C
#ifndef _STATEPTR_TYPE_DEF
#define _STATEPTR_TYPE_DEF
	typedef struct burm_state *STATEPTR_TYPE;
#endif
#endif

struct tree_node {
	TERM_OP op;
	STATEPTR_TYPE state;
	NODEPTR_TYPE left;
	NODEPTR_TYPE right;
	uint64_t value;
	uint64_t parent;
	MEMORY scope;
	char const* name;
	int negations;
	ACCESS reg;
};

#define STATE_LABEL(p) ((p)->state)
#define OP_LABEL(p) ((p)->op)
#define LEFT_CHILD(p) ((p)->left)
#define RIGHT_CHILD(p) ((p)->right)
#define PANIC printf

NODEPTR_TYPE tree_unsupported();
NODEPTR_TYPE tree_leaf(TERM_OP c);
NODEPTR_TYPE tree_literal(uint64_t value);
NODEPTR_TYPE tree_variable(MEMORY reg, char const* name);
NODEPTR_TYPE tree_node_single(TERM_OP c, NODEPTR_TYPE child);
NODEPTR_TYPE tree_subtree(TERM_OP c, NODEPTR_TYPE left, NODEPTR_TYPE right);
NODEPTR_TYPE tree_assign_define(MEMORY scope, char const* name, NODEPTR_TYPE left);
NODEPTR_TYPE tree_assign(MEMORY scope, NODEPTR_TYPE lhs, NODEPTR_TYPE rhs);
NODEPTR_TYPE tree_goto(char const *name);
NODEPTR_TYPE tree_if(NODEPTR_TYPE left, char const* then);
NODEPTR_TYPE tree_node_not(NODEPTR_TYPE cterm);
NODEPTR_TYPE tree_call(char const *name, NODEPTR_TYPE args);
NODEPTR_TYPE tree_arglist(NODEPTR_TYPE n, NODEPTR_TYPE list);

#endif //DLAXAR_CB_TREE_H
