#ifndef DLAXAR_CB_MEMORY_H
#define DLAXAR_CB_MEMORY_H

#include <stdbool.h>

typedef struct mem *MEMORY;
typedef struct memory_access* ACCESS;
typedef struct stack stack;

#define REG_COUNT (25)

typedef enum {
	rdi=0,
	rsi=1,
	rdx=2,
	rcx=3,
	r8=4,
	r9=5,
	rax=6,
	r10=7,
	r11=8,
	xmm0,
	xmm1,
	xmm2,
	xmm3,
	xmm4,
	xmm5,
	xmm6,
	xmm7,
	xmm8,
	xmm9,
	xmm10,
	xmm11,
	xmm12,
	xmm13,
	xmm14,
	xmm15,
	__none = -1
} memory_register;

typedef enum {
	REGISTER,
	STACK
} mem_access_type;

typedef enum {
	NONE,
	DISABLE_WRITE
} protect;

struct access {
	memory_register reg;
	stack* stack_position;
};

struct memory_access {
	mem_access_type type;
	protect protect;
	struct access access;
};

struct stack {
	char const* name;
	ACCESS access;
	stack* next;
	stack* prev;
	bool committed;
};

struct mem {
	char const* name;
	bool availability[REG_COUNT];
	int parameters;
	int arguments;
	char const* variables[REG_COUNT];
	bool stackInitialized;
	stack* sp;
	ACCESS registers[REG_COUNT];
	bool returnAddressOnStack;
	bool stackFramed;
	int savePoint;
	int additionalStackSpace;
	bool lastStmtWasReturn;
};

MEMORY create_scope(MEMORY parent, char const* name);

void mem_allocate(MEMORY scope, char const* name);

void mem_allocate_parameters(MEMORY scope, char const *name);

ACCESS mem_allocate_arguments(MEMORY scope, int argc);

ACCESS mem_allocate_tmp(MEMORY scope);

char const* reg_to_str(memory_register reg);

char const* access_to_str(ACCESS a);

ACCESS mem_access(MEMORY scope, char const *name);

void mem_free_tmp(MEMORY scope);

void mem_save_to_stack(MEMORY scope);

void mem_restore_savepoint(MEMORY scope);

void mem_push_call(MEMORY scope);

void is_return(MEMORY scope, bool was);

#endif //DLAXAR_CB_MEMORY_H
