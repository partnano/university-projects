#ifndef DLAXAR_CB_BURG_H
#define DLAXAR_CB_BURG_H

#include "memory.h"

#ifndef _BURG_C
#ifndef _NODEPTR_TYPE_DEF
#define _NODEPTR_TYPE_DEF
typedef struct tree_node* NODEPTR_TYPE;
#endif
#endif

#ifndef _BURG_C
#ifndef _STATEPTR_TYPE_DEF
#define _STATEPTR_TYPE_DEF
	typedef struct burm_state* STATEPTR_TYPE;
#endif
#endif

STATEPTR_TYPE burm_label(NODEPTR_TYPE p);

MEMORY scope;

#endif //DLAXAR_CB_BURG_H
