#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include "memory.h"
#include "codegen.h"

static
char const* reglookup[] = {
		"%rdi",
		"%rsi",
		"%rdx",
		"%rcx",
		"%r8",
		"%r9",
		"%rax",
		"%r10",
		"%r11",
		"%xmm0",
		"%xmm1",
		"%xmm2",
		"%xmm3",
		"%xmm4",
		"%xmm5",
		"%xmm6",
		"%xmm7",
		"%xmm8",
		"%xmm9",
		"%xmm10",
		"%xmm11",
		"%xmm12",
		"%xmm13",
		"%xmm14",
		"%xmm15"
};

static
stack* empty_stack_node() {
	stack* newSp = malloc(sizeof(stack));
	newSp->name = NULL;
	newSp->prev = newSp->next = NULL;
	newSp->committed = true;

	return newSp;
}

static
void stack_push(MEMORY scope, stack* newSp) {
	newSp->next = scope->sp;
	if(scope->sp != NULL) {
		scope->sp->prev = newSp;
	}
	scope->sp = newSp;
	newSp->prev = NULL;

}

static
void stack_append(MEMORY scope, stack* newSp) {
	stack* sp = scope->sp;

	if(sp == NULL) {
		scope->sp = newSp;
		return;
	}

	while(sp->next != NULL) { sp = sp->next; }

	sp->next = newSp;
	newSp->prev = sp;
}

static
void stack_debug(MEMORY scope) {
	stack* sp = scope->sp;

	printf("# Stack content: ");
	while(sp != NULL) {
		printf("%s > ", sp->name);
		sp = sp->next;
	}
	printf("(bottom).\n");
}

static void stack_remove(MEMORY scope) {
	stack* newSp = scope->sp->next;
	scope->sp->prev = NULL;
	free(scope->sp);
	scope->sp = newSp;
}

static
void stack_pop(MEMORY scope, memory_register into) {
	stack_debug(scope);
	scope->registers[into] = scope->sp->access;

	if(scope->sp->name != NULL) {
		scope->variables[into] = scope->sp->name;
	}

	scope->availability[into] = 0;

	// this magicly (and badly) keeps the relationship of node to access (since node has a pointer to
	// where the access stood before pushing it onto the stack). This ensures, that the reg.type will
	// be set to REGISTER after popping
	if(scope->registers[into] == NULL) {
		scope->registers[into] = malloc(sizeof(struct memory_access));
	}

	scope->registers[into]->type = REGISTER;
	scope->registers[into]->access.reg = into;

	stack_remove(scope);
}

static stack* stack_head(MEMORY scope) {
	return scope->sp;
}

#define START_TMP_REG (0)
#define TMP_COUNT (9)

MEMORY create_scope(MEMORY parent, char const* name) {
	MEMORY m = malloc(sizeof(struct mem));
	m->name = name;
	m->parameters = m->arguments = 0;
	m->stackInitialized = m->stackFramed = m->returnAddressOnStack = false;
	m->sp = NULL;
	m->additionalStackSpace = 0;
	m->lastStmtWasReturn = false;
	for(size_t i = 0; i < REG_COUNT; ++i) {
		m->variables[i] = 0;
		m->availability[i] = 1;
		m->registers[i] = NULL;
	}
	return m;
}

void mem_allocate(MEMORY scope, const char* name) {
	ACCESS a = mem_allocate_tmp(scope);
	printf("# allocation used for variable %s\n", name);
	memory_register r = a->access.reg;
	a->protect = DISABLE_WRITE;

	scope->variables[r] = name;
	return;
}

void mem_init_stack(MEMORY scope) {
	scope->stackInitialized = true;
}

ACCESS mem_allocate_stack(MEMORY scope, char const* name) {
	if(!scope->stackInitialized) {
		mem_init_stack(scope);
	}

	printf("# allocating stack for %s\n", name);

	stack* newSp = empty_stack_node();
	newSp->name = name;
	newSp->access = malloc(sizeof(struct memory_access));
	newSp->access->type = STACK;
	newSp->access->protect = DISABLE_WRITE;
	newSp->access->access.stack_position = newSp;

	stack_push(scope, newSp);

	return newSp->access;
}

ACCESS mem_allocate_queue(MEMORY scope, char const* name) {
	if(!scope->stackInitialized) {
		mem_init_stack(scope);
	}

	printf("# allocating stack for %s (at the end)\n", name);

	stack* newSp = empty_stack_node();
	newSp->name = name;
	newSp->access = malloc(sizeof(struct memory_access));
	newSp->access->type = STACK;
	newSp->access->protect = DISABLE_WRITE;
	newSp->access->access.stack_position = newSp;

	stack_append(scope, newSp);

	return newSp->access;
}

void mem_allocate_parameters(MEMORY scope, char const *name) {
	scope->parameters++;

	if(scope->parameters <= 6) {
		mem_allocate(scope, name);
	} else {
		mem_allocate_queue(scope, name);
	}
}

ACCESS mem_allocate_arguments(MEMORY scope, int argc) {
	stack_debug(scope);
	if(argc >= 6) {
		stack *newSp = empty_stack_node();
		newSp->access = malloc(sizeof(struct memory_access));
		newSp->access->type = STACK;
		newSp->access->protect = DISABLE_WRITE;
		newSp->access->access.stack_position = newSp;
		newSp->committed = false;

		stack_push(scope, newSp);

		return newSp->access;
	} else {
		if(scope->availability[argc] != 1) {
			fprintf(stderr, "Expected register %d to be empty but was unavailable when allocating arguments\n", argc);
			exit(1);
		}

		ACCESS out = malloc(sizeof(struct memory_access));
		out->type = REGISTER;
		out->protect = DISABLE_WRITE;
		out->access.reg = (memory_register) argc;
		return scope->registers[argc] = out;
	}
}

/**
 * This will never work. The memory management now simulates how the code will
 * allocate it's memory and uses this simulation to select registers. However,
 * due to variables in the code in combination with branching, it cannot be
 * said with absolute certainty how the code will execute. Consider the following
 * example:
 *
 * <code>
 * if (cond) goto A;
 *
 * # at this point here, the memory manager decides to move the variable 'b'
 * # to the stack in order to have more registers available
 * pushq 'b'
 *
 * addq 0(%rsp), 0(%rsp) # accesses and modifies the variable 'b' on stack
 *
 * A: # label
 * # due to the static and linear top-to-bottom analysis of the memory manager
 * # this code here would expect the variable 'b' to sit on top of the stack.
 * # However, on the real machine, if (cond) is true, 'b' is never pushed
 * # onto the stack but resides in it's original register
 *
 * addq 0(%rsp), 0(%rsp) # modifies something (may or may not be 'b')
 * </code>
 *
 * @param scope
 */
// todo try to be a bit more intelligent about what to move out
static
void mem_move_out(MEMORY scope) {
	mem_init_stack(scope);
	printf("# moving out %%rax\n");
	stack* newSp = empty_stack_node();
	newSp->name = scope->variables[rax];

	newSp->access = scope->registers[rax];
	newSp->access->access.stack_position = newSp;
	newSp->access->type = STACK;

	stack_push(scope, newSp);
	scope->savePoint++;

	generate_stack_push(scope, rax);
	scope->availability[rax] = 1;
	scope->variables[rax] = NULL;
	scope->additionalStackSpace++;
}

ACCESS mem_allocate_tmp(MEMORY scope) {
	for(size_t i = START_TMP_REG; i < TMP_COUNT; ++i) {
		if(scope->availability[i] == 1) {
			scope->availability[i] = 0;

			ACCESS out = malloc(sizeof(struct memory_access));
			out->type = REGISTER;
			out->protect = NONE;
			out->access.reg = (memory_register) i;

			printf("# allocated register (tmp) %s\n", reg_to_str((memory_register) i));
			return scope->registers[i] = out;
		}
	}

	// see note above mem_move_out for details
	fprintf(stderr, "Cannot allocate tmp register: none available\n");
	exit(40);

//	mem_move_out(scope);
//	return mem_allocate_tmp(scope);
}

char const* reg_to_str(memory_register reg) {
	return reglookup[reg];
}

char const* access_to_str(ACCESS a) {
	if(a->type == REGISTER) {
		return reg_to_str(a->access.reg);
	} else {
		// todo memory leak
		// todo allocate correct size
		char* str = malloc(sizeof(char)*20);

		size_t depth = 0;
		stack* sp = a->access.stack_position;
		while((sp = sp->prev) != NULL) {
			if(sp->committed) {
				depth++;
			}
		}

		sprintf(str, "%d(%%rsp)", 8*(depth));
		return str;
	}
}

ACCESS mem_access(MEMORY scope, char const *name) {
	for(size_t i = 0; i < REG_COUNT; ++i) {
		if(scope->availability[i] == 0 && scope->variables[i] != NULL && strcmp(scope->variables[i], name) == 0) {
			return scope->registers[i];
		}
	}

	if(scope->stackInitialized) {
		stack* sp = scope->sp;
		do {
			if(sp->name != NULL && strcmp(sp->name, name) == 0) {
				return sp->access;
			}
		} while ((sp = sp->next) != NULL);
	}

	fprintf(stderr, "var not found: %s\n", name);
	exit(11);
}

void mem_free_tmp(MEMORY scope) {
	for(size_t i = 0; i < REG_COUNT; ++i) {
		if(scope->availability[i] == 0 && scope->variables[i] == 0) {
			scope->availability[i] = 1;
		}
	}
}

void mem_save_to_stack(MEMORY scope) {
	scope->savePoint = 0;
	for(size_t i = 0; i < REG_COUNT; ++i) {
		if(scope->availability[i] == 0) {

			if(!scope->stackInitialized) {
				mem_init_stack(scope);
			}

			stack* newSp = empty_stack_node();
			if(scope->variables[i] != NULL) {
				newSp->name = scope->variables[i];
			} else {
				newSp->name = reg_to_str((memory_register) i);
			}
			newSp->access = scope->registers[i];
			newSp->access->access.stack_position = newSp;
			newSp->access->type = STACK;

			stack_push(scope, newSp);
			scope->savePoint++;

			generate_stack_push(scope, (memory_register) i);
			scope->availability[i] = 1;
			scope->variables[i] = NULL;
		}
	}

	if(scope->stackFramed && scope->savePoint % 2 != 0) {
		printf("# aligning stack pointer\n");
		generate_align_stackpointer();

		scope->savePoint++;
	}
}

void mem_restore_savepoint(MEMORY scope) {
	printf("# Restoring %d registers (with %d arguments)\n", scope->savePoint, scope->arguments);
	stack_debug(scope);

	// remove overflow args from actual stack
	if(scope->arguments > 6) {
		generate_remove_from_stack((size_t) (8 * (scope->arguments - 6)));
	}

	// remove overflow from memory management stack
	for(int i = scope->savePoint; i < scope->arguments; ++i) {
		stack_remove(scope);
	}

	while(scope->savePoint > 0) {
		memory_register into = stack_head(scope)->access->access.reg;

		stack_pop(scope, into);

		generate_stack_pop(scope, into);
		--scope->savePoint;
	}

	stack_debug(scope);
}

void mem_push_call(MEMORY scope) {
	if(scope->returnAddressOnStack) return;

	printf("# pushing return address onto stack\n");
	scope->returnAddressOnStack = true;

	stack* newSp = empty_stack_node();
	newSp->name = "return_address";
	newSp->access = NULL;
	stack_push(scope, newSp);

	stack_debug(scope);

	if(scope->stackInitialized && !scope->stackFramed) {
		// todo: move codegen somehwere else
		printf("enter $0, $0 # pushed stack pointer\n");

		newSp = empty_stack_node();
		newSp->name = "base_pointer";

		stack_push(scope, newSp);
		stack_debug(scope);

		scope->stackFramed = true;
	}
}

void is_return(MEMORY scope, bool was) {
	scope->lastStmtWasReturn = was;
}
