.globl _jumpOverReturn
_jumpOverReturn:
# pushing return address onto stack
# Stack content: return_address > (bottom).
jmp L_jumpOverReturn_toReturn
movq $1, %rax
retq
L_jumpOverReturn_toReturn:
movq $2, %rax
retq
.globl _jumpOverAndBack
_jumpOverAndBack:
# pushing return address onto stack
# Stack content: return_address > (bottom).
jmp L_jumpOverAndBack_return2
L_jumpOverAndBack_return1:
movq $1, %rax
retq
L_jumpOverAndBack_return2:
jmp L_jumpOverAndBack_return1
movq $2, %rax
retq
.globl _loopMul
_loopMul:
# allocated register (tmp) %rdi
# allocation used for variable x
# allocated register (tmp) %rsi
# allocation used for variable n
# pushing return address onto stack
# Stack content: return_address > (bottom).
# allocated register (tmp) %rdx
# allocation used for variable res
movq $0, %rdx
L_loopMul_loopstart:
# dst is write protected or immediate
# allocated register (tmp) %rcx
movq %rdi, %rcx
addq %rdx, %rcx
movq %rcx, %rdx
# dst is write protected or immediate
# allocated register (tmp) %rcx
movq $-1, %rcx
addq %rsi, %rcx
movq %rcx, %rsi

# allocated register (tmp) %rcx
movq %rsi, %rcx
cmpq $0, %rcx
jle L_if_0_no

L_if_0_yes:
jmp L_loopMul_loopstart
L_if_0_no:
movq %rdx, %rax
retq
.globl _divide
_divide:
# allocated register (tmp) %rdi
# allocation used for variable x
# allocated register (tmp) %rsi
# allocation used for variable y
# pushing return address onto stack
# Stack content: return_address > (bottom).
# allocated register (tmp) %rdx
# allocation used for variable res
movq $0, %rdx
L_divide_loopstart2:
# dst is write protected or immediate
# allocated register (tmp) %rcx
movq $1, %rcx
addq %rdx, %rcx
movq %rcx, %rdx
# allocated register (tmp) %rcx
movq %rsi, %rcx
negq %rcx
# dst is tmp register
addq %rdi, %rcx
movq %rcx, %rdi
# dst is write protected or immediate
# allocated register (tmp) %rcx
movq $-1, %rcx
addq %rsi, %rcx

# allocated register (tmp) %r8
movq %rdi, %r8
cmpq %rcx, %r8
jle L_if_1_no

L_if_1_yes:
jmp L_divide_loopstart2
L_if_1_no:
movq %rdx, %rax
retq
.globl _eq
_eq:
# allocated register (tmp) %rdi
# allocation used for variable x
# allocated register (tmp) %rsi
# allocation used for variable y
# pushing return address onto stack
# Stack content: return_address > (bottom).

# allocated register (tmp) %rdx
movq %rsi, %rdx
cmpq %rdi, %rdx
je L_if_2_no

L_if_2_yes:
jmp L_eq_else
L_if_2_no:
movq $1, %rax
retq
L_eq_else:
movq $0, %rax
retq
.globl _checkIfZGtXAndXEqY
_checkIfZGtXAndXEqY:
# allocated register (tmp) %rdi
# allocation used for variable x
# allocated register (tmp) %rsi
# allocation used for variable y
# allocated register (tmp) %rdx
# allocation used for variable z
# pushing return address onto stack
# Stack content: return_address > (bottom).

# allocated register (tmp) %rcx
movq %rdx, %rcx
cmpq %rdi, %rcx
jle L_if_5_no


# allocated register (tmp) %rcx
movq %rsi, %rcx
cmpq %rdi, %rcx
je L_if_not_3_no

# 0
L_if_not_3_yes: 	jmp L_if_5_no
L_if_not_3_no: 
# allocated register (tmp) %rcx
movq %rdx, %rcx
cmpq %rdi, %rcx
jle L_if_5_no


# allocated register (tmp) %rcx
movq %rsi, %rcx
cmpq %rdi, %rcx
je L_if_not_4_no

# 0
L_if_not_4_yes: 	jmp L_if_5_no
L_if_not_4_no: L_if_5_yes:
jmp L_checkIfZGtXAndXEqY_else2
L_if_5_no:
movq $0, %rax
retq
L_checkIfZGtXAndXEqY_else2:
movq $1, %rax
retq
.globl _checkIfXEqYAndZGtX
_checkIfXEqYAndZGtX:
# allocated register (tmp) %rdi
# allocation used for variable x
# allocated register (tmp) %rsi
# allocation used for variable y
# allocated register (tmp) %rdx
# allocation used for variable z
# pushing return address onto stack
# Stack content: return_address > (bottom).

# allocated register (tmp) %rcx
movq %rsi, %rcx
cmpq %rdi, %rcx
je L_if_not_6_no

# 0
L_if_not_6_yes: 	jmp L_if_7_no
L_if_not_6_no: 
# allocated register (tmp) %rcx
movq %rdx, %rcx
cmpq %rdi, %rcx
jle L_if_7_no

L_if_7_yes:
jmp L_checkIfXEqYAndZGtX_else3
L_if_7_no:
movq $0, %rax
retq
L_checkIfXEqYAndZGtX_else3:
movq $1, %rax
retq
.globl _checkIfXEqYAndXEqY
_checkIfXEqYAndXEqY:
# allocated register (tmp) %rdi
# allocation used for variable x
# allocated register (tmp) %rsi
# allocation used for variable y
# pushing return address onto stack
# Stack content: return_address > (bottom).

# allocated register (tmp) %rdx
movq %rsi, %rdx
cmpq %rdi, %rdx
je L_if_not_8_no

# 0
L_if_not_8_yes: 	jmp L_if_10_no
L_if_not_8_no: 
# allocated register (tmp) %rdx
movq %rsi, %rdx
cmpq %rdi, %rdx
je L_if_not_9_no

# 0
L_if_not_9_yes: 	jmp L_if_10_no
L_if_not_9_no: L_if_10_yes:
jmp L_checkIfXEqYAndXEqY_else4
L_if_10_no:
movq $0, %rax
retq
L_checkIfXEqYAndXEqY_else4:
movq $1, %rax
retq
