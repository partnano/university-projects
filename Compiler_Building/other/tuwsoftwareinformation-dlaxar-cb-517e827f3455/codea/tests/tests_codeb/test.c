
#include <assert.h>
#include <stdio.h>
#include <inttypes.h>

extern int64_t useStaticVar();
extern int64_t useDynamicVar();
extern int64_t paramToVar(int64_t);

extern int64_t varAssign42();
extern int64_t parameterAssign7(int64_t);
extern int64_t* arrayAssign11on0(int64_t);

extern int64_t jumpOverReturn();
extern int64_t jumpOverAndBack();
extern int64_t loopMul(int64_t, int64_t);
extern int64_t divide(int64_t, int64_t);
extern int64_t eq(int64_t, int64_t);
extern int64_t checkIfZGtXAndXEqY(int64_t,int64_t, int64_t);
extern int64_t checkIfXEqYAndZGtX(int64_t,int64_t, int64_t);
extern int64_t checkIfXEqYAndXEqY(int64_t,int64_t);


void test(char const* function, int64_t expected, int64_t result) {
	if(expected == result) {
		printf("\e[32m");
	} else {
		printf("\e[31m");
	}
	printf("Function: %s\n", function);
	printf("\e[0m");

	printf("Expected: %lld\nResult:  %lld\n", expected, result);
	assert(expected == result);
}

int main() {
	printf("Tests:\n\n");

	// variable tests
	test("useStaticVar", 12, useStaticVar());
	test("useDynamicVar", 42, useDynamicVar(2,5));
	test("paramToVar", 25, paramToVar(5));

	// variable assign tests
	int64_t a[] = {1,2,3,5,8,13};
	test("varAssign42", 42, varAssign42());
	test("parameterAssign7", 7, parameterAssign7(5));

	arrayAssign11on0((uint64_t) a);
	test("arrayAssign11on0", 11, a[0]);

	// goto tests
	test("jumpOverReturn", 2, jumpOverReturn());
	test("jumpOverAndBack", 1, jumpOverAndBack(2,5));

	// goto tests
	test("loopMul", 72, loopMul(12, 6));
	test("divide", 2, divide(12, 6));
	test("divide", 2, divide(13, 6));
	test("eq", 1, eq(13, 13));
	test("eq", 0, eq(13, 14));
	test("checkIfZGtXAndXEqY", 0, checkIfZGtXAndXEqY(1, 3, 3));
	test("checkIfZGtXAndXEqY", 1, checkIfZGtXAndXEqY(3, 3, 5));
	test("checkIfZGtXAndXEqY", 0, checkIfZGtXAndXEqY(3, 3, 0));
	test("checkIfXEqYAndZGtX", 0, checkIfXEqYAndZGtX(1, 3, 3));
	test("checkIfXEqYAndZGtX", 1, checkIfXEqYAndZGtX(3, 3, 5));
	test("checkIfXEqYAndZGtX", 0, checkIfXEqYAndZGtX(3, 3, 0));

	test("checkIfXEqYAndXEqY", 1, checkIfXEqYAndXEqY(3, 3));
	test("checkIfXEqYAndXEqY", 0, checkIfXEqYAndXEqY(2, 3));


}
