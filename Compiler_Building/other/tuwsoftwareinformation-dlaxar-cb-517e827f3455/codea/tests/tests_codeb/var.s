.globl _useStaticVar
_useStaticVar:
# pushing return address onto stack
# Stack content: return_address > (bottom).
# allocated register (tmp) %rdi
# allocation used for variable x
movq $2, %rdi
# allocated register (tmp) %rsi
# allocation used for variable y
movq $6, %rsi
# dst is write protected or immediate
# allocated register (tmp) %rdx
movq %rsi, %rdx
imul %rdi, %rdx
movq %rdx, %rax
retq
.globl _useDynamicVar
_useDynamicVar:
# pushing return address onto stack
# Stack content: return_address > (bottom).
# allocated register (tmp) %rdi
# allocation used for variable x
movq $41, %rdi
# dst is write protected or immediate
# allocated register (tmp) %rsi
movq $1, %rsi
addq %rdi, %rsi
# allocated register (tmp) %rdx
# allocation used for variable y
movq %rsi, %rdx
movq %rdx, %rax
retq
.globl _paramToVar
_paramToVar:
# allocated register (tmp) %rdi
# allocation used for variable x
# pushing return address onto stack
# Stack content: return_address > (bottom).
# allocated register (tmp) %rsi
# allocation used for variable y
movq %rdi, %rsi
# dst is write protected or immediate
# allocated register (tmp) %rdx
movq %rsi, %rdx
imul %rsi, %rdx
movq %rdx, %rax
retq
.globl _varAssign42
_varAssign42:
# pushing return address onto stack
# Stack content: return_address > (bottom).
# allocated register (tmp) %rdi
# allocation used for variable x
movq $0, %rdi
movq $42, %rdi
movq %rdi, %rax
retq
.globl _parameterAssign7
_parameterAssign7:
# allocated register (tmp) %rdi
# allocation used for variable x
# pushing return address onto stack
# Stack content: return_address > (bottom).
movq $7, %rdi
movq %rdi, %rax
retq
.globl _arrayAssign11on0
_arrayAssign11on0:
# allocated register (tmp) %rdi
# allocation used for variable x
# pushing return address onto stack
# Stack content: return_address > (bottom).
# allocated register (tmp) %rsi
movq $0, %rsi
leaq (, %rsi, 8), %rsi
# dst is tmp register
addq %rdi, %rsi
movq $11, (%rsi)
retq
