
#include <assert.h>
#include <stdio.h>
#include <inttypes.h>

extern int64_t void1();
extern int64_t randomNumber();
extern int64_t add42(int64_t);
extern int64_t add2(int64_t, int64_t);
extern int64_t add4(int64_t, int64_t, int64_t, int64_t);
extern int64_t add2SubTrees(int64_t, int64_t, int64_t, int64_t);

extern int64_t mul2(int64_t, int64_t);
extern int64_t mul4(int64_t, int64_t, int64_t, int64_t);
extern int64_t mixAddMul(int64_t, int64_t, int64_t, int64_t);

extern int64_t singleConstOneMinus();
extern int64_t doubleConstOneMinus();
extern int64_t tribleConstOneMinus();
extern int64_t singleMinus(int64_t);
extern int64_t doubleMinus(int64_t);
extern int64_t tribleMinus(int64_t);
extern int64_t minusAplusB(int64_t, int64_t);
extern int64_t plusAminusB(int64_t, int64_t);

extern int64_t arrayAtZero(int64_t);
extern int64_t arrayAtB(int64_t, int64_t);
extern int64_t arrayAtArrayAtC(int64_t, int64_t, int64_t);

extern int64_t APlusB(int64_t, int64_t, int64_t);
extern int64_t APlusBPlus42(int64_t, int64_t, int64_t);
extern int64_t AMinusB(int64_t, int64_t, int64_t);

extern int64_t longAddExpr(int64_t, int64_t, int64_t);
extern int64_t longAddChainExpr(int64_t, int64_t);
extern int64_t longMulChainExpr(int64_t, int64_t);
extern int64_t longMixedExpr(int64_t);

void test(char const* function, int64_t expected, int64_t result) {
	if(expected == result) {
		printf("\e[32m");
	} else {
		printf("\e[31m");
	}
	printf("Function: %s\n", function);
	printf("\e[0m");

	printf("Expected: %lld\nResult:  %lld\n", expected, result);
	assert(expected == result);
}

int main() {
	printf("Tests:\n\n");

	// add tests
	test("add42", 49, add42(7));
	test("add2", 7, add2(2,5));
	test("add4", 17, add4(2,5,9,1));
	test("add2SubTrees", 14, add2SubTrees(2,5,2,5));
	test("randomNumber", 4, randomNumber());
	test("void1", 0, void1());

	// mul tests
	test("mul2", 36, mul2(12,3));
	test("mul2", 36, mul2(3,12));
	test("mul2", -36, mul2(-3,12));
	test("mul4", 36, mul4(2,2,3,3));
	test("mul4", 36, mul4(-2,2,-3,3));
	test("mul4", 0, mul4(-2,2,0,3));

	test("mixAddMul", 90, mul4(2,5,3,3));

	// minus rep tests
	test("singleConstOneMinus", -1, singleConstOneMinus());
	test("doubleConstOneMinus", 1, doubleConstOneMinus());
	test("tribleConstOneMinus", -1, tribleConstOneMinus());
	test("singleMinus", -9, singleMinus(9));
	test("doubleMinus", 14, doubleMinus(14));
	test("doubleMinus", -14, doubleMinus(-14));
	test("tribleMinus", -5, tribleMinus(5));
	test("tribleMinus", 5, tribleMinus(-5));
	test("minusAplusB", 1, minusAplusB(2,3));
	test("minusAplusB", -1, minusAplusB(3,2));
	test("plusAminusB", -1, plusAminusB(2,3));
	test("plusAminusB", 1, plusAminusB(3,2));

	// array access
	int64_t a[] = {1,2,3,5,8,13};
	int64_t b[] = {3,5,7,11,19,23};

	test("arrayAtZero", 1, arrayAtZero((uint64_t) a));
	test("arrayAtZero", 3, arrayAtZero((uint64_t) b));
	test("arrayAtB", 3, arrayAtB((uint64_t) a, 2));
	test("arrayAtB", 8, arrayAtB((uint64_t) a, 4));
	test("arrayAtB", 23, arrayAtB((uint64_t) b, 5));
	test("arrayAtArrayAtC", 13, arrayAtArrayAtC((uint64_t) a, (uint64_t) b, 1));

	// not all parameters in use
	test("APlusB", 7, APlusB(2, 5, 9));
	test("APlusB", 2, APlusB(2, 0, 9));
	test("APlusBPlus42", 52, APlusBPlus42(2, 8, 9));
	test("APlusBPlus42", 40, APlusBPlus42(-2, 0, 9));
	test("AMinusB", 4, AMinusB(6, 2, 1));
	test("AMinusB", 6, AMinusB(9, 3, 1));

	test("longAddExpr", 38, longAddExpr(2,3,4));
	test("longAddExpr", 38, longAddExpr(2,3,4));
	test("longAddChainExpr", 35, longAddChainExpr(7,0));
	test("longMulChainExpr", 49392, longMulChainExpr(7,1));
	test("longMixedExpr", 3300, longMixedExpr(11));
}
