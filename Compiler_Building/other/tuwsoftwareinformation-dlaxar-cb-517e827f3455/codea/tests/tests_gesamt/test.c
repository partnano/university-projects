
#include <assert.h>
#include <stdio.h>
#include <inttypes.h>

extern int64_t return10fromAnotherFunction();

extern int64_t proxyReturnParam(int64_t);
extern int64_t proxyReturnParamProxyReturnParam(int64_t);
extern int64_t proxyReturnParamPlus10(int64_t);
extern int64_t proxyReturnPlus11(int64_t);

extern int64_t reverseTwoParameter(int64_t, int64_t);

// recursive functions
extern int64_t recursiveDistanceTo10(int64_t);
extern int64_t recursiveDivide(int64_t, int64_t);
extern int64_t recursiveStrLen(int64_t);

extern int64_t proxyAPlusProxyB(int64_t, int64_t);
extern int64_t aDivCPlusBDivC(int64_t, int64_t, int64_t);
extern int64_t AOrBMoreDividableByC(int64_t, int64_t, int64_t);

extern int64_t Ackermannfunktion(int64_t, int64_t);
extern int64_t AckermannfunktionR(int64_t, int64_t);

extern int64_t callCallTest(int64_t);

void test(char const* function, int64_t expected, int64_t result) {
	if(expected == result) {
		printf("\e[32m");
	} else {
		printf("\e[31m");
	}
	printf("Function: %s\n", function);
	printf("\e[0m");

	printf("Expected: %lld\nResult:   %lld\n", expected, result);
	assert(expected == result);
}

int main() {
	printf("Tests:\n\n");

	// basic tests
	test("return10fromAnotherFunction", 10, return10fromAnotherFunction());

	test("proxyReturnParam", 7, proxyReturnParam(7));
	test("proxyReturnParamProxyReturnParam", 27, proxyReturnParamProxyReturnParam(27));
	test("proxyReturnParamPlus10", 17, proxyReturnParamPlus10(7));
	test("proxyReturnPlus11", 18, proxyReturnPlus11(7));

	test("reverseTwoParameter", 3, reverseTwoParameter(3,6));

	test("recursiveDistanceTo10", 2, recursiveDistanceTo10(8));
	test("recursiveDivide", 7, recursiveDivide(49,7));

	int64_t str[] = {'H','e','l','l','o',' ','W','o','r','d','!',0};
	test("recursiveStrLen", 11, recursiveStrLen((int64_t) str));

	test("proxyAPlusProxyB", 18, proxyAPlusProxyB(7,11));

	test("aDivCPlusBDivC", 10, aDivCPlusBDivC(21,49,7));
	test("aDivCPlusBDivC", 5, aDivCPlusBDivC(12,3,3));

	test("AOrBMoreDividableByC", 1, AOrBMoreDividableByC(12,3,3));
	test("AOrBMoreDividableByC", 2, AOrBMoreDividableByC(21,49,7));

	// Ackermann function
	test("Ackermannfunktion", 4, Ackermannfunktion(1,2));
	test("Ackermannfunktion", 61, Ackermannfunktion(3,3));

	test("AckermannfunktionR", 4, AckermannfunktionR(2,1));
	test("AckermannfunktionR", 61, AckermannfunktionR(3,3));

	// call c function
	test("callCallTest", 11, callCallTest(11));
	test("callCallTest", 21, callCallTest(21));
}

int64_t callTest(int64_t ret) {
	return ret;
}