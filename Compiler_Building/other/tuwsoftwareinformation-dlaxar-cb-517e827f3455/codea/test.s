	.section	__TEXT,__text,regular,pure_instructions
	.macosx_version_min 10, 11
	.globl	_main
	.align	4, 0x90
_main:                                  ## @main
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp0:
	.cfi_def_cfa_offset 16
Ltmp1:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp2:
	.cfi_def_cfa_register %rbp
	movl	$0, -4(%rbp)
	movl	$0, -8(%rbp)
	movl	$2, -12(%rbp)
	movl	$1, -16(%rbp)
	movl	-16(%rbp), %eax
	cmpl	-8(%rbp), %eax
	jle	LBB0_4
## BB#1:
	movl	-12(%rbp), %eax
	cmpl	-12(%rbp), %eax
	jle	LBB0_3
## BB#2:
	movl	-8(%rbp), %eax
	cmpl	-12(%rbp), %eax
	jne	LBB0_4
LBB0_3:
	jmp	LBB0_5
LBB0_4:
	movl	$1, -8(%rbp)
LBB0_5:
	movl	-8(%rbp), %eax
	popq	%rbp
	retq
	.cfi_endproc


.subsections_via_symbols
