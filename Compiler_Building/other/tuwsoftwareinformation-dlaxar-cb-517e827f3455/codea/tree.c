#include <assert.h>
#include "tree.h"
#include "memory.h"

static
bool is_arithmetic(TERM_OP op) {
	return op == TERM_ADD || op == TERM_MINUS || op == TERM_MULT;
}

NODEPTR_TYPE empty(void) {
	NODEPTR_TYPE node = malloc(sizeof(struct tree_node));
	node->op =TERM_UNSUPPORTED;
	node->state = NULL;
	node->left = node->right = NULL;
	node->value = 0;
	node->scope = NULL;
	node->negations = 0;
	node->reg = NULL;
	node->name = NULL;
	return node;
}

NODEPTR_TYPE tree_unsupported(void) {
	NODEPTR_TYPE node = empty();
	node->op = TERM_UNSUPPORTED;

	return node;
}

NODEPTR_TYPE tree_leaf(TERM_OP c) {
	NODEPTR_TYPE node = empty();
	node->op = c;

	return node;
}

NODEPTR_TYPE tree_literal(uint64_t value) {
	NODEPTR_TYPE node = empty();
	node->op = TERM_CONST;
	node->value = value;

	return node;
}

NODEPTR_TYPE tree_node_single(TERM_OP c, NODEPTR_TYPE child) {
	NODEPTR_TYPE node = empty();
	node->op = c;
	node->left = child;

	if(is_arithmetic(c) && node->left->op == TERM_CONST) {
		// todo handle overflow
		node->op = TERM_CONST;
		switch(c) {
			case TERM_MINUS:
				node->value = 0-node->left->value;
				break;
			default:
				assert(0);
		}
	}

	return node;
}

NODEPTR_TYPE tree_subtree(TERM_OP c, NODEPTR_TYPE left, NODEPTR_TYPE right) {
	NODEPTR_TYPE node = empty();
	node->op = c;
	node->left = left;
	node->right = right;

	if(is_arithmetic(c) && node->left->op == TERM_CONST && node->right->op == TERM_CONST) {
		// todo handle overflow
		node->op = TERM_CONST;
		switch(c) {
			case TERM_ADD:
				node->value = node->left->value + node->right->value;
				break;
			case TERM_MULT:
				node->value = node->left->value * node->right->value;
				break;
			default:
				assert(0);
		}
	}

	return node;
}

NODEPTR_TYPE tree_variable(MEMORY scope, char const* name) {
	NODEPTR_TYPE node = empty();
	node->op = TERM_VARIABLE;
	node->scope = scope;
	node->name = name;

	return node;
}

NODEPTR_TYPE tree_assign_define(MEMORY scope, char const *name, NODEPTR_TYPE left) {
	NODEPTR_TYPE node = empty();
	node->op = TERM_DEFINE;
	node->left = left;
	node->scope = scope;
	node->name = name;

	return node;
}

NODEPTR_TYPE tree_assign(MEMORY scope, NODEPTR_TYPE lhs, NODEPTR_TYPE rhs) {
	NODEPTR_TYPE node = empty();
	node->op = TERM_ASSIGN;
	node->left = lhs;
	node->right = rhs;
	node->scope = scope;

	return node;
}

NODEPTR_TYPE tree_goto(char const *name) {
	NODEPTR_TYPE node = empty();
	node->op = TERM_GOTO;
	node->name = name;

	return node;
}

// this counts upwards for each if in the code
static uint64_t if_c = 0;

static
void propagate_parent(NODEPTR_TYPE node, uint64_t c) {
	if(OP_LABEL(node) == TERM_IF || OP_LABEL(node) == TERM_AND || OP_LABEL(node) == TERM_GT
	   || OP_LABEL(node) == TERM_NEQUAL) {
		node->parent = c;

		if(node->left != NULL) {
			propagate_parent(node->left, c);
		}

		if(node->right != NULL) {
			propagate_parent(node->right, c);
		}
	}

	if(OP_LABEL(node) == TERM_NOT) {
		node->parent = c;
	}
}

NODEPTR_TYPE tree_if(NODEPTR_TYPE left, char const* then) {
	NODEPTR_TYPE node = empty();
	node->op = TERM_IF;
	node->left = left;
	node->right = tree_goto(then);
	node->value = if_c++;

	propagate_parent(node, node->value);

	return node;
}

static
void propagate_not(NODEPTR_TYPE node) {
	node->negations++;

	// todo only set on compare operations (would be nicer, makes no actual difference)
	if(node->left != NULL) {
		propagate_not(node->left);
	}
	if(node->right != NULL) {
		propagate_not(node->right);
	}
}

NODEPTR_TYPE tree_node_not(NODEPTR_TYPE cterm) {
	NODEPTR_TYPE node = empty();
	node->op = TERM_NOT;
	node->left = cterm;
	node->value = if_c++;

	propagate_not(node->left);
	propagate_parent(cterm, node->value);

	return node;
}

NODEPTR_TYPE process_args(NODEPTR_TYPE head) {
	size_t length = 0;
	NODEPTR_TYPE p = head;
	while(p->right != NULL) {
		++length;
		p = p->right;
	}

	if(length > 6) {
		p = head;
		length -= 6;
		size_t steps = length;
		NODEPTR_TYPE middle = head;
		NODEPTR_TYPE newHead = head;
		NODEPTR_TYPE current;

		// move pointer to start of the in-order-queue
		while(steps-- > 0) {
			if(steps == 0) {
				newHead = middle;
			}
			middle = middle->right;
		}

		current = newHead;

		while(--length > 0) {
			steps = length;
			p = head;
			while(steps-- > 1) {
				p = p->right;
			}

			current->right = p;
			current = current->right;
		}

		current->right = middle;
		return newHead;
	}

	return head;

}

NODEPTR_TYPE tree_call(char const *name, NODEPTR_TYPE args) {
	NODEPTR_TYPE node = empty();
	node->op = TERM_CALL;

	args = process_args(args);

	node->left = args;
	node->name = name;

	return node;
}

/**
 *
 * list: TERM_LAST (right)> last arg > snd last arg > ... > second arg > first arg > null
 * @param n
 * @param list
 * @return
 */
NODEPTR_TYPE tree_arglist(NODEPTR_TYPE n, NODEPTR_TYPE list) {
	NODEPTR_TYPE head = list;

	NODEPTR_TYPE tail = empty();
	tail->op = TERM_ARGS;
	tail->left = n;


	while(list->right != NULL && list->right->op != TERM_LAST) {
		list = list->right;
	}

	tail->right = list->right;
	list->right = tail;

	return head;
}