#!/bin/bash

GCCFLAGS=-std=c99

if [ "$#" -lt 2 ]
then
	echo "Usage: ./test.sh <generator> <testdir>"
	exit 1
fi

MAKEDIR=test-make
GEN_INTO=$MAKEDIR/ss17.s

GENERATOR=$1

failed=0

function executeTest() {
	ss17=$1

	c=`echo $ss17 | sed 's/ss17$/c/'`
	if [ ! -f $c ]
	then
		echo -e "[WARN] c file doesn't exist for case $ss17\n"
	else
		echo "Case: $ss17"
		echo "Generating code into $GEN_INTO"
		$GENERATOR $ss17 > $GEN_INTO

		rc=$?

		if [ $rc -ne 0 ]
		then
			failed=1
			echo -e "[FAIL] Code generation failed!\n"
			return
		fi

		echo "Generated code."

		echo "Building ss17... "
		gcc ${GCCFLAGS} -c -o $MAKEDIR/ss17.o $GEN_INTO
		echo "Built ss17."
		echo "Building C..."
		gcc ${GCCFLAGS} -c -o $MAKEDIR/c.o $c
		echo "Built C."
		gcc ${GCCFLAGS} -o $MAKEDIR/testbin $MAKEDIR/c.o $MAKEDIR/ss17.o

		$MAKEDIR/testbin
		rc=$?

		echo -e "Cleaning..."

		if [ $rc -ne 0 ]
		then
			bn=`basename $ss17`
			cp $GEN_INTO $GEN_INTO-$bn.s
		fi

		rm $MAKEDIR/testbin $GEN_INTO $MAKEDIR/ss17.o $MAKEDIR/c.o
		echo "Done."

		if [ $rc -eq 0 ]
		then
			echo "[PASS] $ss17"
		else
			failed=1
			echo "[FAIL] $ss17 (return code was $rc)"
		fi

		echo -e "\n"

	fi
}

mkdir -p $MAKEDIR

echo -e "\n############### Tests are starting now: ###############\n"

if [ -f $2 ]
then
	echo -e "### SINGLE FILE MODE ###\n"

	executeTest $2

else
	ss17files=`ls $2/*.ss17`


	for ss17 in $ss17files
	do
		executeTest $ss17
	done

	if [ $failed -eq 0 ]
	then
		echo "[PASS] All files passed :)"
	else
		echo "[FAIL] At least one test failed!"
	fi
fi