#ifndef DLAXAR_CB_CODEGEN_H
#define DLAXAR_CB_CODEGEN_H

#include "tree.h"

void generate_function_head(char const* name);

void generate_label(char const* prefix, char const* name);

void generate_return(NODEPTR_TYPE bnode, MEMORY scope);

void generate_mulq(NODEPTR_TYPE node, MEMORY scope);

void generate_addq(NODEPTR_TYPE node, MEMORY scope);

void generate_minus(NODEPTR_TYPE node, MEMORY scope);

void generate_compute_array_address(NODEPTR_TYPE node, MEMORY scope);

void generate_array(NODEPTR_TYPE node, MEMORY scope);

void generate_definition(NODEPTR_TYPE node, MEMORY scope);

void generate_assignment(NODEPTR_TYPE node, MEMORY scope);

void generate_not_equal(NODEPTR_TYPE node, MEMORY scope);

void generate_greater_than(NODEPTR_TYPE node, MEMORY scope);

void generate_jump_to_end_of_nested_condition(char const *jmp, NODEPTR_TYPE node, const char* yn);

void generate_not_labels(NODEPTR_TYPE node, MEMORY scope);

void generate_call(NODEPTR_TYPE node, MEMORY scope);

void generate_stack_push(MEMORY scope, memory_register reg);

void generate_stack_pop(MEMORY scope, memory_register into);

void generate_leave(MEMORY scope);

void generate_remove_from_stack(size_t bytes);

void generate_align_stackpointer(void);

void generate_void(MEMORY scope);

#endif //DLAXAR_CB_CODEGEN_H
