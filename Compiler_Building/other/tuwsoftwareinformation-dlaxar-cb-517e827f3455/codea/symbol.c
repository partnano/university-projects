#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "symbol.h"
#include <stdint.h>
#include "y.tab.h"

symbol_table* add_symbol(char *symbol, symbol_table *table) {
	symbol_table *nt = malloc(sizeof(symbol_table));
	nt->id = symbol;

	if(0 == table) {
		nt->next = 0;
	} else {
		nt->next = table;
	}
	return nt;
}

symbol_table* find_symbol(char *symbol, symbol_table *table) {
	while(table != NULL) {
		if(strcmp(symbol, table->id) == 0) {
			return table;
		} else {
			table = table->next;
		}
	}
	return NULL;
}

static
void print_symbols(symbol_table *table) {
	printf("Table>>\n");
	while(table != NULL) {
		printf("%s\n", table->id);
		table = table->next;
	}
	printf("<<Table\n");
}

void require_symbol(char *symbol, symbol_table *table) {
	if(find_symbol(symbol, table) == NULL) {
		fprintf(stderr, "Could not find symbol '%s'\n", symbol);
		print_symbols(table);
		exit(3);
	}
}

void require_unused_symbol(char *symbol, symbol_table *table) {
	if(find_symbol(symbol, table) != NULL) {
		fprintf(stderr, "Duplicate symbol '%s'\n", symbol);
		exit(3);
	}
}
