#include <printf.h>
#include <assert.h>
#include "codegen.h"
#include "burg.h"

char const* ABI_PREFIX = "_";

static
bool is_memory(NODEPTR_TYPE node) {
	return node->reg != NULL;
}

static
bool is_register(NODEPTR_TYPE node) {
	return is_memory(node) && node->reg->type == REGISTER;
}

static
bool is_write_protected(NODEPTR_TYPE node) {
	if(node->op != TERM_VARIABLE) return false;

	if(is_memory(node)) {
		return node->reg->protect == DISABLE_WRITE;
	}

	return true;
}

static
bool is_tmp_register(NODEPTR_TYPE node) {
	return !is_write_protected(node) && is_register(node);
}

static
bool is_stack(NODEPTR_TYPE node) {
	return is_memory(node) && node->reg->type == STACK;
}

static
bool is_immediate(NODEPTR_TYPE node) {
	return node->op == TERM_CONST; // !(is_register(node) || is_write_protected(node));
}

static
void assure_is_register(MEMORY scope, NODEPTR_TYPE operand, ACCESS *left) {
	if(is_tmp_register(operand)) {
		*left = operand->reg;
	}
	else if(is_write_protected(operand)) {
		*left = mem_allocate_tmp(scope);
		printf("movq %s, %s\n", access_to_str(mem_access(scope, operand->name)), access_to_str(*left));
	}
	else if(is_immediate(operand)) {
		*left = mem_allocate_tmp(scope);
		printf("movq $%lli, %s\n", (int64_t) operand->value, access_to_str(*left));
	} else {
		fprintf(stderr, "Unexpected fallthrough in assure_is_register");
		assert(0);
	}
}


static
void prepare_operation_src_dest(MEMORY scope, char const *cmd, NODEPTR_TYPE src, NODEPTR_TYPE dst,
                                ACCESS *src_reg, ACCESS *dst_reg, bool commutative) {
	ACCESS bu;
	if(src_reg == NULL) {
		src_reg = &bu;
	}
	if(dst_reg == NULL) {
		dst_reg = &bu;
	}

	uint64_t src_value = 0;
	if(is_memory(src) && is_tmp_register(dst)) {
		printf("# both registers \n");
		*src_reg = src->reg;
		*dst_reg = dst->reg;
	} else if(is_tmp_register(src)) {
		printf("# src is temp\n");
		if(commutative) {
			printf("# using commutivity\n");
			*dst_reg = src->reg;
			if(is_immediate(dst)) {
				*src_reg = NULL;
				src_value = dst->value;
			} else {
				// variable
				*src_reg = mem_access(scope, dst->name);
			}
		} else {
			printf("# can't use commutivity\n");
			assure_is_register(scope, dst, dst_reg);

			if(is_immediate(src)) {
				*src_reg = NULL;
				src_value = src->value;
			} else {
				*src_reg = mem_access(scope, src->name);
			}
		}
	} else if(is_tmp_register(dst)) {
		printf("# dst is tmp register\n");
		*dst_reg = dst->reg;

		if(is_immediate(src)) {
			*src_reg = NULL;
			src_value = src->value;
		} else {
			*src_reg = mem_access(scope, src->name);
		}
	} else if(is_write_protected(dst) || is_immediate(dst)) {
		printf("# dst is write protected or immediate\n");
		assure_is_register(scope, dst, dst_reg);

		if(is_immediate(src)) {
			*src_reg = NULL;
			src_value = src->value;
		} else {
			*src_reg = mem_access(scope, src->name);
		}
	} else {
		fprintf(stderr, "unexpected fallthrough");
		exit(255);
	}


	if(*src_reg == NULL) {
		printf("%s $%llu, %s\n", cmd, src_value, access_to_str(*dst_reg));
	} else {
		printf("%s %s, %s\n", cmd, access_to_str(*src_reg), access_to_str(*dst_reg));
	}
}

void generate_function_head(char const* name) {
	printf(".globl %s%s\n", ABI_PREFIX, name);
	printf("%s%s:\n", ABI_PREFIX, name);
//	printf("enter $0, $0\n");

}

void generate_label(char const* prefix, char const* name) {
	printf("%s%s_%s:\n", prefix, scope->name, name);
}

void generate_return(NODEPTR_TYPE bnode, MEMORY scope) {
	if(is_register(bnode->left)) {
		printf("movq %s, %%rax\n", access_to_str(bnode->left->reg));
	}
	else if(is_write_protected(bnode->left)){
		printf("movq %s, %%rax\n", access_to_str(mem_access(scope, bnode->left->name)));
	} else {
		printf("movq $%llu, %%rax\n", bnode->left->value);
	}

	generate_leave(scope);
}

void generate_mulq(NODEPTR_TYPE bnode, MEMORY scope) {
	ACCESS left, right;

	prepare_operation_src_dest(scope, "imul", bnode->left, bnode->right, &left, &right, true);

	bnode->reg = right;
}

void generate_addq(NODEPTR_TYPE bnode, MEMORY scope) {
	ACCESS left, right;
	prepare_operation_src_dest(scope, "addq", bnode->left, bnode->right, &left, &right, true);

	bnode->reg = right;
}

void generate_minus(NODEPTR_TYPE bnode, MEMORY scope) {
	ACCESS left;
	assure_is_register(scope, bnode->left, &left);

	printf("negq %s\n", access_to_str(left));

	bnode->reg = left;
}

void generate_compute_array_address(NODEPTR_TYPE node, MEMORY scope) {
	assure_is_register(scope, node->right, &node->right->reg);

	printf("leaq (, %s, 8), %s\n", access_to_str(node->right->reg), access_to_str(node->right->reg));

	prepare_operation_src_dest(scope, "addq", node->left, node->right, NULL, &node->reg, true);
}

void generate_array(NODEPTR_TYPE node, MEMORY scope) {
	generate_compute_array_address(node, scope);

	printf("movq (%s), %s\n", access_to_str(node->reg), access_to_str(node->reg));
}

static
void assign(MEMORY scope, NODEPTR_TYPE value, ACCESS lhs) {
	if(is_register(value) || is_stack(value)) {
		printf("movq %s, %s\n", access_to_str(value->reg), access_to_str(lhs));
	} else if(is_write_protected(value)) {
		printf("movq %s, %s\n", access_to_str(mem_access(scope, value->name)), access_to_str(lhs));
	} else {
		printf("movq $%llu, %s\n", value->value, access_to_str(lhs));
	}
}

void generate_definition(NODEPTR_TYPE node, MEMORY scope) {
	mem_allocate(scope, node->name);

	assign(scope, node->left, mem_access(scope, node->name));
}

void generate_assignment(NODEPTR_TYPE node, MEMORY scope) {
	if(is_write_protected(node->left)) {
		assign(scope, node->right, mem_access(scope, node->left->name));
	} else {
		// array assignment

		NODEPTR_TYPE value = node->right;
		ACCESS lhs = node->left->reg;

		if(is_register(value) || is_stack(value)) {
			printf("movq %s, (%s)\n", access_to_str(value->reg), access_to_str(lhs));
		} else if(is_write_protected(value)) {
			printf("movq %s, (%s)\n", access_to_str(mem_access(scope, value->name)), access_to_str(lhs));
		} else {
			printf("movq $%llu, (%s)\n", value->value, access_to_str(lhs));
		}
	}
}

void generate_jump_to_end_of_nested_condition(char const *jmp, NODEPTR_TYPE node, const char* yn) {
	if(yn == NULL) {
		yn = "no";
	}
	if(node->negations == 0) {
		printf("%s L_if_%llu_%s\n", jmp, node->parent, yn);
	} else {
		printf("%s L_if_not_%llu_%s\n", jmp, node->parent, yn);
	}
}

static void compare(MEMORY scope, NODEPTR_TYPE left, NODEPTR_TYPE right) {
	ACCESS r = right->reg;

	if(is_register(left)) {
		printf("cmpq %s, %s\n", access_to_str(left->reg), access_to_str(r));
	} else if(is_write_protected(left)) {
		printf("cmpq %s, %s\n", access_to_str(mem_access(scope, left->name)), access_to_str(r));
	} else {
		printf("cmpq $%llu, %s\n", left->value, access_to_str(r));
	}
	mem_free_tmp(scope);

}

void generate_not_equal(NODEPTR_TYPE node, MEMORY scope) {
//	todo unused tmp register allocated here
	assure_is_register(scope, node->right, &node->right->reg);

	compare(scope, node->left, node->right);

	generate_jump_to_end_of_nested_condition("je", node, "no");
}

void generate_greater_than(NODEPTR_TYPE node, MEMORY scope) {
//	todo unused tmp register allocated here
	assure_is_register(scope, node->left, &node->left->reg);

	compare(scope, node->right, node->left);

	generate_jump_to_end_of_nested_condition("jle", node, "no");
}

void generate_not_labels(NODEPTR_TYPE node, MEMORY scope) {
	printf("# %d\n", node->negations);
//	printf("jmp L_if_not_%llu_%s\n", node->value, "yes");

	printf("L_if_not_%llu_yes: ", node->value);
	generate_jump_to_end_of_nested_condition("\tjmp", node, "no");

	printf("L_if_not_%llu_no: # convert to yes -> continue with and's", node->value);
//	printf("jmp L_if_not_%llu_after\n", node->value);

//	printf("L_if_not_%llu_after: \n", node->value);
}

static
void generate_argument_impl(NODEPTR_TYPE node, MEMORY scope) {
	NODEPTR_TYPE expr = node->left;

	ACCESS a = mem_allocate_arguments(scope, scope->arguments);

	if(is_write_protected(expr)) {
		if(a->type == REGISTER) {
			printf("movq %s, %s\n", access_to_str(mem_access(scope, expr->name)), access_to_str(a));
		} else {
			printf("pushq %s\n", access_to_str(mem_access(scope, expr->name)));
			a->access.stack_position->committed = true;
		}
	} else if(is_memory(expr)) {
		if(a->type == REGISTER) {
			printf("movq %s, %s\n", access_to_str(expr->reg), access_to_str(a));
		} else {
			printf("pushq %s\n", access_to_str(expr->reg));
			a->access.stack_position->committed = true;
		}
	} else {
		printf("movq $%llu, %s\n", expr->value, access_to_str(a));
	}

	scope->arguments++;
}

static void generate_argument_stack(NODEPTR_TYPE node, MEMORY scope) {
	if(node->right->op == TERM_ARGS) {
		generate_argument_stack(node->right, scope);
	}
	generate_argument_impl(node, scope);
}

void generate_argument(NODEPTR_TYPE node, MEMORY scope) {

	if(scope->arguments == 0) {
		printf("# backing up registers\n");
		mem_save_to_stack(scope);
	}

	generate_argument_impl(node, scope);
}

void generate_call(NODEPTR_TYPE node, MEMORY scope) {

	printf("callq %s%s\n", ABI_PREFIX, node->name);

	printf("# restoring registers\n");
	mem_restore_savepoint(scope);
	scope->arguments = 0;

	// move out %rax
	ACCESS out = mem_allocate_tmp(scope);
	printf("movq %%rax, %s\n", access_to_str(out));

	node->reg = out;
}

void generate_stack_push(MEMORY scope, memory_register reg) {
	printf("pushq %s\n", reg_to_str(reg));
}

void generate_stack_pop(MEMORY scope, memory_register into) {
	printf("popq %s\n", reg_to_str(into));
}

void generate_leave(MEMORY scope) {
	if(scope->additionalStackSpace > 0) {
		generate_remove_from_stack((size_t) (8 * scope->additionalStackSpace));
	}
	if(scope->stackFramed) {
		printf("leave\n");
	}

	printf("retq\n");
}

void generate_remove_from_stack(size_t bytes) {
	printf("addq $%zu, %%rsp \n", bytes);
}

void generate_align_stackpointer(void) {
	printf("subq $8, %%rsp\n");
}

void generate_void(MEMORY scope) {
	if(!scope->lastStmtWasReturn) {
		printf("retq\n");
	}
}
