%{
#include <stdio.h>
#include <stdarg.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdint.h>
#include "symbol.h"
#include "burg.h"
#include "terminals.h"
#include "tree.h"
#include "codegen.h"
#include "memory.h"

#define YYERROR_VERBOSE 1
#define YYDEBUG (1)
extern FILE *yyin;
extern int errno;
extern int optind;
extern char *optarg;
int yydebug;

int line_counter = 0;


void yyerror(char *err) {
	fprintf(stderr, "> %s\n", err);

}


int yylex();

void L(char const* restrict format, ...) {
	if(!yydebug) {return;}


	va_list args;
	va_start(args, format);
	(void) vfprintf(stderr, format, args);
	(void) fprintf(stderr, "\n");
	va_end(args);
}

char buf[500];

%}

%union {
	char *str;
	uint64_t num;
}

%token <str> T_IDENTIFIER
%token <num> T_NUMBER

%token T_NEQUAL

%token T_END
%token T_RETURN
%token T_GOTO
%token T_IF
%token T_VAR
%token T_AND
%token T_NOT

%type <str> Labeldef

@autoinh variables memory labels

@attributes {
	MEMORY memory;
} Funcdef

@attributes {
	symbol_table *label_prev;
	symbol_table *label_now;
	symbol_table *variables;
} Labeldef

@attributes {
	symbol_table *label_prev;
	symbol_table *label_now;
	symbol_table *labels;
	symbol_table *variables_prev;
	symbol_table *variables_now;
	MEMORY memory;
} Stats StatsRequired

@attributes {
	symbol_table *label_prev;
	symbol_table *label_now;
	symbol_table *labels;
	symbol_table *variables_prev;
	symbol_table *variables_now;
	NODEPTR_TYPE ast;
	MEMORY memory;
} Stat

@attributes {
	symbol_table *variables_prev;
	symbol_table *variables_now;
	MEMORY memory;
} Pars

@attributes {
	symbol_table *variables;
	NODEPTR_TYPE ast;
	MEMORY memory;
} AndCond Cond Cterm Expr Term SumExpr MultExpr MinusExpr Lexpr Args


@attributes { char *name; } T_IDENTIFIER
@attributes { uint64_t value; } T_NUMBER

@traversal @postorder POSTHOOK
@traversal @preorder PREHOOK

@traversal @preorder @lefttoright CODEGEN
@traversal @postorder @lefttoright POSTCODEGEN

%%

Program     :
			|           Funcdef ';' Program
						@{
							@i @Funcdef.memory@ = create_scope(NULL, "funcdef");
						@}
;

Funcdef     :           T_IDENTIFIER '(' Pars ')' Stats T_END
						@{
							@i @Pars.variables_prev@ = NULL;
							@i @Stats.label_prev@ = NULL;
							@i @Stats.variables_prev@ = @Pars.variables_now@;
							@i @Stats.labels@ = @Stats.label_now@;

							@CODEGEN scope = @Funcdef.memory@;
							@CODEGEN @Funcdef.memory@->name = @T_IDENTIFIER.name@;
							@CODEGEN generate_function_head(@T_IDENTIFIER.name@);
							@POSTCODEGEN generate_void(@Funcdef.memory@); // fixes void
						@}
;

Pars        :
						@{
							@i @Pars.variables_now@ = @Pars.variables_prev@;
						@}
			|           T_IDENTIFIER ',' Pars
						@{
							@i @Pars.1.variables_prev@ = add_symbol(@T_IDENTIFIER.name@, @Pars.0.variables_prev@);
							@i @Pars.0.variables_now@ = @Pars.1.variables_now@;

							@PREHOOK require_unused_symbol(@T_IDENTIFIER.name@, @Pars.variables_prev@);
							
							@CODEGEN mem_allocate_parameters(scope, @T_IDENTIFIER.name@);

						@}
			|           T_IDENTIFIER
						@{
							@i @Pars.0.variables_now@ = add_symbol(@T_IDENTIFIER.name@, @Pars.variables_prev@);

							@PREHOOK require_unused_symbol(@T_IDENTIFIER.name@, @Pars.variables_prev@);

							@CODEGEN mem_allocate_parameters(scope, @T_IDENTIFIER.name@);
						@}
;

Stats       :
						@{
							@i @Stats.label_now@ = @Stats.label_prev@;
							@i @Stats.variables_now@ = @Stats.variables_prev@;
						@}
			|           StatsRequired
						@{
							@i @StatsRequired.label_prev@ = @Stats.label_prev@;
							@i @StatsRequired.variables_prev@ = @Stats.variables_prev@;

							@i @Stats.label_now@ = @StatsRequired.label_now@;
							@i @Stats.variables_now@ = @StatsRequired.variables_now@;
						@}
;

StatsRequired:          Labeldef StatsRequired
						@{
							@i @Labeldef.label_prev@ = @StatsRequired.0.label_prev@;
							@i @Labeldef.variables@ = @StatsRequired.0.variables_prev@;
							@i @StatsRequired.1.label_prev@ = @Labeldef.label_now@;
							@i @StatsRequired.0.label_now@ = @StatsRequired.1.label_now@;

							@i @StatsRequired.0.variables_now@ = @StatsRequired.1.variables_now@;
							@i @StatsRequired.1.variables_prev@ = @StatsRequired.0.variables_prev@;
						@}
			|           Stat ';' Stats
						@{
							@i @Stat.label_prev@ = @StatsRequired.0.label_prev@;
							@i @Stats.0.label_prev@ = @Stat.label_now@;
							@i @StatsRequired.0.label_now@ = @Stats.0.label_now@;

							@i @Stat.variables_prev@ = @StatsRequired.0.variables_prev@;
							@i @Stats.0.variables_prev@ = @Stat.variables_now@;
							@i @StatsRequired.0.variables_now@ = @Stats.0.variables_now@;

							@CODEGEN mem_push_call(@Stats.0.memory@);
							@CODEGEN burm_label(@Stat.ast@); burm_reduce(@Stat.ast@, 1);
						@}
;

Labeldef    :           T_IDENTIFIER ':'
						@{
							@PREHOOK require_unused_symbol(@T_IDENTIFIER.name@, @Labeldef.label_prev@);
							@PREHOOK require_unused_symbol(@T_IDENTIFIER.name@, @Labeldef.variables@);

							@i @Labeldef.label_now@ = add_symbol(@T_IDENTIFIER.name@, @Labeldef.label_prev@);

							@CODEGEN generate_label("L_", @T_IDENTIFIER.name@);
						@}
;

Stat        :           T_RETURN Expr
						@{
							@i @Stat.label_now@ = @Stat.label_prev@;
							@i @Stat.0.variables_now@ = @Stat.0.variables_prev@;
							@i @Expr.variables@ = @Stat.0.variables_prev@;

							@i @Stat.ast@ = tree_node_single(TERM_RETURN, @Expr.ast@);

						@}
			|           T_GOTO T_IDENTIFIER
						@{
							@i @Stat.label_now@ = @Stat.label_prev@;
							@i @Stat.0.variables_now@ = @Stat.0.variables_prev@;

							@i @Stat.ast@ = tree_goto(@T_IDENTIFIER.name@);

							@POSTHOOK require_symbol(@T_IDENTIFIER.name@, @Stat.labels@);
						@}
			|           T_IF Cond T_GOTO T_IDENTIFIER
						@{
							@i @Stat.label_now@ = @Stat.label_prev@;
							@i @Stat.0.variables_now@ = @Stat.0.variables_prev@;
							@i @Cond.variables@ = @Stat.0.variables_prev@;

							@i @Stat.ast@ = tree_if(@Cond.ast@, @T_IDENTIFIER.name@);

							@POSTHOOK require_symbol(@T_IDENTIFIER.name@, @Stat.labels@);
						@}
			|           T_VAR T_IDENTIFIER '=' Expr
						@{
							@i @Stat.label_now@ = @Stat.label_prev@;
							@i @Stat.0.variables_now@ = add_symbol(@T_IDENTIFIER.name@, @Stat.0.variables_prev@);
							@i @Expr.variables@ = @Stat.0.variables_prev@;

							@i @Stat.ast@ = tree_assign_define(@Stat.memory@, @T_IDENTIFIER.name@, @Expr.ast@);

							@PREHOOK require_unused_symbol(@T_IDENTIFIER.name@, @Stat.variables_prev@);
							@PREHOOK require_unused_symbol(@T_IDENTIFIER.name@, @Stat.label_prev@);
						@}
			|           Lexpr '=' Expr
						@{
							@i @Stat.label_now@ = @Stat.label_prev@;
							@i @Stat.0.variables_now@ = @Stat.0.variables_prev@;
							@i @Expr.variables@ = @Stat.0.variables_prev@;
							@i @Lexpr.variables@ = @Stat.0.variables_prev@;

							@i @Stat.ast@ = tree_assign(@Stat.memory@, @Lexpr.ast@, @Expr.ast@);
						@}
			|           Term
						@{
							@i @Stat.label_now@ = @Stat.label_prev@;
							@i @Stat.0.variables_now@ = @Stat.0.variables_prev@;
							@i @Term.variables@ = @Stat.0.variables_prev@;

							@i @Stat.ast@ = @Term.ast@;
						@}
;

Cond        :           Cterm
						@{
							@i @Cond.ast@ = @Cterm.ast@;
						@}
			|           Cterm T_AND AndCond
						@{
							@i @Cond.ast@ = tree_subtree(TERM_AND, @Cterm.ast@, @AndCond.ast@);
						@}
			|           T_NOT Cterm
						@{
							@i @Cond.ast@ = tree_node_not(@Cterm.ast@);
						@}
;

AndCond     :           Cterm
						@{
							@i @AndCond.ast@ = @Cterm.ast@;
						@}
			|           Cterm T_AND AndCond
						@{
							@i @AndCond.0.ast@ = tree_subtree(TERM_AND, @Cterm.ast@, @AndCond.1.ast@);
						@}
;

Cterm       :           '(' Cond ')'
						@{
							@i @Cterm.ast@ = @Cond.ast@;
						@}
			|           Expr T_NEQUAL Expr
						@{
							@i @Cterm.ast@ = tree_subtree(TERM_NEQUAL, @Expr.0.ast@, @Expr.1.ast@);
						@}
			|           Expr   '>'    Expr
						@{
							@i @Cterm.ast@ = tree_subtree(TERM_GT    , @Expr.0.ast@, @Expr.1.ast@);
						@}
;

Lexpr       :           T_IDENTIFIER
						@{
							@i @Lexpr.ast@ = tree_variable(@Lexpr.memory@, @T_IDENTIFIER.name@);

							@PREHOOK require_symbol(@T_IDENTIFIER.name@, @Lexpr.variables@);
						@}
			|           Term '[' Expr ']'
			            @{
			                @i @Lexpr.ast@ = tree_subtree(TERM_ARRAY, @Term.ast@, @Expr.ast@);
			            @}
;

Expr        :           SumExpr
						@{
							@i @Expr.ast@ = @SumExpr.ast@;
						@}
			|           MultExpr
						@{
							@i @Expr.ast@ = @MultExpr.ast@;
						@}
			|           '-' MinusExpr
						@{
							@i @Expr.ast@ = tree_node_single(TERM_MINUS, @MinusExpr.0.ast@);
						@}
;

SumExpr     :           Term '+' SumExpr
						@{
							@i @SumExpr.0.ast@ = tree_subtree(TERM_ADD, @Term.0.ast@, @SumExpr.1.ast@);
						@}
			|           Term
						@{
							@i @SumExpr.0.ast@ = @Term.0.ast@;
						@}
;

MultExpr    :           Term
						@{
							@i @MultExpr.0.ast@ = @Term.ast@;
						@}
			|           Term '*' MultExpr
						@{
							@i @MultExpr.0.ast@ = tree_subtree(TERM_MULT, @Term.0.ast@, @MultExpr.1.ast@);
						@}
;

MinusExpr   :           Term
						@{
							@i @MinusExpr.0.ast@ = @Term.ast@;
						@}
			|           '-' MinusExpr
						@{
							@i @MinusExpr.ast@ = tree_node_single(TERM_MINUS, @MinusExpr.1.ast@);
						@}
;

Term        :           '(' Expr ')'
						@{
							@i @Term.ast@ = @Expr.ast@;
						@}
			|           T_NUMBER
						@{
							@i @Term.ast@ = tree_literal(@T_NUMBER.value@);
						@}
			|           Term '[' Expr ']'
						@{
							@i @Term.0.ast@ = tree_subtree(TERM_ARRAY, @Term.1.ast@, @Expr.ast@);
						@}
			|           T_IDENTIFIER
						@{
							@PREHOOK require_symbol(@T_IDENTIFIER.name@, @Term.variables@);
							@i @Term.ast@ = tree_variable(@Term.memory@, @T_IDENTIFIER.name@);
						@}
			|           T_IDENTIFIER '(' Args ')'
						@{
							@i @Term.ast@ = tree_call(@T_IDENTIFIER.name@, @Args.ast@);
						@}
;

Args        :
						@{
							@i @Args.ast@ = tree_leaf(TERM_LAST);
						@}
			|           Expr ',' Args
						@{
							@i @Args.0.ast@ = tree_arglist(@Expr.ast@, @Args.1.ast@);
						@}
			|           Expr
						@{
							@i @Args.0.ast@ = tree_subtree(TERM_ARGS, @Expr.ast@, tree_leaf(TERM_LAST));
						@}
;



%%

int main(int argc,char *argv[]) {
	char c;
	while( (c=getopt(argc, argv,"v")) != EOF ) {
		switch(c) {
			case 'v':
				yydebug=1;
				break;
		}
	}
        if ( argc > optind ) {
                if ( (yyin = fopen(argv[optind],"r")) == 0 ) {
                        perror(argv[optind]);
                        exit(1);
                }
        }
	if(yyparse() > 0) {
		exit(2);
	}
}
