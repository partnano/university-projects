#ifndef DLAXAR_CB_SYMBOL_H
#define DLAXAR_CB_SYMBOL_H

typedef struct symbol_table symbol_table;

struct symbol_table {
	char const* id;
	symbol_table *next;
};

symbol_table* add_symbol(char *symbol, symbol_table *table);

void require_symbol(char *symbol, symbol_table *table);

void require_unused_symbol(char *symbol, symbol_table *table);

#endif //DLAXAR_CB_SYMBOL_H
