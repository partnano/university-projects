#!/bin/bash

eCases=`ls test/error`

for f in $eCases
do
	./ag test/error/$f

	rc=$?
	if [ $rc -ne 3 ]
	then
		echo "[FAIL] $f failed with status ${rc} (should have a static error (3))"
	else
		echo "[PASS] $f passed"
	fi
done

cases=`ls test/static`

for f in $cases
do
	./ag test/static/$f

	rc=$?
	if [ $rc -ne 0 ]
	then
		echo "[FAIL] $f failed with status ${rc}"
	else
		echo "[PASS] $f passed"
	fi
done