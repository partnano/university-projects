	.file	"a.c"
	.text

	.globl _asma
	.globl asma

_asma:
asma:
.LFB0:
	.cfi_startproc

	# This is an extremely straightforward implementation of the exercise
	# and probably could be optimized

	# rdi = x0, rsi = x1

	movq %rdi, %rax # rax = x0
	movq %rdx, %r11 # r11 = y

	mulq %r11       # rdx:rax = rax * r11
	movq %rax, (%rcx) # r0 = (rax*r11)_0 = (rax)
	movq %rdx, %r8

	movq %rsi, %rax # rax = x1
	mulq %r11 # rdx:rax = rax * r11
	add %rax, %r8 # r8 = rax + r8 <=> r8 = (x1*y)_0 + (x0*y)_1

	movq %r8, 8(%rcx)

	adc $0, %rdx

	movq %rdx, 16(%rcx)


	ret
	.cfi_endproc
.LFE0:
