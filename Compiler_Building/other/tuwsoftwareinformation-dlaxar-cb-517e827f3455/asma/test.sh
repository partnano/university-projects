#!/bin/bash

if [[ $# -ne 1 ]]; then
	echo "usage: $0 <asma-file>"
	exit 1
fi

g++ -std=c++14 test.cpp $1 -o test.1337
./test.1337
rm test.1337
