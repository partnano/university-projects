#include <cstddef>
#include <cstdio>
#include <cstring>
using namespace std;

using uint64 = unsigned long long;

extern "C"
void asma(uint64 a_lo, uint64 a_hi, uint64 b, uint64* r);

uint64 const testcases[][6] =
{
	{
		0, 0,
		0,
		0, 0, 0,
	},
	{
		0x664a'f8a9'e5f4'7002, 0xd078'9420'32bb'261a,
		0,
		0, 0, 0,
	},
	{
		0, 0,
		0xd0b8'07a1'6f98'7529,
		0, 0, 0,
	},
	{
		0x797e'3551'2b24'298e, 0xceb3'a604'1aac'bdab,
		1,
		0, 0x797e'3551'2b24'298e, 0xceb3'a604'1aac'bdab,
	},
	{
		0, 1,
		0x230b'2bdb'a795'945e,
		0, 0, 0x230b'2bdb'a795'945e,
	},
	{
		0xde38'48ef'c37f'1b33, 0x810d'd740'7625'8599,
		0xa6f4'c92d'b161'906f,
		0x90ec'fb92'd86d'6e8e, 0xe8eb'a60b'b1a5'9f7b, 0xa582'e9e3'bd63'fd57,
	},
	{
		0x015c'1ab1'75d3'0aeb, 0x0acf'7bee'e0b3'75bc,
		0x64fc'307d'1c57'a95b,
		0x0089'5159'87d7'44df, 0x6e3e'db3a'1cc1'022b, 0xc217'7ae9'eb67'f5d4,
	},
	{
		0xffff'ffff'ffff'ffff, 0xffff'ffff'ffff'ffff,
		0xffff'ffff'ffff'ffff,
		0xffff'ffff'ffff'fffe, 0xffff'ffff'ffff'ffff, 1,
	},
};

uint64 const random[3] =
{
	0xbe01'351a'7805'ce20,
	0x111b'1cde'c1fa'952a,
	0xad72'e119'4fa5'f4bd,
};

void green()
{
	printf("\e[32m");
}

void red()
{
	printf("\e[31m");
}

void reset()
{
	printf("\e[0m");
}

void test(size_t index)
{
	uint64 const* tc = testcases[index];
	
	uint64 r[5] = {0, 0, 0, 0, 0};
	asma(tc[1], tc[0], tc[2], r + 1);
	
	if(r[1] != tc[5] || r[2] != tc[4] || r[3] != tc[3])
	{
		red();
		printf("[FAIL]");
		reset();
		
		printf(" test case #%zu:\n", index + 1);
		printf("\tcomputation: %016llx.%016llx * %016llx\n", tc[0], tc[1], tc[2]);
		printf("\texpected:    %016llx.%016llx.%016llx\n", tc[3], tc[4], tc[5]);
		printf("\tgot          %016llx.%016llx.%016llx\n", r[3], r[2], r[1]);
		printf("\n");
		
		return;
	}
	
	memcpy(r + 1, random, sizeof random);
	asma(tc[1], tc[0], tc[2], r + 1);
	
	if(r[1] != tc[5] || r[2] != tc[4] || r[3] != tc[3])
	{
		red();
		printf("[FAIL]");
		reset();
		
		printf(" test case #%zu: incorrect result if buffer not zero-initialized:\n", index + 1);
		printf("\tcomputation: %016llx.%016llx * %016llx\n", tc[0], tc[1], tc[2]);
		printf("\texpected:    %016llx.%016llx.%016llx\n", tc[3], tc[4], tc[5]);
		printf("\tgot          %016llx.%016llx.%016llx\n", r[3], r[2], r[1]);
		printf("\n");
		
		return;
	}
	
	if(r[0] != 0 || r[4] != 0)
	{
		red();
		printf("[FAIL]");
		reset();
		
		printf(" test case #%zu: buffer overflow:\n", index + 1);
		
		if(r[0] != 0)
			printf("\tbytes before result overwritten\n");
		
		if(r[4] != 0)
			printf("\tbytes after result overwritten\n");
		
		return;
	}
	
	green();
	printf("[PASS]");
	reset();
	
	printf(" test case #%zu\n", index + 1);
}

int main()
{
	for(size_t i = 0; i != sizeof testcases / sizeof *testcases; ++i)
		test(i);
}
