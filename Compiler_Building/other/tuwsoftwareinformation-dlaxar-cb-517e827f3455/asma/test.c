#include <stdlib.h>
#include <stdio.h>

extern void asma(unsigned long x0, unsigned long x1, unsigned long y, unsigned long *a);

int main() {
	unsigned long *a = malloc(24);
	asma(0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, a);
	printf("%lx %lx %lx\n", a[2], a[1], a[0]);
}