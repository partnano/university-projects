#!/bin/bash

cases=`ls test`

for f in $cases
do
	./parser test/$f

	rc=$?
	if [ $rc -ne 0 ]
	then
		echo "[FAIL] $f failed with status (${rc})"
	else
		echo "[PASS] $f passed"
	fi
done