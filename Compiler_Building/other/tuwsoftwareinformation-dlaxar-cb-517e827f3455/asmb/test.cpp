#include <algorithm>
#include <cstddef>
#include <cstdio>
#include <cstring>
#include <stdexcept>
#include <string>
#include <vector>

using uint64 = unsigned long long;

extern "C"
void asmb(uint64 const* a, uint64 n, uint64 b, uint64* r);

struct TestCase
{
	std::string name;
	std::vector<uint64> a;
	uint64 b;
	std::vector<uint64> r;
};

TestCase const testcases[] =
{
	{
		"0 * 0",
		{0,},
		0,
		{0, 0,},
	},
	{
		"any * 0, n = 1",
		{0x730b'ee82'8793'7f83,},
		0,
		{0, 0,},
	},
	{
		"any * 0, n = 2",
		{0x664a'f8a9'e5f4'7002, 0xd078'9420'32bb'261a,},
		0,
		{0, 0, 0,},
	},
	{
		"any * 0, n = 3",
		{0x9210'6de5'1a91'd15f, 0xd217'2499'b822'321c, 0x0fc3'6320'fa99'037b,},
		0,
		{0, 0, 0, 0,},
	},
	{
		"any * 0, n = 4",
		{0x45f2'52cd'eb02'445a, 0x2b88'abe4'0d2c'cbeb, 0x6eca'4a7c'79b8'd081, 0x5297'd097'693c'98f2,},
		0,
		{0, 0, 0, 0, 0,},
	},
	{
		"0 * any",
		{0,},
		0xd0b8'07a1'6f98'7529,
		{0, 0,},
	},
	{
		"any * 1, n = 1",
		{0xd4bc'a185'6393'9d36,},
		1,
		{0, 0xd4bc'a185'6393'9d36,},
	},
	{
		"any * 1, n = 2",
		{0x797e'3551'2b24'298e, 0xceb3'a604'1aac'bdab,},
		1,
		{0, 0x797e'3551'2b24'298e, 0xceb3'a604'1aac'bdab,},
	},
	{
		"any * 1, n = 3",
		{0x418d'ee24'f269'f971, 0xc90d'6ee8'9f61'4f1f, 0x7766'51ca'20f5'c551,},
		1,
		{0, 0x418d'ee24'f269'f971, 0xc90d'6ee8'9f61'4f1f, 0x7766'51ca'20f5'c551,},
	},
	{
		"any * 1, n = 4",
		{0xf742'3852'348b'33ca, 0xdd98'f1ec'cfa6'64df, 0xf7ae'a424'001b'0bc9, 0xc5e8'02e1'0ffd'5159,},
		1,
		{0, 0xf742'3852'348b'33ca, 0xdd98'f1ec'cfa6'64df, 0xf7ae'a424'001b'0bc9, 0xc5e8'02e1'0ffd'5159,},
	},
	{
		"1 * any",
		{1,},
		0x230b'2bdb'a795'945e,
		{0, 0x230b'2bdb'a795'945e,},
	},
	{
		"any * any, n = 1",
		{0xa98e'b589'6eea'7f31,},
		0xc882'de22'876b'20db,
		{0x84ce'2b6c'2e84'0707, 0x3208'e207'32fb'eeeb,},
	},
	{
		"any * any, n = 2",
		{0xde38'48ef'c37f'1b33, 0x810d'd740'7625'8599,},
		0xa6f4'c92d'b161'906f,
		{0x90ec'fb92'd86d'6e8e, 0xe8eb'a60b'b1a5'9f7b, 0xa582'e9e3'bd63'fd57,},
	},
	{
		"any * any, n = 3",
		{0x1013'8402'd1ee'b159, 0xdd27'fc7b'8571'b920, 0x1056'4867'398c'cb6c,},
		0xa2b1'7e8b'2aed'd5fd,
		{0x0a37'7efa'6710'6648, 0xc227'0f98'7fc3'75c9, 0x8f35'b0ae'de0e'eb7c, 0x6fc2'6d42'1061'e5bc,},
	},
	{
		"any * any, n = 4",
		{0xf4c5'dbfb'3a25'6093, 0x5214'c561'4fe9'5913, 0x72ab'37d0'21c0'f986, 0x631d'e144'70dd'fbf1,},
		0x5e63'bde9'6c64'6228,
		{0x5a40'04df'94a2'5b71, 0xdd39'0f0c'5f96'6356, 0x874d'6597'9874'c1f7, 0x19e1'9dd9'd466'789f, 0x547f'5e3f'b345'9fa8,},
	},
	{
		"max * max, n = 1",
		{0xffff'ffff'ffff'ffff,},
		0xffff'ffff'ffff'ffff,
		{0xffff'ffff'ffff'fffe, 1},
	},
	{
		"max * max, n = 2",
		{0xffff'ffff'ffff'ffff, 0xffff'ffff'ffff'ffff,},
		0xffff'ffff'ffff'ffff,
		{0xffff'ffff'ffff'fffe, 0xffff'ffff'ffff'ffff, 1,},
	},
	{
		"max * max, n = 3",
		{0xffff'ffff'ffff'ffff, 0xffff'ffff'ffff'ffff, 0xffff'ffff'ffff'ffff,},
		0xffff'ffff'ffff'ffff,
		{0xffff'ffff'ffff'fffe, 0xffff'ffff'ffff'ffff, 0xffff'ffff'ffff'ffff, 1,},
	},
	{
		"max * max, n = 4",
		{0xffff'ffff'ffff'ffff, 0xffff'ffff'ffff'ffff, 0xffff'ffff'ffff'ffff, 0xffff'ffff'ffff'ffff,},
		0xffff'ffff'ffff'ffff,
		{0xffff'ffff'ffff'fffe, 0xffff'ffff'ffff'ffff, 0xffff'ffff'ffff'ffff, 0xffff'ffff'ffff'ffff, 1,},
	},
};

constexpr uint64 const OVERFLOW_COOKIE = 0xbe01'351a'7805'ce20;
constexpr uint64 const RANDOM_COOKIE   = 0x111b'1cde'c1fa'952a;

void green()
{
	std::printf("\e[32m");
}

void red()
{
	std::printf("\e[31m");
}

void reset()
{
	std::printf("\e[0m");
}

template <typename It>
void printNum(It begin, It end)
{
	if(begin == end)
		throw std::runtime_error("0-word number, wtf?");
	
	std::printf("%016llx", *begin++);
	
	while(begin != end)
		std::printf(".%016llx", *begin++);
}

template <typename It1, typename It2, typename It3>
void printComputation(It1 abegin, It1 aend, uint64 b, It2 expectedBegin, It2 expectedEnd, It3 gotBegin, It3 gotEnd)
{
	std::printf("\tcomputation: ");
	printNum(abegin, aend);
	std::printf(" * ");
	printNum(&b, &b + 1);
	std::printf("\n");
	
	std::printf("\texpected:    ");
	printNum(expectedBegin, expectedEnd);
	std::printf("\n");
	
	std::printf("\tgot:         ");
	printNum(gotBegin, gotEnd);
	std::printf("\n");
}

void printResult(std::string const& name, std::size_t number, bool passed, std::string const& message = "")
{
	if(passed)
	{
		green();
		std::printf("[PASS]");
	}
	
	else
	{
		red();
		std::printf("[FAIL]");
	}
	
	reset();
	
	std::printf(" test case #%02zu: %s", number, name.c_str());
	
	if(!message.empty())
		std::printf(": %s", message.c_str());
	
	std::printf("\n");
}

void test(TestCase const& tc, std::size_t number)
{
	if(tc.a.size() + 1 != tc.r.size())
		throw std::runtime_error("invalid test case");
	
	std::vector<uint64> r(tc.r.size() + 2);
	r.front() = OVERFLOW_COOKIE;
	r.back()  = OVERFLOW_COOKIE;
	
	std::vector<uint64> a = tc.a;
	std::reverse(a.begin(), a.end());
	
	asmb(a.data(), a.size(), tc.b, r.data() + 1);
	
	if(!std::equal(tc.r.rbegin(), tc.r.rend(), r.begin() + 1))
	{
		printResult(tc.name, number, false, "incorrect result");
		printComputation(tc.a.begin(), tc.a.end(), tc.b, tc.r.begin(), tc.r.end(), r.rbegin() + 1, r.rend() - 1);
		return;
	}
	
	std::fill(r.begin() + 1, r.end() - 1, RANDOM_COOKIE);
	asmb(a.data(), a.size(), tc.b, r.data() + 1);
	
	if(!std::equal(tc.r.rbegin(), tc.r.rend(), r.begin() + 1))
	{
		printResult(tc.name, number, false, "incorrect result if buffer not zero-initialized");
		printComputation(tc.a.begin(), tc.a.end(), tc.b, tc.r.begin(), tc.r.end(), r.rbegin() + 1, r.rend() - 1);
		return;
	}
	
	if(r.front() != OVERFLOW_COOKIE || r.back() != OVERFLOW_COOKIE)
	{
		printResult(tc.name, number, false, "buffer overflow");
		
		if(r.front() != OVERFLOW_COOKIE)
			std::printf("\tbytes before result overwritten\n");
		
		if(r.back() != OVERFLOW_COOKIE)
			std::printf("\tbytes after result overwritten\n");
		
		return;
	}

	printResult(tc.name, number, true);
}

int main()
{
	int number = 1;
	
	for(auto& tc : testcases)
		test(tc, number++);
}
