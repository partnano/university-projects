#include <stdlib.h>
#include <stdio.h>

#define MAX_IN (10)

extern void asmb(unsigned long *x, size_t n, unsigned long y, unsigned long *a);

void test(unsigned long *x, size_t n, unsigned long y, unsigned long *a) {

	size_t i = n;
	printf("                   ");
	while(i-- > 0) {
		printf("0x%16lx ", x[i]);
	}
	printf("* 0x%16lx =\n", y);
	i = n;
	printf("-------------------");
	while(i-- > 0) {
		printf("-------------------");
	}
	printf("----------------------");
	printf("\n");

	asmb(x, n, y, a);

	i = n+1;
	while(n != 0 && i-- > 0) {
		printf("0x%16lx ", a[n]);
	}

	printf("\n\n");
}


int main() {
	unsigned long *x = malloc(MAX_IN * sizeof(unsigned long));
	unsigned long *a = malloc((MAX_IN + 1) * sizeof(unsigned long));


	x[0] = 0xE;
	test(x, 1, 0x2, a);

	test((unsigned long *) 0xffff, 0, 9, (unsigned long *) 0x7777);

	x[0] = 0x8000000000000000;
	x[1] = 0x8000000000000000;
	test(x, 2, 0x2, a);

	x[0] = 0xffffffffffffffff;
	x[1] = 0xffffffffffffffff;
	test(x, 2, 0x2, a);

	x[0] = 0xffffffffffffffff;
	x[1] = 0xeeeeeeeeeeeeeeee;
	test(x, 2, 0x2, a);

	x[0] = 1;
	test(x, 1, 0, a);

	x[0] = 0xffffffffffffffff;
	x[1] = 0xffffffffffffffff;
	x[2] = 0xffffffffffffffff;
	test(x, 3, 0xffffffffffffffff, a);

	x[0] = 0;
	x[1] = 0;
	test(x, 2, 0, a);

	x[0] = 0x664af8a9e5f47002;
	x[1] = 0xd078942032bb261a;
	test(x, 2, 0, a);

	x[0] = 0xffffffffffffffff;
x[1] = 0xfffffffffffffffe;
test(x, 2, 0xffffffffffffffff, a);
//
//	size_t c = 65536*4096*2;
//	unsigned long *f = malloc(c * sizeof(unsigned long));
//	unsigned long *e = malloc((c+1) * sizeof(unsigned long));
//
//	for(int i = 0; i < c; i++) {
//		f[i] = 0xFFFFFFFFFFFFFFFF;
//	}
//
//	asmb(f, c, 0xFFFFFFFFFFFFFFFF, e);
//
//	printf("%16lx (...) %16lx\n", e[c], e[0]);



}