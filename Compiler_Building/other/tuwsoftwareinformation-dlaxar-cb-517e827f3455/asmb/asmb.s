	.file	"a.c"
	.text

	# duplicate labels
	.globl	_asmb
	.globl asmb
	.align	4, 0x90
_asmb:                                  ## @asmb
asmb:
	.cfi_startproc

	pushq	%rbp
Ltmp0:
	.cfi_def_cfa_offset 16
Ltmp1:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp2:
	.cfi_def_cfa_register %rbp

	# rdi = x
	# rsi = n
	# rdx = y
	# rcx = a

	# mul x => rdx:rax = rax * x

	movq %rdx, %r8 # r8 = y
	movq $0, %r9 # r9 = 0

	movq $0, %r11

	cmpq %rsi, %r11
	je   end

loop:
	movq (%rdi, %r9, 8), %rax # ... and store value at that addr into rax

	mulq %r8 # rdx:rax = rax * y; cf = bullshit
	add  %rax, %r11 # add carry and previous overflow

	movq %r11, (%rcx, %r9, 8) # store into memory (r9*8 + rcx)
	movq %rdx, %r11 # backup rdx
	adc  $0, %r11 # add carry to overflow

	inc  %r9 # prepare next pass # if r9 > n -> exit; else goto loop;

	cmpq %rsi, %r9 # flags = n - r9
	jne  loop

	add  $0, %r11 # add carry and previous overflow
	movq %r11, (%rcx,%r9, 8) # store into memory

	# end of function (stack operations follow)
end:
	popq %rbp
	retq


	.cfi_endproc
