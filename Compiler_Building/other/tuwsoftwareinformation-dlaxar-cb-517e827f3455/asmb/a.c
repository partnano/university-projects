#include <ntsid.h>

void asmb(unsigned long *x, size_t n, unsigned long y, unsigned long *a) {
	a[0] = x[0];
	a[1] = n;
	a[2] = y;

	a[n] = 15;
}
