	.section	__TEXT,__text,regular,pure_instructions
	.macosx_version_min 10, 11
	.globl	_asmb
	.align	4, 0x90
_asmb:                                  ## @asmb
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp0:
	.cfi_def_cfa_offset 16
Ltmp1:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp2:
	.cfi_def_cfa_register %rbp
	movq	(%rdi), %rax
	movq	%rax, (%rcx)
	movq	%rsi, 8(%rcx)
	movq	%rdx, 16(%rcx)
	movq	$15, (%rcx,%rsi,8)
	popq	%rbp
	retq
	.cfi_endproc


.subsections_via_symbols
