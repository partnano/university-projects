%{

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "symtab.h"

void yyerror(char const* message)
{
	fprintf(stderr, "%s\n", message);
}

int yylex();

%}

%union {
	char* id;
	int num;
}

%token <id>  TT_ID
%token <num> TT_NUM

%token TT_KW_AND
%token TT_KW_END
%token TT_KW_GOTO
%token TT_KW_IF
%token TT_KW_NOT
%token TT_KW_RETURN
%token TT_KW_VAR

%token TT_PT_NEQ
%token TT_PT_GT
%token TT_PT_SCOLON
%token TT_PT_COLON
%token TT_PT_COMMA
%token TT_PT_LPAREN
%token TT_PT_RPAREN
%token TT_PT_ASSIGN
%token TT_PT_ADD
%token TT_PT_MUL
%token TT_PT_NEG
%token TT_PT_LBRACK
%token TT_PT_RBRACK

@attributes
{
	char* name;
} TT_ID

@autoinh symtab

@attributes
{
	void* symtab;
} function params param stmts stmt labels label goto cond compl cterm lexpr expr negations sum product term args

@traversal @preorder buildLabels
@traversal checkLabels

@traversal variables

%%

program:
	| function TT_PT_SCOLON program
		@{
			@i @function.symtab@ = symtab_create();
		@}
	;

function: TT_ID TT_PT_LPAREN params TT_PT_RPAREN stmts TT_KW_END
	;

params:
	| param
	| param TT_PT_COMMA params
	;

param: TT_ID
		@{
			@variables symtab_insert(@param.symtab@, @TT_ID.name@, LOCAL);
		@}
	;

stmts:
	| stmt TT_PT_SCOLON stmts
	| labels stmt TT_PT_SCOLON stmts
	;

labels: label
	| label labels
	;

label: TT_ID TT_PT_COLON
		@{
			@buildLabels symtab_insert(@label.symtab@, @TT_ID.name@, LABEL);
		@}

stmt:  TT_KW_RETURN expr
	| goto
	| TT_KW_IF cond goto
	| TT_KW_VAR TT_ID TT_PT_ASSIGN expr
		@{
			@variables symtab_insert(@stmt.symtab@, @TT_ID.name@, LOCAL);
		@}
	| lexpr TT_PT_ASSIGN expr
	| term
	;

goto: TT_KW_GOTO TT_ID
		@{
			@checkLabels symtab_require(@goto.symtab@, @TT_ID.name@, LABEL);
		@}

cond: compl
	| compl TT_KW_AND cond
	;

compl: cterm
	| TT_KW_NOT cterm
	;

cterm: TT_PT_LPAREN cond TT_PT_RPAREN
	| expr TT_PT_NEQ expr
	| expr TT_PT_GT  expr
	;

lexpr: TT_ID
		@{
			@variables symtab_require(@lexpr.symtab@, @TT_ID.name@, LOCAL);
		@}
	| term TT_PT_LBRACK expr TT_PT_RBRACK
	;

expr: term
	| negations
	| sum
	| product
	;

negations: term
	| TT_PT_NEG negations
	;

sum: term
	| term TT_PT_ADD sum
	;

product: term
	| term TT_PT_MUL product
	;

term: TT_PT_LPAREN expr TT_PT_RPAREN
	| TT_NUM
	| term TT_PT_LBRACK expr TT_PT_RBRACK
	| TT_ID
		@{
			@variables symtab_require(@term.symtab@, @TT_ID.name@, LOCAL);
		@}
	| TT_ID TT_PT_LPAREN args TT_PT_RPAREN
	;

args:
	| expr
	| expr TT_PT_COMMA args
	;

%%

int main(int argc, char** argv)
{
	if(yyparse())
		return 2;

	return 0;
}
