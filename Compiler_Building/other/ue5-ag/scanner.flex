%option noyywrap

%{
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "y.tab.h"
%}

WHITESPACE [ \t\r\n]+|\(\*([^*)]|\*[^)]|[^*]\))*\*\)

KEYWORD and|end|goto|if|not|return|var
IDENT   [a-zA-Z][a-zA-Z0-9]*
NUM     [0-9_]+
PUNCT   !=|[;:,()=>+\-*\[\]]

%%

{WHITESPACE}

"and"    return TT_KW_AND;
"end"    return TT_KW_END;
"goto"   return TT_KW_GOTO;
"if"     return TT_KW_IF;
"not"    return TT_KW_NOT;
"return" return TT_KW_RETURN;
"var"    return TT_KW_VAR;

{IDENT} {
	yylval.id = strdup(yytext);
	return TT_ID;
	@{ @TT_ID.name@ = yylval.id; @}
}

"!=" return TT_PT_NEQ;
">"  return TT_PT_GT;
";"  return TT_PT_SCOLON;
":"  return TT_PT_COLON;
","  return TT_PT_COMMA;
"("  return TT_PT_LPAREN;
")"  return TT_PT_RPAREN;
"="  return TT_PT_ASSIGN;
"+"  return TT_PT_ADD;
"*"  return TT_PT_MUL;
"-"  return TT_PT_NEG;
"["  return TT_PT_LBRACK;
"]"  return TT_PT_RBRACK;

{NUM} {
	size_t count = 0;
	
	for(char* p = yytext; *p; ++p)
		if(*p != '_')
			++count;
	
	char* num = malloc(count + 1);
	
	if(!num)
		exit(3);
	
	char* p = yytext;
	for(size_t i = 0; i != count; ++p)
		if(*p != '_')
			num[i++] = *p;
	
	num[count] = 0;
	yylval.num = atoi(num);
	free(num);

	return TT_NUM;
}

. {
	printf("unexpected character: %s\n", yytext);
	exit(1);
}
