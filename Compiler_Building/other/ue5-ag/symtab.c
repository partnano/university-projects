#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "symtab.h"

typedef struct
{
	void* next;
	char const* name;
	SymType type;
} SymTabNode;

typedef struct
{
	SymTabNode* head;
} SymTab;

void* symtab_create()
{
	SymTab* symtab = malloc(sizeof(SymTab));
	symtab->head = NULL;
	return symtab;
}

static
SymTabNode* symtab_search(SymTab* symtab, char const* name)
{
	for(SymTabNode* node = symtab->head; node; node = node->next)
		if(strcmp(node->name, name) == 0)
			return node;
	
	return NULL;
}

static
SymTab* get(void* symtab)
{
	return (SymTab*)symtab;
}

static
char const* stringof(SymType type)
{
	switch(type)
	{
	case LABEL: return "label";
	case LOCAL: return "local";
	default: break;
	}
	
	fprintf(stderr, "unknown symbol type %d\n", type);
	exit(3);
}

void symtab_insert(void* symtab, char const* name, SymType type)
{
	printf("inserting symbol %s (%s) ... ", name, stringof(type));
	
	SymTab* tab = get(symtab);
	SymTabNode* node = symtab_search(tab, name);
	
	if(node)
	{
		char const* fmt = "error: symbol already exists (previously defined as %s)\n";
		printf(fmt, stringof(node->type));
		exit(3);
	}
	
	SymTabNode* new = malloc(sizeof(SymTabNode));
	new->next = tab->head;
	new->name = name;
	new->type = type;
	tab->head = new;
	
	printf("ok\n");
}

void symtab_require(void* symtab, char const* name, SymType type)
{
	printf("checking symbol %s (%s) ... ", name, stringof(type));
	SymTabNode* sym = symtab_search(get(symtab), name);
	
	if(!sym)
	{
		printf("error: symbol not found\n");
		exit(3);
	}
	
	if(sym->type != type)
	{
		char const* fmt = "error: symbol kind does not match: expected %s, got %s\n";
		printf(fmt, stringof(type), stringof(sym->type));
		exit(3);
	}
	
	printf("ok\n");
}
