#pragma once

typedef enum
{
	LABEL,
	LOCAL,
} SymType;

void* symtab_create();
void symtab_insert(void* symtab, char const* name, SymType type);
void symtab_require(void* symtab, char const* name, SymType type);
