package at.ac.tuwien.inso.sepm.ticketline.client;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.client.rest.EventRestClient;
import at.ac.tuwien.inso.sepm.ticketline.client.rest.implementation.SimpleEventRestClient;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EventRestClientTest
{
	@Test(expected = NullPointerException.class)
	public void noValidRestClient() throws DataAccessException
	{
		EventRestClient faultyClient = new SimpleEventRestClient(null);
		faultyClient.findAll();
	}
}
