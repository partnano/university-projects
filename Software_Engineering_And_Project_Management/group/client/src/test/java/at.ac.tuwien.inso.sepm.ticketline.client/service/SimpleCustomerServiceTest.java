package at.ac.tuwien.inso.sepm.ticketline.client.service;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.client.exception.ValidationException;
import at.ac.tuwien.inso.sepm.ticketline.client.rest.CustomerRestClient;
import at.ac.tuwien.inso.sepm.ticketline.client.service.implementation.SimpleCustomerService;
import at.ac.tuwien.inso.sepm.ticketline.rest.customer.CustomerDTO;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SimpleCustomerServiceTest
{
	@MockBean
	private CustomerRestClient client;

	@Autowired
	private SimpleCustomerService service;

	private static final Long ID = 1L;
	private static final String firstName = "Firstname";
	private static final String lastName = "Lastname";
	private static final String valid_email = "test@mail.com";
	private static final String invalid_email = "invalid";

	@Test
	public void retrieveCustomers() throws DataAccessException
	{
		CustomerDTO customer = new CustomerDTO();
		customer.setFirstName(firstName);
		customer.setLastName(lastName);
		customer.setEmail(valid_email);

		List<CustomerDTO> list = new ArrayList<>();
		list.add(customer);

		BDDMockito.given(client.findAll()).willReturn(list);

		Assertions.assertThat(service.findAll()).isEqualTo(list);
	}

	@Test
	public void retrieveCustomer() throws DataAccessException, ValidationException
	{
		CustomerDTO customer = new CustomerDTO();
		customer.setFirstName(firstName);
		customer.setLastName(lastName);
		customer.setEmail(valid_email);

		BDDMockito.given(client.findOne(Mockito.anyLong())).willReturn(customer);

		Assertions.assertThat(service.findOne(1L)).isEqualTo(customer);
	}

	@Test
	public void createCustomer_success() throws DataAccessException, ValidationException
	{
		CustomerDTO customer = new CustomerDTO();
		customer.setFirstName(firstName);
		customer.setLastName(lastName);
		customer.setEmail(valid_email);

		BDDMockito.given(client.create(Mockito.any(CustomerDTO.class))).willReturn(customer);
		CustomerDTO result = service.create(customer);
		assertThat(result, is(customer));
	}

	@Test(expected = ValidationException.class)
	public void createCustomer_emptyFirstName() throws DataAccessException, ValidationException
	{
		CustomerDTO customer = new CustomerDTO();
		customer.setFirstName("");
		customer.setLastName(lastName);
		customer.setEmail(valid_email);

		service.create(customer);
	}

	@Test(expected = ValidationException.class)
	public void createCustomer_emptyLastName() throws DataAccessException, ValidationException
	{
		CustomerDTO customer = new CustomerDTO();
		customer.setFirstName(firstName);
		customer.setLastName("");
		customer.setEmail(valid_email);

		service.create(customer);
	}

	@Test(expected = ValidationException.class)
	public void createCustomer_emptyEmail() throws DataAccessException, ValidationException
	{
		CustomerDTO customer = new CustomerDTO();
		customer.setFirstName(firstName);
		customer.setLastName(lastName);
		customer.setEmail("");

		service.create(customer);
	}

	@Test(expected = ValidationException.class)
	public void createCustomer_invalidEmail() throws DataAccessException, ValidationException
	{
		CustomerDTO customer = new CustomerDTO();
		customer.setFirstName(firstName);
		customer.setLastName(lastName);
		customer.setEmail(invalid_email);

		service.create(customer);
	}

	//----------------------
	@Test
	public void updateCustomer_success() throws DataAccessException, ValidationException
	{
		CustomerDTO customer = new CustomerDTO();
		customer.setId(ID);
		customer.setFirstName(firstName);
		customer.setLastName(lastName);
		customer.setEmail(valid_email);

		BDDMockito.given(client.update(Mockito.any(CustomerDTO.class))).willReturn(customer);
		CustomerDTO result = service.update(customer);
		assertThat(result, is(customer));
	}

	@Test(expected = ValidationException.class)
	public void updateCustomer_emptyFirstName() throws DataAccessException, ValidationException
	{
		CustomerDTO customer = new CustomerDTO();
		customer.setId(ID);
		customer.setFirstName("");
		customer.setLastName(lastName);
		customer.setEmail(valid_email);

		service.update(customer);
	}

	@Test(expected = ValidationException.class)
	public void updateCustomer_emptyLastName() throws DataAccessException, ValidationException
	{
		CustomerDTO customer = new CustomerDTO();
		customer.setId(ID);
		customer.setFirstName(firstName);
		customer.setLastName("");
		customer.setEmail(valid_email);

		service.update(customer);
	}

	@Test(expected = ValidationException.class)
	public void updateCustomer_emptyEmail() throws DataAccessException, ValidationException
	{
		CustomerDTO customer = new CustomerDTO();
		customer.setId(ID);
		customer.setFirstName(firstName);
		customer.setLastName(lastName);
		customer.setEmail("");

		service.update(customer);
	}

	@Test(expected = ValidationException.class)
	public void updateCustomer_invalidEmail() throws DataAccessException, ValidationException
	{
		CustomerDTO customer = new CustomerDTO();
		customer.setId(ID);
		customer.setFirstName(firstName);
		customer.setLastName(lastName);
		customer.setEmail(invalid_email);

		service.update(customer);
	}
}
