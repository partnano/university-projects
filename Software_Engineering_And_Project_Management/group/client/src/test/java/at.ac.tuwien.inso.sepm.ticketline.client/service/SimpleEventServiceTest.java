package at.ac.tuwien.inso.sepm.ticketline.client.service;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.client.exception.ValidationException;
import at.ac.tuwien.inso.sepm.ticketline.client.rest.EventRestClient;
import at.ac.tuwien.inso.sepm.ticketline.client.service.implementation.SimpleEventService;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventAreaDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventFilterDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.enums.Category;
import at.ac.tuwien.inso.sepm.ticketline.rest.location.AreaDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.location.LocationDTO;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SimpleEventServiceTest
{
	@MockBean
	private EventRestClient client;

	@Autowired
	private SimpleEventService service;

	private static final String name = "Testevent";
	private static final LocalDateTime time = LocalDateTime.now();
	private static final int duration = 40;
	private static final String artist = "Testartist";
	private static final Category category = Category.CONCERT;
	private static final LocationDTO location = LocationDTO.builder()
	                                                       .name("Testlocation")
	                                                       .areas(new HashSet<>())
	                                                       .build();

	@Test
	public void retrieveEvents() throws DataAccessException
	{
		EventDTO event = EventDTO.builder()
		                         .name(name)
		                         .duration(duration)
		                         .time(time)
		                         .artist(artist)
		                         .category(category)
		                         .location(location)
		                         .build();

		List<EventDTO> list = new ArrayList<>();
		list.add(event);

		BDDMockito.given(client.findAll()).willReturn(list);

		Assertions.assertThat(service.findAll()).isEqualTo(list);
	}

	@Test
	public void retrieveFilteredEvents() throws DataAccessException
	{
		EventDTO event = EventDTO.builder()
		                         .name(name)
		                         .duration(duration)
		                         .time(time)
		                         .artist(artist)
		                         .category(category)
		                         .location(location)
		                         .build();

		List<EventDTO> list = new ArrayList<>();
		list.add(event);

		BDDMockito.given(client.findByFilter(Mockito.any(EventFilterDTO.class))).willReturn(list);

		Assertions.assertThat(service.findByFilter(new EventFilterDTO())).isEqualTo(list);
	}

	@Test
	public void createEvent_success() throws DataAccessException, ValidationException
	{
		EventDTO event = EventDTO.builder()
		                         .name(name)
		                         .duration(duration)
		                         .time(time)
		                         .artist(artist)
		                         .category(category)
		                         .location(location)
		                         .areas(new HashSet<>())
		                         .build();

		BDDMockito.given(client.create(Mockito.any())).willReturn(event);
		EventDTO result = service.create(event);
		assertThat(result, is(event));
	}

	@Test(expected = ValidationException.class)
	public void createCustomer_emptyName() throws DataAccessException, ValidationException
	{
		EventDTO event = EventDTO.builder()
		                         .name("")
		                         .duration(duration)
		                         .time(time)
		                         .artist(artist)
		                         .category(category)
		                         .location(location)
		                         .areas(new HashSet<>())
		                         .build();

		service.create(event);
	}

	@Test(expected = ValidationException.class)
	public void createCustomer_emptyTime() throws DataAccessException, ValidationException
	{
		EventDTO event = EventDTO.builder()
		                         .name(name)
		                         .time(null)
		                         .duration(duration)
		                         .artist(artist)
		                         .category(category)
		                         .location(location)
		                         .areas(new HashSet<>())
		                         .build();

		service.create(event);
	}

	@Test(expected = ValidationException.class)
	public void createCustomer_emptyArtist() throws DataAccessException, ValidationException
	{
		EventDTO event = EventDTO.builder()
		                         .name(name)
		                         .time(time)
		                         .duration(duration)
		                         .artist("")
		                         .category(category)
		                         .location(location)
		                         .areas(new HashSet<>())
		                         .build();

		service.create(event);
	}

	@Test(expected = ValidationException.class)
	public void createCustomer_emptyCategory() throws DataAccessException, ValidationException
	{
		EventDTO event = EventDTO.builder()
		                         .name(name)
		                         .time(time)
		                         .artist(artist)
		                         .category(null)
		                         .location(location)
		                         .areas(new HashSet<>())
		                         .build();

		service.create(event);
	}

	@Test(expected = ValidationException.class)
	public void createCustomer_emptyLocation() throws DataAccessException, ValidationException
	{
		EventDTO event = EventDTO.builder()
		                         .name(name)
		                         .duration(duration)
		                         .time(time)
		                         .artist(artist)
		                         .category(category)
		                         .location(null)
		                         .areas(new HashSet<>())
		                         .build();

		service.create(event);
	}

	@Test(expected = ValidationException.class)
	public void createCustomer_emptyDuration() throws DataAccessException, ValidationException
	{
		EventDTO event = EventDTO.builder()
		                         .name(name)
		                         .duration(null)
		                         .artist(artist)
		                         .category(category)
		                         .location(location)
		                         .areas(new HashSet<>())
		                         .build();

		service.create(event);
	}

	@Test(expected = ValidationException.class)
	public void createCustomer_duplicateAreas() throws DataAccessException, ValidationException
	{
		AreaDTO area = AreaDTO.builder().orderIndex(1L).build();

		Set<EventAreaDTO> eventAreas = new HashSet<>();
		eventAreas.add(EventAreaDTO.builder().area(area).price(1f).build());
		eventAreas.add(EventAreaDTO.builder().area(area).price(2f).build());

		EventDTO event = EventDTO.builder()
		                         .name(name)
		                         .duration(null)
		                         .artist(artist)
		                         .category(category)
		                         .location(location)
		                         .areas(eventAreas)
		                         .build();

		service.create(event);
	}

	@Test(expected = ValidationException.class)
	public void createCustomer_missingAreas() throws DataAccessException, ValidationException
	{
		Set<AreaDTO> areas = new HashSet<>();
		areas.add(AreaDTO.builder().build());
		LocationDTO location = LocationDTO.builder().build();
		location.setAreas(areas);

		EventDTO event = EventDTO.builder()
		                         .name(name)
		                         .duration(null)
		                         .artist(artist)
		                         .category(category)
		                         .location(location)
		                         .areas(new HashSet<>())
		                         .build();

		service.create(event);
	}
}
