package at.ac.tuwien.inso.sepm.ticketline.client.service;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.client.rest.LocationRestClient;
import at.ac.tuwien.inso.sepm.ticketline.client.service.implementation.SimpleLocationService;
import at.ac.tuwien.inso.sepm.ticketline.rest.location.LocationDTO;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SimpleLocationServiceTest
{
	@MockBean
	private LocationRestClient client;

	@Autowired
	private SimpleLocationService service;

	private static final String LOCATION_NAME = "Test Location";
	private static final String LOCATION_STREET = "Test Street";
	private static final String LOCATION_CITY = "Test City";
	private static final String LOCATION_ZIP = "1234";
	private static final String LOCATION_COUNTRY = "Test Country";

	@Test
	public void retrieveLocations() throws DataAccessException
	{
		LocationDTO location = LocationDTO.builder()
		                                  .name(LOCATION_NAME)
		                                  .street(LOCATION_STREET)
		                                  .city(LOCATION_CITY)
		                                  .zip(LOCATION_ZIP)
		                                  .country(LOCATION_COUNTRY)
		                                  .build();

		List<LocationDTO> list = new ArrayList<>();
		list.add(location);

		BDDMockito.given(client.findAll()).willReturn(list);

		Assertions.assertThat(service.findAll()).isEqualTo(list);
	}
}
