package at.ac.tuwien.inso.sepm.ticketline.client;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.client.exception.InvalidCSVException;
import at.ac.tuwien.inso.sepm.ticketline.client.service.EventService;
import at.ac.tuwien.inso.sepm.ticketline.client.service.LocationService;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.enums.Category;
import at.ac.tuwien.inso.sepm.ticketline.rest.location.AreaDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.location.LocationDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.location.RowDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.location.SectionDTO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.LocalDateTime;
import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest

public class EventCSVParserTest
{
	@Autowired
	private EventService eventService;

	@MockBean
	private LocationService locationService;

	@Test
	public void successfulRowCSVParsing() throws DataAccessException, InvalidCSVException
	{
		Set<AreaDTO> rows = new HashSet<>();
		for(int i = 0; i < 5; ++i)
		{
			rows.add(RowDTO.builder().orderIndex(i + 1L).build());
		}

		BDDMockito.given(locationService.findAll())
		          .willReturn(Collections.singletonList(LocationDTO.builder()
		                                                           .name("Test Location")
		                                                           .areas(rows)
		                                                           .build()));

		List<EventDTO> dtoList = eventService.parse(getClass().getClassLoader()
		                                                      .getResource("test_csv/valid_row.csv")
		                                                      .getPath());

		EventDTO dto = dtoList.get(0);
		assertThat(dto).isNotNull();
		assertThat(dto.getName()).isEqualTo("Test Concert");
		assertThat(dto.getArtist()).isEqualTo("Test Artist");
		assertThat(dto.getCategory()).isEqualTo(Category.CONCERT);
		assertThat(dto.getDuration()).isEqualTo(40);
		assertThat(dto.getLocation()).isNotNull();
		assertThat(dto.getTime()).isEqualTo(LocalDateTime.of(2017, 10, 22, 14, 0));
	}

	@Test
	public void successfulSectionCSVParsing() throws DataAccessException, InvalidCSVException
	{
		Set<AreaDTO> sections = new HashSet<>();
		sections.add(SectionDTO.builder().id(1L).name("a").build());
		sections.add(SectionDTO.builder().id(2L).name("b").build());
		sections.add(SectionDTO.builder().id(3L).name("c").build());
		sections.add(SectionDTO.builder().id(4L).name("d").build());

		BDDMockito.given(locationService.findAll())
		          .willReturn(Collections.singletonList(LocationDTO.builder()
		                                                           .name("Test Location")
		                                                           .areas(sections)
		                                                           .build()));

		List<EventDTO> dtoList = eventService.parse(getClass().getClassLoader()
		                                                      .getResource("test_csv/valid_section.csv")
		                                                      .getPath());

		EventDTO dto = dtoList.get(0);
		assertThat(dto).isNotNull();
		assertThat(dto.getName()).isEqualTo("Test Concert");
		assertThat(dto.getArtist()).isEqualTo("Test Artist");
		assertThat(dto.getCategory()).isEqualTo(Category.CONCERT);
		assertThat(dto.getDuration()).isEqualTo(40);
		assertThat(dto.getLocation()).isNotNull();
		assertThat(dto.getTime()).isEqualTo(LocalDateTime.of(2017, 10, 22, 14, 0));
	}

	@Test
	public void successfulMultiCSVParsing() throws DataAccessException, InvalidCSVException
	{
		Set<AreaDTO> rows = new HashSet<>();
		for(int i = 0; i < 5; ++i)
		{
			rows.add(RowDTO.builder().orderIndex(i + 1L).build());
		}

		Set<AreaDTO> sections = new HashSet<>();
		sections.add(SectionDTO.builder().id(1L).name("a").build());
		sections.add(SectionDTO.builder().id(2L).name("b").build());
		sections.add(SectionDTO.builder().id(3L).name("c").build());
		sections.add(SectionDTO.builder().id(4L).name("d").build());

		List<LocationDTO> locations = new ArrayList<>();
		locations.add(LocationDTO.builder().name("Test Location 1").areas(sections).build());
		locations.add(LocationDTO.builder().name("Test Location 2").areas(rows).build());

		BDDMockito.given(locationService.findAll()).willReturn(locations);

		List<EventDTO> dtoList = eventService.parse(getClass().getClassLoader()
		                                                      .getResource("test_csv/valid_multiple.csv")
		                                                      .getPath());

		assertThat(dtoList).isNotNull();
		assertThat(dtoList.size()).isEqualTo(3);
	}

	@Test(expected = InvalidCSVException.class)
	public void notAllRequiredFields() throws DataAccessException, InvalidCSVException
	{
		eventService.parse(getClass().getClassLoader().getResource("test_csv/missing_fields.csv").getPath());
	}
}
