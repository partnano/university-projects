package at.ac.tuwien.inso.sepm.ticketline.client;

import at.ac.tuwien.inso.sepm.ticketline.client.util.PDFReceiptGenerator;
import at.ac.tuwien.inso.sepm.ticketline.rest.booking.BookingDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.booking.ReceiptDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.booking.SeatBookingDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.customer.CustomerDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventAreaDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.location.AreaDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.location.LocationDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.location.RowDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.location.SeatDTO;
import org.assertj.core.api.Assertions;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PDFGeneratorTest
{
	@Rule
	public TemporaryFolder folder = new TemporaryFolder();

	@Test
	public void shouldCreateConfirmationPDFOnValidBooking() throws PDFReceiptGenerator.PDFCreationException, IOException
	{
		BookingDTO booking = generateCompleteBooking();

		final PDFReceiptGenerator pdfReceiptGenerator = new PDFReceiptGenerator(PDFReceiptGenerator.ReceiptType.CONFIRMATION,
		                                                                        booking);

		File f = new File(folder.getRoot(), "test.pdf");

		Assertions.assertThat(f.exists()).isFalse();

		pdfReceiptGenerator.generateReceipt(f);

		Assertions.assertThat(f.exists()).isTrue();
	}

	@Test
	public void shouldCreateCancellationPDFOnValidBooking() throws PDFReceiptGenerator.PDFCreationException, IOException
	{
		BookingDTO booking = generateCompleteBooking();

		final PDFReceiptGenerator pdfReceiptGenerator = new PDFReceiptGenerator(PDFReceiptGenerator.ReceiptType.CANCELLATION,
		                                                                        booking);

		File f = new File(folder.getRoot(), "test_cancellation.pdf");

		Assertions.assertThat(f.exists()).isFalse();

		pdfReceiptGenerator.generateReceipt(f);

		Assertions.assertThat(f.exists()).isTrue();
	}

	@Test(expected = AssertionError.class)
	public void shouldFailToCreatePDFOnBookingWithoutEvent() throws PDFReceiptGenerator.PDFCreationException,
	                                                                IOException
	{
		BookingDTO booking = generateCompleteBooking();

		// remove event
		booking.setEvent(null);

		final PDFReceiptGenerator pdfReceiptGenerator = new PDFReceiptGenerator(PDFReceiptGenerator.ReceiptType.CONFIRMATION,
		                                                                        booking);
	}

	private BookingDTO generateCompleteBooking()
	{
		LocationDTO location = new LocationDTO();
		location.setName("Location");
		location.setId(1L);

		EventDTO event = new EventDTO();
		event.setName("bla");
		event.setLocation(location);
		event.setTime(LocalDateTime.now());
		event.setId(1L);

		CustomerDTO customer = new CustomerDTO();
		customer.setFirstName("bla");
		customer.setLastName("bla");
		customer.setCustomerNumber(1L);

		Set<EventAreaDTO> eventAreas = new HashSet<>();

		RowDTO row = new RowDTO();
		row.setId(1L);
		row.setLocation(location);
		row.setName("1");
		row.setOrderIndex(1L);

		SeatDTO seat = new SeatDTO();
		seat.setId(1L);
		seat.setName("A");
		seat.setOrderIndex(1L);
		seat.setRow(row);

		Set<SeatDTO> seats = new HashSet<>();
		seats.add(seat);

		Set<AreaDTO> rows = new HashSet<>();
		rows.add(row);

		EventAreaDTO eventArea = new EventAreaDTO();
		eventArea.setArea(row);
		eventArea.setPrice(100f);
		eventArea.setEvent(event);

		eventAreas.add(eventArea);

		row.setSeats(seats);
		location.setAreas(rows);
		event.setEventAreas(eventAreas);

		Set<BookingDTO> bookings = new HashSet<>();

		SeatBookingDTO booking = new SeatBookingDTO();
		booking.setId(11L);
		booking.setReservationNumber(11);
		booking.setCreatedAt(LocalDateTime.now());
		booking.setPaid(true);
		booking.setBookedAt(LocalDateTime.now());
		booking.setPaymentMethod("CASH");
		booking.setSeats(seats);
		booking.setEvent(event);
		booking.setCustomer(customer);

		bookings.add(booking);

		ReceiptDTO receipt = new ReceiptDTO();
		receipt.setId(1L);
		receipt.setCreatedAt(LocalDateTime.now());

		booking.setPaymentReceipt(receipt);
		booking.setCancellationReceipt(receipt);

		event.setBookings(bookings);

		return booking;
	}
}
