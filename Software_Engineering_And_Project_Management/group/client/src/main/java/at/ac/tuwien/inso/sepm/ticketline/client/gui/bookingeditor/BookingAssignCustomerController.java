package at.ac.tuwien.inso.sepm.ticketline.client.gui.bookingeditor;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.client.gui.MiscAlerts;
import at.ac.tuwien.inso.sepm.ticketline.client.gui.customers.AddCustomerController;
import at.ac.tuwien.inso.sepm.ticketline.client.service.CustomerService;
import at.ac.tuwien.inso.sepm.ticketline.client.util.BundleManager;
import at.ac.tuwien.inso.sepm.ticketline.client.util.JavaFXUtils;
import at.ac.tuwien.inso.sepm.ticketline.rest.customer.CustomerDTO;
import at.ac.tuwien.inso.springfx.SpringFxmlLoader;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.function.Consumer;

@Component
public class BookingAssignCustomerController
{
	private static final Logger LOGGER = LoggerFactory.getLogger(BookingAssignCustomerController.class);

	@FXML
	private TableView<CustomerDTO> ui_customerTable;

	@FXML
	private TableColumn<CustomerDTO, Long> ui_customerNumberColumn;

	@FXML
	private TableColumn<CustomerDTO, String> ui_nameColumn;

	@Autowired
	private final CustomerService service;

	private Stage dialog;
	private Consumer<CustomerDTO> onCustomerSelected;
	private SpringFxmlLoader springFxmlLoader;

	public BookingAssignCustomerController(CustomerService service)
	{
		this.service = service;
	}

	@FXML
	private void initialize()
	{
		ui_customerNumberColumn.setCellValueFactory(data -> new SimpleObjectProperty<>(data.getValue().getCustomerNumber()));
		ui_nameColumn.setCellValueFactory(data -> new SimpleObjectProperty<>(
			data.getValue().getFirstName() + " " + data.getValue().getLastName()));
	}

	@FXML
	private void ui_assignCustomerClicked()
	{
		LOGGER.info("assign customer button clicked");

		if(ui_customerTable.getSelectionModel().isEmpty())
		{
			JavaFXUtils.createErrorDialog("You have to select a customer", dialog).showAndWait();
			return;
		}

		onCustomerSelected.accept(ui_customerTable.getSelectionModel().getSelectedItem());
		dialog.close();
	}

	@FXML
	private void ui_assignAnonymousClicked()
	{
		LOGGER.info("assign anonymous button clicked");
		try
		{
			CustomerDTO customerDTO = service.createAnonymous(new CustomerDTO());
			onCustomerSelected.accept(customerDTO);
			dialog.close();
		}
		catch(DataAccessException ex)
		{
			LOGGER.error("creating anonymous customer failed: {}", ex.getMessage());
			MiscAlerts.showDataAccessAlert();
		}
	}

	@FXML
	private void ui_cancelClicked()
	{
		LOGGER.info("cancel button clicked");
		dialog.close();
	}

	public static BookingAssignCustomerController create(Stage parent, SpringFxmlLoader springFxmlLoader,
	                                                     Consumer<CustomerDTO> onCustomerSelected)
	{
		SpringFxmlLoader.Wrapper<BookingAssignCustomerController> wrapper = springFxmlLoader.loadAndWrap(
			"/fxml/bookingeditor/assignCustomer.fxml");

		Stage dialog = new Stage();
		dialog.setResizable(false);
		dialog.initModality(Modality.APPLICATION_MODAL);
		dialog.initOwner(parent);
		dialog.setScene(new Scene((Parent)wrapper.getLoadedObject()));
		dialog.setTitle(BundleManager.getBundle().getString("bookingeditor.assignCustomer.title"));

		dialog.addEventHandler(KeyEvent.KEY_RELEASED, (KeyEvent event) ->
		{
			if(KeyCode.ESCAPE == event.getCode())
			{
				dialog.close();
			}
		});

		BookingAssignCustomerController controller = wrapper.getController();
		controller.setSpringFxmlLoader(springFxmlLoader);
		controller.dialog = dialog;
		controller.onCustomerSelected = onCustomerSelected;
		return controller;
	}

	private void loadContent()
	{
		try
		{
			ObservableList<CustomerDTO> allCustomers = FXCollections.observableArrayList(service.findAll());
			allCustomers.removeIf(customerDTO -> customerDTO.getFirstName() == null);

			ui_customerTable.setItems(allCustomers);
		}
		catch(DataAccessException ex)
		{
			JavaFXUtils.createExceptionDialog(ex, dialog);
		}
	}

	public void show()
	{
		loadContent();
		dialog.showAndWait();
	}

	public void ui_newCustomerClicked()
	{
		AddCustomerController.create((Stage)ui_customerTable.getScene().getWindow(), springFxmlLoader, service);
		loadContent();
	}

	private void setSpringFxmlLoader(SpringFxmlLoader springFxmlLoader)
	{
		this.springFxmlLoader = springFxmlLoader;
	}
}
