package at.ac.tuwien.inso.sepm.ticketline.client.rest;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.rest.news.DetailedNewsDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.news.SimpleNewsDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.page.RestResponsePage;

import java.util.List;

public interface NewsRestClient
{
	/**
	 * Find all news entries.
	 *
	 * @return list of news entries
	 * @throws DataAccessException in case something went wrong
	 */
	List<SimpleNewsDTO> findAll() throws DataAccessException;

	/**
	 * Find page of all news entries.
	 *
	 * @return page of all news entries
	 * @throws DataAccessException in case something went wrong
	 */
	RestResponsePage<SimpleNewsDTO> findAll(int page, int size) throws DataAccessException;

	/**
	 * Find page of unread news entries.
	 *
	 * @return page of unread news entries
	 * @throws DataAccessException in case something went wrong
	 */
	RestResponsePage<SimpleNewsDTO> findUnread(int page, int size) throws DataAccessException;

	/**
	 * Publish a new news entry. Only works as admin.
	 *
	 * @param news news to be created.
	 * @return created news
	 * @throws DataAccessException in case something goes wrong (i.e. user is no admin)
	 */
	DetailedNewsDTO publishNews(DetailedNewsDTO news) throws DataAccessException;

	/**
	 * Get a detailed version of the news entry given
	 *
	 * @param news SimpleNewsDTO where DetailedNewsDTO is to be found
	 * @return DetailedNewsDTO
	 * @throws DataAccessException in case something goes wrong
	 */
	DetailedNewsDTO findOne(SimpleNewsDTO news) throws DataAccessException;

	/**
	 * Update the given newsDTO
	 *
	 * @param newsDTO to be updated
	 * @return updated newsDTO
	 * @throws DataAccessException in case something goes wrong
	 */
	DetailedNewsDTO update(DetailedNewsDTO newsDTO) throws DataAccessException;
}
