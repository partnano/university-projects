package at.ac.tuwien.inso.sepm.ticketline.client.validation;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.ValidationException;
import at.ac.tuwien.inso.sepm.ticketline.rest.customer.CustomerDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CustomerValidator
{
	private static final Logger LOGGER = LoggerFactory.getLogger(CustomerValidator.class);

	public static void validate(CustomerDTO customer) throws ValidationException
	{
		ValidationException ex = new ValidationException();

		if(customer == null)
			ex.addMessage("customers.empty");
		else
		{
			if(customer.getFirstName() == null || customer.getFirstName().trim().isEmpty())
				ex.addMessage("customers.firstNameEmpty");

			if(customer.getLastName() == null || customer.getLastName().trim().isEmpty())
				ex.addMessage("customers.lastNameEmpty");

			if(customer.getEmail() == null || customer.getEmail().trim().isEmpty())
				ex.addMessage("customers.emailEmpty");

			else if(!customer.getEmail().contains("@"))
				ex.addMessage("customers.emailNoAt");
		}

		if(ex.hasMessage())
		{
			LOGGER.info("Customer validation failed: {}", ex);
			throw ex;
		}
	}

	public static void validateId(Long id) throws ValidationException
	{
		ValidationException ex = new ValidationException();

		if(id == null)
			ex.addMessage("customers.idEmpty");

		else if(id < 1)
			ex.addMessage("customers.idNegative");

		if(ex.hasMessage())
		{
			LOGGER.info("Customer id validation failed: {}", ex);
			throw ex;
		}
	}
}
