package at.ac.tuwien.inso.sepm.ticketline.client.util;

public enum SeatStatus
{
	AVAILABLE,
	RESERVED,
	BOOKED
}
