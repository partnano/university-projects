package at.ac.tuwien.inso.sepm.ticketline.client.service;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.client.exception.ValidationException;
import at.ac.tuwien.inso.sepm.ticketline.rest.booking.BookingDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.customer.CustomerDTO;

import java.util.List;

public interface BookingService
{
	/**
	 * Create a new booking
	 *
	 * @param booking to be created
	 * @return created booking
	 * @throws DataAccessException in case something goes wrong
	 */
	BookingDTO create(BookingDTO booking) throws ValidationException, DataAccessException;

	/**
	 * Update a booking
	 *
	 * @param booking to be updated
	 * @return updated booking
	 * @throws DataAccessException in case something goes wrong
	 */
	BookingDTO update(BookingDTO booking) throws ValidationException, DataAccessException;

	/**
	 * Retrieves booking to specific reservationNumber
	 *
	 * @param reservationNumber the number to be used to look up the bookings
	 * @return matched booking
	 */
	BookingDTO findByReservationNumber(int reservationNumber) throws ValidationException, DataAccessException;

	/**
	 * Retrieves all bookings of the specified customer
	 *
	 * @param customer the customer to be used to look up the bookings
	 * @return all matching bookings
	 */
	List<BookingDTO> findAll(CustomerDTO customer) throws DataAccessException;

	/**
	 * Delete a booking
	 *
	 * @param bookingDTO to be deleted
	 * @return canceled booking
	 * @throws DataAccessException in case something goes wrong
	 */
	BookingDTO delete(BookingDTO bookingDTO) throws ValidationException, DataAccessException;

	/**
	 * Load event into a booking
	 *
	 * @param bookingDTO bookingDTO to load event in
	 * @throws DataAccessException in case something goes wrong
	 */
	void populateBookingEvent(BookingDTO bookingDTO) throws DataAccessException;
}
