package at.ac.tuwien.inso.sepm.ticketline.client.validation;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.ValidationException;
import at.ac.tuwien.inso.sepm.ticketline.rest.payment.CreditCardDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CreditCardValidator
{
	private static final Logger LOGGER = LoggerFactory.getLogger(CreditCardValidator.class);

	public static void validate(CreditCardDTO creditCardDTO) throws ValidationException
	{
		ValidationException ex = new ValidationException();

		if(creditCardDTO == null)
			ex.addMessage("card.empty");

		else
		{
			if(creditCardDTO.getCardNumber() == null || creditCardDTO.getCardNumber().trim().isEmpty())
				ex.addMessage("card.number.empty");

			if(creditCardDTO.getExpirationMonth() == null)
				ex.addMessage("card.exp.month.empty");

			else if(creditCardDTO.getExpirationMonth() < 1 || 12 < creditCardDTO.getExpirationMonth())
				ex.addMessage("card.exp.month.invalid");

			if(creditCardDTO.getExpirationYear() == null)
				ex.addMessage("card.exp.year.empty");

			else if(creditCardDTO.getExpirationYear() < 0)
				ex.addMessage("card.exp.year.invalid");

			if(creditCardDTO.getCvc() == null)
				ex.addMessage("card.cvc.empty");

			else if(creditCardDTO.getCvc() < 100 || 9999 < creditCardDTO.getCvc())
				ex.addMessage("card.cvc.invalid");

			if(ex.hasMessage())
			{
				LOGGER.info("Credit card validation failed: {}", ex);
				throw ex;
			}
		}
	}
}
