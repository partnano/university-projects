package at.ac.tuwien.inso.sepm.ticketline.client.gui.payment;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.client.exception.ValidationException;
import at.ac.tuwien.inso.sepm.ticketline.client.gui.MiscAlerts;
import at.ac.tuwien.inso.sepm.ticketline.client.service.PaymentService;
import at.ac.tuwien.inso.sepm.ticketline.client.service.PaymentServiceFactory;
import at.ac.tuwien.inso.sepm.ticketline.client.util.BundleManager;
import at.ac.tuwien.inso.sepm.ticketline.rest.booking.BookingDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.payment.CreditCardDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.payment.PaymentProvider;
import at.ac.tuwien.inso.springfx.SpringFxmlLoader;
import javafx.collections.FXCollections;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.StringConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.function.Consumer;

@Component
public class PaymentController
{
	public ComboBox<PaymentProvider> ui_paymentProviderDropdown;
	public GridPane ui_cardForm_pane;
	public TextField ui_cardnumberInput;
	public TextField ui_expmonthInput;
	public TextField ui_expyearInput;
	public TextField ui_cvcInput;
	public Label ui_paymentPrice;

	private Stage dialog;
	private Consumer<BookingDTO> onBookingPaid;

	private BookingDTO booking;

	@Autowired
	private PaymentServiceFactory paymentServiceFactory;

	public void initialize()
	{
		ui_paymentProviderDropdown.setItems(FXCollections.observableArrayList(PaymentProvider.values()));
		ui_paymentProviderDropdown.setConverter(new StringConverter<>()
		{
			@Override
			public String toString(PaymentProvider paymentProvider)
			{
				if(paymentProvider == null)
				{
					return null;
				}
				else
				{
					return BundleManager.getBundle().getString(paymentProvider.getName());
				}
			}

			@Override
			public PaymentProvider fromString(String paymentName)
			{
				return PaymentProvider.valueOf(paymentName);
			}
		});
		ui_paymentProviderDropdown.getSelectionModel().selectFirst();
	}

	public void onProviderChanged()
	{
		PaymentService paymentService = paymentServiceFactory.getService(ui_paymentProviderDropdown.getSelectionModel()
		                                                                                           .getSelectedItem());
		ui_cardForm_pane.setVisible(paymentService.formNeeded());
	}

	public static PaymentController create(Stage parent, SpringFxmlLoader springFxmlLoader,
	                                       Consumer<BookingDTO> onBookingPaid)
	{
		SpringFxmlLoader.Wrapper<PaymentController> wrapper = springFxmlLoader.loadAndWrap(
			"/fxml/payment/paymentWindow.fxml");

		Stage dialog = new Stage();
		dialog.setResizable(false);
		dialog.initModality(Modality.APPLICATION_MODAL);
		dialog.initOwner(parent);
		dialog.setScene(new Scene((Parent)wrapper.getLoadedObject()));
		dialog.setTitle(BundleManager.getBundle().getString("bookingeditor.payment.title"));

		dialog.addEventHandler(KeyEvent.KEY_RELEASED, (KeyEvent event) ->
		{
			if(KeyCode.ESCAPE == event.getCode())
			{
				dialog.close();
			}
		});

		PaymentController controller = wrapper.getController();
		controller.dialog = dialog;
		controller.onBookingPaid = onBookingPaid;
		return controller;
	}

	public void setBooking(BookingDTO booking)
	{
		this.booking = booking;
	}

	public void onPayButtonClicked()
	{
		CreditCardDTO creditCardDTO = new CreditCardDTO();
		creditCardDTO.setCardNumber(ui_cardnumberInput.getText());
		try
		{
			creditCardDTO.setExpirationMonth(Integer.parseInt(ui_expmonthInput.getText()));
		}
		catch(NumberFormatException ex)
		{
			creditCardDTO.setExpirationMonth(null);
		}
		try
		{
			creditCardDTO.setExpirationYear(Integer.parseInt(ui_expyearInput.getText()));
		}
		catch(NumberFormatException ex)
		{
			creditCardDTO.setExpirationYear(null);
		}
		try
		{
			creditCardDTO.setCvc(Integer.parseInt(ui_cvcInput.getText()));
		}
		catch(NumberFormatException ex)
		{
			creditCardDTO.setCvc(null);
		}

		try
		{
			PaymentProvider provider = ui_paymentProviderDropdown.getSelectionModel().getSelectedItem();
			booking.setPaymentMethod(provider.name());
			// remove circle to be able to send to server
			booking.getEvent().setBookings(null);
			BookingDTO updatedBooking = paymentServiceFactory.getService(provider).pay(booking, creditCardDTO);
			onBookingPaid.accept(updatedBooking);
			dialog.close();
		}
		catch(ValidationException ex)
		{
			MiscAlerts.showValidationErrorAlert(ex);
		}
		catch(DataAccessException ex)
		{
			MiscAlerts.showDataAccessAlert();
		}
	}

	public void show()
	{
		clearInputs();
		ui_paymentPrice.setText(String.format("%.2f %s",
		                                      booking.totalPriceThroughEvent(),
		                                      BundleManager.getBundle().getString("currency")));
		dialog.showAndWait();
	}

	private void clearInputs()
	{
		ui_cardnumberInput.clear();
		ui_expmonthInput.clear();
		ui_expyearInput.clear();
		ui_cvcInput.clear();
	}
}
