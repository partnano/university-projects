package at.ac.tuwien.inso.sepm.ticketline.client.gui.users;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.client.exception.ValidationException;
import at.ac.tuwien.inso.sepm.ticketline.client.gui.MiscAlerts;
import at.ac.tuwien.inso.sepm.ticketline.client.gui.TabHeaderController;
import at.ac.tuwien.inso.sepm.ticketline.client.service.UserService;
import at.ac.tuwien.inso.sepm.ticketline.client.util.BundleManager;
import at.ac.tuwien.inso.sepm.ticketline.rest.user.UserDTO;
import at.ac.tuwien.inso.springfx.SpringFxmlLoader;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.Stage;
import javafx.util.Duration;
import org.controlsfx.glyphfont.FontAwesome;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class UserController
{
	public Button ui_addUserButton;
	public TableView<UserDTO> ui_userTable;
	public TableColumn<UserDTO, String> ui_nameColumn;
	public TableColumn<UserDTO, String> ui_roleColumn;
	public TableColumn<UserDTO, String> ui_statusColumn;
	public Button ui_editUserButton;
	public Button ui_lockUserButton;
	public Button ui_resetPasswordButton;
	private final FontAwesome fontAwesome;
	public Label ui_noUserSelectedLabel;
	public Button ui_unlockUserButton;

	@FXML
	private Button ui_firstButton;
	@FXML
	private Button ui_previousButton;
	@FXML
	private Button ui_nextButton;
	@FXML
	private Button ui_lastButton;
	@FXML
	private Label ui_paginationLabel;

	private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);
	private ObservableList<UserDTO> userList;

	private final UserService userService;
	private final SpringFxmlLoader loader;

	private int currentPage = 1;
	private static final int ELEMENTS_PER_PAGE = 50;

	@FXML
	private TabHeaderController tabHeaderController;

	@Autowired
	public UserController(FontAwesome fontAwesome, UserService userService, SpringFxmlLoader loader)
	{
		this.fontAwesome = fontAwesome;
		this.userService = userService;
		this.loader = loader;
	}

	@FXML
	private void initialize()
	{
		tabHeaderController.setIcon(FontAwesome.Glyph.MEH_ALT);
		tabHeaderController.setTitle(BundleManager.getBundle().getString("users.usersTitle"));
		LOGGER.debug("initializing");

		ui_noUserSelectedLabel.managedProperty().bind(ui_noUserSelectedLabel.visibleProperty());

		userList = FXCollections.observableArrayList();
		ui_userTable.setItems(userList);

		ui_nameColumn.setCellValueFactory(cellData -> new SimpleObjectProperty<>(
			cellData.getValue().getLastName() + " " + cellData.getValue().getFirstName()));

		ui_roleColumn.setCellValueFactory(cellData -> new SimpleObjectProperty<>(
			cellData.getValue().isAdmin() ? BundleManager.getBundle().getString("users.admin")
			                              : BundleManager.getBundle().getString("users.user")));

		ui_statusColumn.setCellValueFactory(cellData -> new SimpleObjectProperty<>(
			cellData.getValue().isLocked() ? BundleManager.getBundle().getString("users.status.locked")
			                               : BundleManager.getBundle().getString("users.status.active")));

		ui_statusColumn.setCellFactory(column -> new TableCell<>()
		{
			@Override
			protected void updateItem(String item, boolean empty)
			{
				super.updateItem(item, empty);

				if(item == null || empty)
				{
					setText(null);
					setStyle("");
				}

				else if(item.equals(BundleManager.getBundle().getString("users.status.locked")))
				{

					setStyle("-fx-background-color: #ff4f59; -fx-alignment: CENTER");
					setText(BundleManager.getBundle().getString("users.status.locked"));
				}
				else
				{
					setStyle("-fx-alignment: CENTER");
					setText(BundleManager.getBundle().getString("users.status.active"));
				}
			}
		});

		ui_userTable.getSelectionModel()
		            .selectedItemProperty()
		            .addListener((options, oldValue, newValue) -> {
			            if(newValue != null)
			            {
				            if(newValue.isLocked())
				            	ui_lockUserButton.setText(BundleManager.getBundle().getString("users.unlockUserButton"));
				            else
				            	ui_lockUserButton.setText(BundleManager.getBundle().getString("users.lockUser"));
			            }
		            });

		ui_nameColumn.setStyle("-fx-alignment: CENTER;");
		ui_statusColumn.setStyle("-fx-alignment: CENTER;");
		ui_roleColumn.setStyle("-fx-alignment: CENTER;");

		ui_firstButton.setText(BundleManager.getBundle().getString("pagination.first"));
		ui_previousButton.setGraphic(fontAwesome.create(FontAwesome.Glyph.CHEVRON_LEFT));
		ui_lastButton.setText(BundleManager.getBundle().getString("pagination.last"));
		ui_nextButton.setGraphic(fontAwesome.create(FontAwesome.Glyph.CHEVRON_RIGHT));
		ui_paginationLabel.setText("");

		//ui_paginationBox = new HBox(ui_firstButton, ui_previousButton, ui_paginationLabel, ui_nextButton, ui_lastButton);
		//LOGGER.debug(ui_paginationBox.getChildren()+"");
		//ui_paginationBox.getChildren().addAll(ui_firstButton, ui_previousButton, ui_paginationLabel, ui_nextButton, ui_lastButton);
		//loadUsers();

	}

	public void ui_addUserButtonClick()
	{
		Stage stage = (Stage)ui_userTable.getScene().getWindow();

		ModifyUserWindowController.showAndWait(stage,
		                                       loader,
		                                       BundleManager.getBundle().getString("users.addUser"),
		                                       null);
		loadUsers();
	}

	public void ui_editUserButtonClick()
	{
		if(ui_userTable.getSelectionModel().getSelectedItem() != null)
		{

			Stage stage = (Stage)ui_userTable.getScene().getWindow();
			ModifyUserWindowController.showAndWait(stage,
			                                       loader,
			                                       BundleManager.getBundle().getString("users.editUser"),
			                                       ui_userTable.getSelectionModel().getSelectedItem());
			loadUsers();
		}
		else
		{
			ui_noUserSelectedLabel.setVisible(true);
			Timeline timeout = new Timeline(new KeyFrame(Duration.seconds(5),
			                                             event -> ui_noUserSelectedLabel.setVisible(false)));
			timeout.setCycleCount(1);
			timeout.play();
		}
	}

	public void onLockUserButtonClick()
	{
		UserDTO user = ui_userTable.getSelectionModel().getSelectedItem();

		if(user == null)
		{
			ui_noUserSelectedLabel.setVisible(true);
			Timeline timeout = new Timeline(new KeyFrame(Duration.seconds(5),
			                                             event -> ui_noUserSelectedLabel.setVisible(false)));
			timeout.setCycleCount(1);
			timeout.play();
			return;
		}

		Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
		alert.getButtonTypes().remove(0);

		boolean lock = !user.isLocked();

		if(lock)
		{
			ButtonType ok = new ButtonType(BundleManager.getBundle().getString("users.lockAlert.lock.button"), ButtonBar.ButtonData.OK_DONE);
			alert.getButtonTypes().add(0, ok);
			alert.setHeaderText(BundleManager.getBundle().getString("users.lockAlert.lock.header"));
			alert.setContentText(BundleManager.getBundle().getString("users.lockAlert.lock.content"));
		}
		else
		{
			ButtonType ok = new ButtonType(BundleManager.getBundle().getString("users.lockAlert.unlock.button"), ButtonBar.ButtonData.OK_DONE);
			alert.getButtonTypes().add(0, ok);
			alert.setHeaderText(BundleManager.getBundle().getString("users.lockAlert.unlock.header"));
			alert.setContentText(BundleManager.getBundle().getString("users.lockAlert.unlock.content"));
		}

		Optional<ButtonType> result = alert.showAndWait();
		if(result.isPresent() && result.get().getButtonData() == ButtonBar.ButtonData.OK_DONE)
		{
			user.setLocked(lock);

			try
			{
				userService.update(user);
				loadUsers();
			}
			catch(DataAccessException e)
			{
				MiscAlerts.showDataAccessAlert();
			}
			catch(ValidationException e)
			{
				MiscAlerts.showValidationErrorAlert(e);
			}
		}
	}

	public void onResetPasswordButtonClick()
	{
		UserDTO user = ui_userTable.getSelectionModel().getSelectedItem();

		if(user != null)
		{
			try
			{
				userService.resetPassword(user.getId());
			}
			catch(DataAccessException ex)
			{
				MiscAlerts.showDataAccessAlert();
			}
			catch(ValidationException ex)
			{
				MiscAlerts.showValidationErrorAlert(ex);
			}
		}
		else
		{
			ui_noUserSelectedLabel.setVisible(true);
			Timeline timeout = new Timeline(new KeyFrame(Duration.seconds(5),
			                                             event -> ui_noUserSelectedLabel.setVisible(false)));
			timeout.setCycleCount(1);
			timeout.play();
		}
	}

	public void loadUsers()
	{
		LOGGER.debug("Loading Users... ");

		try
		{
			Page<UserDTO> userDTOPage = userService.findAll(currentPage - 1, ELEMENTS_PER_PAGE);

			LOGGER.debug(userDTOPage.getContent().get(0).toString());

			int numPages = userDTOPage.getTotalPages();

			ui_paginationLabel.setText(currentPage + "/" + numPages);

			ui_firstButton.setOnAction(event -> {
				currentPage = 1;
				loadUsers();
			});

			ui_previousButton.setOnAction(event -> {
				if(currentPage > 1)
				{
					currentPage--;
				}
				loadUsers();
			});

			ui_lastButton.setOnAction(event -> {
				currentPage = numPages;
				loadUsers();
			});
			ui_nextButton.setOnAction(event -> {
				if(currentPage < numPages)
				{
					currentPage++;
				}
				loadUsers();
			});

			if(currentPage >= numPages)
			{
				ui_nextButton.setDisable(true);
				ui_lastButton.setDisable(true);
			}
			else
			{
				ui_nextButton.setDisable(false);
				ui_lastButton.setDisable(false);
			}

			if(currentPage <= 1)
			{
				ui_previousButton.setDisable(true);
				ui_firstButton.setDisable(true);
			}
			else
			{
				ui_previousButton.setDisable(false);
				ui_firstButton.setDisable(false);
			}

			userList.clear();
			userList.addAll(userDTOPage.getContent());
		}
		catch(DataAccessException e)
		{
			MiscAlerts.showDataAccessAlert();
		}
	}
}
