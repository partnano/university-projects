package at.ac.tuwien.inso.sepm.ticketline.client.gui;

import at.ac.tuwien.inso.sepm.ticketline.client.gui.customers.CustomerController;
import at.ac.tuwien.inso.sepm.ticketline.client.gui.events.EventsController;
import at.ac.tuwien.inso.sepm.ticketline.client.gui.news.NewsController;
import at.ac.tuwien.inso.sepm.ticketline.client.gui.top10events.Top10EventsController;
import at.ac.tuwien.inso.sepm.ticketline.client.gui.users.UserController;
import at.ac.tuwien.inso.sepm.ticketline.client.service.AuthenticationInformationService;
import at.ac.tuwien.inso.sepm.ticketline.client.util.BundleManager;
import at.ac.tuwien.inso.sepm.ticketline.rest.authentication.AuthenticationTokenInfo;
import at.ac.tuwien.inso.springfx.SpringFxmlLoader;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import org.controlsfx.glyphfont.FontAwesome;
import org.controlsfx.glyphfont.Glyph;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Locale;
import java.util.Optional;

@Component
public class MainController
{
	private static final Logger LOGGER = LoggerFactory.getLogger(MainController.class);
	private static final int TAB_ICON_FONT_SIZE = 20;

	@FXML
	private Menu ui_applicationMenu;

	@FXML
	private StackPane spMainContent;

	@FXML
	private ProgressBar pbLoadingProgress;

	@FXML
	private TabPane tpContent;

	@FXML
	private MenuBar mbMain;

	private Node login;

	private final SpringFxmlLoader loader;
	private final FontAwesome fontAwesome;

	private final AuthenticationInformationService authenticationInformationService;

	public MainController(SpringFxmlLoader loader, FontAwesome fontAwesome,
	                      AuthenticationInformationService authService)
	{
		this.loader = loader;
		this.fontAwesome = fontAwesome;
		this.authenticationInformationService = authService;

		authService.addAuthenticationChangeListener(this::loadMainUi);
	}

	@FXML
	private void initialize()
	{
		LOGGER.trace("MainController initializing ...");

		Platform.runLater(() -> mbMain.setUseSystemMenuBar(true));
		pbLoadingProgress.setProgress(0);
		login = loader.load("/fxml/authenticationComponent.fxml");
		spMainContent.getChildren().add(login);

		Menu languageMenu = new Menu(BundleManager.getBundle().getString("application.language"));

		RadioMenuItem englishLanguageItem = new RadioMenuItem(BundleManager.getBundle()
		                                                                   .getString("application.english"));
		englishLanguageItem.setSelected(true);
		englishLanguageItem.setUserData(Locale.ENGLISH);
		RadioMenuItem germanLanguageItem = new RadioMenuItem(BundleManager.getBundle().getString("application.german"));
		germanLanguageItem.setUserData(Locale.GERMAN);
		ToggleGroup languageToggle = new ToggleGroup();
		englishLanguageItem.setToggleGroup(languageToggle);
		germanLanguageItem.setToggleGroup(languageToggle);
		languageMenu.getItems().addAll(englishLanguageItem, germanLanguageItem);

		ui_applicationMenu.getItems().add(languageMenu);

		languageToggle.selectedToggleProperty().addListener((ov, old_toggle, new_toggle) ->
		                                                    {
			                                                    if(languageToggle.getSelectedToggle() != null)
			                                                    {
				                                                    onLanguageChanged((Locale)languageToggle.getSelectedToggle()
				                                                                                            .getUserData());
			                                                    }
		                                                    });
	}

	private void onLanguageChanged(Locale selectedLocale)
	{
		int currentTabIndex = tpContent.getSelectionModel().getSelectedIndex();
		LOGGER.info("locale changed to {}", selectedLocale.getDisplayName());
		BundleManager.changeLocale(selectedLocale);
		loadMainUi(authenticationInformationService.getCurrentAuthenticationTokenInfo().orElse(null));
		tpContent.getSelectionModel().select(currentTabIndex);
	}

	@FXML
	private void exitApplication()
	{
		LOGGER.trace("MainController exit application ...");

		Stage stage = (Stage) spMainContent.getScene().getWindow();
		stage.fireEvent(new WindowEvent(stage, WindowEvent.WINDOW_CLOSE_REQUEST));
	}

	@FXML
	private void aboutApplication()
	{
		LOGGER.trace("MainController aboutApplication ...");

		Stage stage = (Stage) spMainContent.getScene().getWindow();
		Stage dialog = new Stage();
		dialog.setResizable(false);
		dialog.initModality(Modality.APPLICATION_MODAL);
		dialog.initOwner(stage);
		dialog.setScene(new Scene(loader.load("/fxml/aboutDialog.fxml")));
		dialog.setTitle(BundleManager.getBundle().getString("dialog.about.title"));
		dialog.showAndWait();
	}

	private void initNewsTabPane()
	{
		LOGGER.trace("MainController initNewsPane ...");

		SpringFxmlLoader.Wrapper<NewsController> wrapper = loader.loadAndWrap("/fxml/news/newsComponent.fxml");
		NewsController newsController = wrapper.getController();
		newsController.loadNews();
		Tab newsTab = new Tab(null, wrapper.getLoadedObject());
		Glyph newsGlyph = fontAwesome.create(FontAwesome.Glyph.NEWSPAPER_ALT);
		newsGlyph.setFontSize(TAB_ICON_FONT_SIZE);
		newsGlyph.setColor(Color.WHITE);
		newsTab.setGraphic(newsGlyph);
		tpContent.getTabs().add(newsTab);
	}

	private void initEventsTabPane()
	{
		LOGGER.trace("MainController initEventsTabPane ...");

		SpringFxmlLoader.Wrapper<EventsController> wrapper = loader.loadAndWrap("/fxml/events/eventsComponent.fxml");
		EventsController eventsController = wrapper.getController();
		eventsController.loadEvents();
		Tab eventsTab = new Tab(null, wrapper.getLoadedObject());
		Glyph eventsGlyph = fontAwesome.create(FontAwesome.Glyph.CALENDAR);
		eventsGlyph.setFontSize(TAB_ICON_FONT_SIZE);
		eventsGlyph.setColor(Color.WHITE);
		eventsTab.setGraphic(eventsGlyph);
		tpContent.getTabs().add(eventsTab);
	}

	private void initCustomerTabPane()
	{
		LOGGER.trace("MainController initCustomerTabPane ...");

		SpringFxmlLoader.Wrapper<CustomerController> wrapper = loader.loadAndWrap(
			"/fxml/customers/customerComponent.fxml");
		CustomerController customerController = wrapper.getController();
		customerController.loadCustomers();
		Tab customerTab = new Tab(null, wrapper.getLoadedObject());
		Glyph customerGlyph = fontAwesome.create(FontAwesome.Glyph.USERS);
		customerGlyph.setFontSize(TAB_ICON_FONT_SIZE);
		customerGlyph.setColor(Color.WHITE);
		customerTab.setGraphic(customerGlyph);
		tpContent.getTabs().add(customerTab);
	}

	private void initTop10EventsTabPane()
	{
		LOGGER.trace("MainController initTop10EventsTabPane ...");

		SpringFxmlLoader.Wrapper<Top10EventsController> wrapper = loader.loadAndWrap(
			"/fxml/top10events/top10EventsComponent.fxml");
		Top10EventsController top10EventsController = wrapper.getController();
		top10EventsController.loadEvents();
		Tab tab = new Tab(null, wrapper.getLoadedObject());
		Glyph glyph = fontAwesome.create(FontAwesome.Glyph.BAR_CHART);
		glyph.setFontSize(TAB_ICON_FONT_SIZE);
		glyph.setColor(Color.WHITE);
		tab.setGraphic(glyph);
		tpContent.getTabs().add(tab);
	}

	private void initUserTabPane()
	{
		LOGGER.trace("MainController initUserTabPane ...");

		SpringFxmlLoader.Wrapper<UserController> wrapper = loader.loadAndWrap("/fxml/users/usersComponent.fxml");
		UserController userController = wrapper.getController();
		userController.loadUsers();
		Tab userTab = new Tab(null, wrapper.getLoadedObject());
		Glyph userGlyph = fontAwesome.create(FontAwesome.Glyph.MEH_ALT);
		userGlyph.setFontSize(TAB_ICON_FONT_SIZE);
		userGlyph.setColor(Color.WHITE);
		userTab.setGraphic(userGlyph);
		tpContent.getTabs().add(userTab);
	}

	private void loadMainUi(AuthenticationTokenInfo info)
	{
		if(info != null)
		{
			LOGGER.debug("logged in with roles {}", info.getRoles());

			spMainContent.getChildren().remove(login);
			tpContent.getTabs().clear();
			initNewsTabPane();
			initEventsTabPane();
			initTop10EventsTabPane();
			initCustomerTabPane();

			if(info.getRoles().contains("ADMIN"))
				initUserTabPane();
		}
		else
		{
			LOGGER.debug("logged out");
			spMainContent.getChildren().add(login);
		}
	}

	public void setProgressbarProgress(double progress)
	{
		pbLoadingProgress.setProgress(progress);
	}

	public void logout()
	{
		Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
		alert.setTitle(BundleManager.getBundle().getString("logout.title"));
		alert.setHeaderText(BundleManager.getBundle().getString("logout.header"));
		alert.setContentText(BundleManager.getBundle().getString("logout.content"));

		Optional<ButtonType> result = alert.showAndWait();

		if(result.isPresent() && result.get() == ButtonType.OK)
		{
			authenticationInformationService.clearAuthentication();
		}
	}
}
