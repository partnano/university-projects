package at.ac.tuwien.inso.sepm.ticketline.client.exception;

public class PaymentException extends Exception
{
	public PaymentException(String message, Throwable cause)
	{
		super(message, cause);
	}

	public PaymentException(String message)
	{
		super(message);
	}
}
