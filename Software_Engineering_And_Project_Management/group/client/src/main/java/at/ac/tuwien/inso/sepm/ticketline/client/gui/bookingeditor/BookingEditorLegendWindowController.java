package at.ac.tuwien.inso.sepm.ticketline.client.gui.bookingeditor;

import at.ac.tuwien.inso.sepm.ticketline.client.gui.locationplan.Seat;
import at.ac.tuwien.inso.sepm.ticketline.client.util.BundleManager;
import at.ac.tuwien.inso.springfx.SpringFxmlLoader;
import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.springframework.stereotype.Component;

@Component
public class BookingEditorLegendWindowController
{
	@FXML
	private GridPane ui_gridPane;

	private Stage dialog;

	@FXML
	private void initialize()
	{
		Seat availableSeat = new Seat(null, (seat) -> {
		}, null, 0);
		availableSeat.setFree();

		Seat reservedSeat = new Seat(null, (seat) -> {
		}, null, 0);
		reservedSeat.setReserved();

		Seat bookedSeat = new Seat(null, (seat) -> {
		}, null, 0);
		bookedSeat.setBooked();

		ui_gridPane.add(availableSeat, 0, 0);
		ui_gridPane.add(new Text(BundleManager.getBundle().getString("legendWindow.availableDescription")), 1, 0);

		ui_gridPane.add(reservedSeat, 0, 1);
		ui_gridPane.add(new Text(BundleManager.getBundle().getString("legendWindow.reservedDescription")), 1, 1);

		ui_gridPane.add(bookedSeat, 0, 2);
		ui_gridPane.add(new Text(BundleManager.getBundle().getString("legendWindow.bookedDescription")), 1, 2);
	}

	public void create(Stage stage, SpringFxmlLoader springFxmlLoader)
	{
		Stage dialog = new Stage();
		dialog.setResizable(false);
		dialog.initModality(Modality.APPLICATION_MODAL);
		dialog.initOwner(stage);
		SpringFxmlLoader.Wrapper<BookingEditorLegendWindowController> wrapper = springFxmlLoader.loadAndWrap("/fxml/bookingeditor/bookingEditorLegendWindow.fxml");

		dialog.setScene(new Scene((Parent)wrapper.getLoadedObject()));
		dialog.setTitle(BundleManager.getBundle().getString("legendWindow.title"));

		this.dialog = dialog;
	}

	public void show()
	{
		dialog.showAndWait();
	}
}
