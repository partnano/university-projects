package at.ac.tuwien.inso.sepm.ticketline.client.rest;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.rest.page.RestResponsePage;
import at.ac.tuwien.inso.sepm.ticketline.rest.user.UserDTO;

public interface UserRestClient
{
	/**
	 * Retrieves Page of Users
	 *
	 * @param page Pagenumber
	 * @param size Numer of Elements per Page
	 * @return Page of Users
	 * @throws DataAccessException if something goes wrong
	 */
	RestResponsePage<UserDTO> findAll(int page, int size) throws DataAccessException;

	/**
	 * Create new user
	 *
	 * @param userDTO user to be created
	 * @return created User
	 */
	UserDTO create(UserDTO userDTO) throws DataAccessException;

	/**
	 * Updates a user
	 *
	 * @param userDTO the user to update
	 * @return the updated user
	 * @throws DataAccessException if updating fails for some reason
	 */
	UserDTO update(UserDTO userDTO) throws DataAccessException;

	/**
	 * Resets the password for a user.
	 *
	 * @param id the id of the user whose password is to be reset
	 */
	void resetPassword(long id) throws DataAccessException;
}
