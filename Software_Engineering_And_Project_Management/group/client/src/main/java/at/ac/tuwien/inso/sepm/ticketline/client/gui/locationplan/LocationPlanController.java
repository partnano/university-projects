package at.ac.tuwien.inso.sepm.ticketline.client.gui.locationplan;

import at.ac.tuwien.inso.sepm.ticketline.client.util.EventTools;
import at.ac.tuwien.inso.sepm.ticketline.rest.booking.BookingDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.booking.SeatBookingDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventAreaDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.location.AreaDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.location.RowDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.location.SeatDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.location.SectionDTO;
import at.ac.tuwien.inso.springfx.SpringFxmlLoader;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.function.Consumer;

@Component
public class LocationPlanController
{
	private static final Logger LOGGER = LoggerFactory.getLogger(LocationPlanController.class);
	public VBox ui_rowVBox;
	public AnchorPane ui_anchorPane;
	public BorderPane ui_borderPane;

	private Consumer<SelectionChangedEvent> selectionListener;

	private final Map<SeatDTO, Seat> seatsToSeats = new HashMap<>();
	private final Set<SeatDTO> selectedSeats = Collections.newSetFromMap(new HashMap<>());

	private final Map<SectionDTO, Section> sectionsToSections = new HashMap<>();
	private final Set<SectionDTO> sectionsInBooking = Collections.newSetFromMap(new HashMap<>());
	private Section selectedSection = null;

	private int prefHeight = 0;
	private int prefWidth = 0;

	@FXML
	private VBox ui_seatVBox;

	private void setSelectionListener(Consumer<SelectionChangedEvent> selectionListener)
	{
		this.selectionListener = selectionListener;
	}

	public void setSelection(Set<SeatDTO> seatsToSelect)
	{
		for(SeatDTO seatDTO : seatsToSeats.keySet())
		{
			Seat correspondingSeat = seatsToSeats.get(seatDTO);
			if (seatsToSelect.contains(seatDTO))
				correspondingSeat.setSelected(true);
			else
				correspondingSeat.setSelected(false);
		}

		selectedSeats.clear();
		selectedSeats.addAll(seatsToSelect);
	}

	public void setSectionsInBooking(Set<SectionDTO> newSectionsInBooking)
	{
		LOGGER.debug("updating sections in booking, new sections, size: {}, elem:{}",
		             newSectionsInBooking.size(),
		             newSectionsInBooking);
		LOGGER.debug("sectionsToSection elements {}", sectionsToSections.keySet());

		// checks all sections to see if it is included in newSectionsToBook
		// and if so, marks the corresponding section to be drawn as includedInBooking
		for(Map.Entry<SectionDTO, Section> sectionEntries : sectionsToSections.entrySet())
		{
			boolean sectionIsInBooking = false;
			for(SectionDTO inBooking : newSectionsInBooking)
			{
				if(inBooking.getId().equals(sectionEntries.getKey().getId()))
				{
					sectionIsInBooking = true;
					break;
				}
			}

			sectionEntries.getValue().setIncludedInBooking(sectionIsInBooking);
		}

		sectionsInBooking.clear();
		sectionsInBooking.addAll(newSectionsInBooking);
	}

	public void clearSelection()
	{
		setSelection(new HashSet<>());
	}

	public void clearSectionsInBooking()
	{
		setSectionsInBooking(new HashSet<>());
	}

	public void clearSelectedSection()
	{
		for(Map.Entry<SectionDTO, Section> sectionDTOSectionEntry : sectionsToSections.entrySet())
		{
			sectionDTOSectionEntry.getValue().setSelected(false);
		}
		selectionListener.accept(new SelectionChangedEvent(null, null, null));
	}

	public void setEvent(EventDTO event)
	{
		Set<EventAreaDTO> eventAreas = event.getEventAreas();


		selectedSeats.clear();
		selectedSection = null;

		ui_rowVBox.setSpacing(5);
		ui_rowVBox.getChildren().clear();

		Node[] rows = new Node[eventAreas.size()];

		List<EventAreaDTO> areas = new ArrayList<>(event.getEventAreas());
		areas.sort(Comparator.comparing(o -> o.getArea().getOrderIndex()));

		for(EventAreaDTO eventArea : areas)
		{
			AreaDTO area = eventArea.getArea();

			if(area instanceof SectionDTO)
			{

				SectionDTO dto = (SectionDTO)area;
				Section section = new Section(dto, this::sectionClicked);
				section.setFree();

				Label rowLabel = new Label();
				rowLabel.setPrefSize(50, 100);
				rowLabel.setText(dto.getName());
				rowLabel.setAlignment(Pos.CENTER);
				rowLabel.setStyle("-fx-font-size: 18; -fx-font-weight: bold;");

				ui_rowVBox.getChildren().add(rowLabel);

				if(EventTools.getNumAvailableTickets(dto, event) == 0)
					section.setBooked();

				TilePane pane = new TilePane();
				pane.setAlignment(Pos.CENTER_LEFT);
				pane.getChildren().add(section);

				rows[dto.getOrderIndex().intValue() - 1] = pane;
				sectionsToSections.put(dto, section);
			}
			else if(area instanceof RowDTO)
			{
				RowDTO row = (RowDTO)area;

				Label rowLabel = new Label();
				rowLabel.setPrefSize(Seat.getSIZE(), Seat.getSIZE());
				rowLabel.setText(row.getName());
				rowLabel.setAlignment(Pos.CENTER);
				rowLabel.setStyle("-fx-font-size: 18; -fx-font-weight: bold");

				LOGGER.debug(row.getName());

				ui_rowVBox.getChildren().add(rowLabel);


				TilePane rowPane = new TilePane();
				rowPane.setAlignment(Pos.CENTER);
				rowPane.setPrefColumns(row.getSeats().size());
				rowPane.setHgap(5);

				Set<SeatDTO> seats = new TreeSet<>(Comparator.comparingLong(SeatDTO::getOrderIndex));
				seats.addAll(row.getSeats());

				int seatCount = 0;

				for(SeatDTO dto : seats)
				{
					seatCount++;
					Seat seat = new Seat(dto, this::seatClicked, row, BookingDTO.priceForArea(row, event));
					seat.setFree();

					//find a better solution for this
					for(BookingDTO bookingDTO : event.getBookings())
					{
						if(bookingDTO instanceof SeatBookingDTO)
						{
							Set<SeatDTO> bookedSeats = ((SeatBookingDTO)bookingDTO).getSeats();

							for(SeatDTO bookedSeat : bookedSeats)
							{
								if(bookedSeat.getId().equals(dto.getId()))
								{
									if(!bookingDTO.isCanceled())
									{
										if(bookingDTO.isPaid())
										{
											seat.setBooked();
										}
										else if(!bookingDTO.isPaid())
										{
											seat.setReserved();
										}
									}
								}
							}
						}
					}
					rowPane.getChildren().add(seat);
					seatsToSeats.put(dto, seat);
				}


				rows[row.getOrderIndex().intValue() - 1] = rowPane;
				setPrefWidth(seatCount * (Seat.getSIZE() + (int)rowPane.getHgap() * 2));
				setPrefHeight(rows.length * (Seat.getSIZE() + (int)ui_rowVBox.getSpacing()));

			}
			else
				throw new AssertionError("unhandled area type");
		}

		ui_seatVBox.setSpacing(5);
		ui_seatVBox.getChildren().setAll(rows);
	}

	private void seatClicked(SeatDTO seat)
	{
		if(seatsToSeats.get(seat).toggleSelection())
			selectedSeats.add(seat);
		else
			selectedSeats.remove(seat);

		LOGGER.info("seat {} (id = {}) selected", seat.getName(), seat.getId());
		selectionListener.accept(new SelectionChangedEvent(Collections.unmodifiableSet(selectedSeats), null, seat));
	}

	private void sectionClicked(SectionDTO section)
	{
		setSelectedSection(section);
	}

	private void setSelectedSection(SectionDTO section)
	{
		if(selectedSection != null)
			selectedSection.setSelected(false);

		Section currentSection = sectionsToSections.get(section);
		currentSection.setSelected(true);
		selectedSection = currentSection;

		LOGGER.info("section {} (id = {}) selected", section.getName(), section.getId());
		selectionListener.accept(new SelectionChangedEvent(null, section, null));
	}

	public static Node create(SpringFxmlLoader loader, EventDTO event, Consumer<SelectionChangedEvent> onSelectionChanged)
	{
		SpringFxmlLoader.Wrapper<LocationPlanController> wrapper = loader.loadAndWrap("/fxml/locationplan/locationPlan.fxml");
		LocationPlanController controller = wrapper.getController();
		controller.setEvent(event);
		controller.setSelectionListener(onSelectionChanged);

		LOGGER.debug("created locationplan");
		return wrapper.getLoadedObject();
	}

	public static SpringFxmlLoader.Wrapper<LocationPlanController> createWrapper(SpringFxmlLoader loader, EventDTO event, Consumer<SelectionChangedEvent> onSelectionChanged)
	{
		SpringFxmlLoader.Wrapper<LocationPlanController> wrapper = loader.loadAndWrap("/fxml/locationplan/locationPlan.fxml");

		LocationPlanController controller = wrapper.getController();
		controller.setEvent(event);
		controller.setSelectionListener(onSelectionChanged);

		LOGGER.debug("created locationplan");
		return wrapper;
	}

	public int getPrefHeight()
	{
		return prefHeight;
	}

	private void setPrefHeight(int prefHeight)
	{
		this.prefHeight = prefHeight;
	}

	public int getPrefWidth()
	{
		return prefWidth;
	}

	private void setPrefWidth(int prefWidth)
	{
		if(prefWidth > getPrefWidth())
		{
			this.prefWidth = prefWidth;
		}
	}
}
