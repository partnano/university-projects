package at.ac.tuwien.inso.sepm.ticketline.client.service.implementation;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.client.exception.InvalidCSVException;
import at.ac.tuwien.inso.sepm.ticketline.client.exception.ValidationException;
import at.ac.tuwien.inso.sepm.ticketline.client.rest.EventRestClient;
import at.ac.tuwien.inso.sepm.ticketline.client.service.EventService;
import at.ac.tuwien.inso.sepm.ticketline.client.service.LocationService;
import at.ac.tuwien.inso.sepm.ticketline.client.validation.EventValidator;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventAreaDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventFilterDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.enums.Category;
import at.ac.tuwien.inso.sepm.ticketline.rest.location.AreaDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.location.LocationDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.page.RestResponsePage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.*;

@Service
public class SimpleEventService implements EventService
{
	private static final Logger LOGGER = LoggerFactory.getLogger(SimpleEventService.class);

	private final EventRestClient eventRestClient;
	private final LocationService locationService;

	public SimpleEventService(EventRestClient eventRestClient, LocationService locationService)
	{
		this.eventRestClient = eventRestClient;
		this.locationService = locationService;
	}

	@Override
	public List<EventDTO> findAll() throws DataAccessException
	{
		LOGGER.trace("EventService findAll()");
		return eventRestClient.findAll();
	}

	@Override
	public RestResponsePage<EventDTO> findAll(int page, int size) throws DataAccessException
	{
		LOGGER.trace("EventService.findAll(int, int)");
		return eventRestClient.findAll(page, size);
	}

	@Override
	public EventDTO findOne(Long id) throws DataAccessException
	{
		LOGGER.trace("EventService findOne()");
		return eventRestClient.findOne(id);
	}

	@Override
	public List<EventDTO> findByFilter(EventFilterDTO filter) throws DataAccessException
	{
		LOGGER.trace("EventService findByFilter()");
		return eventRestClient.findByFilter(filter);
	}

	@Override
	public RestResponsePage<EventDTO> findByFilter(EventFilterDTO filter, int page, int size) throws DataAccessException
	{
		LOGGER.trace("Eventervice.findByFilter(filter, int, int)");
		return eventRestClient.findByFilter(filter, page, size);
	}

	@Override
	public EventDTO create(EventDTO event) throws ValidationException, DataAccessException
	{
		EventValidator.validate(event);

		LOGGER.trace("EventService create()");
		return eventRestClient.create(event);
	}

	@Override
	public Map<EventDTO, Integer> findTopEvents(Category category, Integer month, Integer year, int limit) throws
	                                                                                                       ValidationException,
	                                                                                                       DataAccessException
	{
		LOGGER.trace("EventService findTopEvents()");
		EventValidator.validateMonth(month);
		EventValidator.validateYear(year);
		EventValidator.validateLimit(limit);
		return eventRestClient.findTopEvents(category, month, year, limit);
	}

	@Override
	public List<EventDTO> parse(String pathToCSV) throws DataAccessException, InvalidCSVException
	{
		LOGGER.debug("Trying to parse csv ...");

		List<EventDTO> output = new ArrayList<>();
		String[] header = null;
		String line;

		try(BufferedReader reader = new BufferedReader(new FileReader(pathToCSV)))
		{
			while((line = reader.readLine()) != null)
			{
				String[] splitLine = line.split(",");

				// skip empty lines
				if(splitLine[0].equals(""))
					continue;

				// csv has to start with "name"
				if(splitLine[0].equals("name"))
				{
					header = splitLine;
					continue;
				}

				if(header == null)
				{
					LOGGER.error("No header found in CSV parsing.");
					throw new InvalidCSVException("No header found in CSV parsing.");
				}

				if(splitLine.length != header.length)
				{
					LOGGER.error("Data not fitting header in CSV parsing.");
					throw new InvalidCSVException("Data not fitting header in CSV parsing.");
				}

				EventDTO dto = new EventDTO();
				Set<? extends AreaDTO> locationAreas = new HashSet<>();
				dto.setEventAreas(new HashSet<>());

				boolean eventHasSections = false;
				boolean eventHasRows = false;

				for(int i = 0; i < header.length; i++)
				{
					header[i] = header[i].toLowerCase();

					switch(header[i])
					{
					case "name":
						LOGGER.debug("CSV Parser: Found name: " + splitLine[i]);

						dto.setName(splitLine[i]);
						break;

					case "description":
						LOGGER.debug("CSV Parser: Found description: " + splitLine[i]);

						dto.setDescription(splitLine[i]);
						break;

					case "artist":
						LOGGER.debug("CSV Parser: Found artist: " + splitLine[i]);

						dto.setArtist(splitLine[i]);
						break;

					case "category":
						LOGGER.debug("CSV Parser: Found category: " + splitLine[i]);

						dto.setCategory(Category.fromString(splitLine[i]));
						break;

					case "duration":
						LOGGER.debug("CSV Parser: Found duration: " + splitLine[i]);

						dto.setDuration(Integer.parseInt(splitLine[i]));
						break;

					case "location":
						LOGGER.debug("CSV Parser: Found location: " + splitLine[i]);

						List<LocationDTO> locations = locationService.findAll();
						for(LocationDTO location : locations)
						{
							if(location.getName().equals(splitLine[i]))
								dto.setLocation(location);
						}

						if(dto.getLocation() == null)
						{
							LOGGER.error(
								"Location not found while parsing " + splitLine[i]);
							throw new InvalidCSVException(
								"Location not found while parsing! " + splitLine[i]);
						}

						// location assumed to be correct
						locationAreas = dto.getLocation().getAreas();

						break;

					case "time":
						LOGGER.debug("CSV Parser: Found time: " + splitLine[i]);

						DateTimeFormatter format = DateTimeFormatter.ofPattern(
							"yyyy-MM-dd HH:mm");
						LocalDateTime eventTime = LocalDateTime.parse(splitLine[i], format);
						dto.setTime(eventTime);
						break;

					case "default": // rows
						LOGGER.debug("CSV Parser: Found default (price): " + splitLine[i]);

						eventHasRows = true;

						// Create all possible EventArea entries (i.e. one entry per row) and set
						// the price to the given value
						for(AreaDTO locationArea : locationAreas)
						{
							EventAreaDTO eventArea = EventAreaDTO.builder()
							                                     .area(locationArea)
							                                     .event(dto)
							                                     .price(Float.parseFloat(
								                                     splitLine[i])).build();
							dto.getEventAreas().add(eventArea);
						}

						break;

					case "from": // rows
						LOGGER.debug("CSV Parser: Found from (row price): " + splitLine[i]);

						if(header[i + 1] == null || header[i + 2] == null || !header[i
						                                                             + 1].equals(
							"to") || !header[i + 2].equals("price"))
						{
							LOGGER.error(
								"Something went wrong with row triple in CSV parsing");
							throw new InvalidCSVException(
								"Something went wrong with row triple in CSV parsing");
						}

						Integer[] rowPrice = new Integer[3];

						rowPrice[0] = Integer.parseInt(splitLine[i++]);
						rowPrice[1] = Integer.parseInt(splitLine[i++]);
						rowPrice[2] = Integer.parseInt(splitLine[i]);

						if(rowPrice[0] > rowPrice[1])
						{
							LOGGER.error(
								"'from' value can not be after 'to' value in CSV parsing");
							throw new InvalidCSVException(
								"'from' value can not be after 'to' value in CSV parsing");
						}

						// apply prices to rows in range
						boolean foundEventRowPrice;
						Integer rangeStart = rowPrice[1];
						Integer rangeEnd = rowPrice[2];
						for(AreaDTO locationRow : locationAreas)
						{
							foundEventRowPrice = false;
							// Find an area (i.e. a row) that is in the provided range
							if(locationRow.getOrderIndex() >= rangeStart
							   && locationRow.getOrderIndex() <= rangeEnd)
							{
								for(EventAreaDTO eventArea : dto.getEventAreas())
								{
									if(eventArea.getArea().equals(locationRow))
									{
										foundEventRowPrice = true;
										// Apply price to the found area
										eventArea.setPrice(Float.parseFloat(
											splitLine[i]));
									}
								}
								if(!foundEventRowPrice)
								{
									LOGGER.error("Missing default price");
									throw new InvalidCSVException(
										"Missing default price");
								}
							}
						}

						break;

					default: // sections
						LOGGER.debug("CSV Parser: Found section: " + splitLine[i]);

						eventHasSections = true;

						// apply prices to sections
						boolean foundSection = false;
						for(AreaDTO locationSection : locationAreas)
						{
							if(locationSection.getName().equals(header[i]))
							{
								foundSection = true;

								EventAreaDTO eventArea = EventAreaDTO.builder()
								                                     .area(locationSection)
								                                     .event(dto)
								                                     .price(Float.parseFloat(
									                                     splitLine[i]))
								                                     .build();
								dto.getEventAreas().add(eventArea);
								break;
							}
						}
						if(!foundSection)
						{
							LOGGER.error("Given section not existing in given location");
							throw new InvalidCSVException(
								"Given section not existing in given location");
						}
					}
				}

				// basic validity check
				if(dto.getName() == null || dto.getArtist() == null || dto.getCategory() == null
				   || dto.getTime() == null)
				{
					LOGGER.error("CSV does not have all required entries! (Base entries)");
					throw new InvalidCSVException(
						"CSV does not have all required entries! (Base entries)");
				}

				if(dto.getEventAreas().isEmpty())
				{
					LOGGER.error("CSV does not have all required entries! (Row/Section entries)");
					throw new InvalidCSVException(
						"CSV does not have all required entries! (Row/Section entries)");
				}

				if(eventHasRows && eventHasSections)
				{
					LOGGER.error("CSV can not have both rows and sections!");
					throw new InvalidCSVException("CSV can not have both rows and sections!");
				}

				// check if all sections of the location have been assigned a price
				if(eventHasSections)
				{
					for(AreaDTO locationSection : locationAreas)
					{
						boolean found = false;
						for(EventAreaDTO eventSection : dto.getEventAreas())
						{
							if(locationSection.getName()
							                  .equals(eventSection.getArea().getName()))
							{
								found = true;
								break;
							}
						}

						if(!found)
						{
							LOGGER.error("Price is not set for all sections in csv");
							throw new InvalidCSVException(
								"Price is not set for all sections in csv");
						}
					}
				}

				output.add(dto);
			}

			return output;
		}
		catch(IOException e)
		{
			LOGGER.error("Something went wrong with CSV parsing: " + e.getMessage());
			throw new InvalidCSVException("Something went wrong with csv parsing: " + e.getMessage());
		}
		catch(NumberFormatException e)
		{
			LOGGER.error("Invalid numbers in CSV file");
			throw new InvalidCSVException("Invalid numbers in CSV file");
		}
		catch(DateTimeParseException e)
		{
			LOGGER.error("Invalid date in CSV file");
			throw new InvalidCSVException("Invalid date in CSV file");
		}
	}
}
