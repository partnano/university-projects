package at.ac.tuwien.inso.sepm.ticketline.client.gui.bookingeditor;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.client.gui.MiscAlerts;
import at.ac.tuwien.inso.sepm.ticketline.client.gui.locationplan.LocationPlanController;
import at.ac.tuwien.inso.sepm.ticketline.client.gui.locationplan.SelectionChangedEvent;
import at.ac.tuwien.inso.sepm.ticketline.client.gui.payment.PaymentController;
import at.ac.tuwien.inso.sepm.ticketline.client.service.BookingService;
import at.ac.tuwien.inso.sepm.ticketline.client.service.EventService;
import at.ac.tuwien.inso.sepm.ticketline.client.util.BundleManager;
import at.ac.tuwien.inso.sepm.ticketline.client.util.EventTools;
import at.ac.tuwien.inso.sepm.ticketline.rest.booking.BookingDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.booking.SectionBookingDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.booking.SectionBookingEntryDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.customer.CustomerDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventAreaDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.location.AreaDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.location.SectionDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.payment.PaymentProvider;
import at.ac.tuwien.inso.springfx.SpringFxmlLoader;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@Component
public class SectionBookingEditorController extends AbstractBookingEditorController
{
	private static final Logger LOGGER = LoggerFactory.getLogger(SectionBookingEditorController.class);

	@FXML
	private GridPane ui_sectionInfoPane;

	private SectionDTO selectedSection = null;
	private final Map<SectionDTO, Long> sectionsToBook = new HashMap<>();
	private final ObservableList<SectionDTO> sectionList = FXCollections.observableArrayList();

	private Button addAmountButton;
	private TextField amountInput;

	public SectionBookingEditorController(EventService eventService, BookingService bookingService)
	{
		super(eventService, bookingService);
	}

	@Override
	protected void initialize()
	{
		super.initialize();

		addAmountButton = new Button(BundleManager.getBundle().getString("bookingeditor.detailsPane.addAmountButton"));
		amountInput = new TextField();
	}

	@Override
	protected BookingDTO getBookingWithSelectedAreas()
	{
		SectionBookingDTO booking = new SectionBookingDTO();
		Set<SectionBookingEntryDTO> sections = new HashSet<>();

		sectionsToBook.forEach((section, amount) -> {
			SectionBookingEntryDTO dto = new SectionBookingEntryDTO();
			dto.setSection(section);
			dto.setAmount(amount);
			sections.add(dto);
		});

		booking.setSections(sections);
		return booking;
	}

	// returns updated event
	public static EventDTO showAndWait(Stage stage, SpringFxmlLoader springFxmlLoader, EventDTO event)
	{
		LOGGER.debug("creating new SectionBookingEditorController from event info");
		Stage dialog = createDialog(stage);
		dialog.addEventHandler(KeyEvent.KEY_RELEASED, (KeyEvent keyEvent) -> {
			if(KeyCode.ESCAPE == keyEvent.getCode())
			{
				dialog.close();
			}
		});

		SpringFxmlLoader.Wrapper<SectionBookingEditorController> wrapper = springFxmlLoader.loadAndWrap(
				"/fxml/bookingeditor/sectionBookingEditor.fxml");

		SectionBookingEditorController controller = wrapper.getController();
		controller.setEvent(event);

		createAndSetLegend(stage, controller, springFxmlLoader);

		controller.dialog = dialog;
		controller.assignCustomerController = BookingAssignCustomerController.create(stage,
		                                                                             springFxmlLoader,
		                                                                             controller::onCustomerAssignedToBooking);
		controller.paymentController = PaymentController.create(stage, springFxmlLoader, controller::onBookingPaid);

		controller.customInitialization(springFxmlLoader, null);

		dialog.setScene(new Scene((Parent)wrapper.getLoadedObject()));
		dialog.setTitle(BundleManager.getBundle().getString("bookingeditor.title"));

		dialog.showAndWait();

		// reset view after showAndWait has returned, i.e. after window was closed...
		controller.resetSelection();

		return controller.getEvent();
	}

	// booking.event must be set
	// returns updated event
	public static void showAndWait(Stage stage, SpringFxmlLoader springFxmlLoader, SectionBookingDTO booking)
	{
		LOGGER.debug("creating new SectionBookingEditorController from booking info");
		Stage dialog = createDialog(stage);

		SpringFxmlLoader.Wrapper<SectionBookingEditorController> wrapper = springFxmlLoader.loadAndWrap(
				"/fxml/bookingeditor/sectionBookingEditor.fxml");

		SectionBookingEditorController controller = wrapper.getController();
		controller.booking = booking;

		try
		{
			bookingService.populateBookingEvent(booking);
		}
		catch(DataAccessException e)
		{
			MiscAlerts.showDataAccessAlert();
		}

		controller.setEvent(booking.getEvent());

		createAndSetLegend(stage, controller, springFxmlLoader);

		controller.dialog = dialog;
		controller.assignCustomerController = BookingAssignCustomerController.create(stage,
		                                                                             springFxmlLoader,
		                                                                             controller::onCustomerAssignedToBooking);

		controller.paymentController = PaymentController.create(stage, springFxmlLoader, controller::onBookingPaid);

		controller.customInitialization(springFxmlLoader, booking);

		dialog.setScene(new Scene((Parent)wrapper.getLoadedObject()));
		dialog.setTitle(BundleManager.getBundle().getString("bookingeditor.title"));

		dialog.showAndWait();

		// reset view after showAndWait has returned, i.e. after window was closed...
		controller.resetSelection();
	}

	// suppress because ui_selectedSeatsTable takes either Seats or Sections
	@SuppressWarnings("unchecked")
	private void customInitialization(SpringFxmlLoader springFxmlLoader, SectionBookingDTO booking)
	{
		LOGGER.debug("Event consists of Sections");

		ui_selectedSeatsTable.setItems(sectionList);
		ui_selectedSeatsTable.setEditable(true);

		ui_infoPane.setVisible(true);

		TableColumn<SectionDTO, String> ui_sectionColumn = new TableColumn<>(BundleManager.getBundle()
		                                                                                  .getString(
				                                                                                  "bookingeditor.seattable.section"));
		ui_sectionColumn.setStyle("-fx-alignment: CENTER;");
		ui_sectionColumn.setCellValueFactory(cellData -> new SimpleObjectProperty<>(cellData.getValue().getName()));

		TableColumn<SectionDTO, String> ui_priceColumn = new TableColumn<>(BundleManager.getBundle()
		                                                                                .getString(
				                                                                                "bookingeditor.seattable.price"));
		ui_priceColumn.setStyle("-fx-alignment: CENTER;");
		ui_priceColumn.setCellValueFactory(cellData -> {
			SectionDTO section = cellData.getValue();

			Float sectionPrice = BookingDTO.priceForArea(section, event);

			return new SimpleObjectProperty<>(String.format("%.2f %s",
			                                                sectionPrice,
			                                                BundleManager.getBundle().getString("currency")));
		});

		TableColumn<SectionDTO, Long> ui_amountColumn = new TableColumn<>(BundleManager.getBundle()
		                                                                               .getString(
				                                                                               "bookingeditor.seattable.amount"));
		ui_priceColumn.setStyle("-fx-alignment: CENTER;");

		ui_amountColumn.setCellValueFactory(cellData -> new SimpleObjectProperty<>(sectionsToBook.get(cellData.getValue())));

		ui_selectedSeatsTable.getColumns().add(ui_sectionColumn);
		ui_selectedSeatsTable.getColumns().add(ui_priceColumn);
		ui_selectedSeatsTable.getColumns().add(ui_amountColumn);

		SpringFxmlLoader.Wrapper<LocationPlanController> locationPlan = LocationPlanController.createWrapper(
				springFxmlLoader,
				event,
				this::onSectionsInBookingChanged);

		locationPlanController = locationPlan.getController();

		setBooking(booking);

		createLocationPlan(locationPlan);
	}

	private void setBooking(SectionBookingDTO booking)
	{
		super.setBooking(booking);

		if(booking != null)
		{
			enableEditing(false);

			sectionsToBook.clear();
			sectionList.clear();

			for(SectionBookingEntryDTO entry : booking.getSections())
			{
				sectionsToBook.put(entry.getSection(), entry.getAmount());
				sectionList.add(entry.getSection());
			}
			locationPlanController.setSectionsInBooking(new HashSet<>(sectionList));
		}
		else
		{
			enableEditing(true);
		}

		updateButtonVisibility();
		updateBookingInfo();
	}

	private void enableEditing(boolean enable)
	{
		addAmountButton.setDisable(!enable);
		amountInput.setDisable(!enable);
	}

	private void onSectionsInBookingChanged(SelectionChangedEvent e)
	{
		selectedSection = e.getSection();

		updateSectionInfo(selectedSection);
		updateButtonVisibility();
	}

	@FXML
	private void ui_onNewBookingButtonClick()
	{
		LOGGER.debug("new booking button pressed");
		resetSelection();
	}

	@Override
	protected void onBookingSaved(BookingDTO booking)
	{
		if(booking instanceof SectionBookingDTO)
			setBooking((SectionBookingDTO)booking);
		else
			throw new AssertionError(
					"Saved booking must be of type SectionBookingDTO but was " + booking.getClass().toString());
	}

	private void resetSelection()
	{
		ui_selectedSeatsTable.getItems().clear();
		selectedSection = null;
		sectionsToBook.clear();
		sectionList.clear();
		locationPlanController.clearSectionsInBooking();
		locationPlanController.clearSelectedSection();
		setBooking(null);
	}

	private void updateSectionInfo(SectionDTO section)
	{
		ui_sectionInfoPane.getChildren().clear();

		if(section == null)
		{
			// Show info that no section is selected
			ui_sectionInfoPane.add(new Text(BundleManager.getBundle()
			                                             .getString("bookingeditor.detailsPane.noSelection")),
			                       0,
			                       2,
			                       2,
			                       1);
			return;
		}

		// Add section name row

		ui_sectionInfoPane.add(new Text(BundleManager.getBundle().getString("bookingeditor.detailsPane.sectionName")),
		                       0,
		                       0);
		ui_sectionInfoPane.add(new Text(section.getName()), 1, 0);

		// Add section price info row

		ui_sectionInfoPane.add(new Text(BundleManager.getBundle().getString("bookingeditor.detailsPane.sectionPrice")),
		                       0,
		                       1);
		ui_sectionInfoPane.add(new Text(String.format("%.2f %s",
		                                              BookingDTO.priceForArea(section, event),
		                                              BundleManager.getBundle().getString("currency"))), 1, 1);

		// Add available tickets row

		ui_sectionInfoPane.add(new Text(BundleManager.getBundle()
		                                             .getString("bookingeditor.detailsPane.availableTickets")), 0, 2);
		ui_sectionInfoPane.add(new Text(
				EventTools.getNumAvailableTickets(section, event) + " / " + section.getCapacity()), 1, 2);

		// Add tickets in booking label row

		ui_sectionInfoPane.add(new Text(BundleManager.getBundle().getString("bookingeditor.detailsPane.amountInput")),
		                       0,
		                       3,
		                       2,
		                       1);

		// Add ticket amount row

		Long bookingAmount = sectionsToBook.get(section);
		if(bookingAmount != null)
			amountInput.setText(bookingAmount + "");
		else
			amountInput.clear();

		amountInput.setPromptText(BundleManager.getBundle().getString("bookingeditor.detailsPane.amountInput.prompt"));

		addAmountButton.setOnAction(e -> {
			Long oldAmount = sectionsToBook.get(selectedSection);
			Long newAmount;

			try
			{
				newAmount = Long.parseLong(amountInput.getText());
			}
			catch(NumberFormatException ex)
			{
				// invalid number entered -> restore old value
				if(oldAmount == null)
					amountInput.clear();
				else
					amountInput.setText("" + oldAmount);

				return;
			}

			if(newAmount == 0)
			{
				amountInput.clear();
				sectionsToBook.remove(selectedSection);
			}
			else
				sectionsToBook.put(selectedSection, newAmount);

			sectionList.clear();
			for(Map.Entry<SectionDTO, Long> entry : sectionsToBook.entrySet())
			{
				if(entry.getValue() > 0)
				{
					sectionList.add(entry.getKey());
				}
			}
			locationPlanController.setSectionsInBooking(new HashSet<>(sectionList));

			updateButtonVisibility();
			updateBookingInfo();
		});

		ui_sectionInfoPane.add(amountInput, 0, 4);
		ui_sectionInfoPane.add(addAmountButton, 1, 4);
	}

	protected void updateBookingInfo()
	{
		ui_detailsPane.getChildren().clear();

		Float price = 0f;

		for(Object o : ui_selectedSeatsTable.getItems())
		{
			if(o instanceof SectionDTO)
			{
				for(EventAreaDTO eventAreaDTO : event.getEventAreas())
				{
					AreaDTO s = eventAreaDTO.getArea();
					if(s instanceof SectionDTO && s.getId().equals(((SectionDTO)o).getId()))
					{
						price += eventAreaDTO.getPrice();
					}
				}
			}
		}

		ui_detailsPane.add(new Text(BundleManager.getBundle().getString("bookingEditor.detailsPane.totalPrice")), 0, 0);
		ui_detailsPane.add(new Text(String.format("%.2f €", price)), 1, 0);

		if(booking == null)
		{
			ui_detailsPane.add(new Text(BundleManager.getBundle().getString("bookingeditor.detailsPane.statusLabel")),
			                   0,
			                   1);
			if(sectionList.isEmpty())
			{
				ui_detailsPane.add(new Text(BundleManager.getBundle().getString("bookingStatus.empty")), 1, 1);
			}
			else if(!checkSelectionValidity())
			{
				ui_detailsPane.add(new Text(BundleManager.getBundle().getString("bookingeditor.invalidSelectionLabel")),
				                   1,
				                   1);
			}
			else
			{
				ui_detailsPane.add(new Text(BundleManager.getBundle().getString("bookingStatus.available")), 1, 1);
			}
		}
		else
		{
			String statusIdentifier = "";

			switch(booking.getBookingStatus())
			{
			case RESERVED:
				statusIdentifier = "bookingStatus.reserved";
				break;
			case BOOKED:
				statusIdentifier = "bookingStatus.booked";
				break;
			case CANCELED:
				statusIdentifier = "bookingStatus.canceled";
				break;
			}

			ui_detailsPane.add(new Text(BundleManager.getBundle().getString("bookingeditor.detailsPane.statusLabel")),
			                   0,
			                   1);
			ui_detailsPane.add(new Text(BundleManager.getBundle().getString(statusIdentifier)), 1, 1);

			ui_detailsPane.add(new Text(BundleManager.getBundle()
			                                         .getString("bookingeditor.detailsPane.reservationNumberLabel")),
			                   0,
			                   2);
			ui_detailsPane.add(new Text(booking.getReservationNumber() + ""), 1, 2);

			ui_detailsPane.add(new Text(BundleManager.getBundle().getString("bookingeditor.detailsPane.customerLabel")),
			                   0,
			                   3);
			CustomerDTO customer = booking.getCustomer();
			if(customer != null)
			{
				if(customer.getFirstName() != null && customer.getLastName() != null)
				{

					ui_detailsPane.add(new Text(customer.getLastName() + " " + customer.getFirstName() + " (" + customer.getCustomerNumber() + ")"), 1, 3);
				}
				else
				{
					ui_detailsPane.add(new Text(BundleManager.getBundle().getString("customer.anonymous") + " (" + customer.getCustomerNumber() + ")"), 1, 3);
				}
			}
			else
			{
				ui_detailsPane.add(new Text(BundleManager.getBundle().getString("bookingeditor.detailsPane.anonymous")),
				                   1,
				                   3);
			}

			if(booking.getBookingStatus() == BookingDTO.BookingStatus.BOOKED)
			{
				ui_detailsPane.add(new Text(BundleManager.getBundle()
				                                         .getString("bookingeditor.detailsPane.paymentMethod")), 0, 4);

				LOGGER.debug("payment method: {}", booking.getPaymentMethod());

				ui_detailsPane.add(new Text(BundleManager.getBundle()
				                                         .getString(PaymentProvider.valueOf(booking.getPaymentMethod())
				                                                                   .getName())), 1, 4);
			}
		}
	}

	private void updateButtonVisibility()
	{
		if(booking != null)
		{
			ui_reserveButton.setVisible(false);

			if(booking.isCanceled())
			{
				// canceled booking

				// -> hide cancel button
				ui_cancelReservationButton.setVisible(false);

				// -> change printReceiptButton text to print cancelation receipt
				ui_printReceiptButton.setText(BundleManager.getBundle()
				                                           .getString("bookingEditor.printCancellationReceiptButton"));
			}
			else
			{
				// regular booking or reservation
				ui_printReceiptButton.setText(BundleManager.getBundle().getString("bookingEditor.printReceiptButton"));

				ui_cancelReservationButton.setVisible(true);
			}

			// reservation -> show booking button, to enable conversion to booking
			if(booking.getBookingStatus() == BookingDTO.BookingStatus.RESERVED)
			{
				ui_bookButton.setVisible(true);
				ui_bookButton.setDisable(false);
			}
			else
			{
				ui_bookButton.setVisible(false);
			}

			if(booking.isPaid())
				ui_printReceiptButton.setVisible(true);
			else
				ui_printReceiptButton.setVisible(false);
		}
		else
		{
			boolean validConfiguration = checkSelectionValidity();

			ui_reserveButton.setVisible(true);
			ui_bookButton.setVisible(true);
			ui_printReceiptButton.setVisible(false);

			if(validConfiguration)
			{
				// enable booking
				ui_cancelReservationButton.setVisible(false);

				if(sectionList.isEmpty())
				{
					ui_bookButton.setDisable(true);
					ui_reserveButton.setDisable(true);
				}
				else
				{
					ui_bookButton.setDisable(false);
					ui_reserveButton.setDisable(false);
				}
			}
			else
			{
				// show invalid selection warning
				ui_bookButton.setVisible(false);
				ui_reserveButton.setVisible(false);
				ui_cancelReservationButton.setVisible(false);
			}
		}
	}

	private boolean checkSelectionValidity()
	{
		boolean validConfiguration = true;

		// check if selection is valid
		for(Map.Entry<SectionDTO, Long> entry : sectionsToBook.entrySet())
		{
			int numAvailableTickets = EventTools.getNumAvailableTickets(entry.getKey(), event);
			Long ticketsToBook = entry.getValue();

			if(numAvailableTickets < ticketsToBook)
			{
				validConfiguration = false;
				break;
			}
		}

		return validConfiguration;
	}
}
