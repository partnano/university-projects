package at.ac.tuwien.inso.sepm.ticketline.client.gui.customers;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.client.exception.ValidationException;
import at.ac.tuwien.inso.sepm.ticketline.client.gui.MiscAlerts;
import at.ac.tuwien.inso.sepm.ticketline.client.service.CustomerService;
import at.ac.tuwien.inso.sepm.ticketline.rest.customer.CustomerDTO;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.springframework.stereotype.Component;

@Component
public class EditCustomerController
{
	@FXML
	private TextField ui_firstNameInput;
	@FXML
	private TextField ui_lastNameInput;
	@FXML
	private TextField ui_emailInput;

	public Button ui_addCustomerButton;
	private CustomerDTO currentCustomer;
	private CustomerService customerService;

	public void setCurrentCustomer(CustomerDTO currentCustomer)
	{
		this.currentCustomer = currentCustomer;
	}

	public void setCustomerService(CustomerService customerService)
	{
		this.customerService = customerService;
	}

	public void loadContent()
	{
		ui_firstNameInput.setText(currentCustomer.getFirstName());
		ui_lastNameInput.setText(currentCustomer.getLastName());
		ui_emailInput.setText(currentCustomer.getEmail());
	}

	public void ui_addCustomerButtonClick()
	{
		currentCustomer.setFirstName(ui_firstNameInput.getText());
		currentCustomer.setLastName(ui_lastNameInput.getText());
		currentCustomer.setEmail((ui_emailInput.getText()));

		try
		{
			customerService.update(currentCustomer);
			Stage tmp = (Stage)ui_firstNameInput.getScene().getWindow();
			tmp.close();
		}
		catch(ValidationException | DataAccessException e)
		{
			MiscAlerts.showDataAccessAlert();
		}
	}
}
