package at.ac.tuwien.inso.sepm.ticketline.client.service.implementation;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.rest.booking.BookingDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.payment.CreditCardDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class CashPaymentService extends AbstractPaymentService
{
	private static final Logger LOGGER = LoggerFactory.getLogger(CashPaymentService.class);

	@Override
	public BookingDTO pay(BookingDTO bookingDTO, CreditCardDTO creditCardDTO) throws DataAccessException
	{
		LOGGER.info("paying {}", bookingDTO);
		BookingDTO updatedBookingDTO = getBookingRestClient().pay(bookingDTO, creditCardDTO);
		LOGGER.info("paid {}", updatedBookingDTO);
		return updatedBookingDTO;
	}

	public boolean formNeeded()
	{
		return false;
	}
}
