package at.ac.tuwien.inso.sepm.ticketline.client.service;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.client.exception.ValidationException;
import at.ac.tuwien.inso.sepm.ticketline.rest.news.DetailedNewsDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.news.SimpleNewsDTO;
import org.springframework.data.domain.Page;

import java.util.List;

public interface NewsService
{
	/**
	 * Find all news entries.
	 *
	 * @return list of news entries
	 * @throws DataAccessException in case something went wrong
	 */
	List<SimpleNewsDTO> findAll() throws DataAccessException;

	/**
	 * Find page of all news entries.
	 *
	 * @return page of all news entries
	 * @throws DataAccessException in case something went wrong
	 */
	Page<SimpleNewsDTO> findAll(int page, int size) throws DataAccessException;

	/**
	 * Find page of unread news entries.
	 *
	 * @return page of unread news entries
	 * @throws DataAccessException in case something went wrong
	 */
	Page<SimpleNewsDTO> findUnread(int page, int size) throws DataAccessException;

	/**
	 * Publish a new news entry. Only works as admin.
	 *
	 * @param news news to be created.
	 * @return created news
	 * @throws DataAccessException in case something goes wrong (i.e. user is no admin)
	 * @throws ValidationException in case validation of new news entry fails
	 */
	DetailedNewsDTO publishNews(DetailedNewsDTO news) throws DataAccessException, ValidationException;

	/**
	 * Get a detailed version of the news entry given
	 *
	 * @param newsDTO SimpleNewsDTO where DetailedNewsDTO is to be found
	 * @return DetailedNewsDTO
	 * @throws DataAccessException in case something goes wrong
	 */
	DetailedNewsDTO findOne(SimpleNewsDTO newsDTO) throws DataAccessException;

	/**
	 * Update a newsDTO
	 *
	 * @param newsDTO to be updated
	 * @return DetailedNewsDTO
	 * @throws DataAccessException in case something goes wrong
	 */
	DetailedNewsDTO update(DetailedNewsDTO newsDTO) throws DataAccessException, ValidationException;
}
