package at.ac.tuwien.inso.sepm.ticketline.client.gui.news;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.client.exception.ValidationException;
import at.ac.tuwien.inso.sepm.ticketline.client.gui.MiscAlerts;
import at.ac.tuwien.inso.sepm.ticketline.client.service.NewsService;
import at.ac.tuwien.inso.sepm.ticketline.client.util.BundleManager;
import at.ac.tuwien.inso.sepm.ticketline.rest.news.DetailedNewsDTO;
import at.ac.tuwien.inso.springfx.SpringFxmlLoader;
import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Optional;
import java.util.ResourceBundle;

@Component
public class NewsEditorController
{
	private static final Logger LOGGER = LoggerFactory.getLogger(NewsEditorController.class);
	private static final ResourceBundle BUNDLE = BundleManager.getBundle();
	private NewsService service;
	private Stage dialog;

	private byte[] byteImage;

	// FXML objects
	public AnchorPane ui_anchorPane;
	public TextField ui_titleText;
	public TextArea ui_storyTextArea;
	public Button ui_setImageButton;
	public Button ui_publishButton;
	public Label ui_imageLabel;

	public static void showAndWait(Stage stage, SpringFxmlLoader springFxmlLoader, NewsService newsService)
	{
		SpringFxmlLoader.Wrapper<NewsEditorController> wrapper = springFxmlLoader.loadAndWrap(
			"/fxml/news/newsEditor.fxml");
		NewsEditorController controller = wrapper.getController();
		controller.setService(newsService);

		Stage dialog = new Stage();
		controller.setDialog(dialog);

		dialog.addEventHandler(KeyEvent.KEY_RELEASED, (KeyEvent event) ->
		{
			if(KeyCode.ESCAPE == event.getCode())
			{
				dialog.close();
			}
		});

		dialog.initModality(Modality.APPLICATION_MODAL);
		dialog.initOwner(stage);
		dialog.setScene(new Scene((Parent)wrapper.getLoadedObject()));
		dialog.setTitle(BundleManager.getBundle().getString("news.editor.window.title"));
		dialog.showAndWait();
	}

	@FXML
	public void initialize()
	{
		ui_anchorPane.setStyle("-fx-background-color: white");

		ui_titleText.setStyle(
			"-fx-background-insets: 0;" + "-fx-text-box-border: transparent;" + "-fx-focus-color: transparent;"
			+ "-fx-faint-focus-color: transparent;");

		ui_storyTextArea.setStyle("-fx-background-color: -fx-text-box-border, -fx-control-inner-background;"
		                          + "-fx-text-box-border: transparent;" + "-fx-background-insets: 0;"
		                          + "-fx-box-border: none;" + "-fx-focus-color: transparent;"
		                          + "-fx-faint-focus-color: transparent;");
	}

	private void setService(NewsService service)
	{
		this.service = service;
	}

	private void setDialog(Stage dialog)
	{
		this.dialog = dialog;
	}

	// FXML UI FUNCTIONS

	public void setImage()
	{
		LOGGER.debug("calling filechooser to set image");

		FileChooser chooser = new FileChooser();
		FileChooser.ExtensionFilter filter = new FileChooser.ExtensionFilter("Image Files", "*.png", "*.jpg", "*.jpeg");

		chooser.setTitle(BundleManager.getBundle().getString("news.editor.filechooser"));
		chooser.getExtensionFilters().add(filter);
		File image = chooser.showOpenDialog(dialog);

		try
		{
			byteImage = Files.readAllBytes(image.toPath());
		}
		catch(IOException e)
		{
			// shouldn't happen
		}

		LOGGER.info("Opened file chooser.");

		ui_imageLabel.setText(image.getName());
	}

	public void publishNews()
	{
		LOGGER.debug("trying to save news");

		String title = ui_titleText.getText();
		String text = ui_storyTextArea.getText();

		DetailedNewsDTO news = DetailedNewsDTO.builder().title(title).text(text).byteImage(byteImage).build();

		Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
		alert.setTitle(BUNDLE.getString("news.editor.confirm.title"));
		alert.setHeaderText(null);
		alert.setContentText(BUNDLE.getString("news.editor.confirm.content"));

		try
		{
			Optional<ButtonType> result = alert.showAndWait();
			if(result.isPresent() && result.get() == ButtonType.OK)
			{
				service.publishNews(news);
				dialog.close();
			}
		}
		catch(ValidationException ex)
		{
			MiscAlerts.showValidationErrorAlert(ex);
		}
		catch(DataAccessException ex)
		{
			MiscAlerts.showDataAccessAlert();
		}
	}
}
