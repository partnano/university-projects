package at.ac.tuwien.inso.sepm.ticketline.client.rest.implementation;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.client.rest.UserRestClient;
import at.ac.tuwien.inso.sepm.ticketline.rest.page.RestResponsePage;
import at.ac.tuwien.inso.sepm.ticketline.rest.user.UserDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientException;

@Component
public class SimpleUserRestClient implements UserRestClient
{
	private static final Logger LOGGER = LoggerFactory.getLogger(SimpleUserRestClient.class);

	private static final String USERS_URL = "/users";

	private final RestClient restClient;

	public SimpleUserRestClient(RestClient restClient)
	{
		this.restClient = restClient;
	}

	@Override
	public RestResponsePage<UserDTO> findAll(int page, int size) throws DataAccessException
	{

		try
		{
			LOGGER.debug("Retrieving page of all users from {}", restClient.getServiceURI(USERS_URL));

			ResponseEntity<RestResponsePage<UserDTO>> users = restClient.exchange(restClient.getServiceURI(
				USERS_URL + "?page=" + page + "&size=" + size), HttpMethod.GET,
			                                                                      null,
			                                                                      new ParameterizedTypeReference<RestResponsePage<UserDTO>>()
			                                                                      {
			                                                                      });

			LOGGER.debug("Result status was {} ", users.getStatusCode());
			return users.getBody();
		}
		catch(HttpStatusCodeException e)
		{
			throw new DataAccessException("Failed retrieve users with status code " + e.getStatusCode());
		}
		catch(RestClientException e)
		{
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	public UserDTO create(UserDTO userDTO) throws DataAccessException
	{
		try
		{
			LOGGER.debug("creating new user ...");

			ResponseEntity<UserDTO> result = restClient.exchange(restClient.getServiceURI(USERS_URL),
			                                                     HttpMethod.POST,
			                                                     new HttpEntity<>(userDTO),
			                                                     UserDTO.class);

			LOGGER.debug("... successfully created new user, id = {}", result.getBody().getId());

			return result.getBody();
		}
		catch(RestClientException ex)
		{
			throw new DataAccessException(ex.getMessage(), ex);
		}
	}

	@Override
	public UserDTO update(UserDTO userDTO) throws DataAccessException
	{
		try
		{
			LOGGER.debug("updating user {} ...", userDTO);

			ResponseEntity<UserDTO> result = restClient.exchange(
				restClient.getServiceURI(USERS_URL) + "/" + userDTO.getId(),
				HttpMethod.POST,
				new HttpEntity<>(userDTO),
				UserDTO.class);

			LOGGER.debug("... user {} updated successfully", result.getBody());

			return result.getBody();
		}
		catch(RestClientException ex)
		{
			throw new DataAccessException(ex.getMessage(), ex);
		}
	}

	@Override
	public void resetPassword(long id) throws DataAccessException
	{
		try
		{
			LOGGER.debug("resetting password for user {} ...", id);

			restClient.exchange(restClient.getServiceURI(USERS_URL) + String.format("/%s/resetpw", id),
			                    HttpMethod.POST,
			                    null,
			                    Object.class);

			LOGGER.debug("... successfully reset password for user {}", id);
		}
		catch(RestClientException ex)
		{
			throw new DataAccessException(ex.getMessage(), ex);
		}
	}
}
