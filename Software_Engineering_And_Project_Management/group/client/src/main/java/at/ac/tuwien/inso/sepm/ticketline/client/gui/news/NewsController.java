package at.ac.tuwien.inso.sepm.ticketline.client.gui.news;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.client.exception.ValidationException;
import at.ac.tuwien.inso.sepm.ticketline.client.gui.MiscAlerts;
import at.ac.tuwien.inso.sepm.ticketline.client.gui.TabHeaderController;
import at.ac.tuwien.inso.sepm.ticketline.client.service.AuthenticationInformationService;
import at.ac.tuwien.inso.sepm.ticketline.client.service.NewsService;
import at.ac.tuwien.inso.sepm.ticketline.client.util.BundleManager;
import at.ac.tuwien.inso.sepm.ticketline.rest.news.DetailedNewsDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.news.SimpleNewsDTO;
import at.ac.tuwien.inso.springfx.SpringFxmlLoader;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import javafx.stage.Stage;
import org.controlsfx.glyphfont.FontAwesome;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import java.io.ByteArrayInputStream;
import java.time.format.DateTimeFormatter;

@Component
public class NewsController
{

	private static final Logger LOGGER = LoggerFactory.getLogger(NewsController.class);
	public ListView<SimpleNewsDTO> ui_newsListView;
	public BorderPane ui_newsPane;
	public Label ui_newsTypeLabel;
	public Button ui_changeNewsViewButton;
	public Button ui_addNewsButton;
	public HBox ui_paginationControlBox;
	public Button ui_firstButton;
	public Button ui_previousButton;
	public Label ui_paginationLabel;
	public Button ui_nextButton;
	public Button ui_lastButton;
	public ComboBox<Integer> ui_paginationDropdown;
	private ObservableList<SimpleNewsDTO> newsList;
	private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");

	private boolean showAllNews = false;
	private int selectedIndex = -1;
	private int currentPage = 1;
	private final FontAwesome fontAwesome;
	private int elementsPerPage = 5;

	@FXML
	private TabHeaderController tabHeaderController;

	private final SpringFxmlLoader springFxmlLoader;
	private final NewsService newsService;

	private final AuthenticationInformationService authService;

	public NewsController(SpringFxmlLoader springFxmlLoader, NewsService newsService,
	                      AuthenticationInformationService authService)
	{
		this.springFxmlLoader = springFxmlLoader;
		this.newsService = newsService;
		this.authService = authService;
		fontAwesome = new FontAwesome();
	}

	@FXML
	private void initialize()
	{
		ui_paginationDropdown.setPromptText(BundleManager.getBundle().getString("pagination.dropdownText"));
		ui_paginationDropdown.getItems().addAll(5, 10, 20, 50, 100, 200);

		ui_paginationDropdown.getSelectionModel()
		                     .selectedItemProperty()
		                     .addListener((options, oldValue, newValue) -> {
			                     if(newValue == null)
			                     {
				                     elementsPerPage = 5;
				                     loadNews();
			                     }
			                     else
			                     {
				                     elementsPerPage = newValue;
				                     loadNews();
			                     }
		                     });

		tabHeaderController.setIcon(FontAwesome.Glyph.NEWSPAPER_ALT);
		tabHeaderController.setTitle("News");
		newsList = FXCollections.observableArrayList();
		ui_newsListView.setItems(newsList);

		ui_newsListView.getSelectionModel()
		               .selectedItemProperty()
		               .addListener((observable, oldValue, newValue) -> {

			               if(ui_newsListView.getSelectionModel().getSelectedItem() != null
			                  && ui_newsListView.getSelectionModel().getSelectedIndex() != selectedIndex)
			               {
				               createNewsDetailView(newValue);
				               selectedIndex = ui_newsListView.getSelectionModel().getSelectedIndex();
			               }
		               });

		ui_changeNewsViewButton.setText(BundleManager.getBundle().getString("news.newsType.all"));
		ui_newsTypeLabel.setText(BundleManager.getBundle().getString("news.newsType.unread"));

		ui_newsListView.setCellFactory(cellData -> new NewsListCell());
	}

	private void createNewsDetailView(SimpleNewsDTO newsDTO)
	{
		ui_newsPane.getChildren().clear();

		LOGGER.debug("creating detail view...");

		DetailedNewsDTO selected = null;
		try
		{
			newsDTO.setRead(true);
			selected = newsService.findOne(newsDTO);

			selected.setRead(true);
			newsService.update(selected);

			//need to do this because listviews are stupid
			//Platform.runLater(this::loadNews);
		}
		catch(DataAccessException e)
		{
			MiscAlerts.showDataAccessAlert();
		}
		catch(ValidationException e)
		{
			MiscAlerts.showValidationErrorAlert(e);
		}

		Label headlineLabel = new Label(selected.getTitle());
		headlineLabel.setStyle("-fx-font-weight: bold; -fx-font-size: 24");
		headlineLabel.setWrapText(true);

		Text dateText = new Text(selected.getPublishedAt().format(formatter) + ", ");
		dateText.setStyle("-fx-font-weight: bold");

		Text newsText = new Text(selected.getText());

		TextFlow content = new TextFlow();

		BorderPane contentPane = new BorderPane();

		if(selected.getByteImage() != null)
		{
			Image i = new Image(new ByteArrayInputStream(selected.getByteImage()));
			ImageView imageView = new ImageView(i);
			imageView.setFitHeight(200);
			imageView.setPreserveRatio(true);

			contentPane.setTop(imageView);
			BorderPane.setMargin(imageView, new Insets(20, 0, 20, 0));
			ui_newsPane.setTop(imageView);
		}

		BorderPane.setMargin(headlineLabel, new Insets(20, 0, 20, 0));

		content.getChildren().addAll(dateText, newsText);
		contentPane.setCenter(content);

		contentPane.setTop(headlineLabel);
		ui_newsPane.setCenter(contentPane);
	}

	public void loadNews()
	{
		if(!authService.isAdmin())
			ui_addNewsButton.setVisible(false);

		newsList.clear();
		LOGGER.info("loading news ... ");
		try
		{
			Page<SimpleNewsDTO> newsDTOPage;

			if(showAllNews) // show all news
			{
				ui_changeNewsViewButton.setText(BundleManager.getBundle().getString("news.newsType.unread"));
				ui_newsTypeLabel.setText(BundleManager.getBundle().getString("news.newsType.all"));
				newsDTOPage = newsService.findAll(currentPage - 1, elementsPerPage);
			}
			else // show unreads
			{
				ui_changeNewsViewButton.setText(BundleManager.getBundle().getString("news.newsType.all"));
				ui_newsTypeLabel.setText(BundleManager.getBundle().getString("news.newsType.unread"));
				newsDTOPage = newsService.findUnread(currentPage - 1, elementsPerPage);
			}

			int numPages = newsDTOPage.getTotalPages();

			ui_paginationLabel.setText(currentPage + "/" + numPages);

			ui_firstButton.setText(BundleManager.getBundle().getString("pagination.first"));
			ui_firstButton.setOnAction(event -> {
				currentPage = 1;
				selectedIndex = -1;
				ui_newsListView.getSelectionModel().select(selectedIndex);

				loadNews();
			});

			ui_previousButton.setGraphic(fontAwesome.create(FontAwesome.Glyph.CHEVRON_LEFT));
			ui_previousButton.setOnAction(event -> {
				if(currentPage > 1)
				{
					currentPage--;
					selectedIndex = -1;
					ui_newsListView.getSelectionModel().select(selectedIndex);
				}
				loadNews();
			});

			ui_lastButton.setText(BundleManager.getBundle().getString("pagination.last"));
			ui_nextButton.setGraphic(fontAwesome.create(FontAwesome.Glyph.CHEVRON_RIGHT));

			ui_lastButton.setOnAction(event -> {
				currentPage = numPages;
				selectedIndex = -1;
				ui_newsListView.getSelectionModel().select(selectedIndex);

				loadNews();
			});
			ui_nextButton.setOnAction(event -> {
				if(currentPage < numPages)
				{
					currentPage++;
					selectedIndex = -1;
					ui_newsListView.getSelectionModel().select(selectedIndex);
				}
				loadNews();
			});

			LOGGER.debug("Pages: " + currentPage + " " + numPages);

			if(currentPage >= numPages)
			{
				ui_nextButton.setDisable(true);
				ui_lastButton.setDisable(true);
			}
			else
			{
				ui_nextButton.setDisable(false);
				ui_lastButton.setDisable(false);
			}

			if(currentPage <= 1)
			{
				ui_previousButton.setDisable(true);
				ui_firstButton.setDisable(true);
			}
			else
			{
				ui_previousButton.setDisable(false);
				ui_firstButton.setDisable(false);
			}

			newsList.clear();
			newsList.addAll(newsDTOPage.getContent());

			if(showAllNews)
				ui_newsListView.getSelectionModel().select(selectedIndex);
			else
			{
				selectedIndex = -1;
				ui_newsListView.getSelectionModel().select(selectedIndex);
			}
		}
		catch(DataAccessException e)
		{
			MiscAlerts.showDataAccessAlert();
		}
	}

	//Toggles between displaying all and unread news
	public void onChangeNewsViewButtonClick()
	{
		LOGGER.debug(ui_changeNewsViewButton.getText(), BundleManager.getBundle().getString("news.newsType.all"));

		if(ui_changeNewsViewButton.getText().equals(BundleManager.getBundle().getString("news.newsType.all")))
		{
			showAllNews = true;
			currentPage = 1;
		}
		else
		{
			showAllNews = false;
			currentPage = 1;
		}
		loadNews();
	}

	public void onAddNewsButtonClick()
	{
		LOGGER.debug("add news button clicked");
		Stage stage = (Stage)ui_newsListView.getScene().getWindow();
		NewsEditorController.showAndWait(stage, springFxmlLoader, newsService);
		loadNews();
	}
}
