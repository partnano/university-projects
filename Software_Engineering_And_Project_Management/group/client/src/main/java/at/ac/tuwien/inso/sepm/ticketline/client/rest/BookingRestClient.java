package at.ac.tuwien.inso.sepm.ticketline.client.rest;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.rest.booking.BookingDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.customer.CustomerDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.payment.CreditCardDTO;

import java.util.List;

public interface BookingRestClient
{
	/**
	 * Create a new booking
	 *
	 * @param booking to be created
	 * @return created booking
	 * @throws DataAccessException in case something goes wrong
	 */
	BookingDTO create(BookingDTO booking) throws DataAccessException;

	/**
	 * Update a booking
	 *
	 * @param booking to be updated
	 * @return updated booking
	 * @throws DataAccessException in case something goes wrong
	 */
	BookingDTO update(BookingDTO booking) throws DataAccessException;

	/**
	 * Retrieves booking to specific reservationNumber
	 *
	 * @param reservationNumber the number to be used to look up the bookings
	 * @return matched booking
	 */
	BookingDTO findByReservationNumber(int reservationNumber) throws DataAccessException;

	/**
	 * Retrieves all bookings of the specified customer
	 *
	 * @param customer the customer to be used to look up the bookings
	 * @return all matching bookings
	 * @throws DataAccessException in case something goes wrong
	 */
	List<BookingDTO> findAll(CustomerDTO customer) throws DataAccessException;

	/**
	 * Requests the withdrawal of the booking price from the given credit card.
	 *
	 * @param bookingDTO    to be paid
	 * @param creditCardDTO credit card to be used
	 * @return the updated booking
	 * @throws DataAccessException in case something goes wrong
	 */
	BookingDTO pay(BookingDTO bookingDTO, CreditCardDTO creditCardDTO) throws DataAccessException;

	/**
	 * Requests the paid booking to be refunded and canceled.
	 *
	 * @param bookingDTO to be refunded
	 * @return the updated booking
	 * @throws DataAccessException in case something goes wrong
	 */
	BookingDTO refund(BookingDTO bookingDTO) throws DataAccessException;
}
