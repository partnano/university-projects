package at.ac.tuwien.inso.sepm.ticketline.client.rest.implementation;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.client.rest.EventRestClient;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventFilterDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.Top10EventDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.enums.Category;
import at.ac.tuwien.inso.sepm.ticketline.rest.page.RestResponsePage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientException;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class SimpleEventRestClient implements EventRestClient
{
	private static final Logger LOGGER = LoggerFactory.getLogger(SimpleEventRestClient.class);
	private static final String EVENTS_URL = "/events";

	private final RestClient restClient;

	public SimpleEventRestClient(RestClient restClient)
	{
		this.restClient = restClient;
	}

	@Override
	public List<EventDTO> findAll() throws DataAccessException
	{
		try
		{
			LOGGER.debug("Retrieving all events from {}", restClient.getServiceURI(EVENTS_URL));

			ResponseEntity<List<EventDTO>> events = restClient.exchange(restClient.getServiceURI(EVENTS_URL),
			                                                            HttpMethod.GET,
			                                                            null,
			                                                            new ParameterizedTypeReference<List<EventDTO>>()
			                                                            {
			                                                            });

			LOGGER.debug("Result status was {}", events.getStatusCode());
			return events.getBody();
		}
		catch(HttpStatusCodeException e)
		{
			throw new DataAccessException("Failed retrieve events with status code " + e.getStatusCode());
		}
		catch(RestClientException e)
		{
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	@Override
	public RestResponsePage<EventDTO> findAll(int page, int size) throws DataAccessException
	{
		try
		{
			LOGGER.debug("Retrieving page of all events from {}", restClient.getServiceURI(EVENTS_URL));

			ResponseEntity<RestResponsePage<EventDTO>> events = restClient.exchange(restClient.getServiceURI(
				EVENTS_URL + "?page=" + page + "&size=" + size), HttpMethod.GET,
			                                                                        null,
			                                                                        new ParameterizedTypeReference<RestResponsePage<EventDTO>>()
			                                                                        {
			                                                                        });

			LOGGER.debug("Result status was {} ", events.getStatusCode());
			return events.getBody();
		}
		catch(HttpStatusCodeException e)
		{
			throw new DataAccessException("Failed retrieve events with status code " + e.getStatusCode());
		}
		catch(RestClientException e)
		{
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	@Override
	public EventDTO findOne(Long id) throws DataAccessException
	{
		try
		{
			LOGGER.debug("retrieving event with id {} ...", id);

			ResponseEntity<EventDTO> result = restClient.exchange(restClient.getServiceURI(
				EVENTS_URL + "/" + id), HttpMethod.GET, null, EventDTO.class);

			LOGGER.debug("... sucessfully retrieved event with id {}", id);

			return result.getBody();
		}
		catch(RestClientException ex)
		{
			throw new DataAccessException(ex.getMessage(), ex);
		}
	}

	@Override
	public List<EventDTO> findByFilter(EventFilterDTO filter) throws DataAccessException
	{
		if(filter == null)
			throw new DataAccessException("EventFilterDTO is null");

		try
		{
			LOGGER.debug("Retrieving filtered events from {} with filter {}",
			             restClient.getServiceURI(EVENTS_URL),
			             filter);

			ResponseEntity<List<EventDTO>> events = restClient.exchange(restClient.getServiceURI(
				EVENTS_URL + "/filtered"),
			                                                            HttpMethod.POST,
			                                                            new HttpEntity<>(filter),
			                                                            new ParameterizedTypeReference<List<EventDTO>>()
			                                                            {
			                                                            });

			LOGGER.debug("Result status was {}", events.getStatusCode());
			return events.getBody();
		}
		catch(HttpStatusCodeException e)
		{
			throw new DataAccessException("Failed retrieve events with status code " + e.getStatusCode());
		}
		catch(RestClientException e)
		{
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	@Override
	public RestResponsePage<EventDTO> findByFilter(EventFilterDTO filter, int page, int size) throws DataAccessException
	{
		try
		{
			LOGGER.debug("Retrieving page of filtered events from {}", restClient.getServiceURI(EVENTS_URL));

			ResponseEntity<RestResponsePage<EventDTO>> events = restClient.exchange(restClient.getServiceURI(
					EVENTS_URL + "/filtered?page=" + page + "&size=" + size), HttpMethod.POST,
			                                                                        new HttpEntity<>(filter),
			                                                                        new ParameterizedTypeReference<RestResponsePage<EventDTO>>()
			                                                                        {
			                                                                        });

			LOGGER.debug("Result status was {} ", events.getStatusCode());
			return events.getBody();
		}
		catch(HttpStatusCodeException e)
		{
			throw new DataAccessException("Failed retrieve events with status code " + e.getStatusCode());
		}
		catch(RestClientException e)
		{
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	@Override
	public Map<EventDTO, Integer> findTopEvents(Category category, Integer month, Integer year, int limit) throws
	                                                                                                       DataAccessException
	{
		try
		{
			LOGGER.debug("Retrieving top {} events", limit);

			StringBuilder params = new StringBuilder();

			if(category != null)
			{
				params.append("category=");
				params.append(category.name());
				params.append('&');
			}

			if(month != null)
			{
				params.append("month=");
				params.append(month);
				params.append('&');
			}

			if(year != null)
			{
				params.append("year=");
				params.append(year);
				params.append('&');
			}

			params.append("limit=");
			params.append(limit);

			ResponseEntity<List<Top10EventDTO>> events = restClient.exchange(restClient.getServiceURI(
				EVENTS_URL + "/top?" + params.toString()), HttpMethod.GET,
			                                                                 null,
			                                                                 new ParameterizedTypeReference<List<Top10EventDTO>>()
			                                                                 {
			                                                                 });

			LOGGER.debug("Result status was {}", events.getStatusCode());

			return events.getBody()
			             .stream()
			             .collect(Collectors.toMap(Top10EventDTO::getEvent, Top10EventDTO::getBookings));
		}
		catch(HttpStatusCodeException ex)
		{
			throw new DataAccessException("Failed to retrieve events with status code " + ex.getStatusCode());
		}
		catch(RestClientException ex)
		{
			throw new DataAccessException(ex.getMessage(), ex);
		}
	}

	@Override
	public EventDTO create(EventDTO event) throws DataAccessException
	{
		if(event == null)
			throw new DataAccessException("Event to be created is null");

		try
		{
			LOGGER.debug("Creating new event on {}", restClient.getServiceURI(EVENTS_URL));

			ResponseEntity<EventDTO> createdEvent = restClient.exchange(restClient.getServiceURI(EVENTS_URL),
			                                                            HttpMethod.POST,
			                                                            new HttpEntity<>(event),
			                                                            new ParameterizedTypeReference<EventDTO>()
			                                                            {
			                                                            });

			LOGGER.debug("Result status was {}", createdEvent.getStatusCode());

			return createdEvent.getBody();
		}
		catch(HttpStatusCodeException e)
		{
			throw new DataAccessException(
				"Failed create/retrieve events with status code " + e.getStatusCode());
		}
		catch(RestClientException e)
		{
			throw new DataAccessException(e.getMessage(), e);
		}
	}
}
