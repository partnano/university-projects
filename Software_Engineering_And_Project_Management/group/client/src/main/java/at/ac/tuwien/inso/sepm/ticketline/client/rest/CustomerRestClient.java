package at.ac.tuwien.inso.sepm.ticketline.client.rest;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.rest.customer.CustomerDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.page.RestResponsePage;

import java.util.List;

public interface CustomerRestClient
{
	/**
	 * Find all customers
	 *
	 * @return List of customers
	 *
	 * @throws DataAccessException in case something goes wrong
	 */
	List<CustomerDTO> findAll() throws DataAccessException;

	/**
	 * Find page of all customers
	 *
	 * @return Page of customers
	 *
	 * @throws DataAccessException in case something goes wrong
	 */
	RestResponsePage<CustomerDTO> findAll(int page, int size) throws DataAccessException;

	/**
	 * Find a specific customer
	 *
	 * @param id for identification of customer to be found
	 *
	 * @return specified customer
	 *
	 * @throws DataAccessException in case something goes wrong
	 */
	CustomerDTO findOne(Long id) throws DataAccessException;

	/**
	 * Create a new customer
	 *
	 * @param customer to be created
	 *
	 * @return created customer
	 *
	 * @throws DataAccessException in case something goes wrong
	 */
	CustomerDTO create(CustomerDTO customer) throws DataAccessException;

	/**
	 * Update a customer
	 *
	 * @param customer to be updated
	 *
	 * @return updated customer
	 *
	 * @throws DataAccessException in case something goes wrong
	 */
	CustomerDTO update(CustomerDTO customer) throws DataAccessException;
}
