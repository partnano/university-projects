package at.ac.tuwien.inso.sepm.ticketline.client.service;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.client.exception.ValidationException;
import at.ac.tuwien.inso.sepm.ticketline.rest.user.UserDTO;
import org.springframework.data.domain.Page;

public interface UserService
{
	Page<UserDTO> findAll(int page, int size) throws DataAccessException;

	UserDTO create(UserDTO userDTO) throws DataAccessException, ValidationException;

	UserDTO update(UserDTO userDTO) throws DataAccessException, ValidationException;

	void resetPassword(long id) throws DataAccessException, ValidationException;
}
