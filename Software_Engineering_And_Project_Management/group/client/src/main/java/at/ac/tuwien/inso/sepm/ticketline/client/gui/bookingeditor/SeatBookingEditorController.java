package at.ac.tuwien.inso.sepm.ticketline.client.gui.bookingeditor;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.client.gui.MiscAlerts;
import at.ac.tuwien.inso.sepm.ticketline.client.gui.locationplan.LocationPlanController;
import at.ac.tuwien.inso.sepm.ticketline.client.gui.locationplan.SelectionChangedEvent;
import at.ac.tuwien.inso.sepm.ticketline.client.gui.payment.PaymentController;
import at.ac.tuwien.inso.sepm.ticketline.client.service.BookingService;
import at.ac.tuwien.inso.sepm.ticketline.client.service.EventService;
import at.ac.tuwien.inso.sepm.ticketline.client.util.BundleManager;
import at.ac.tuwien.inso.sepm.ticketline.client.util.EventTools;
import at.ac.tuwien.inso.sepm.ticketline.rest.booking.BookingDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.booking.SeatBookingDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.customer.CustomerDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventAreaDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.location.AreaDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.location.RowDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.location.SeatDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.payment.PaymentProvider;
import at.ac.tuwien.inso.springfx.SpringFxmlLoader;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.Set;

@Component
public class SeatBookingEditorController extends AbstractBookingEditorController
{
	private static final Logger LOGGER = LoggerFactory.getLogger(SeatBookingEditorController.class);

	private ObservableList<SeatDTO> selectedSeats = FXCollections.observableArrayList();

	public SeatBookingEditorController(EventService eventService, BookingService bookingService)
	{
		super(eventService, bookingService);
	}

	@Override
	protected BookingDTO getBookingWithSelectedAreas()
	{
		SeatBookingDTO booking = new SeatBookingDTO();
		booking.setSeats(Set.of(selectedSeats.toArray(new SeatDTO[0])));
		return booking;
	}

	public static EventDTO showAndWait(Stage stage, SpringFxmlLoader springFxmlLoader, EventDTO event)
	{
		LOGGER.debug("creating new BookingEditorController");
		Stage dialog = createDialog(stage);

		dialog.addEventHandler(KeyEvent.KEY_RELEASED, (KeyEvent keyEvent) -> {
			if(KeyCode.ESCAPE == keyEvent.getCode())
			{
				dialog.close();
			}
		});

		SpringFxmlLoader.Wrapper<SeatBookingEditorController> wrapper = springFxmlLoader.loadAndWrap(
				"/fxml/bookingeditor/seatBookingEditor.fxml");

		SeatBookingEditorController controller = wrapper.getController();
		controller.setEvent(event);

		createAndSetLegend(stage, controller, springFxmlLoader);

		controller.dialog = dialog;

		controller.assignCustomerController = BookingAssignCustomerController.create(stage,
		                                                                             springFxmlLoader,
		                                                                             controller::onCustomerAssignedToBooking);

		controller.paymentController = PaymentController.create(stage, springFxmlLoader, controller::onBookingPaid);

		controller.customInitialization(springFxmlLoader, null);

		dialog.setScene(new Scene((Parent)wrapper.getLoadedObject()));
		dialog.setTitle(BundleManager.getBundle().getString("bookingeditor.title"));

		dialog.showAndWait();
		controller.ui_selectedSeatsTable.getItems().clear();

		return controller.getEvent();
	}

	public static void showAndWait(Stage stage, SpringFxmlLoader springFxmlLoader, SeatBookingDTO booking)
	{
		LOGGER.debug("creating new BookingEditorController");
		Stage dialog = createDialog(stage);

		SpringFxmlLoader.Wrapper<SeatBookingEditorController> wrapper = springFxmlLoader.loadAndWrap(
				"/fxml/bookingeditor/seatBookingEditor.fxml");

		SeatBookingEditorController controller = wrapper.getController();

		try
		{
			bookingService.populateBookingEvent(booking);
		}
		catch(DataAccessException e)
		{
			MiscAlerts.showDataAccessAlert();
		}

		controller.setEvent(booking.getEvent());

		createAndSetLegend(stage, controller, springFxmlLoader);

		controller.dialog = dialog;

		controller.assignCustomerController = BookingAssignCustomerController.create(stage,
		                                                                             springFxmlLoader,
		                                                                             controller::onCustomerAssignedToBooking);

		controller.paymentController = PaymentController.create(stage, springFxmlLoader, controller::onBookingPaid);

		controller.customInitialization(springFxmlLoader, booking);

		dialog.setScene(new Scene((Parent)wrapper.getLoadedObject()));
		dialog.setTitle(BundleManager.getBundle().getString("bookingeditor.title"));

		dialog.showAndWait();
		controller.ui_selectedSeatsTable.getItems().clear();
	}

	// suppress because ui_selectedSeatsTable takes either Seats or Sections
	@SuppressWarnings("unchecked")
	private void customInitialization(SpringFxmlLoader springFxmlLoader, SeatBookingDTO booking)
	{
		LOGGER.info("Event consists of Seats");

		selectedSeats = FXCollections.observableArrayList();
		ui_selectedSeatsTable.setItems(selectedSeats);

		TableColumn<SeatDTO, String> ui_seatColumn = new TableColumn<>(BundleManager.getBundle()
		                                                                            .getString(
				                                                                            "bookingeditor.seattable.seat"));
		ui_seatColumn.setStyle("-fx-alignment: CENTER;");
		ui_seatColumn.setCellValueFactory(cellData -> new SimpleObjectProperty<>(cellData.getValue().getName()));

		TableColumn<SeatDTO, String> ui_rowColumn = new TableColumn<>(BundleManager.getBundle()
		                                                                           .getString(
				                                                                           "bookingeditor.seattable.row"));
		ui_rowColumn.setStyle("-fx-alignment: CENTER;");

		ui_rowColumn.setCellValueFactory(cellData -> {
			for(AreaDTO areaDTO : event.getLocation().getAreas())
			{
				RowDTO row = (RowDTO)areaDTO;
				for(SeatDTO s : row.getSeats())
				{
					if(s.getId().equals(cellData.getValue().getId()))
					{
						return new SimpleObjectProperty<>(row.getName());
					}
				}
			}
			return new SimpleObjectProperty<>("");
		});

		TableColumn<SeatDTO, String> ui_priceColumn = new TableColumn<>(BundleManager.getBundle()
		                                                                             .getString(
				                                                                             "bookingeditor.seattable.price"));
		ui_priceColumn.setStyle("-fx-alignment: CENTER;");
		ui_priceColumn.setCellValueFactory(cellData -> {

			for(EventAreaDTO eventAreaDTO : event.getEventAreas())
			{
				RowDTO row = (RowDTO)eventAreaDTO.getArea();
				for(SeatDTO s : row.getSeats())
				{
					if(s.getId().equals(cellData.getValue().getId()))
					{
						return new SimpleObjectProperty<>(String.format("%.2f %s",
						                                                eventAreaDTO.getPrice(),
						                                                BundleManager.getBundle()
						                                                             .getString("currency")));
					}
				}
			}
			return new SimpleObjectProperty<>("0 " + BundleManager.getBundle().getString("currency"));
		});

		ui_selectedSeatsTable.getColumns().add(ui_seatColumn);
		ui_selectedSeatsTable.getColumns().add(ui_rowColumn);
		ui_selectedSeatsTable.getColumns().add(ui_priceColumn);

		SpringFxmlLoader.Wrapper<LocationPlanController> locationPlan = LocationPlanController.createWrapper(
				springFxmlLoader,
				event,
				this::seatClicked);
		locationPlanController = locationPlan.getController();

		setBooking(booking);

		createLocationPlan(locationPlan);
	}

	private void setBooking(SeatBookingDTO booking)
	{
		super.setBooking(booking);

		selectedSeats.clear();

		if(booking != null)
		{
			locationPlanController.setSelection(booking.getSeats());
			selectedSeats.addAll(booking.getSeats());
		}
		else
		{
			locationPlanController.clearSelection();
		}

		updateBookingInfo();
		updateButtonVisibility();
	}

	@Override
	protected void onBookingSaved(BookingDTO booking)
	{
		if(booking instanceof SeatBookingDTO)
			setBooking((SeatBookingDTO)booking);
		else
			throw new AssertionError(
					"Saved booking must be of type SeatBookingDTO but was " + booking.getClass().toString());
	}

	/**
	 * Alter the current selection to improve usability by optimizing the following things:
	 * - only whole bookings may be selected
	 * - only one booking may be selected at a time
	 */
	private void seatClicked(SelectionChangedEvent e)
	{
		SeatDTO lastClickedSeat = e.getLastClickedSeat();
		SeatBookingDTO seatBooking = (SeatBookingDTO)EventTools.getBookingForSeat(lastClickedSeat, event);

		// available seat clicked
		if(seatBooking == null)
		{
			// all previously selected seats were available
			if(booking == null)
			{
				selectedSeats.clear();
				selectedSeats.addAll(e.getSeats());
			}
			else
			{
				setBooking(null);
				locationPlanController.setSelection(Collections.singleton(lastClickedSeat));
				selectedSeats.addAll(lastClickedSeat);
			}
		}
		else
		{
			// seat of same booking was selected -> deselect booking
			if(seatBooking == booking)
			{
				setBooking(null);
			}
			else
			{
				// seat of different booking was selected -> select new booking
				setBooking(seatBooking);
			}
		}

		updateBookingInfo();
		updateButtonVisibility();
	}

	@Override
	protected void updateBookingInfo()
	{
		ui_infoPane.setVisible(true);

		ui_detailsPane.getChildren().clear();

		Float price = 0f;

		for(Object o : ui_selectedSeatsTable.getItems())
		{
			if(o instanceof SeatDTO)
			{
				for(EventAreaDTO eventAreaDTO : event.getEventAreas())
				{
					RowDTO row = (RowDTO)eventAreaDTO.getArea();
					for(SeatDTO s : row.getSeats())
					{
						if(s.getId().equals(((SeatDTO)o).getId()))
						{
							price += eventAreaDTO.getPrice();
						}
					}
				}
			}
		}

		ui_detailsPane.add(new Text(BundleManager.getBundle().getString("bookingEditor.detailsPane.totalPrice")), 0, 0);
		ui_detailsPane.add(new Text(String.format("%.2f €", price)), 1, 0);

		if(booking == null)
		{

			ui_detailsPane.add(new Text(BundleManager.getBundle().getString("bookingeditor.detailsPane.statusLabel")),
			                   0,
			                   1);
			if(selectedSeats.isEmpty())
			{

				ui_detailsPane.add(new Text(BundleManager.getBundle().getString("bookingStatus.empty")), 1, 1);
			}
			else
			{
				ui_detailsPane.add(new Text(BundleManager.getBundle().getString("bookingStatus.available")),
				                   1,
				                   1);
			}
		}
		else
		{

			String statusIdentifier = "";

			switch(booking.getBookingStatus())
			{
			case RESERVED:
				statusIdentifier = "bookingStatus.reserved";
				break;
			case BOOKED:
				statusIdentifier = "bookingStatus.booked";
				break;
			case CANCELED:
				statusIdentifier = "bookingStatus.canceled";
				break;
			}

			ui_detailsPane.add(new Text(BundleManager.getBundle().getString("bookingeditor.detailsPane.statusLabel")),
			                   0,
			                   1);
			ui_detailsPane.add(new Text(BundleManager.getBundle().getString(statusIdentifier)), 1, 1);

			ui_detailsPane.add(new Text(BundleManager.getBundle()
			                                         .getString("bookingeditor.detailsPane.reservationNumberLabel")),
			                   0,
			                   2);
			ui_detailsPane.add(new Text(booking.getReservationNumber() + ""), 1, 2);

			ui_detailsPane.add(new Text(BundleManager.getBundle().getString("bookingeditor.detailsPane.customerLabel")),
			                   0,
			                   3);
			CustomerDTO customer = booking.getCustomer();
			if(customer != null)
			{
				if(customer.getFirstName() != null && customer.getLastName() != null)
				{

					ui_detailsPane.add(new Text(customer.getLastName() + " " + customer.getFirstName() + " (" + customer.getCustomerNumber() + ")"), 1, 3);
				}
				else
				{
					ui_detailsPane.add(new Text(BundleManager.getBundle().getString("customer.anonymous") + " (" + customer.getCustomerNumber() + ")"), 1, 3);
				}
			}
			else
			{
				ui_detailsPane.add(new Text(BundleManager.getBundle().getString("bookingeditor.detailsPane.anonymous")),
				                   1,
				                   3);
			}

			if(booking.getBookingStatus() == BookingDTO.BookingStatus.BOOKED)
			{
				ui_detailsPane.add(new Text(BundleManager.getBundle()
				                                         .getString(
					                                         "bookingeditor.detailsPane.paymentMethod")),
				                   0,
				                   4);

				ui_detailsPane.add(new Text(BundleManager.getBundle()
				                                         .getString(PaymentProvider.valueOf(booking.getPaymentMethod())
				                                                                   .getName())), 1, 4);
			}
		}
	}

	private void updateButtonVisibility()
	{
		if(booking != null)
		{
			ui_reserveButton.setVisible(false);

			if(booking.isCanceled())
			{
				// canceled booking

				// -> hide cancel button
				ui_cancelReservationButton.setVisible(false);

				// -> change printReceiptButton text to print cancelation receipt
				ui_printReceiptButton.setText(BundleManager.getBundle()
				                                           .getString("bookingEditor.printCancellationReceiptButton"));
			}
			else
			{
				// regular booking or reservation
				ui_printReceiptButton.setText(BundleManager.getBundle().getString("bookingEditor.printReceiptButton"));

				ui_cancelReservationButton.setVisible(true);
			}

			// reservation -> show booking button, to enable conversion to booking
			if(booking.getBookingStatus() == BookingDTO.BookingStatus.RESERVED)
			{
				ui_bookButton.setVisible(true);
				ui_bookButton.setDisable(false);
			}
			else
			{
				ui_bookButton.setVisible(false);
			}

			if(booking.isPaid())
				ui_printReceiptButton.setVisible(true);
			else
				ui_printReceiptButton.setVisible(false);
		}
		else
		{
			ui_reserveButton.setVisible(true);
			ui_bookButton.setVisible(true);
			ui_printReceiptButton.setVisible(false);

			// enable booking
			ui_cancelReservationButton.setVisible(false);

			if(selectedSeats.isEmpty())
			{
				ui_bookButton.setDisable(true);
				ui_reserveButton.setDisable(true);
			}
			else
			{
				ui_bookButton.setDisable(false);
				ui_reserveButton.setDisable(false);
			}
		}
	}
}
