package at.ac.tuwien.inso.sepm.ticketline.client.validation;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.ValidationException;
import at.ac.tuwien.inso.sepm.ticketline.rest.news.DetailedNewsDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NewsValidator
{
	private static final Logger LOGGER = LoggerFactory.getLogger(NewsValidator.class);

	public static void validateDetailedNews(DetailedNewsDTO news) throws ValidationException
	{
		LOGGER.trace("Validating news object...");

		ValidationException ex = new ValidationException();

		if(news == null)
			ex.addMessage("news.isNull");
		else
		{
			if(news.getTitle() == null)
				ex.addMessage("news.noTitle");

			if(news.getText() == null)
				ex.addMessage("news.noText");

			if (news.getByteImage() != null)
			{
				// don't be bigger than 5 MB
				if (news.getByteImage().length > 5000000)
					ex.addMessage("news.imageSize");
			}
		}

		if(ex.hasMessage())
		{
			LOGGER.debug("News validation failed.");
			throw ex;
		}
	}
}
