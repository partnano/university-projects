package at.ac.tuwien.inso.sepm.ticketline.client.service;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.client.exception.InvalidCSVException;
import at.ac.tuwien.inso.sepm.ticketline.client.exception.ValidationException;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventFilterDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.enums.Category;
import at.ac.tuwien.inso.sepm.ticketline.rest.page.RestResponsePage;

import java.util.List;
import java.util.Map;

public interface EventService
{
	/**
	 * Find all event entries.
	 *
	 * @return list of event entries
	 *
	 * @throws DataAccessException in case something went wrong
	 */
	List<EventDTO> findAll() throws DataAccessException;

	/**
	 * Find a page of all event entries.
	 *
	 * @return Page of events.
	 *
	 * @throws DataAccessException in case something went wrong.
	 */
	RestResponsePage<EventDTO> findAll(int page, int size) throws DataAccessException;

	/**
	 * Find one event entry.
	 *
	 * @return event entry
	 *
	 * @param id id of event
	 *
	 * @throws DataAccessException in case something went wrong
	 */
	EventDTO findOne(Long id) throws DataAccessException;

	/**
	 * Find events with filter.
	 * Every filter that is not to be considered should be null.
	 *
	 * @return list of event entries
	 *
	 * @throws DataAccessException in case something went wrong
	 */
	List<EventDTO> findByFilter(EventFilterDTO filter) throws DataAccessException;

	/**
	 * Find a page of filtered event entries.
	 *
	 * @param filter filter to be used to filter event entries
	 * @param page page to be found
	 * @param size size of page to be found
	 * @return Page of events.
	 * @throws DataAccessException in case something went wrong.
	 */
	RestResponsePage<EventDTO> findByFilter(EventFilterDTO filter, int page, int size) throws DataAccessException;

	/**
	 * Create new event
	 *
	 * @param event to be created
	 *
	 * @return created event
	 *
	 * @throws DataAccessException in case something went wrong
	 * @throws ValidationException in case the validation of the given event fails
	 */
	EventDTO create(EventDTO event) throws ValidationException, DataAccessException;

	/**
	 * Find most booked events
	 *
	 * @param category category to filter by, or null
	 * @param month    month to filter by, or null
	 * @param year     year to filter by, or null
	 * @param limit    number of events to return
	 * @return a map of up to {@code limit} most popular events with their booked ticked count
	 * @throws ValidationException if the arguments are invalid
	 * @throws DataAccessException if something else went wrong
	 */
	Map<EventDTO, Integer> findTopEvents(Category category, Integer month, Integer year, int limit) throws
	                                                                                                ValidationException,
	                                                                                                DataAccessException;

	/**
	 * Parse and validate CSV to EventDTO
	 *
	 * @param pathToCSV file path to the csv file to be imported
	 *
	 * @return Generated EventDTO
	 */
	List<EventDTO> parse(String pathToCSV) throws DataAccessException, InvalidCSVException;
}
