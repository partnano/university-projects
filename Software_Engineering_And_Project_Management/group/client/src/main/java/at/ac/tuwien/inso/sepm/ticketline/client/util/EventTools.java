package at.ac.tuwien.inso.sepm.ticketline.client.util;

import at.ac.tuwien.inso.sepm.ticketline.rest.booking.BookingDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.booking.SeatBookingDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.booking.SectionBookingDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.booking.SectionBookingEntryDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.location.SeatDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.location.SectionDTO;

import java.util.Set;

//this class contains suboptimal solutions. these are to be fixed asap
public class EventTools
{
	public static int getNumAvailableTickets(SectionDTO section, EventDTO event)
	{
		Set<SectionBookingEntryDTO> sectionBookingEntries;

		int currentlyBooked = 0;
		int currentlyReserved = 0;

		for(BookingDTO bookingDTO : event.getBookings())
		{
			if(bookingDTO instanceof SectionBookingDTO)
			{
				sectionBookingEntries = ((SectionBookingDTO)bookingDTO).getSections();
				for(SectionBookingEntryDTO sectionBookingEntry : sectionBookingEntries)
				{
					if(sectionBookingEntry.getSection().getId().equals(section.getId()))
					{
						if(!bookingDTO.isCanceled())
						{
							if(bookingDTO.isPaid())
								currentlyBooked += sectionBookingEntry.getAmount();
							else
								currentlyReserved += sectionBookingEntry.getAmount();
						}
					}
				}
			}
		}
		return section.getCapacity() - currentlyBooked - currentlyReserved;
	}

	public static BookingDTO getBookingForSeat(SeatDTO s, EventDTO event)
	{
		Set<SeatDTO> bookedSeats;

		for(BookingDTO bookingDTO : event.getBookings())
		{
			if(!bookingDTO.isCanceled() && bookingDTO instanceof SeatBookingDTO)
			{
				bookedSeats = ((SeatBookingDTO)bookingDTO).getSeats();
				for(SeatDTO bookedSeat : bookedSeats)
				{
					if(bookedSeat.getId().equals(s.getId()))
					{
						return bookingDTO;
					}
				}
			}
		}
		return null;
	}
}
