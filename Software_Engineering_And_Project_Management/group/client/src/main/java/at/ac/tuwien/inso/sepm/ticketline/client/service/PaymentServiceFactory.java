package at.ac.tuwien.inso.sepm.ticketline.client.service;

import at.ac.tuwien.inso.sepm.ticketline.client.service.implementation.CashPaymentService;
import at.ac.tuwien.inso.sepm.ticketline.client.service.implementation.StripePaymentService;
import at.ac.tuwien.inso.sepm.ticketline.rest.payment.PaymentProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PaymentServiceFactory
{
	@Autowired
	private StripePaymentService stripePaymentService;

	@Autowired
	private CashPaymentService cashPaymentService;

	public PaymentService getService(PaymentProvider paymentProvider)
	{
		if(paymentProvider.equals(PaymentProvider.STRIPE))
			return stripePaymentService;
		else
			return cashPaymentService;
	}
}
