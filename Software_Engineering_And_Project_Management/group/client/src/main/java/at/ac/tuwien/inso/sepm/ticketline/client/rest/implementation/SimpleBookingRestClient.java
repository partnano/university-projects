package at.ac.tuwien.inso.sepm.ticketline.client.rest.implementation;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.client.rest.BookingRestClient;
import at.ac.tuwien.inso.sepm.ticketline.rest.booking.BookingDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.customer.CustomerDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.payment.CreditCardDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.payment.PaymentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;

import java.util.List;

@Component
public class SimpleBookingRestClient implements BookingRestClient
{
	private static final Logger LOGGER = LoggerFactory.getLogger(SimpleBookingRestClient.class);
	private static final String ENDPOINT = "/bookings";

	private final RestClient client;

	public SimpleBookingRestClient(RestClient client)
	{
		this.client = client;
	}

	@Override
	public BookingDTO create(BookingDTO booking) throws DataAccessException
	{
		try
		{
			LOGGER.debug("creating new booking ...");

			ResponseEntity<BookingDTO> result = client.exchange(client.getServiceURI(ENDPOINT),
			                                                    HttpMethod.POST,
			                                                    new HttpEntity<>(booking),
			                                                    BookingDTO.class);

			LOGGER.debug("... successfully created new booking, id = {}", result.getBody().getId());

			return result.getBody();
		}
		catch(RestClientException ex)
		{
			throw new DataAccessException(ex.getMessage(), ex);
		}
	}

	@Override
	public BookingDTO update(BookingDTO booking) throws DataAccessException
	{
		try
		{
			LOGGER.debug("updating booking ...");

			ResponseEntity<BookingDTO> result = client.exchange(client.getServiceURI(ENDPOINT + "/" + booking.getId()),
			                                                    HttpMethod.PUT,
			                                                    new HttpEntity<>(booking),
			                                                    BookingDTO.class);

			LOGGER.debug("... successfully updated booking");

			return result.getBody();
		}
		catch(RestClientException ex)
		{
			throw new DataAccessException(ex.getMessage(), ex);
		}
	}

	@Override
	public BookingDTO findByReservationNumber(int reservationNumber) throws DataAccessException
	{
		try
		{
			LOGGER.debug("requesting booking for res.number: {}", reservationNumber);

			ResponseEntity<BookingDTO> result = client.exchange(client.getServiceURI(
				ENDPOINT + "/reservation/" + reservationNumber),
			                                                    HttpMethod.GET,
			                                                    null,
			                                                    new ParameterizedTypeReference<BookingDTO>()
			                                                    {
			                                                    });

			BookingDTO bookingDTO = result.getBody();

			LOGGER.debug("... successfully received booking for res.number {}: {}",
			             reservationNumber,
			             bookingDTO);

			return bookingDTO;
		}
		catch(RestClientException ex)
		{
			throw new DataAccessException(ex.getMessage(), ex);
		}
	}

	@Override
	public List<BookingDTO> findAll(CustomerDTO customer) throws DataAccessException
	{
		try
		{
			LOGGER.debug("requesting bookings for customer (id: {})", customer.getId());

			ResponseEntity<List<BookingDTO>> result = client.exchange(client.getServiceURI(
				ENDPOINT + "?customerId=" + customer.getId()),
			                                                          HttpMethod.GET,
			                                                          null,
			                                                          new ParameterizedTypeReference<List<BookingDTO>>()
			                                                          {
			                                                          });

			List<BookingDTO> bookingDTOs = result.getBody();

			LOGGER.debug("... successfully received bookings for customer (id: {}): {}",
			             customer.getId(),
			             bookingDTOs);

			return bookingDTOs;
		}
		catch(RestClientException ex)
		{
			throw new DataAccessException(ex.getMessage(), ex);
		}
	}

	@Override
	public BookingDTO pay(BookingDTO bookingDTO, CreditCardDTO creditCardDTO) throws DataAccessException
	{
		try
		{
			LOGGER.debug("requesting booking to be paid");
			PaymentContext paymentContext = new PaymentContext();
			paymentContext.setBookingDTO(bookingDTO);
			paymentContext.setCreditCardDTO(creditCardDTO);
			ResponseEntity<BookingDTO> result = client.exchange(client.getServiceURI(ENDPOINT + "/pay"),
			                                                    HttpMethod.POST,
			                                                    new HttpEntity<>(paymentContext),
			                                                    new ParameterizedTypeReference<BookingDTO>()
			                                                    {
			                                                    });
			LOGGER.debug("paid booking: {}", result.getBody());
			return result.getBody();
		}
		catch(RestClientException ex)
		{
			throw new DataAccessException(ex.getMessage(), ex);
		}
	}

	@Override
	public BookingDTO refund(BookingDTO bookingDTO) throws DataAccessException
	{
		try
		{
			LOGGER.debug("requesting booking to be canceled und refunded");
			ResponseEntity<BookingDTO> result = client.exchange(client.getServiceURI(
				ENDPOINT + "/" + bookingDTO.getId() + "/refund"),
			                                                    HttpMethod.POST,
			                                                    null,
			                                                    new ParameterizedTypeReference<BookingDTO>()
			                                                    {
			                                                    });
			LOGGER.debug("canceled booking: {}", result.getBody());
			return result.getBody();
		}
		catch(RestClientException ex)
		{
			throw new DataAccessException(ex.getMessage(), ex);
		}
	}
}
