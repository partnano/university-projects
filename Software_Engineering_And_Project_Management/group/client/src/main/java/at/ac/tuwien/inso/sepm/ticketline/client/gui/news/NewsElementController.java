package at.ac.tuwien.inso.sepm.ticketline.client.gui.news;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class NewsElementController
{
	public VBox newsElement;

	@FXML
	private Label lblDate;

	@FXML
	private Label lblTitle;

	@FXML
	private Label lblText;
}
