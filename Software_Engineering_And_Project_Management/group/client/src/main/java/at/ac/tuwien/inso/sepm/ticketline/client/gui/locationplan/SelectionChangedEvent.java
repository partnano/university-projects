package at.ac.tuwien.inso.sepm.ticketline.client.gui.locationplan;

import at.ac.tuwien.inso.sepm.ticketline.rest.location.SeatDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.location.SectionDTO;

import java.util.Set;

public class SelectionChangedEvent
{
	private final Set<SeatDTO> seats;
	private final SectionDTO section;
	private final SeatDTO lastClickedSeat;

	/**
	 * Can be used to inform about the state of a new location plan selection
	 * @param seats The selected seats after the selection change, if any
	 * @param section The sections after the selection change, if any
	 * @param lastClickedSeat The item that was last clicked. This param is only present if the selection change was caused by a click on the location plan.
	 */
	public SelectionChangedEvent(Set<SeatDTO> seats, SectionDTO section, SeatDTO lastClickedSeat)
	{
		this.seats = seats;
		this.section = section;
		this.lastClickedSeat = lastClickedSeat;
	}

	public Set<SeatDTO> getSeats()
	{
		return seats;
	}

	public SeatDTO getLastClickedSeat()
	{
		return lastClickedSeat;
	}

	public SectionDTO getSection()
	{
		return section;
	}
}
