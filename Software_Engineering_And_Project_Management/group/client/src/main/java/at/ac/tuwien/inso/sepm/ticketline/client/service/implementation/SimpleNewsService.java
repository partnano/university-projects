package at.ac.tuwien.inso.sepm.ticketline.client.service.implementation;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.client.exception.ValidationException;
import at.ac.tuwien.inso.sepm.ticketline.client.rest.NewsRestClient;
import at.ac.tuwien.inso.sepm.ticketline.client.service.NewsService;
import at.ac.tuwien.inso.sepm.ticketline.client.validation.NewsValidator;
import at.ac.tuwien.inso.sepm.ticketline.rest.news.DetailedNewsDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.news.SimpleNewsDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SimpleNewsService implements NewsService
{
	private static final Logger LOGGER = LoggerFactory.getLogger(SimpleNewsService.class);

	private final NewsRestClient newsRestClient;

	public SimpleNewsService(NewsRestClient newsRestClient)
	{
		this.newsRestClient = newsRestClient;
	}

	@Override
	public List<SimpleNewsDTO> findAll() throws DataAccessException
	{
		return newsRestClient.findAll();
	}

	@Override
	public Page<SimpleNewsDTO> findAll(int page, int size) throws DataAccessException
	{
		return newsRestClient.findAll(page, size);
	}

	@Override
	public Page<SimpleNewsDTO> findUnread(int page, int size) throws DataAccessException
	{
		return newsRestClient.findUnread(page, size);
	}

	@Override
	public DetailedNewsDTO publishNews(DetailedNewsDTO news) throws DataAccessException, ValidationException
	{
		NewsValidator.validateDetailedNews(news);

		LOGGER.debug("Publishing news ...");
		DetailedNewsDTO result = newsRestClient.publishNews(news);
		LOGGER.debug("...published news");

		return result;
	}

	@Override
	public DetailedNewsDTO findOne(SimpleNewsDTO newsDTO) throws DataAccessException
	{
		return newsRestClient.findOne(newsDTO);
	}

	@Override
	public DetailedNewsDTO update(DetailedNewsDTO newsDTO) throws DataAccessException, ValidationException
	{
		NewsValidator.validateDetailedNews(newsDTO);

		LOGGER.debug("Updating news ...");
		DetailedNewsDTO result = newsRestClient.update(newsDTO);
		LOGGER.debug("...updated news");

		return result;
	}
}
