package at.ac.tuwien.inso.sepm.ticketline.client.rest;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventFilterDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.enums.Category;
import at.ac.tuwien.inso.sepm.ticketline.rest.page.RestResponsePage;

import java.util.List;
import java.util.Map;

public interface EventRestClient
{
	/**
	 * Find all event entries.
	 *
	 * @return List of events.
	 * @throws DataAccessException in case something went wrong.
	 */
	List<EventDTO> findAll() throws DataAccessException;

	/**
	 * Find a page of all event entries.
	 *
	 * @param page page to be found
	 * @param size size of page to be found
	 * @return Page of events.
	 * @throws DataAccessException in case something went wrong.
	 */
	RestResponsePage<EventDTO> findAll(int page, int size) throws DataAccessException;

	/**
	 * Find one event entry.
	 *
	 * @param id id of event
	 * @return event entry
	 * @throws DataAccessException in case something went wrong
	 */
	EventDTO findOne(Long id) throws DataAccessException;

	/**
	 * Find event entries with filters applied.
	 * Every filter that isn't to be considered should be null.
	 *
	 * @param filter EventFilterDTO with relevant filters
	 * @return List of events
	 * @throws DataAccessException in case something went wrong
	 */
	List<EventDTO> findByFilter(EventFilterDTO filter) throws DataAccessException;

	/**
	 * Find most popular events
	 * @param category category to filter by, or null
	 * @param month month to filter by, or null
	 * @param year year to filter by, or null
	 * @param limit number of events to return
	 * @return a list of up to {@code limit} most popular events
	 * @throws DataAccessException if something went wrong
	 */
	Map<EventDTO, Integer> findTopEvents(Category category, Integer month, Integer year, int limit) throws
	                                                                                                DataAccessException;

	/**
	 * Find a page of filtered event entries.
	 *
	 * @param filter filter to be used to filter event entries
	 * @param page page to be found
	 * @param size size of page to be found
	 * @return Page of events.
	 * @throws DataAccessException in case something went wrong.
	 */
	RestResponsePage<EventDTO> findByFilter(EventFilterDTO filter, int page, int size) throws DataAccessException;

	/**
	 * Create new event
	 *
	 * @param event to be created
	 * @return created event
	 * @throws DataAccessException in case something went wrong
	 */
	EventDTO create(EventDTO event) throws DataAccessException;
}
