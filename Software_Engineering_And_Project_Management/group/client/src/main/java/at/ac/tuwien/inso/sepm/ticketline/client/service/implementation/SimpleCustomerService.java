package at.ac.tuwien.inso.sepm.ticketline.client.service.implementation;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.client.exception.ValidationException;
import at.ac.tuwien.inso.sepm.ticketline.client.rest.CustomerRestClient;
import at.ac.tuwien.inso.sepm.ticketline.client.service.CustomerService;
import at.ac.tuwien.inso.sepm.ticketline.client.validation.CustomerValidator;
import at.ac.tuwien.inso.sepm.ticketline.rest.customer.CustomerDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SimpleCustomerService implements CustomerService
{
	private static final Logger LOGGER = LoggerFactory.getLogger(SimpleCustomerService.class);

	private final CustomerRestClient dao;

	public SimpleCustomerService(CustomerRestClient dao)
	{
		this.dao = dao;
	}

	@Override
	public List<CustomerDTO> findAll() throws DataAccessException
	{
		LOGGER.debug("retrieving all customers ...");
		List<CustomerDTO> result = dao.findAll();
		LOGGER.debug("... successfully retrieved all customers");
		return result;
	}

	@Override
	public Page<CustomerDTO> findAll(int page, int size) throws DataAccessException
	{
		LOGGER.debug("retrieving page of all customers ...");
		Page<CustomerDTO> result = dao.findAll(page, size);
		LOGGER.debug("... successfully retrieved page of customers");
		return result;
	}

	@Override
	public CustomerDTO findOne(Long id) throws ValidationException, DataAccessException
	{
		CustomerValidator.validateId(id);

		LOGGER.debug("retrieving customer with id {} ..", id);
		CustomerDTO result = dao.findOne(id);
		LOGGER.debug("... customer retrieved successfully");
		return result;
	}

	@Override
	public CustomerDTO create(CustomerDTO customer) throws ValidationException, DataAccessException
	{
		CustomerValidator.validate(customer);

		LOGGER.debug("creating new customer ...");
		CustomerDTO result = dao.create(customer);
		LOGGER.debug("... customer successfully created");
		return result;
	}

	@Override
	public CustomerDTO createAnonymous(CustomerDTO customer) throws DataAccessException
	{
		LOGGER.debug("creating new anonymous customer ...");
		CustomerDTO result = dao.create(customer);
		LOGGER.debug("... customer successfully created");
		return result;
	}

	@Override
	public CustomerDTO update(CustomerDTO customer) throws ValidationException, DataAccessException
	{
		CustomerValidator.validate(customer);

		LOGGER.debug("updating customer ...");
		CustomerDTO result = dao.update(customer);
		LOGGER.debug("... customer successfully updated");
		return result;
	}
}
