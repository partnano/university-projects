package at.ac.tuwien.inso.sepm.ticketline.client.gui.news;

import at.ac.tuwien.inso.sepm.ticketline.rest.news.SimpleNewsDTO;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.Separator;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;

public class NewsListCell extends ListCell<SimpleNewsDTO>
{
	private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.LONG,
	                                                                                         FormatStyle.SHORT);

	@Override
	public void updateItem(SimpleNewsDTO item, boolean empty)
	{
		super.updateItem(item, empty);

		if(empty || item == null)
		{
			setGraphic(null);
		}

		if(item != null)
		{
			BorderPane newsPane = new BorderPane();
			Label summaryLabel = new Label(item.getSummary() + " ...");

			summaryLabel.setWrapText(true);

			Label headlineLabel = new Label(item.getTitle());
			headlineLabel.setStyle("-fx-font-weight: bold; -fx-font-size: 14px;");

			headlineLabel.setWrapText(true);

			Label dateLabel = new Label(FORMATTER.format(item.getPublishedAt()));
			dateLabel.setStyle("-fx-font-weight: bold; -fx-font-size: 10px;");

			HBox summaryBox = new HBox(5);

			summaryBox.getChildren().add(summaryLabel);

			summaryBox.setAlignment(Pos.CENTER_LEFT);

			VBox vbox = new VBox(5);
			HBox hbox = new HBox(5);

			vbox.getChildren().add(headlineLabel);

			if(!item.isRead())
			{
				Label newLabel = new Label("New!");
				newLabel.setTextFill(Color.web("#35b137"));
				newLabel.setStyle("-fx-border-width: 1px; -fx-border-radius: 2px; -fx-border-color: #35b137");
				hbox.getChildren().addAll(dateLabel, newLabel);
				vbox.getChildren().add(hbox);
			}
			else
			{
				vbox.getChildren().add(dateLabel);
			}
			newsPane.setTop(vbox);

			newsPane.setCenter(summaryBox);
			newsPane.setMaxWidth(this.getListView().getWidth() - 40);
			newsPane.setBottom(new Separator());
			BorderPane.setAlignment(summaryLabel, Pos.TOP_LEFT);
			setGraphic(newsPane);
		}
	}
}