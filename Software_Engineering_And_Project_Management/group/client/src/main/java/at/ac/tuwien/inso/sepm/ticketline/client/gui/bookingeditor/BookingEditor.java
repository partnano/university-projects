package at.ac.tuwien.inso.sepm.ticketline.client.gui.bookingeditor;

import at.ac.tuwien.inso.sepm.ticketline.rest.booking.BookingDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.booking.SeatBookingDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.booking.SectionBookingDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.location.AreaDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.location.RowDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.location.SectionDTO;
import at.ac.tuwien.inso.springfx.SpringFxmlLoader;
import javafx.stage.Stage;

public class BookingEditor
{
	public static void showAndWait(Stage parent, SpringFxmlLoader loader, EventDTO event)
	{
		AreaDTO firstArea = event.getEventAreas().iterator().next().getArea();

		if(firstArea instanceof SectionDTO)
		{
			SectionBookingEditorController.showAndWait(parent, loader, event);
		}
		else if(firstArea instanceof RowDTO)
		{
			SeatBookingEditorController.showAndWait(parent, loader, event);
		}
		else
			throw new AssertionError("unknown area type: " + firstArea.getClass().getName());
	}

	public static void showAndWait(Stage parent, SpringFxmlLoader loader, BookingDTO booking)
	{
		if(booking instanceof SectionBookingDTO)
		{
			SectionBookingEditorController.showAndWait(parent, loader, (SectionBookingDTO)booking);
		}
		else if(booking instanceof SeatBookingDTO)
		{
			SeatBookingEditorController.showAndWait(parent, loader, (SeatBookingDTO)booking);
		}
		else
			throw new AssertionError("unknown booking type: " + booking.getClass().getName());
	}
}
