package at.ac.tuwien.inso.sepm.ticketline.client.util;

import at.ac.tuwien.inso.sepm.ticketline.client.gui.bookingeditor.SeatBookingEditorController;
import at.ac.tuwien.inso.sepm.ticketline.rest.booking.BookingDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.booking.SeatBookingDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.booking.SectionBookingDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.booking.SectionBookingEntryDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.location.RowDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.location.SeatDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.location.SectionDTO;
import com.itextpdf.io.image.ImageDataFactory;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.*;
import com.itextpdf.layout.property.HorizontalAlignment;
import com.itextpdf.layout.property.TextAlignment;
import com.itextpdf.layout.property.UnitValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.MalformedURLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.ResourceBundle;

public class PDFReceiptGenerator
{
	private static final Logger LOGGER = LoggerFactory.getLogger(SeatBookingEditorController.class);
	private static final Float TAX_MULTIPLIER = 1.13f;
	private Document currentPdf;

	private ResourceBundle rb;
	private EventDTO event;

	public enum ReceiptType
	{
		CONFIRMATION,
		CANCELLATION
	}

	private final ReceiptType receiptType;
	private final BookingDTO booking;

	public PDFReceiptGenerator(ReceiptType receiptType, BookingDTO booking)
	{
		this.receiptType = receiptType;
		this.booking = booking;

		if(booking.getEvent() == null)
			throw new AssertionError("Booking without Event received");

		this.event = booking.getEvent();

		rb = BundleManager.getBundle();
	}

	public File generateReceipt(File dest) throws PDFCreationException
	{
		currentPdf = initDocument(dest);
		currentPdf.setFontSize(10);

		addCompanyName();
		addCustomerData();
		addReceiptHeadline();
		addEventInfo();

		Float totalPrice;

		if(booking instanceof SeatBookingDTO)
			totalPrice = addSeatItemList();
		else if(booking instanceof SectionBookingDTO)
			totalPrice = addSectionItemList();
		else
			throw new AssertionError("unsupported booking type: " + booking.getClass().toString());

		addPaymentInfo(totalPrice);

		currentPdf.close();
		return dest;
	}

	// adds company name to this.currentPdf
	private void addCompanyName()
	{
		Document c = currentPdf;

		Text companyName = new Text("Ticketline Event GmbH\n").setUnderline().setBold();
		Text address1 = new Text("Karlsplatz 13\n");
		Text address2 = new Text("1040 Wien\n");
		Text tel = new Text("Tel.: +43 1 33 31 423\n");
		Text email = new Text("E-Mail: ticketline@hotmail.at\n");
		Text uid = new Text("UID-Nummer: ATU12345678");

		c.add(new Paragraph().add(companyName).add(address1).add(address2));
		c.add(new Paragraph().setMarginTop(10).add(tel).add(email).add(uid));

		ClassLoader classLoader = getClass().getClassLoader();
		File file = new File(classLoader.getResource("image/ticketlineLogo.png").getFile());

		Image img = null;
		try
		{
			img = new Image(ImageDataFactory.create(file.getPath()));
		}
		catch(MalformedURLException e)
		{
			e.printStackTrace();
		}
		img.scale(0.6f, 0.6f);
		img.setFixedPosition(PageSize.A4.getWidth() - img.getImageScaledWidth() - 20,
		                     PageSize.A4.getHeight() - img.getImageScaledHeight() - 20);
		img.setHorizontalAlignment(HorizontalAlignment.RIGHT);

		c.add(img);
	}

	// adds customer name if exists and customer number to this.currentPdf
	private void addCustomerData()
	{
		Text customerName = new Text("");

		if(booking.getCustomer().getLastName() != null)
			customerName.setText(rb.getString("receipt.customerName") + ": " + booking.getCustomer().getLastName() + " "
			                     + booking.getCustomer().getFirstName() + "\n");

		Text customerNumber = new Text(
			rb.getString("receipt.customerNumber") + ": " + booking.getCustomer().getCustomerNumber() + "\n");

		Long receiptID;
		switch(receiptType)
		{
		case CONFIRMATION:
			receiptID = booking.getPaymentReceipt().getId();
			break;
		case CANCELLATION:
			receiptID = booking.getCancellationReceipt().getId();
			break;
		default:
			throw new AssertionError("unsupported receipt type: " + receiptType);
		}

		Text receiptNumber = new Text(rb.getString("receipt.receiptNumber") + ": " + receiptID + "\n");
		Text reservationNumber = new Text(
			rb.getString("receipt.reservationNumber") + ": " + booking.getReservationNumber() + "\n");

		currentPdf.add(new Paragraph().add(customerName)
		                              .add(customerNumber)
		                              .add(receiptNumber)
		                              .add(reservationNumber)
		                              .setMarginTop(50));
	}

	// adds booking date and receipt title to this.currentPdf
	private void addReceiptHeadline()
	{
		DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");

		LocalDateTime receiptCreationDate;
		String headlineText;

		switch(receiptType)
		{
		case CONFIRMATION:
			receiptCreationDate = booking.getPaymentReceipt().getCreatedAt();
			headlineText = rb.getString("receipt.headline");
			break;
		case CANCELLATION:
			receiptCreationDate = booking.getCancellationReceipt().getCreatedAt();
			headlineText = String.format("%s %s",
			                             rb.getString("receipt.headlineCanceled"),
			                             booking.getPaymentReceipt().getId());
			break;
		default:
			throw new AssertionError("unsupported receipt type: " + receiptType);
		}

		String receiptCreationText = rb.getString("receipt.date") + ": " + receiptCreationDate.format(dateFormatter);
		currentPdf.add(new Paragraph(receiptCreationText).setTextAlignment(TextAlignment.RIGHT).setMarginTop(50));

		currentPdf.add(new Paragraph(headlineText).setMarginTop(10)
		                                          .setTextAlignment(TextAlignment.CENTER)
		                                          .setBold()
		                                          .setFontSize(15));
	}

	// returns total price
	private Float addSeatItemList()
	{
		SeatBookingDTO booking = (SeatBookingDTO)this.booking;

		Table itemTable = initItemTable();
		int itemPos = 1;
		Float totalPrice = 0f;

		HashMap<RowDTO, LinkedList<SeatDTO>> seatsPerRow = booking.seatsGroupedByRow();

		for(Map.Entry<RowDTO, LinkedList<SeatDTO>> rowAndSeats : seatsPerRow.entrySet())
		{
			RowDTO row = rowAndSeats.getKey();
			Float amount = rowAndSeats.getValue().size() + 0f;

			Float rowPrice = booking.priceForArea(row);

			if(receiptType == ReceiptType.CANCELLATION)
				rowPrice *= -1;

			Float totalRowPrice = rowPrice * amount;

			itemTable.addCell(itemPos++ + "");
			itemTable.addCell(rb.getString("receipt.seatInRow") + " " + row.getName());
			itemTable.addCell(String.format("%.2f", rowPrice));
			itemTable.addCell(Math.round(amount) + "");
			itemTable.addCell(new Cell().add(new Paragraph(String.format("%.2f", totalRowPrice)))
			                            .setTextAlignment(TextAlignment.RIGHT));

			totalPrice += totalRowPrice;
		}

		itemTable.addCell(new Cell(1, 3).add(new Paragraph("")));
		itemTable.addCell(rb.getString("receipt.total") + ":");
		itemTable.addCell(new Paragraph(String.format("%.2f", totalPrice)).setTextAlignment(TextAlignment.RIGHT));

		currentPdf.add(itemTable);
		return totalPrice;
	}

	private Float addSectionItemList()
	{
		SectionBookingDTO booking = (SectionBookingDTO)this.booking;

		Table itemTable = initItemTable();

		int itemPos = 1;
		Float totalPrice = 0f;

		for(SectionBookingEntryDTO sectionBookingEntry : booking.getSections())
		{
			SectionDTO section = sectionBookingEntry.getSection();
			Float sectionPrice = booking.priceForArea(section);
			Long amount = sectionBookingEntry.getAmount();

			if(receiptType == ReceiptType.CANCELLATION)
				sectionPrice *= -1;

			Float totalItemPrice = sectionPrice * amount;

			itemTable.addCell(itemPos++ + "");
			itemTable.addCell(rb.getString("receipt.admissionToSection") + " " + section.getName());
			itemTable.addCell(String.format("%.2f", sectionPrice));
			itemTable.addCell(amount + "");
			itemTable.addCell(new Cell().add(new Paragraph(String.format("%.2f", totalItemPrice)))
			                            .setTextAlignment(TextAlignment.RIGHT));

			totalPrice += totalItemPrice;
		}

		itemTable.addCell(new Cell(1, 3).add(new Paragraph("")));
		itemTable.addCell(rb.getString("receipt.total") + ":");
		itemTable.addCell(new Paragraph(String.format("%.2f", totalPrice)).setTextAlignment(TextAlignment.RIGHT));

		currentPdf.add(itemTable);
		return totalPrice;
	}

	private Table initItemTable()
	{
		// #, item, price, amount, sum
		Table itemTable = new Table(5);
		itemTable.setMarginTop(20);
		itemTable.setWidth(UnitValue.createPercentValue(100));

		itemTable.addCell(rb.getString("receipt.pos"));
		itemTable.addCell(rb.getString("receipt.item"));
		itemTable.addCell(rb.getString("receipt.itemPrice"));
		itemTable.addCell(rb.getString("receipt.itemAmount"));
		itemTable.addCell(rb.getString("receipt.sumGross"));

		return itemTable;
	}

	private void addEventInfo()
	{
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy, HH:mm");

		Text eventName = new Text(rb.getString("receipt.eventName") + ": " + event.getName() + "\n").setBold()
		                                                                                            .setUnderline();
		Text eventDate = new Text(String.format("%s: %s\n",
		                                        rb.getString("receipt.eventDate"),
		                                        event.getTime().format(formatter)));
		Text eventLocation = new Text(String.format("%s: %s",
		                                            rb.getString("receipt.eventLocation"),
		                                            event.getLocation().getName()));

		currentPdf.add(new Paragraph().add(eventName).add(eventDate).add(eventLocation).setMarginTop(20));
	}

	private void addPaymentInfo(Float totalPrice)
	{
		// absolute value because it would be negative on cancellation (totalPrice < 0)
		Float vat = Math.abs(totalPrice - (totalPrice / TAX_MULTIPLIER));
		currentPdf.add(new Paragraph(String.format("%s %.2f", rb.getString("receipt.taxInfo"), vat)).setMarginTop(20));

		String paymentStatus;
		switch(receiptType)
		{
		case CONFIRMATION:
			paymentStatus = rb.getString("receipt.paymentReceived");
			break;
		case CANCELLATION:
			paymentStatus = rb.getString("receipt.paymentRefunded");
			break;
		default:
			throw new AssertionError("unsupported receipt type: " + receiptType);
		}

		currentPdf.add(new Paragraph(String.format("%s %s: %s", paymentStatus,
		                                           rb.getString("receipt.paymentType"),
		                                           booking.getPaymentMethod())));
	}

	public Document initDocument(File dest) throws PDFCreationException
	{
		PdfWriter writer;
		try
		{
			writer = new PdfWriter(dest);
		}
		catch(FileNotFoundException e)
		{
			LOGGER.error("PDF could not be saved: {}", e);
			throw new PDFCreationException("Could not create PDF");
		}

		PdfDocument pdf = new PdfDocument(writer);
		return new Document(pdf);
	}

	public class PDFCreationException extends Exception
	{
		public PDFCreationException()
		{
		}

		public PDFCreationException(String message)
		{
			super(message);
		}

		public PDFCreationException(String message, Throwable cause)
		{
			super(message, cause);
		}
	}
}

