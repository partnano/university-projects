package at.ac.tuwien.inso.sepm.ticketline.client.gui.locationplan;

import at.ac.tuwien.inso.sepm.ticketline.client.util.BundleManager;
import at.ac.tuwien.inso.sepm.ticketline.rest.location.RowDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.location.SeatDTO;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.StrokeType;
import javafx.util.Duration;

import java.util.function.Consumer;

public class Seat extends StackPane
{
	private static final int SIZE = 30;

	private static final Color STROKE_COLOR_UNSELECTED = Color.BLACK;
	private static final Color STROKE_COLOR_SELECTED = Color.ORANGE;
	private static final Color FILL_COLOR_FREE = new Color(0.2078, 0.6941, 0.2157, 1);
	private static final Color FILL_COLOR_RESERVED = new Color(0.7882, 0.7882, 0.2, 1);
	private static final Color FILL_COLOR_BOOKED = new Color(0.8627, 0.2118, 0.2118, 1);
	private final Rectangle rectangle;

	private boolean selected = false;

	public Seat(SeatDTO dto, Consumer<SeatDTO> onClick, RowDTO row, float price)
	{

		rectangle = new Rectangle(SIZE, SIZE);
		setWidth(SIZE);
		setHeight(SIZE);
		rectangle.setArcHeight(SIZE / 2);
		rectangle.setArcWidth(SIZE / 2);
		rectangle.setStrokeType(StrokeType.INSIDE);
		rectangle.setStrokeWidth(2);
		rectangle.setStroke(STROKE_COLOR_UNSELECTED);
		getChildren().add(rectangle);
		if(dto != null)
		{
			String priceString = String.format("%.2f €", price);
			Tooltip labelTip = new Tooltip(BundleManager.getBundle().getString("seat.number") + dto.getName() + "\n"
				                               + BundleManager.getBundle().getString("seat.row") + row.getName() + "\n"
				                               + BundleManager.getBundle().getString("seat.price") + priceString);
			labelTip.setShowDelay(new Duration(100));
			labelTip.setStyle("-fx-font-size: 14");

			Label seatLabel = new Label(dto.getName());
			seatLabel.setPrefSize(SIZE, SIZE);
			seatLabel.setAlignment(Pos.CENTER);
			seatLabel.setTooltip(labelTip);
			seatLabel.setStyle("-fx-font-weight: bold;");

			getChildren().add(seatLabel);
		}
		setOnMouseClicked(e -> onClick.accept(dto));
	}

	public void setFree()
	{
		rectangle.setFill(FILL_COLOR_FREE);
	}

	public void setReserved()
	{
		rectangle.setFill(FILL_COLOR_RESERVED);
	}

	public void setBooked()
	{
		rectangle.setFill(FILL_COLOR_BOOKED);
	}

	public void setSelected(boolean selected)
	{
		this.selected = selected;

		updateSelectionColor();
	}

	private void updateSelectionColor() {
		if(selected)
			rectangle.setStroke(STROKE_COLOR_SELECTED);
		else
			rectangle.setStroke(STROKE_COLOR_UNSELECTED);
	}

	public boolean toggleSelection()
	{
		selected = !selected;

		updateSelectionColor();

		return selected;
	}

	public static int getSIZE()
	{
		return SIZE;
	}
}
