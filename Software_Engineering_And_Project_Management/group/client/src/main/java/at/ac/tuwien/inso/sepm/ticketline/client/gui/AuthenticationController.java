package at.ac.tuwien.inso.sepm.ticketline.client.gui;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.client.service.AuthenticationService;
import at.ac.tuwien.inso.sepm.ticketline.client.util.JavaFXUtils;
import at.ac.tuwien.inso.sepm.ticketline.rest.authentication.AuthenticationRequest;
import at.ac.tuwien.inso.sepm.ticketline.rest.authentication.AuthenticationTokenInfo;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.PasswordField;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextField;
import org.springframework.stereotype.Component;

@Component
public class AuthenticationController
{
	@FXML
	private TextField txtUsername;

	@FXML
	private PasswordField txtPassword;

	private final AuthenticationService service;
	private final MainController mainController;

	public AuthenticationController(AuthenticationService service, MainController mainController)
	{
		this.service = service;
		this.mainController = mainController;
	}

	@FXML
	private void handleAuthenticate(ActionEvent actionEvent)
	{
		Task<AuthenticationTokenInfo> task = new Task<>()
		{
			@Override
			protected AuthenticationTokenInfo call() throws DataAccessException
			{
				return service.authenticate(
					AuthenticationRequest.builder()
					                     .username(txtUsername.getText())
					                     .password(txtPassword.getText())
					                     .build());
			}

			@Override
			protected void failed()
			{
				super.failed();
				JavaFXUtils.createExceptionDialog(getException(),
				                                  ((Node)actionEvent.getTarget()).getScene().getWindow()).showAndWait();
			}
		};

		task.runningProperty().addListener((observable, oldValue, running) ->
			                                   mainController.setProgressbarProgress(
				                                   running ? ProgressBar.INDETERMINATE_PROGRESS : 0)
		);

		new Thread(task).start();
	}
}
