package at.ac.tuwien.inso.sepm.ticketline.client.rest.implementation;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.client.rest.NewsRestClient;
import at.ac.tuwien.inso.sepm.ticketline.rest.news.DetailedNewsDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.news.SimpleNewsDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.page.RestResponsePage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientException;

import java.util.List;

@Component
public class SimpleNewsRestClient implements NewsRestClient
{
	private static final Logger LOGGER = LoggerFactory.getLogger(SimpleNewsRestClient.class);
	private static final String NEWS_URL = "/news";

	private final RestClient restClient;

	public SimpleNewsRestClient(RestClient restClient)
	{
		this.restClient = restClient;
	}

	@Override
	public List<SimpleNewsDTO> findAll() throws DataAccessException
	{
		try
		{
			LOGGER.debug("Retrieving all news from {}", restClient.getServiceURI(NEWS_URL));

			ResponseEntity<List<SimpleNewsDTO>> news = restClient.exchange(restClient.getServiceURI(NEWS_URL),
			                                                               HttpMethod.GET,
			                                                               null,
			                                                               new ParameterizedTypeReference<List<SimpleNewsDTO>>()
			                                                               {
			                                                               });

			LOGGER.debug("Result status was {} with content {}", news.getStatusCode(), news.getBody());
			return news.getBody();
		}
		catch(HttpStatusCodeException e)
		{
			throw new DataAccessException("Failed retrieve news with status code " + e.getStatusCode());
		}
		catch(RestClientException e)
		{
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	@Override
	public RestResponsePage<SimpleNewsDTO> findAll(int page, int size) throws DataAccessException
	{
		try
		{
			LOGGER.debug("Retrieving page of all news from {}", restClient.getServiceURI(NEWS_URL));

			ResponseEntity<RestResponsePage<SimpleNewsDTO>> news = restClient.exchange(restClient.getServiceURI(
				NEWS_URL + "?page=" + page + "&size=" + size), HttpMethod.GET,
			                                                                           null,
			                                                                           new ParameterizedTypeReference<RestResponsePage<SimpleNewsDTO>>()
			                                                                           {
			                                                                           });

			LOGGER.debug("Result status was {} with content {}", news.getStatusCode(), news.getBody());
			return news.getBody();
		}
		catch(HttpStatusCodeException e)
		{
			throw new DataAccessException("Failed retrieve news with status code " + e.getStatusCode());
		}
		catch(RestClientException e)
		{
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	@Override
	public RestResponsePage<SimpleNewsDTO> findUnread(int page, int size) throws DataAccessException
	{
		try
		{
			LOGGER.debug("Retrieving page of unread news from {}", restClient.getServiceURI(NEWS_URL + "/unread"));

			ResponseEntity<RestResponsePage<SimpleNewsDTO>> news = restClient.exchange(restClient.getServiceURI(
				NEWS_URL + "/unread?page=" + page + "&size=" + size),
			                                                                           HttpMethod.GET,
			                                                                           null,
			                                                                           new ParameterizedTypeReference<RestResponsePage<SimpleNewsDTO>>()
			                                                                           {
			                                                                           });

			LOGGER.debug("Result status was {} with content {}", news.getStatusCode(), news.getBody());
			return news.getBody();
		}
		catch(HttpStatusCodeException e)
		{
			throw new DataAccessException("Failed retrieve news with status code " + e.getStatusCode());
		}
		catch(RestClientException e)
		{
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	@Override
	public DetailedNewsDTO publishNews(DetailedNewsDTO news) throws DataAccessException
	{
		try
		{
			LOGGER.debug("Publishing news on {}", restClient.getServiceURI(NEWS_URL));

			ResponseEntity<DetailedNewsDTO> published = restClient.exchange(restClient.getServiceURI(NEWS_URL),
			                                                                HttpMethod.POST,
			                                                                new HttpEntity<>(news),
			                                                                DetailedNewsDTO.class);

			LOGGER.debug("Result status was {} with content {}", published.getStatusCode(), published.getBody());
			return published.getBody();
		}
		catch(HttpStatusCodeException e)
		{
			throw new DataAccessException("Failed retrieve news with status code " + e.getStatusCode());
		}
		catch(RestClientException e)
		{
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	@Override
	public DetailedNewsDTO findOne(SimpleNewsDTO news) throws DataAccessException
	{
		try
		{
			LOGGER.debug("Publishing news on {}", restClient.getServiceURI(NEWS_URL));

			ResponseEntity<DetailedNewsDTO> published = restClient.exchange(
				restClient.getServiceURI(NEWS_URL) + "/" + news.getId(), HttpMethod.GET, null, DetailedNewsDTO.class);

			LOGGER.debug("Result status was {} with content {}", published.getStatusCode(), published.getBody());
			return published.getBody();
		}
		catch(HttpStatusCodeException e)
		{
			throw new DataAccessException("Failed retrieve news with status code " + e.getStatusCode());
		}
		catch(RestClientException e)
		{
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	@Override
	public DetailedNewsDTO update(DetailedNewsDTO newsDTO) throws DataAccessException
	{
		try
		{
			LOGGER.debug("Updating news on {}", restClient.getServiceURI(NEWS_URL));

			ResponseEntity<DetailedNewsDTO> updated = restClient.exchange(restClient.getServiceURI(NEWS_URL),
			                                                              HttpMethod.PUT,
			                                                              new HttpEntity<>(newsDTO),
			                                                              DetailedNewsDTO.class);

			LOGGER.debug("Result status was {} with content {}", updated.getStatusCode(), updated.getBody());
			return updated.getBody();
		}
		catch(HttpStatusCodeException e)
		{
			throw new DataAccessException("Failed retrieve news with status code " + e.getStatusCode());
		}
		catch(RestClientException e)
		{
			throw new DataAccessException(e.getMessage(), e);
		}
	}
}
