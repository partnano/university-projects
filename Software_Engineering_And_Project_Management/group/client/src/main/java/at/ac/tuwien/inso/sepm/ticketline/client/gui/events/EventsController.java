package at.ac.tuwien.inso.sepm.ticketline.client.gui.events;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.client.exception.ValidationException;
import at.ac.tuwien.inso.sepm.ticketline.client.gui.MiscAlerts;
import at.ac.tuwien.inso.sepm.ticketline.client.gui.TabHeaderController;
import at.ac.tuwien.inso.sepm.ticketline.client.gui.bookingeditor.SeatBookingEditorController;
import at.ac.tuwien.inso.sepm.ticketline.client.gui.bookingeditor.SectionBookingEditorController;
import at.ac.tuwien.inso.sepm.ticketline.client.service.BookingService;
import at.ac.tuwien.inso.sepm.ticketline.client.service.EventService;
import at.ac.tuwien.inso.sepm.ticketline.client.util.BundleManager;
import at.ac.tuwien.inso.sepm.ticketline.rest.booking.BookingDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.booking.SeatBookingDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.booking.SectionBookingDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventAreaDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventFilterDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.enums.Category;
import at.ac.tuwien.inso.sepm.ticketline.rest.location.RowDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.location.SectionDTO;
import at.ac.tuwien.inso.springfx.SpringFxmlLoader;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.Duration;
import javafx.util.StringConverter;
import org.controlsfx.glyphfont.FontAwesome;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import java.time.format.DateTimeFormatter;
import java.util.Iterator;
import java.util.Optional;

@Component
public class EventsController
{
	private static final Logger LOGGER = LoggerFactory.getLogger(EventsController.class);

	private final SpringFxmlLoader springFxmlLoader;
	public TextField ui_filterNameInput;
	public TextField ui_filterDescriptionInput;
	public TextField ui_filterDurationInput;
	public TextField ui_filterLocationDescriptionInput;
	public TextField ui_filterStreetInput;
	public TextField ui_filterPlaceInput;
	public TextField ui_filterZipInput;
	public TextField ui_filterCountryInput;
	public DatePicker ui_filterCalendarDatePicker;
	public TextField ui_filterDateTimeInput;
	public TextField ui_filterDatePriceInput;
	public TextField ui_filterArtistFirstNameInput;
	public TextField ui_filterArtistLastNameInput;
	public ComboBox<Category> ui_filterTypeDropdown;
	public Button ui_findReservationButton;
	public Button ui_resetFilterButton;
	public ScrollPane ui_filterScrollPane;
	public GridPane ui_filterGridPane;
	public Label ui_filterResetLabel;
	public TableView<EventDTO> ui_eventsTable;
	public TableColumn<EventDTO, String> ui_nameColumn;
	public TableColumn<EventDTO, String> ui_locationColumn;
	public TableColumn<EventDTO, String> ui_dateColumn;
	public TableColumn<EventDTO, String> ui_artistColumn;
	public Button ui_createEventButton;

	public HBox ui_paginationControlBox;

	private final EventService eventService;
	private ObservableList<EventDTO> eventList;
	private static final int ELEMENTS_PER_PAGE = 30;
	private int currentPage = 1;
	private final FontAwesome fontAwesome;
	@FXML
	private Button ui_firstButton;
	@FXML
	private Button ui_lastButton;
	@FXML
	private Button ui_previousButton;
	@FXML
	private Button ui_nextButton;
	@FXML
	private Label ui_paginationLabel;

	private EventFilterDTO filter = null;

	private EventDTO lastSavedEvent = null;

	private final BookingService bookingService;

	@FXML
	private TabHeaderController tabHeaderController;

	public EventsController(SpringFxmlLoader springFxmlLoader, EventService eventService,
	                        BookingService bookingService)
	{
		this.springFxmlLoader = springFxmlLoader;
		this.eventService = eventService;
		this.bookingService = bookingService;
		fontAwesome = new FontAwesome();
	}

	@FXML
	private void initialize()
	{
		LOGGER.trace("EventsController initialize...");

		tabHeaderController.setIcon(FontAwesome.Glyph.CALENDAR);
		tabHeaderController.setTitle("Events");
		ui_filterResetLabel.managedProperty().bind(ui_filterResetLabel.visibleProperty());
		ui_filterResetLabel.setVisible(false);

		eventList = FXCollections.observableArrayList();

		ui_eventsTable.setItems(eventList);
		//ui_eventsTable.setFixedCellSize(25);
		//ui_eventsTable.prefHeightProperty().bind(ui_eventsTable.fixedCellSizeProperty().multiply(Bindings.size(ui_eventsTable.getItems())));

		ui_nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
		ui_dateColumn.setCellValueFactory(cellData ->
		                                  {
			                                  DateTimeFormatter formatter = DateTimeFormatter.ofPattern(
				                                  "yyyy-MM-dd HH:mm");
			                                  return new SimpleObjectProperty<>(cellData.getValue()
			                                                                            .getTime()
			                                                                            .format(formatter));
		                                  }

		                                 );

		ui_locationColumn.setCellValueFactory(cellData -> new SimpleObjectProperty<>(cellData.getValue()
		                                                                                     .getLocation()
		                                                                                     .getName()));

		ui_artistColumn.setCellValueFactory(new PropertyValueFactory<>("artist"));

		ui_filterTypeDropdown.setItems(FXCollections.observableArrayList(null, Category.CONCERT, Category.MUSICAL));
		ui_filterTypeDropdown.setCellFactory(new Callback<>()
		{
			@Override
			public ListCell<Category> call(ListView<Category> l)
			{
				return new ListCell<>()
				{
					@Override
					protected void updateItem(Category category, boolean empty)
					{
						super.updateItem(category, empty);
						if(category == null || empty)
						{
							setGraphic(null);
						}
						else
						{
							setText(BundleManager.getBundle().getString(category.getValue()));
						}
					}
				};
			}
		});
		ui_filterTypeDropdown.setConverter(new StringConverter<>()
		{
			@Override
			public String toString(Category category)
			{
				if(category == null)
				{
					return null;
				}
				else
				{
					return BundleManager.getBundle().getString(category.getValue());
				}
			}

			@Override
			public Category fromString(String locID)
			{
				return null;
			}
		});
		ui_filterTypeDropdown.getSelectionModel().selectFirst();

		ui_eventsTable.setRowFactory(tv -> {
			TableRow<EventDTO> row = new TableRow<>();

			row.setOnMouseClicked(event -> {
				if(!row.isEmpty() && event.getButton() == MouseButton.PRIMARY && event.getClickCount() == 2)
				{

					EventDTO clickedEvent = row.getItem();

					selectEvent(clickedEvent);
				}
			});
			return row;
		});
	}

	public void ui_findReservationButtonClicked(ActionEvent actionEvent)
	{
		LOGGER.info("FindReservation button pressed");

		TextInputDialog dialog = new TextInputDialog();
		dialog.setTitle(BundleManager.getBundle().getString("events.findReservation.window.title"));
		dialog.setHeaderText(null);
		dialog.setContentText(BundleManager.getBundle().getString("events.findReservation.window.content"));
		dialog.setGraphic(null);

		Optional<String> result = dialog.showAndWait();
		if(!result.isPresent())
			return;

		try
		{
			Integer reservationNumber = Integer.parseInt(result.get());
			BookingDTO booking = bookingService.findByReservationNumber(reservationNumber);

			LOGGER.info("Opening bookingeditor via booking (reservationnumber)");

			Stage parent = (Stage)ui_eventsTable.getScene().getWindow();

			if(booking instanceof SectionBookingDTO)
				SectionBookingEditorController.showAndWait(parent, springFxmlLoader, (SectionBookingDTO)booking);

			else if(booking instanceof SeatBookingDTO)
				SeatBookingEditorController.showAndWait(parent, springFxmlLoader, (SeatBookingDTO)booking);

			else
				throw new AssertionError("unknown booking type: " + booking.getClass().getName());
		}
		catch(NumberFormatException ex)
		{
			LOGGER.error("NumberFormatException: ", ex);
			ValidationException vex = new ValidationException();
			vex.addMessage("events.findReservation.window.errorMessage");
			MiscAlerts.showValidationErrorAlert(vex);
		}
		catch(DataAccessException e)
		{
			MiscAlerts.showDataAccessAlert();
		}
		catch(ValidationException e)
		{
			MiscAlerts.showValidationErrorAlert(e);
		}
	}

	public void ui_resetFilterButtonClicked(ActionEvent actionEvent)
	{
		LOGGER.info("ResetFilter button pressed");

		for(Node node : ui_filterGridPane.getChildren())
		{
			if(node instanceof TextField)
			{
				((TextField)node).setText("");
			}
		}

		ui_filterResetLabel.setVisible(true);


		Timeline timeout = new Timeline(new KeyFrame(Duration.seconds(5),
		                                             event -> ui_filterResetLabel.setVisible(false)));
		timeout.setCycleCount(1);
		timeout.play();
		currentPage = 1;
		this.filter = null;
		loadEvents();
	}

	public void ui_applyFilterButtonClick()
	{
		LOGGER.info("ApplyFilter button pressed");

		String name = ui_filterNameInput.getText();
		Category category = ui_filterTypeDropdown.getSelectionModel().getSelectedItem();
		Integer duration = null;
		try
		{
			duration = Integer.parseInt(ui_filterDurationInput.getText());
		}
		catch(NumberFormatException ex)
		{
			//ignore
		}

		EventFilterDTO filter = new EventFilterDTO();

		filter.setArtistFirstname(ui_filterArtistFirstNameInput.getText());
		filter.setArtistLastname(ui_filterArtistLastNameInput.getText());
		filter.setName(name);
		filter.setDescription(ui_filterDescriptionInput.getText());
		filter.setCategory(category);
		filter.setDuration(duration);

		filter.setLocationName(ui_filterLocationDescriptionInput.getText());
		filter.setStreet(ui_filterStreetInput.getText());
		filter.setCity(ui_filterPlaceInput.getText());
		filter.setZip(ui_filterZipInput.getText());
		filter.setCountry(ui_filterCountryInput.getText());

		if(ui_filterDateTimeInput.getText().contains(":"))
		{
			String[] stringTime = ui_filterDateTimeInput.getText().trim().split(":");
			filter.setTime(ui_filterCalendarDatePicker.getValue()
			                                          .atTime(Integer.parseInt(stringTime[0]),
			                                                  Integer.parseInt(stringTime[1])));
		}

		try
		{
			filter.setPrice(Float.parseFloat(ui_filterDatePriceInput.getText()));
		}
		catch(NumberFormatException e)
		{
			// ignore
		}

		try
		{
			eventList.clear();
			currentPage = 1;
			this.filter = filter;
			loadEvents();
			//eventList.addAll(eventService.findByFilter(filter));
		}
		catch(NumberFormatException e)
		{
			LOGGER.error("NumberFormatException " + e.getMessage());
			e.printStackTrace();
			// TODO: error dialogue
		}
	}

	public void loadEvents()
	{
		LOGGER.info("loading events...");

		try
		{
			Page<EventDTO> eventDTOPage;
			if(filter == null)
			{
				eventDTOPage = eventService.findAll(currentPage - 1, ELEMENTS_PER_PAGE);
			}
			else
			{
				eventDTOPage = eventService.findByFilter(filter, currentPage - 1, ELEMENTS_PER_PAGE);
			}

			int numPages = eventDTOPage.getTotalPages();

			ui_paginationLabel.setText(currentPage + "/" + numPages);

			ui_firstButton.setText(BundleManager.getBundle().getString("pagination.first"));
			ui_firstButton.setOnAction(event -> {
				currentPage = 1;
				loadEvents();
			});

			ui_previousButton.setGraphic(fontAwesome.create(FontAwesome.Glyph.CHEVRON_LEFT));
			ui_previousButton.setOnAction(event -> {
				if(currentPage > 1)
				{
					currentPage--;
				}
				loadEvents();
			});

			ui_lastButton.setText(BundleManager.getBundle().getString("pagination.last"));
			ui_nextButton.setGraphic(fontAwesome.create(FontAwesome.Glyph.CHEVRON_RIGHT));

			ui_lastButton.setOnAction(event -> {
				currentPage = numPages;
				loadEvents();
			});
			ui_nextButton.setOnAction(event -> {
				if(currentPage < numPages)
				{
					currentPage++;
				}
				loadEvents();
			});

			LOGGER.debug("Pages: " + currentPage + " " + numPages);

			if(currentPage >= numPages)
			{
				ui_nextButton.setDisable(true);
				ui_lastButton.setDisable(true);
			}
			else
			{
				ui_nextButton.setDisable(false);
				ui_lastButton.setDisable(false);
			}

			if(currentPage <= 1)
			{
				ui_previousButton.setDisable(true);
				ui_firstButton.setDisable(true);
			}
			else
			{
				ui_previousButton.setDisable(false);
				ui_firstButton.setDisable(false);
			}

			eventList.clear();
			eventList.addAll(eventDTOPage.getContent());
			//eventList.addAll(eventService.findAll());
		}
		catch(DataAccessException e)
		{
			MiscAlerts.showDataAccessAlert();
		}
	}

	public void ui_createEventButtonClick(ActionEvent actionEvent)
	{
		LOGGER.info("CreateEvent button pressed");

		Stage stage = (Stage)ui_filterGridPane.getScene().getWindow();
		Stage dialog = new Stage();
		dialog.addEventHandler(KeyEvent.KEY_RELEASED, (KeyEvent event) ->
		{
			if(KeyCode.ESCAPE == event.getCode())
			{
				dialog.close();
			}
		});

		dialog.setResizable(false);
		dialog.initModality(Modality.APPLICATION_MODAL);
		dialog.initOwner(stage);
		SpringFxmlLoader.Wrapper<CreateEventDialogController> wrapper = springFxmlLoader.loadAndWrap(
			"/fxml/events/createEventDialog.fxml");

		CreateEventDialogController createEventDialogController = wrapper.getController();
		createEventDialogController.setEventService(eventService);

		dialog.setScene(new Scene((Parent)wrapper.getLoadedObject()));
		dialog.setTitle(BundleManager.getBundle().getString("events.createEvent"));
		dialog.showAndWait();

		loadEvents();
	}

	public void ui_createBookingButtonClicked(ActionEvent event)
	{
		EventDTO selectedEvent = ui_eventsTable.getSelectionModel().getSelectedItem();

		if(selectedEvent != null)
		{
			selectEvent(selectedEvent);
		}
		else
		{
			Alert alert = new Alert(Alert.AlertType.ERROR);

			alert.setTitle(BundleManager.getBundle().getString("events.noEventSelectedAlertTitle"));
			alert.setHeaderText(BundleManager.getBundle().getString("events.noEventSelectedAlertTitle"));
			alert.setContentText(BundleManager.getBundle().getString("events.noEventSelectedAlert"));

			alert.showAndWait();
		}
	}

	@SuppressWarnings("unchecked")
	public void ui_onKeyPressedEventsTable(KeyEvent keyEvent)
	{
		LOGGER.info("key pressed on eventstable");
		if(keyEvent.getCode().equals(KeyCode.ENTER))
		{
			if(keyEvent.getTarget() instanceof TableView)
			{
				EventDTO clickedEvent = ((TableView<EventDTO>)keyEvent.getTarget()).getSelectionModel()
				                                                                   .getSelectedItem();
				selectEvent(clickedEvent);
			}
		}
	}

	private void selectEvent(EventDTO clickedEvent)
	{

		LOGGER.info("opening booking editor for clicked event: {}", clickedEvent);

		Iterator<EventAreaDTO> iterator = clickedEvent.getEventAreas().iterator();
		EventAreaDTO eventArea = iterator.next();

		Stage parent = (Stage)ui_eventsTable.getScene().getWindow();

		EventDTO updatedEvent = null;
		if(eventArea.getArea() instanceof SectionDTO)
		{
			updatedEvent = SectionBookingEditorController.showAndWait(parent, springFxmlLoader, clickedEvent);
		}
		else if(eventArea.getArea() instanceof RowDTO)
		{
			updatedEvent = SeatBookingEditorController.showAndWait(parent, springFxmlLoader, clickedEvent);
		}

		LOGGER.debug("updated event received: {}", updatedEvent);

		// Sync bookings with the event stored in this row
		// We are only applying the bookings here as
		// row.setItem(updatedEvent) didn't work for some kind of reason
		clickedEvent.setBookings(updatedEvent.getBookings());
	}

	public void ui_onKeyPressedFilter(KeyEvent keyEvent)
	{
		LOGGER.info("key pressed on filter");
		if(keyEvent.getCode().equals(KeyCode.ENTER))
		{
			ui_applyFilterButtonClick();
		}
	}
}


