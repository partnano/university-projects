package at.ac.tuwien.inso.sepm.ticketline.client.gui;


import at.ac.tuwien.inso.sepm.ticketline.client.exception.ValidationException;
import at.ac.tuwien.inso.sepm.ticketline.client.util.BundleManager;
import javafx.scene.control.Alert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MiscAlerts
{
	private static final Logger LOGGER = LoggerFactory.getLogger(MainController.class);

	public static void showUnderConstructionAlert()
	{
		Alert alert = new Alert(Alert.AlertType.WARNING);
		alert.setTitle(BundleManager.getBundle().getString("miscAlerts.underConstruction.title"));
		alert.setHeaderText(BundleManager.getBundle().getString("miscAlerts.underConstruction.header"));
		alert.setContentText(BundleManager.getBundle().getString("miscAlerts.underConstruction.content"));
		alert.showAndWait();
	}

	public static void showDataAccessAlert()
	{
		LOGGER.debug("Creating DataAccessAlert");

		Alert alert = new Alert(Alert.AlertType.ERROR);
		alert.setTitle(BundleManager.getBundle().getString("miscAlerts.dataAccess.title"));
		alert.setHeaderText(BundleManager.getBundle().getString("miscAlerts.dataAccess.header"));
		alert.setContentText(BundleManager.getBundle().getString("miscAlerts.dataAccess.content"));
		alert.showAndWait();
	}

	public static void showValidationErrorAlert(ValidationException ex)
	{
		Alert alert = new Alert(Alert.AlertType.ERROR);
		alert.setTitle(BundleManager.getBundle().getString("miscalerts.validation.title"));
		alert.setHeaderText("");

		StringBuilder errorMessage = new StringBuilder();
		for(String errorCode : ex.getMessages())
		{
			errorMessage.append(BundleManager.getBundle().getString(errorCode)).append("\n");
		}
		alert.setContentText(errorMessage.toString());

		alert.showAndWait();
	}
}
