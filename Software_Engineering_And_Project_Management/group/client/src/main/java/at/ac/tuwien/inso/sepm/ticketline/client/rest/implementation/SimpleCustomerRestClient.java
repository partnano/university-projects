package at.ac.tuwien.inso.sepm.ticketline.client.rest.implementation;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.client.rest.CustomerRestClient;
import at.ac.tuwien.inso.sepm.ticketline.rest.customer.CustomerDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.page.RestResponsePage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientException;

import java.util.List;

@Component
public class SimpleCustomerRestClient implements CustomerRestClient
{
	private static final Logger LOGGER = LoggerFactory.getLogger(SimpleCustomerRestClient.class);
	private static final String CUSTOMERS_URL = "/customers";

	private final RestClient client;

	public SimpleCustomerRestClient(RestClient client)
	{
		this.client = client;
	}

	@Override
	public List<CustomerDTO> findAll() throws DataAccessException
	{
		try
		{
			LOGGER.debug("retrieving all customers ...");

			ResponseEntity<List<CustomerDTO>> result = client.exchange(client.getServiceURI(CUSTOMERS_URL),
			                                                           HttpMethod.GET,
			                                                           null,
			                                                           new ParameterizedTypeReference<List<CustomerDTO>>()
			                                                           {
			                                                           });

			LOGGER.debug("... successfully retrieved {} customers", result.getBody().size());

			return result.getBody();
		}
		catch(RestClientException ex)
		{
			throw new DataAccessException(ex.getMessage(), ex);
		}
	}

	@Override
	public RestResponsePage<CustomerDTO> findAll(int page, int size) throws DataAccessException
	{
		try
		{
			LOGGER.debug("Retrieving page of all events from {}", client.getServiceURI(CUSTOMERS_URL));

			ResponseEntity<RestResponsePage<CustomerDTO>> events = client.exchange(client.getServiceURI(
					CUSTOMERS_URL + "?page=" + page + "&size=" + size), HttpMethod.GET,
			                                                                        null,
			                                                                        new ParameterizedTypeReference<RestResponsePage<CustomerDTO>>()
			                                                                        {
			                                                                        });

			LOGGER.debug("Result status was {} ", events.getStatusCode());
			return events.getBody();
		}
		catch(HttpStatusCodeException e)
		{
			throw new DataAccessException("Failed retrieve events with status code " + e.getStatusCode());
		}
		catch(RestClientException e)
		{
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	@Override
	public CustomerDTO findOne(Long id) throws DataAccessException
	{
		try
		{
			LOGGER.debug("retrieving customer with id {} ...", id);

			ResponseEntity<CustomerDTO> result = client.exchange(client.getServiceURI(CUSTOMERS_URL + "/" + id),
			                                                     HttpMethod.GET,
			                                                     null,
			                                                     CustomerDTO.class);

			LOGGER.debug("... sucessfully retrieved customer with id {}", id);

			return result.getBody();
		}
		catch(RestClientException ex)
		{
			throw new DataAccessException(ex.getMessage(), ex);
		}
	}

	@Override
	public CustomerDTO create(CustomerDTO customer) throws DataAccessException
	{
		try
		{
			LOGGER.debug("creating new customer ...");

			ResponseEntity<CustomerDTO> result = client.exchange(client.getServiceURI(CUSTOMERS_URL),
			                                                     HttpMethod.POST,
			                                                     new HttpEntity<>(customer),
			                                                     CustomerDTO.class);

			LOGGER.debug("... successfully created new customer, id = {}", result.getBody().getId());

			return result.getBody();
		}
		catch(RestClientException ex)
		{
			throw new DataAccessException(ex.getMessage(), ex);
		}
	}

	@Override
	public CustomerDTO update(CustomerDTO customer) throws DataAccessException
	{
		try
		{
			LOGGER.debug("updating customer ...");

			ResponseEntity<CustomerDTO> result = client.exchange(client.getServiceURI(
				CUSTOMERS_URL + "/" + customer.getId()), HttpMethod.PUT, new HttpEntity<>(customer),
			                                                     CustomerDTO.class);

			LOGGER.debug("... successfully updated customer");

			return result.getBody();
		}
		catch(RestClientException ex)
		{
			throw new DataAccessException(ex.getMessage(), ex);
		}
	}
}
