package at.ac.tuwien.inso.sepm.ticketline.client.gui.users;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.client.exception.ValidationException;
import at.ac.tuwien.inso.sepm.ticketline.client.gui.MiscAlerts;
import at.ac.tuwien.inso.sepm.ticketline.client.service.UserService;
import at.ac.tuwien.inso.sepm.ticketline.client.util.BundleManager;
import at.ac.tuwien.inso.sepm.ticketline.rest.user.UserDTO;
import at.ac.tuwien.inso.springfx.SpringFxmlLoader;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ModifyUserWindowController
{
	@FXML
	private TextField ui_emailField;

	@FXML
	private TextField ui_firstNameInput;

	@FXML
	private TextField ui_lastNameInput;

	@FXML
	private ComboBox<String> ui_roleDropdown;

	@FXML
	private Label ui_modifyUserLabel;

	@FXML
	private Button ui_createUserButton;

	@Autowired
	private UserService service;

	private String titleText;
	private UserDTO currentUser = null;

	@FXML
	private void initialize()
	{
		ObservableList<String> roles = FXCollections.observableArrayList();
		roles.add(BundleManager.getBundle().getString("users.user"));
		roles.add(BundleManager.getBundle().getString("users.admin"));

		ui_roleDropdown.setItems(roles);
		ui_roleDropdown.getSelectionModel().selectFirst();
		loadContent();
	}

	private void loadContent()
	{
		ui_modifyUserLabel.setText(titleText);
		ui_createUserButton.setText(titleText);

		if(currentUser != null)
		{
			ui_emailField.setText(currentUser.getEmail());
			ui_firstNameInput.setText(currentUser.getFirstName());
			ui_lastNameInput.setText(currentUser.getLastName());

			if(currentUser.isAdmin())
			{
				ui_roleDropdown.getSelectionModel().selectLast();
			}
			else
			{
				ui_roleDropdown.getSelectionModel().selectFirst();
			}
		}
	}

	public void onCreateUserButtonClick()
	{
		UserDTO user = new UserDTO();
		user.setEmail(ui_emailField.getText());
		user.setFirstName(ui_firstNameInput.getText());
		user.setLastName(ui_lastNameInput.getText());

		boolean isAdmin = ui_roleDropdown.getSelectionModel()
		                                 .getSelectedItem()
		                                 .equals(BundleManager.getBundle().getString("users.admin"));
		user.setAdmin(isAdmin);

		try
		{
			if(currentUser == null)
				service.create(user);
			else
			{
				user.setId(currentUser.getId());
				service.update(user);
			}

			Stage stage = (Stage)ui_roleDropdown.getScene().getWindow();
			stage.close();
		}
		catch(DataAccessException e)
		{
			MiscAlerts.showDataAccessAlert();
		}
		catch(ValidationException e)
		{
			MiscAlerts.showValidationErrorAlert(e);
		}
	}

	public static void showAndWait(Stage stage, SpringFxmlLoader loader, String titleText, UserDTO user)
	{
		Stage dialog = new Stage();
		dialog.setResizable(false);
		dialog.initModality(Modality.APPLICATION_MODAL);
		dialog.initOwner(stage);

		dialog.addEventHandler(KeyEvent.KEY_RELEASED, (KeyEvent event) ->
		{
			if(KeyCode.ESCAPE == event.getCode())
			{
				dialog.close();
			}
		});

		SpringFxmlLoader.Wrapper<ModifyUserWindowController> wrapper = loader.loadAndWrap(
			"/fxml/users/modifyUserWindow.fxml");
		ModifyUserWindowController controller = wrapper.getController();
		controller.titleText = titleText;
		controller.currentUser = user;
		controller.loadContent();

		dialog.setScene(new Scene((Parent)wrapper.getLoadedObject()));
		dialog.setTitle(BundleManager.getBundle().getString("users.createUser.title"));
		dialog.showAndWait();
	}
}
