package at.ac.tuwien.inso.sepm.ticketline.client.validation;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.ValidationException;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventAreaDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.location.AreaDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EventValidator
{
	private static final Logger LOGGER = LoggerFactory.getLogger(EventValidator.class);

	public static void validate(EventDTO event) throws ValidationException
	{
		ValidationException ex = new ValidationException();

		if(event == null)
			ex.addMessage("events.empty");
		if(event != null)
		{
			if((event.getName() == null || event.getName().trim().isEmpty()))
				ex.addMessage("events.nameEmpty");

			if(event.getDuration() == null)
				ex.addMessage("events.durationEmpty");

			else if(event.getDuration() < 0)
				ex.addMessage("events.durationNegative");

			if(event.getTime() == null)
				ex.addMessage("events.timeEmpty");

			if(event.getLocation() == null)
				ex.addMessage("events.locationEmpty");

			if(event.getCategory() == null)
				ex.addMessage("events.categoryEmpty");

			if(event.getArtist() == null || event.getArtist().trim().isEmpty())
				ex.addMessage("events.artistEmpty");

			EventAreaDTO[] eventAreas = new EventAreaDTO[event.getEventAreas().size()];
			event.getEventAreas().toArray(eventAreas);
			outerloop:
			for(int i = 0; i < event.getEventAreas().size(); ++i)
			{
				for(int j = i + 1; j < event.getEventAreas().size(); ++j)
				{
					if(eventAreas[i].getArea().equals(eventAreas[j].getArea()))
					{
						ex.addMessage("events.areasDuplicate");
						break outerloop;
					}
				}
			}

			if(event.getLocation() != null)
			{
				for(AreaDTO area : event.getLocation().getAreas())
				{
					boolean found = false;
					for(EventAreaDTO curr : event.getEventAreas())
					{
						if(curr.getArea().equals(area))
							found = true;
					}
					if(!found)
					{
						ex.addMessage("events.areasMissing");
						break;
					}
				}
			}
		}

		if(ex.hasMessage())
		{
			LOGGER.info("Event validation failed: {}", ex);
			throw ex;
		}
	}

	public static void validateLimit(int limit) throws ValidationException
	{
		if(limit <= 0)
		{
			ValidationException ex = new ValidationException();
			ex.addMessage("events.limitNegativeOrZero");
			throw ex;
		}
	}

	public static void validateMonth(Integer month) throws ValidationException
	{
		if(month == null)
			return;

		if(month < 1 || month > 12)
		{
			ValidationException ex = new ValidationException();
			ex.addMessage("events.invalidMonth");
			throw ex;
		}
	}

	public static void validateYear(Integer year) throws ValidationException
	{
		if(year == null)
			return;

		if(year < 2000)
		{
			ValidationException ex = new ValidationException();
			ex.addMessage("events.invalidYear");
			throw ex;
		}
	}
}
