package at.ac.tuwien.inso.sepm.ticketline.client.service.implementation;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.client.exception.ValidationException;
import at.ac.tuwien.inso.sepm.ticketline.client.rest.BookingRestClient;
import at.ac.tuwien.inso.sepm.ticketline.client.service.PaymentService;
import at.ac.tuwien.inso.sepm.ticketline.client.validation.CreditCardValidator;
import at.ac.tuwien.inso.sepm.ticketline.rest.booking.BookingDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.payment.CreditCardDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class AbstractPaymentService implements PaymentService
{
	private static final Logger LOGGER = LoggerFactory.getLogger(AbstractPaymentService.class);

	@Autowired
	private BookingRestClient bookingRestClient;

	protected BookingRestClient getBookingRestClient()
	{
		return bookingRestClient;
	}

	@Override
	public BookingDTO pay(BookingDTO bookingDTO, CreditCardDTO creditCardDTO) throws DataAccessException,
	                                                                                 ValidationException
	{
		LOGGER.info("paying {}", bookingDTO);
		CreditCardValidator.validate(creditCardDTO);
		BookingDTO updatedBookingDTO = bookingRestClient.pay(bookingDTO, creditCardDTO);
		LOGGER.info("paid {}", updatedBookingDTO);
		return updatedBookingDTO;
	}

	@Override
	public BookingDTO refund(BookingDTO bookingDTO) throws DataAccessException
	{
		LOGGER.info("refunding {}", bookingDTO);
		BookingDTO updatedBookingDTO = bookingRestClient.refund(bookingDTO);
		LOGGER.info("refunded {}", updatedBookingDTO);
		return updatedBookingDTO;
	}
}
