package at.ac.tuwien.inso.sepm.ticketline.client.gui.customers;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.client.exception.ValidationException;
import at.ac.tuwien.inso.sepm.ticketline.client.gui.MiscAlerts;
import at.ac.tuwien.inso.sepm.ticketline.client.service.CustomerService;
import at.ac.tuwien.inso.sepm.ticketline.client.util.BundleManager;
import at.ac.tuwien.inso.sepm.ticketline.rest.customer.CustomerDTO;
import at.ac.tuwien.inso.springfx.SpringFxmlLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.springframework.stereotype.Component;

@Component
public class AddCustomerController
{
	public TextField ui_firstNameInput;
	public TextField ui_lastNameInput;
	public TextField ui_emailInput;
	public Button ui_addCustomerButton;

	private CustomerService customerService;

	private void setCustomerService(CustomerService customerService)
	{
		this.customerService = customerService;
	}

	public void ui_addCustomerButtonClick()
	{
		CustomerDTO newCustomer = new CustomerDTO();
		newCustomer.setFirstName(ui_firstNameInput.getText());
		newCustomer.setLastName(ui_lastNameInput.getText());
		newCustomer.setEmail((ui_emailInput.getText()));

		try
		{
			customerService.create(newCustomer);
			Stage tmp = (Stage)ui_firstNameInput.getScene().getWindow();
			tmp.close();
		}
		catch(DataAccessException e)
		{
			MiscAlerts.showDataAccessAlert();
		}
		catch(ValidationException e)
		{
			MiscAlerts.showValidationErrorAlert(e);
		}
	}

	public static void create(Stage stage, SpringFxmlLoader springFxmlLoader, CustomerService customerService)
	{
		Stage dialog = new Stage();
		dialog.setResizable(false);
		dialog.initModality(Modality.APPLICATION_MODAL);
		dialog.initOwner(stage);

		dialog.addEventHandler(KeyEvent.KEY_RELEASED, (KeyEvent keyEvent) ->
		{
			if(KeyCode.ESCAPE == keyEvent.getCode())
			{
				dialog.close();
			}
		});

		SpringFxmlLoader.Wrapper<AddCustomerController> wrapper = springFxmlLoader.loadAndWrap(
			"/fxml/customers/addCustomerWindow.fxml");
		AddCustomerController addCustomerController = wrapper.getController();
		addCustomerController.setCustomerService(customerService);

		dialog.setScene(new Scene((Parent)wrapper.getLoadedObject()));
		dialog.setTitle(BundleManager.getBundle().getString("customers.addCustomer"));
		dialog.showAndWait();
	}
}
