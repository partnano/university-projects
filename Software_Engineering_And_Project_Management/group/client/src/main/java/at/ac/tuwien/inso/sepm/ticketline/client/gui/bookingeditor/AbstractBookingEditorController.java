package at.ac.tuwien.inso.sepm.ticketline.client.gui.bookingeditor;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.client.exception.ValidationException;
import at.ac.tuwien.inso.sepm.ticketline.client.gui.MiscAlerts;
import at.ac.tuwien.inso.sepm.ticketline.client.gui.locationplan.LocationPlanController;
import at.ac.tuwien.inso.sepm.ticketline.client.gui.payment.PaymentController;
import at.ac.tuwien.inso.sepm.ticketline.client.service.BookingService;
import at.ac.tuwien.inso.sepm.ticketline.client.service.EventService;
import at.ac.tuwien.inso.sepm.ticketline.client.util.BundleManager;
import at.ac.tuwien.inso.sepm.ticketline.client.util.JavaFXUtils;
import at.ac.tuwien.inso.sepm.ticketline.client.util.PDFReceiptGenerator;
import at.ac.tuwien.inso.sepm.ticketline.rest.booking.BookingDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.customer.CustomerDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventDTO;
import at.ac.tuwien.inso.springfx.SpringFxmlLoader;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.transform.Scale;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Duration;
import org.controlsfx.glyphfont.FontAwesome;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.File;
import java.time.format.DateTimeFormatter;

@Component
public abstract class AbstractBookingEditorController
{
	private static final Logger LOGGER = LoggerFactory.getLogger(AbstractBookingEditorController.class);

	@FXML
	protected TableView ui_selectedSeatsTable;

	@FXML
	protected GridPane ui_detailsPane;

	@FXML
	protected Label ui_eventNameLabel;

	@FXML
	protected Label ui_eventDescriptionLabel;

	@FXML
	protected TitledPane ui_infoPane;

	@FXML
	protected TitledPane ui_selectedSeatsPane;

	@FXML
	protected Button ui_showLegendButton;

	@FXML
	protected Button ui_bookButton;

	@FXML
	protected Button ui_reserveButton;

	@FXML
	protected Button ui_printReceiptButton;

	@FXML
	protected Button ui_cancelReservationButton;

	@FXML
	protected Label ui_invalidSelectionLabel;

	@Autowired
	private final EventService eventServiceNonStatic;

	@Autowired
	private final BookingService bookingServiceNonStatic;

	// to be able to access them in static methods
	private static EventService eventService;
	protected static BookingService bookingService;

	private BookingEditorLegendWindowController bookingEditorLegendWindowController;
	protected BookingAssignCustomerController assignCustomerController;
	protected PaymentController paymentController;
	protected Stage dialog;

	protected EventDTO event;
	protected BookingDTO booking;

	public ScrollPane ui_locationPlanScrollPane;
	private Group zoomGroup;
	public Button ui_zoomOutButton;
	public Button ui_zoomInButton;
	public HBox ui_zoomButtonBox;
	private final FontAwesome fontAwesome;

	private AnchorPane locationPlanNode;

	protected LocationPlanController locationPlanController;

	private double scrollPaneMouseX;
	private double scrollPaneMouseY;

	protected AbstractBookingEditorController(EventService eventService, BookingService bookingService)
	{
		fontAwesome = new FontAwesome();

		this.eventServiceNonStatic = eventService;
		this.bookingServiceNonStatic = bookingService;
	}

	@PostConstruct
	private void initStaticServices()
	{
		eventService = this.eventServiceNonStatic;
		bookingService = this.bookingServiceNonStatic;
	}

	@FXML
	protected void initialize()
	{
		ui_infoPane.setCollapsible(false);
		ui_infoPane.setVisible(false);
		ui_selectedSeatsPane.setCollapsible(false);
		ui_eventNameLabel.setText("");
		ui_eventDescriptionLabel.setText("");
		ui_invalidSelectionLabel.setVisible(false);

		Platform.runLater(() ->
		{
			ui_locationPlanScrollPane.setOnMouseMoved(event ->
			{
				scrollPaneMouseX = event.getX();
				scrollPaneMouseY = event.getY();
			});

			ui_locationPlanScrollPane.getScene().setOnKeyPressed(event -> {
				switch(event.getCode())
				{
				case MINUS:
					downscaleLocationPlan();
					break;
				case PLUS:
					upscaleLocationPlan();
					break;
				case ESCAPE:
					dialog.close();
				}
			});
		});

		Tooltip zoomTip = new Tooltip(BundleManager.getBundle().getString("locationPlan.zoomTip"));
		zoomTip.setShowDelay(new Duration(200));

		ui_zoomInButton.setGraphic(fontAwesome.create(FontAwesome.Glyph.SEARCH_PLUS));
		ui_zoomInButton.setTooltip(zoomTip);
		ui_zoomOutButton.setGraphic(fontAwesome.create(FontAwesome.Glyph.SEARCH_MINUS));
		ui_zoomOutButton.setTooltip(zoomTip);

		// buttons managed property to visibiliy -> buttons only take up space when visible
		ui_printReceiptButton.managedProperty().bind(ui_printReceiptButton.visibleProperty());
		ui_bookButton.managedProperty().bind(ui_bookButton.visibleProperty());
		ui_reserveButton.managedProperty().bind(ui_reserveButton.visibleProperty());
		ui_cancelReservationButton.managedProperty().bind(ui_cancelReservationButton.visibleProperty());
	}

	public void createLocationPlan(SpringFxmlLoader.Wrapper<LocationPlanController> locationPlan)
	{

		locationPlanNode = (AnchorPane)locationPlan.getLoadedObject();

		Platform.runLater(() -> {
			VBox locationPlanBox = new VBox();
			locationPlanBox.setPrefSize(ui_locationPlanScrollPane.getWidth() - 20, ui_locationPlanScrollPane.getHeight() - 20);
			locationPlanBox.setMinSize(locationPlan.getController().getPrefWidth(), locationPlan.getController().getPrefHeight());
			locationPlanBox.setAlignment(Pos.CENTER);
			locationPlanBox.getChildren().add(locationPlanNode);
			zoomGroup = new Group();
			zoomGroup.getChildren().add(locationPlanBox);

			Group contentGroup = new Group(zoomGroup);

			LOGGER.debug(ui_locationPlanScrollPane.getWidth() + " / " + ui_zoomButtonBox.getWidth());

			ui_locationPlanScrollPane.setContent(contentGroup);
		});
	}

	private void setBookingEditorLegendWindowController(
		BookingEditorLegendWindowController bookingEditorLegendWindowController)
	{
		this.bookingEditorLegendWindowController = bookingEditorLegendWindowController;
	}

	public void setBooking(BookingDTO booking)
	{
		this.booking = booking;
	}

	protected void setEvent(EventDTO event)
	{
		this.event = event;

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.YYYY, HH:mm");
		String eventTime = event.getTime().format(formatter);
		ui_eventNameLabel.setText(event.getName());
		ui_eventDescriptionLabel.setText(eventTime + ", " + event.getLocation().getName());
	}

	public EventDTO getEvent()
	{
		return event;
	}

	@FXML
	private void ui_onShowLegendButtonClick()
	{
		bookingEditorLegendWindowController.show();
	}

	@FXML
	private void ui_onCancelReservationButtonClick()
	{
		if(booking != null)
		{
			try
			{
				BookingDTO canceledBooking = bookingService.delete(booking);

				reloadEvent();
				onBookingSaved(canceledBooking);
			}
			catch(DataAccessException ex)
			{
				MiscAlerts.showDataAccessAlert();
			}
			catch(ValidationException ex)
			{
				//can't happen
			}
		}
	}

	@FXML
	private void ui_onReserveButtonClick()
	{
		assignCustomerController.show();

		try
		{
			if(booking != null)
				booking = bookingService.create(booking);

			reloadEvent();
			onBookingSaved(booking);
		}
		catch(DataAccessException | ValidationException ex)
		{
			JavaFXUtils.createExceptionDialog(ex, dialog);
		}
	}

	@FXML
	private void ui_onBookButtonClick()
	{
		if(booking == null)
			assignCustomerController.show();

		if(booking != null && booking.getCustomer() != null)
		{
			booking.setEvent(event);
			paymentController.setBooking(booking);
			paymentController.show();
			onBookingSaved(booking);
			try
			{
				reloadEvent();
			}
			catch(DataAccessException ex)
			{
				MiscAlerts.showDataAccessAlert();
			}
		}
	}

	@FXML
	void ui_onPrintReceiptButtonClick()
	{
		LOGGER.info("saving receipt...");

		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle(BundleManager.getBundle().getString("bookingEditor.printReceiptButton"));

		// Allow only pdfs to be saved
		FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("PDF (*.pdf)", "*.pdf");
		fileChooser.getExtensionFilters().add(extFilter);

		String initialFileName = BundleManager.getBundle().getString("receipt.filename");

		if(booking.isCanceled())
			initialFileName = BundleManager.getBundle().getString("receipt.cancellationFilename");

		initialFileName += booking.getReservationNumber();

		fileChooser.setInitialFileName(initialFileName);

		File file = fileChooser.showSaveDialog(ui_printReceiptButton.getScene().getWindow());

		if(file == null)
			return;

		PDFReceiptGenerator.ReceiptType receiptType = PDFReceiptGenerator.ReceiptType.CONFIRMATION;

		if(booking.isCanceled())
			receiptType = PDFReceiptGenerator.ReceiptType.CANCELLATION;

		booking.setEvent(event);
		PDFReceiptGenerator pdfGenerator = new PDFReceiptGenerator(receiptType, booking);
		try
		{
			pdfGenerator.generateReceipt(file);
		}
		catch(PDFReceiptGenerator.PDFCreationException e)
		{
			LOGGER.error("could not generate pdf: {}", e);
			MiscAlerts.showDataAccessAlert();
			return;
		}
		booking.setEvent(null);
	}

	protected abstract BookingDTO getBookingWithSelectedAreas();

	protected void onCustomerAssignedToBooking(CustomerDTO customer)
	{
		// booking is null if new booking (not converting reservation to booking)
		if(booking == null)
		{
			booking = getBookingWithSelectedAreas();
			booking.setEvent(event);
			booking.setCustomer(customer);
		}
	}

	protected void onBookingPaid(BookingDTO savedBooking)
	{
		try
		{
			onBookingSaved(savedBooking);
			reloadEvent();
		}
		catch(DataAccessException ex)
		{
			JavaFXUtils.createExceptionDialog(ex, dialog);
		}
	}

	private void reloadEvent() throws DataAccessException
	{
		event = eventService.findOne(event.getId());
		locationPlanController.setEvent(event);
	}

	protected abstract void onBookingSaved(BookingDTO booking);

	/**
	 * client-side controlled constrained: setBooking has to be called before
	 */
	protected abstract void updateBookingInfo();

	protected static Stage createDialog(Stage stage)
	{
		Stage dialog = new Stage();
		dialog.initModality(Modality.APPLICATION_MODAL);
		dialog.initOwner(stage);
		return dialog;
	}

	protected static void createAndSetLegend(Stage stage, AbstractBookingEditorController controller,
	                                         SpringFxmlLoader springFxmlLoader)
	{
		BookingEditorLegendWindowController legend = new BookingEditorLegendWindowController();
		legend.create(stage, springFxmlLoader);

		controller.setBookingEditorLegendWindowController(legend);
	}

	private void upscaleLocationPlan()
	{
		ui_locationPlanScrollPane.setVvalue(scrollPaneMouseY / ui_locationPlanScrollPane.getHeight());
		ui_locationPlanScrollPane.setHvalue(scrollPaneMouseX / ui_locationPlanScrollPane.getWidth());
		zoomGroup.getTransforms().add(new Scale(1.1, 1.1, 600, 600));
	}

	private void downscaleLocationPlan()
	{
		ui_locationPlanScrollPane.setVvalue(scrollPaneMouseY / ui_locationPlanScrollPane.getHeight());
		ui_locationPlanScrollPane.setHvalue(scrollPaneMouseX / ui_locationPlanScrollPane.getWidth());
		zoomGroup.getTransforms().add(new Scale(0.9, 0.9, 600, 600));
	}

	public void onZoomOutButtonPressed()
	{
		downscaleLocationPlan();
	}

	public void onZoomInButtonPressed()
	{
		upscaleLocationPlan();
	}
}
