package at.ac.tuwien.inso.sepm.ticketline.client.service.implementation;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.client.exception.ValidationException;
import at.ac.tuwien.inso.sepm.ticketline.client.rest.BookingRestClient;
import at.ac.tuwien.inso.sepm.ticketline.client.service.BookingService;
import at.ac.tuwien.inso.sepm.ticketline.client.service.EventService;
import at.ac.tuwien.inso.sepm.ticketline.client.service.PaymentService;
import at.ac.tuwien.inso.sepm.ticketline.client.service.PaymentServiceFactory;
import at.ac.tuwien.inso.sepm.ticketline.client.validation.BookingValidator;
import at.ac.tuwien.inso.sepm.ticketline.rest.booking.BookingDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.customer.CustomerDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.payment.PaymentProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SimpleBookingService implements BookingService
{
	private static final Logger LOGGER = LoggerFactory.getLogger(SimpleBookingService.class);

	private final BookingRestClient restClient;
	private final EventService eventService;
	private final PaymentServiceFactory paymentServiceFactory;

	public SimpleBookingService(BookingRestClient restClient, EventService eventService,
	                            PaymentServiceFactory paymentServiceFactory)
	{
		this.restClient = restClient;
		this.eventService = eventService;
		this.paymentServiceFactory = paymentServiceFactory;
	}

	@Override
	public BookingDTO create(BookingDTO booking) throws ValidationException, DataAccessException
	{
		BookingValidator.validate(booking);

		LOGGER.debug("creating new booking...");
		BookingDTO result = restClient.create(booking);
		LOGGER.debug("... booking successfully created");
		return result;
	}

	@Override
	public BookingDTO update(BookingDTO booking) throws ValidationException, DataAccessException
	{
		BookingValidator.validate(booking);
		BookingValidator.validateId(booking.getId());

		LOGGER.debug("updating booking ...");

		BookingDTO result = restClient.update(booking);
		LOGGER.debug("... booking successfully updated");
		return result;
	}

	@Override
	public BookingDTO findByReservationNumber(int reservationNumber) throws DataAccessException, ValidationException
	{
		BookingValidator.validateReservationNumber(reservationNumber);

		LOGGER.debug("retrieving booking per res.number...");
		BookingDTO result = restClient.findByReservationNumber(reservationNumber);
		populateBookingEvent(result);
		LOGGER.debug("... retrieved booking per res.number: {}", result);
		return result;
	}

	@Override
	public List<BookingDTO> findAll(CustomerDTO customer) throws DataAccessException
	{
		LOGGER.debug("retrieving all bookings for customer (id: {})", customer.getId());
		List<BookingDTO> bookings = restClient.findAll(customer);
		LOGGER.debug("... retrieved all bookings for customer (id: {}): {}", customer, bookings);
		return bookings;
	}

	@Override
	public BookingDTO delete(BookingDTO bookingDTO) throws ValidationException, DataAccessException
	{
		if(bookingDTO.isCanceled())
		{
			throw new DataAccessException("booking already canceled");
		}

		LOGGER.debug("deleting booking: {}", bookingDTO);

		BookingDTO updatedBooking;

		LOGGER.debug("canceling booking ...");
		if(bookingDTO.isPaid())
		{
			LOGGER.debug("booking was paid, refunding..");
			PaymentService paymentService = paymentServiceFactory.getService(PaymentProvider.valueOf(bookingDTO.getPaymentMethod()));
			updatedBooking = paymentService.refund(bookingDTO);
		}
		else
		{
			LOGGER.debug("booking was reservation");
			bookingDTO.setCanceled(true);
			updatedBooking = update(bookingDTO);
		}

		LOGGER.debug("... booking successfully canceled");
		return updatedBooking;
	}

	public void populateBookingEvent(BookingDTO bookingDTO) throws DataAccessException
	{
		bookingDTO.setEvent(eventService.findOne(bookingDTO.getEvent().getId()));
	}
}
