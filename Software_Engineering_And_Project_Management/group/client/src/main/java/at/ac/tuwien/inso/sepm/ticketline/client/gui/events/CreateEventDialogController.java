package at.ac.tuwien.inso.sepm.ticketline.client.gui.events;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.client.exception.InvalidCSVException;
import at.ac.tuwien.inso.sepm.ticketline.client.exception.ValidationException;
import at.ac.tuwien.inso.sepm.ticketline.client.gui.MiscAlerts;
import at.ac.tuwien.inso.sepm.ticketline.client.service.EventService;
import at.ac.tuwien.inso.sepm.ticketline.client.service.LocationService;
import at.ac.tuwien.inso.sepm.ticketline.client.util.BundleManager;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventAreaDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.enums.Category;
import at.ac.tuwien.inso.sepm.ticketline.rest.location.AreaDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.location.LocationDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.location.RowDTO;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.StringConverter;
import org.controlsfx.glyphfont.FontAwesome;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;

@Component
public class CreateEventDialogController
{
	private static final Logger LOGGER = LoggerFactory.getLogger(CreateEventDialogController.class);

	public TextField ui_artistInput;
	public ComboBox<Category> ui_categoryDropdown;
	public TextField ui_eventTime;
	public TextField ui_descriptionInput;
	public TextField ui_durationInput;
	public ComboBox<LocationDTO> ui_locationDropdown;
	public TableColumn<EventAreaDTO, Long> ui_rowTableRow;
	public TableColumn<EventAreaDTO, Float> ui_rowTablePricePerSeat;
	public Button ui_addRangeButton;
	public Button ui_removeSelectedRangeButton;
	public Button ui_createEventButton;
	public Button ui_abortButton;
	public Button ui_importEventButton;
	public TextField ui_nameInput;
	public Label ui_categoryLabel;
	public DatePicker ui_datePicker;
	public TableView<EventAreaDTO> ui_rowPriceTable;
	public TextField ui_fromInput;
	public TextField ui_toInput;
	public TextField ui_pricePerSeatInput;
	private ObservableList<EventAreaDTO> rowDataList;
	private final FontAwesome fontAwesome;

	@Autowired
	private LocationService locationService;

	public void setEventService(EventService eventService)
	{
		this.eventService = eventService;
	}

	private EventService eventService;

	public CreateEventDialogController()
	{
		this.fontAwesome = new FontAwesome();
	}

	@FXML
	private void initialize()
	{
		LOGGER.debug("initialize");

		try
		{
			ui_categoryDropdown.setItems(FXCollections.observableArrayList(Category.CONCERT,
			                                                               Category.MUSICAL));
			ui_categoryDropdown.setConverter(new StringConverter<>()
			{
				@Override
				public String toString(Category category)
				{
					if(category == null)
					{
						return null;
					}
					else
					{
						return BundleManager.getBundle().getString(category.getValue());
					}
				}

				@Override
				public Category fromString(String locID)
				{
					return null;
				}
			});
			ui_categoryDropdown.getSelectionModel().selectFirst();
			ObservableList<LocationDTO> locationList = FXCollections.observableArrayList();
			locationList.addAll(locationService.findAll());
			ui_locationDropdown.setItems(locationList);

			ui_locationDropdown.setConverter(new StringConverter<>()
			{
				@Override
				public String toString(LocationDTO loc)
				{
					if(loc == null)
					{
						return null;
					}
					else
					{
						return loc.getName() + " (" + BundleManager.getBundle()
						                                           .getString(
							                                           "createEvent.capacity")
						       + ": " + loc.getAreas().size() + " " + (
							       loc.getAreas().toArray()[0] instanceof RowDTO
							       ? BundleManager.getBundle().getString("createEvent.rows")
							       : BundleManager.getBundle()
							                      .getString("createEvent.sections")) + ")";
					}
				}

				@Override
				public LocationDTO fromString(String locID)
				{
					return null;
				}
			});

			ui_locationDropdown.getSelectionModel().selectFirst();
		}
		catch(DataAccessException e)
		{
			MiscAlerts.showDataAccessAlert();
		}

		ui_addRangeButton.setGraphic(fontAwesome.create(FontAwesome.Glyph.PLUS));
		ui_removeSelectedRangeButton.setGraphic(fontAwesome.create(FontAwesome.Glyph.MINUS));

		ui_addRangeButton.disableProperty().bind(ui_fromInput.textProperty().isEmpty());
		ui_addRangeButton.disableProperty().bind(ui_toInput.textProperty().isEmpty());
		ui_addRangeButton.disableProperty().bind(ui_pricePerSeatInput.textProperty().isEmpty());

		ui_rowPriceTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

		rowDataList = FXCollections.observableArrayList();
		ui_rowPriceTable.setItems(rowDataList);

		ui_rowTableRow.setStyle("-fx-alignment: CENTER;");
		ui_rowTableRow.setCellValueFactory(cellData -> new SimpleObjectProperty<>(cellData.getValue()
		                                                                                  .getArea()
		                                                                                  .getOrderIndex()));

		ui_rowTablePricePerSeat.setStyle("-fx-alignment: CENTER;");
		ui_rowTablePricePerSeat.setCellValueFactory(new PropertyValueFactory<>("price"));
	}

	public void ui_onAddRangeButtonClick()
	{
		LocationDTO l = ui_locationDropdown.getSelectionModel().getSelectedItem();

		try
		{
			int fromRow = Integer.parseInt(ui_fromInput.getText());
			int toRow = Integer.parseInt(ui_toInput.getText());
			float price = Float.parseFloat(ui_pricePerSeatInput.getText());

			// without -1 that's off-by-1 and can still throw an exception
			for(int i = Math.max(fromRow, 1); i <= Math.min(toRow, l.getAreas().size()); i++)
			{
				EventAreaDTO newEventArea = new EventAreaDTO();
				newEventArea.setPrice(price);

				for(AreaDTO a : l.getAreas())
				{
					if(a.getOrderIndex() == i)
					{
						newEventArea.setArea(a);
						break;
					}
				}
				if(rowDataList.contains(newEventArea))
				{
					rowDataList.remove(rowDataList.indexOf(newEventArea));
				}
				rowDataList.add(newEventArea);
				List<EventAreaDTO> tmp = new ArrayList<>(rowDataList.sorted(Comparator.comparing(o -> o.getArea()
				                                                                                       .getOrderIndex())));
				rowDataList.clear();
				rowDataList.addAll(tmp);
			}
		}
		catch(NumberFormatException e)
		{
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle(BundleManager.getBundle().getString("createEvent.alert.header"));
			alert.setHeaderText(null);
			alert.setContentText(BundleManager.getBundle().getString("createEvent.alert.invalidNumber"));
			alert.showAndWait();
		}
	}

	public void ui_onRemoveSelectedRangeButtonClick(ActionEvent actionEvent)
	{
		ObservableList<EventAreaDTO> selectedItems = ui_rowPriceTable.getSelectionModel().getSelectedItems();
		LOGGER.debug(selectedItems + "well");
		if(!selectedItems.isEmpty())
		{
			rowDataList.removeAll(selectedItems);
		}
		else
		{
			Alert alert = new Alert(Alert.AlertType.INFORMATION);
			alert.setTitle(BundleManager.getBundle().getString("createEvent.alert.header"));
			alert.setHeaderText(null);
			alert.setContentText(BundleManager.getBundle().getString("createEvent.alert.selectArea"));
			alert.showAndWait();
		}
	}

	public void ui_onCreateEventButtonClick(ActionEvent actionEvent)
	{
		LOGGER.info("CreateEvent button pressed");

		EventDTO newEvent = new EventDTO();

		LocalDate localDate = ui_datePicker.getValue();
		if(localDate == null)
		{
			newEvent.setTime(null);
		}
		else
		{
			if(ui_eventTime.getText().contains(":"))
			{
				try
				{
					String[] stringTime = ui_eventTime.getText().trim().split(":");
					newEvent.setTime(localDate.atTime(Integer.parseInt(stringTime[0]),
					                                  Integer.parseInt(stringTime[1])));
				}
				catch(DateTimeException e)
				{
					newEvent.setTime(null);
				}
			}
			else
			{
				newEvent.setTime(null);
			}
		}
		try
		{
			newEvent.setDuration(Integer.parseInt(ui_durationInput.getText()));
		}
		catch(NumberFormatException e)
		{
			//ignore
		}

		newEvent.setName(ui_nameInput.getText());
		newEvent.setDescription(ui_descriptionInput.getText());
		newEvent.setArtist(ui_artistInput.getText());
		newEvent.setCategory(ui_categoryDropdown.getSelectionModel().getSelectedItem());

		newEvent.setLocation(ui_locationDropdown.getSelectionModel().getSelectedItem());
		newEvent.setEventAreas(new HashSet<>(ui_rowPriceTable.getItems()));

		try
		{
			eventService.create(newEvent);
			Node source = (Node)actionEvent.getSource();
			Stage stage = (Stage)source.getScene().getWindow();
			stage.close();
		}
		catch(DataAccessException e)
		{
			LOGGER.debug(e.getMessage());
			MiscAlerts.showDataAccessAlert();
		}
		catch(ValidationException e)
		{
			MiscAlerts.showValidationErrorAlert(e);
		}
	}

	public void ui_onLocationChanged(ActionEvent actionEvent)
	{
		rowDataList.clear();
	}

	public void ui_onAbortButtonClick(ActionEvent actionEvent)
	{
		LOGGER.info("Abort button pressed");

		Stage tmp = (Stage)ui_abortButton.getScene().getWindow();
		tmp.close();
	}

	public void ui_onImportEventButtonClick(ActionEvent actionEvent)
	{
		LOGGER.info("ImportEvent button pressed");

		FileChooser chooser = new FileChooser();
		chooser.setTitle(BundleManager.getBundle().getString("createEvents.fileChooserTitle"));
		chooser.getExtensionFilters()
		       .addAll(new FileChooser.ExtensionFilter(BundleManager.getBundle()
		                                                            .getString(
			                                                            "createEvents.fileChooserExtension"),
		                                               "*.csv"));

		File selected = chooser.showOpenDialog(ui_importEventButton.getScene().getWindow());
		if(selected != null)
		{
			try
			{
				StringBuilder alertContent = new StringBuilder();
				List<EventDTO> parsedEvents = eventService.parse(selected.getPath());
				for(EventDTO current : parsedEvents)
				{
					eventService.create(current);
					alertContent.append(current.getName()).append("\n");
				}
				Alert alert = new Alert(Alert.AlertType.INFORMATION);
				alert.setTitle(BundleManager.getBundle()
				                            .getString("createEvents.alert.infoAlertTitle"));
				alert.setHeaderText(BundleManager.getBundle()
				                                 .getString("createEvents.alert.infoAlertHeader"));
				alert.setContentText(alertContent.toString());
				alert.showAndWait();

				Stage tmp = (Stage)ui_importEventButton.getScene().getWindow();
				tmp.close();
			}
			catch(ValidationException | DataAccessException e)
			{
				LOGGER.error("Failed to create event");
				MiscAlerts.showDataAccessAlert();
			}
			catch(InvalidCSVException e)
			{
				LOGGER.debug("Failed to parse event " + e.getMessage());
				Alert alert = new Alert(Alert.AlertType.ERROR);
				alert.setTitle(BundleManager.getBundle()
				                            .getString("createEvents.alert.errorAlertTitle"));
				alert.setHeaderText(BundleManager.getBundle()
				                                 .getString("createEvents.alert.errorAlertHeader"));
				alert.setContentText(BundleManager.getBundle()
				                                  .getString("createEvents.alert.errorAlertContent"));
				alert.showAndWait();
			}
		}
	}
}

