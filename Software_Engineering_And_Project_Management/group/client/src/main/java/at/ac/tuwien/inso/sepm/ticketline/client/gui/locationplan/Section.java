package at.ac.tuwien.inso.sepm.ticketline.client.gui.locationplan;

import at.ac.tuwien.inso.sepm.ticketline.rest.location.SectionDTO;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.StrokeType;

import java.util.function.Consumer;

public class Section extends Rectangle
{
	private static final int WIDTH = 500;
	private static final int HEIGHT = 100;
	private static final Color STROKE_COLOR_UNSELECTED = Color.BLACK;
	private static final Color STROKE_COLOR_SELECTED = Color.ORANGE;
	private static final Color FILL_COLOR = Color.GREEN;
	private static final Color FILL_COLOR_FREE = new Color(0.2078, 0.6941, 0.2157, 1);
	private static final Color FILL_COLOR_BOOKED = new Color(0.8627, 0.2118, 0.2118, 1);

	public Section(SectionDTO dto, Consumer<SectionDTO> onClick)
	{
		setHeight(HEIGHT);
		setWidth(WIDTH);
		setFill(FILL_COLOR);
		setStrokeWidth(3);
		setStrokeType(StrokeType.INSIDE);
		setStroke(STROKE_COLOR_UNSELECTED);
		setOnMouseClicked(e -> onClick.accept(dto));
	}

	public void setSelected(boolean selected)
	{
		if(selected)
			setOpacity(0.75);
		else
			setOpacity(1);
	}

	public void setFree()
	{
		setFill(FILL_COLOR_FREE);
	}

	public void setBooked()
	{
		setFill(FILL_COLOR_BOOKED);
	}

	public void setIncludedInBooking(boolean includedInBooking)
	{
		if(includedInBooking)
			setStroke(STROKE_COLOR_SELECTED);
		else
			setStroke(STROKE_COLOR_UNSELECTED);
	}
}
