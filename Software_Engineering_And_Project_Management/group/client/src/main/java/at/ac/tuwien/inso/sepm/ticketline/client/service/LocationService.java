package at.ac.tuwien.inso.sepm.ticketline.client.service;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.rest.location.LocationDTO;

import java.util.List;

public interface LocationService
{
	/**
	 * Find all locations
	 *
	 * @return list of locations
	 *
	 * @throws DataAccessException in case something went wrong
	 */
	List<LocationDTO> findAll() throws DataAccessException;
}
