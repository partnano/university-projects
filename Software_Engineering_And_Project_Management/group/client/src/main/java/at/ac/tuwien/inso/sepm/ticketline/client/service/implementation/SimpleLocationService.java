package at.ac.tuwien.inso.sepm.ticketline.client.service.implementation;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.client.rest.LocationRestClient;
import at.ac.tuwien.inso.sepm.ticketline.client.service.LocationService;
import at.ac.tuwien.inso.sepm.ticketline.rest.location.LocationDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SimpleLocationService implements LocationService
{
	private static final Logger LOGGER = LoggerFactory.getLogger(SimpleNewsService.class);

	private final LocationRestClient locationRestClient;

	public SimpleLocationService(LocationRestClient locationRestClient)
	{
		this.locationRestClient = locationRestClient;
	}

	@Override
	public List<LocationDTO> findAll() throws DataAccessException
	{
		LOGGER.trace("LocationService findAll()");
		return locationRestClient.findAll();
	}
}
