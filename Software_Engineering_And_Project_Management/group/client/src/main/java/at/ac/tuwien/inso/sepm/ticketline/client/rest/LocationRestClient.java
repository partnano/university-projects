package at.ac.tuwien.inso.sepm.ticketline.client.rest;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.rest.location.LocationDTO;

import java.util.List;

public interface LocationRestClient
{
	/**
	 * Find all location entries.
	 *
	 * @return List of locations.
	 *
	 * @throws DataAccessException in case something went wrong.
	 */
	List<LocationDTO> findAll() throws DataAccessException;
}
