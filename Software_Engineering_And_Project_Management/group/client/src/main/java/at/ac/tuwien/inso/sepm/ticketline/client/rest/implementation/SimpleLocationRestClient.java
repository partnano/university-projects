package at.ac.tuwien.inso.sepm.ticketline.client.rest.implementation;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.client.rest.LocationRestClient;
import at.ac.tuwien.inso.sepm.ticketline.rest.location.LocationDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientException;

import java.util.List;

@Component
public class SimpleLocationRestClient implements LocationRestClient
{
	private static final Logger LOGGER = LoggerFactory.getLogger(SimpleNewsRestClient.class);
	private static final String LOCATIONS_URL = "/locations";

	private final RestClient restClient;

	public SimpleLocationRestClient(RestClient restClient)
	{
		this.restClient = restClient;
	}

	@Override
	public List<LocationDTO> findAll() throws DataAccessException
	{
		try
		{
			LOGGER.debug("Retrieving all events from {}", restClient.getServiceURI(LOCATIONS_URL));

			ResponseEntity<List<LocationDTO>> events = restClient.exchange(restClient.getServiceURI(LOCATIONS_URL),
			                                                               HttpMethod.GET,
			                                                               null,
			                                                               new ParameterizedTypeReference<List<LocationDTO>>()
			                                                               {
			                                                               });

			LOGGER.debug("Result status was {} with content {}", events.getStatusCode(), events.getBody());
			return events.getBody();
		}
		catch(HttpStatusCodeException e)
		{
			throw new DataAccessException("Failed retrieve locations with status code " + e.getStatusCode());
		}
		catch(RestClientException e)
		{
			throw new DataAccessException(e.getMessage(), e);
		}
	}
}
