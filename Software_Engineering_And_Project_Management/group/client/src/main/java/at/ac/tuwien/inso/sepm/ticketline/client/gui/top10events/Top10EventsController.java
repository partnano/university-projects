package at.ac.tuwien.inso.sepm.ticketline.client.gui.top10events;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.client.exception.ValidationException;
import at.ac.tuwien.inso.sepm.ticketline.client.gui.MiscAlerts;
import at.ac.tuwien.inso.sepm.ticketline.client.gui.TabHeaderController;
import at.ac.tuwien.inso.sepm.ticketline.client.gui.bookingeditor.BookingEditor;
import at.ac.tuwien.inso.sepm.ticketline.client.service.EventService;
import at.ac.tuwien.inso.sepm.ticketline.client.util.BundleManager;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.enums.Category;
import at.ac.tuwien.inso.sepm.ticketline.rest.location.LocationDTO;
import at.ac.tuwien.inso.springfx.SpringFxmlLoader;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import org.controlsfx.glyphfont.FontAwesome;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Component;

import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.time.format.TextStyle;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Component
public class Top10EventsController
{
	private static final int EVENT_COUNT = 10;

	private static final int FIRST_YEAR = 2017;
	private static final int CURRENT_YEAR = Calendar.getInstance().get(Calendar.YEAR);

	private static final DateTimeFormatter DATE_FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

	private final XYChart.Series<Integer, String> topEventSeries = new XYChart.Series<>();

	@FXML
	private TabHeaderController tabHeaderController;

	@FXML
	private ComboBox<String> ui_categoryList;

	@FXML
	private ComboBox<String> ui_monthList;

	@FXML
	private ComboBox<String> ui_yearList;

	@FXML
	private BarChart<Integer, String> ui_chart;

	@FXML
	private TableView<Pair<EventDTO, Integer>> ui_eventTable;

	@FXML
	private TableColumn<Pair<EventDTO, Integer>, Integer> ui_eventTicketsColumn;

	@FXML
	private TableColumn<Pair<EventDTO, Integer>, String> ui_eventNameColumn;

	@FXML
	private TableColumn<Pair<EventDTO, Integer>, String> ui_eventLocationColumn;

	@FXML
	private TableColumn<Pair<EventDTO, Integer>, String> ui_eventTimeColumn;

	@Autowired
	private final EventService service;

	private final SpringFxmlLoader loader;

	public Top10EventsController(SpringFxmlLoader loader, EventService service)
	{
		this.loader = loader;
		this.service = service;
	}

	@FXML
	private void initialize()
	{
		tabHeaderController.setIcon(FontAwesome.Glyph.BAR_CHART);
		tabHeaderController.setTitle(BundleManager.getBundle().getString("top10events.title"));

		ui_eventTable.setRowFactory(param ->
		{
			TableRow<Pair<EventDTO, Integer>> row = new TableRow<>();

			row.setOnMouseClicked(e ->
			{
				if(row.getItem() != null)
					rowClicked(row.getItem().getFirst(), e);
			});

			return row;
		});

		ui_eventTicketsColumn.setCellValueFactory(data -> new SimpleObjectProperty<>(data.getValue()
		                                                                                 .getSecond()));

		ui_eventNameColumn.setCellValueFactory(data -> new SimpleObjectProperty<>(data.getValue()
		                                                                              .getFirst()
		                                                                              .getName()));

		ui_eventLocationColumn.setCellValueFactory(data ->
		{
			LocationDTO location = data.getValue().getFirst().getLocation();
			String text = String.format("%s/%s: %s",
			location.getCountry(),
			location.getCity(),
			location.getName());
			return new SimpleObjectProperty<>(text);
		});

		ui_eventTimeColumn.setCellValueFactory(data -> new SimpleObjectProperty<>(data.getValue()
		                                                                              .getFirst()
		                                                                              .getTime()
		                                                                              .format(DATE_FORMAT)));

		ui_categoryList.getItems().add(BundleManager.getBundle().getString("top10events.filter.anyCategory"));

		ui_categoryList.getItems()
		               .addAll(Arrays.stream(Category.values())
		                             .map(c -> BundleManager.getBundle().getString(c.getValue()))
		                             .collect(Collectors.toList()));

		ui_categoryList.getSelectionModel().select(0);
		ui_categoryList.getSelectionModel().selectedItemProperty().addListener(v -> updateEvents());

		ui_monthList.getItems().add(BundleManager.getBundle().getString("top10events.filter.anyMonth"));

		ui_monthList.getItems()
		            .addAll(Arrays.stream(Month.values())
		                          .map(m -> m.getDisplayName(TextStyle.FULL, Locale.getDefault()))
		                          .collect(Collectors.toList()));

		ui_monthList.getSelectionModel().select(0);
		ui_monthList.getSelectionModel().selectedItemProperty().addListener(v -> updateEvents());

		ui_yearList.getItems().add(BundleManager.getBundle().getString("top10events.filter.anyYear"));

		ui_yearList.getItems()
		           .addAll(IntStream.iterate(FIRST_YEAR, y -> y <= CURRENT_YEAR, y -> y + 1)
		                            .mapToObj(Integer::toString)
		                            .collect(Collectors.toList()));

		ui_yearList.getSelectionModel().select(0);
		ui_yearList.getSelectionModel().selectedItemProperty().addListener(v -> updateEvents());

		ui_chart.getXAxis().setLabel(BundleManager.getBundle().getString("top10events.chart.amount"));
		ui_chart.getYAxis().setLabel(BundleManager.getBundle().getString("top10events.chart.event"));
		ui_chart.getData().add(topEventSeries);
	}

	public void loadEvents()
	{
		updateEvents();
	}

	private void updateData(Map<EventDTO, Integer> events)
	{
		List<Pair<EventDTO, Integer>> items = events.entrySet()
		                                            .stream()
		                                            .map(e -> Pair.of(e.getKey(), e.getValue()))
		                                            .sorted((a, b) -> b.getSecond() - a.getSecond())
		                                            .collect(Collectors.toList());

		ui_eventTable.setItems(FXCollections.observableList(items));

		topEventSeries.getData().clear();

		for(Pair<EventDTO, Integer> item : items)
		{
			topEventSeries.getData().add(0, new XYChart.Data<>(item.getSecond(), item.getFirst().getName()));
		}
	}

	private void updateEvents()
	{
		try
		{
			int categoryIndex = ui_categoryList.getSelectionModel().getSelectedIndex();
			Category category = categoryIndex == 0 ? null : Category.values()[categoryIndex - 1];

			int monthIndex = ui_monthList.getSelectionModel().getSelectedIndex();
			Integer month = monthIndex == 0 ? null : monthIndex;

			int yearIndex = ui_yearList.getSelectionModel().getSelectedIndex();
			Integer year = yearIndex == 0 ? null : FIRST_YEAR + yearIndex - 1;

			updateData(service.findTopEvents(category, month, year, EVENT_COUNT));
		}
		catch(DataAccessException ex)
		{
			MiscAlerts.showDataAccessAlert();
		}
		catch(ValidationException ex)
		{
			MiscAlerts.showValidationErrorAlert(ex);
		}
	}

	private void rowClicked(EventDTO dto, MouseEvent event)
	{
		if(dto == null)
			return;

		if(event.getButton() == MouseButton.PRIMARY && event.getClickCount() == 2)
		{
			Stage parent = (Stage)ui_eventTable.getScene().getWindow();
			BookingEditor.showAndWait(parent, loader, dto);
		}
	}
}
