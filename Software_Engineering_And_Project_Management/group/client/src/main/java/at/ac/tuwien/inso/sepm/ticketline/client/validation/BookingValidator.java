package at.ac.tuwien.inso.sepm.ticketline.client.validation;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.ValidationException;
import at.ac.tuwien.inso.sepm.ticketline.rest.booking.BookingDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BookingValidator
{
	private static final Logger LOGGER = LoggerFactory.getLogger(BookingValidator.class);

	public static void validate(BookingDTO booking) throws ValidationException
	{
		ValidationException ex = new ValidationException();

		if(booking == null)
			ex.addMessage("booking.empty");
		else
		{
			//when canceling event may be null
			if(booking.getCreatedAt() == null && booking.getEvent() == null)
				ex.addMessage("booking.eventEmpty");
		}

		if(ex.hasMessage())
		{
			LOGGER.info("Booking validation failed: {}", ex);
			throw ex;
		}
	}

	public static void validateId(Long id) throws ValidationException
	{
		ValidationException ex = new ValidationException();

		if(id == null)
			ex.addMessage("booking.idEmpty");

		else if(id < 1)
			ex.addMessage("booking.idNegative");

		if(ex.hasMessage())
		{
			LOGGER.info("Booking id validation failed: {}", ex);
			throw ex;
		}
	}

	public static void validateReservationNumber(int reservationNumber) throws ValidationException
	{
		ValidationException ex = new ValidationException();

		if(reservationNumber < 0)
			ex.addMessage("events.findReservation.window.errorMessage");

		if(ex.hasMessage())
		{
			LOGGER.info("Booking reservationNumber validation failed: {}", ex);
			throw ex;
		}
	}
}
