package at.ac.tuwien.inso.sepm.ticketline.client.exception;

public class InvalidCSVException extends Exception
{
	public InvalidCSVException()
	{
	}

	public InvalidCSVException(String message)
	{
		super(message);
	}

	public InvalidCSVException(String message, Throwable cause)
	{
		super(message, cause);
	}
}
