package at.ac.tuwien.inso.sepm.ticketline.client.service;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.client.exception.ValidationException;
import at.ac.tuwien.inso.sepm.ticketline.rest.booking.BookingDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.payment.CreditCardDTO;

public interface PaymentService
{
	/**
	 * Withdraw a given amount from the given credit card for a booking.
	 *
	 * @param bookingDTO    to be paid
	 * @param creditCardDTO to pay with
	 * @throws DataAccessException in case something goes wrong
	 */
	BookingDTO pay(BookingDTO bookingDTO, CreditCardDTO creditCardDTO) throws DataAccessException,
	                                                                          ValidationException;

	/**
	 * Refunds the given booking
	 *
	 * @param bookingDTO to be refunded
	 * @throws DataAccessException in case something goes wrong
	 */
	BookingDTO refund(BookingDTO bookingDTO) throws DataAccessException;

	/**
	 * Returns whether the specific payment method requires an credit card form or not.
	 *
	 * @return whether the specific payment method requires an credit card form or not
	 */
	boolean formNeeded();
}
