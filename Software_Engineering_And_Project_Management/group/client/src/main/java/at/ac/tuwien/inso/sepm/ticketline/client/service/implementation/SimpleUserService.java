package at.ac.tuwien.inso.sepm.ticketline.client.service.implementation;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.client.exception.ValidationException;
import at.ac.tuwien.inso.sepm.ticketline.client.rest.UserRestClient;
import at.ac.tuwien.inso.sepm.ticketline.client.service.UserService;
import at.ac.tuwien.inso.sepm.ticketline.rest.user.UserDTO;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

@Service
public class SimpleUserService implements UserService
{
	private final UserRestClient client;

	public SimpleUserService(UserRestClient client)
	{
		this.client = client;
	}

	@Override
	public Page<UserDTO> findAll(int page, int size) throws DataAccessException
	{
		return client.findAll(page, size);
	}

	private static void validateUserDTO(UserDTO dto) throws ValidationException
	{
		ValidationException ex = new ValidationException();

		if(dto.getFirstName() == null || dto.getFirstName().isEmpty())
			ex.addMessage("users.firstNameEmpty");

		if(dto.getLastName() == null || dto.getLastName().isEmpty())
			ex.addMessage("users.lastNameEmpty");

		if(ex.hasMessage())
			throw ex;
	}

	@Override
	public UserDTO create(UserDTO userDTO) throws DataAccessException, ValidationException
	{
		validateUserDTO(userDTO);
		return client.create(userDTO);
	}

	@Override
	public UserDTO update(UserDTO userDTO) throws DataAccessException, ValidationException
	{
		validateUserDTO(userDTO);
		return client.update(userDTO);
	}

	@Override
	public void resetPassword(long id) throws DataAccessException, ValidationException
	{
		if(id < 0)
			throw new ValidationException();

		client.resetPassword(id);
	}
}
