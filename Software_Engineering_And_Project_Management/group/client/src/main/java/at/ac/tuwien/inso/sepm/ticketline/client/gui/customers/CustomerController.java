package at.ac.tuwien.inso.sepm.ticketline.client.gui.customers;

import at.ac.tuwien.inso.sepm.ticketline.client.exception.DataAccessException;
import at.ac.tuwien.inso.sepm.ticketline.client.gui.MiscAlerts;
import at.ac.tuwien.inso.sepm.ticketline.client.gui.TabHeaderController;
import at.ac.tuwien.inso.sepm.ticketline.client.gui.bookingeditor.BookingEditor;
import at.ac.tuwien.inso.sepm.ticketline.client.service.BookingService;
import at.ac.tuwien.inso.sepm.ticketline.client.service.CustomerService;
import at.ac.tuwien.inso.sepm.ticketline.client.util.BundleManager;
import at.ac.tuwien.inso.sepm.ticketline.rest.booking.BookingDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.customer.CustomerDTO;
import at.ac.tuwien.inso.springfx.SpringFxmlLoader;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.controlsfx.glyphfont.FontAwesome;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import java.time.format.DateTimeFormatter;
import java.util.List;

@Component
public class CustomerController
{
	private final SpringFxmlLoader springFxmlLoader;
	private final CustomerService customerService;
	private final BookingService bookingService;
	public TableView<CustomerDTO> ui_customerTable;
	public TableColumn<CustomerDTO, String> ui_nameColumn;
	public TableColumn<CustomerDTO, Long> ui_customerNumberColumn;
	public Label ui_nameLabel;
	public Label ui_emailLabel;
	public TableView<BookingDTO> ui_customerBookingsTable;
	public TableColumn<BookingDTO, String> ui_bookingEventNameColumn;
	public TableColumn<BookingDTO, String> ui_bookingEventDateColumn;
	public TableColumn<BookingDTO, String> ui_bookingEventStatusColumn;
	public TableColumn<BookingDTO, String> ui_bookingTotalPrice;
	public TableColumn<BookingDTO, Integer> ui_bookingReservationNumber;
	public Button ui_addCustomerButton;
	public VBox ui_customerDetails;
	public Button ui_editCustomerButton;

	// pagination
	public HBox ui_paginationControlBox;
	public Button ui_firstButton;
	public Button ui_previousButton;
	public Label ui_paginationLabel;
	public Button ui_nextButton;
	public Button ui_lastButton;
	private int currentPage = 1;
	private final FontAwesome fontAwesome;
	private static final int ELEMENTS_PER_PAGE = 20;

	private ObservableList<CustomerDTO> customerTableData;
	private ObservableList<BookingDTO> customerBookingsTableData;
	private CustomerDTO selectedCustomer;

	@FXML
	private TabHeaderController tabHeaderController;

	private static final Logger LOGGER = LoggerFactory.getLogger(CustomerController.class);

	public CustomerController(SpringFxmlLoader springFxmlLoader, CustomerService customerService,
	                          BookingService bookingService)
	{
		this.springFxmlLoader = springFxmlLoader;
		this.customerService = customerService;
		this.bookingService = bookingService;
		fontAwesome = new FontAwesome();
	}

	@FXML
	private void initialize()
	{
		ui_customerDetails.setVisible(false);
		tabHeaderController.setIcon(FontAwesome.Glyph.USERS);
		tabHeaderController.setTitle(BundleManager.getBundle().getString("customers.customers"));
		customerTableData = FXCollections.observableArrayList();
		customerBookingsTableData = FXCollections.observableArrayList();

		ui_editCustomerButton.disableProperty()
		                     .bind(ui_customerTable.getSelectionModel().selectedItemProperty().isNull());

		ui_customerTable.setItems(customerTableData);
		ui_customerBookingsTable.setItems(customerBookingsTableData);

		ui_bookingEventDateColumn.setCellValueFactory(cellData -> {
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
			return new SimpleObjectProperty<>(cellData.getValue().getEvent().getTime().format(formatter));
		});

		ui_bookingReservationNumber.setCellValueFactory(new PropertyValueFactory<>("reservationNumber"));

		ui_bookingEventStatusColumn.setCellValueFactory(cellData -> new SimpleObjectProperty<>(getBookingStatusText(
				cellData.getValue())));

		ui_nameColumn.setCellValueFactory(
			cellData -> {
				if(cellData.getValue().getLastName() == null || cellData.getValue().getFirstName() == null)
				{
					return new SimpleObjectProperty<>(BundleManager.getBundle().getString("customer.anonymous"));
				}
				else
				{
					return new SimpleObjectProperty<>(cellData.getValue().getLastName() + " " + cellData.getValue().getFirstName());
				}
			}
		);

		ui_customerNumberColumn.setCellValueFactory(new PropertyValueFactory<>("customerNumber"));

		ui_bookingTotalPrice.setCellValueFactory(cellData -> new SimpleObjectProperty<>(String.format("%.2f %s",
		                                                                                              cellData.getValue()
		                                                                                                      .getTotalPrice(),
		                                                                                BundleManager.getBundle()
		                                                                                             .getString(
				                                                                                             "currency"))));

		ui_bookingEventNameColumn.setCellValueFactory(cellData -> new SimpleObjectProperty<>(cellData.getValue()
		                                                                                             .getEvent()
		                                                                                             .getName()));

		ui_customerTable.setRowFactory(param -> new TableRow<>());

		ui_customerTable.getSelectionModel()
		                .getSelectedItems()
		                .addListener((ListChangeListener<? super CustomerDTO>)change -> selectionChanged());

		ui_customerBookingsTable.setRowFactory(tv ->
		{
			TableRow<BookingDTO> row = new TableRow<>();

			row.setOnMouseClicked(event ->
			{
				if(!row.isEmpty())
					customerBookingClicked(row.getItem(), event);
			});

			return row;
		});
	}

	private void customerBookingClicked(BookingDTO booking, MouseEvent event)
	{
		LOGGER.info("click on customer's booking detected");

		if(event.getButton() == MouseButton.PRIMARY && event.getClickCount() == 2)
		{
			Stage parent = (Stage)ui_customerBookingsTable.getScene().getWindow();
			BookingEditor.showAndWait(parent, springFxmlLoader, booking);
			showCustomer(ui_customerTable.getSelectionModel().getSelectedItem());
		}
	}

	private String getBookingStatusText(BookingDTO booking)
	{
		String localizationString;

		switch(booking.getBookingStatus())
		{
		case BOOKED:
			localizationString = "bookingStatus.booked";
			break;
		case RESERVED:
			localizationString = "bookingStatus.reserved";
			break;
		case CANCELED:
			localizationString = "bookingStatus.canceled";
			break;
		default:
			localizationString = "bookingStatus.unknown";
		}

		return BundleManager.getBundle().getString(localizationString);
	}

	private void showCustomer(CustomerDTO customer)
	{
		if(customer == null)
		{
			return;
		}

		ui_nameLabel.setText(customer.getFirstName() + " " + customer.getLastName());
		ui_emailLabel.setText(customer.getEmail());
		ui_customerDetails.setVisible(true);

		customerBookingsTableData.clear();

		try
		{
			List<BookingDTO> customerBookings = bookingService.findAll(customer);
			LOGGER.info("received customer bookings: ", customerBookings);

			customerBookingsTableData.addAll(customerBookings);
		}
		catch(DataAccessException e)
		{
			e.printStackTrace();
		}
	}

	private void selectionChanged()
	{
		selectedCustomer = ui_customerTable.getSelectionModel().getSelectedItem();
		LOGGER.debug("selectedcust: {}", selectedCustomer);
		showCustomer(selectedCustomer);
	}

	public void loadCustomers()
	{
		try
		{
			Page<CustomerDTO> customerDTOPage = customerService.findAll(currentPage - 1, ELEMENTS_PER_PAGE);
			int numPages = customerDTOPage.getTotalPages();

			ui_paginationLabel.setText(currentPage + "/" + numPages);

			ui_firstButton.setText(BundleManager.getBundle().getString("pagination.first"));
			ui_firstButton.setOnAction(event -> {
				currentPage = 1;
				loadCustomers();
			});

			ui_previousButton.setGraphic(fontAwesome.create(FontAwesome.Glyph.CHEVRON_LEFT));
			ui_previousButton.setOnAction(event -> {
				if(currentPage > 1)
				{
					currentPage--;
				}
				loadCustomers();
			});

			ui_lastButton.setText(BundleManager.getBundle().getString("pagination.last"));
			ui_nextButton.setGraphic(fontAwesome.create(FontAwesome.Glyph.CHEVRON_RIGHT));

			ui_lastButton.setOnAction(event -> {
				currentPage = numPages;
				loadCustomers();
			});
			ui_nextButton.setOnAction(event -> {
				if(currentPage < numPages)
				{
					currentPage++;
				}
				loadCustomers();
			});
			LOGGER.debug("Pages: " + currentPage + " " + numPages);

			if(currentPage >= numPages)
			{
				ui_nextButton.setDisable(true);
				ui_lastButton.setDisable(true);
			}
			else
			{
				ui_nextButton.setDisable(false);
				ui_lastButton.setDisable(false);
			}

			if(currentPage <= 1)
			{
				ui_previousButton.setDisable(true);
				ui_firstButton.setDisable(true);
			}
			else
			{
				ui_previousButton.setDisable(false);
				ui_firstButton.setDisable(false);
			}

			customerTableData.clear();
			customerTableData.addAll(customerDTOPage.getContent());
		}
		catch(DataAccessException e)
		{
			MiscAlerts.showDataAccessAlert();
		}

		LOGGER.debug("refreshed customer table data");
	}

	public void ui_addCustomerButtonClick()
	{
		Stage stage = (Stage)ui_customerTable.getScene().getWindow();

		AddCustomerController.create(stage, springFxmlLoader, customerService);
		loadCustomers();
	}

	public void ui_editCustomerButtonClick()
	{
		Stage stage = (Stage)ui_customerTable.getScene().getWindow();
		Stage dialog = new Stage();
		dialog.setResizable(false);
		dialog.initModality(Modality.APPLICATION_MODAL);
		dialog.initOwner(stage);

		dialog.addEventHandler(KeyEvent.KEY_RELEASED, (KeyEvent keyEvent) -> {
			if(KeyCode.ESCAPE == keyEvent.getCode())
			{
				dialog.close();
			}
		});

		SpringFxmlLoader.Wrapper<EditCustomerController> wrapper = springFxmlLoader.loadAndWrap(
				"/fxml/customers/editCustomerWindow.fxml");
		EditCustomerController editCustomerController = wrapper.getController();
		editCustomerController.setCustomerService(customerService);
		editCustomerController.setCurrentCustomer(selectedCustomer);
		editCustomerController.loadContent();

		dialog.setScene(new Scene((Parent)wrapper.getLoadedObject()));
		dialog.setTitle(BundleManager.getBundle().getString("customers.editCustomer"));
		dialog.showAndWait();

		loadCustomers();
	}

	public void ui_onKeyPressedCustomerTable(KeyEvent keyEvent)
	{
		if(KeyCode.ENTER == keyEvent.getCode())
		{
			if(ui_customerTable.getSelectionModel().getSelectedItem() != null)
			{
				selectedCustomer = ui_customerTable.getSelectionModel().getSelectedItem();
				showCustomer(selectedCustomer);
				ui_editCustomerButtonClick();
			}
		}
	}
}
