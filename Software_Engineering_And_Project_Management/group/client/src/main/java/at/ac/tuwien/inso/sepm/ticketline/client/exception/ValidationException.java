package at.ac.tuwien.inso.sepm.ticketline.client.exception;

import java.util.ArrayList;
import java.util.List;

public class ValidationException extends Exception
{
	private final List<String> messages;

	public ValidationException()
	{
		messages = new ArrayList<>();
	}

	public void addMessage(String message)
	{
		messages.add(message);
	}

	public boolean hasMessage()
	{
		return !messages.isEmpty();
	}

	public List<String> getMessages()
	{
		return messages;
	}
}
