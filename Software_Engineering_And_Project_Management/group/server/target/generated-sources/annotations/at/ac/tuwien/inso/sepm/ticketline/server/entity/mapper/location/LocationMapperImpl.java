package at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.location;

import at.ac.tuwien.inso.sepm.ticketline.rest.location.AreaDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.location.LocationDTO;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Area;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Location;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.annotation.Generated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2018-01-21T20:46:50+0100",
    comments = "version: 1.2.0.Final, compiler: javac, environment: Java 9.0.4 (Oracle Corporation)"
)
@Component
public class LocationMapperImpl implements LocationMapper {

    @Autowired
    private GenericAreaMapper genericAreaMapper;

    @Override
    public Location locationDTOtoLocation(LocationDTO locationDTO) {
        if ( locationDTO == null ) {
            return null;
        }

        Location location = new Location();

        location.setId( locationDTO.getId() );
        location.setName( locationDTO.getName() );
        location.setStreet( locationDTO.getStreet() );
        location.setZip( locationDTO.getZip() );
        location.setAreas( areaDTOSetToAreaSet( locationDTO.getAreas() ) );
        location.setCountry( locationDTO.getCountry() );
        location.setCity( locationDTO.getCity() );

        return location;
    }

    @Override
    public LocationDTO locationToLocationDTO(Location location) {
        if ( location == null ) {
            return null;
        }

        LocationDTO locationDTO = new LocationDTO();

        locationDTO.setId( location.getId() );
        locationDTO.setName( location.getName() );
        locationDTO.setStreet( location.getStreet() );
        locationDTO.setZip( location.getZip() );
        locationDTO.setAreas( areaSetToAreaDTOSet( location.getAreas() ) );
        locationDTO.setCity( location.getCity() );
        locationDTO.setCountry( location.getCountry() );

        return locationDTO;
    }

    @Override
    public List<LocationDTO> locationToLocationDTO(List<Location> locations) {
        if ( locations == null ) {
            return null;
        }

        List<LocationDTO> list = new ArrayList<LocationDTO>( locations.size() );
        for ( Location location : locations ) {
            list.add( locationToLocationDTO( location ) );
        }

        return list;
    }

    protected Set<Area> areaDTOSetToAreaSet(Set<AreaDTO> set) {
        if ( set == null ) {
            return null;
        }

        Set<Area> set1 = new HashSet<Area>( Math.max( (int) ( set.size() / .75f ) + 1, 16 ) );
        for ( AreaDTO areaDTO : set ) {
            set1.add( genericAreaMapper.areaDTOToArea( areaDTO ) );
        }

        return set1;
    }

    protected Set<AreaDTO> areaSetToAreaDTOSet(Set<Area> set) {
        if ( set == null ) {
            return null;
        }

        Set<AreaDTO> set1 = new HashSet<AreaDTO>( Math.max( (int) ( set.size() / .75f ) + 1, 16 ) );
        for ( Area area : set ) {
            set1.add( genericAreaMapper.areaToAreaDTO( area ) );
        }

        return set1;
    }
}
