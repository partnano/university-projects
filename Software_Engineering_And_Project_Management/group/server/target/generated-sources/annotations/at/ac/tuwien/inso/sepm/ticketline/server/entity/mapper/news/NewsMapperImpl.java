package at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.news;

import at.ac.tuwien.inso.sepm.ticketline.rest.news.DetailedNewsDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.news.SimpleNewsDTO;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.News;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2018-01-21T20:46:46+0100",
    comments = "version: 1.2.0.Final, compiler: javac, environment: Java 9.0.4 (Oracle Corporation)"
)
@Component
public class NewsMapperImpl implements NewsMapper {

    @Autowired
    private NewsSummaryMapper newsSummaryMapper;

    @Override
    public News detailedNewsDTOToNews(DetailedNewsDTO detailedNewsDTO) {
        if ( detailedNewsDTO == null ) {
            return null;
        }

        News news = new News();

        news.setId( detailedNewsDTO.getId() );
        news.setPublishedAt( detailedNewsDTO.getPublishedAt() );
        news.setTitle( detailedNewsDTO.getTitle() );
        news.setText( detailedNewsDTO.getText() );
        news.setRead( detailedNewsDTO.isRead() );
        byte[] byteImage = detailedNewsDTO.getByteImage();
        if ( byteImage != null ) {
            news.setByteImage( Arrays.copyOf( byteImage, byteImage.length ) );
        }

        return news;
    }

    @Override
    public DetailedNewsDTO newsToDetailedNewsDTO(News one) {
        if ( one == null ) {
            return null;
        }

        DetailedNewsDTO detailedNewsDTO = new DetailedNewsDTO();

        detailedNewsDTO.setId( one.getId() );
        detailedNewsDTO.setPublishedAt( one.getPublishedAt() );
        detailedNewsDTO.setTitle( one.getTitle() );
        detailedNewsDTO.setText( one.getText() );
        detailedNewsDTO.setRead( one.isRead() );
        byte[] byteImage = one.getByteImage();
        if ( byteImage != null ) {
            detailedNewsDTO.setByteImage( Arrays.copyOf( byteImage, byteImage.length ) );
        }

        return detailedNewsDTO;
    }

    @Override
    public List<SimpleNewsDTO> newsToSimpleNewsDTO(List<News> all) {
        if ( all == null ) {
            return null;
        }

        List<SimpleNewsDTO> list = new ArrayList<SimpleNewsDTO>( all.size() );
        for ( News news : all ) {
            list.add( newsToSimpleNewsDTO( news ) );
        }

        return list;
    }

    @Override
    public SimpleNewsDTO newsToSimpleNewsDTO(News one) {
        if ( one == null ) {
            return null;
        }

        SimpleNewsDTO simpleNewsDTO = new SimpleNewsDTO();

        simpleNewsDTO.setSummary( newsSummaryMapper.trimTextToSummary( one.getText() ) );
        simpleNewsDTO.setId( one.getId() );
        simpleNewsDTO.setPublishedAt( one.getPublishedAt() );
        simpleNewsDTO.setTitle( one.getTitle() );
        simpleNewsDTO.setRead( one.isRead() );
        byte[] byteImage = one.getByteImage();
        if ( byteImage != null ) {
            simpleNewsDTO.setByteImage( Arrays.copyOf( byteImage, byteImage.length ) );
        }

        return simpleNewsDTO;
    }
}
