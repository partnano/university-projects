package at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.booking;

import at.ac.tuwien.inso.sepm.ticketline.rest.booking.BookingDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.booking.ReceiptDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.booking.SectionBookingDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.booking.SectionBookingEntryDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.customer.CustomerDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventAreaDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.location.AreaDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.location.LocationDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.location.SectionDTO;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Area;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Booking;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Customer;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Event;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.EventArea;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Location;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Receipt;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Section;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.SectionBooking;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.SectionBookingEntry;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.location.GenericAreaMapper;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.annotation.Generated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2018-01-21T20:46:50+0100",
    comments = "version: 1.2.0.Final, compiler: javac, environment: Java 9.0.4 (Oracle Corporation)"
)
@Component
public class SectionBookingMapperImpl extends SectionBookingMapper {

    @Autowired
    private GenericAreaMapper genericAreaMapper;
    @Autowired
    private GenericBookingMapper genericBookingMapper;

    @Override
    public SectionBookingDTO sectionBookingToSectionBookingDTO(SectionBooking sectionBooking) {
        if ( sectionBooking == null ) {
            return null;
        }

        SectionBookingDTO sectionBookingDTO = new SectionBookingDTO();

        sectionBookingDTO.setSections( sectionBookingEntrySetToSectionBookingEntryDTOSet( sectionBooking.getEntries() ) );
        sectionBookingDTO.setId( sectionBooking.getId() );
        sectionBookingDTO.setReservationNumber( sectionBooking.getReservationNumber() );
        sectionBookingDTO.setPaid( sectionBooking.isPaid() );
        sectionBookingDTO.setCanceled( sectionBooking.isCanceled() );
        sectionBookingDTO.setCreatedAt( sectionBooking.getCreatedAt() );
        sectionBookingDTO.setBookedAt( sectionBooking.getBookedAt() );
        sectionBookingDTO.setPaymentMethod( sectionBooking.getPaymentMethod() );
        sectionBookingDTO.setEvent( eventToEventDTO( sectionBooking.getEvent() ) );
        sectionBookingDTO.setCustomer( customerToCustomerDTO( sectionBooking.getCustomer() ) );
        sectionBookingDTO.setPaymentReceipt( receiptToReceiptDTO( sectionBooking.getPaymentReceipt() ) );
        sectionBookingDTO.setCancellationReceipt( receiptToReceiptDTO( sectionBooking.getCancellationReceipt() ) );
        sectionBookingDTO.setTotalPrice( sectionBooking.getTotalPrice() );

        return sectionBookingDTO;
    }

    @Override
    public List<SectionBookingDTO> sectionBookingsToSectionBookingDTOs(List<SectionBooking> sectionBookings) {
        if ( sectionBookings == null ) {
            return null;
        }

        List<SectionBookingDTO> list = new ArrayList<SectionBookingDTO>( sectionBookings.size() );
        for ( SectionBooking sectionBooking : sectionBookings ) {
            list.add( sectionBookingToSectionBookingDTO( sectionBooking ) );
        }

        return list;
    }

    @Override
    public SectionBooking sectionBookingDTOToSectionBooking(SectionBookingDTO sectionBooking) {
        if ( sectionBooking == null ) {
            return null;
        }

        SectionBooking sectionBooking1 = new SectionBooking();

        sectionBooking1.setEntries( sectionBookingEntryDTOSetToSectionBookingEntrySet( sectionBooking.getSections() ) );
        sectionBooking1.setId( sectionBooking.getId() );
        sectionBooking1.setReservationNumber( sectionBooking.getReservationNumber() );
        sectionBooking1.setPaid( sectionBooking.isPaid() );
        sectionBooking1.setCanceled( sectionBooking.isCanceled() );
        sectionBooking1.setCreatedAt( sectionBooking.getCreatedAt() );
        sectionBooking1.setBookedAt( sectionBooking.getBookedAt() );
        sectionBooking1.setPaymentMethod( sectionBooking.getPaymentMethod() );
        sectionBooking1.setEvent( eventDTOToEvent( sectionBooking.getEvent() ) );
        sectionBooking1.setCustomer( customerDTOToCustomer( sectionBooking.getCustomer() ) );
        sectionBooking1.setPaymentReceipt( receiptDTOToReceipt( sectionBooking.getPaymentReceipt() ) );
        sectionBooking1.setCancellationReceipt( receiptDTOToReceipt( sectionBooking.getCancellationReceipt() ) );
        sectionBooking1.setTotalPrice( sectionBooking.getTotalPrice() );

        addBookingReferenceToSectionBookingEntry( sectionBooking, sectionBooking1 );

        return sectionBooking1;
    }

    @Override
    public List<SectionBooking> sectionBookingDTOsToSectionBookings(List<SectionBookingDTO> sectionBookings) {
        if ( sectionBookings == null ) {
            return null;
        }

        List<SectionBooking> list = new ArrayList<SectionBooking>( sectionBookings.size() );
        for ( SectionBookingDTO sectionBookingDTO : sectionBookings ) {
            list.add( sectionBookingDTOToSectionBooking( sectionBookingDTO ) );
        }

        return list;
    }

    @Override
    public EventDTO eventToEventDTO(Event event) {
        if ( event == null ) {
            return null;
        }

        EventDTO eventDTO = new EventDTO();

        eventDTO.setId( event.getId() );
        eventDTO.setTime( event.getTime() );
        eventDTO.setArtist( event.getArtist() );
        eventDTO.setCategory( event.getCategory() );
        eventDTO.setName( event.getName() );
        eventDTO.setDescription( event.getDescription() );
        eventDTO.setDuration( event.getDuration() );
        eventDTO.setLocation( locationToLocationDTO( event.getLocation() ) );
        eventDTO.setEventAreas( eventAreaSetToEventAreaDTOSet( event.getEventAreas() ) );

        return eventDTO;
    }

    @Override
    public EventAreaDTO eventAreaToEventAreaDTO(EventArea eventArea) {
        if ( eventArea == null ) {
            return null;
        }

        EventAreaDTO eventAreaDTO = new EventAreaDTO();

        eventAreaDTO.setId( eventArea.getId() );
        eventAreaDTO.setArea( genericAreaMapper.areaToAreaDTO( eventArea.getArea() ) );
        eventAreaDTO.setPrice( eventArea.getPrice() );

        return eventAreaDTO;
    }

    protected Set<AreaDTO> areaSetToAreaDTOSet(Set<Area> set) {
        if ( set == null ) {
            return null;
        }

        Set<AreaDTO> set1 = new HashSet<AreaDTO>( Math.max( (int) ( set.size() / .75f ) + 1, 16 ) );
        for ( Area area : set ) {
            set1.add( genericAreaMapper.areaToAreaDTO( area ) );
        }

        return set1;
    }

    protected LocationDTO locationToLocationDTO(Location location) {
        if ( location == null ) {
            return null;
        }

        LocationDTO locationDTO = new LocationDTO();

        locationDTO.setId( location.getId() );
        locationDTO.setName( location.getName() );
        locationDTO.setStreet( location.getStreet() );
        locationDTO.setZip( location.getZip() );
        locationDTO.setAreas( areaSetToAreaDTOSet( location.getAreas() ) );
        locationDTO.setCity( location.getCity() );
        locationDTO.setCountry( location.getCountry() );

        return locationDTO;
    }

    protected SectionDTO sectionToSectionDTO(Section section) {
        if ( section == null ) {
            return null;
        }

        SectionDTO sectionDTO = new SectionDTO();

        sectionDTO.setId( section.getId() );
        sectionDTO.setLocation( locationToLocationDTO( section.getLocation() ) );
        sectionDTO.setOrderIndex( section.getOrderIndex() );
        sectionDTO.setName( section.getName() );
        sectionDTO.setCapacity( section.getCapacity() );

        return sectionDTO;
    }

    protected SectionBookingEntryDTO sectionBookingEntryToSectionBookingEntryDTO(SectionBookingEntry sectionBookingEntry) {
        if ( sectionBookingEntry == null ) {
            return null;
        }

        SectionBookingEntryDTO sectionBookingEntryDTO = new SectionBookingEntryDTO();

        sectionBookingEntryDTO.setId( sectionBookingEntry.getId() );
        sectionBookingEntryDTO.setSection( sectionToSectionDTO( sectionBookingEntry.getSection() ) );
        sectionBookingEntryDTO.setAmount( sectionBookingEntry.getAmount() );

        return sectionBookingEntryDTO;
    }

    protected Set<SectionBookingEntryDTO> sectionBookingEntrySetToSectionBookingEntryDTOSet(Set<SectionBookingEntry> set) {
        if ( set == null ) {
            return null;
        }

        Set<SectionBookingEntryDTO> set1 = new HashSet<SectionBookingEntryDTO>( Math.max( (int) ( set.size() / .75f ) + 1, 16 ) );
        for ( SectionBookingEntry sectionBookingEntry : set ) {
            set1.add( sectionBookingEntryToSectionBookingEntryDTO( sectionBookingEntry ) );
        }

        return set1;
    }

    protected CustomerDTO customerToCustomerDTO(Customer customer) {
        if ( customer == null ) {
            return null;
        }

        CustomerDTO customerDTO = new CustomerDTO();

        customerDTO.setId( customer.getId() );
        customerDTO.setFirstName( customer.getFirstName() );
        customerDTO.setLastName( customer.getLastName() );
        customerDTO.setEmail( customer.getEmail() );
        customerDTO.setCustomerNumber( customer.getCustomerNumber() );

        return customerDTO;
    }

    protected ReceiptDTO receiptToReceiptDTO(Receipt receipt) {
        if ( receipt == null ) {
            return null;
        }

        ReceiptDTO receiptDTO = new ReceiptDTO();

        receiptDTO.setId( receipt.getId() );
        receiptDTO.setCreatedAt( receipt.getCreatedAt() );

        return receiptDTO;
    }

    protected Set<Area> areaDTOSetToAreaSet(Set<AreaDTO> set) {
        if ( set == null ) {
            return null;
        }

        Set<Area> set1 = new HashSet<Area>( Math.max( (int) ( set.size() / .75f ) + 1, 16 ) );
        for ( AreaDTO areaDTO : set ) {
            set1.add( genericAreaMapper.areaDTOToArea( areaDTO ) );
        }

        return set1;
    }

    protected Location locationDTOToLocation(LocationDTO locationDTO) {
        if ( locationDTO == null ) {
            return null;
        }

        Location location = new Location();

        location.setId( locationDTO.getId() );
        location.setName( locationDTO.getName() );
        location.setStreet( locationDTO.getStreet() );
        location.setZip( locationDTO.getZip() );
        location.setAreas( areaDTOSetToAreaSet( locationDTO.getAreas() ) );
        location.setCountry( locationDTO.getCountry() );
        location.setCity( locationDTO.getCity() );

        return location;
    }

    protected Section sectionDTOToSection(SectionDTO sectionDTO) {
        if ( sectionDTO == null ) {
            return null;
        }

        Section section = new Section();

        section.setId( sectionDTO.getId() );
        section.setLocation( locationDTOToLocation( sectionDTO.getLocation() ) );
        section.setOrderIndex( sectionDTO.getOrderIndex() );
        section.setName( sectionDTO.getName() );
        section.setCapacity( sectionDTO.getCapacity() );

        return section;
    }

    protected SectionBookingEntry sectionBookingEntryDTOToSectionBookingEntry(SectionBookingEntryDTO sectionBookingEntryDTO) {
        if ( sectionBookingEntryDTO == null ) {
            return null;
        }

        SectionBookingEntry sectionBookingEntry = new SectionBookingEntry();

        sectionBookingEntry.setId( sectionBookingEntryDTO.getId() );
        sectionBookingEntry.setSection( sectionDTOToSection( sectionBookingEntryDTO.getSection() ) );
        sectionBookingEntry.setAmount( sectionBookingEntryDTO.getAmount() );

        return sectionBookingEntry;
    }

    protected Set<SectionBookingEntry> sectionBookingEntryDTOSetToSectionBookingEntrySet(Set<SectionBookingEntryDTO> set) {
        if ( set == null ) {
            return null;
        }

        Set<SectionBookingEntry> set1 = new HashSet<SectionBookingEntry>( Math.max( (int) ( set.size() / .75f ) + 1, 16 ) );
        for ( SectionBookingEntryDTO sectionBookingEntryDTO : set ) {
            set1.add( sectionBookingEntryDTOToSectionBookingEntry( sectionBookingEntryDTO ) );
        }

        return set1;
    }

    protected EventArea eventAreaDTOToEventArea(EventAreaDTO eventAreaDTO) {
        if ( eventAreaDTO == null ) {
            return null;
        }

        EventArea eventArea = new EventArea();

        eventArea.setId( eventAreaDTO.getId() );
        eventArea.setArea( genericAreaMapper.areaDTOToArea( eventAreaDTO.getArea() ) );
        eventArea.setEvent( eventDTOToEvent( eventAreaDTO.getEvent() ) );
        eventArea.setPrice( eventAreaDTO.getPrice() );

        return eventArea;
    }

    protected Set<EventArea> eventAreaDTOSetToEventAreaSet(Set<EventAreaDTO> set) {
        if ( set == null ) {
            return null;
        }

        Set<EventArea> set1 = new HashSet<EventArea>( Math.max( (int) ( set.size() / .75f ) + 1, 16 ) );
        for ( EventAreaDTO eventAreaDTO : set ) {
            set1.add( eventAreaDTOToEventArea( eventAreaDTO ) );
        }

        return set1;
    }

    protected Set<Booking> bookingDTOSetToBookingSet(Set<BookingDTO> set) {
        if ( set == null ) {
            return null;
        }

        Set<Booking> set1 = new HashSet<Booking>( Math.max( (int) ( set.size() / .75f ) + 1, 16 ) );
        for ( BookingDTO bookingDTO : set ) {
            set1.add( genericBookingMapper.bookingDTOToBooking( bookingDTO ) );
        }

        return set1;
    }

    protected Event eventDTOToEvent(EventDTO eventDTO) {
        if ( eventDTO == null ) {
            return null;
        }

        Event event = new Event();

        event.setId( eventDTO.getId() );
        event.setTime( eventDTO.getTime() );
        event.setName( eventDTO.getName() );
        event.setDescription( eventDTO.getDescription() );
        event.setArtist( eventDTO.getArtist() );
        event.setCategory( eventDTO.getCategory() );
        event.setDuration( eventDTO.getDuration() );
        event.setLocation( locationDTOToLocation( eventDTO.getLocation() ) );
        event.setEventAreas( eventAreaDTOSetToEventAreaSet( eventDTO.getEventAreas() ) );
        event.setBookings( bookingDTOSetToBookingSet( eventDTO.getBookings() ) );

        return event;
    }

    protected Customer customerDTOToCustomer(CustomerDTO customerDTO) {
        if ( customerDTO == null ) {
            return null;
        }

        Customer customer = new Customer();

        customer.setId( customerDTO.getId() );
        customer.setFirstName( customerDTO.getFirstName() );
        customer.setLastName( customerDTO.getLastName() );
        customer.setEmail( customerDTO.getEmail() );
        customer.setCustomerNumber( customerDTO.getCustomerNumber() );

        return customer;
    }

    protected Receipt receiptDTOToReceipt(ReceiptDTO receiptDTO) {
        if ( receiptDTO == null ) {
            return null;
        }

        Receipt receipt = new Receipt();

        receipt.setId( receiptDTO.getId() );
        receipt.setCreatedAt( receiptDTO.getCreatedAt() );

        return receipt;
    }

    protected Set<EventAreaDTO> eventAreaSetToEventAreaDTOSet(Set<EventArea> set) {
        if ( set == null ) {
            return null;
        }

        Set<EventAreaDTO> set1 = new HashSet<EventAreaDTO>( Math.max( (int) ( set.size() / .75f ) + 1, 16 ) );
        for ( EventArea eventArea : set ) {
            set1.add( eventAreaToEventAreaDTO( eventArea ) );
        }

        return set1;
    }
}
