package at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.location;

import at.ac.tuwien.inso.sepm.ticketline.rest.location.RowDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.location.SeatDTO;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Row;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Seat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2018-01-21T20:46:51+0100",
    comments = "version: 1.2.0.Final, compiler: javac, environment: Java 9.0.4 (Oracle Corporation)"
)
@Component
public class RowMapperImpl implements RowMapper {

    @Override
    public Row rowDTOtoRow(RowDTO rowDTO) {
        if ( rowDTO == null ) {
            return null;
        }

        Row row = new Row();

        row.setId( rowDTO.getId() );
        row.setOrderIndex( rowDTO.getOrderIndex() );
        row.setName( rowDTO.getName() );
        row.setSeats( seatDTOSetToSeatSet( rowDTO.getSeats() ) );

        return row;
    }

    @Override
    public RowDTO rowToRowDTO(Row Row) {
        if ( Row == null ) {
            return null;
        }

        RowDTO rowDTO = new RowDTO();

        rowDTO.setId( Row.getId() );
        rowDTO.setOrderIndex( Row.getOrderIndex() );
        rowDTO.setName( Row.getName() );
        rowDTO.setSeats( seatSetToSeatDTOSet( Row.getSeats() ) );

        return rowDTO;
    }

    @Override
    public List<RowDTO> rowToRowDTO(List<Row> Rows) {
        if ( Rows == null ) {
            return null;
        }

        List<RowDTO> list = new ArrayList<RowDTO>( Rows.size() );
        for ( Row row : Rows ) {
            list.add( rowToRowDTO( row ) );
        }

        return list;
    }

    @Override
    public SeatDTO seatToSeatDTO(Seat seat) {
        if ( seat == null ) {
            return null;
        }

        SeatDTO seatDTO = new SeatDTO();

        seatDTO.setId( seat.getId() );
        seatDTO.setOrderIndex( seat.getOrderIndex() );
        seatDTO.setName( seat.getName() );

        return seatDTO;
    }

    @Override
    public Seat seatDTOToSeat(SeatDTO seatDTO) {
        if ( seatDTO == null ) {
            return null;
        }

        Seat seat = new Seat();

        seat.setId( seatDTO.getId() );
        seat.setOrderIndex( seatDTO.getOrderIndex() );
        seat.setName( seatDTO.getName() );

        return seat;
    }

    protected Set<Seat> seatDTOSetToSeatSet(Set<SeatDTO> set) {
        if ( set == null ) {
            return null;
        }

        Set<Seat> set1 = new HashSet<Seat>( Math.max( (int) ( set.size() / .75f ) + 1, 16 ) );
        for ( SeatDTO seatDTO : set ) {
            set1.add( seatDTOToSeat( seatDTO ) );
        }

        return set1;
    }

    protected Set<SeatDTO> seatSetToSeatDTOSet(Set<Seat> set) {
        if ( set == null ) {
            return null;
        }

        Set<SeatDTO> set1 = new HashSet<SeatDTO>( Math.max( (int) ( set.size() / .75f ) + 1, 16 ) );
        for ( Seat seat : set ) {
            set1.add( seatToSeatDTO( seat ) );
        }

        return set1;
    }
}
