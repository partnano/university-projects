package at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.location;

import at.ac.tuwien.inso.sepm.ticketline.rest.location.SectionDTO;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Section;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2018-01-21T20:46:41+0100",
    comments = "version: 1.2.0.Final, compiler: javac, environment: Java 9.0.4 (Oracle Corporation)"
)
@Component
public class SectionMapperImpl implements SectionMapper {

    @Override
    public Section sectionDTOtoSection(SectionDTO sectionDTO) {
        if ( sectionDTO == null ) {
            return null;
        }

        Section section = new Section();

        section.setId( sectionDTO.getId() );
        section.setOrderIndex( sectionDTO.getOrderIndex() );
        section.setName( sectionDTO.getName() );
        section.setCapacity( sectionDTO.getCapacity() );

        return section;
    }

    @Override
    public SectionDTO sectionToSectionDTO(Section section) {
        if ( section == null ) {
            return null;
        }

        SectionDTO sectionDTO = new SectionDTO();

        sectionDTO.setId( section.getId() );
        sectionDTO.setOrderIndex( section.getOrderIndex() );
        sectionDTO.setName( section.getName() );
        sectionDTO.setCapacity( section.getCapacity() );

        return sectionDTO;
    }

    @Override
    public List<SectionDTO> sectionToSectionDTO(List<Section> sections) {
        if ( sections == null ) {
            return null;
        }

        List<SectionDTO> list = new ArrayList<SectionDTO>( sections.size() );
        for ( Section section : sections ) {
            list.add( sectionToSectionDTO( section ) );
        }

        return list;
    }
}
