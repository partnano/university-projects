package at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.event;

import at.ac.tuwien.inso.sepm.ticketline.rest.booking.BookingDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventAreaDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventDTO;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Booking;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Event;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.EventArea;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.booking.GenericBookingMapper;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.location.GenericAreaMapper;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.annotation.Generated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2018-01-21T20:46:50+0100",
    comments = "version: 1.2.0.Final, compiler: javac, environment: Java 9.0.4 (Oracle Corporation)"
)
@Component
public class EventMapperImpl implements EventMapper {

    @Autowired
    private SimpleEventMapper simpleEventMapper;
    @Autowired
    private GenericAreaMapper genericAreaMapper;
    @Autowired
    private GenericBookingMapper genericBookingMapper;

    @Override
    public Event eventDTOtoEvent(EventDTO eventDTO) {
        if ( eventDTO == null ) {
            return null;
        }

        Event event = new Event();

        event.setId( eventDTO.getId() );
        event.setTime( eventDTO.getTime() );
        event.setName( eventDTO.getName() );
        event.setDescription( eventDTO.getDescription() );
        event.setArtist( eventDTO.getArtist() );
        event.setCategory( eventDTO.getCategory() );
        event.setDuration( eventDTO.getDuration() );
        event.setLocation( simpleEventMapper.locationDTOToLocation( eventDTO.getLocation() ) );
        event.setEventAreas( eventAreaDTOSetToEventAreaSet( eventDTO.getEventAreas() ) );
        event.setBookings( bookingDTOSetToBookingSet( eventDTO.getBookings() ) );

        return event;
    }

    @Override
    public EventDTO eventToEventDTO(Event event) {
        if ( event == null ) {
            return null;
        }

        EventDTO eventDTO = new EventDTO();

        eventDTO.setId( event.getId() );
        eventDTO.setTime( event.getTime() );
        eventDTO.setArtist( event.getArtist() );
        eventDTO.setCategory( event.getCategory() );
        eventDTO.setName( event.getName() );
        eventDTO.setDescription( event.getDescription() );
        eventDTO.setDuration( event.getDuration() );
        eventDTO.setLocation( simpleEventMapper.locationToLocationDTO( event.getLocation() ) );
        eventDTO.setBookings( bookingSetToBookingDTOSet( event.getBookings() ) );
        eventDTO.setEventAreas( eventAreaSetToEventAreaDTOSet( event.getEventAreas() ) );

        return eventDTO;
    }

    @Override
    public List<EventDTO> eventToEventDTO(List<Event> events) {
        if ( events == null ) {
            return null;
        }

        List<EventDTO> list = new ArrayList<EventDTO>( events.size() );
        for ( Event event : events ) {
            list.add( eventToEventDTO( event ) );
        }

        return list;
    }

    @Override
    public EventArea eventAreaDTOtoEventArea(EventAreaDTO source) {
        if ( source == null ) {
            return null;
        }

        EventArea eventArea = new EventArea();

        eventArea.setId( source.getId() );
        eventArea.setArea( genericAreaMapper.areaDTOToArea( source.getArea() ) );
        eventArea.setPrice( source.getPrice() );

        return eventArea;
    }

    @Override
    public EventAreaDTO eventAreaToEventAreaDTO(EventArea source) {
        if ( source == null ) {
            return null;
        }

        EventAreaDTO eventAreaDTO = new EventAreaDTO();

        eventAreaDTO.setId( source.getId() );
        eventAreaDTO.setArea( genericAreaMapper.areaToAreaDTO( source.getArea() ) );
        eventAreaDTO.setPrice( source.getPrice() );

        return eventAreaDTO;
    }

    protected Set<EventArea> eventAreaDTOSetToEventAreaSet(Set<EventAreaDTO> set) {
        if ( set == null ) {
            return null;
        }

        Set<EventArea> set1 = new HashSet<EventArea>( Math.max( (int) ( set.size() / .75f ) + 1, 16 ) );
        for ( EventAreaDTO eventAreaDTO : set ) {
            set1.add( eventAreaDTOtoEventArea( eventAreaDTO ) );
        }

        return set1;
    }

    protected Set<Booking> bookingDTOSetToBookingSet(Set<BookingDTO> set) {
        if ( set == null ) {
            return null;
        }

        Set<Booking> set1 = new HashSet<Booking>( Math.max( (int) ( set.size() / .75f ) + 1, 16 ) );
        for ( BookingDTO bookingDTO : set ) {
            set1.add( genericBookingMapper.bookingDTOToBooking( bookingDTO ) );
        }

        return set1;
    }

    protected Set<BookingDTO> bookingSetToBookingDTOSet(Set<Booking> set) {
        if ( set == null ) {
            return null;
        }

        Set<BookingDTO> set1 = new HashSet<BookingDTO>( Math.max( (int) ( set.size() / .75f ) + 1, 16 ) );
        for ( Booking booking : set ) {
            set1.add( genericBookingMapper.bookingToBookingDTO( booking ) );
        }

        return set1;
    }

    protected Set<EventAreaDTO> eventAreaSetToEventAreaDTOSet(Set<EventArea> set) {
        if ( set == null ) {
            return null;
        }

        Set<EventAreaDTO> set1 = new HashSet<EventAreaDTO>( Math.max( (int) ( set.size() / .75f ) + 1, 16 ) );
        for ( EventArea eventArea : set ) {
            set1.add( eventAreaToEventAreaDTO( eventArea ) );
        }

        return set1;
    }
}
