package at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.booking;

import at.ac.tuwien.inso.sepm.ticketline.rest.booking.BookingDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.booking.ReceiptDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.booking.SeatBookingDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.customer.CustomerDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventAreaDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.location.AreaDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.location.LocationDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.location.RowDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.location.SeatDTO;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Area;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Booking;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Customer;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Event;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.EventArea;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Location;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Receipt;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Row;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Seat;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.SeatBooking;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.location.GenericAreaMapper;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.annotation.Generated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2018-01-21T20:46:45+0100",
    comments = "version: 1.2.0.Final, compiler: javac, environment: Java 9.0.4 (Oracle Corporation)"
)
@Component
public class SeatBookingMapperImpl extends SeatBookingMapper {

    @Autowired
    private GenericAreaMapper genericAreaMapper;
    @Autowired
    private GenericBookingMapper genericBookingMapper;

    @Override
    public SeatBookingDTO seatBookingToSeatBookingDTO(SeatBooking seatBooking) {
        if ( seatBooking == null ) {
            return null;
        }

        SeatBookingDTO seatBookingDTO = new SeatBookingDTO();

        seatBookingDTO.setId( seatBooking.getId() );
        seatBookingDTO.setReservationNumber( seatBooking.getReservationNumber() );
        seatBookingDTO.setPaid( seatBooking.isPaid() );
        seatBookingDTO.setCanceled( seatBooking.isCanceled() );
        seatBookingDTO.setCreatedAt( seatBooking.getCreatedAt() );
        seatBookingDTO.setBookedAt( seatBooking.getBookedAt() );
        seatBookingDTO.setPaymentMethod( seatBooking.getPaymentMethod() );
        seatBookingDTO.setEvent( eventToEventDTO( seatBooking.getEvent() ) );
        seatBookingDTO.setCustomer( customerToCustomerDTO( seatBooking.getCustomer() ) );
        seatBookingDTO.setPaymentReceipt( receiptToReceiptDTO( seatBooking.getPaymentReceipt() ) );
        seatBookingDTO.setCancellationReceipt( receiptToReceiptDTO( seatBooking.getCancellationReceipt() ) );
        seatBookingDTO.setTotalPrice( seatBooking.getTotalPrice() );
        seatBookingDTO.setSeats( seatSetToSeatDTOSet( seatBooking.getSeats() ) );

        return seatBookingDTO;
    }

    @Override
    public List<SeatBookingDTO> seatBookingsToSeatBookingDTOs(List<SeatBooking> seatBookings) {
        if ( seatBookings == null ) {
            return null;
        }

        List<SeatBookingDTO> list = new ArrayList<SeatBookingDTO>( seatBookings.size() );
        for ( SeatBooking seatBooking : seatBookings ) {
            list.add( seatBookingToSeatBookingDTO( seatBooking ) );
        }

        return list;
    }

    @Override
    public SeatBooking seatBookingDTOToSeatBooking(SeatBookingDTO seatBooking) {
        if ( seatBooking == null ) {
            return null;
        }

        SeatBooking seatBooking1 = new SeatBooking();

        seatBooking1.setId( seatBooking.getId() );
        seatBooking1.setReservationNumber( seatBooking.getReservationNumber() );
        seatBooking1.setPaid( seatBooking.isPaid() );
        seatBooking1.setCanceled( seatBooking.isCanceled() );
        seatBooking1.setCreatedAt( seatBooking.getCreatedAt() );
        seatBooking1.setBookedAt( seatBooking.getBookedAt() );
        seatBooking1.setPaymentMethod( seatBooking.getPaymentMethod() );
        seatBooking1.setEvent( eventDTOToEvent( seatBooking.getEvent() ) );
        seatBooking1.setCustomer( customerDTOToCustomer( seatBooking.getCustomer() ) );
        seatBooking1.setPaymentReceipt( receiptDTOToReceipt( seatBooking.getPaymentReceipt() ) );
        seatBooking1.setCancellationReceipt( receiptDTOToReceipt( seatBooking.getCancellationReceipt() ) );
        seatBooking1.setTotalPrice( seatBooking.getTotalPrice() );
        seatBooking1.setSeats( seatDTOSetToSeatSet( seatBooking.getSeats() ) );

        replaceSeats( seatBooking, seatBooking1 );

        return seatBooking1;
    }

    @Override
    public List<SeatBooking> seatBookingsDTOsToSeatBookings(List<SeatBookingDTO> seatBookings) {
        if ( seatBookings == null ) {
            return null;
        }

        List<SeatBooking> list = new ArrayList<SeatBooking>( seatBookings.size() );
        for ( SeatBookingDTO seatBookingDTO : seatBookings ) {
            list.add( seatBookingDTOToSeatBooking( seatBookingDTO ) );
        }

        return list;
    }

    @Override
    public EventDTO eventToEventDTO(Event event) {
        if ( event == null ) {
            return null;
        }

        EventDTO eventDTO = new EventDTO();

        eventDTO.setId( event.getId() );
        eventDTO.setTime( event.getTime() );
        eventDTO.setArtist( event.getArtist() );
        eventDTO.setCategory( event.getCategory() );
        eventDTO.setName( event.getName() );
        eventDTO.setDescription( event.getDescription() );
        eventDTO.setDuration( event.getDuration() );
        eventDTO.setLocation( locationToLocationDTO( event.getLocation() ) );
        eventDTO.setEventAreas( eventAreaSetToEventAreaDTOSet( event.getEventAreas() ) );

        return eventDTO;
    }

    @Override
    public EventAreaDTO eventAreaToEventAreaDTO(EventArea eventArea) {
        if ( eventArea == null ) {
            return null;
        }

        EventAreaDTO eventAreaDTO = new EventAreaDTO();

        eventAreaDTO.setId( eventArea.getId() );
        eventAreaDTO.setArea( genericAreaMapper.areaToAreaDTO( eventArea.getArea() ) );
        eventAreaDTO.setPrice( eventArea.getPrice() );

        return eventAreaDTO;
    }

    @Override
    public RowDTO rowToRowDTO(Row row) {
        if ( row == null ) {
            return null;
        }

        RowDTO rowDTO = new RowDTO();

        rowDTO.setId( row.getId() );
        rowDTO.setOrderIndex( row.getOrderIndex() );
        rowDTO.setName( row.getName() );

        return rowDTO;
    }

    protected CustomerDTO customerToCustomerDTO(Customer customer) {
        if ( customer == null ) {
            return null;
        }

        CustomerDTO customerDTO = new CustomerDTO();

        customerDTO.setId( customer.getId() );
        customerDTO.setFirstName( customer.getFirstName() );
        customerDTO.setLastName( customer.getLastName() );
        customerDTO.setEmail( customer.getEmail() );
        customerDTO.setCustomerNumber( customer.getCustomerNumber() );

        return customerDTO;
    }

    protected ReceiptDTO receiptToReceiptDTO(Receipt receipt) {
        if ( receipt == null ) {
            return null;
        }

        ReceiptDTO receiptDTO = new ReceiptDTO();

        receiptDTO.setId( receipt.getId() );
        receiptDTO.setCreatedAt( receipt.getCreatedAt() );

        return receiptDTO;
    }

    protected SeatDTO seatToSeatDTO(Seat seat) {
        if ( seat == null ) {
            return null;
        }

        SeatDTO seatDTO = new SeatDTO();

        seatDTO.setId( seat.getId() );
        seatDTO.setRow( rowToRowDTO( seat.getRow() ) );
        seatDTO.setOrderIndex( seat.getOrderIndex() );
        seatDTO.setName( seat.getName() );

        return seatDTO;
    }

    protected Set<SeatDTO> seatSetToSeatDTOSet(Set<Seat> set) {
        if ( set == null ) {
            return null;
        }

        Set<SeatDTO> set1 = new HashSet<SeatDTO>( Math.max( (int) ( set.size() / .75f ) + 1, 16 ) );
        for ( Seat seat : set ) {
            set1.add( seatToSeatDTO( seat ) );
        }

        return set1;
    }

    protected Set<Area> areaDTOSetToAreaSet(Set<AreaDTO> set) {
        if ( set == null ) {
            return null;
        }

        Set<Area> set1 = new HashSet<Area>( Math.max( (int) ( set.size() / .75f ) + 1, 16 ) );
        for ( AreaDTO areaDTO : set ) {
            set1.add( genericAreaMapper.areaDTOToArea( areaDTO ) );
        }

        return set1;
    }

    protected Location locationDTOToLocation(LocationDTO locationDTO) {
        if ( locationDTO == null ) {
            return null;
        }

        Location location = new Location();

        location.setId( locationDTO.getId() );
        location.setName( locationDTO.getName() );
        location.setStreet( locationDTO.getStreet() );
        location.setZip( locationDTO.getZip() );
        location.setAreas( areaDTOSetToAreaSet( locationDTO.getAreas() ) );
        location.setCountry( locationDTO.getCountry() );
        location.setCity( locationDTO.getCity() );

        return location;
    }

    protected EventArea eventAreaDTOToEventArea(EventAreaDTO eventAreaDTO) {
        if ( eventAreaDTO == null ) {
            return null;
        }

        EventArea eventArea = new EventArea();

        eventArea.setId( eventAreaDTO.getId() );
        eventArea.setArea( genericAreaMapper.areaDTOToArea( eventAreaDTO.getArea() ) );
        eventArea.setEvent( eventDTOToEvent( eventAreaDTO.getEvent() ) );
        eventArea.setPrice( eventAreaDTO.getPrice() );

        return eventArea;
    }

    protected Set<EventArea> eventAreaDTOSetToEventAreaSet(Set<EventAreaDTO> set) {
        if ( set == null ) {
            return null;
        }

        Set<EventArea> set1 = new HashSet<EventArea>( Math.max( (int) ( set.size() / .75f ) + 1, 16 ) );
        for ( EventAreaDTO eventAreaDTO : set ) {
            set1.add( eventAreaDTOToEventArea( eventAreaDTO ) );
        }

        return set1;
    }

    protected Set<Booking> bookingDTOSetToBookingSet(Set<BookingDTO> set) {
        if ( set == null ) {
            return null;
        }

        Set<Booking> set1 = new HashSet<Booking>( Math.max( (int) ( set.size() / .75f ) + 1, 16 ) );
        for ( BookingDTO bookingDTO : set ) {
            set1.add( genericBookingMapper.bookingDTOToBooking( bookingDTO ) );
        }

        return set1;
    }

    protected Event eventDTOToEvent(EventDTO eventDTO) {
        if ( eventDTO == null ) {
            return null;
        }

        Event event = new Event();

        event.setId( eventDTO.getId() );
        event.setTime( eventDTO.getTime() );
        event.setName( eventDTO.getName() );
        event.setDescription( eventDTO.getDescription() );
        event.setArtist( eventDTO.getArtist() );
        event.setCategory( eventDTO.getCategory() );
        event.setDuration( eventDTO.getDuration() );
        event.setLocation( locationDTOToLocation( eventDTO.getLocation() ) );
        event.setEventAreas( eventAreaDTOSetToEventAreaSet( eventDTO.getEventAreas() ) );
        event.setBookings( bookingDTOSetToBookingSet( eventDTO.getBookings() ) );

        return event;
    }

    protected Customer customerDTOToCustomer(CustomerDTO customerDTO) {
        if ( customerDTO == null ) {
            return null;
        }

        Customer customer = new Customer();

        customer.setId( customerDTO.getId() );
        customer.setFirstName( customerDTO.getFirstName() );
        customer.setLastName( customerDTO.getLastName() );
        customer.setEmail( customerDTO.getEmail() );
        customer.setCustomerNumber( customerDTO.getCustomerNumber() );

        return customer;
    }

    protected Receipt receiptDTOToReceipt(ReceiptDTO receiptDTO) {
        if ( receiptDTO == null ) {
            return null;
        }

        Receipt receipt = new Receipt();

        receipt.setId( receiptDTO.getId() );
        receipt.setCreatedAt( receiptDTO.getCreatedAt() );

        return receipt;
    }

    protected Set<Seat> seatDTOSetToSeatSet(Set<SeatDTO> set) {
        if ( set == null ) {
            return null;
        }

        Set<Seat> set1 = new HashSet<Seat>( Math.max( (int) ( set.size() / .75f ) + 1, 16 ) );
        for ( SeatDTO seatDTO : set ) {
            set1.add( seatDTOToSeat( seatDTO ) );
        }

        return set1;
    }

    protected Row rowDTOToRow(RowDTO rowDTO) {
        if ( rowDTO == null ) {
            return null;
        }

        Row row = new Row();

        row.setId( rowDTO.getId() );
        row.setLocation( locationDTOToLocation( rowDTO.getLocation() ) );
        row.setOrderIndex( rowDTO.getOrderIndex() );
        row.setName( rowDTO.getName() );
        row.setSeats( seatDTOSetToSeatSet( rowDTO.getSeats() ) );

        return row;
    }

    protected Seat seatDTOToSeat(SeatDTO seatDTO) {
        if ( seatDTO == null ) {
            return null;
        }

        Seat seat = new Seat();

        seat.setId( seatDTO.getId() );
        seat.setRow( rowDTOToRow( seatDTO.getRow() ) );
        seat.setOrderIndex( seatDTO.getOrderIndex() );
        seat.setName( seatDTO.getName() );

        return seat;
    }

    protected Set<AreaDTO> areaSetToAreaDTOSet(Set<Area> set) {
        if ( set == null ) {
            return null;
        }

        Set<AreaDTO> set1 = new HashSet<AreaDTO>( Math.max( (int) ( set.size() / .75f ) + 1, 16 ) );
        for ( Area area : set ) {
            set1.add( genericAreaMapper.areaToAreaDTO( area ) );
        }

        return set1;
    }

    protected LocationDTO locationToLocationDTO(Location location) {
        if ( location == null ) {
            return null;
        }

        LocationDTO locationDTO = new LocationDTO();

        locationDTO.setId( location.getId() );
        locationDTO.setName( location.getName() );
        locationDTO.setStreet( location.getStreet() );
        locationDTO.setZip( location.getZip() );
        locationDTO.setAreas( areaSetToAreaDTOSet( location.getAreas() ) );
        locationDTO.setCity( location.getCity() );
        locationDTO.setCountry( location.getCountry() );

        return locationDTO;
    }

    protected Set<EventAreaDTO> eventAreaSetToEventAreaDTOSet(Set<EventArea> set) {
        if ( set == null ) {
            return null;
        }

        Set<EventAreaDTO> set1 = new HashSet<EventAreaDTO>( Math.max( (int) ( set.size() / .75f ) + 1, 16 ) );
        for ( EventArea eventArea : set ) {
            set1.add( eventAreaToEventAreaDTO( eventArea ) );
        }

        return set1;
    }
}
