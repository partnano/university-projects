package at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.booking;

import at.ac.tuwien.inso.sepm.ticketline.rest.booking.BookingDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventAreaDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.location.AreaDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.location.LocationDTO;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Area;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Booking;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Event;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.EventArea;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Location;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.location.GenericAreaMapper;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.annotation.Generated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2018-01-21T20:46:51+0100",
    comments = "version: 1.2.0.Final, compiler: javac, environment: Java 9.0.4 (Oracle Corporation)"
)
@Component
public class BookingMapperImpl extends BookingMapper {

    @Autowired
    private GenericAreaMapper genericAreaMapper;

    @Override
    public EventDTO eventToEventDTO(Event event) {
        if ( event == null ) {
            return null;
        }

        EventDTO eventDTO = new EventDTO();

        eventDTO.setId( event.getId() );
        eventDTO.setTime( event.getTime() );
        eventDTO.setArtist( event.getArtist() );
        eventDTO.setCategory( event.getCategory() );
        eventDTO.setName( event.getName() );
        eventDTO.setDescription( event.getDescription() );
        eventDTO.setDuration( event.getDuration() );
        eventDTO.setLocation( locationToLocationDTO( event.getLocation() ) );
        eventDTO.setEventAreas( eventAreaSetToEventAreaDTOSet( event.getEventAreas() ) );

        return eventDTO;
    }

    @Override
    public EventAreaDTO eventAreaToEventAreaDTO(EventArea eventArea) {
        if ( eventArea == null ) {
            return null;
        }

        EventAreaDTO eventAreaDTO = new EventAreaDTO();

        eventAreaDTO.setId( eventArea.getId() );
        eventAreaDTO.setArea( genericAreaMapper.areaToAreaDTO( eventArea.getArea() ) );
        eventAreaDTO.setPrice( eventArea.getPrice() );

        return eventAreaDTO;
    }

    @Override
    public List<BookingDTO> bookingsToBookingDTOs(List<Booking> bookings) {
        if ( bookings == null ) {
            return null;
        }

        List<BookingDTO> list = new ArrayList<BookingDTO>( bookings.size() );
        for ( Booking booking : bookings ) {
            list.add( bookingToBookingDTO( booking ) );
        }

        return list;
    }

    @Override
    public List<Booking> bookingDTOsToBookings(List<BookingDTO> bookings) {
        if ( bookings == null ) {
            return null;
        }

        List<Booking> list = new ArrayList<Booking>( bookings.size() );
        for ( BookingDTO bookingDTO : bookings ) {
            list.add( bookingDTOToBooking( bookingDTO ) );
        }

        return list;
    }

    protected Set<AreaDTO> areaSetToAreaDTOSet(Set<Area> set) {
        if ( set == null ) {
            return null;
        }

        Set<AreaDTO> set1 = new HashSet<AreaDTO>( Math.max( (int) ( set.size() / .75f ) + 1, 16 ) );
        for ( Area area : set ) {
            set1.add( genericAreaMapper.areaToAreaDTO( area ) );
        }

        return set1;
    }

    protected LocationDTO locationToLocationDTO(Location location) {
        if ( location == null ) {
            return null;
        }

        LocationDTO locationDTO = new LocationDTO();

        locationDTO.setId( location.getId() );
        locationDTO.setName( location.getName() );
        locationDTO.setStreet( location.getStreet() );
        locationDTO.setZip( location.getZip() );
        locationDTO.setAreas( areaSetToAreaDTOSet( location.getAreas() ) );
        locationDTO.setCity( location.getCity() );
        locationDTO.setCountry( location.getCountry() );

        return locationDTO;
    }

    protected Set<EventAreaDTO> eventAreaSetToEventAreaDTOSet(Set<EventArea> set) {
        if ( set == null ) {
            return null;
        }

        Set<EventAreaDTO> set1 = new HashSet<EventAreaDTO>( Math.max( (int) ( set.size() / .75f ) + 1, 16 ) );
        for ( EventArea eventArea : set ) {
            set1.add( eventAreaToEventAreaDTO( eventArea ) );
        }

        return set1;
    }
}
