package at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.location;

import at.ac.tuwien.inso.sepm.ticketline.rest.location.SeatDTO;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Seat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2018-01-21T20:46:46+0100",
    comments = "version: 1.2.0.Final, compiler: javac, environment: Java 9.0.4 (Oracle Corporation)"
)
@Component
public class SeatMapperImpl implements SeatMapper {

    @Override
    public Seat seatDTOtoSeat(SeatDTO seatDTO) {
        if ( seatDTO == null ) {
            return null;
        }

        Seat seat = new Seat();

        seat.setId( seatDTO.getId() );
        seat.setOrderIndex( seatDTO.getOrderIndex() );
        seat.setName( seatDTO.getName() );

        return seat;
    }

    @Override
    public SeatDTO seatToSeatDTO(Seat seat) {
        if ( seat == null ) {
            return null;
        }

        SeatDTO seatDTO = new SeatDTO();

        seatDTO.setId( seat.getId() );
        seatDTO.setOrderIndex( seat.getOrderIndex() );
        seatDTO.setName( seat.getName() );

        return seatDTO;
    }

    @Override
    public List<SeatDTO> seatToSeatDTO(List<Seat> seats) {
        if ( seats == null ) {
            return null;
        }

        List<SeatDTO> list = new ArrayList<SeatDTO>( seats.size() );
        for ( Seat seat : seats ) {
            list.add( seatToSeatDTO( seat ) );
        }

        return list;
    }

    @Override
    public Set<SeatDTO> seatToSeatDTO(Set<Seat> seats) {
        if ( seats == null ) {
            return null;
        }

        Set<SeatDTO> set = new HashSet<SeatDTO>( Math.max( (int) ( seats.size() / .75f ) + 1, 16 ) );
        for ( Seat seat : seats ) {
            set.add( seatToSeatDTO( seat ) );
        }

        return set;
    }

    @Override
    public Set<Seat> seatDTOToSeat(Set<SeatDTO> seats) {
        if ( seats == null ) {
            return null;
        }

        Set<Seat> set = new HashSet<Seat>( Math.max( (int) ( seats.size() / .75f ) + 1, 16 ) );
        for ( SeatDTO seatDTO : seats ) {
            set.add( seatDTOtoSeat( seatDTO ) );
        }

        return set;
    }
}
