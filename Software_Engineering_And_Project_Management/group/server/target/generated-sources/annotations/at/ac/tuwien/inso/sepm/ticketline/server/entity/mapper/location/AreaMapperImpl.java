package at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.location;

import at.ac.tuwien.inso.sepm.ticketline.rest.location.AreaDTO;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Area;
import java.util.HashSet;
import java.util.Set;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2018-01-21T20:46:46+0100",
    comments = "version: 1.2.0.Final, compiler: javac, environment: Java 9.0.4 (Oracle Corporation)"
)
@Component
public class AreaMapperImpl implements AreaMapper {

    @Override
    public AreaDTO areaToAreaDTO(Area area) {
        if ( area == null ) {
            return null;
        }

        AreaDTO areaDTO = new AreaDTO();

        areaDTO.setId( area.getId() );
        areaDTO.setOrderIndex( area.getOrderIndex() );
        areaDTO.setName( area.getName() );

        return areaDTO;
    }

    @Override
    public Area areaDTOToArea(AreaDTO areaDTO) {
        if ( areaDTO == null ) {
            return null;
        }

        Area area = new Area();

        area.setId( areaDTO.getId() );
        area.setOrderIndex( areaDTO.getOrderIndex() );
        area.setName( areaDTO.getName() );

        return area;
    }

    @Override
    public Set<AreaDTO> areaToAreaDTO(Set<Area> areas) {
        if ( areas == null ) {
            return null;
        }

        Set<AreaDTO> set = new HashSet<AreaDTO>( Math.max( (int) ( areas.size() / .75f ) + 1, 16 ) );
        for ( Area area : areas ) {
            set.add( areaToAreaDTO( area ) );
        }

        return set;
    }

    @Override
    public Set<Area> areaDTOToArea(Set<AreaDTO> areaDTOs) {
        if ( areaDTOs == null ) {
            return null;
        }

        Set<Area> set = new HashSet<Area>( Math.max( (int) ( areaDTOs.size() / .75f ) + 1, 16 ) );
        for ( AreaDTO areaDTO : areaDTOs ) {
            set.add( areaDTOToArea( areaDTO ) );
        }

        return set;
    }
}
