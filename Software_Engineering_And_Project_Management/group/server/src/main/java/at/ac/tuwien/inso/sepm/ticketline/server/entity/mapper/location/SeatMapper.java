package at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.location;

import at.ac.tuwien.inso.sepm.ticketline.rest.location.SeatDTO;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Seat;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;
import java.util.Set;

@Mapper(componentModel = "spring")
public interface SeatMapper
{
	@Mapping(target = "row", ignore = true)
	Seat seatDTOtoSeat(SeatDTO seatDTO);

	@Mapping(target = "row", ignore = true)
	SeatDTO seatToSeatDTO(Seat seat);

	List<SeatDTO> seatToSeatDTO(List<Seat> seats);

	Set<SeatDTO> seatToSeatDTO(Set<Seat> seats);

	Set<Seat> seatDTOToSeat(Set<SeatDTO> seats);
}
