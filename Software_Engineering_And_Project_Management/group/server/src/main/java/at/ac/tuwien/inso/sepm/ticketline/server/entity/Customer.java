package at.ac.tuwien.inso.sepm.ticketline.server.entity;

import javax.persistence.*;
import java.util.Objects;
import java.util.Set;

@Entity
public class Customer
{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "seq_customers_id")
	@SequenceGenerator(name = "seq_customers_id", sequenceName = "seq_customers_id")
	private Long id;

	@Column
	private String firstName;

	@Column
	private String lastName;

	@Column
	private String email;

	@Column(nullable = false)
	private long customerNumber;

	@OneToMany(mappedBy = "customer")
	private Set<Booking> bookings;

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public String getFirstName()
	{
		return firstName;
	}

	public void setFirstName(String firstName)
	{
		this.firstName = firstName;
	}

	public String getLastName()
	{
		return lastName;
	}

	public void setLastName(String lastName)
	{
		this.lastName = lastName;
	}

	public String getEmail()
	{
		return email;
	}

	public void setEmail(String email)
	{
		this.email = email;
	}

	public long getCustomerNumber()
	{
		return customerNumber;
	}

	public void setCustomerNumber(long customerNumber)
	{
		this.customerNumber = customerNumber;
	}

	@Override
	public boolean equals(Object o)
	{
		if(this == o)
			return true;

		if(o == null || getClass() != o.getClass())
			return false;

		Customer customer = (Customer)o;
		return customerNumber == customer.customerNumber && Objects.equals(id, customer.id) && Objects.equals(
			firstName,
			customer.firstName) && Objects.equals(lastName, customer.lastName) && Objects.equals(email,
		                                                                                             customer.email)
		       && Objects.equals(bookings, customer.bookings);
	}

	@Override
	public String toString()
	{
		return "Customer{" + "id=" + id + ", firstName='" + firstName + '\'' + ", lastName='" + lastName + '\''
		       + ", email='" + email + '\'' + ", customerNumber=" + customerNumber + ", bookings=" + bookings
		       + '}';
	}
}
