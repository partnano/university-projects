package at.ac.tuwien.inso.sepm.ticketline.server.endpoint;

import at.ac.tuwien.inso.sepm.ticketline.rest.authentication.AuthenticationRequest;
import at.ac.tuwien.inso.sepm.ticketline.rest.authentication.AuthenticationToken;
import at.ac.tuwien.inso.sepm.ticketline.rest.authentication.AuthenticationTokenInfo;
import at.ac.tuwien.inso.sepm.ticketline.server.security.AuthenticationConstants;
import at.ac.tuwien.inso.sepm.ticketline.server.service.HeaderTokenAuthenticationService;
import at.ac.tuwien.inso.sepm.ticketline.server.service.implementation.SimpleHeaderTokenAuthenticationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping(value = "/authentication")
@Api(value = "authentication")
public class AuthenticationEndpoint
{
	private final HeaderTokenAuthenticationService service;

	public AuthenticationEndpoint(SimpleHeaderTokenAuthenticationService service)
	{
		this.service = service;
	}

	private static String tokenFromAuthorizationHeader(String authorization)
	{
		return authorization.substring(AuthenticationConstants.TOKEN_PREFIX.length()).trim();
	}

	@RequestMapping(method = RequestMethod.POST)
	@ApiOperation(value = "Get an authentication token with your username and password")
	public AuthenticationToken authenticate(@RequestBody AuthenticationRequest request)
	{
		return service.authenticate(request.getUsername(), request.getPassword());
	}

	@RequestMapping(method = RequestMethod.GET)
	@ApiOperation(value = "Get some valid authentication tokens")
	public AuthenticationToken authenticate(
		@ApiIgnore @RequestHeader(value = HttpHeaders.AUTHORIZATION) String authorization)
	{
		return service.renewAuthentication(tokenFromAuthorizationHeader(authorization));
	}

	@RequestMapping(value = "/info/{token}", method = RequestMethod.GET)
	@ApiOperation(value = "Get information about a specific authentication token")
	public AuthenticationTokenInfo tokenInfoAny(@PathVariable String token)
	{
		return service.authenticationTokenInfo(token);
	}

	@RequestMapping(value = "/info", method = RequestMethod.GET)
	@ApiOperation(value = "Get information about the current users authentication token")
	public AuthenticationTokenInfo tokenInfoCurrent(
		@ApiIgnore @RequestHeader(value = HttpHeaders.AUTHORIZATION) String authorization)
	{
		return service.authenticationTokenInfo(tokenFromAuthorizationHeader(authorization));
	}
}
