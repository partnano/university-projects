package at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.event;

import at.ac.tuwien.inso.sepm.ticketline.rest.location.LocationDTO;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Location;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.location.LocationMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SimpleEventMapper
{
	@Autowired
	private LocationMapper locationMapper;

	// Enable location mapping

	public LocationDTO locationToLocationDTO(Location location)
	{
		return locationMapper.locationToLocationDTO(location);
	}

	public Location locationDTOToLocation(LocationDTO locationDTO)
	{
		return locationMapper.locationDTOtoLocation(locationDTO);
	}
}
