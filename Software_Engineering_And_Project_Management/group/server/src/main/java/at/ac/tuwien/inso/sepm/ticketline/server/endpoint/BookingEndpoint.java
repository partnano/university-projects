package at.ac.tuwien.inso.sepm.ticketline.server.endpoint;

import at.ac.tuwien.inso.sepm.ticketline.rest.booking.BookingDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.payment.PaymentContext;
import at.ac.tuwien.inso.sepm.ticketline.rest.payment.PaymentProvider;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Booking;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Customer;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.booking.BookingMapper;
import at.ac.tuwien.inso.sepm.ticketline.server.exception.*;
import at.ac.tuwien.inso.sepm.ticketline.server.service.BookingService;
import at.ac.tuwien.inso.sepm.ticketline.server.service.CustomerService;
import at.ac.tuwien.inso.sepm.ticketline.server.service.PaymentService;
import at.ac.tuwien.inso.sepm.ticketline.server.service.PaymentServiceFactory;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/bookings")
@Api("bookings")
public class BookingEndpoint
{
	private static final Logger LOGGER = LoggerFactory.getLogger(BookingEndpoint.class);

	private final BookingService bookingService;
	private final BookingMapper bookingMapper;

	private final CustomerService customerService;

	private final PaymentServiceFactory paymentServiceFactory;

	public BookingEndpoint(BookingService bookingService, BookingMapper bookingMapper, CustomerService customerService,
	                       PaymentServiceFactory paymentServiceFactory)
	{
		this.bookingService = bookingService;
		this.bookingMapper = bookingMapper;
		this.customerService = customerService;
		this.paymentServiceFactory = paymentServiceFactory;
	}

	@RequestMapping(method = RequestMethod.POST)
	@ApiOperation("Create a new booking entry")
	public BookingDTO create(@RequestBody BookingDTO booking) throws ValidationException
	{
		try
		{
			LOGGER.info("creating booking, data from client: {}", booking);
			Booking bookingFromClient = bookingMapper.bookingDTOToBooking(booking);

			LOGGER.debug("booking after mapping {}", bookingFromClient);
			Booking createdBooking = bookingService.create(bookingFromClient);

			LOGGER.debug("created booking: {}", createdBooking);
			BookingDTO bookingDTO = bookingMapper.bookingToBookingDTO(createdBooking);

			LOGGER.info("sending created booking: {}", bookingDTO);
			return bookingDTO;
		}
		catch(ValidationException ex)
		{
			LOGGER.debug("validation failed for booking", ex);
			throw ex;
		}
		catch(BookingServiceException ex)
		{
			throw new InternalServerError(ex.getMessage());
		}
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	@ApiOperation("Update a booking entry")
	public BookingDTO update(@PathVariable Long id, @RequestBody BookingDTO booking) throws ValidationException
	{
		LOGGER.info("updating booking with id {}: {}" + "", id, booking);
		booking.setId(id);
		BookingDTO updatedBooking = bookingMapper.bookingToBookingDTO(bookingService.update(bookingMapper.bookingDTOToBooking(
			booking)));
		LOGGER.info("sending updated booking: {}", updatedBooking);
		return updatedBooking;
	}

	@RequestMapping(value = "/reservation/{reservationNumber}", method = RequestMethod.GET)
	@ApiOperation("Find a booking via reservation number")
	public BookingDTO findByReservationNumber(@PathVariable int reservationNumber) throws ValidationException
	{
		LOGGER.info("trying to find booking with res.number {}: ", reservationNumber);
		BookingDTO booking = bookingMapper.bookingToBookingDTO(bookingService.findByReservationNumber(
			reservationNumber));
		LOGGER.info("sending retrieved booking: {}", booking);
		return booking;
	}

	@RequestMapping(method = RequestMethod.GET)
	@ApiOperation("Retrieve all bookings of the specified customer")
	public List<BookingDTO> findAll(@RequestParam Long customerId) throws NotFoundException
	{
		LOGGER.debug("getting bookings for customer from bookingService ...");

		Customer customer;
		try
		{
			customer = customerService.findOne(customerId);
		}
		catch(ValidationException e)
		{
			LOGGER.info("could not find bookings for customer with id {} - could not find customer: {}",
			            customerId,
			            e);
			throw new NotFoundException("Customer with id " + customerId + " not found.");
		}

		List<Booking> bookings = bookingService.findAll(customer);
		List<BookingDTO> bookingDTOs = bookingMapper.bookingsToBookingDTOs(bookings);

		LOGGER.debug("retrieved bookings for customer from bookingService, passing to client...");
		LOGGER.debug("bookings after mapping: {}", bookingDTOs);
		return bookingDTOs;
	}

	@RequestMapping(value = "/pay", method = RequestMethod.POST)
	@ApiOperation("Carries out the payment for the specified booking")
	public BookingDTO pay(@RequestBody PaymentContext paymentContext) throws PaymentException, ValidationException,
	                                                                         BookingServiceException
	{
		Booking booking;
		if(paymentContext.getBookingDTO().getId() != null)
		{
			booking = bookingService.findOne(paymentContext.getBookingDTO().getId());
		}
		else
		{
			booking = bookingMapper.bookingDTOToBooking(paymentContext.getBookingDTO());
			booking = bookingService.create(booking);
		}
		booking.setPaymentMethod(paymentContext.getBookingDTO().getPaymentMethod());
		PaymentService paymentService = paymentServiceFactory.getService(PaymentProvider.valueOf(booking.getPaymentMethod()));
		paymentService.pay(booking, paymentContext.getCreditCardDTO());
		booking.setPaid(true);
		return bookingMapper.bookingToBookingDTO(bookingService.update(booking));
	}

	@RequestMapping(value = "/{id}/refund", method = RequestMethod.POST)
	public BookingDTO refund(@PathVariable Long id) throws PaymentException, ValidationException
	{
		Booking booking = bookingService.findOne(id);
		PaymentService paymentService = paymentServiceFactory.getService(PaymentProvider.valueOf(booking.getPaymentMethod()));
		paymentService.refund(booking);
		booking.setCanceled(true);
		return bookingMapper.bookingToBookingDTO(bookingService.update(booking));
	}
}
