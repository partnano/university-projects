package at.ac.tuwien.inso.sepm.ticketline.server.endpoint;

import at.ac.tuwien.inso.sepm.ticketline.rest.location.LocationDTO;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.location.LocationMapper;
import at.ac.tuwien.inso.sepm.ticketline.server.service.LocationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/locations")
@Api(value = "locations")
public class LocationsEndPoint
{
	private static final Logger LOGGER = LoggerFactory.getLogger(LocationsEndPoint.class);

	private final LocationService locationService;
	private final LocationMapper locationMapper;

	public LocationsEndPoint(LocationService locationService, LocationMapper locationMapper)
	{
		this.locationService = locationService;
		this.locationMapper = locationMapper;
	}

	@RequestMapping(method = RequestMethod.GET)
	@ApiOperation(value = "Get list of locations")
	public List<LocationDTO> findAll()
	{
		LOGGER.debug("List of locations requested...");
		List<LocationDTO> allLocations = locationMapper.locationToLocationDTO(locationService.findAll());
		LOGGER.debug("sending list of locations.");
		return allLocations;
	}
}
