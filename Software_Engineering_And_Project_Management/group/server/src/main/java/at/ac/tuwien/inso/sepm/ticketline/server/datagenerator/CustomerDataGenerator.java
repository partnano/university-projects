package at.ac.tuwien.inso.sepm.ticketline.server.datagenerator;

import at.ac.tuwien.inso.sepm.ticketline.server.entity.Customer;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.CustomerRepository;
import com.github.javafaker.Faker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.concurrent.ThreadLocalRandom;

@Profile("generateData")
@Component("customerDataGenerator")
public class CustomerDataGenerator
{
	private static final Logger LOGGER = LoggerFactory.getLogger(CustomerDataGenerator.class);
	private static final int NUMBER_OF_CUSTOMERS_TO_GENERATE = 300;

	private final CustomerRepository customerRepository;
	private final Faker faker;

	public CustomerDataGenerator(CustomerRepository customerRepository)
	{
		this.customerRepository = customerRepository;
		faker = new Faker();
	}

	@PostConstruct
	private void generateCustomers()
	{
		if(customerRepository.count() > 0)
		{
			LOGGER.debug("customers already generated");
		}
		else
		{
			LOGGER.info("generating {} customer entries", NUMBER_OF_CUSTOMERS_TO_GENERATE);

			for(int i = 0; i < NUMBER_OF_CUSTOMERS_TO_GENERATE; ++i)
			{
				Customer customer = new Customer();
				customer.setFirstName(faker.name().firstName());
				customer.setLastName(faker.name().lastName());
				customer.setEmail(customer.getFirstName() + "." + customer.getLastName() + "@mail.com");
				customer.setCustomerNumber(ThreadLocalRandom.current().nextLong(1, 100000));
				customerRepository.save(customer);
			}
		}
	}
}
