package at.ac.tuwien.inso.sepm.ticketline.server.exception;

public class BookingServiceException extends Exception
{
	public BookingServiceException()
	{
	}

	public BookingServiceException(String message)
	{
		super(message);
	}

	public BookingServiceException(String message, Throwable cause)
	{
		super(message, cause);
	}

	public BookingServiceException(Throwable cause)
	{
		super(cause);
	}
}
