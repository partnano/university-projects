package at.ac.tuwien.inso.sepm.ticketline.server.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Note: using multiple @Id statements to declare composite keys is supported by hibernate,
 * but not JPA compliant.
 * source: https://stackoverflow.com/a/19813646
 */
@Entity
@Table(name = "area")
@Inheritance(strategy = InheritanceType.JOINED)
public class Area implements Serializable
{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "seq_area_id")
	@SequenceGenerator(name = "seq_area_id", sequenceName = "seq_area_id")
	private Long id;

	@ManyToOne
	@JoinColumn(name = "fk_location_id")
	private Location location;

	@Column(name = "orderIndex", nullable = false)
	private Long orderIndex;

	@Column(nullable = false)
	private String name;

	public Area()
	{
	}

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public Location getLocation()
	{
		return location;
	}

	public void setLocation(Location location)
	{
		this.location = location;
	}

	public Long getOrderIndex()
	{
		return orderIndex;
	}

	public void setOrderIndex(Long orderIndex)
	{
		this.orderIndex = orderIndex;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	@Override
	public String toString()
	{
		return "Area{" + "id=" + id + ", orderIndex=" + orderIndex + ", name='" + name + '\'' + '}';
	}

	@Override
	public boolean equals(Object o)
	{
		if(this == o)
			return true;

		if(o == null || getClass() != o.getClass())
			return false;

		Area area = (Area)o;

		return id != null ? id.equals(area.id) : area.id == null;
	}

	public static AreaBuilder builder()
	{
		return new AreaBuilder();
	}

	public static class AreaBuilder
	{
		private Long id;
		private Location location;
		private Long orderIndex;
		private String name;

		public AreaBuilder id(Long id)
		{
			this.id = id;
			return this;
		}

		public AreaBuilder location(Location location)
		{
			this.location = location;
			return this;
		}

		public AreaBuilder orderIndex(Long orderIndex)
		{
			this.orderIndex = orderIndex;
			return this;
		}

		public AreaBuilder name(String name)
		{
			this.name = name;
			return this;
		}

		public Area build()
		{
			Area area = new Area();
			return build(area);
		}

		public Area build(Area area)
		{
			return applyFields(area);
		}

		private Area applyFields(Area area)
		{
			area.setId(id);
			area.setLocation(location);
			area.setOrderIndex(orderIndex);
			area.setName(name);
			return area;
		}
	}
}
