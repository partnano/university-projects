package at.ac.tuwien.inso.sepm.ticketline.server.service;

import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventFilterDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.enums.Category;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Event;
import at.ac.tuwien.inso.sepm.ticketline.server.exception.ValidationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

public interface EventService
{
	/**
	 * Find all events.
	 *
	 * @return list of all events
	 */
	List<Event> findAll();

	/**
	 * Find page of events
	 *
	 * @return page of events (from all)
 	 */
	Page<EventDTO> findPageFromAll(Pageable pageRequest);

	/**
	 * Find one event entry.
	 *
	 * @return event entry
	 *
	 * @param id id of event
	 *
	 * @throws ValidationException in case something went wrong
	 */
	Event findOne(Long id) throws ValidationException;

	/**
	 * Find events by filters.
	 * Every filter that isn't to be considered should be null.
	 *
	 * @param filter EventFilterDTO with requested filters.
	 *
	 * @return list of found events
	 */
	List<Event> findByFilter(EventFilterDTO filter);

	/**
	 * Find most popular events.
	 * @param category category to filter by, or null
	 * @param month month to filter by, or null
	 * @param year year to filter by, or null
	 * @param limit amount of events to return
	 * @return a map of up to {@code limit} events with their booking count
	 */
	Map<Event, Integer> findTopEvents(Category category, Integer month, Integer year, int limit) throws
	                                                                                             ValidationException;

	/**
	 * Find a page of events with filters.
	 * Every filter that isn't to be considered should be null.
	 *
	 * @param filter EventFilterDTO with requested filters.
	 *
	 * @return list of found events
	 */
	Page<EventDTO> findPageByFilter(EventFilterDTO filter, Pageable pageRequest);

	/**
	 * Create a new event, imported from a CSV
	 *
	 * @param event to be created
	 *
	 * @return the created event
	 */
	Event create(Event event);
}
