package at.ac.tuwien.inso.sepm.ticketline.server.datagenerator;

import at.ac.tuwien.inso.sepm.ticketline.server.entity.User;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.UserRepository;
import com.github.javafaker.Faker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Profile("generateData")
@Component("userDataGenerator")
public class UserDataGenerator
{
	private static final Logger LOGGER = LoggerFactory.getLogger(UserDataGenerator.class);
	private static final int NUMBER_OF_USERS_TO_GENERATE = 100;

	private static final String PASSWORD_SALT = "$2a$06$OxtUVAYWL7LuDmyZpNitaOV.7uH6Ag8MtXuFdcy3307ym6YfqEuLS";
	private static final String PASSWORD_HASH = BCrypt.hashpw("password", PASSWORD_SALT);

	private final UserRepository userRepository;
	private final Faker faker;

	public UserDataGenerator(UserRepository userRepository)
	{
		this.userRepository = userRepository;
		faker = new Faker();
	}

	@PostConstruct
	private void generateUsers()
	{
		if(userRepository.count() > 0)
		{
			LOGGER.debug("users already generated");
		}
		else
		{
			LOGGER.info("generating {} user entries", NUMBER_OF_USERS_TO_GENERATE);

			for(int i = 0; i < NUMBER_OF_USERS_TO_GENERATE; ++i)
			{
				User user = new User();
				user.setFirstName(faker.name().firstName());
				user.setLastName(faker.name().lastName());
				user.setEmail(String.format("%s.%s@datagen.com",
				                            user.getFirstName().toLowerCase(),
				                            user.getLastName().toLowerCase()));
				user.setAdmin(faker.bool().bool());
				user.setLocked(faker.bool().bool());
				user.setPasswordHash(PASSWORD_HASH);
				user.setPasswordSalt(PASSWORD_SALT);
				userRepository.save(user);
			}

			User admin = new User();
			admin.setFirstName("Root");
			admin.setLastName("Root");
			admin.setEmail("admin");
			admin.setAdmin(true);
			admin.setLocked(false);
			admin.setPasswordHash(PASSWORD_HASH);
			admin.setPasswordSalt(PASSWORD_SALT);
			userRepository.save(admin);
		}
	}
}
