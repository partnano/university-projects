package at.ac.tuwien.inso.sepm.ticketline.server.datagenerator;

import at.ac.tuwien.inso.sepm.ticketline.server.entity.Area;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Row;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Seat;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.AreaRepository;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.SeatRepository;
import com.github.javafaker.Faker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;

@Profile("generateData")
@Component("seatDataGenerator")
@DependsOn("areaDataGenerator")
public class SeatDataGenerator
{
	private static final Logger LOGGER = LoggerFactory.getLogger(SeatDataGenerator.class);

	private static final int NUMBER_OF_SEATS_TO_GENERATE = 12;

	private final SeatRepository seatRepository;
	private final AreaRepository areaRepository;
	private final Faker faker;

	public SeatDataGenerator(SeatRepository seatRepository, AreaRepository areaRepository)
	{
		this.seatRepository = seatRepository;
		this.areaRepository = areaRepository;
		faker = new Faker();
	}

	@PostConstruct
	public void generateSeats()
	{
		if(seatRepository.count() > 0)
		{
			LOGGER.info("seats already generated");
			return;
		}

		LOGGER.info("generating max {} seat entries per row", NUMBER_OF_SEATS_TO_GENERATE);

		List<Area> areas = areaRepository.findAll();

		for(int j = 0; j < areaRepository.count(); ++j)
		{
			if(areas.get(j) instanceof Row)
			{
				Row row = (Row)areas.get(j);

				for(int i = 0; i < NUMBER_OF_SEATS_TO_GENERATE + faker.number().numberBetween(-5, 5); i++)
				{
					Seat seat = Seat.builder()
					                .name(i + 1 + "")
					                .orderIndex((long)i + 1)
					                .row(row)
					                .build();
					row.getSeats().add(seat);
					seatRepository.save(seat);
				}

				areaRepository.save(row);
			}
		}
	}
}
