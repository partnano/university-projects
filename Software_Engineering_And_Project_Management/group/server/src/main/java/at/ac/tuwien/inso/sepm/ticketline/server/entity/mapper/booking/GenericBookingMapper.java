package at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.booking;

import at.ac.tuwien.inso.sepm.ticketline.rest.booking.BookingDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.booking.SeatBookingDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.booking.SectionBookingDTO;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Booking;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.SeatBooking;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.SectionBooking;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class GenericBookingMapper
{
	@Autowired
	private SeatBookingMapper seatBookingMapper;

	@Autowired
	private SectionBookingMapper sectionBookingMapper;

	public BookingDTO bookingToBookingDTO(Booking booking)
	{
		booking.setEvent(null);

		if(booking instanceof SectionBooking)
		{
			SectionBooking s = (SectionBooking)booking;
			return sectionBookingMapper.sectionBookingToSectionBookingDTO(s);
		}
		else // booking of type SeatBooking, as Booking is SectionBooking xor SeatBooking
		{
			SeatBooking s = (SeatBooking)booking;
			return seatBookingMapper.seatBookingToSeatBookingDTO(s);
		}
	}

	public Booking bookingDTOToBooking(BookingDTO booking)
	{
		booking.setEvent(null);

		if(booking instanceof SectionBookingDTO)
		{
			SectionBookingDTO s = (SectionBookingDTO)booking;
			return sectionBookingMapper.sectionBookingDTOToSectionBooking(s);
		}
		else // booking of type SeatBooking, as Booking is SectionBooking xor SeatBooking
		{
			SeatBookingDTO s = (SeatBookingDTO)booking;
			return seatBookingMapper.seatBookingDTOToSeatBooking(s);
		}
	}
}
