package at.ac.tuwien.inso.sepm.ticketline.server.repository;

import at.ac.tuwien.inso.sepm.ticketline.server.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

public interface UserRepository extends JpaRepository<User, Long>, PagingAndSortingRepository<User, Long>
{
	/**
	 * find single user via email (unique)
	 *
	 * @param email email to search
	 * @return user
	 */
	@Query("select u from User u where u.email = :email")
	User findUserByEmail(@Param("email") String email);

	/**
	 * Find page of all users
	 *
	 * @param pageable pagerequest
	 * @return page of all users
	 */
	Page<User> findAllByOrderByIdDesc(Pageable pageable);
}
