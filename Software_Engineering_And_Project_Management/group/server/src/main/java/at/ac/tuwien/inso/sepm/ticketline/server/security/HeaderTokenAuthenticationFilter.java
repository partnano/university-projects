package at.ac.tuwien.inso.sepm.ticketline.server.security;

import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.AuthenticationDetailsSource;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.util.Assert;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class HeaderTokenAuthenticationFilter extends OncePerRequestFilter
{
	private final AuthenticationManager manager;
	private final AuthenticationDetailsSource<HttpServletRequest, ?> source = new WebAuthenticationDetailsSource();

	public HeaderTokenAuthenticationFilter(AuthenticationManager manager)
	{
		Assert.notNull(manager, "manager cannot be null");
		this.manager = manager;
	}

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response,
	                                FilterChain chain) throws IOException, ServletException
	{
		String header = request.getHeader(HttpHeaders.AUTHORIZATION);

		if((header == null) || !header.startsWith(AuthenticationConstants.TOKEN_PREFIX))
		{
			chain.doFilter(request, response);
			return;
		}

		try
		{
			String token = header.substring(AuthenticationConstants.TOKEN_PREFIX.length());
			AuthenticationHeaderToken authenticationRequest = new AuthenticationHeaderToken(token);
			authenticationRequest.setDetails(source.buildDetails(request));
			Authentication authResult = manager.authenticate(authenticationRequest);
			SecurityContextHolder.getContext().setAuthentication(authResult);
		}
		catch(AuthenticationException failed)
		{
			SecurityContextHolder.clearContext();
		}

		chain.doFilter(request, response);
	}
}
