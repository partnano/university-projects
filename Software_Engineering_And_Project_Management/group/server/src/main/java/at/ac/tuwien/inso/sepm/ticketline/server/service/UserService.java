package at.ac.tuwien.inso.sepm.ticketline.server.service;

import at.ac.tuwien.inso.sepm.ticketline.rest.user.UserDTO;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.User;
import at.ac.tuwien.inso.sepm.ticketline.server.exception.ValidationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

public interface UserService
{
	/**
	 * Retrieve Page of all Users
	 *
	 * @param pageable Page Reference
	 * @return Page of all Users
	 */
	Page<UserDTO> findAll(Pageable pageable);

	/**
	 * Find User by username (email)
	 *
	 * @param email username
	 * @return found user
	 */
	User findByEmail(String email);

	/**
	 * Create new User
	 *
	 * @param user User to be added
	 * @return added user
	 * @throws ValidationException if something goes wrong
	 */
	User create(User user) throws ValidationException;

	/**
	 * Update a user
	 *
	 * @param user user to be updated
	 * @return the updated user details
	 * @throws ValidationException if validation fails
	 */
	User update(User user, String currentUsername) throws ValidationException;

	/**
	 * Resets a users' password and sends them an email containing the new password.
	 *
	 * @param id the user whose password is to be reset
	 * @throws ValidationException if validation fails
	 */
	void resetPassword(Long id) throws ValidationException;

	/**
	 * Authenticates a user.
	 *
	 * @param email    the name of the user
	 * @param password the password of the user
	 * @return a authentication if successful
	 * @throws AuthenticationException if authentication fails for any reason
	 */
	Authentication authenticate(String email, String password) throws AuthenticationException, ValidationException;
}
