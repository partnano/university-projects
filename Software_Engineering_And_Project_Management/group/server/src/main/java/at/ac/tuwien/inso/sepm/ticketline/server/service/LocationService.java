package at.ac.tuwien.inso.sepm.ticketline.server.service;

import at.ac.tuwien.inso.sepm.ticketline.server.entity.Location;

import java.util.List;

public interface LocationService
{
	/**
	 * Find all locations.
	 *
	 * @return list of all locations
	 */
	List<Location> findAll();
}
