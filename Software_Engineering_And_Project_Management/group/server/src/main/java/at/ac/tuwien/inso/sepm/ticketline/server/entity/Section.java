package at.ac.tuwien.inso.sepm.ticketline.server.entity;

import javax.persistence.*;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "sections")
public class Section extends Area
{
	@Column(nullable = false)
	private int capacity;

	@OneToMany(mappedBy = "section", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private Set<SectionBookingEntry> entries;

	public int getCapacity()
	{
		return capacity;
	}

	public void setCapacity(int capacity)
	{
		this.capacity = capacity;
	}

	public Set<SectionBookingEntry> getEntries()
	{
		return entries;
	}

	public void setEntries(Set<SectionBookingEntry> entries)
	{
		this.entries = entries;
	}

	public static SectionBuilder builder()
	{
		return new SectionBuilder();
	}

	/*@Override
	public boolean equals(Object o)
	{
		if(this == o)
			return true;

		if(o == null || getClass() != o.getClass())
			return false;

		if(!super.equals(o))
			return false;

		return true;
	}*/

	@Override
	public int hashCode()
	{
		return Objects.hash(capacity);
	}

	public static final class SectionBuilder extends AreaBuilder
	{
		private int capacity;
		private Set<SectionBookingEntry> entries;

		private SectionBuilder()
		{
		}

		public SectionBuilder capacity(int capacity)
		{
			this.capacity = capacity;
			return this;
		}

		public SectionBuilder entries(Set<SectionBookingEntry> entries)
		{
			this.entries = entries;
			return this;
		}

		@Override
		public SectionBuilder id(Long id)
		{
			return (SectionBuilder)super.id(id);
		}

		@Override
		public SectionBuilder location(Location location)
		{
			return (SectionBuilder)super.location(location);
		}

		@Override
		public SectionBuilder orderIndex(Long orderIndex)
		{
			return (SectionBuilder)super.orderIndex(orderIndex);
		}

		@Override
		public SectionBuilder name(String name)
		{
			return (SectionBuilder)super.name(name);
		}

		public Section build()
		{
			Section section = new Section();
			section = (Section)super.build(section);
			section.setCapacity(capacity);
			section.setEntries(entries);
			return section;
		}
	}
}
