package at.ac.tuwien.inso.sepm.ticketline.server.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class UserNews
{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "seq_usernews_id")
	@SequenceGenerator(name = "seq_usernews_id", sequenceName = "seq_usernews_id")
	private Long id;

	@ManyToOne
	@JoinColumn(name = "fk_news_id")
	private News news;

	@ManyToOne
	@JoinColumn(name = "fk_user_id")
	private User user;

	@Column
	private boolean hasRead;

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public News getNews()
	{
		return news;
	}

	public void setNews(News news)
	{
		this.news = news;
	}

	public User getUser()
	{
		return user;
	}

	public void setUser(User user)
	{
		this.user = user;
	}

	public boolean isHasRead()
	{
		return hasRead;
	}

	public void setHasRead(boolean hasRead)
	{
		this.hasRead = hasRead;
	}

	@Override
	public boolean equals(Object o)
	{
		if(this == o)
			return true;
		if(o == null || getClass() != o.getClass())
			return false;

		UserNews userNews = (UserNews)o;
		return Objects.equals(id, userNews.id) && Objects.equals(news, userNews.news) && Objects.equals(user,
		                                                                                                userNews.user)
		       && Objects.equals(hasRead, userNews.hasRead);
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(id);
	}

	@Override
	public String toString()
	{
		return "UserNews{" + "id=" + id + ", news=" + news + ", user=" + user + ", hasRead=" + hasRead + '}';
	}
}
