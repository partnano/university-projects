package at.ac.tuwien.inso.sepm.ticketline.server.service.implementation;

import at.ac.tuwien.inso.sepm.ticketline.rest.user.UserDTO;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.User;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.user.UserMapper;
import at.ac.tuwien.inso.sepm.ticketline.server.exception.NotFoundException;
import at.ac.tuwien.inso.sepm.ticketline.server.exception.ValidationException;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.UserRepository;
import at.ac.tuwien.inso.sepm.ticketline.server.service.EmailService;
import at.ac.tuwien.inso.sepm.ticketline.server.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SimpleUserService implements UserService
{
	private static final Logger LOGGER = LoggerFactory.getLogger(SimpleUserService.class);

	private static final String EMAIL_SOURCE = "mailer@ticketline3.at";
	private static final String EMAIL_CONTENT_TEMPLATE = "User: %s, Password: %s";

	@Autowired
	private EmailService emailService;

	private final UserRepository userRepository;
	private final UserMapper mapper;

	public SimpleUserService(UserRepository userRepository, UserMapper mapper)
	{
		this.userRepository = userRepository;
		this.mapper = mapper;
	}

	@Override
	public Page<UserDTO> findAll(Pageable pageable)
	{

		LOGGER.debug("retrieving users...");
		Page<User> result = userRepository.findAllByOrderByIdDesc(pageable);
		return convert(result, pageable);
	}

	@Override
	public User findByEmail(String email)
	{
		LOGGER.debug("retrieving user...");
		return userRepository.findUserByEmail(email);
	}

	private String generatePassword(User user)
	{
		String password = "password";
		String passwordSalt = BCrypt.gensalt();
		String passwordHash = BCrypt.hashpw(password, passwordSalt);

		user.setPasswordSalt(passwordSalt);
		user.setPasswordHash(passwordHash);

		return password;
	}

	@Override
	public User create(User user) throws ValidationException
	{
		if(user == null)
			throw new ValidationException("user");

		if(user.getEmail() == null || user.getEmail().isEmpty())
			throw new ValidationException("user.email");

		String password = generatePassword(user);
		User result = userRepository.save(user);

		emailService.sendEmail(EMAIL_SOURCE,
		                       List.of(user.getEmail()),
		                       "Your account details for Ticketline",
		                       String.format(EMAIL_CONTENT_TEMPLATE, user.getEmail(), password));

		return result;
	}

	@Override
	public User update(User user, String currentUsername) throws ValidationException
	{
		if(user == null)
			throw new ValidationException("user");

		if(user.getEmail().equals(currentUsername))
		{
			if(user.isLocked())
				throw new ValidationException("locking current user");
		}

		if(user.getEmail() == null || user.getEmail().isEmpty())
			throw new ValidationException("user.email");

		User current = userRepository.findOne(user.getId());

		if(current == null)
			throw new NotFoundException("no such user");

		user.setPasswordHash(current.getPasswordHash());
		user.setPasswordSalt(current.getPasswordSalt());
		return userRepository.save(user);
	}

	@Override
	public void resetPassword(Long id) throws ValidationException
	{
		if(id == null || id < 0)
			throw new ValidationException("id");

		User user = userRepository.findOne(id);

		if(user == null)
			throw new NotFoundException("no such user");

		String password = generatePassword(user);
		userRepository.save(user);

		emailService.sendEmail(EMAIL_SOURCE,
		                       List.of(user.getEmail()),
		                       "Your new account details for Ticketline",
		                       String.format(EMAIL_CONTENT_TEMPLATE, user.getEmail(), password));
	}

	private static final int MAX_INVALID_LOGINS = 4;

	private void recordInvalidLoginAttempt(User user)
	{
		LOGGER.info("invalid login attempt for user '{}'", user.getEmail());

		user.setInvalidLogins(user.getInvalidLogins() + 1);

		if(user.getInvalidLogins() > MAX_INVALID_LOGINS)
		{
			LOGGER.info("too many invalid login attempts, locking user '{}'", user.getEmail());
			user.setInvalidLogins(MAX_INVALID_LOGINS);
			user.setLocked(true);
		}

		userRepository.save(user);
	}

	private void clearInvalidLoginAttempts(User user)
	{
		LOGGER.info("clearing invalid login attempts for user '{}'", user.getEmail());
		user.setInvalidLogins(0);
		userRepository.save(user);
	}

	@Override
	public Authentication authenticate(String email, String password) throws AuthenticationException,
	                                                                         ValidationException
	{
		if(email == null || email.isEmpty())
			throw new ValidationException("email");

		if(password == null || password.isEmpty())
			throw new ValidationException("password");

		User user = userRepository.findUserByEmail(email);

		if(user == null)
			throw new BadCredentialsException("invalid user or password");

		String hashedPassword = BCrypt.hashpw(password, user.getPasswordSalt());

		if(!user.getPasswordHash().equals(hashedPassword))
		{
			recordInvalidLoginAttempt(user);
			throw new BadCredentialsException("invalid user or password");
		}

		if(user.isLocked())
		{
			LOGGER.info("user '{}' tried logging in but is locked", user.getEmail());
			throw new LockedException("account locked");
		}

		clearInvalidLoginAttempts(user);

		SimpleGrantedAuthority adminAuthority = new SimpleGrantedAuthority("ADMIN");
		SimpleGrantedAuthority userAuthority = new SimpleGrantedAuthority("USER");

		List<GrantedAuthority> authorities = new ArrayList<>();
		authorities.add(userAuthority);

		if(user.isAdmin())
			authorities.add(adminAuthority);

		LOGGER.info("user '{}' logged in successfully", user.getEmail());

		return new UsernamePasswordAuthenticationToken(email, password, authorities);
	}

	private Page<UserDTO> convert(Page<User> page, Pageable pageRequest)
	{
		if(page == null)
			throw new NotFoundException();

		List<UserDTO> userDTOs = mapper.usersToUserDTOs(page.getContent());
		Page<UserDTO> pageOfUserDTOs = new PageImpl<>(userDTOs, pageRequest, page.getTotalElements());

		// don't think this can happen
		if(pageRequest.getPageNumber() > pageOfUserDTOs.getTotalPages())
			throw new NotFoundException();

		LOGGER.debug("... sending page of users");

		return pageOfUserDTOs;
	}
}
