package at.ac.tuwien.inso.sepm.ticketline.server.service.implementation;

import at.ac.tuwien.inso.sepm.ticketline.server.configuration.Utils;
import at.ac.tuwien.inso.sepm.ticketline.server.exception.ValidationException;
import at.ac.tuwien.inso.sepm.ticketline.server.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Pair;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class SimpleAuthenticationProvider implements AuthenticationProvider
{
	@Autowired
	private UserService service;

	private final Map<String, Pair<String, List<String>>> predefinedUsers = new HashMap<>();

	private void initializePredefinedUsers()
	{
		if(Utils.isRunningAsTest())
		{
			predefinedUsers.put("admin", Pair.of("password", List.of("ADMIN", "USER")));
			predefinedUsers.put("user", Pair.of("password", List.of("USER")));
		}
	}

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException
	{
		String email = (String)authentication.getPrincipal();
		String password = (String)authentication.getCredentials();

		initializePredefinedUsers();

		Pair<String, List<String>> predefinedUser = predefinedUsers.get(email);

		if(predefinedUser != null && predefinedUser.getFirst().equals(password))
		{
			return new UsernamePasswordAuthenticationToken(email,
			                                               password,
			                                               predefinedUser.getSecond()
			                                                             .stream()
			                                                             .map(SimpleGrantedAuthority::new)
			                                                             .collect(Collectors.toList()));
		}

		try
		{
			return service.authenticate(email, password);
		}
		catch(ValidationException ex)
		{
			// this should never happen
			throw new AssertionError("unexpected exception", ex);
		}
	}

	@Override
	public boolean supports(Class<?> authentication)
	{
		return true;
	}
}
