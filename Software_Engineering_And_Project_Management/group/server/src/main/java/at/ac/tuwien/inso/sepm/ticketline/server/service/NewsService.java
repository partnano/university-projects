package at.ac.tuwien.inso.sepm.ticketline.server.service;

import at.ac.tuwien.inso.sepm.ticketline.rest.news.SimpleNewsDTO;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.News;
import at.ac.tuwien.inso.sepm.ticketline.server.exception.ValidationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface NewsService
{
	/**
	 * Find all news entries ordered by published at date (descending).
	 *
	 * @return ordered list of al news entries
	 */
	List<News> findAll();

	/**
	 * Find page of all news entries ordered by publishing date (descending).
	 *
	 * @param pageable pagerequest for page and size of page
	 * @param username which user requested the page
	 * @return ordered page of all news entries
	 */
	Page<SimpleNewsDTO> findAll(Pageable pageable, String username);

	/**
	 * Find page of unread news entries ordered by publishing date (descending).
	 *
	 * @param pageable pagerequest for page and size of page
	 * @param username which user requested the page
	 * @return page of simplenewsDTO
	 */
	Page<SimpleNewsDTO> findUnread(Pageable pageable, String username);

	/**
	 * Find a single news entry by id.
	 *
	 * @param id the is of the news entry
	 * @return the news entry
	 */
	News findOne(Long id);

	/**
	 * Publish a single news entry
	 *
	 * @param news to publish
	 * @return published news entry
	 * @throws ValidationException in case news object is invalid
	 */
	News publishNews(News news) throws ValidationException;

	/**
	 * Update isRead for news object
	 *
	 * @param news     relevant news object
	 * @param username relevant username
	 * @return updated news entry
	 */
	News update(News news, String username);
}
