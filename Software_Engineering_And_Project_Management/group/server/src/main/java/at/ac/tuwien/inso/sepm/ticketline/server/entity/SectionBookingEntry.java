package at.ac.tuwien.inso.sepm.ticketline.server.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = {"fk_booking_id", "fk_section_id"}))
public class SectionBookingEntry
{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "seq_entry_id")
	@SequenceGenerator(name = "seq_entry_id", sequenceName = "seq_entry_id")
	private Long id;

	@ManyToOne
	@JoinColumn(name = "fk_booking_id")
	private SectionBooking booking;

	@ManyToOne
	@JoinColumn(name = "fk_section_id")
	private Section section;

	private Long amount;

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public SectionBooking getBooking()
	{
		return booking;
	}

	public void setBooking(SectionBooking booking)
	{
		this.booking = booking;
	}

	public Section getSection()
	{
		return section;
	}

	public void setSection(Section section)
	{
		this.section = section;
	}

	public Long getAmount()
	{
		return amount;
	}

	public void setAmount(Long amount)
	{
		this.amount = amount;
	}

	@Override
	public String toString()
	{
		String bookingId = "not set";
		if(booking != null)
			bookingId = "id=" + booking.getId();

		return "SectionBookingEntry{" + "id=" + id + ", booking=" + bookingId + ", section=" + section
		       + ", amount=" + amount + '}';
	}

	@Override
	public boolean equals(Object o)
	{
		if(this == o)
			return true;

		if(o == null || getClass() != o.getClass())
			return false;

		SectionBookingEntry that = (SectionBookingEntry)o;

		return Objects.equals(id, that.id);
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(id, booking, section, amount);
	}
}
