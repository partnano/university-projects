package at.ac.tuwien.inso.sepm.ticketline.server.service;

import at.ac.tuwien.inso.sepm.ticketline.rest.customer.CustomerDTO;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Customer;
import at.ac.tuwien.inso.sepm.ticketline.server.exception.ValidationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface CustomerService
{
	/**
	 * Retrieve all customers
	 *
	 * @return List of customers
	 */
	List<Customer> findAll();

	/**
	 * Find page of all customers
	 *
	 * @param pageable pagerequest for page
	 * @return page of customer
	 */
	Page<CustomerDTO> findAll(Pageable pageable);

	/**
	 * Find a specific customer
	 *
	 * @param id for identification of customer to be found
	 *
	 * @return specified customer
	 */
	Customer findOne(Long id) throws ValidationException;

	/**
	 * Create a new customer
	 *
	 * @param customer to be created
	 *
	 * @return created customer
	 */
	Customer create(Customer customer) throws ValidationException;

	/**
	 * Update a customer.
	 *
	 * @param customer to be updated
	 *
	 * @return updated customer
	 */
	Customer update(Customer customer) throws ValidationException;
}
