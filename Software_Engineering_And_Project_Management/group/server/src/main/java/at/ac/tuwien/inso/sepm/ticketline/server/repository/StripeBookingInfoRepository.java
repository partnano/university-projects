package at.ac.tuwien.inso.sepm.ticketline.server.repository;

import at.ac.tuwien.inso.sepm.ticketline.server.entity.Booking;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.StripeBookingInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StripeBookingInfoRepository extends JpaRepository<StripeBookingInfo, Long>, BookingInfoRepository
{
	StripeBookingInfo findByBooking(Booking booking);
}
