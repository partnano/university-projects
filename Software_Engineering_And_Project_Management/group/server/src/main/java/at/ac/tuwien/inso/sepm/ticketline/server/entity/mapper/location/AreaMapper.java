package at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.location;

import at.ac.tuwien.inso.sepm.ticketline.rest.location.AreaDTO;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Area;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.Set;

@Mapper(componentModel = "spring")
public interface AreaMapper
{
	@Mapping(target = "location", ignore = true)
	AreaDTO areaToAreaDTO(Area area);

	@Mapping(target = "location", ignore = true)
	Area areaDTOToArea(AreaDTO areaDTO);

	Set<AreaDTO> areaToAreaDTO(Set<Area> areas);

	Set<Area> areaDTOToArea(Set<AreaDTO> areaDTOs);
}