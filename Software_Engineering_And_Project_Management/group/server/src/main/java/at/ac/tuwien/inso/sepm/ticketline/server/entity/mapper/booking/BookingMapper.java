package at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.booking;

import at.ac.tuwien.inso.sepm.ticketline.rest.booking.BookingDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.booking.SeatBookingDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.booking.SectionBookingDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventAreaDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventDTO;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.*;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.location.GenericAreaMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Mapper(componentModel = "spring", uses = GenericAreaMapper.class)
public abstract class BookingMapper
{
	@Autowired
	private SeatBookingMapper seatBookingMapper;

	@Autowired
	private SectionBookingMapper sectionBookingMapper;

	public BookingDTO bookingToBookingDTO(Booking booking)
	{
		if(booking == null)
			throw new NullPointerException("booking");

		if(booking instanceof SectionBooking)
		{
			SectionBooking s = (SectionBooking)booking;
			return sectionBookingMapper.sectionBookingToSectionBookingDTO(s);
		}
		else if(booking instanceof SeatBooking)
		{
			SeatBooking s = (SeatBooking)booking;
			return seatBookingMapper.seatBookingToSeatBookingDTO(s);
		}
		else
			throw new AssertionError("unexpected booking type: " + booking.getClass().getName());
	}

	public Booking bookingDTOToBooking(BookingDTO booking)
	{
		if(booking == null)
			throw new NullPointerException("booking");

		if(booking instanceof SectionBookingDTO)
		{
			SectionBookingDTO s = (SectionBookingDTO)booking;
			return sectionBookingMapper.sectionBookingDTOToSectionBooking(s);
		}
		else if(booking instanceof SeatBookingDTO)
		{
			SeatBookingDTO s = (SeatBookingDTO)booking;
			return seatBookingMapper.seatBookingDTOToSeatBooking(s);
		}
		else
			throw new AssertionError("unexpected booking type: " + booking.getClass().getName());
	}

	@Mapping(target = "bookings", ignore = true)
	public abstract EventDTO eventToEventDTO(Event event);

	@Mapping(target = "event", ignore = true)
	public abstract EventAreaDTO eventAreaToEventAreaDTO(EventArea eventArea);

	public abstract List<BookingDTO> bookingsToBookingDTOs(List<Booking> bookings);

	public abstract List<Booking> bookingDTOsToBookings(List<BookingDTO> bookings);
}
