package at.ac.tuwien.inso.sepm.ticketline.server.validation;

import at.ac.tuwien.inso.sepm.ticketline.rest.payment.CreditCardDTO;
import at.ac.tuwien.inso.sepm.ticketline.server.exception.ValidationException;

public class CreditCardValidator
{

	public static void validate(CreditCardDTO creditCardDTO) throws ValidationException
	{
		if(creditCardDTO == null)
			throw new ValidationException("card.empty");

		else
		{
			if(creditCardDTO.getCardNumber() == null || creditCardDTO.getCardNumber().trim().isEmpty())
				throw new ValidationException("card.number.empty");

			if(creditCardDTO.getExpirationMonth() == null)
				throw new ValidationException("card.exp.month.empty");

			else if(creditCardDTO.getExpirationMonth() < 1 || 12 < creditCardDTO.getExpirationMonth())
				throw new ValidationException("card.exp.month.invalid");

			if(creditCardDTO.getExpirationYear() == null)
				throw new ValidationException("card.exp.year.empty");

			else if(creditCardDTO.getExpirationYear() < 0)
				throw new ValidationException("card.exp.year.invalid");

			if(creditCardDTO.getCvc() == null)
				throw new ValidationException("card.cvc.empty");

			else if(creditCardDTO.getCvc() < 100 || 9999 < creditCardDTO.getCvc())
				throw new ValidationException("card.cvc.invalid");
		}
	}
}
