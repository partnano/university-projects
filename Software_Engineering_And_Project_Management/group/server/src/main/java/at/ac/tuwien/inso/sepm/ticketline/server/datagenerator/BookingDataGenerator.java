package at.ac.tuwien.inso.sepm.ticketline.server.datagenerator;

import at.ac.tuwien.inso.sepm.ticketline.rest.payment.PaymentProvider;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.*;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.BookingRepository;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.CustomerRepository;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.EventRepository;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.ReceiptRepository;
import com.github.javafaker.Faker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Profile("generateData")
@Component("bookingDataGenerator")
@DependsOn({"seatDataGenerator", "eventDataGenerator", "customerDataGenerator"})
public class BookingDataGenerator
{
	private static final Logger LOGGER = LoggerFactory.getLogger(LocationDataGenerator.class);
	private static final int NUMBER_OF_ROW_BOOKINGS_TO_GENERATE_PER_CUSTOMER = 3;
	private static final int NUMBER_OF_SECTION_BOOKINGS_TO_GENERATE_PER_CUSTOMER = 5;

	private final CustomerRepository customerRepository;
	private final EventRepository eventRepository;
	private final BookingRepository bookingRepository;
	private final ReceiptRepository receiptRepository;
	private final Faker faker;

	private final HashMap<Event, Set<Seat>> alreadyUsedSeats;

	public BookingDataGenerator(CustomerRepository customerRepository, EventRepository eventRepository,
	                            BookingRepository bookingRepository, ReceiptRepository receiptRepository)
	{
		this.customerRepository = customerRepository;
		this.eventRepository = eventRepository;
		this.bookingRepository = bookingRepository;
		this.receiptRepository = receiptRepository;
		this.faker = new Faker();

		this.alreadyUsedSeats = new HashMap<>();
	}

	@PostConstruct
	private void generateBookings()
	{
		if(bookingRepository.findAll().size() > 0)
		{
			LOGGER.info("bookings already generated");
			return;
		}

		LOGGER.info("generating booking entries");

		generateSeatBookings();
		generateSectionBookings();
	}

	private static boolean isSectionEvent(Event event)
	{
		return event.getLocation().getAreas().iterator().next() instanceof Section;
	}

	private void generateSectionBookings()
	{
		Random random = new Random();

		List<Event> sectionEvents = eventRepository.findAll()
		                                           .stream()
		                                           .filter(BookingDataGenerator::isSectionEvent)
		                                           .collect(Collectors.toList());

		List<Customer> customers = customerRepository.findAll();

		for(Customer customer : customers)
		{
			if(random.nextFloat() > 0.8)
				continue;

			int bookingsPerCustomer = random.nextInt(NUMBER_OF_SECTION_BOOKINGS_TO_GENERATE_PER_CUSTOMER) + 1;
			Set<Event> alreadyUsed = Collections.newSetFromMap(new IdentityHashMap<>());

			for(int i = 0; i != bookingsPerCustomer; ++i)
			{
				Event event;

				do
					event = sectionEvents.get(random.nextInt(sectionEvents.size()));
				while(alreadyUsed.contains(event));

				alreadyUsed.add(event);

				LocalDateTime date = LocalDateTime.ofInstant(faker.date().past(365, TimeUnit.DAYS).toInstant(),
				                                             ZoneId.systemDefault());

				SectionBooking booking = new SectionBooking();
				booking.setEvent(event);
				booking.setCustomer(customer);
				booking.setReservationNumber(random.nextInt(999999) + 1);
				booking.setCreatedAt(date);

				setRandomBookingStatus(booking, date);

				List<Section> sections = event.getLocation()
				                              .getAreas()
				                              .stream()
				                              .map(Section.class::cast)
				                              .collect(Collectors.toList());

				Section section = sections.get(random.nextInt(sections.size()));

				Set<SectionBookingEntry> entries = new HashSet<>();
				SectionBookingEntry entry = new SectionBookingEntry();
				entry.setBooking(booking);
				entry.setSection(section);
				entry.setAmount((long)(random.nextInt(3) + 1));
				entries.add(entry);
				booking.setEntries(entries);

				bookingRepository.save(booking);
			}
		}
	}

	private void generateSeatBookings()
	{
		Random random = new Random();

		// Generate row bookings for every customer
		List<Customer> allCustomers = customerRepository.findAll();

		List<Event> allEvents = eventRepository.findAll();

		for(Customer customer : allCustomers)
		{
			if(random.nextFloat() > 0.8)
				continue;

			int bookingsPerCustomer = random.nextInt(NUMBER_OF_ROW_BOOKINGS_TO_GENERATE_PER_CUSTOMER) + 1;
			for(int i = 0; i < bookingsPerCustomer; i++)
			{
				Event event = getRandomEventWithSeats();
				LocalDateTime created = LocalDateTime.ofInstant(faker.date()
				                                                     .past(365 * 3, TimeUnit.MINUTES)
				                                                     .toInstant(), ZoneId.systemDefault());

				LOGGER.debug("started creating new booking");

				SeatBooking booking = new SeatBooking();

				Set<Seat> seats = new HashSet<>();
				int randomNumberOfSeatsToBook = faker.number().numberBetween(0, 5);
				for(int j = 0; j < randomNumberOfSeatsToBook; j++)
				{
					Seat randomSeat = getRandomSeatForEvent(event);

					LOGGER.debug("received random seat {}", randomSeat);

					if(randomSeat == null || alreadyUsedSeats.get(event).contains(randomSeat))
					{
						LOGGER.debug("seat already used, moving on..");
						continue;
					}
					else
					{
						LOGGER.debug("added seat to event");
						alreadyUsedSeats.get(event).add(randomSeat);
					}

					seats.add(randomSeat);
				}

				booking.setCreatedAt(created);
				booking.setBookedAt(created);
				booking.setReservationNumber(random.nextInt(999999) + 1);
				booking.setCustomer(customer);
				booking.setCanceled(false);
				booking.setEvent(event);
				booking.setSeats(seats);

				setRandomBookingStatus(booking, created);

				// ignore bookings with no seats
				if(booking.getSeats().size() == 0)
					continue;

				LOGGER.debug("saving seat booking {}", booking);
				bookingRepository.save(booking);
			}
		}
	}

	private void setRandomBookingStatus(Booking booking, LocalDateTime bookingDate)
	{
		Random random = new Random();

		// set booked and create receipt
		if(random.nextBoolean())
		{
			booking.setBookedAt(bookingDate);
			booking.setPaid(true);
			booking.setPaymentMethod(faker.bool().bool() ? PaymentProvider.STRIPE.name() : PaymentProvider.CASH.name());

			Receipt paymentReceipt = new Receipt();
			paymentReceipt.setCreatedAt(bookingDate);
			paymentReceipt = receiptRepository.save(paymentReceipt);

			LOGGER.debug("saved payment receipt: {}", paymentReceipt);

			booking.setPaymentReceipt(paymentReceipt);

			// set cancelled and create cancellation receipt
			if(random.nextBoolean())
			{
				booking.setCanceled(true);

				Receipt cancellationReceipt = new Receipt();
				cancellationReceipt.setCreatedAt(bookingDate);
				cancellationReceipt = receiptRepository.save(cancellationReceipt);

				LOGGER.debug("saved cancellation receipt: {}", cancellationReceipt);

				booking.setCancellationReceipt(cancellationReceipt);
			}
		}
	}

	private Event getRandomEventWithSeats()
	{
		List<Event> allEvents = eventRepository.findAll();
		Event event = allEvents.get(faker.number().numberBetween(0, allEvents.size() - 1));
		Set<Area> locationAreas = event.getLocation().getAreas();

		for(Area area : locationAreas)
		{
			if(area instanceof Row)
			{
				return event;
			}
			else
			{
				break;
			}
		}

		// Event has no rows, look for another event.
		return getRandomEventWithSeats();
	}

	/**
	 * @param event event expected to have location with seats
	 */
	private Seat getRandomSeatForEvent(Event event)
	{
		if(!alreadyUsedSeats.containsKey(event))
		{
			alreadyUsedSeats.put(event, new HashSet<>());
		}

		LinkedList<Area> rows = new LinkedList<>(event.getLocation().getAreas());

		LOGGER.debug("finding random seat, rows: {}", rows);

		if(rows.size() == 0)
			return null;

		Row randomRow = (Row)rows.get(faker.number().numberBetween(0, rows.size() - 1));

		LOGGER.debug("finding random seat, selected random row: {}", randomRow);

		LinkedList<Seat> seats = new LinkedList<>(randomRow.getSeats());

		if(seats.size() == 0)
			return null;

		Seat randomSeat = seats.get(faker.number().numberBetween(0, seats.size() - 1));

		LOGGER.debug("finding random seat, selected random seat: {}", randomSeat);

		return randomSeat;
	}
}
