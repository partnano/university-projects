package at.ac.tuwien.inso.sepm.ticketline.server.service;

import at.ac.tuwien.inso.sepm.ticketline.rest.payment.CreditCardDTO;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Booking;
import at.ac.tuwien.inso.sepm.ticketline.server.exception.PaymentException;
import at.ac.tuwien.inso.sepm.ticketline.server.exception.ValidationException;

public interface PaymentService
{
	/**
	 * Withdraw a given amount from the given credit card for a booking.
	 *
	 * @param booking       to be paid
	 * @param creditCardDTO to pay with
	 */
	void pay(Booking booking, CreditCardDTO creditCardDTO) throws PaymentException, ValidationException;

	/**
	 * Refunds the given booking
	 *
	 * @param booking to be refunded
	 * @throws PaymentException if there is an error while paying
	 */
	void refund(Booking booking) throws PaymentException;

	//void setBookingInfoRepository(BookingInfoRepository bookingInfoRepository);
}
