package at.ac.tuwien.inso.sepm.ticketline.server.datagenerator;

import at.ac.tuwien.inso.sepm.ticketline.server.entity.Row;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Section;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.AreaRepository;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.LocationRepository;
import com.github.javafaker.Faker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Profile("generateData")
@Component("areaDataGenerator")
@DependsOn("locationDataGenerator")
public class AreaDataGenerator
{
	private static final Logger LOGGER = LoggerFactory.getLogger(AreaDataGenerator.class);

	private static final int NUMBER_OF_ROWS_TO_GENERATE = 17;
	private static final int NUMBER_OF_SECTIONS_TO_GENERATE = 5;

	private final AreaRepository areaRepository;
	private final LocationRepository locationRepository;
	private final Faker faker;

	public AreaDataGenerator(AreaRepository areaRepository, LocationRepository locationRepository)
	{
		this.areaRepository = areaRepository;
		this.locationRepository = locationRepository;
		faker = new Faker();
	}

	@PostConstruct
	private void generateAreas()
	{
		if(areaRepository.count() > 0)
		{
			LOGGER.info("areas already generated");
			return;
		}

		LOGGER.info("generating {} area entries", NUMBER_OF_ROWS_TO_GENERATE);

		for(int i = 0; i < NUMBER_OF_ROWS_TO_GENERATE + faker.number().numberBetween(-10, 10); i++)
		{
			Row row = Row.builder()
			             .name(i + 1 + "")
			             .orderIndex((long)i + 1)
			             .location(locationRepository.findAll().get(0))
			             .build();

			LOGGER.debug("saving row {}", row);
			areaRepository.save(row);
		}

		for(int i = 0; i < NUMBER_OF_SECTIONS_TO_GENERATE + faker.number().numberBetween(-2, 2); i++)
		{
			Section section = Section.builder()
			                         .name(i + 1 + "")
			                         .capacity(faker.number().numberBetween(10, 200))
			                         .orderIndex((long)i + 1)
			                         .location(locationRepository.findAll().get(1))
			                         .build();

			LOGGER.debug("saving section {}", section);
			areaRepository.save(section);
		}
	}
}
