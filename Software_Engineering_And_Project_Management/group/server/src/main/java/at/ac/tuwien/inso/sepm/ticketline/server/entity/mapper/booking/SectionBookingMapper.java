package at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.booking;

import at.ac.tuwien.inso.sepm.ticketline.rest.booking.SectionBookingDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventAreaDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventDTO;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Event;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.EventArea;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.SectionBooking;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.SectionBookingEntry;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.location.GenericAreaMapper;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import java.util.List;
import java.util.Set;

@Mapper(componentModel = "spring", uses = {GenericAreaMapper.class, GenericBookingMapper.class})
public abstract class SectionBookingMapper
{
	@Mapping(source = "entries", target = "sections")
	public abstract SectionBookingDTO sectionBookingToSectionBookingDTO(SectionBooking sectionBooking);

	public abstract List<SectionBookingDTO> sectionBookingsToSectionBookingDTOs(List<SectionBooking> sectionBookings);

	@Mapping(source = "sections", target = "entries")
	public abstract SectionBooking sectionBookingDTOToSectionBooking(SectionBookingDTO sectionBooking);

	public abstract List<SectionBooking> sectionBookingDTOsToSectionBookings(List<SectionBookingDTO> sectionBookings);

	@Mapping(target = "bookings", ignore = true)
	public abstract EventDTO eventToEventDTO(Event event);

	@Mapping(target = "event", ignore = true)
	public abstract EventAreaDTO eventAreaToEventAreaDTO(EventArea eventArea);

	@AfterMapping
	public void addBookingReferenceToSectionBookingEntry(SectionBookingDTO sectionBookingDTO,
	                                                     @MappingTarget SectionBooking sectionBooking)
	{
		Set<SectionBookingEntry> entries = sectionBooking.getEntries();

		// As the bookingEntryDTO does not have a booking attribute,
		// we manually add the booking reference after mapping data from the client
		if(entries != null)
		{
			for(SectionBookingEntry entry : entries)
			{
				entry.setBooking(sectionBooking);
			}
		}
	}
}
