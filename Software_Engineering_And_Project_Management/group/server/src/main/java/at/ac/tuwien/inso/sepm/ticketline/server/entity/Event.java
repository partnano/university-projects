package at.ac.tuwien.inso.sepm.ticketline.server.entity;

import at.ac.tuwien.inso.sepm.ticketline.rest.event.enums.Category;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Set;

@Entity
public class Event
{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "seq_event_id")
	@SequenceGenerator(name = "seq_event_id", sequenceName = "seq_event_id")
	private Long id;

	@Column(nullable = false, name = "time")
	private LocalDateTime time;

	@Column(nullable = false)
	private String name;

	@Column
	private String description;

	@Column
	private String artist;

	@Column(nullable = false)
	private Category category;

	@Column(nullable = false)
	private Integer duration;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "location_id")
	private Location location;

	@OneToMany(mappedBy = "event", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private Set<EventArea> eventAreas;

	@OneToMany(mappedBy = "event", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private Set<Booking> bookings;

	public Long getId()
	{
		return id;
	}

	public Event setId(Long id)
	{
		this.id = id;
		return this;
	}

	public LocalDateTime getTime()
	{
		return time;
	}

	public void setTime(LocalDateTime time)
	{
		this.time = time;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public String getArtist()
	{
		return artist;
	}

	public void setArtist(String artist)
	{
		this.artist = artist;
	}

	public Category getCategory()
	{
		return category;
	}

	public void setCategory(Category category)
	{
		this.category = category;
	}

	public Integer getDuration()
	{
		return duration;
	}

	public void setDuration(Integer duration)
	{
		this.duration = duration;
	}

	public Location getLocation()
	{
		return location;
	}

	public void setLocation(Location location)
	{
		this.location = location;
	}

	public Set<EventArea> getEventAreas()
	{
		return eventAreas;
	}

	public void setEventAreas(Set<EventArea> eventAreas)
	{
		this.eventAreas = eventAreas;
	}

	public Set<Booking> getBookings()
	{
		return bookings;
	}

	public void setBookings(Set<Booking> bookings)
	{
		this.bookings = bookings;
	}

	public static EventBuilder builder()
	{
		return new EventBuilder();
	}

	@Override
	public String toString()
	{
		return "Event{" + "id=" + id + ", time=" + time + ", name='" + name + '\'' + ", artist='" + artist
		       + '\'' + ", category='" + category + '\'' + ", duration='" + duration + '\'' + ", location="
		       + location + ", eventAreas=" + eventAreas + '}';
	}

	@Override
	public boolean equals(Object o)
	{
		if(this == o)
			return true;

		if(o == null || getClass() != o.getClass())
			return false;

		Event event = (Event)o;

		if(duration != null ? !duration.equals(event.duration) : event.duration != null)
			return false;

		if(id != null ? !id.equals(event.id) : event.id != null)
			return false;

		if(time != null ? !time.equals(event.time) : event.time != null)
			return false;

		if(name != null ? !name.equals(event.name) : event.name != null)
			return false;

		if(description != null ? !description.equals(event.description) : event.description != null)
			return false;

		if(artist != null ? !artist.equals(event.artist) : event.artist != null)
			return false;

		if(category != event.category)
			return false;

		if(location != null ? !location.equals(event.location) : event.location != null)
			return false;

		return eventAreas != null ? eventAreas.equals(event.eventAreas) : event.eventAreas == null;
	}

	public static final class EventBuilder
	{
		private Long id;
		private LocalDateTime time;
		private String name;
		private String description;
		private String artist;
		private Category category;
		private Integer duration;
		private Location location;
		private Set<EventArea> eventAreas;
		private Set<Booking> bookings;

		private EventBuilder()
		{
		}

		public EventBuilder id(Long id)
		{
			this.id = id;
			return this;
		}

		public EventBuilder time(LocalDateTime time)
		{
			this.time = time;
			return this;
		}

		public EventBuilder name(String name)
		{
			this.name = name;
			return this;
		}

		public EventBuilder description(String description)
		{
			this.description = description;
			return this;
		}

		public EventBuilder artist(String artist)
		{
			this.artist = artist;
			return this;
		}

		public EventBuilder category(Category category)
		{
			this.category = category;
			return this;
		}

		public EventBuilder duration(Integer duration)
		{
			this.duration = duration;
			return this;
		}

		public EventBuilder location(Location location)
		{
			this.location = location;
			return this;
		}

		public EventBuilder areas(Set<EventArea> areas)
		{
			this.eventAreas = areas;
			return this;
		}

		public EventBuilder bookings(Set<Booking> bookings)
		{
			this.bookings = bookings;
			return this;
		}

		public Event build()
		{
			Event event = new Event();
			event.setId(id);
			event.setTime(time);
			event.setName(name);
			event.setDescription(description);
			event.setCategory(category);
			event.setDuration(duration);
			event.setArtist(artist);
			event.setLocation(location);
			event.setEventAreas(eventAreas);
			event.setBookings(bookings);
			return event;
		}
	}
}
