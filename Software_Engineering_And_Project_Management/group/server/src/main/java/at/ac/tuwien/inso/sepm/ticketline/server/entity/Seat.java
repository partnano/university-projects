package at.ac.tuwien.inso.sepm.ticketline.server.entity;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "seats")
public class Seat
{
	@Id
	@GeneratedValue
	private Long id;

	@ManyToOne
	@JoinColumn
	private Row row;

	@Column(name = "orderIndex", nullable = false)
	private Long orderIndex;

	@Column
	private String name;

	@ManyToMany(mappedBy = "seats")
	private Set<SeatBooking> bookings;

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public Row getRow()
	{
		return row;
	}

	public void setRow(Row row)
	{
		this.row = row;
	}

	public Long getOrderIndex()
	{
		return orderIndex;
	}

	public void setOrderIndex(Long orderIndex)
	{
		this.orderIndex = orderIndex;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public static SeatBuilder builder()
	{
		return new SeatBuilder();
	}

	public static final class SeatBuilder
	{
		private Long id;
		private Row row;
		private Long orderIndex;
		private String name;

		public SeatBuilder id(Long id)
		{
			this.id = id;
			return this;
		}

		public SeatBuilder row(Row row)
		{
			this.row = row;
			return this;
		}

		public SeatBuilder orderIndex(Long orderIndex)
		{
			this.orderIndex = orderIndex;
			return this;
		}

		public SeatBuilder name(String name)
		{
			this.name = name;
			return this;
		}

		public Seat build()
		{
			Seat seat = new Seat();
			seat.setId(id);
			seat.setRow(row);
			seat.setOrderIndex(orderIndex);
			seat.setName(name);
			return seat;
		}
	}

	@Override
	public String toString()
	{
		String rowId = "null";
		if(row != null)
			rowId = "" + row.getId();

		return "Seat{" + "id=" + id + ", row=" + rowId + ", orderIndex=" + orderIndex + ", name='" + name
			+ '\'' + '}';
	}

	@Override
	public boolean equals(Object o)
	{
		if(this == o)
			return true;

		if(o == null || getClass() != o.getClass())
			return false;

		Seat seat = (Seat)o;

		if(id != null ? !id.equals(seat.id) : seat.id != null)
			return false;

		if(row != null ? !row.equals(seat.row) : seat.row != null)
			return false;

		if(orderIndex != null ? !orderIndex.equals(seat.orderIndex) : seat.orderIndex != null)
			return false;

		return name != null ? name.equals(seat.name) : seat.name == null;
	}
}
