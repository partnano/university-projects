package at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.location;

import at.ac.tuwien.inso.sepm.ticketline.rest.location.SectionDTO;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Section;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface SectionMapper
{
	@Mapping(target = "location", ignore = true)
	Section sectionDTOtoSection(SectionDTO sectionDTO);

	@Mapping(target = "location", ignore = true)
	SectionDTO sectionToSectionDTO(Section section);

	List<SectionDTO> sectionToSectionDTO(List<Section> sections);
}
