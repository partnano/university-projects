package at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.location;

import at.ac.tuwien.inso.sepm.ticketline.rest.location.RowDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.location.SeatDTO;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Row;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Seat;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface RowMapper
{
	@Mapping(target = "location", ignore = true)
	Row rowDTOtoRow(RowDTO rowDTO);

	@Mapping(target = "location", ignore = true)
	RowDTO rowToRowDTO(Row Row);

	List<RowDTO> rowToRowDTO(List<Row> Rows);

	// Enable cross referencing by removing seat to row reference before mapping
	// and adding it back later (see SimpleRowMapper.class)

	@Mapping(target = "row", ignore = true)
	SeatDTO seatToSeatDTO(Seat seat);

	@Mapping(target = "row", ignore = true)
	Seat seatDTOToSeat(SeatDTO seatDTO);
}
