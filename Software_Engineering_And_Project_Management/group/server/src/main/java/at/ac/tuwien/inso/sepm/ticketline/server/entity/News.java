package at.ac.tuwien.inso.sepm.ticketline.server.entity;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

@Entity
public class News
{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "seq_news_id")
	@SequenceGenerator(name = "seq_news_id", sequenceName = "seq_news_id")
	private Long id;

	@Column(nullable = false, name = "published_at")
	private LocalDateTime publishedAt;

	@Column(nullable = false)
	@Size(max = 100)
	private String title;

	@Column(nullable = false, length = 10_000)
	private String text;

	@Column(nullable = false)
	private boolean isRead;

	@Column(length = 5000000)
	private byte[] byteImage;

	@OneToMany(mappedBy = "news", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private List<UserNews> userNews;

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public LocalDateTime getPublishedAt()
	{
		return publishedAt;
	}

	public void setPublishedAt(LocalDateTime publishedAt)
	{
		this.publishedAt = publishedAt;
	}

	public String getTitle()
	{
		return title;
	}

	public void setTitle(String title)
	{
		this.title = title;
	}

	public String getText()
	{
		return text;
	}

	public void setText(String text)
	{
		this.text = text;
	}

	public static NewsBuilder builder()
	{
		return new NewsBuilder();
	}

	public boolean isRead()
	{
		return isRead;
	}

	public void setRead(boolean read)
	{
		isRead = read;
	}

	public List<UserNews> getUserNews()
	{
		return userNews;
	}

	public void setUserNews(List<UserNews> userNews)
	{
		this.userNews = userNews;
	}

	public byte[] getByteImage()
	{
		return byteImage;
	}

	public void setByteImage(byte[] byteImage)
	{
		this.byteImage = byteImage;
	}

	@Override
	public boolean equals(Object o)
	{
		if(this == o)
			return true;

		if(o == null || getClass() != o.getClass())
			return false;

		News news = (News)o;
		return isRead == news.isRead && Objects.equals(id, news.id) && Objects.equals(publishedAt, news.publishedAt)
		       && Objects.equals(title, news.title) && Objects.equals(text, news.text);
	}

	@Override
	public int hashCode()
	{

		return Objects.hash(id, publishedAt, title, text, isRead);
	}

	@Override
	public String toString()
	{
		return "News{" + "id=" + id + ", publishedAt=" + publishedAt + ", title='" + title + '\'' + ", text='" + text
		       + '\'' + ", isRead=" + isRead + '}';
	}

	public static final class NewsBuilder
	{
		private Long id;
		private LocalDateTime publishedAt;
		private String title;
		private String text;
		private boolean isRead;
		private byte[] byteImage;

		private NewsBuilder()
		{
		}

		public NewsBuilder id(Long id)
		{
			this.id = id;
			return this;
		}

		public NewsBuilder publishedAt(LocalDateTime publishedAt)
		{
			this.publishedAt = publishedAt;
			return this;
		}

		public NewsBuilder title(String title)
		{
			this.title = title;
			return this;
		}

		public NewsBuilder text(String text)
		{
			this.text = text;
			return this;
		}

		public NewsBuilder isRead(boolean isRead)
		{
			this.isRead = isRead;
			return this;
		}

		public NewsBuilder byteImage(byte[] byteImage)
		{
			this.byteImage = byteImage;
			return this;
		}

		public News build()
		{
			News news = new News();
			news.setId(id);
			news.setPublishedAt(publishedAt);
			news.setTitle(title);
			news.setText(text);
			news.setRead(isRead);
			news.setByteImage(byteImage);
			return news;
		}
	}
}
