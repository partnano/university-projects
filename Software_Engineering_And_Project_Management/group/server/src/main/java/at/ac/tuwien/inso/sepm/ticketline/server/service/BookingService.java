package at.ac.tuwien.inso.sepm.ticketline.server.service;

import at.ac.tuwien.inso.sepm.ticketline.server.entity.Booking;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Customer;
import at.ac.tuwien.inso.sepm.ticketline.server.exception.BookingServiceException;
import at.ac.tuwien.inso.sepm.ticketline.server.exception.ValidationException;
import org.springframework.dao.DataAccessException;

import java.util.List;

public interface BookingService
{
	/**
	 * Create a new booking
	 *
	 * @param booking to be created
	 * @return created booking
	 */
	Booking create(Booking booking) throws ValidationException, BookingServiceException;

	/**
	 * Update a booking.
	 *
	 * @param booking to be updated
	 * @return updated booking
	 */
	Booking update(Booking booking) throws ValidationException;

	/**
	 * Retrieves booking to specific reservationNumber
	 *
	 * @param reservationNumber the number to be used to look up the bookings
	 * @return matched booking
	 */
	Booking findByReservationNumber(int reservationNumber) throws ValidationException;

	/**
	 * Retrieves all bookings of the specified customer
	 *
	 * @param customer the customer to be used to look up the bookings
	 * @return all matching bookings
	 */
	List<Booking> findAll(Customer customer) throws DataAccessException;

	/**
	 * Retrieves one booking
	 *
	 * @param id of the booking to be fetched
	 * @return the matching booking
	 * @throws DataAccessException in case something goes wrong
	 */
	Booking findOne(Long id) throws DataAccessException;
}
