package at.ac.tuwien.inso.sepm.ticketline.server.service.implementation;

import at.ac.tuwien.inso.sepm.ticketline.server.entity.*;
import at.ac.tuwien.inso.sepm.ticketline.server.exception.BookingServiceException;
import at.ac.tuwien.inso.sepm.ticketline.server.exception.NotFoundException;
import at.ac.tuwien.inso.sepm.ticketline.server.exception.ValidationException;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.BookingRepository;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.EventRepository;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.ReceiptRepository;
import at.ac.tuwien.inso.sepm.ticketline.server.service.BookingService;
import at.ac.tuwien.inso.sepm.ticketline.server.service.EventService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.temporal.ChronoField;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Service
public class SimpleBookingService implements BookingService
{
	private static final Logger LOGGER = LoggerFactory.getLogger(SimpleBookingService.class);

	private final BookingRepository repository;

	private final EventService eventService;
	private final EventRepository eventRepository;
	private final ReceiptRepository receiptRepository;

	public SimpleBookingService(BookingRepository repository, EventService eventService,
	                            EventRepository eventRepository, ReceiptRepository receiptRepository)
	{
		this.repository = repository;
		this.eventService = eventService;
		this.eventRepository = eventRepository;
		this.receiptRepository = receiptRepository;
	}

	@Override
	public Booking create(Booking booking) throws ValidationException, BookingServiceException
	{
		if(booking == null)
		{
			LOGGER.error("validation booking null");
			throw new ValidationException("booking");
		}

		if(booking.getCustomer() == null)
		{
			LOGGER.error("validation customer null");
			throw new ValidationException("booking.customer");
		}

		if(booking.getEvent() == null)
		{
			LOGGER.error("validation event null");
			throw new ValidationException("book ing.event");
		}

		if(booking instanceof SeatBooking)
		{
			LOGGER.debug("checking if seats have already been booked");
			LOGGER.debug("current booking for this event: {}", booking.getEvent().getBookings());

			final Event event = eventService.findOne(booking.getEvent().getId());

			for(Seat seat : ((SeatBooking)booking).getSeats())
			{
				for(Booking existingBooking : event.getBookings())
				{
					if(!existingBooking.isCanceled())
					{
						if(((SeatBooking)existingBooking).getSeats().contains(seat))
						{
							LOGGER.error("validation seat already booked");
							throw new ValidationException("booking.seatAlreadyBooked");
						}
					}
				}
			}
		}
		else if(booking instanceof SectionBooking)
		{
			Set<Booking> existingBookings = eventService.findOne(booking.getEvent().getId()).getBookings();
			Map<Section, Long> usedBySection = new HashMap<>();

			for(Booking existingBooking : existingBookings)
			{
				if(!existingBooking.isCanceled())
				{
					for(SectionBookingEntry entry : ((SectionBooking)existingBooking).getEntries())
					{
						Section section = entry.getSection();
						long current = usedBySection.getOrDefault(section, 0L);
						usedBySection.put(section, current + entry.getAmount());
					}
				}
			}

			for(SectionBookingEntry entry : ((SectionBooking)booking).getEntries())
			{
				Section section = entry.getSection();
				long used = usedBySection.getOrDefault(section, 0L);

				if(section.getCapacity() - used < entry.getAmount())
				{
					LOGGER.error("validation not enough space in section null");

					throw new ValidationException("booking.notEnoughSpaceInSection");
				}
			}
		}
		else
			throw new AssertionError("unhandled booking type: " + booking.getClass().getName());

		long resNumber = LocalDateTime.now().getLong(ChronoField.MICRO_OF_SECOND) + LocalDateTime.now()
		                                                                                         .getLong(
			                                                                                         ChronoField.SECOND_OF_DAY)
		                 + LocalDateTime.now().getLong(ChronoField.DAY_OF_MONTH);
		booking.setReservationNumber((int)resNumber);

		booking.setCreatedAt(LocalDateTime.now());

		LOGGER.debug("directly booked? isPaid: {}", booking.isPaid());

		if(booking.isPaid() && booking.getCancellationReceipt() == null) // in case it is directly booked
		{
			markBooked(booking);
		}

		LOGGER.debug("creating new booking ...");
		Booking result;
		try
		{
			result = repository.save(booking);
		}
		catch(Exception ex)
		{
			LOGGER.debug("... error created booking: {}", ex);
			throw new BookingServiceException(ex.getMessage());
		}

		LOGGER.debug("... successfully created booking");
		return result;
	}

	@Override
	public Booking update(Booking booking) throws ValidationException
	{
		if(booking.getEvent() == null)
		{
			booking.setEvent(eventRepository.findByBookings(booking));
		}

		if(booking == null)
		{
			throw new ValidationException("booking");
		}

		if(booking.getId() == null)
		{
			throw new ValidationException("booking.id");
		}


		if(booking.getCreatedAt() == null)
		{
			throw new ValidationException("booking.createdAt");
		}

		if(booking.getCustomer() == null)
		{
			throw new ValidationException("booking.customer");
		}

		if(booking.getEvent() == null)
		{
			throw new ValidationException("booking.event");
		}

		if(booking.isPaid() && booking.getPaymentReceipt() == null) // in case of booking a reservation
			markBooked(booking);

		if(booking.isCanceled() && booking.getCancellationReceipt() == null) // in case a booking was cancelled
			markCancelled(booking);

		LOGGER.debug("updating booking ...");
		Booking result = repository.save(booking);
		LOGGER.debug("... successfully updated booking");
		return result;
	}

	private void markBooked(Booking booking)
	{
		Receipt receipt = new Receipt();
		receipt.setCreatedAt(booking.getCreatedAt());
		receipt = receiptRepository.save(receipt);

		LOGGER.debug("created receipt: " + receipt);

		booking.setPaymentReceipt(receipt);
		booking.setBookedAt(LocalDateTime.now());
	}

	private void markCancelled(Booking booking)
	{
		Receipt receipt = new Receipt();
		receipt.setCreatedAt(booking.getCreatedAt());
		receipt = receiptRepository.save(receipt);

		booking.setCancellationReceipt(receipt);
	}

	@Override
	public Booking findByReservationNumber(int reservationNumber) throws ValidationException
	{
		if(reservationNumber < 0)
			throw new ValidationException("reservationNumber.belowZero");

		LOGGER.debug("retrieving booking by res.number");
		Booking result = repository.findByReservationNumber(reservationNumber);

		if(result == null)
			throw new NotFoundException();

		LOGGER.debug("... successfully retrieved booking");
		return result;
	}

	@Override
	public List<Booking> findAll(Customer customer) throws DataAccessException
	{
		LOGGER.debug("retrieving all bookings for customer (id: {})", customer.getId());
		List<Booking> bookings = repository.findByCustomer(customer);

		for(Booking booking : bookings)
		{
			booking.setTotalPrice(booking.totalPrice());
		}

		LOGGER.debug("... retrieved all bookings for customer (id: {}): {}", customer, bookings);
		return bookings;
	}

	public Booking findOne(Long id) throws DataAccessException
	{
		LOGGER.debug("retrieve booking with id {}", id);
		Booking booking = repository.findOne(id);
		LOGGER.debug("...retrieved {}", booking);
		return booking;
	}
}
