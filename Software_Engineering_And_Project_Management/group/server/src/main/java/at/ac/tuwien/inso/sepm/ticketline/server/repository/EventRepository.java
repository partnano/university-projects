package at.ac.tuwien.inso.sepm.ticketline.server.repository;

import at.ac.tuwien.inso.sepm.ticketline.rest.event.enums.Category;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Booking;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Event;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface EventRepository extends JpaRepository<Event, Long>, PagingAndSortingRepository<Event, Long>
{
	/**
	 * Find all events.
	 *
	 * @return list of all events
	 */
	List<Event> findAll();

	/**
	 * Find a page of events
	 *
	 * @return page of events (from all events in repo)
	 */
	Page<Event> findAllByOrderByIdDesc(Pageable pageRequest);

	/**
	 * Find certain events, determined by filters.
	 *
	 * @return list of found events
	 */
	@Query("select distinct e from Event e inner join e.eventAreas ea where "
	       + "e.artist           like %:artistFirstname% and " + "e.artist           like %:artistLastname%  and "
	       + "e.location.name    like %:locationName%    and " + "e.description      like %:description%     and "
	       + "e.location.street  like %:street%          and " + "e.location.city    like %:city%            and "
	       + "e.location.zip     like %:zip%             and " + "e.location.country like %:country%         and "
	       + "e.name             like %:name%            and "
	       + "(:category = null or e.category is :category) and "
	       + "(:duration = null or e.duration >= :duration - 30) and "
	       + "(:duration = null or e.duration <= :duration + 30) and " + "(:time     = null or e.time = :time) and "
	       + "(:price    = null or (ea.price >= :price -10.0F and ea.price <= :price +10.0F))")
	List<Event> findByFilter(@Param("name") String name, @Param("description") String description,
	                         @Param("category") Category category, @Param("duration") Integer duration,
	                         @Param("artistFirstname") String artistFirstName,
	                         @Param("artistLastname") String artistLastName,
	                         @Param("locationName") String locationName,
	                         @Param("street") String street,
	                         @Param("city") String city,
	                         @Param("zip") String zip,
	                         @Param("country") String country,
	                         @Param("time") LocalDateTime time,
	                         @Param("price") Float price);

	/**
	 * Find a page of certain events, determined by filters.
	 *
	 * @return page of found events
	 */
	@Query("select distinct e from Event e inner join e.eventAreas ea where " +
	       "e.artist           like %:artistFirstname% and " +
	       "e.artist           like %:artistLastname%  and " +
	       "e.location.name    like %:locationName%    and " +
	       "e.description      like %:description%     and " +
	       "e.location.street  like %:street%          and " +
	       "e.location.city    like %:city%            and " +
	       "e.location.zip     like %:zip%             and " +
	       "e.location.country like %:country%         and " +
	       "e.name             like %:name%            and " +
	       "(:category = null or e.category is :category) and " +
	       "(:duration = null or e.duration >= :duration - 30) and " +
	       "(:duration = null or e.duration <= :duration + 30) and " +
	       "(:time     = null or e.time = :time) and " +
	       "(:price    = null or (ea.price >= :price -10.0F and ea.price <= :price +10.0F)) " +
	       "order by e.id desc")
	Page<Event> findByFilter(@Param("name") String name,
	                         @Param("description") String description,
	                         @Param("category") Category category,
	                         @Param("duration") Integer duration,
	                         @Param("artistFirstname") String artistFirstName,
	                         @Param("artistLastname") String artistLastName,
	                         @Param("locationName") String locationName,
	                         @Param("street") String street,
	                         @Param("city") String city,
	                         @Param("zip") String zip,
	                         @Param("country") String country,
	                         @Param("time") LocalDateTime time,
	                         @Param("price") Float price,
	                         Pageable pageRequest);

	Event findByBookings(Booking booking);
}
