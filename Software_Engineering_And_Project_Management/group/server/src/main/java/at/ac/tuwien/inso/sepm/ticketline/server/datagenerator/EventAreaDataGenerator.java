package at.ac.tuwien.inso.sepm.ticketline.server.datagenerator;

import at.ac.tuwien.inso.sepm.ticketline.server.entity.Area;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Event;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.EventArea;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.EventRepository;
import com.github.javafaker.Faker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Profile("generateData")
@Component("eventAreaDataGenerator")
@DependsOn({"eventDataGenerator", "areaDataGenerator"})
public class EventAreaDataGenerator
{
	private static final Logger LOGGER = LoggerFactory.getLogger(EventAreaDataGenerator.class);

	private final EventRepository eventRepository;
	private final Faker faker;

	public EventAreaDataGenerator(EventRepository eventRepository)
	{
		this.eventRepository = eventRepository;
		faker = new Faker();
	}

	@PostConstruct
	private void generateEventAreas()
	{
		List<Event> events = eventRepository.findAll();

		for(Event event : events)
		{
			if(event.getEventAreas().size() > 0)
				continue;

			Set<EventArea> eventAreas = new HashSet<>();
			for(Area area : event.getLocation().getAreas())
			{
				eventAreas.add(EventArea.builder()
				                        .area(area)
				                        .event(event)
				                        .price((float)faker.number().randomDouble(2, 10, 500))
				                        .build());
			}
			event.setEventAreas(eventAreas);
		}
		eventRepository.save(events);
	}
}
