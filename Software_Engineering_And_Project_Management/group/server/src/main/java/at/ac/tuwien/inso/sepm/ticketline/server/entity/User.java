package at.ac.tuwien.inso.sepm.ticketline.server.entity;

import javax.persistence.*;
import java.util.List;

@Entity
public class User
{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "seq_user_id")
	@SequenceGenerator(name = "seq_user_id", sequenceName = "seq_user_id")
	private Long id;

	@Column(nullable = false, unique = true)
	private String email;

	@Column
	private String firstName;

	@Column
	private String lastName;

	@Column(nullable = false)
	private String passwordHash;

	@Column(nullable = false)
	private String passwordSalt;

	@Column(nullable = false)
	private boolean isAdmin;

	@Column(nullable = false)
	private boolean isLocked;

	@Column(nullable = false)
	private int invalidLogins;

	@OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private List<UserNews> userNews;

	public String getPasswordHash()
	{
		return passwordHash;
	}

	public void setPasswordHash(String passwordHash)
	{
		this.passwordHash = passwordHash;
	}

	public String getPasswordSalt()
	{
		return passwordSalt;
	}

	public void setPasswordSalt(String passwordSalt)
	{
		this.passwordSalt = passwordSalt;
	}

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public String getFirstName()
	{
		return firstName;
	}

	public void setFirstName(String firstName)
	{
		this.firstName = firstName;
	}

	public String getLastName()
	{
		return lastName;
	}

	public void setLastName(String lastName)
	{
		this.lastName = lastName;
	}

	public boolean isAdmin()
	{
		return isAdmin;
	}

	public void setAdmin(boolean admin)
	{
		isAdmin = admin;
	}

	public boolean isLocked()
	{
		return isLocked;
	}

	public void setLocked(boolean locked)
	{
		isLocked = locked;
	}

	public String getEmail()
	{
		return email;
	}

	public void setEmail(String email)
	{
		this.email = email;
	}

	public int getInvalidLogins()
	{
		return invalidLogins;
	}

	public void setInvalidLogins(int invalidLogins)
	{
		this.invalidLogins = invalidLogins;
	}

	public List<UserNews> getUserNews()
	{
		return userNews;
	}

	public void setUserNews(List<UserNews> userNews)
	{
		this.userNews = userNews;
	}

	@Override
	public boolean equals(Object o)
	{
		if(this == o)
			return true;
		if(o == null || getClass() != o.getClass())
			return false;

		User user = (User)o;

		return id.equals(user.id);
	}

	@Override
	public int hashCode()
	{
		return id.hashCode();
	}

	@Override
	public String toString()
	{
		return "User{" + "id=" + id + ", firstName='" + firstName + '\'' + ", lastName='" + lastName + '\''
		       + ", passwordHash='" + passwordHash + '\'' + ", isAdmin=" + isAdmin + ", isLocked=" + isLocked
		       + ", email='" + email + '}';
	}
}
