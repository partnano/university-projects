package at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.booking;

import at.ac.tuwien.inso.sepm.ticketline.rest.booking.SeatBookingDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventAreaDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.location.RowDTO;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.*;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.location.GenericAreaMapper;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.SeatRepository;
import org.mapstruct.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Mapper(componentModel = "spring", uses = {GenericAreaMapper.class, GenericBookingMapper.class})
public abstract class SeatBookingMapper
{
	@Autowired
	private SeatRepository seatRepository;

	public abstract SeatBookingDTO seatBookingToSeatBookingDTO(SeatBooking seatBooking);

	public abstract List<SeatBookingDTO> seatBookingsToSeatBookingDTOs(List<SeatBooking> seatBookings);

	public abstract SeatBooking seatBookingDTOToSeatBooking(SeatBookingDTO seatBooking);

	public abstract List<SeatBooking> seatBookingsDTOsToSeatBookings(List<SeatBookingDTO> seatBookings);

	@Mapping(target = "bookings", ignore = true)
	public abstract EventDTO eventToEventDTO(Event event);

	//	@Mapping(target = "bookings", ignore = true)
	//	public abstract Event eventDTOtoEvent(EventDTO event);

	@Mapping(target = "event", ignore = true)
	public abstract EventAreaDTO eventAreaToEventAreaDTO(EventArea eventArea);

	//	@Mapping(target = "event", ignore = true)
	//	public abstract EventArea eventAreaDTOToEventArea(EventAreaDTO eventArea);

	@Mappings({@Mapping(target = "seats", ignore = true), @Mapping(target = "location", ignore = true)})
	public abstract RowDTO rowToRowDTO(Row row);

	@AfterMapping
	public void replaceSeats(SeatBookingDTO seatBookingDTO, @MappingTarget SeatBooking seatBooking)
	{
		if(seatBooking.getSeats() != null)
		{
			Set<Seat> newSeats = new HashSet<>();
			for(Seat seat : seatBooking.getSeats())
			{
				newSeats.add(seatRepository.findOne(seat.getId()));
			}
			seatBooking.setSeats(newSeats);
		}
	}
}
