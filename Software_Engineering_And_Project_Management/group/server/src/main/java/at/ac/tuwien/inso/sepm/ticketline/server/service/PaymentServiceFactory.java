package at.ac.tuwien.inso.sepm.ticketline.server.service;

import at.ac.tuwien.inso.sepm.ticketline.rest.payment.PaymentProvider;
import at.ac.tuwien.inso.sepm.ticketline.server.service.implementation.CashPaymentService;
import at.ac.tuwien.inso.sepm.ticketline.server.service.implementation.StripePaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PaymentServiceFactory
{
	@Autowired
	private StripePaymentService stripePaymentService;

	@Autowired
	private CashPaymentService cashPaymentService;

	public PaymentService getService(PaymentProvider paymentProvider)
	{
		switch(paymentProvider)
		{
		case STRIPE:
			return stripePaymentService;
		case CASH:
			return cashPaymentService;
		default:
			//should not happen
			return null;
		}
	}
}
