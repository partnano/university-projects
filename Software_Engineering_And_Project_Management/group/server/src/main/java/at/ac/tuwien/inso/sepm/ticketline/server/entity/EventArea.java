package at.ac.tuwien.inso.sepm.ticketline.server.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(uniqueConstraints = {@UniqueConstraint(columnNames = {"fk_area_id", "fk_event_id", "price"})})
public class EventArea
{
	@Id
	@GeneratedValue
	private Long id;

	@ManyToOne
	@JoinColumn(name = "fk_area_id")
	private Area area;

	@ManyToOne
	@JoinColumn(name = "fk_event_id")
	private Event event;

	@Column(nullable = false)
	private Float price;

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public Area getArea()
	{
		return area;
	}

	public void setArea(Area area)
	{
		this.area = area;
	}

	public Event getEvent()
	{
		return event;
	}

	public void setEvent(Event event)
	{
		this.event = event;
	}

	public Float getPrice()
	{
		return price;
	}

	public void setPrice(Float price)
	{
		this.price = price;
	}

	public static EventAreaBuilder builder()
	{
		return new EventAreaBuilder();
	}

	@Override
	public boolean equals(Object o)
	{
		if(this == o)
			return true;

		if(o == null || getClass() != o.getClass())
			return false;

		EventArea eventArea = (EventArea)o;
		return Objects.equals(id, eventArea.id)
			&& Objects.equals(area, eventArea.area)
			&& Objects.equals(event, eventArea.event)
			&& Objects.equals(price, eventArea.price);
	}

	@Override
	public String toString()
	{
		return "EventArea{" +
			"id=" + id +
			", area=" + area +
			", event=" + event +
			", price=" + price +
			'}';
	}

	public static final class EventAreaBuilder
	{
		private Long id;
		private Area area;
		private Event event;
		private Float price;

		private EventAreaBuilder()
		{
		}

		public EventAreaBuilder id(Long id)
		{
			this.id = id;
			return this;
		}

		public EventAreaBuilder area(Area area)
		{
			this.area = area;
			return this;
		}

		public EventAreaBuilder event(Event event)
		{
			this.event = event;
			return this;
		}

		public EventAreaBuilder price(Float price)
		{
			this.price = price;
			return this;
		}

		public EventArea build()
		{
			EventArea eventArea = new EventArea();
			eventArea.setId(id);
			eventArea.setArea(area);
			eventArea.setEvent(event);
			eventArea.setPrice(price);
			return eventArea;
		}
	}
}
