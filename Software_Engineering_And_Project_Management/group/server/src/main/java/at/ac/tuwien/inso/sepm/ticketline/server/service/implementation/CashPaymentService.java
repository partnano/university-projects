package at.ac.tuwien.inso.sepm.ticketline.server.service.implementation;

import at.ac.tuwien.inso.sepm.ticketline.rest.payment.CreditCardDTO;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Booking;
import at.ac.tuwien.inso.sepm.ticketline.server.service.PaymentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service("payment.cash")
public class CashPaymentService implements PaymentService
{
	private static final Logger LOGGER = LoggerFactory.getLogger(CashPaymentService.class);

	@Override
	public void pay(Booking booking, CreditCardDTO creditCardDTO)
	{
		LOGGER.info("paying {}€ for {}", booking);
	}

	@Override
	public void refund(Booking booking)
	{
		LOGGER.info("refunding {}", booking);
	}
}
