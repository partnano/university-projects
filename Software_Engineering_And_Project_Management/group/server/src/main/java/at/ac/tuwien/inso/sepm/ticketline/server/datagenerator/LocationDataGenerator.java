package at.ac.tuwien.inso.sepm.ticketline.server.datagenerator;

import at.ac.tuwien.inso.sepm.ticketline.server.entity.Location;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.LocationRepository;
import com.github.javafaker.Faker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Profile("generateData")
@Component("locationDataGenerator")
public class LocationDataGenerator
{
	private static final Logger LOGGER = LoggerFactory.getLogger(LocationDataGenerator.class);
	private static final int NUMBER_OF_LOCATIONS_TO_GENERATE = 2;

	private final LocationRepository locationRepository;
	private final Faker faker = new Faker();

	public LocationDataGenerator(LocationRepository locationRepository)
	{
		this.locationRepository = locationRepository;
	}

	@PostConstruct
	private void generateLocations()
	{
		if(locationRepository.count() > 0)
		{
			LOGGER.info("locations already generated");
			return;
		}

		LOGGER.info("generating {} location entries", NUMBER_OF_LOCATIONS_TO_GENERATE);

		for(int i = 0; i < NUMBER_OF_LOCATIONS_TO_GENERATE; i++)
		{
			Location location = Location.builder()
			                            .name(faker.lordOfTheRings().location())
			                            .street(faker.address().streetName())
			                            .zip(faker.address().zipCode())
			                            .country(faker.address().country())
			                            .city(faker.address().city())
			                            .build();

			LOGGER.debug("saving location {}", location);
			locationRepository.save(location);
		}
	}
}
