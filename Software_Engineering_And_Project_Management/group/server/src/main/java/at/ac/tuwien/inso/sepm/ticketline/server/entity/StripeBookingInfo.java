package at.ac.tuwien.inso.sepm.ticketline.server.entity;

import javax.persistence.*;

@Entity
public class StripeBookingInfo
{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "seq_stripe_booking_info_id")
	@SequenceGenerator(name = "seq_stripe_booking_info_id", sequenceName = "seq_stripe_booking_info_id")
	private Long id;

	@Column(nullable = false)
	private String chargeId;

	@OneToOne(fetch = FetchType.EAGER)
	private Booking booking;

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public String getChargeId()
	{
		return chargeId;
	}

	public void setChargeId(String chargeId)
	{
		this.chargeId = chargeId;
	}

	public Booking getBooking()
	{
		return booking;
	}

	public void setBooking(Booking booking)
	{
		this.booking = booking;
	}
}
