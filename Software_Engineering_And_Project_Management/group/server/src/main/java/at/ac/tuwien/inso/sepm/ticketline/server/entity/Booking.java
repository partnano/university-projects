package at.ac.tuwien.inso.sepm.ticketline.server.entity;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class Booking
{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "seq_bookings_id")
	@SequenceGenerator(name = "seq_bookings_id", sequenceName = "seq_bookings_id")
	private Long id;

	@Column(nullable = false)
	private int reservationNumber;

	@Column(nullable = false)
	private boolean isPaid;

	@Column(nullable = false)
	private boolean isCanceled;

	@Column(nullable = false)
	private LocalDateTime createdAt;

	@Column
	private LocalDateTime bookedAt;

	@Column
	private String paymentMethod;

	@ManyToOne(fetch = FetchType.EAGER)
	private Customer customer;

	@ManyToOne(fetch = FetchType.EAGER)
	private Event event;

	@OneToOne
	private Receipt paymentReceipt;

	@OneToOne
	private Receipt cancellationReceipt;
	// not a column
	private Float totalPrice;

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public int getReservationNumber()
	{
		return reservationNumber;
	}

	public void setReservationNumber(int reservationNumber)
	{
		this.reservationNumber = reservationNumber;
	}

	public boolean isPaid()
	{
		return isPaid;
	}

	public void setPaid(boolean paid)
	{
		isPaid = paid;
	}

	public boolean isCanceled()
	{
		return isCanceled;
	}

	public void setCanceled(boolean isCanceled)
	{
		this.isCanceled = isCanceled;
	}

	public LocalDateTime getCreatedAt()
	{
		return createdAt;
	}

	public void setCreatedAt(LocalDateTime createdAt)
	{
		this.createdAt = createdAt;
	}

	public LocalDateTime getBookedAt()
	{
		return bookedAt;
	}

	public void setBookedAt(LocalDateTime bookedAt)
	{
		this.bookedAt = bookedAt;
	}

	public String getPaymentMethod()
	{
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod)
	{
		this.paymentMethod = paymentMethod;
	}

	public Event getEvent()
	{
		return event;
	}

	public void setEvent(Event event)
	{
		this.event = event;
	}

	public Customer getCustomer()
	{
		return customer;
	}

	public void setCustomer(Customer customer)
	{
		this.customer = customer;
	}

	public Receipt getPaymentReceipt()
	{
		return paymentReceipt;
	}

	public void setPaymentReceipt(Receipt paymentReceipt)
	{
		this.paymentReceipt = paymentReceipt;
	}

	public Receipt getCancellationReceipt()
	{
		return cancellationReceipt;
	}

	public void setCancellationReceipt(Receipt cancellationReceipt)
	{
		this.cancellationReceipt = cancellationReceipt;
	}

	public Float getTotalPrice()
	{
		return totalPrice;
	}

	public void setTotalPrice(Float totalPrice)
	{
		this.totalPrice = totalPrice;
	}

	// TOTAL PRICE SHENANIGANS
	protected Float priceForArea(Area area)
	{
		if(area == null)
			return 0.0f;

		return priceForArea(area, event);
	}

	private static Float priceForArea(Area area, Event event)
	{
		if(event == null)
			return 0.0f;

		for(EventArea eventArea : event.getEventAreas())
		{
			if(eventArea.getArea().getId().equals(area.getId()))
			{
				return eventArea.getPrice();
			}
		}

		throw new RuntimeException(
				"no price defined for area with id " + area.getId() + " in event with id " + event.getId()
				+ "\navailable event areas: " + event.getEventAreas());
	}

	public Float totalPrice() { return 0.0f; }

	@Override
	public boolean equals(Object o)
	{
		if(this == o)
		{
			return true;
		}

		if(o == null || getClass() != o.getClass())
		{
			return false;
		}

		Booking booking = (Booking)o;

		return Objects.equals(id, booking.id);
	}

	@Override
	public String toString()
	{

		String eventId = "not set";
		if(event != null)
		{
			eventId = "id=" + event.getId();
		}

		String customerId = "not set";
		if(customer != null)
		{
			customerId = "id=" + customer.getId();
		}

		return "Booking{" + "id=" + id + ", event=" + eventId + ", customer=" + customerId + ", reservationNumber="
		       + reservationNumber + ", isPaid=" + isPaid + ", isCanceled="
		       + isCanceled + ", createdAt=" + createdAt + ", bookedAt=" + bookedAt + '}';
	}
}
