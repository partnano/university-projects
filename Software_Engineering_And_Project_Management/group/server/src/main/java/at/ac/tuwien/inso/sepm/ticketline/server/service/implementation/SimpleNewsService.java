package at.ac.tuwien.inso.sepm.ticketline.server.service.implementation;

import at.ac.tuwien.inso.sepm.ticketline.rest.news.SimpleNewsDTO;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.News;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.User;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.UserNews;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.news.NewsMapper;
import at.ac.tuwien.inso.sepm.ticketline.server.exception.NotFoundException;
import at.ac.tuwien.inso.sepm.ticketline.server.exception.ValidationException;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.NewsRepository;
import at.ac.tuwien.inso.sepm.ticketline.server.service.NewsService;
import at.ac.tuwien.inso.sepm.ticketline.server.service.UserService;
import at.ac.tuwien.inso.sepm.ticketline.server.validation.NewsValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Service
public class SimpleNewsService implements NewsService
{
	private final NewsRepository repository;
	private final NewsMapper mapper;

	private final UserService userService;

	private static final Logger LOGGER = LoggerFactory.getLogger(SimpleHeaderTokenAuthenticationService.class);

	public SimpleNewsService(NewsRepository repository, NewsMapper mapper, UserService userService)
	{
		this.repository = repository;
		this.mapper = mapper;
		this.userService = userService;
	}

	@Override
	public List<News> findAll()
	{
		LOGGER.debug("retrieving all news...");
		List<News> allNews = repository.findAllByOrderByPublishedAtDesc();
		LOGGER.debug("...retrieved all news");
		return allNews;
	}

	@Override
	public Page<SimpleNewsDTO> findAll(Pageable pageable, String username)
	{
		LOGGER.debug("retrieving a page of all news...");
		Page<News> pageOfNews = repository.findAllByOrderByPublishedAtDesc(pageable);
		LOGGER.debug("...retrieved a page of all news");

		return convert(pageOfNews, pageable, username);
	}

	@Override
	public Page<SimpleNewsDTO> findUnread(Pageable pageable, String username)
	{
		LOGGER.debug("retrieving a page of unread news...");

		// username == email
		User user = userService.findByEmail(username);

		if(user.getUserNews() == null)
			return this.findAll(pageable, username);

		List<News> allNews = repository.findAllByOrderByPublishedAtDesc();
		List<News> readNews = new LinkedList<>();

		for(UserNews userNews : user.getUserNews())
		{
			readNews.add(userNews.getNews());
		}

		allNews.removeAll(readNews);

		int start = pageable.getOffset();
		int end = (start + pageable.getPageSize()) > allNews.size() ? allNews.size() : (start + pageable.getPageSize());

		Page<SimpleNewsDTO> pageOfUnreads = new PageImpl<>(mapper.newsToSimpleNewsDTO(allNews.subList(start, end)),
		                                                   pageable,
		                                                   allNews.size());

		LOGGER.debug("...retrieved a page of unread news");

		return pageOfUnreads;
	}

	@Override
	public News findOne(Long id)
	{
		LOGGER.debug("retrieving news with id {}...", id);
		News news = repository.findOneById(id).orElseThrow(NotFoundException::new);
		LOGGER.debug("...retrieved news: {}", news);
		return news;
	}

	@Override
	public News publishNews(News news) throws ValidationException
	{
		NewsValidator.validateNews(news);

		LOGGER.debug("publishing news: {}", news);
		news.setPublishedAt(LocalDateTime.now());
		News publishedNews = repository.save(news);
		LOGGER.debug("successfully published news: {}", publishedNews);
		return publishedNews;
	}

	@Override
	public News update(News news, String username)
	{
		// username == email
		User user = userService.findByEmail(username);
		News serverNews = repository.findOne(news.getId());

		List<UserNews> userNews = serverNews.getUserNews();

		boolean add = true;
		for(UserNews un : userNews)
		{
			if(un.getNews().equals(serverNews) && un.getUser().equals(user))
			{
				add = false;
				break;
			}
		}

		if(add)
		{
			UserNews newUserNews = new UserNews();
			newUserNews.setHasRead(true);
			newUserNews.setUser(user);
			newUserNews.setNews(serverNews);

			userNews.add(newUserNews);
		}

		LOGGER.debug("Trying to update news entry");
		repository.save(serverNews);

		return serverNews;
	}

	private Page<SimpleNewsDTO> convert(Page<News> page, Pageable pageRequest, String username)
	{
		if(page == null)
			throw new NotFoundException();

		List<News> news = page.getContent();

		for(News n : news)
		{
			// default not read
			n.setRead(false);

			if(n.getUserNews() == null)
			{
				n.setRead(false);
			}
			else
			{
				for(UserNews un : n.getUserNews())
				{
					// if user in connecting table, then it's read
					// probably not all too performant .. but oh well
					if(username.equals(un.getUser().getEmail()))
					{
						n.setRead(un.isHasRead());
						break;
					}
				}
			}
		}

		List<SimpleNewsDTO> newsDTOs = mapper.newsToSimpleNewsDTO(news);

		Page<SimpleNewsDTO> pageOfNewsDTOs = new PageImpl<>(newsDTOs, pageRequest, page.getTotalElements());

		// don't think this can happen
		if(pageRequest.getPageNumber() > pageOfNewsDTOs.getTotalPages())
			throw new NotFoundException();

		LOGGER.debug("... sending page of events");

		return pageOfNewsDTOs;
	}
}
