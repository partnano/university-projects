package at.ac.tuwien.inso.sepm.ticketline.server.datagenerator;

import at.ac.tuwien.inso.sepm.ticketline.rest.event.enums.Category;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Event;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Location;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.EventRepository;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.LocationRepository;
import com.github.javafaker.Faker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.concurrent.TimeUnit;

@Profile("generateData")
@Component("eventDataGenerator")
@DependsOn("locationDataGenerator")
public class EventDataGenerator
{
	private static final Logger LOGGER = LoggerFactory.getLogger(EventDataGenerator.class);

	private static final int NUMBER_OF_EVENTS_TO_GENERATE = 1000;

	private final EventRepository eventRepository;
	private final LocationRepository locationRepository;
	private final Faker faker = new Faker();

	public EventDataGenerator(EventRepository eventRepository, LocationRepository locationRepository)
	{
		this.eventRepository = eventRepository;
		this.locationRepository = locationRepository;
	}

	@PostConstruct
	private void generateEvents()
	{
		if(eventRepository.count() > 0)
		{
			LOGGER.info("events already generated");
			return;
		}

		LOGGER.info("generating {} event entries", NUMBER_OF_EVENTS_TO_GENERATE);

		for(int i = 0; i < NUMBER_OF_EVENTS_TO_GENERATE; i++)
		{
			Location location = locationRepository.findAll()
			                                      .get((int)faker.number()
			                                                     .numberBetween(0, locationRepository.count()));

			LocalDateTime time;

			// use past and future dates
			if(i < NUMBER_OF_EVENTS_TO_GENERATE / 2)
				time = LocalDateTime.ofInstant(faker.date().past(365 * 3, TimeUnit.MINUTES).toInstant(),
				                               ZoneId.systemDefault()).withNano(0).withSecond(0);
			else
				time = LocalDateTime.ofInstant(faker.date().future(365 * 3, TimeUnit.MINUTES).toInstant(),
				                               ZoneId.systemDefault()).withNano(0).withSecond(0);

			// use both categories
			Category category = Category.CONCERT;
			if(faker.number().numberBetween(0, 1) == 1)
				category = Category.MUSICAL;

			Event event = Event.builder()
			                   .name(faker.book().title())
			                   .description(faker.lorem().sentence(faker.number().numberBetween(5, 10)))
			                   .duration(faker.number().numberBetween(30, 180))
			                   .artist(faker.name().fullName())
			                   .time(time)
			                   .location(location)
			                   .category(category)
			                   .build();

			eventRepository.save(event);
		}
	}
}
