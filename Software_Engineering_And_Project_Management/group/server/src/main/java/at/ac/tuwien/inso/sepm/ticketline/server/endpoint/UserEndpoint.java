package at.ac.tuwien.inso.sepm.ticketline.server.endpoint;

import at.ac.tuwien.inso.sepm.ticketline.rest.user.UserDTO;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.user.UserMapper;
import at.ac.tuwien.inso.sepm.ticketline.server.exception.ValidationException;
import at.ac.tuwien.inso.sepm.ticketline.server.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@RestController
@RequestMapping("/users")
@Api("Users")
public class UserEndpoint
{
	private static final Logger LOGGER = LoggerFactory.getLogger(UserEndpoint.class);

	private final UserService service;
	private final UserMapper mapper;

	public UserEndpoint(UserService service, UserMapper mapper)
	{
		this.service = service;
		this.mapper = mapper;
	}

	@PreAuthorize("hasRole('ADMIN')")
	@RequestMapping(method = RequestMethod.GET, params = {"page", "size"})
	@ApiOperation("Get a list of all User entries")
	public Page<UserDTO> findAll(@RequestParam int page, @RequestParam int size)
	{
		LOGGER.debug("all Users requested...");
		Page<UserDTO> allUsers = service.findAll(new PageRequest(page, size));
		LOGGER.debug("...sending all Users");
		return allUsers;
	}

	@PreAuthorize("hasRole('ADMIN')")
	@RequestMapping(method = RequestMethod.POST)
	@ApiOperation("Create a new user entry")
	public UserDTO create(@RequestBody UserDTO userDTO) throws ValidationException
	{
		LOGGER.info("creating user: {}", userDTO);
		UserDTO createdUser = mapper.userToUserDTO(service.create(mapper.userDTOToUser(userDTO)));
		LOGGER.info("sending created user: {}", createdUser);
		return createdUser;
	}

	@PreAuthorize("hasRole('ADMIN')")
	@RequestMapping(value = "/{id}", method = RequestMethod.POST)
	@ApiOperation("Update an existing user")
	public UserDTO update(@RequestBody UserDTO userDTO, Principal user) throws ValidationException
	{
		LOGGER.info("updating user: {}", userDTO);
		UserDTO updatedUser = mapper.userToUserDTO(service.update(mapper.userDTOToUser(userDTO),
		                                                          user.getName()));
		LOGGER.info("sending updated user: {}", updatedUser);
		return updatedUser;
	}

	@PreAuthorize("hasRole('ADMIN')")
	@RequestMapping(value = "/{id}/resetpw", method = RequestMethod.POST)
	@ApiOperation("Reset a users' password")
	public void resetPassword(@PathVariable Long id) throws ValidationException
	{
		LOGGER.info("resetting password for user {}", id);
		service.resetPassword(id);
	}
}
