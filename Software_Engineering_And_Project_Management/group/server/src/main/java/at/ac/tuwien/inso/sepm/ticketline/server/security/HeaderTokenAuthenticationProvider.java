package at.ac.tuwien.inso.sepm.ticketline.server.security;

import at.ac.tuwien.inso.sepm.ticketline.server.service.HeaderTokenAuthenticationService;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

@Component
public class HeaderTokenAuthenticationProvider implements AuthenticationProvider
{
	private final HeaderTokenAuthenticationService service;

	public HeaderTokenAuthenticationProvider(HeaderTokenAuthenticationService service)
	{
		Assert.notNull(service, "service cannot be null");
		this.service = service;
	}

	@Override
	public Authentication authenticate(Authentication authentication)
	{
		String headerToken = (String)authentication.getCredentials();
		User user = service.authenticate(headerToken);
		AuthenticationHeaderToken token = new AuthenticationHeaderToken(user,
		                                                                headerToken,
		                                                                user.getAuthorities());
		token.setDetails(authentication.getDetails());
		return token;
	}

	@Override
	public boolean supports(Class<?> authentication)
	{
		return AuthenticationHeaderToken.class.isAssignableFrom(authentication);
	}
}
