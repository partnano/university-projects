package at.ac.tuwien.inso.sepm.ticketline.server.endpoint;

import at.ac.tuwien.inso.sepm.ticketline.rest.customer.CustomerDTO;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.customer.CustomerMapper;
import at.ac.tuwien.inso.sepm.ticketline.server.exception.ValidationException;
import at.ac.tuwien.inso.sepm.ticketline.server.service.CustomerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/customers")
@Api("customers")
public class CustomerEndpoint
{
	private static final Logger LOGGER = LoggerFactory.getLogger(CustomerEndpoint.class);

	private final CustomerService service;
	private final CustomerMapper mapper;

	public CustomerEndpoint(CustomerService service, CustomerMapper mapper)
	{
		this.service = service;
		this.mapper = mapper;
	}

	@RequestMapping(method = RequestMethod.GET)
	@ApiOperation("Get a list of all customer entries")
	public List<CustomerDTO> findAll()
	{
		LOGGER.debug("all customers requested...");
		List<CustomerDTO> allCustomers = mapper.customersToCustomerDTOs(service.findAll());
		LOGGER.debug("...sending all customers");
		return allCustomers;
	}

	@RequestMapping(params = {"page", "size"}, method = RequestMethod.GET)
	@ApiOperation("Get a page of all customer entries")
	public Page<CustomerDTO> findAll(@RequestParam("page") int page, @RequestParam("size") int size)
	{
		LOGGER.debug("page of all customers requested...");
		Page<CustomerDTO> pageOfCustomers = service.findAll(new PageRequest(page, size));
		LOGGER.debug("... sending page");
		return pageOfCustomers;
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@ApiOperation("Get a single customer entry")
	public CustomerDTO find(@PathVariable Long id) throws ValidationException
	{
		LOGGER.debug("customer with id {} requested", id);
		CustomerDTO customer = mapper.customerToCustomerDTO(service.findOne(id));
		LOGGER.debug("sending customer with id {}:", id, customer);
		return customer;
	}

	@RequestMapping(method = RequestMethod.POST)
	@ApiOperation("Create a new customer entry")
	public CustomerDTO create(@RequestBody CustomerDTO customer) throws ValidationException
	{
		LOGGER.info("creating customer: {}", customer);
		CustomerDTO createdCustomer = mapper.customerToCustomerDTO(service.create(mapper.customerDTOToCustomer(
			customer)));
		LOGGER.info("sending created customer: {}", createdCustomer);
		return createdCustomer;
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	@ApiOperation("Update a customer entry")
	public CustomerDTO update(@PathVariable Long id, @RequestBody CustomerDTO customer) throws ValidationException
	{
		LOGGER.info("updating customer with id {}: ", id, customer);
		customer.setId(id);
		CustomerDTO updatedCustomer = mapper.customerToCustomerDTO(service.update(mapper.customerDTOToCustomer(
			customer)));
		LOGGER.info("sending updated customer: {}", updatedCustomer);
		return updatedCustomer;
	}
}
