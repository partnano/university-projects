package at.ac.tuwien.inso.sepm.ticketline.server.entity;

import javax.persistence.*;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "seatbookings")
public class SeatBooking extends Booking
{
	@ManyToMany
	@JoinTable(name = "seat_seatbooking",
	           joinColumns = @JoinColumn(name = "booking_id", referencedColumnName = "id"),
	           inverseJoinColumns = @JoinColumn(name = "seat_id", referencedColumnName = "id"))
	private Set<Seat> seats;

	public Set<Seat> getSeats()
	{
		return seats;
	}

	public void setSeats(Set<Seat> seats)
	{
		this.seats = seats;
	}

	@Override
	public Float totalPrice()
	{
		float sum = 0.0f;

		if (seats == null)
			return sum;

		for(Seat seat : seats)
		{
			Area row = seat.getRow();
			Float rowPrice = priceForArea(row);

			sum += rowPrice;
		}

		return sum;
	}

	@Override
	public boolean equals(Object o)
	{
		if(this == o)
			return true;

		if(o == null || getClass() != o.getClass())
			return false;

		if(!super.equals(o))
			return false;

		SeatBooking that = (SeatBooking)o;
		return Objects.equals(seats, that.seats);
	}

	@Override
	public String toString()
	{
		return "SeatBooking{" +
			"seats=" + seats +
			'}';
	}
}
