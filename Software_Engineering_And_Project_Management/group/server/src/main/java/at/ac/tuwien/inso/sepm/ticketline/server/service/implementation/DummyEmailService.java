package at.ac.tuwien.inso.sepm.ticketline.server.service.implementation;

import at.ac.tuwien.inso.sepm.ticketline.server.exception.ValidationException;
import at.ac.tuwien.inso.sepm.ticketline.server.service.EmailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DummyEmailService implements EmailService
{
	private final Logger LOGGER = LoggerFactory.getLogger(DummyEmailService.class);

	@Override
	public void sendEmail(String sender, List<String> recipients, String subject, String content) throws
	                                                                                              DataAccessException,
	                                                                                              ValidationException
	{
		if(sender == null || sender.isEmpty())
			throw new ValidationException("sender");

		if(recipients == null || recipients.isEmpty())
			throw new ValidationException("recipients");

		if(subject == null)
			throw new ValidationException("subject");

		if(content == null)
			throw new ValidationException("content");

		LOGGER.info("sending new email:");
		LOGGER.info("\tsender={}", sender);
		LOGGER.info("\trecipients={}", recipients);
		LOGGER.info("\tsubject={}", subject);
		LOGGER.info("\tcontent={}", content);
	}
}
