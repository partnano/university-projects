package at.ac.tuwien.inso.sepm.ticketline.server.service;

import at.ac.tuwien.inso.sepm.ticketline.server.exception.ValidationException;
import org.springframework.dao.DataAccessException;

import java.util.List;

public interface EmailService
{
	/**
	 * sends an email
	 *
	 * @param sender     the address to be shown as the sender
	 * @param recipients a list of all recipients
	 * @param subject    the subject of the email
	 * @param content    the content
	 * @throws DataAccessException if sending fails for whatever reason
	 * @throws ValidationException if validation fails
	 */
	void sendEmail(String sender, List<String> recipients, String subject, String content) throws DataAccessException,
	                                                                                              ValidationException;
}
