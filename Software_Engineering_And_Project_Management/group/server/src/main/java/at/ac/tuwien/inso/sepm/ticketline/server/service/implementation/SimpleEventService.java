package at.ac.tuwien.inso.sepm.ticketline.server.service.implementation;

import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventFilterDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.enums.Category;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.*;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.event.EventMapper;
import at.ac.tuwien.inso.sepm.ticketline.server.exception.NotFoundException;
import at.ac.tuwien.inso.sepm.ticketline.server.exception.ValidationException;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.EventRepository;
import at.ac.tuwien.inso.sepm.ticketline.server.service.EventService;
import javafx.util.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class SimpleEventService implements EventService
{
	private static final Logger LOGGER = LoggerFactory.getLogger(SimpleCustomerService.class);

	private final EventRepository repository;
	private final EventMapper mapper;

	public SimpleEventService(EventRepository repository, EventMapper mapper)
	{
		this.repository = repository;
		this.mapper = mapper;
	}

	@Override
	public List<Event> findAll()
	{
		LOGGER.debug("retrieving all events...");
		List<Event> all = repository.findAll();
		LOGGER.debug("...retrieved all events");
		return all;
	}

	@Override
	public Page<EventDTO> findPageFromAll(Pageable pageRequest)
	{
		LOGGER.debug("retrieving a page of all events...");
		Page<Event> pageOfEvents = repository.findAllByOrderByIdDesc(pageRequest);
		LOGGER.debug("...retrieved a page of all events");

		return convert(pageOfEvents, pageRequest);
	}

	@Override
	public Event findOne(Long id) throws ValidationException
	{
		if(id == null)
			throw new ValidationException("id");

		LOGGER.debug("retrieving event with id {}...", id);
		Event event = repository.findOne(id);

		if(event == null)
			throw new NotFoundException();

		LOGGER.debug("...retrieved event");
		return event;
	}

	@Override
	public List<Event> findByFilter(EventFilterDTO filter)
	{
		LOGGER.debug("retrieving filtered events with filter {} ...", filter);

		List<Event> some = repository.findByFilter(filter.getName() == null ? "" : filter.getName(),
		                                           filter.getDescription() == null ? "" : filter.getDescription(),
		                                           filter.getCategory(),
		                                           filter.getDuration(),
		                                           filter.getArtistFirstname() == null ? "" : filter.getArtistFirstname(),
		                                           filter.getArtistLastname() == null ? "" : filter.getArtistLastname(),
		                                           filter.getLocationName() == null ? "" : filter.getLocationName(),
		                                           filter.getStreet() == null ? "" : filter.getStreet(),
		                                           filter.getCity() == null ? "" : filter.getCity(),
		                                           filter.getZip() == null ? "" : filter.getZip(),
		                                           filter.getCountry() == null ? "" : filter.getCountry(),
		                                           filter.getTime(),
		                                           filter.getPrice());

		LOGGER.debug("... retrieved filtered events");
		return some;
	}

	private int countBookedTicketsForEvent(Event event)
	{
		int count = 0;

		for(Booking booking : event.getBookings())
		{
			if(booking instanceof SeatBooking)
			{
				count += ((SeatBooking)booking).getSeats().size();
			}
			else if(booking instanceof SectionBooking)
			{
				for(SectionBookingEntry entry : ((SectionBooking)booking).getEntries())
				{
					count += (int)(long)entry.getAmount();
				}
			}
			else if(booking == null)
				throw new AssertionError("booking == null");
			else
				throw new AssertionError("unknown booking type");
		}

		return count;
	}

	@Override
	public Map<Event, Integer> findTopEvents(Category category, Integer month, Integer year, int limit) throws
	                                                                                                    ValidationException
	{
		LOGGER.debug("retrieving top {} events ...", limit);

		if(month != null && (month < 1 || month > 12))
			throw new ValidationException("month");

		if(year != null && (year < 2000))
			throw new ValidationException("year");

		List<Pair<Event, Integer>> events = new ArrayList<>();

		EventFilterDTO categoryFilter = new EventFilterDTO();
		categoryFilter.setCategory(category);

		for(Event event : findByFilter(categoryFilter))
		{
			if(month != null && event.getTime().getMonth().getValue() != month)
				continue;

			if(year != null && event.getTime().getYear() != year)
				continue;

			events.add(new Pair<>(event, countBookedTicketsForEvent(event)));
		}

		LOGGER.debug("... retrieved top events");

		return events.stream()
		             .sorted(Comparator.comparingInt(pair -> (int)((Pair)pair).getValue()).reversed())
		             .limit(limit)
		             .collect(Collectors.toMap(Pair::getKey, Pair::getValue));
	}

	@Override
	public Page<EventDTO> findPageByFilter(EventFilterDTO filter, Pageable pageRequest)
	{
		LOGGER.debug("retrieving filtered events with filter {} ...", filter);

		Page<Event> some = repository.findByFilter(filter.getName() == null ? "" : filter.getName(),
		                                           filter.getDescription() == null ? "" : filter.getDescription(),
		                                           filter.getCategory(),
		                                           filter.getDuration(),
		                                           filter.getArtistFirstname() == null ? "" : filter.getArtistFirstname(),
		                                           filter.getArtistLastname() == null ? "" : filter.getArtistLastname(),
		                                           filter.getLocationName() == null ? "" : filter.getLocationName(),
		                                           filter.getStreet() == null ? "" : filter.getStreet(),
		                                           filter.getCity() == null ? "" : filter.getCity(),
		                                           filter.getZip() == null ? "" : filter.getZip(),
		                                           filter.getCountry() == null ? "" : filter.getCountry(),
		                                           filter.getTime(),
		                                           filter.getPrice(),
		                                           pageRequest);

		LOGGER.debug("... retrieved filtered events");

		return convert(some, pageRequest);
	}

	@Override
	public Event create(Event event)
	{
		LOGGER.debug("creating event: {}", event);

		if(event.getEventAreas() != null)
		{
			for(EventArea eventArea : event.getEventAreas())
			{
				eventArea.setEvent(event);
			}
		}

		Event savedEvent = repository.save(event);
		LOGGER.debug("event created");
		return savedEvent;
	}

	private Page<EventDTO> convert(Page<Event> page, Pageable pageRequest)
	{
		if(page == null)
			throw new NotFoundException();

		LOGGER.debug("PAGE DEBUG: {}, {}", page.getSize(), page.getTotalPages());

		List<EventDTO> eventDTOs = mapper.eventToEventDTO(page.getContent());
		Page<EventDTO> pageOfEventDTOs = new PageImpl<>(eventDTOs, pageRequest, page.getTotalElements());

		LOGGER.debug("PAGEOFEVENTS DEBUG: {}, {}", pageOfEventDTOs.getSize(), pageOfEventDTOs.getTotalPages());

		// don't think this can happen
		if(pageRequest.getPageNumber() > pageOfEventDTOs.getTotalPages())
			throw new NotFoundException();

		LOGGER.debug("... sending page of events");

		return pageOfEventDTOs;
	}
}
