package at.ac.tuwien.inso.sepm.ticketline.server.entity;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
public class Receipt
{
	@Id
	@SequenceGenerator(name = "seq_receipt_id", sequenceName = "seq_receipt_id", initialValue = 4242)
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "seq_receipt_id")
	private Long id;

	@Column(nullable = false)
	private LocalDateTime createdAt;

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public LocalDateTime getCreatedAt()
	{
		return createdAt;
	}

	public void setCreatedAt(LocalDateTime createdAt)
	{
		this.createdAt = createdAt;
	}

	@Override
	public boolean equals(Object o)
	{
		if(this == o)
			return true;
		if(o == null || getClass() != o.getClass())
			return false;

		Receipt receipt = (Receipt)o;

		if(!id.equals(receipt.id))
			return false;
		return createdAt != null ? createdAt.equals(receipt.createdAt) : receipt.createdAt == null;
	}

	@Override
	public int hashCode()
	{
		int result = id.hashCode();
		result = 31 * result + (createdAt != null ? createdAt.hashCode() : 0);
		return result;
	}

	@Override
	public String toString()
	{
		return "Receipt{" + "id=" + id + ", createdAt=" + createdAt + '}';
	}
}
