package at.ac.tuwien.inso.sepm.ticketline.server.repository;

import at.ac.tuwien.inso.sepm.ticketline.server.entity.Booking;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookingRepository extends JpaRepository<Booking, Long>
{
	@Override
	List<Booking> findAll();

/*
	@Override
	Booking findOne(Long id);
*/

	List<Booking> findByCustomer(Customer customer);

	Booking findByReservationNumber(int reservationNumber);
}
