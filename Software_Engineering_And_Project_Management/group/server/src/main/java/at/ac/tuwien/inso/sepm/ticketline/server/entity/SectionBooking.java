package at.ac.tuwien.inso.sepm.ticketline.server.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import java.util.Set;

@Entity
public class SectionBooking extends Booking
{
	@OneToMany(mappedBy = "booking", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private Set<SectionBookingEntry> entries;

	public Set<SectionBookingEntry> getEntries()
	{
		return entries;
	}

	public void setEntries(Set<SectionBookingEntry> entries)
	{
		this.entries = entries;
	}

	@Override
	public Float totalPrice()
	{
		float sum = 0.0f;

		if(entries == null)
			return sum;

		for(SectionBookingEntry sectionEntry : entries)
		{
			Float sectionPrice = priceForArea(sectionEntry.getSection());
			sum += sectionPrice * sectionEntry.getAmount();
		}

		return sum;
	}

	@Override
	public String toString()
	{
		return "SectionBooking{" + "entries=" + entries + "} " + super.toString();
	}

	@Override
	public boolean equals(Object o)
	{
		if(this == o)
			return true;

		if(o == null || getClass() != o.getClass())
			return false;

		return super.equals(o);
	}
}
