package at.ac.tuwien.inso.sepm.ticketline.server.service.implementation;

import at.ac.tuwien.inso.sepm.ticketline.rest.payment.CreditCardDTO;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.*;
import at.ac.tuwien.inso.sepm.ticketline.server.exception.PaymentException;
import at.ac.tuwien.inso.sepm.ticketline.server.exception.ValidationException;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.StripeBookingInfoRepository;
import at.ac.tuwien.inso.sepm.ticketline.server.service.PaymentService;
import at.ac.tuwien.inso.sepm.ticketline.server.validation.CreditCardValidator;
import com.stripe.Stripe;
import com.stripe.exception.*;
import com.stripe.model.Charge;
import com.stripe.model.Refund;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class StripePaymentService implements PaymentService
{
	private static final Logger LOGGER = LoggerFactory.getLogger(StripePaymentService.class);

	private final StripeBookingInfoRepository stripeBookingInfoRepository;

	public StripePaymentService(StripeBookingInfoRepository stripeBookingInfoRepository)
	{
		this.stripeBookingInfoRepository = stripeBookingInfoRepository;
		Stripe.apiKey = "sk_test_5gCtU8jBbVnpqgzfVTVDJ9t5";
	}

	/*public void setBookingInfoRepository(BookingInfoRepository bookingInfoRepository)
	{
		if(bookingInfoRepository instanceof StripeBookingInfoRepository)
			this.stripeBookingInfoRepository = (StripeBookingInfoRepository)bookingInfoRepository;
	}*/

	public void pay(Booking booking, CreditCardDTO creditCardDTO) throws PaymentException, ValidationException
	{
		LOGGER.info("paying for {} with stripe", booking);
		try
		{
			com.stripe.model.Customer customer = createCustomer(booking.getCustomer());

			createCard(customer, creditCardDTO);

			Float price = calculatePrice(booking);
			Charge charge = createCharge(price, customer);

			StripeBookingInfo stripeBookingInfo = new StripeBookingInfo();
			stripeBookingInfo.setBooking(booking);
			stripeBookingInfo.setChargeId(charge.getId());
			stripeBookingInfoRepository.save(stripeBookingInfo);
		}
		catch(AuthenticationException | InvalidRequestException | APIConnectionException | CardException | APIException e)
		{
			LOGGER.error(e.getMessage());
			e.printStackTrace();
			throw new PaymentException(e.getMessage(), e);
		}
	}

	private Float calculatePrice(Booking booking)
	{
		Float price = 0f;

		LOGGER.debug("calculation price");
		if(booking instanceof SeatBooking)
		{
			LOGGER.debug("is seatbooking");
			SeatBooking seatBooking = (SeatBooking)booking;
			for(Seat seat : seatBooking.getSeats())
			{
				for(EventArea eventArea : seatBooking.getEvent().getEventAreas())
				{
					if(eventArea.getArea().equals(seat.getRow()))
					{
						LOGGER.debug("found match: {} and {}",
						             eventArea.getArea(),
						             seat.getRow());
						price += eventArea.getPrice();
					}
					else
					{
						LOGGER.debug("not matching: {} and {}",
						             eventArea.getArea(),
						             seat.getRow());
					}
				}
			}
		}
		else if(booking instanceof SectionBooking)
		{
			LOGGER.debug("is sectionbooking");
			SectionBooking sectionBooking = (SectionBooking)booking;
			for(SectionBookingEntry sectionBookingEntry : sectionBooking.getEntries())
			{
				for(EventArea eventArea : sectionBooking.getEvent().getEventAreas())
				{
					if(eventArea.getArea().equals(sectionBookingEntry.getSection()))
						price += (eventArea.getPrice() * sectionBookingEntry.getAmount());
				}
			}
		}

		LOGGER.debug("calculated price: {}", price);
		return price;
	}

	private com.stripe.model.Customer createCustomer(Customer customer) throws AuthenticationException,
	                                                                           InvalidRequestException,
	                                                                           APIConnectionException, APIException,
	                                                                           CardException
	{
		Map<String, Object> customerParams = new HashMap<>();
		customerParams.put("description",
		                   "Customer for " + customer.getFirstName() + " " + customer.getLastName());
		return com.stripe.model.Customer.create(customerParams);
	}

	private void createCard(com.stripe.model.Customer customer, CreditCardDTO creditCardDTO) throws
	                                                                                         AuthenticationException,
	                                                                                         InvalidRequestException,
	                                                                                         APIConnectionException,
	                                                                                         APIException,
	                                                                                         CardException,
	                                                                                         ValidationException
	{
		CreditCardValidator.validate(creditCardDTO);

		Map<String, Object> sourceParams = new HashMap<>();
		sourceParams.put("object", "card");
		sourceParams.put("exp_month", creditCardDTO.getExpirationMonth());
		sourceParams.put("exp_year", creditCardDTO.getExpirationYear());
		sourceParams.put("number", creditCardDTO.getCardNumber());
		sourceParams.put("cvc", creditCardDTO.getCvc());

		Map<String, Object> cardParams = new HashMap<>();
		cardParams.put("source", sourceParams);

		customer.getSources().create(cardParams);
	}

	private Charge createCharge(Float price, com.stripe.model.Customer customer) throws AuthenticationException,
	                                                                                    InvalidRequestException,
	                                                                                    APIConnectionException,
	                                                                                    APIException, CardException
	{
		Map<String, Object> chargeParams = new HashMap<>();
		int amount = (int)(price * 100);
		LOGGER.debug("price: {}, amount: {}", price, amount);
		chargeParams.put("amount", amount);
		chargeParams.put("currency", "EUR");
		chargeParams.put("description", "test charge");
		chargeParams.put("customer", customer.getId());
		return Charge.create(chargeParams);
	}

	@Override
	public void refund(Booking booking) throws PaymentException
	{
		try
		{
			LOGGER.info("refunding {}", booking);
			StripeBookingInfo stripeBookingInfo = stripeBookingInfoRepository.findByBooking(booking);

			Map<String, Object> refundParams = new HashMap<>();
			refundParams.put("charge", stripeBookingInfo.getChargeId());
			Refund.create(refundParams);
		}
		catch(AuthenticationException | InvalidRequestException | APIConnectionException | CardException | APIException e)
		{
			LOGGER.error(e.getMessage());
			throw new PaymentException(e.getMessage(), e);
		}
	}
}
