package at.ac.tuwien.inso.sepm.ticketline.server.service.implementation;

import at.ac.tuwien.inso.sepm.ticketline.rest.customer.CustomerDTO;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Customer;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.customer.CustomerMapper;
import at.ac.tuwien.inso.sepm.ticketline.server.exception.NotFoundException;
import at.ac.tuwien.inso.sepm.ticketline.server.exception.ValidationException;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.CustomerRepository;
import at.ac.tuwien.inso.sepm.ticketline.server.service.CustomerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.temporal.ChronoField;
import java.util.List;

@Service
public class SimpleCustomerService implements CustomerService
{
	private static final Logger LOGGER = LoggerFactory.getLogger(SimpleCustomerService.class);

	private final CustomerRepository repository;
	private final CustomerMapper mapper;

	public SimpleCustomerService(CustomerRepository repository, CustomerMapper mapper)
	{
		this.repository = repository;
		this.mapper = mapper;
	}

	@Override
	public List<Customer> findAll()
	{
		LOGGER.debug("retrieving all customers ...");
		List<Customer> result = repository.findAll();
		LOGGER.debug("... successfully retrieved all customers");
		return result;
	}

	@Override
	public Page<CustomerDTO> findAll(Pageable pageable)
	{
		LOGGER.debug("retrieving page of all customers ...");
		Page<Customer> page = repository.findAllByOrderByIdDesc(pageable);
		LOGGER.debug("... successfully retrieved page of all customers");

		return convert(page, pageable);
	}

	@Override
	public Customer findOne(Long id) throws ValidationException
	{
		if(id == null)
			throw new ValidationException("id");

		LOGGER.debug("retrieving customer with id {} ...", id);
		Customer customer = repository.findOne(id);

		if(customer == null)
			throw new NotFoundException();

		LOGGER.debug("... successfully retrieved customer");
		return customer;
	}

	@Override
	public Customer create(Customer customer) throws ValidationException
	{
		if(customer == null)
			throw new ValidationException("customer");

		// numbers game, don't ask
		Long customerNumber = LocalDateTime.now().getLong(ChronoField.MICRO_OF_SECOND) + LocalDateTime.now()
		                                                                                              .getLong(
			                                                                                              ChronoField.SECOND_OF_DAY)
		                      + LocalDateTime.now().getLong(ChronoField.DAY_OF_MONTH);
		customer.setCustomerNumber(customerNumber);

		LOGGER.debug("creating new customer ...");
		Customer result = repository.save(customer);
		LOGGER.debug("... successfully created customer");
		return result;
	}

	@Override
	public Customer update(Customer customer) throws ValidationException
	{
		if(customer == null)
			throw new ValidationException("customer");

		if(customer.getFirstName() == null || customer.getFirstName().isEmpty())
			throw new ValidationException("customer.firstName");

		if(customer.getLastName() == null || customer.getLastName().isEmpty())
			throw new ValidationException("customer.lastName");

		if(customer.getEmail() == null || customer.getEmail().isEmpty())
			throw new ValidationException("customer.email");

		LOGGER.debug("updating customer ...");
		Customer result = repository.save(customer);
		LOGGER.debug("... successfully updated customer");
		return result;
	}

	private Page<CustomerDTO> convert(Page<Customer> page, Pageable pageRequest)
	{
		if(page == null)
			throw new NotFoundException();

		List<CustomerDTO> customerDTOs = mapper.customersToCustomerDTOs(page.getContent());
		Page<CustomerDTO> pageOfCustomerDTOs = new PageImpl<>(customerDTOs, pageRequest, page.getTotalElements());

		// don't think this can happen
		if(pageRequest.getPageNumber() > pageOfCustomerDTOs.getTotalPages())
			throw new NotFoundException();

		LOGGER.debug("... sending page of events");

		return pageOfCustomerDTOs;
	}
}
