package at.ac.tuwien.inso.sepm.ticketline.server.endpoint;

import at.ac.tuwien.inso.sepm.ticketline.rest.news.DetailedNewsDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.news.SimpleNewsDTO;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.News;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.news.NewsMapper;
import at.ac.tuwien.inso.sepm.ticketline.server.exception.InternalServerError;
import at.ac.tuwien.inso.sepm.ticketline.server.exception.ValidationException;
import at.ac.tuwien.inso.sepm.ticketline.server.service.NewsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping(value = "/news")
@Api(value = "news")
public class NewsEndpoint
{
	private static final Logger LOGGER = LoggerFactory.getLogger(NewsEndpoint.class);

	private final NewsService service;
	private final NewsMapper mapper;

	public NewsEndpoint(NewsService service, NewsMapper mapper)
	{
		this.service = service;
		this.mapper = mapper;
	}

	@RequestMapping(method = RequestMethod.GET)
	@ApiOperation(value = "Get list of simple news entries")
	public List<SimpleNewsDTO> findAll()
	{
		LOGGER.debug("list of news requested...");
		List<SimpleNewsDTO> allNews = mapper.newsToSimpleNewsDTO(service.findAll());
		LOGGER.debug("...sending list of news");
		return allNews;
	}

	@RequestMapping(params = {"page", "size"}, method = RequestMethod.GET)
	@ApiOperation(value = "Get page of all news as simple news entries")
	public Page<SimpleNewsDTO> findAll(@RequestParam("page") int page, @RequestParam("size") int size, Principal user)
	{
		LOGGER.debug("page of news requested by user {} ...", user.getName());
		Page<SimpleNewsDTO> pageOfNews = service.findAll(new PageRequest(page, size), user.getName());
		LOGGER.debug("...sending page of news");

		return pageOfNews;
	}

	@RequestMapping(value = "/unread", params = {"page", "size"}, method = RequestMethod.GET)
	@ApiOperation(value = "Get page of unread news")
	public Page<SimpleNewsDTO> findUnread(@RequestParam("page") int page, @RequestParam("size") int size,
	                                      Principal user)
	{
		LOGGER.debug("page of unread news requested by user {} ...", user.getName());
		Page<SimpleNewsDTO> result = service.findUnread(new PageRequest(page, size), user.getName());
		LOGGER.debug("... sending page of news");

		return result;
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@ApiOperation(value = "Get detailed information about a specific news entry")
	public DetailedNewsDTO find(@PathVariable Long id)
	{
		LOGGER.debug("news with id {} requested...", id);
		DetailedNewsDTO news = mapper.newsToDetailedNewsDTO(service.findOne(id));
		LOGGER.debug("...sending news with id {}: {}", id, news);
		return news;
	}

	@RequestMapping(method = RequestMethod.POST)
	@PreAuthorize("hasRole('ADMIN')")
	@ApiOperation(value = "Publish a new news entry")
	public DetailedNewsDTO publishNews(@RequestBody DetailedNewsDTO detailedNewsDTO)
	{
		LOGGER.info("request to publish news: {}", detailedNewsDTO);
		News news = mapper.detailedNewsDTOToNews(detailedNewsDTO);

		try
		{
			news = service.publishNews(news);
		}
		catch(ValidationException e)
		{
			// this shouldn't really be happening,
			// so throw an InternalServerError
			LOGGER.error(e.getMessage());
			throw new InternalServerError();
		}

		LOGGER.info("sending published news: {}", news);
		return mapper.newsToDetailedNewsDTO(news);
	}

	@RequestMapping(method = RequestMethod.PUT)
	@ApiOperation(value = "Update a news entry (mainly used for isRead flag")
	public DetailedNewsDTO update(@RequestBody DetailedNewsDTO detailedNewsDTO, Principal user)
	{
		LOGGER.debug("news update requested from user {}...", user.getName());
		DetailedNewsDTO result = mapper.newsToDetailedNewsDTO(service.update(mapper.detailedNewsDTOToNews(
			detailedNewsDTO), user.getName()));
		LOGGER.debug("...updated news");

		return result;
	}
}
