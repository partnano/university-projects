package at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.event;

import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventAreaDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventDTO;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Event;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.EventArea;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.booking.GenericBookingMapper;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.location.GenericAreaMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring",
	uses =
		{
			SimpleEventMapper.class,
			GenericAreaMapper.class,
			GenericBookingMapper.class
		})
public interface EventMapper
{
	Event eventDTOtoEvent(EventDTO eventDTO);

	EventDTO eventToEventDTO(Event event);

	List<EventDTO> eventToEventDTO(List<Event> events);

	@Mapping(target = "event", ignore = true)
	EventArea eventAreaDTOtoEventArea(EventAreaDTO source);

	@Mapping(target = "event", ignore = true)
	EventAreaDTO eventAreaToEventAreaDTO(EventArea source);
}