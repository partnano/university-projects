package at.ac.tuwien.inso.sepm.ticketline.server.datagenerator;

import at.ac.tuwien.inso.sepm.ticketline.server.entity.News;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.NewsRepository;
import com.github.javafaker.Faker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

@Profile("generateData")
@Component
public class NewsDataGenerator
{
	private static final Logger LOGGER = LoggerFactory.getLogger(NewsDataGenerator.class);
	private static final int NUMBER_OF_NEWS_TO_GENERATE = 500;

	private final ClassLoader classLoader = getClass().getClassLoader();
	private final File IMAGE1 = new File(Objects.requireNonNull(classLoader.getResource("datagen/test_image1.jpg"))
	                                            .getFile());
	private final File IMAGE2 = new File(Objects.requireNonNull(classLoader.getResource("datagen/test_image2.jpg"))
	                                            .getFile());
	private final File IMAGE3 = new File(Objects.requireNonNull(classLoader.getResource("datagen/test_image3.jpg"))
	                                            .getFile());
	private final File IMAGE4 = new File(Objects.requireNonNull(classLoader.getResource("datagen/test_image4.jpg"))
	                                            .getFile());
	private final File IMAGE5 = new File(Objects.requireNonNull(classLoader.getResource("datagen/test_image5.jpg"))
	                                            .getFile());
	private byte[] BYTE_IMAGE1 = null;
	private byte[] BYTE_IMAGE2 = null;
	private byte[] BYTE_IMAGE3 = null;
	private byte[] BYTE_IMAGE4 = null;
	private byte[] BYTE_IMAGE5 = null;

	private final NewsRepository newsRepository;
	private final Faker faker = new Faker();

	public NewsDataGenerator(NewsRepository newsRepository)
	{
		this.newsRepository = newsRepository;
	}

	@PostConstruct
	private void generateNews()
	{
		if(newsRepository.count() > 0)
		{
			LOGGER.info("news already generated");
			return;
		}

		try
		{
			BYTE_IMAGE1 = Files.readAllBytes(IMAGE1.toPath());
			BYTE_IMAGE2 = Files.readAllBytes(IMAGE2.toPath());
			BYTE_IMAGE3 = Files.readAllBytes(IMAGE3.toPath());
			BYTE_IMAGE4 = Files.readAllBytes(IMAGE4.toPath());
			BYTE_IMAGE5 = Files.readAllBytes(IMAGE5.toPath());
		}
		catch(IOException e)
		{
			LOGGER.error("IO EXCEPTION WHILE GENERATING NEWS WITH IMAGE: {}", e);
		}

		LOGGER.info("generating {} news entries", NUMBER_OF_NEWS_TO_GENERATE);

		for(int i = 0; i < NUMBER_OF_NEWS_TO_GENERATE; i++)
		{
			Instant published = faker.date().past(365 * 3, TimeUnit.DAYS).toInstant();

			News news;
			byte[] byteImage;
			int rand = faker.number().numberBetween(1, 5);

			switch(rand)
			{
			case 1:
				byteImage = BYTE_IMAGE1;
				break;
			case 2:
				byteImage = BYTE_IMAGE2;
				break;
			case 3:
				byteImage = BYTE_IMAGE3;
				break;
			case 4:
				byteImage = BYTE_IMAGE4;
				break;
			case 5:
				byteImage = BYTE_IMAGE5;
				break;
			default:
				byteImage = null;
			}

			if(faker.number().numberBetween(0, 100) > 50)
			{
				news = News.builder()
				           .title(faker.ancient().titan())
				           .text(faker.lorem().paragraph(faker.number().numberBetween(5, 15))
				                 + "\n\n"
				                 + faker.lorem().paragraph(faker.number().numberBetween(5, 15))
				                 + "\n"
				                 + faker.lorem().paragraph(faker.number().numberBetween(5, 15)))
				           .byteImage(byteImage)
				           .publishedAt(LocalDateTime.ofInstant(published, ZoneId.systemDefault()))
				           .build();
			}
			else
			{
				news = News.builder()
				           .title(faker.ancient().titan())
				           .text(faker.lorem().paragraph(faker.number().numberBetween(5, 15))
				                 + "\n\n"
				                 + faker.lorem().paragraph(faker.number().numberBetween(5, 15))
				                 + "\n"
				                 + faker.lorem().paragraph(faker.number().numberBetween(5, 15)))
				           .publishedAt(LocalDateTime.ofInstant(published, ZoneId.systemDefault()))
				           .build();
			}

			LOGGER.debug("saving news {}", news);
			newsRepository.save(news);
		}
	}
}
