package at.ac.tuwien.inso.sepm.ticketline.server.entity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Set;

@Entity
@Table(name = "rows")
public class Row extends Area
{
	@OneToMany(mappedBy = "row", fetch = FetchType.EAGER)
	private Set<Seat> seats;

	public Set<Seat> getSeats()
	{
		return seats;
	}

	public Row setSeats(Set<Seat> seats)
	{
		this.seats = seats;
		return this;
	}

	@Override
	public String toString()
	{
		return "Row{" + "seats=" + seats + "} " + super.toString();
	}

	/*@Override
	public boolean equals(Object o)
	{
		if(this == o)
			return true;

		if(o == null || getClass() != o.getClass())
			return false;

		Row row = (Row)o;

		return super.equals(row);
	}*/

	public static RowBuilder builder()
	{
		return new RowBuilder();
	}

	public static final class RowBuilder extends AreaBuilder
	{
		private Set<Seat> seats;

		private RowBuilder()
		{
		}

		public RowBuilder seats(Set<Seat> seats)
		{
			this.seats = seats;
			return this;
		}

		@Override
		public RowBuilder location(Location location)
		{
			return (RowBuilder)super.location(location);
		}

		@Override
		public RowBuilder id(Long id)
		{
			return (RowBuilder)super.id(id);
		}

		@Override
		public RowBuilder orderIndex(Long orderIndex)
		{
			return (RowBuilder)super.orderIndex(orderIndex);
		}

		@Override
		public RowBuilder name(String name)
		{
			return (RowBuilder)super.name(name);
		}

		public Row build()
		{
			Row row = new Row();
			row = (Row)super.build(row);
			row.setSeats(seats);
			return row;
		}
	}
}
