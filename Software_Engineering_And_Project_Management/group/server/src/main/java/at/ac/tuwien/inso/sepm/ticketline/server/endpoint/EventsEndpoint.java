package at.ac.tuwien.inso.sepm.ticketline.server.endpoint;

import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventFilterDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.Top10EventDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.enums.Category;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Event;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.event.EventMapper;
import at.ac.tuwien.inso.sepm.ticketline.server.exception.ValidationException;
import at.ac.tuwien.inso.sepm.ticketline.server.service.EventService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/events")
@Api(value = "events")
public class EventsEndpoint
{

	private static final Logger LOGGER = LoggerFactory.getLogger(EventsEndpoint.class);

	private final EventService eventService;
	private final EventMapper eventsMapper;

	public EventsEndpoint(EventService eventService, EventMapper eventsMapper)
	{
		this.eventService = eventService;
		this.eventsMapper = eventsMapper;
	}

	@RequestMapping(method = RequestMethod.GET)
	@ApiOperation(value = "Get list of events")
	public List<EventDTO> findAll()
	{
		LOGGER.debug("list of events requested...");
		List<EventDTO> eventDTOs = eventsMapper.eventToEventDTO(eventService.findAll());
		LOGGER.debug("...sending list of events.");
		return eventDTOs;
	}


	@RequestMapping(params = { "page", "size" }, method = RequestMethod.GET)
	@ApiOperation("Get a page of all events")
	public Page<EventDTO> findAll(@RequestParam("page") int page, @RequestParam("size") int size)
	{
		return eventService.findPageFromAll(new PageRequest(page, size));
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@ApiOperation("Get a single event entry")
	public EventDTO find(@PathVariable Long id) throws ValidationException
	{
		LOGGER.debug("event with id {} requested", id);
		EventDTO event = eventsMapper.eventToEventDTO(eventService.findOne(id));
		LOGGER.debug("sending event with id {}:", id);
		return event;
	}

	@RequestMapping(value = "/filtered", method = RequestMethod.POST)
	@ApiOperation(value = "Get a filtered list of events")
	public List<EventDTO> findByFilter(@RequestBody EventFilterDTO filter)
	{
		LOGGER.debug("list of filtered ({}) events requested ...", filter);
		List<EventDTO> some = eventsMapper.eventToEventDTO(eventService.findByFilter(filter));
		LOGGER.debug("sending events with filter {}:", filter);

		return some;
	}

	@RequestMapping(value = "/top", method = RequestMethod.GET)
	@ApiOperation(value = "Get a list of most popular events")
	public List<Top10EventDTO> findTopEvents(@RequestParam(required = false) Category category,
	                                         @RequestParam(required = false) Integer month,
	                                         @RequestParam(required = false) Integer year,
	                                         @RequestParam int limit) throws ValidationException
	{
		LOGGER.debug("up to {} top events requested", limit);

		Map<Event, Integer> events = eventService.findTopEvents(category, month, year, limit);
		List<Top10EventDTO> result = new ArrayList<>();

		for(Map.Entry<Event, Integer> entry : events.entrySet())
		{
			Top10EventDTO dto = new Top10EventDTO();
			dto.setEvent(eventsMapper.eventToEventDTO(entry.getKey()));
			dto.setBookings(entry.getValue());
			result.add(dto);
		}

		LOGGER.debug("returning events ...");
		return result;
	}

	@RequestMapping(value = "/filtered", params = {"page", "size"}, method = RequestMethod.POST)
	@ApiOperation(value = "Get a page of filtered events")
	public Page<EventDTO> findByFilter(@RequestBody EventFilterDTO filter,
	                                   @RequestParam("page") int page, @RequestParam("size") int size)
	{
		return eventService.findPageByFilter(filter, new PageRequest(page, size));
	}

	@RequestMapping(method = RequestMethod.POST)
	@ApiOperation(value = "Create new event")
	public EventDTO create(@RequestBody EventDTO event)
	{
		LOGGER.info("create event requested for event: {}", event);
		EventDTO createdEvent = eventsMapper.eventToEventDTO(eventService.create(eventsMapper.eventDTOtoEvent(
			event)));
		LOGGER.info("sending created event: {}", createdEvent);
		return createdEvent;
	}
}
