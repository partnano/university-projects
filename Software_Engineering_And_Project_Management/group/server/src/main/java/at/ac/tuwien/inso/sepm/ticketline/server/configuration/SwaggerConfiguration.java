package at.ac.tuwien.inso.sepm.ticketline.server.configuration;

import at.ac.tuwien.inso.sepm.ticketline.server.configuration.properties.ApplicationConfigurationProperties;
import com.google.common.collect.Lists;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.ResponseMessage;
import springfox.documentation.service.SecurityScheme;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;

@Configuration
@EnableSwagger2
public class SwaggerConfiguration
{
	private final ApplicationConfigurationProperties config;

	public SwaggerConfiguration(ApplicationConfigurationProperties config)
	{
		this.config = config;
	}

	private static ResponseMessage createSuccessResponse()
	{
		return new ResponseMessageBuilder().code(HttpStatus.OK.value()).message("Success").build();
	}

	private static ResponseMessage createUnauthorizedResponse()
	{
		return new ResponseMessageBuilder().code(HttpStatus.UNAUTHORIZED.value())
		                                   .message("Unauthorized request, login first")
		                                   .build();
	}

	@Bean
	public Docket ticketlineApiDocket()
	{
		ApiInfo apiInfo = new ApiInfo("Ticketline Server",
		                              "Interactive API documentation for the Ticketline Server",
		                              config.getVersion(),
		                              null,
		                              null,
		                              null, null,
		                              Collections.emptyList());

		List<SecurityScheme> securitySchemes = Lists.newArrayList(new ApiKey("Authorization",
		                                                                     "Authorization",
		                                                                     "header"));

		List<ResponseMessage> getResponses = Lists.newArrayList(createSuccessResponse(),
		                                                        createUnauthorizedResponse());
		List<ResponseMessage> postResponses = Lists.newArrayList(createSuccessResponse(),
		                                                         createUnauthorizedResponse());

		Docket docket = new Docket(DocumentationType.SWAGGER_2);
		return docket.select()
		             .apis(RequestHandlerSelectors.withClassAnnotation(RestController.class))
		             .paths(PathSelectors.any())
		             .build()
		             .apiInfo(apiInfo)
		             .genericModelSubstitutes(ResponseEntity.class)
		             .securitySchemes(securitySchemes)
		             .useDefaultResponseMessages(false)
		             .globalResponseMessage(RequestMethod.GET, getResponses)
		             .globalResponseMessage(RequestMethod.POST, postResponses)
		             .consumes(new HashSet<>(Collections.singletonList(MediaType.APPLICATION_JSON_VALUE)))
		             .produces(new HashSet<>(Collections.singletonList(MediaType.APPLICATION_JSON_VALUE)));
	}
}
