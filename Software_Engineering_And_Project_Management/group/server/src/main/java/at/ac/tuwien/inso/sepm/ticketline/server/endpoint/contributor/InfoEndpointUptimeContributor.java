package at.ac.tuwien.inso.sepm.ticketline.server.endpoint.contributor;

import org.springframework.boot.actuate.info.Info;
import org.springframework.boot.actuate.info.InfoContributor;
import org.springframework.stereotype.Component;

import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.time.Duration;
import java.time.temporal.ChronoUnit;

@Component
public class InfoEndpointUptimeContributor implements InfoContributor
{
	private final RuntimeMXBean runtime = ManagementFactory.getRuntimeMXBean();

	@Override
	public void contribute(Info.Builder builder)
	{
		builder.withDetail("uptime", Duration.of(runtime.getUptime(), ChronoUnit.MILLIS));
	}
}
