package at.ac.tuwien.inso.sepm.ticketline.server.validation;

import at.ac.tuwien.inso.sepm.ticketline.server.entity.News;
import at.ac.tuwien.inso.sepm.ticketline.server.exception.ValidationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NewsValidator
{
	private static final Logger LOGGER = LoggerFactory.getLogger(NewsValidator.class);

	public static void validateNews(News news) throws ValidationException
	{
		LOGGER.trace("Validating news object...");

		if(news == null)
			throw new ValidationException("news.isNull");
		else
		{
			if(news.getTitle() == null)
				throw new ValidationException("news.noTitle");

			if(news.getText() == null)
				throw new ValidationException("news.noText");

			if (news.getByteImage() != null)
			{
				// don't be bigger than 5 MB
				if (news.getByteImage().length > 5000000)
					throw new ValidationException("news.imageSize");
			}
		}
	}
}
