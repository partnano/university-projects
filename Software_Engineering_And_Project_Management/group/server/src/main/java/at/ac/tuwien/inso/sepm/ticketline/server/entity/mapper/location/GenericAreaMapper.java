package at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.location;

import at.ac.tuwien.inso.sepm.ticketline.rest.location.AreaDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.location.RowDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.location.SectionDTO;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Area;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Row;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Section;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.booking.SeatBookingMapper;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.booking.SectionBookingMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class GenericAreaMapper
{
	@Autowired
	private RowMapper rowMapper;

	@Autowired
	private SectionMapper sectionMapper;

	@Autowired
	private AreaMapper areaMapper;

	@Autowired
	private SeatBookingMapper seatBookingMapper;

	@Autowired
	private SectionBookingMapper sectionBookingMapper;

	// Enable deep mapping (e.g. Seat to SeatDTO instead of the basic bahaviour Seat to AreaDTO)

	public AreaDTO areaToAreaDTO(Area area)
	{
		// Temporarily remove reference to this location to enable mapping (remove loop)
		area.setLocation(null);

		if(area instanceof Section)
		{
			Section s = (Section)area;
			return sectionMapper.sectionToSectionDTO(s);
		}
		else if(area instanceof Row)
		{
			Row r = (Row)area;
			return rowMapper.rowToRowDTO(r);
		}
		else
		{
			return areaMapper.areaToAreaDTO(area);
		}
	}

	public Area areaDTOToArea(AreaDTO area)
	{
		// Temporarily remove reference to this location to enable mapping (remove loop)
		area.setLocation(null);

		if(area instanceof SectionDTO)
		{
			SectionDTO s = (SectionDTO)area;
			return sectionMapper.sectionDTOtoSection(s);
		}
		else if(area instanceof RowDTO)
		{
			RowDTO r = (RowDTO)area;
			return rowMapper.rowDTOtoRow(r);
		}
		else
		{
			return areaMapper.areaDTOToArea(area);
		}
	}
}
