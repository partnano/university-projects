package at.ac.tuwien.inso.sepm.ticketline.server.entity;

import javax.persistence.*;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "location")
public class Location
{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "seq_location_id")
	@SequenceGenerator(name = "seq_location_id", sequenceName = "seq_location_id")
	private Long id;

	// column names == variable names per default
	@Column(nullable = false)
	private String name;

	@Column(nullable = false)
	private String street;

	@Column(nullable = false)
	private String city;

	@Column(nullable = false)
	private String zip;

	@Column(nullable = false)
	private String country;

	@OneToMany(mappedBy = "location", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private Set<Area> areas;

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getStreet()
	{
		return street;
	}

	public void setStreet(String street)
	{
		this.street = street;
	}

	public String getZip()
	{
		return zip;
	}

	public void setZip(String zip)
	{
		this.zip = zip;
	}

	public Set<Area> getAreas()
	{
		return areas;
	}

	public void setAreas(Set<Area> areas)
	{
		this.areas = areas;
	}

	public String getCountry()
	{
		return country;
	}

	public void setCountry(String country)
	{
		this.country = country;
	}

	public String getCity()
	{
		return city;
	}

	public void setCity(String city)
	{
		this.city = city;
	}

	@Override
	public String toString()
	{
		return "Location{" + "id=" + id + ", name='" + name + '\'' + ", street='" + street + '\'' + ", zip='"
		       + zip + '\'' + ", areas=" + areas + '}';
	}

	@Override
	public boolean equals(Object o)
	{
		if(this == o)
			return true;

		if(o == null || getClass() != o.getClass())
			return false;

		Location location = (Location)o;

		return Objects.equals(id, location.id)
		       && Objects.equals(name, location.name)
		       && Objects.equals(street, location.street)
		       && Objects.equals(city, location.city)
		       && Objects.equals(zip, location.zip)
		       && Objects.equals(country, location.country);
	}

	public static LocationBuilder builder()
	{
		return new LocationBuilder();
	}

	public static final class LocationBuilder
	{
		private Long id;
		private String name;
		private String street;
		private String city;
		private String zip;
		private String country;
		private Set<Area> areas;

		private LocationBuilder()
		{
		}

		public LocationBuilder id(Long id)
		{
			this.id = id;
			return this;
		}

		public LocationBuilder name(String name)
		{
			this.name = name;
			return this;
		}

		public LocationBuilder street(String street)
		{
			this.street = street;
			return this;
		}

		public LocationBuilder city(String city)
		{
			this.city = city;
			return this;
		}

		public LocationBuilder zip(String zip)
		{
			this.zip = zip;
			return this;
		}

		public LocationBuilder country(String country)
		{
			this.country = country;
			return this;
		}

		public LocationBuilder area(Set<Area> areas)
		{
			this.areas = areas;
			return this;
		}

		public Location build()
		{
			Location location = new Location();
			location.setId(id);
			location.setName(name);
			location.setStreet(street);
			location.setCity(city);
			location.setZip(zip);
			location.setCountry(country);
			location.setAreas(areas);
			return location;
		}
	}
}
