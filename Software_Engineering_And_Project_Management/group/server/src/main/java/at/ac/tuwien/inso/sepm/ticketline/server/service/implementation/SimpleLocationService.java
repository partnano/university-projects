package at.ac.tuwien.inso.sepm.ticketline.server.service.implementation;

import at.ac.tuwien.inso.sepm.ticketline.server.entity.Location;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.LocationRepository;
import at.ac.tuwien.inso.sepm.ticketline.server.service.LocationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SimpleLocationService implements LocationService
{
	private static final Logger LOGGER = LoggerFactory.getLogger(SimpleCustomerService.class);

	private final LocationRepository repository;

	public SimpleLocationService(LocationRepository repository)
	{
		this.repository = repository;
	}

	@Override
	public List<Location> findAll()
	{
		LOGGER.debug("retrieving all locations...");
		List<Location> all = repository.findAll();
		LOGGER.debug("...retrieved all locations");
		return all;
	}
}
