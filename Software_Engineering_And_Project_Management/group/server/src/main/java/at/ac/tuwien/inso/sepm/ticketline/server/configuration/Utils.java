package at.ac.tuwien.inso.sepm.ticketline.server.configuration;

public class Utils
{
	public static boolean isRunningAsTest()
	{
		StackTraceElement[] trace = new Exception().getStackTrace();

		for(StackTraceElement element : trace)
		{
			if(element.getClassName().startsWith("org.junit"))
				return true;
		}

		return false;
	}
}
