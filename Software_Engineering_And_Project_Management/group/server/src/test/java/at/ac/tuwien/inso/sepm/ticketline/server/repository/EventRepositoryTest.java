package at.ac.tuwien.inso.sepm.ticketline.server.repository;

import at.ac.tuwien.inso.sepm.ticketline.rest.event.enums.Category;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Event;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@DataJpaTest
@Transactional
public class EventRepositoryTest
{
	@Autowired
	private EventRepository repository;

	private static final String EVENT_NAME = "Test Event";
	private static final String EVENT_DESCRIPTION = "Test Description";
	private static final String EVENT_ARTIST = "Madonna Donna";
	private static final LocalDateTime EVENT_TIME = LocalDateTime.of(2017, 1, 1, 1, 1, 0, 0);
	private static final Category EVENT_CATEGORY = Category.CONCERT;
	private static final int EVENT_DURATION = 120;

	@Test
	public void retrieveEmptyEvents()
	{
		Assertions.assertThat(repository.findAll()).isEmpty();
	}

	@Test
	public void createEvent()
	{
		Event event = Event.builder()
		                   .name(EVENT_NAME)
		                   .category(EVENT_CATEGORY)
		                   .description(EVENT_DESCRIPTION)
		                   .artist(EVENT_ARTIST)
		                   .time(EVENT_TIME)
		                   .duration(EVENT_DURATION)
		                   .build();

		Assertions.assertThat(event.getId()).isNull();
		Event savedEvent = repository.save(event);
		Assertions.assertThat(savedEvent).isEqualToIgnoringGivenFields(event, "id");
		Assertions.assertThat(savedEvent.getId()).isEqualTo(1L);
	}

	@Test
	public void createEvents()
	{
		Event event1 = Event.builder()
		                    .name(EVENT_NAME)
		                    .category(EVENT_CATEGORY)
		                    .description(EVENT_DESCRIPTION)
		                    .artist(EVENT_ARTIST)
		                    .time(EVENT_TIME)
		                    .duration(EVENT_DURATION)
		                    .build();

		Event event2 = Event.builder()
		                    .name("Test")
		                    .category(EVENT_CATEGORY)
		                    .description(EVENT_DESCRIPTION)
		                    .artist(EVENT_ARTIST)
		                    .time(EVENT_TIME)
		                    .duration(EVENT_DURATION)
		                    .build();

		List<Event> savedEvents = repository.save(List.of(event1, event2));

		Assertions.assertThat(savedEvents.get(0)).isEqualToIgnoringGivenFields(event1, "id");
		Assertions.assertThat(savedEvents.get(0).getId()).isEqualTo(2L);
		Assertions.assertThat(savedEvents.get(1)).isEqualToIgnoringGivenFields(event2, "id");
		Assertions.assertThat(savedEvents.get(1).getId()).isEqualTo(3L);
	}

	@Test
	public void retrieveFilteredEmpty()
	{
		List<Event> filteredEvents = repository.findByFilter("",
		                                                     "",
		                                                     null,
		                                                     null,
		                                                     "",
		                                                     "",
		                                                     "",
		                                                     "",
		                                                     "",
		                                                     "",
		                                                     "",
		                                                     null,
		                                                     null);

		Assertions.assertThat(filteredEvents).isEmpty();
	}
}