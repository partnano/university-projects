package at.ac.tuwien.inso.sepm.ticketline.server.integrationtest;

import at.ac.tuwien.inso.sepm.ticketline.rest.news.DetailedNewsDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.news.SimpleNewsDTO;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.News;
import at.ac.tuwien.inso.sepm.ticketline.server.integrationtest.base.BaseIntegrationTest;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.NewsRepository;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;
import java.util.*;

import static org.hamcrest.core.Is.is;
import static org.mockito.Matchers.any;

public class NewsEndpointTest extends BaseIntegrationTest
{
	private static final String NEWS_ENDPOINT = "/news";
	private static final String SPECIFIC_NEWS_PATH = "/{newsId}";

	private static final String TEST_NEWS_TEXT = "TestNewsText";
	private static final String TEST_NEWS_TITLE = "title";
	private static final LocalDateTime TEST_NEWS_PUBLISHED_AT = LocalDateTime.of(2016, 11, 13, 12, 15, 0, 0);
	private static final long TEST_NEWS_ID = 1L;

	@MockBean
	private NewsRepository repository;

	@Test
	public void findAllNewsUnauthorizedAsAnonymous()
	{
		Response response = RestAssured.given()
		                               .contentType(ContentType.JSON)
		                               .when()
		                               .get(NEWS_ENDPOINT)
		                               .then()
		                               .extract()
		                               .response();

		Assert.assertThat(response.getStatusCode(), is(HttpStatus.UNAUTHORIZED.value()));
	}

	@Test
	public void findAllNewsAsUser()
	{
		BDDMockito.given(repository.findAllByOrderByPublishedAtDesc())
		          .willReturn(Collections.singletonList(News.builder()
		                                                    .id(TEST_NEWS_ID)
		                                                    .title(TEST_NEWS_TITLE)
		                                                    .text(TEST_NEWS_TEXT)
		                                                    .publishedAt(TEST_NEWS_PUBLISHED_AT)
		                                                    .build()));

		Response response = RestAssured.given()
		                               .contentType(ContentType.JSON)
		                               .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
		                               .when()
		                               .get(NEWS_ENDPOINT)
		                               .then()
		                               .extract()
		                               .response();

		Assert.assertThat(response.getStatusCode(), is(HttpStatus.OK.value()));
		Assert.assertThat(Arrays.asList(response.as(SimpleNewsDTO[].class)),
		                  is(Collections.singletonList(SimpleNewsDTO.builder()
		                                                            .id(TEST_NEWS_ID)
		                                                            .title(TEST_NEWS_TITLE)
		                                                            .summary(TEST_NEWS_TEXT)
		                                                            .publishedAt(TEST_NEWS_PUBLISHED_AT)
		                                                            .build())));
	}

	@Test
	public void findSpecificNewsUnauthorizedAsAnonymous()
	{
		Response response = RestAssured.given()
		                               .contentType(ContentType.JSON)
		                               .when()
		                               .get(NEWS_ENDPOINT + SPECIFIC_NEWS_PATH, TEST_NEWS_ID)
		                               .then()
		                               .extract()
		                               .response();

		Assert.assertThat(response.getStatusCode(), is(HttpStatus.UNAUTHORIZED.value()));
	}

	@Test
	public void findSpecificNewsAsUser()
	{
		BDDMockito.given(repository.findOneById(TEST_NEWS_ID))
		          .willReturn(Optional.of(News.builder()
		                                      .id(TEST_NEWS_ID)
		                                      .title(TEST_NEWS_TITLE)
		                                      .text(TEST_NEWS_TEXT)
		                                      .publishedAt(TEST_NEWS_PUBLISHED_AT)
		                                      .build()));

		Response response = RestAssured.given()
		                               .contentType(ContentType.JSON)
		                               .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
		                               .when()
		                               .get(NEWS_ENDPOINT + SPECIFIC_NEWS_PATH, TEST_NEWS_ID)
		                               .then()
		                               .extract()
		                               .response();

		Assert.assertThat(response.getStatusCode(), is(HttpStatus.OK.value()));
		Assert.assertThat(response.as(DetailedNewsDTO.class),
		                  is(DetailedNewsDTO.builder()
		                                    .id(TEST_NEWS_ID)
		                                    .title(TEST_NEWS_TITLE)
		                                    .text(TEST_NEWS_TEXT)
		                                    .publishedAt(TEST_NEWS_PUBLISHED_AT)
		                                    .build()));
	}

	@Test
	public void findSpecificNonExistingNewsNotFoundAsUser()
	{
		BDDMockito.given(repository.findOneById(TEST_NEWS_ID)).willReturn(Optional.empty());

		Response response = RestAssured.given()
		                               .contentType(ContentType.JSON)
		                               .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
		                               .when()
		                               .get(NEWS_ENDPOINT + SPECIFIC_NEWS_PATH, TEST_NEWS_ID)
		                               .then()
		                               .extract()
		                               .response();

		Assert.assertThat(response.getStatusCode(), is(HttpStatus.NOT_FOUND.value()));
	}

	@Test
	public void publishNewsUnauthorizedAsAnonymous()
	{
		Response response = RestAssured.given()
		                               .contentType(ContentType.JSON)
		                               .body(DetailedNewsDTO.builder()
		                                                    .id(TEST_NEWS_ID)
		                                                    .title(TEST_NEWS_TITLE)
		                                                    .text(TEST_NEWS_TEXT)
		                                                    .publishedAt(TEST_NEWS_PUBLISHED_AT)
		                                                    .build())
		                               .when()
		                               .post(NEWS_ENDPOINT)
		                               .then()
		                               .extract()
		                               .response();

		Assert.assertThat(response.getStatusCode(), is(HttpStatus.UNAUTHORIZED.value()));
	}

	@Test
	public void publishNewsUnauthorizedAsUser()
	{
		Response response = RestAssured.given()
		                               .contentType(ContentType.JSON)
		                               .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
		                               .body(DetailedNewsDTO.builder()
		                                                    .id(TEST_NEWS_ID)
		                                                    .title(TEST_NEWS_TITLE)
		                                                    .text(TEST_NEWS_TEXT)
		                                                    .publishedAt(TEST_NEWS_PUBLISHED_AT)
		                                                    .build())
		                               .when()
		                               .post(NEWS_ENDPOINT)
		                               .then()
		                               .extract()
		                               .response();

		Assert.assertThat(response.getStatusCode(), is(HttpStatus.FORBIDDEN.value()));
	}

	@Test
	public void publishNewsAsAdmin()
	{
		BDDMockito.given(repository.save(any(News.class)))
		          .willReturn(News.builder()
		                          .id(TEST_NEWS_ID)
		                          .title(TEST_NEWS_TITLE)
		                          .text(TEST_NEWS_TEXT)
		                          .publishedAt(TEST_NEWS_PUBLISHED_AT)
		                          .build());

		Response response = RestAssured.given()
		                               .contentType(ContentType.JSON)
		                               .header(HttpHeaders.AUTHORIZATION, validAdminTokenWithPrefix)
		                               .body(DetailedNewsDTO.builder()
		                                                    .title(TEST_NEWS_TITLE)
		                                                    .text(TEST_NEWS_TEXT)
		                                                    .build())
		                               .when()
		                               .post(NEWS_ENDPOINT)
		                               .then()
		                               .extract()
		                               .response();

		Assert.assertThat(response.getStatusCode(), is(HttpStatus.OK.value()));
		Assert.assertThat(response.as(DetailedNewsDTO.class),
		                  is(DetailedNewsDTO.builder()
		                                    .id(TEST_NEWS_ID)
		                                    .title(TEST_NEWS_TITLE)
		                                    .text(TEST_NEWS_TEXT)
		                                    .publishedAt(TEST_NEWS_PUBLISHED_AT)
		                                    .build()));
	}

	@Test
	public void retrieveNewsPage_success()
	{
		News news = News.builder()
		                .id(TEST_NEWS_ID)
		                .publishedAt(TEST_NEWS_PUBLISHED_AT)
		                .title(TEST_NEWS_TITLE)
		                .text(TEST_NEWS_TEXT)
		                .build();

		List<News> newsList = new LinkedList<>();
		newsList.add(news);

		PageRequest pageRequest = new PageRequest(1, 1);
		Page<News> newsPage = new PageImpl<>(newsList, pageRequest, 2);

		BDDMockito.given(repository.findAllByOrderByPublishedAtDesc(pageRequest)).willReturn(newsPage);

		Response response = RestAssured.given()
		                               .contentType(ContentType.JSON)
		                               .header(HttpHeaders.AUTHORIZATION, validAdminTokenWithPrefix)
		                               .when()
		                               .get(NEWS_ENDPOINT + "/?page=1&size=1")
		                               .then()
		                               .extract()
		                               .response();

		Assert.assertThat(response.getStatusCode(), is(HttpStatus.OK.value()));
	}
}
