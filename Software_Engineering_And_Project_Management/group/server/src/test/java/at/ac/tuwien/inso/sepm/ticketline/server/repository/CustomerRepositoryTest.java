package at.ac.tuwien.inso.sepm.ticketline.server.repository;

import at.ac.tuwien.inso.sepm.ticketline.server.entity.Customer;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.transaction.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@DataJpaTest
@Transactional
public class CustomerRepositoryTest
{
	@Autowired
	private CustomerRepository repository;

	private static final long ID = 1;
	private static final String FIRST_NAME = "tester";
	private static final String LAST_NAME = "testmaster";
	private static final String EMAIL = "tester@testing.com";

	@Test
	public void retrieveEmptyCustomer()
	{
		Assertions.assertThat(repository.findAll()).isEmpty();
	}

	@Test
	public void createCustomer()
	{
		Customer customer = new Customer();
		customer.setFirstName(FIRST_NAME);
		customer.setLastName(LAST_NAME);
		customer.setEmail(EMAIL);

		Customer savedCustomer = repository.save(customer);

		Assertions.assertThat(savedCustomer).isEqualToIgnoringGivenFields(customer, "id");
		Assertions.assertThat(savedCustomer.getId()).isEqualTo(ID);
	}
}
