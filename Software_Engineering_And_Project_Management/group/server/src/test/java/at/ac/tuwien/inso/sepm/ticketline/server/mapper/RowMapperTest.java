package at.ac.tuwien.inso.sepm.ticketline.server.mapper;

import at.ac.tuwien.inso.sepm.ticketline.rest.location.LocationDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.location.RowDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.location.SeatDTO;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Location;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Row;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Seat;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.location.RowMapper;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.SeatRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Collections;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
public class RowMapperTest
{
	@Configuration
	@ComponentScan(basePackages = "at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper")
	public static class LocationRowMapperTestContextConfiguration
	{
	}

	@MockBean
	SeatRepository seatRepository;

	@Autowired
	@SuppressWarnings("SpringJavaAutowiredMembersInspection")
	private RowMapper mapper;

	private static final String ROW_NAME = "Nightmare";
	private static final Long ORDER_INDEX = 1L;
	private static final Long ROW_ID = 100L;

	private Set<Seat> getSeatsForRow(Row row)
	{
		return Collections.singleton(Seat.builder().name("Bla").orderIndex(5L).row(row).build());
	}

	private Set<SeatDTO> getSeatsForRowDTO()
	{
		return Collections.singleton(SeatDTO.builder().name("Bla").orderIndex(5L).build());
	}

	@Test
	public void shouldMapRowToRowDTO()
	{
		Location location = Location.builder().id(12L).name("Elm").street("Street").zip("636").build();

		Row row = Row.builder().id(ROW_ID).location(location).name(ROW_NAME).orderIndex(ORDER_INDEX).build();

		Set<Seat> seats = getSeatsForRow(row);
		row.setSeats(seats);

		location.setAreas(Collections.singleton(row));

		RowDTO rowDTO = mapper.rowToRowDTO(row);
		assertThat(rowDTO).isNotNull();
		assertThat(rowDTO.getOrderIndex()).isEqualTo(ORDER_INDEX);
		assertThat(rowDTO.getId()).isEqualTo(ROW_ID);
		assertThat(rowDTO.getName()).isEqualTo(ROW_NAME);
		assertThat(rowDTO.getLocation()).isNull();

		// Tests bidirectional reference for seats
		assertThat(rowDTO.getSeats()).isNotNull();
		assertThat(rowDTO.getSeats().size()).isEqualTo(seats.size());

		for(SeatDTO seatDTO : rowDTO.getSeats())
		{
			assertThat(seatDTO.getRow()).isNull();
		}
	}

	@Test
	public void shouldMapRowDTOToRow()
	{
		LocationDTO locationDTO = LocationDTO.builder().id(12L).name("Elm").street("Street").zip("636").build();

		RowDTO rowDTO = RowDTO.builder().id(ROW_ID).name(ROW_NAME).orderIndex(ORDER_INDEX).build();

		Set<SeatDTO> seats = getSeatsForRowDTO();
		rowDTO.setSeats(seats);

		locationDTO.setAreas(Collections.singleton(rowDTO));

		Row row = mapper.rowDTOtoRow(rowDTO);
		assertThat(row).isNotNull();
		assertThat(row.getOrderIndex()).isEqualTo(ORDER_INDEX);
		assertThat(row.getId()).isEqualTo(ROW_ID);
		assertThat(row.getName()).isEqualTo(ROW_NAME);
		assertThat(row.getLocation()).isNull();

		// Tests bidirectional reference for seats
		assertThat(row.getSeats()).isNotNull();
		assertThat(row.getSeats().size()).isEqualTo(seats.size());

		for(Seat seat : row.getSeats())
		{
			assertThat(seat.getRow()).isNull();
		}
	}
}
