package at.ac.tuwien.inso.sepm.ticketline.server.service;

import at.ac.tuwien.inso.sepm.ticketline.server.entity.Customer;
import at.ac.tuwien.inso.sepm.ticketline.server.exception.ValidationException;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.CustomerRepository;
import at.ac.tuwien.inso.sepm.ticketline.server.service.implementation.SimpleCustomerService;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collections;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SimpleCustomerServiceTest
{
	private static final long ID = 1;
	private static final String FIRST_NAME = "tester";
	private static final String LAST_NAME = "testmaster";
	private static final String EMAIL = "tester@testing.com";

	@MockBean
	private CustomerRepository repository;

	@Autowired
	private SimpleCustomerService service;

	@Test
	public void returnsAllCustomers()
	{
		Customer customer = new Customer();
		customer.setId(ID);
		customer.setFirstName(FIRST_NAME);
		customer.setLastName(LAST_NAME);
		customer.setEmail(EMAIL);

		BDDMockito.given(repository.findAll()).willReturn(Collections.singletonList(customer));

		List<Customer> customers = service.findAll();
		Assertions.assertThat(customers).isEqualTo(Collections.singletonList(customer));
	}

	@Test
	public void findsCustomerById() throws ValidationException
	{
		Customer customer = new Customer();
		customer.setId(ID);
		customer.setFirstName(FIRST_NAME);
		customer.setLastName(LAST_NAME);
		customer.setEmail(EMAIL);

		BDDMockito.given(repository.findOne(ID)).willReturn(customer);

		Customer result = service.findOne(ID);
		Assertions.assertThat(result).isEqualTo(customer);
	}

	/*@Test(expected = ValidationException.class)
	public void rejectsInvalidCustomerOnCreate() throws ValidationException
	{
		Customer customer = new Customer();
		customer.setFirstName(FIRST_NAME);
		// no last name
		customer.setEmail(EMAIL);

		service.create(customer);
	}*/

	/*@Test(expected = ValidationException.class)
	public void rejectsInvalidCustomerOnUpdate() throws ValidationException
	{
		service.create(new Customer());
	}*/
}
