package at.ac.tuwien.inso.sepm.ticketline.server.service;

import at.ac.tuwien.inso.sepm.ticketline.rest.user.UserDTO;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.User;
import at.ac.tuwien.inso.sepm.ticketline.server.exception.NotFoundException;
import at.ac.tuwien.inso.sepm.ticketline.server.exception.ValidationException;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.UserRepository;
import at.ac.tuwien.inso.sepm.ticketline.server.service.implementation.SimpleUserService;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.LinkedList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SimpleUserServiceTest
{

	private static final Long USER_ID = 1L;
	private static final String FIRSTNAME = "Testfirstname";
	private static final String LASTNAME = "Testlastname";

	private static final String VALID_PASSWORD_SALT = BCrypt.gensalt();
	private static final String VALID_PASSWORD_HASH = BCrypt.hashpw("password", VALID_PASSWORD_SALT);

	@MockBean
	private UserRepository repository;

	@Autowired
	private SimpleUserService service;

	@Test
	public void retrieveUserPage()
	{
		User user = new User();
		user.setId(USER_ID);
		user.setFirstName(FIRSTNAME);
		user.setLastName(LASTNAME);

		List<User> userList = new LinkedList<>();
		userList.add(user);

		PageRequest testPageRequest = new PageRequest(1, 1);

		Page<User> userPage = new PageImpl<>(userList, testPageRequest, 1);

		BDDMockito.given(repository.findAllByOrderByIdDesc(testPageRequest)).willReturn(userPage);

		Page<UserDTO> result = service.findAll(testPageRequest);
		Assertions.assertThat(result).isNotNull();
		Assertions.assertThat(result.getContent().get(0).getId()).isEqualTo(USER_ID);
	}

	@Test(expected = NotFoundException.class)
	public void retrieveUserPage_notFound()
	{
		User user = new User();
		user.setId(USER_ID);
		user.setFirstName(FIRSTNAME);
		user.setLastName(LASTNAME);

		List<User> userList = new LinkedList<>();
		userList.add(user);

		PageRequest testPageRequest = new PageRequest(1, 1);

		Page<User> userPage = new PageImpl<>(userList, testPageRequest, 1);

		BDDMockito.given(repository.findAllByOrderByIdDesc(testPageRequest)).willReturn(userPage);

		Page<UserDTO> result = service.findAll(new PageRequest(2, 2));
	}

	@Test(expected = ValidationException.class)
	public void rejectsCreatingInvalidUser() throws ValidationException
	{
		User user = new User();
		user.setFirstName("a");
		user.setLastName("b");
		user.setEmail(null);
		service.create(user);
	}

	@Test
	public void createsValidUserSuccessfully() throws ValidationException
	{
		User user = new User();
		user.setFirstName("fred");
		user.setLastName("tester");
		user.setEmail("fred.tester@testing.org");
		user.setLocked(false);
		user.setAdmin(true);
		user.setPasswordSalt(VALID_PASSWORD_SALT);
		user.setPasswordHash(VALID_PASSWORD_HASH);
		service.create(user);
	}

	@Test
	public void updatesUserSuccessfully() throws ValidationException
	{
		User user = new User();
		user.setFirstName("michael");
		user.setLastName("testerovic");
		user.setEmail("mtesterovic@trash-mail.com");
		user.setLocked(true);
		user.setAdmin(false);
		user.setPasswordSalt(VALID_PASSWORD_SALT);
		user.setPasswordHash(VALID_PASSWORD_HASH);

		user.setId(1337L);
		BDDMockito.given(repository.save(user)).willReturn(user);
		service.create(user);

		user.setEmail("mtesterovic@gmail.com");
		user.setLocked(false);

		BDDMockito.given(repository.findOne(user.getId())).willReturn(user);
		service.update(user, "somebodyelse");
	}

	@Test(expected = NotFoundException.class)
	public void rejectsUpdatingNonExistentUser() throws ValidationException
	{
		User user = new User();
		user.setId(99999999L);
		user.setLastName("marc");
		user.setLastName("o'testing");
		user.setEmail("happytester1337@aol.com");
		user.setLocked(false);
		user.setAdmin(true);
		user.setPasswordSalt(VALID_PASSWORD_SALT);
		user.setPasswordHash(VALID_PASSWORD_HASH);

		service.update(user, "somebodyelse");
	}

	@Test(expected = NotFoundException.class)
	public void rejectsResettingPasswordForNonExistentUser() throws ValidationException
	{
		service.resetPassword(10000L);
	}

	@Test(expected = ValidationException.class)
	public void rejectsResettingPasswordForInvalidId() throws ValidationException
	{
		service.resetPassword(-18575L);
	}
}
