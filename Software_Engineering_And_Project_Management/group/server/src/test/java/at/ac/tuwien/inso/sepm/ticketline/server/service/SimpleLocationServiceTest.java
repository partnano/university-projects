package at.ac.tuwien.inso.sepm.ticketline.server.service;

import at.ac.tuwien.inso.sepm.ticketline.server.entity.Location;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.LocationRepository;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SimpleLocationServiceTest
{
	@MockBean
	private LocationRepository repository;

	@Autowired
	private LocationService service;

	private static final String LOCATION_NAME = "Test Location";
	private static final String LOCATION_STREET = "Test Street";
	private static final String LOCATION_CITY = "Test City";
	private static final String LOCATION_ZIP = "1234";
	private static final String LOCATION_COUNTRY = "Test Country";

	@Test
	public void retrieveLocations()
	{
		Location location = Location.builder()
		                            .name(LOCATION_NAME)
		                            .street(LOCATION_STREET)
		                            .city(LOCATION_CITY)
		                            .zip(LOCATION_ZIP)
		                            .country(LOCATION_COUNTRY)
		                            .build();

		List<Location> list = new ArrayList<>();
		list.add(location);

		BDDMockito.given(repository.findAll()).willReturn(list);
		Assertions.assertThat(service.findAll()).isEqualTo(list);
	}
}
