package at.ac.tuwien.inso.sepm.ticketline.server.mapper;

import at.ac.tuwien.inso.sepm.ticketline.rest.location.LocationDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.location.SectionDTO;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Location;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Section;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.location.SectionMapper;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.SeatRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
public class SectionMapperTest
{
	@Configuration
	@ComponentScan(basePackages = "at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper")
	public static class LocationSectionMapperTestContextConfiguration
	{
	}

	@MockBean
	SeatRepository seatRepository;

	@Autowired
	@SuppressWarnings("SpringJavaAutowiredMembersInspection")
	private SectionMapper mapper;

	private static final long ORDER_INDEX = 1L;
	private static final long SECTION_ID = 1L;
	private static final String SECTION_NAME = "Nightmare";
	private static final int SECTION_CAPACITY = 66;

	@Test
	public void shouldMapSectionToSectionDTO()
	{
		Location loc = Location.builder().id(12L).name("Elm").street("Street").zip("636").build();

		Section section = Section.builder()
		                         .id(SECTION_ID)
		                         .orderIndex(ORDER_INDEX)
		                         .location(loc)
		                         .name(SECTION_NAME)
		                         .capacity(SECTION_CAPACITY)
		                         .build();

		loc.setAreas(Collections.singleton(section));

		SectionDTO sectionDTO = mapper.sectionToSectionDTO(section);
		assertThat(sectionDTO).isNotNull();
		assertThat(sectionDTO.getOrderIndex()).isEqualTo(ORDER_INDEX);
		assertThat(sectionDTO.getId()).isEqualTo(SECTION_ID);
		assertThat(sectionDTO.getName()).isEqualTo(SECTION_NAME);
		assertThat(sectionDTO.getCapacity()).isEqualTo(SECTION_CAPACITY);
		assertThat(sectionDTO.getLocation()).isNull();
	}

	@Test
	public void shouldMapSectionDTOToSection()
	{
		LocationDTO locDTO = LocationDTO.builder().id(12L).name("Elm").street("Street").zip("636").build();

		SectionDTO sectionDTO = SectionDTO.builder()
		                                  .id(SECTION_ID)
		                                  .orderIndex(ORDER_INDEX)
		                                  .name(SECTION_NAME)
		                                  .capacity(SECTION_CAPACITY)
		                                  .build();

		locDTO.setAreas(Collections.singleton(sectionDTO));

		Section section = mapper.sectionDTOtoSection(sectionDTO);
		assertThat(section).isNotNull();
		assertThat(section.getOrderIndex()).isEqualTo(ORDER_INDEX);
		assertThat(section.getId()).isEqualTo(SECTION_ID);
		assertThat(section.getName()).isEqualTo(SECTION_NAME);
		assertThat(section.getCapacity()).isEqualTo(SECTION_CAPACITY);
		assertThat(section.getLocation()).isNull();
	}
}
