package at.ac.tuwien.inso.sepm.ticketline.server.integrationtest;

import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventFilterDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.enums.Category;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.*;
import at.ac.tuwien.inso.sepm.ticketline.server.integrationtest.base.BaseIntegrationTest;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.EventRepository;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;
import java.util.*;

import static org.hamcrest.core.Is.is;

public class EventsEndpointTest extends BaseIntegrationTest
{
	private static final String EVENTS_ENDPOINT = "/events";

	private static final long EVENT_ID = 1L;
	private static final String EVENT_NAME = "Test Event";
	private static final String EVENT_DESCRIPTION = "Test Description";
	private static final String EVENT_ARTIST = "Madonna Donna";
	private static final String EVENT_ARTIST_FIRSTNAME = "Madonna";
	private static final String EVENT_ARTIST_LASTNAME = "Donna";
	private static final LocalDateTime EVENT_TIME = LocalDateTime.of(2017, 1, 1, 1, 1, 0, 0);
	private static final String LOCATION_NAME = "Test Location";
	private static final String LOCATION_STREET = "Test Street";
	private static final String LOCATION_CITY = "Test City";
	private static final String LOCATION_ZIP = "1234";
	private static final String LOCATION_COUNTRY = "Test Country";
	private static final Category EVENT_CATEGORY = Category.CONCERT;
	private static final int EVENT_DURATION = 120;

	@MockBean
	private EventRepository repository;

	@Test
	public void findAllEventsUnauthorized()
	{
		Response response = RestAssured.given()
		                               .contentType(ContentType.JSON)
		                               .when()
		                               .get(EVENTS_ENDPOINT)
		                               .then()
		                               .extract()
		                               .response();

		Assert.assertThat(response.getStatusCode(), is(HttpStatus.UNAUTHORIZED.value()));
	}

	@Test
	public void findAllEventsAsUser()
	{
		BDDMockito.given(repository.findAll())
		          .willReturn(Collections.singletonList(Event.builder()
		                                                     .id(EVENT_ID)
		                                                     .name(EVENT_NAME)
		                                                     .artist(EVENT_ARTIST)
		                                                     .category(EVENT_CATEGORY)
		                                                     .time(EVENT_TIME)
		                                                     .build()));

		Response response = RestAssured.given()
		                               .contentType(ContentType.JSON)
		                               .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
		                               .when()
		                               .get(EVENTS_ENDPOINT)
		                               .then()
		                               .extract()
		                               .response();

		Assert.assertThat(response.getStatusCode(), is(HttpStatus.OK.value()));
		Assert.assertThat(Arrays.asList(response.as(EventDTO[].class)),
		                  is(Collections.singletonList(EventDTO.builder()
		                                                       .id(EVENT_ID)
		                                                       .name(EVENT_NAME)
		                                                       .artist(EVENT_ARTIST)
		                                                       .category(EVENT_CATEGORY)
		                                                       .time(EVENT_TIME)
		                                                       .build())));
	}

	@Test
	public void findAllEventsAsAdmin()
	{
		BDDMockito.given(repository.findAll())
		          .willReturn(Collections.singletonList(Event.builder()
		                                                     .id(EVENT_ID)
		                                                     .name(EVENT_NAME)
		                                                     .artist(EVENT_ARTIST)
		                                                     .category(EVENT_CATEGORY)
		                                                     .time(EVENT_TIME)
		                                                     .build()));

		Response response = RestAssured.given()
		                               .contentType(ContentType.JSON)
		                               .header(HttpHeaders.AUTHORIZATION, validAdminTokenWithPrefix)
		                               .when()
		                               .get(EVENTS_ENDPOINT)
		                               .then()
		                               .extract()
		                               .response();

		Assert.assertThat(response.getStatusCode(), is(HttpStatus.OK.value()));
		Assert.assertThat(Arrays.asList(response.as(EventDTO[].class)),
		                  is(Collections.singletonList(EventDTO.builder()
		                                                       .id(EVENT_ID)
		                                                       .name(EVENT_NAME)
		                                                       .artist(EVENT_ARTIST)
		                                                       .category(EVENT_CATEGORY)
		                                                       .time(EVENT_TIME)
		                                                       .build())));
	}

	@Test
	public void createsNewEventSuccessfully()
	{
		EventDTO event = EventDTO.builder()
		                         .name(EVENT_NAME)
		                         .artist(EVENT_ARTIST)
		                         .category(EVENT_CATEGORY)
		                         .time(EVENT_TIME)
		                         .build();

		Response response = RestAssured.given()
		                               .contentType(ContentType.JSON)
		                               .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
		                               .body(event)
		                               .when()
		                               .post(EVENTS_ENDPOINT)
		                               .then()
		                               .extract()
		                               .response();

		Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK.value());
	}

	@Test
	public void findNoFilteredEventSuccessfully()
	{
		Location location = Location.builder()
		                            .name(LOCATION_NAME)
		                            .street(LOCATION_STREET)
		                            .city(LOCATION_CITY)
		                            .zip(LOCATION_ZIP)
		                            .country(LOCATION_COUNTRY)
		                            .build();

		BDDMockito.given(repository.findByFilter(EVENT_NAME,
		                                         EVENT_DESCRIPTION,
		                                         EVENT_CATEGORY,
		                                         EVENT_DURATION,
		                                         EVENT_ARTIST_FIRSTNAME,
		                                         EVENT_ARTIST_LASTNAME,
		                                         LOCATION_NAME,
		                                         LOCATION_STREET,
		                                         LOCATION_CITY,
		                                         LOCATION_ZIP,
		                                         LOCATION_COUNTRY,
		                                         EVENT_TIME,
		                                         null))
		          .willReturn(Collections.singletonList(Event.builder()
		                                                     .id(EVENT_ID)
		                                                     .name(EVENT_NAME)
		                                                     .artist(EVENT_ARTIST)
		                                                     .category(EVENT_CATEGORY)
		                                                     .time(EVENT_TIME)
		                                                     .location(location)
		                                                     .build()));

		EventFilterDTO filter = new EventFilterDTO();
		filter.setArtistFirstname("another");
		filter.setCountry("another");

		Response response = RestAssured.given()
		                               .contentType(ContentType.JSON)
		                               .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
		                               .body(filter)
		                               .when()
		                               .post(EVENTS_ENDPOINT + "/filtered")
		                               .then()
		                               .extract()
		                               .response();

		Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK.value());
		Assert.assertThat(Arrays.asList(response.as(EventDTO[].class)).size(), is(0));
	}

	@Test
	public void findEventFilteredByName()
	{
		Event event = Event.builder()
		                   .id(EVENT_ID)
		                   .name(EVENT_NAME)
		                   .artist(EVENT_ARTIST)
		                   .category(EVENT_CATEGORY)
		                   .time(EVENT_TIME)
		                   .build();

		EventDTO eventDTO = EventDTO.builder()
		                            .id(EVENT_ID)
		                            .name(EVENT_NAME)
		                            .artist(EVENT_ARTIST)
		                            .category(EVENT_CATEGORY)
		                            .time(EVENT_TIME)
		                            .build();

		EventFilterDTO filter = new EventFilterDTO();
		filter.setName(EVENT_NAME);

		BDDMockito.given(repository.findByFilter(Mockito.eq(EVENT_NAME),
		                                         Mockito.anyString(),
		                                         Mockito.any(Category.class),
		                                         Mockito.anyInt(),
		                                         Mockito.anyString(),
		                                         Mockito.anyString(),
		                                         Mockito.anyString(),
		                                         Mockito.anyString(),
		                                         Mockito.anyString(),
		                                         Mockito.anyString(),
		                                         Mockito.anyString(),
		                                         Mockito.anyObject(),
		                                         Mockito.anyObject()))
		          .willReturn(Collections.singletonList(event));

		Response response = RestAssured.given()
		                               .contentType(ContentType.JSON)
		                               .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
		                               .body(filter)
		                               .when()
		                               .post(EVENTS_ENDPOINT + "/filtered")
		                               .then()
		                               .extract()
		                               .response();

		Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK.value());
		Assertions.assertThat(Arrays.asList(response.as(EventDTO[].class)))
		          .isEqualTo(Collections.singletonList(eventDTO));
	}

	@Test
	public void findEventFilteredByCategory()
	{
		Event event = Event.builder()
		                   .id(EVENT_ID)
		                   .name(EVENT_NAME)
		                   .artist(EVENT_ARTIST)
		                   .category(EVENT_CATEGORY)
		                   .time(EVENT_TIME)
		                   .build();

		EventDTO eventDTO = EventDTO.builder()
		                            .id(EVENT_ID)
		                            .name(EVENT_NAME)
		                            .artist(EVENT_ARTIST)
		                            .category(EVENT_CATEGORY)
		                            .time(EVENT_TIME)
		                            .build();

		EventFilterDTO filter = new EventFilterDTO();
		filter.setCategory(EVENT_CATEGORY);

		BDDMockito.given(repository.findByFilter(Mockito.anyString(),
		                                         Mockito.anyString(),
		                                         Mockito.eq(EVENT_CATEGORY),
		                                         Mockito.anyInt(),
		                                         Mockito.anyString(),
		                                         Mockito.anyString(),
		                                         Mockito.anyString(),
		                                         Mockito.anyString(),
		                                         Mockito.anyString(),
		                                         Mockito.anyString(),
		                                         Mockito.anyString(),
		                                         Mockito.anyObject(),
		                                         Mockito.anyObject()))
		          .willReturn(Collections.singletonList(event));

		Response response = RestAssured.given()
		                               .contentType(ContentType.JSON)
		                               .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
		                               .body(filter)
		                               .when()
		                               .post(EVENTS_ENDPOINT + "/filtered")
		                               .then()
		                               .extract()
		                               .response();

		Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK.value());
		Assertions.assertThat(Arrays.asList(response.as(EventDTO[].class)))
		          .isEqualTo(Collections.singletonList(eventDTO));
	}

	@Test
	public void findEventFilteredByDuration()
	{
		Event event = Event.builder()
		                   .id(EVENT_ID)
		                   .name(EVENT_NAME)
		                   .artist(EVENT_ARTIST)
		                   .category(EVENT_CATEGORY)
		                   .time(EVENT_TIME)
		                   .duration(EVENT_DURATION)
		                   .build();

		EventDTO eventDTO = EventDTO.builder()
		                            .id(EVENT_ID)
		                            .name(EVENT_NAME)
		                            .artist(EVENT_ARTIST)
		                            .category(EVENT_CATEGORY)
		                            .time(EVENT_TIME)
		                            .duration(EVENT_DURATION)
		                            .build();

		EventFilterDTO filter = new EventFilterDTO();
		filter.setDuration(EVENT_DURATION);

		BDDMockito.given(repository.findByFilter(Mockito.anyString(),
		                                         Mockito.anyString(),
		                                         Mockito.any(),
		                                         Mockito.eq(EVENT_DURATION),
		                                         Mockito.anyString(),
		                                         Mockito.anyString(),
		                                         Mockito.anyString(),
		                                         Mockito.anyString(),
		                                         Mockito.anyString(),
		                                         Mockito.anyString(),
		                                         Mockito.anyString(),
		                                         Mockito.anyObject(),
		                                         Mockito.anyObject()))
		          .willReturn(Collections.singletonList(event));

		Response response = RestAssured.given()
		                               .contentType(ContentType.JSON)
		                               .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
		                               .body(filter)
		                               .when()
		                               .post(EVENTS_ENDPOINT + "/filtered")
		                               .then()
		                               .extract()
		                               .response();

		Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK.value());
		Assertions.assertThat(Arrays.asList(response.as(EventDTO[].class)))
		          .isEqualTo(Collections.singletonList(eventDTO));
	}

	@Test
	public void findTopEvent()
	{
		SectionBooking sectionBooking = new SectionBooking();
		SectionBookingEntry entry = new SectionBookingEntry();
		entry.setSection(Section.builder().build());
		entry.setAmount(3L);
		sectionBooking.setEntries(Set.of(entry));

		SeatBooking seatBooking = new SeatBooking();
		seatBooking.setSeats(Set.of(Seat.builder().id(1L).build(), Seat.builder().id(2L).build()));

		List<Event> events = new ArrayList<>();
		events.add(Event.builder().id(1L).bookings(Set.of(sectionBooking)).build());
		events.add(Event.builder().id(2L).bookings(Set.of(seatBooking)).build());

		BDDMockito.given(repository.findByFilter("", "", null, null, "", "", "", "", "", "", "", null, null))
		          .willReturn(events);

		Response response = RestAssured.given()
		                               .contentType(ContentType.JSON)
		                               .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
		                               .when()
		                               .get(EVENTS_ENDPOINT + "/top?limit=1")
		                               .then()
		                               .extract()
		                               .response();

		Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK.value());
		Assertions.assertThat(response.getBody().<List<?>>path("").size()).isEqualTo(1);
		Assertions.assertThat(response.getBody().<Integer>path("[0].bookings")).isEqualTo(3);
	}
  
	@Test
	public void findPageOfAllEvents()
	{
		Event event = Event.builder()
		                   .id(EVENT_ID)
		                   .name(EVENT_NAME)
		                   .artist(EVENT_ARTIST)
		                   .category(EVENT_CATEGORY)
		                   .time(EVENT_TIME)
		                   .duration(EVENT_DURATION)
		                   .build();

		List<Event> eventsList = new LinkedList<>();
		eventsList.add(event);

		PageRequest pageRequest = new PageRequest(1, 1);
		Page<Event> eventsPage = new PageImpl<>(eventsList, pageRequest, 2);

		BDDMockito.given(repository.findAllByOrderByIdDesc(pageRequest)).willReturn(eventsPage);

		Response response = RestAssured.given()
		                               .contentType(ContentType.JSON)
		                               .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
		                               .when()
		                               .get(EVENTS_ENDPOINT + "?page=1&size=1")
		                               .then()
		                               .extract()
		                               .response();

		Assert.assertThat(response.getStatusCode(), is(HttpStatus.OK.value()));
		// TODO: convert response to Page and add further assertions
	}
}
