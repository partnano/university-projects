package at.ac.tuwien.inso.sepm.ticketline.server.endpoint;

import at.ac.tuwien.inso.sepm.ticketline.rest.customer.CustomerDTO;
import at.ac.tuwien.inso.sepm.ticketline.server.configuration.JacksonConfiguration;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Customer;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.customer.CustomerMapper;
import at.ac.tuwien.inso.sepm.ticketline.server.exception.ValidationException;
import at.ac.tuwien.inso.sepm.ticketline.server.security.AuthenticationConstants;
import at.ac.tuwien.inso.sepm.ticketline.server.service.CustomerService;
import at.ac.tuwien.inso.sepm.ticketline.server.service.implementation.SimpleHeaderTokenAuthenticationService;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.config.ObjectMapperConfig;
import com.jayway.restassured.config.RestAssuredConfig;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;
import org.assertj.core.api.Assertions;
import org.assertj.core.util.Strings;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.mockito.BDDMockito.given;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CustomerEndpointTest
{
	@MockBean
	private CustomerMapper customerMapper;

	@MockBean
	private CustomerService customerService;

	private static final String SERVER_HOST = "http://localhost";
	private static final String USER_USERNAME = "user";
	private static final String USER_PASSWORD = "password";
	private static final String ADMIN_PASSWORD = "password";
	private static final String ADMIN_USERNAME = "admin";

	@Value("${server.context-path}")
	private String contextPath;

	@LocalServerPort
	private int port;

	@Autowired
	private SimpleHeaderTokenAuthenticationService authenticationService;

	@Autowired
	private JacksonConfiguration jacksonConfiguration;

	private String validUserTokenWithPrefix;
	private String validAdminTokenWithPrefix;

	private static final long ID = 1;
	private static final String FIRST_NAME = "tester";
	private static final String LAST_NAME = "testmaster";
	private static final String EMAIL = "tester@testing.com";

	private static final String CUSTOMERS_ENDPOINT = "/customers";
	private static final String CUSTOMER_ENDPOINT = CUSTOMERS_ENDPOINT + "/" + ID;

	@Before
	public void beforeBase()
	{
		RestAssured.baseURI = SERVER_HOST;
		RestAssured.basePath = contextPath;
		RestAssured.port = port;
		RestAssured.config = RestAssuredConfig.config().
			objectMapperConfig(new ObjectMapperConfig().jackson2ObjectMapperFactory((aClass, s) -> jacksonConfiguration
				.jackson2ObjectMapperBuilder()
				.build()));

		validUserTokenWithPrefix = Strings.join(AuthenticationConstants.TOKEN_PREFIX,
		                                        authenticationService.authenticate(USER_USERNAME, USER_PASSWORD)
		                                                             .getCurrentToken()).with(" ");

		validAdminTokenWithPrefix = Strings.join(AuthenticationConstants.TOKEN_PREFIX,
		                                         authenticationService.authenticate(ADMIN_USERNAME,
		                                                                            ADMIN_PASSWORD)
		                                                              .getCurrentToken()).with(" ");
	}

	@Test
	public void retrieveCustomersAuthorized()
	{
		CustomerDTO customer = new CustomerDTO();
		customer.setFirstName(FIRST_NAME);
		customer.setLastName(LAST_NAME);
		customer.setEmail(EMAIL);

		List<CustomerDTO> list = new ArrayList<>();
		list.add(customer);

		given(customerMapper.customersToCustomerDTOs(Mockito.anyListOf(Customer.class))).willReturn(list);

		Response response = RestAssured.given()
		                               .contentType(ContentType.JSON)
		                               .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
		                               .when()
		                               .get(CUSTOMERS_ENDPOINT)
		                               .then()
		                               .extract()
		                               .response();

		Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK.value());
		Assertions.assertThat(Arrays.asList(response.as(CustomerDTO[].class))).isEqualTo(list);
	}

	@Test
	public void retrieveCustomersUnauthorized()
	{
		Response response = RestAssured.given()
		                               .contentType(ContentType.JSON)
		                               .when()
		                               .get(CUSTOMERS_ENDPOINT)
		                               .then()
		                               .extract()
		                               .response();

		Assert.assertThat(response.getStatusCode(), is(HttpStatus.UNAUTHORIZED.value()));
	}

	@Test
	public void createCustomerAuthorized() throws ValidationException
	{
		CustomerDTO customer = new CustomerDTO();
		customer.setFirstName(FIRST_NAME);
		customer.setLastName(LAST_NAME);
		customer.setEmail(EMAIL);

		BDDMockito.given(customerMapper.customerDTOToCustomer(Mockito.any(CustomerDTO.class))).willReturn(null);
		BDDMockito.given(customerService.create(Mockito.any(Customer.class))).willReturn(null);
		BDDMockito.given(customerMapper.customerToCustomerDTO(Mockito.any(Customer.class)))
		          .willReturn(customer);

		Response response = RestAssured.given()
		                               .contentType(ContentType.JSON)
		                               .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
		                               .body(customer)
		                               .when()
		                               .post(CUSTOMERS_ENDPOINT)
		                               .then()
		                               .extract()
		                               .response();

		Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK.value());
		Assertions.assertThat(response.as(CustomerDTO.class)).isEqualTo(customer);
	}

	@Test
	public void createCustomerUnauthorized()
	{
		CustomerDTO customer = new CustomerDTO();
		customer.setFirstName(FIRST_NAME);
		customer.setLastName(LAST_NAME);
		customer.setEmail(EMAIL);

		Response response = RestAssured.given()
		                               .contentType(ContentType.JSON)
		                               .body(customer)
		                               .when()
		                               .post(CUSTOMERS_ENDPOINT)
		                               .then()
		                               .extract()
		                               .response();

		Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.UNAUTHORIZED.value());
	}

	@Test
	public void retrieveCustomerAuthorized() throws ValidationException
	{
		CustomerDTO customer = new CustomerDTO();
		customer.setFirstName(FIRST_NAME);
		customer.setLastName(LAST_NAME);
		customer.setEmail(EMAIL);

		BDDMockito.given(customerService.findOne(Mockito.anyLong())).willReturn(null);
		BDDMockito.given(customerMapper.customerToCustomerDTO(Mockito.any(Customer.class)))
		          .willReturn(customer);

		Response response = RestAssured.given()
		                               .contentType(ContentType.JSON)
		                               .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
		                               .when()
		                               .get(CUSTOMER_ENDPOINT)
		                               .then()
		                               .extract()
		                               .response();

		Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK.value());
		Assertions.assertThat(response.as(CustomerDTO.class)).isEqualTo(customer);
	}

	@Test
	public void retrieveCustomerUnauthorized()
	{
		Response response = RestAssured.given()
		                               .contentType(ContentType.JSON)
		                               .when()
		                               .get(CUSTOMER_ENDPOINT)
		                               .then()
		                               .extract()
		                               .response();

		Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.UNAUTHORIZED.value());
	}

	@Test
	public void updateCustomerAuthorized()
	{
		CustomerDTO customer = new CustomerDTO();
		customer.setFirstName(FIRST_NAME);
		customer.setLastName(LAST_NAME);
		customer.setEmail(EMAIL);

		BDDMockito.given(customerMapper.customerToCustomerDTO(Mockito.any(Customer.class)))
		          .willReturn(customer);

		Response response = RestAssured.given()
		                               .contentType(ContentType.JSON)
		                               .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
		                               .body(customer)
		                               .when().put(CUSTOMER_ENDPOINT)
		                               .then()
		                               .extract()
		                               .response();

		Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK.value());
		Assertions.assertThat(response.as(CustomerDTO.class)).isEqualTo(customer);
	}

	@Test
	public void updateCustomerUnauthorized()
	{
		Response response = RestAssured.given()
		                               .contentType(ContentType.JSON)
		                               .when().put(CUSTOMER_ENDPOINT)
		                               .then()
		                               .extract()
		                               .response();

		Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.UNAUTHORIZED.value());
	}
}
