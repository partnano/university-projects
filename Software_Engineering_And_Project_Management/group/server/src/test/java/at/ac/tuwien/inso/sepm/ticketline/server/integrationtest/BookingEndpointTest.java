package at.ac.tuwien.inso.sepm.ticketline.server.integrationtest;

import at.ac.tuwien.inso.sepm.ticketline.rest.booking.BookingDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.booking.SeatBookingDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.booking.SectionBookingDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.customer.CustomerDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.payment.CreditCardDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.payment.PaymentContext;
import at.ac.tuwien.inso.sepm.ticketline.rest.payment.PaymentProvider;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Customer;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Event;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.SeatBooking;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.SectionBooking;
import at.ac.tuwien.inso.sepm.ticketline.server.integrationtest.base.BaseIntegrationTest;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.BookingRepository;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.CustomerRepository;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.EventRepository;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.LocationRepository;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.HashSet;

public class BookingEndpointTest extends BaseIntegrationTest
{
	@MockBean
	private BookingRepository repository;

	@MockBean
	private CustomerRepository customerRepository;

	@MockBean
	private EventRepository eventRepository;

	@Autowired
	private LocationRepository locationRepository;

	private final String ENDPOINT = "/bookings";

	private static final long ID = 1;
	private static final String FIRST_NAME = "tester";
	private static final String LAST_NAME = "testmaster";
	private static final String EMAIL = "tester@testing.com";

	/*private static final String LOCATION_NAME1 = "Test Location";
	private static final String LOCATION_NAME2 = "Another Test Location";
	private static final String LOCATION_STREET = "Test Street";
	private static final String LOCATION_CITY = "Test City";
	private static final String LOCATION_ZIP = "1234";
	private static final String LOCATION_COUNTRY = "Test Country";

	private static final long EVENT_ID = 1L;
	private static final String EVENT_NAME = "Test Event";
	private static final String EVENT_ARTIST = "Madonna Donna";
	private static final Category EVENT_CATEGORY = Category.CONCERT;
	private static final LocalDateTime EVENT_TIME = LocalDateTime.of(2017, 1, 1, 1, 1, 1, 1);*/

	@Test
	public void createsBookingSuccessfully()
	{

		BDDMockito.given(repository.save(Mockito.any(SeatBooking.class))).willReturn(new SeatBooking());
		BDDMockito.given(eventRepository.findOne(Mockito.anyLong())).willReturn(new Event());

		SeatBookingDTO booking = new SeatBookingDTO();
		booking.setReservationNumber(11);
		booking.setCreatedAt(LocalDateTime.now());
		booking.setPaid(false);
		booking.setSeats(new HashSet<>());
		EventDTO event = new EventDTO();
		event.setId(1L);
		booking.setEvent(event);
		booking.setCustomer(new CustomerDTO());

		Response response = RestAssured.given()
		                               .contentType(ContentType.JSON)
		                               .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
		                               .body(booking)
		                               .when()
		                               .post(ENDPOINT)
		                               .then()
		                               .extract()
		                               .response();

		Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK.value());
	}

	@Test
	public void rejectsCreatingInvalidCustomer()
	{
		SeatBookingDTO booking = new SeatBookingDTO();
		booking.setId(510L);

		Response response = RestAssured.given()
		                               .contentType(ContentType.JSON)
		                               .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
		                               .body(booking)
		                               .when()
		                               .post(ENDPOINT)
		                               .then()
		                               .extract()
		                               .response();

		Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST.value());
	}

	@Test
	public void updatesBookingSuccessfully()
	{
		SectionBookingDTO booking = new SectionBookingDTO();
		booking.setId(11L);
		booking.setReservationNumber(11);
		booking.setCreatedAt(LocalDateTime.now());
		booking.setPaid(false);
		booking.setSections(new HashSet<>());
		booking.setEvent(new EventDTO());
		booking.setCustomer(new CustomerDTO());

		BDDMockito.given(repository.save(Mockito.any(SectionBooking.class))).willReturn(new SectionBooking());

		Response response = RestAssured.given()
		                               .contentType(ContentType.JSON)
		                               .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
		                               .body(booking)
		                               .when().put(ENDPOINT + "/{id}", 11L)
		                               .then()
		                               .extract()
		                               .response();

		Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK.value());
	}

	@Test
	public void rejectsUpdatingInvalidCustomer()
	{
		SeatBookingDTO booking = new SeatBookingDTO();
		booking.setId(500L);

		Response response = RestAssured.given()
		                               .contentType(ContentType.JSON)
		                               .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
		                               .body(booking)
		                               .when().put(ENDPOINT + "/{id}", 11L)
		                               .then()
		                               .extract()
		                               .response();

		Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST.value());
	}

	@Test
	public void returnsCustomerBookings()
	{
		Customer customer = new Customer();
		customer.setId(ID);
		customer.setFirstName(FIRST_NAME);
		customer.setLastName(LAST_NAME);
		customer.setEmail(EMAIL);

		SectionBooking fakeBooking = new SectionBooking();
		fakeBooking.setId(10L);
		fakeBooking.setCustomer(customer);

		BDDMockito.given(customerRepository.findOne(customer.getId())).willReturn(customer);
		BDDMockito.given(repository.findByCustomer(customer))
		          .willReturn(Collections.singletonList(fakeBooking));

		Response response = RestAssured.given()
		                               .contentType(ContentType.JSON)
		                               .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
		                               .then()
		                               .get(ENDPOINT + "?customerId=" + ID);

		Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK.value());

		BookingDTO[] responseBookingDTOs = response.body().as(BookingDTO[].class);
		Assertions.assertThat(responseBookingDTOs.length).isEqualTo(1);
		Assertions.assertThat(responseBookingDTOs[0].getId()).isEqualTo(fakeBooking.getId());
	}

	@Test
	public void payBooking_shouldFail()
	{
		SectionBookingDTO bookingDTO = new SectionBookingDTO();
		bookingDTO.setId(11L);
		bookingDTO.setReservationNumber(11);
		bookingDTO.setCreatedAt(LocalDateTime.now());
		bookingDTO.setPaid(false);
		bookingDTO.setSections(new HashSet<>());
		bookingDTO.setEvent(new EventDTO());
		bookingDTO.setCustomer(new CustomerDTO());
		bookingDTO.setPaymentMethod(PaymentProvider.STRIPE.name());

		SectionBooking booking = new SectionBooking();
		booking.setId(11L);
		booking.setReservationNumber(11);
		booking.setCreatedAt(LocalDateTime.now());
		booking.setPaid(false);
		booking.setEntries(new HashSet<>());
		booking.setEvent(new Event());
		booking.setCustomer(new Customer());

		CreditCardDTO creditCardDTO = new CreditCardDTO();
		creditCardDTO.setCardNumber("4242424242424242");
		creditCardDTO.setCvc(123);
		creditCardDTO.setExpirationMonth(1);
		creditCardDTO.setExpirationYear(20);

		PaymentContext paymentContext = new PaymentContext();
		paymentContext.setBookingDTO(bookingDTO);
		paymentContext.setCreditCardDTO(creditCardDTO);

		BDDMockito.given(repository.findOne(Mockito.anyLong())).willReturn(booking);
		BDDMockito.given(repository.save(Mockito.any(SectionBooking.class))).willReturn(booking);

		Response response = RestAssured.given()
		                               .contentType(ContentType.JSON)
		                               .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
		                               .body(paymentContext)
		                               .then()
		                               .post(ENDPOINT + "/pay");

		Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST.value());
	}
}
