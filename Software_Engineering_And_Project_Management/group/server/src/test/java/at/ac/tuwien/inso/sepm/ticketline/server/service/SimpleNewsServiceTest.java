package at.ac.tuwien.inso.sepm.ticketline.server.service;

import at.ac.tuwien.inso.sepm.ticketline.rest.news.SimpleNewsDTO;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.News;
import at.ac.tuwien.inso.sepm.ticketline.server.exception.NotFoundException;
import at.ac.tuwien.inso.sepm.ticketline.server.exception.ValidationException;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.NewsRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SimpleNewsServiceTest
{
	@MockBean
	NewsRepository repository;

	@Autowired
	NewsService service;

	private final Long ID = 1L;
	private final LocalDateTime TIMESTAMP = LocalDateTime.of(2000, 1, 1, 1, 1);
	private final String TITLE = "TEST STORY";
	private final String TEXT = "TEST TEXT";

	@Test
	public void publishNews_success() throws ValidationException
	{
		News news = News.builder().id(ID).title(TITLE).text(TEXT).publishedAt(TIMESTAMP).build();

		BDDMockito.given(repository.save(news)).willReturn(news);
		News saved = service.publishNews(news);

		Assert.assertThat(saved, is(news));
	}

	@Test(expected = ValidationException.class)
	public void publishNews_invalid() throws ValidationException
	{
		News news = News.builder().id(ID).publishedAt(TIMESTAMP).build();

		BDDMockito.given(repository.save(news)).willReturn(news);
		service.publishNews(news);
	}

	@Test
	public void retrieveNewsPage_success()
	{
		News news = News.builder().id(ID).publishedAt(TIMESTAMP).title(TITLE).text(TEXT).build();

		List<News> newsList = new LinkedList<>();
		newsList.add(news);

		PageRequest pageRequest = new PageRequest(1, 1);
		Page<News> newsPage = new PageImpl<>(newsList, pageRequest, 2);

		BDDMockito.given(repository.findAllByOrderByPublishedAtDesc(pageRequest)).willReturn(newsPage);

		Page<SimpleNewsDTO> retrieved = service.findAll(pageRequest, "admin");
		Assert.assertThat(retrieved, notNullValue());
		Assert.assertThat(retrieved.getContent().get(0).getId(), is(ID));
		Assert.assertThat(retrieved.getContent().get(0).getTitle(), is(TITLE));
	}

	@Test(expected = NotFoundException.class)
	public void retrieveNewsPage_notFound()
	{
		News news = News.builder().id(ID).publishedAt(TIMESTAMP).title(TITLE).text(TEXT).build();

		List<News> newsList = new LinkedList<>();
		newsList.add(news);

		PageRequest pageRequest = new PageRequest(1, 1);
		Page<News> newsPage = new PageImpl<>(newsList, pageRequest, 2);

		BDDMockito.given(repository.findAllByOrderByPublishedAtDesc(pageRequest)).willReturn(newsPage);

		service.findAll(new PageRequest(3, 1), "admin");
	}
}
