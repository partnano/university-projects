package at.ac.tuwien.inso.sepm.ticketline.server.endpoint;

import at.ac.tuwien.inso.sepm.ticketline.rest.location.LocationDTO;
import at.ac.tuwien.inso.sepm.ticketline.server.configuration.JacksonConfiguration;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Location;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.location.LocationMapper;
import at.ac.tuwien.inso.sepm.ticketline.server.security.AuthenticationConstants;
import at.ac.tuwien.inso.sepm.ticketline.server.service.implementation.SimpleHeaderTokenAuthenticationService;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.config.ObjectMapperConfig;
import com.jayway.restassured.config.RestAssuredConfig;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;
import org.assertj.core.api.Assertions;
import org.assertj.core.util.Strings;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.core.Is.is;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class LocationEndpointTest
{
	@MockBean
	private LocationMapper locationMapper;

	private static final String SERVER_HOST = "http://localhost";
	private static final String USER_USERNAME = "user";
	private static final String USER_PASSWORD = "password";
	private static final String ADMIN_PASSWORD = "password";
	private static final String ADMIN_USERNAME = "admin";

	@Value("${server.context-path}")
	private String contextPath;

	@LocalServerPort
	private int port;

	@Autowired
	private SimpleHeaderTokenAuthenticationService authenticationService;

	@Autowired
	private JacksonConfiguration jacksonConfiguration;

	protected String validUserTokenWithPrefix;
	protected String validAdminTokenWithPrefix;

	private static final String LOCATION_NAME1 = "Test Location";
	private static final String LOCATION_NAME2 = "Another Test Location";
	private static final String LOCATION_STREET = "Test Street";
	private static final String LOCATION_CITY = "Test City";
	private static final String LOCATION_ZIP = "1234";
	private static final String LOCATION_COUNTRY = "Test Country";

	private static final String LOCATIONS_ENDPOINT = "/locations";

	@Before
	public void beforeBase()
	{
		RestAssured.baseURI = SERVER_HOST;
		RestAssured.basePath = contextPath;
		RestAssured.port = port;
		RestAssured.config = RestAssuredConfig.config()
		                                      .objectMapperConfig(new ObjectMapperConfig().jackson2ObjectMapperFactory(
			                                      (aClass, s) -> jacksonConfiguration.jackson2ObjectMapperBuilder()
			                                                                         .build()));

		validUserTokenWithPrefix = Strings.join(AuthenticationConstants.TOKEN_PREFIX,
		                                        authenticationService.authenticate(USER_USERNAME, USER_PASSWORD)
		                                                             .getCurrentToken()).with(" ");

		validAdminTokenWithPrefix = Strings.join(AuthenticationConstants.TOKEN_PREFIX,
		                                         authenticationService.authenticate(ADMIN_USERNAME,
		                                                                            ADMIN_PASSWORD)
		                                                              .getCurrentToken()).with(" ");
	}

	@Test
	public void retrieveLocationsAuthorized()
	{
		LocationDTO location = LocationDTO.builder()
		                                  .name(LOCATION_NAME1)
		                                  .street(LOCATION_STREET)
		                                  .city(LOCATION_CITY)
		                                  .zip(LOCATION_ZIP)
		                                  .country(LOCATION_COUNTRY)
		                                  .build();

		List<LocationDTO> list = new ArrayList<>();
		list.add(location);

		BDDMockito.given(locationMapper.locationToLocationDTO(Mockito.anyListOf(Location.class)))
		          .willReturn(list);

		Response response = RestAssured.given()
		                               .contentType(ContentType.JSON)
		                               .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
		                               .when()
		                               .get(LOCATIONS_ENDPOINT)
		                               .then()
		                               .extract()
		                               .response();

		Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK.value());
		Assertions.assertThat(Arrays.asList(response.as(LocationDTO[].class))).isEqualTo(list);
	}

	@Test
	public void retrieveLocationsUnauthorized()
	{
		Response response = RestAssured.given()
		                               .contentType(ContentType.JSON)
		                               .when()
		                               .get(LOCATIONS_ENDPOINT)
		                               .then()
		                               .extract()
		                               .response();

		Assert.assertThat(response.getStatusCode(), is(HttpStatus.UNAUTHORIZED.value()));
	}
}
