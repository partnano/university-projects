package at.ac.tuwien.inso.sepm.ticketline.server.mapper;

import at.ac.tuwien.inso.sepm.ticketline.rest.location.AreaDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.location.LocationDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.location.RowDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.location.SeatDTO;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Area;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Location;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Row;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Seat;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.location.LocationMapper;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.SeatRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Collections;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
public class LocationMapperTest
{
	@Configuration
	@ComponentScan(basePackages = "at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper")
	public static class LocationMapperTestContextConfiguration
	{
	}

	@MockBean
	SeatRepository seatRepository;

	@Autowired
	@SuppressWarnings("SpringJavaAutowiredMembersInspection")
	private LocationMapper mapper;

	private static final long LOCATION_ID = 1L;
	private static final String LOCATION_NAME = "Nightmare";
	private static final String LOCATION_STREET = "Elm Street";
	private static final String LOCATION_ZIP = "636";

	private static final Long SEAT_ID = 105L;
	private static final String SEAT_NAME = "test";

	private Set<Area> getRows(Location location)
	{
		Seat seat = Seat.builder().id(SEAT_ID).name(SEAT_NAME).build();

		Row row = Row.builder().id(10L).seats(Collections.singleton(seat)).location(location).build();

		seat.setRow(row);

		return Collections.singleton(row);
	}

	private Set<AreaDTO> getRowDTOs(LocationDTO location)
	{
		SeatDTO seat = SeatDTO.builder().id(SEAT_ID).name(SEAT_NAME).build();

		RowDTO row = RowDTO.builder().id(10L).seats(Collections.singleton(seat)).location(location).build();

		seat.setRow(row);

		return Collections.singleton(row);
	}

	@Test
	public void shouldMapLocationToLocationDTO()
	{
		Location location = Location.builder()
		                            .id(LOCATION_ID)
		                            .name(LOCATION_NAME)
		                            .street(LOCATION_STREET)
		                            .zip(LOCATION_ZIP)
		                            .build();

		Set<Area> areas = getRows(location);
		location.setAreas(areas);

		LocationDTO locationDTO = mapper.locationToLocationDTO(location);
		assertThat(locationDTO).isNotNull();
		assertThat(locationDTO.getId()).isEqualTo(LOCATION_ID);
		assertThat(locationDTO.getName()).isEqualTo(LOCATION_NAME);
		assertThat(locationDTO.getStreet()).isEqualTo(LOCATION_STREET);
		assertThat(locationDTO.getZip()).isEqualTo(LOCATION_ZIP);

		// Test bidirectional relation (check if areaDTO references back to locationDTO)

		assertThat(locationDTO.getAreas()).isNotNull();
		assertThat(locationDTO.getAreas().size()).isEqualTo(areas.size());

		for(AreaDTO areaDTO : locationDTO.getAreas())
		{
			assertThat(areaDTO.getLocation()).isNull();
		}
	}

	@Test
	public void shouldMapLocationDTOToLocation()
	{
		LocationDTO locationDTO = LocationDTO.builder()
		                                     .id(LOCATION_ID)
		                                     .name(LOCATION_NAME)
		                                     .street(LOCATION_STREET)
		                                     .zip(LOCATION_ZIP)
		                                     .build();

		Set<AreaDTO> areas = getRowDTOs(locationDTO);
		locationDTO.setAreas(areas);

		Location location = mapper.locationDTOtoLocation(locationDTO);
		assertThat(location).isNotNull();
		assertThat(location.getId()).isEqualTo(LOCATION_ID);
		assertThat(location.getName()).isEqualTo(LOCATION_NAME);
		assertThat(location.getStreet()).isEqualTo(LOCATION_STREET);
		assertThat(location.getZip()).isEqualTo(LOCATION_ZIP);

		// Test bidirectional relation (check if areaDTO references back to locationDTO)

		assertThat(location.getAreas()).isNotNull();
		assertThat(location.getAreas().size()).isEqualTo(areas.size());

		for(Area area : location.getAreas())
		{
			assertThat(area.getLocation()).isNull();
		}
	}
}
