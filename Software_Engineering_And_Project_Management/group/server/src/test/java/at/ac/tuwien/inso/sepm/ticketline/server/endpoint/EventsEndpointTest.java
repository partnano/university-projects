package at.ac.tuwien.inso.sepm.ticketline.server.endpoint;

import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventFilterDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.enums.Category;
import at.ac.tuwien.inso.sepm.ticketline.server.configuration.JacksonConfiguration;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Event;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.event.EventMapper;
import at.ac.tuwien.inso.sepm.ticketline.server.security.AuthenticationConstants;
import at.ac.tuwien.inso.sepm.ticketline.server.service.EventService;
import at.ac.tuwien.inso.sepm.ticketline.server.service.implementation.SimpleHeaderTokenAuthenticationService;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.config.ObjectMapperConfig;
import com.jayway.restassured.config.RestAssuredConfig;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;
import org.assertj.core.api.Assertions;
import org.assertj.core.util.Strings;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.mockito.BDDMockito.given;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class EventsEndpointTest
{
	@MockBean
	private EventMapper eventMapper;

	@MockBean
	private EventService eventService;

	private static final String SERVER_HOST = "http://localhost";
	private static final String USER_USERNAME = "user";
	private static final String USER_PASSWORD = "password";
	private static final String ADMIN_PASSWORD = "password";
	private static final String ADMIN_USERNAME = "admin";

	@Value("${server.context-path}")
	private String contextPath;

	@LocalServerPort
	private int port;

	@Autowired
	private SimpleHeaderTokenAuthenticationService authenticationService;

	@Autowired
	private JacksonConfiguration jacksonConfiguration;

	protected String validUserTokenWithPrefix;
	protected String validAdminTokenWithPrefix;

	private static final String EVENTS_ENDPOINT = "/events";
	private static final String EVENTS_FILTER_ENDPOINT = EVENTS_ENDPOINT + "/filtered";

	private static final long EVENT_ID = 1L;
	private static final String EVENT_NAME = "Test Event";
	private static final String EVENT_ARTIST = "Madonna Donna";
	private static final Category EVENT_CATEGORY = Category.CONCERT;
	private static final LocalDateTime EVENT_TIME = LocalDateTime.of(2017, 1, 1, 1, 1, 1, 1);

	@Before
	public void beforeBase()
	{
		RestAssured.baseURI = SERVER_HOST;
		RestAssured.basePath = contextPath;
		RestAssured.port = port;
		RestAssured.config = RestAssuredConfig.config()
		                                      .objectMapperConfig(new ObjectMapperConfig().jackson2ObjectMapperFactory(
			                                      (aClass, s) -> jacksonConfiguration.jackson2ObjectMapperBuilder()
			                                                                         .build()));

		validUserTokenWithPrefix = Strings.join(AuthenticationConstants.TOKEN_PREFIX,
		                                        authenticationService.authenticate(USER_USERNAME, USER_PASSWORD)
		                                                             .getCurrentToken()).with(" ");

		validAdminTokenWithPrefix = Strings.join(AuthenticationConstants.TOKEN_PREFIX,
		                                         authenticationService.authenticate(ADMIN_USERNAME,
		                                                                            ADMIN_PASSWORD)
		                                                              .getCurrentToken()).with(" ");
	}

	@Test
	public void retrieveEvents_success() throws Exception
	{
		EventDTO event = EventDTO.builder()
		                         .id(EVENT_ID)
		                         .name(EVENT_NAME)
		                         .artist(EVENT_ARTIST)
		                         .category(EVENT_CATEGORY)
		                         .time(EVENT_TIME)
		                         .build();

		List<EventDTO> list = new ArrayList<>();
		list.add(event);

		given(eventMapper.eventToEventDTO(Mockito.anyListOf(Event.class))).willReturn(list);

		Response response = RestAssured.given()
		                               .contentType(ContentType.JSON)
		                               .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
		                               .when()
		                               .get(EVENTS_ENDPOINT)
		                               .then()
		                               .extract()
		                               .response();

		Assert.assertThat(response.getStatusCode(), is(HttpStatus.OK.value()));
		Assert.assertThat(Arrays.asList(response.as(EventDTO[].class)), is(list));
	}

	@Test
	public void retrieveEventsUnAuthorized()
	{
		Response response = RestAssured.given()
		                               .contentType(ContentType.JSON)
		                               .when()
		                               .get(EVENTS_ENDPOINT)
		                               .then()
		                               .extract()
		                               .response();

		Assert.assertThat(response.getStatusCode(), is(HttpStatus.UNAUTHORIZED.value()));
	}

	@Test
	public void filterEventsAuthorized()
	{
		EventDTO event = EventDTO.builder()
		                         .id(EVENT_ID)
		                         .name(EVENT_NAME)
		                         .artist(EVENT_ARTIST)
		                         .category(EVENT_CATEGORY)
		                         .time(EVENT_TIME)
		                         .build();

		List<EventDTO> list = new ArrayList<>();
		list.add(event);

		BDDMockito.given(eventMapper.eventToEventDTO(Mockito.anyListOf(Event.class))).willReturn(list);

		Response response = RestAssured.given()
		                               .contentType(ContentType.JSON)
		                               .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
		                               .body(new EventFilterDTO())
		                               .when()
		                               .post(EVENTS_FILTER_ENDPOINT)
		                               .then()
		                               .extract()
		                               .response();

		Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK.value());
		Assertions.assertThat(Arrays.asList(response.as(EventDTO[].class))).isEqualTo(list);
	}

	@Test
	public void filterEventsUnauthorized()
	{
		Response response = RestAssured.given()
		                               .contentType(ContentType.JSON)
		                               .body(new EventFilterDTO())
		                               .when()
		                               .post(EVENTS_FILTER_ENDPOINT)
		                               .then()
		                               .extract()
		                               .response();

		Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.UNAUTHORIZED.value());
	}

	@Test
	public void createEventAuthorized()
	{
		EventDTO event = EventDTO.builder()
		                         .id(EVENT_ID)
		                         .name(EVENT_NAME)
		                         .artist(EVENT_ARTIST)
		                         .category(EVENT_CATEGORY)
		                         .time(EVENT_TIME)
		                         .build();

		BDDMockito.given(eventMapper.eventDTOtoEvent(Mockito.any(EventDTO.class))).willReturn(null);
		BDDMockito.given(eventService.create(Mockito.any(Event.class))).willReturn(null);
		BDDMockito.given(eventMapper.eventToEventDTO(Mockito.any(Event.class))).willReturn(event);

		Response response = RestAssured.given()
		                               .contentType(ContentType.JSON)
		                               .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
		                               .body(event)
		                               .when()
		                               .post(EVENTS_ENDPOINT)
		                               .then()
		                               .extract()
		                               .response();

		Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK.value());
		Assertions.assertThat(response.as(EventDTO.class)).isEqualTo(event);
	}

	@Test
	public void createEventUnauthorized()
	{
		EventDTO event = EventDTO.builder()
		                         .id(EVENT_ID)
		                         .name(EVENT_NAME)
		                         .artist(EVENT_ARTIST)
		                         .category(EVENT_CATEGORY)
		                         .time(EVENT_TIME)
		                         .build();

		Response response = RestAssured.given()
		                               .contentType(ContentType.JSON)
		                               .body(event)
		                               .when()
		                               .post(EVENTS_ENDPOINT)
		                               .then()
		                               .extract()
		                               .response();

		Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.UNAUTHORIZED.value());
	}
}
