package at.ac.tuwien.inso.sepm.ticketline.server.mapper;

import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventAreaDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.enums.Category;
import at.ac.tuwien.inso.sepm.ticketline.rest.location.AreaDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.location.RowDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.location.SeatDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.location.SectionDTO;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.*;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.event.EventMapper;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.SeatRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
public class EventMapperTest
{
	@Configuration
	@ComponentScan(basePackages = "at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper")
	public static class EventMapperTestContextConfiguration
	{
	}

	@MockBean
	SeatRepository seatRepository;

	@Autowired
	@SuppressWarnings("SpringJavaAutowiredMembersInspection")
	private EventMapper mapper;

	private static final long EVENT_ID = 1L;
	private static final String EVENT_NAME = "Test Event";
	private static final String EVENT_ARTIST = "Madonna Donna";
	private static final Category EVENT_CATEGORY = Category.CONCERT;
	private static final LocalDateTime EVENT_TIME = LocalDateTime.of(2017, 1, 1, 1, 1, 1, 1);
	private static final int SECTION_CAPACITY = 500;
	private static final Long SEAT_ID = 105L;
	private static final String SEAT_NAME = "test";

	private Set<EventArea> getEventSections(Event event)
	{
		return Collections.singleton(EventArea.builder()
		                                      .area(Section.builder()
		                                                   .id(10L)
		                                                   .capacity(SECTION_CAPACITY)
		                                                   .build())
		                                      .event(event)
		                                      .build());
	}

	private Set<EventAreaDTO> getEventSectionsDTO(EventDTO event)
	{
		return Collections.singleton(EventAreaDTO.builder()
		                                         .area(SectionDTO.builder()
		                                                         .id(10L)
		                                                         .capacity(SECTION_CAPACITY)
		                                                         .build())
		                                         .event(event)
		                                         .build());
	}

	private Set<EventArea> getEventRows(Event event)
	{
		Seat seat = Seat.builder().id(SEAT_ID).name(SEAT_NAME).build();

		Row row = Row.builder().id(10L).seats(Collections.singleton(seat)).build();

		seat.setRow(row);

		return Collections.singleton(EventArea.builder().area(row).event(event).build());
	}

	private Set<EventAreaDTO> getEventRowsDTO(EventDTO event)
	{
		SeatDTO seat = SeatDTO.builder().id(SEAT_ID).name(SEAT_NAME).build();

		RowDTO row = RowDTO.builder().id(10L).seats(Collections.singleton(seat)).build();

		return Collections.singleton(EventAreaDTO.builder().area(row).event(event).build());
	}

	@Test
	public void shouldMapEventToEventDTO()
	{
		Event event = Event.builder()
		                   .id(EVENT_ID)
		                   .name(EVENT_NAME)
		                   .artist(EVENT_ARTIST)
		                   .time(EVENT_TIME)
		                   .category(EVENT_CATEGORY)
		                   .build();

		EventDTO eventDTO = mapper.eventToEventDTO(event);
		assertThat(eventDTO).isNotNull();
		assertThat(eventDTO.getId()).isEqualTo(EVENT_ID);
		assertThat(eventDTO.getArtist()).isEqualTo(EVENT_ARTIST);
		assertThat(eventDTO.getCategory()).isEqualTo(EVENT_CATEGORY);
		assertThat(eventDTO.getTime()).isEqualTo(EVENT_TIME);
	}

	@Test
	public void shouldMapEventDTOtoEvent()
	{
		EventDTO eventDTO = EventDTO.builder()
		                            .id(EVENT_ID)
		                            .name(EVENT_NAME)
		                            .artist(EVENT_ARTIST)
		                            .time(EVENT_TIME)
		                            .category(EVENT_CATEGORY)
		                            .build();

		Event event = mapper.eventDTOtoEvent(eventDTO);
		assertThat(event).isNotNull();
		assertThat(event.getId()).isEqualTo(EVENT_ID);
		assertThat(event.getArtist()).isEqualTo(EVENT_ARTIST);
		assertThat(event.getCategory()).isEqualTo(EVENT_CATEGORY);
		assertThat(event.getTime()).isEqualTo(EVENT_TIME);
	}

	@Test
	public void shouldMapEventAreasToEventAreasDTO()
	{
		Event event = Event.builder().id(EVENT_ID).build();
		event.setEventAreas(getEventSections(event));

		EventDTO eventDTO = mapper.eventToEventDTO(event);

		for(EventAreaDTO eventArea : eventDTO.getEventAreas())
		{
			assertThat(eventArea.getEvent()).isNull();
		}
	}

	@Test
	public void shouldMapEventAreasDTOToEventAreas()
	{
		EventDTO eventDTO = EventDTO.builder().id(EVENT_ID).build();
		eventDTO.setEventAreas(getEventRowsDTO(eventDTO));

		Event event = mapper.eventDTOtoEvent(eventDTO);

		for(EventArea eventArea : event.getEventAreas())
		{
			assertThat(eventArea.getEvent()).isNull();
		}
	}

	@Test
	public void shouldMapEventSectionsToSectionDTO()
	{
		Event event = Event.builder().id(EVENT_ID).build();
		event.setEventAreas(getEventSections(event));

		EventDTO eventDTO = mapper.eventToEventDTO(event);

		for(EventAreaDTO eventArea : eventDTO.getEventAreas())
		{
			AreaDTO area = eventArea.getArea();
			assertThat(area).isInstanceOf(SectionDTO.class);
			assertThat(((SectionDTO)area).getCapacity()).isEqualTo(SECTION_CAPACITY);
		}
	}

	@Test
	public void shouldMapEventSectionsDTOToSection()
	{
		EventDTO eventDTO = EventDTO.builder().id(EVENT_ID).build();
		eventDTO.setEventAreas(getEventSectionsDTO(eventDTO));

		Event event = mapper.eventDTOtoEvent(eventDTO);

		for(EventArea eventArea : event.getEventAreas())
		{
			Area area = eventArea.getArea();
			assertThat(area).isInstanceOf(Section.class);
			assertThat(((Section)area).getCapacity()).isEqualTo(SECTION_CAPACITY);
		}
	}

	@Test
	public void shouldMapEventRowsToRowDTO()
	{
		Event event = Event.builder().id(EVENT_ID).build();
		event.setEventAreas(getEventRows(event));

		EventDTO eventDTO = mapper.eventToEventDTO(event);

		for(EventAreaDTO eventArea : eventDTO.getEventAreas())
		{
			AreaDTO area = eventArea.getArea();
			assertThat(area).isInstanceOf(RowDTO.class);

			for(SeatDTO seat : ((RowDTO)area).getSeats())
			{
				assertThat(seat.getName()).isEqualTo(SEAT_NAME);
				assertThat(seat.getId()).isEqualTo(SEAT_ID);
			}
		}
	}

	@Test
	public void shouldMapEventRowDTOToRow()
	{
		EventDTO eventDTO = EventDTO.builder().id(EVENT_ID).build();
		eventDTO.setEventAreas(getEventRowsDTO(eventDTO));

		Event event = mapper.eventDTOtoEvent(eventDTO);

		for(EventArea eventArea : event.getEventAreas())
		{
			Area area = eventArea.getArea();
			assertThat(area).isInstanceOf(Row.class);

			for(Seat seat : ((Row)area).getSeats())
			{
				assertThat(seat.getName()).isEqualTo(SEAT_NAME);
				assertThat(seat.getId()).isEqualTo(SEAT_ID);
			}
		}
	}
}
