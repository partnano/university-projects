package at.ac.tuwien.inso.sepm.ticketline.server.integrationtest.base;

import at.ac.tuwien.inso.sepm.ticketline.server.configuration.JacksonConfiguration;
import at.ac.tuwien.inso.sepm.ticketline.server.security.AuthenticationConstants;
import at.ac.tuwien.inso.sepm.ticketline.server.service.UserService;
import at.ac.tuwien.inso.sepm.ticketline.server.service.implementation.SimpleHeaderTokenAuthenticationService;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.config.ObjectMapperConfig;
import com.jayway.restassured.config.RestAssuredConfig;
import org.assertj.core.util.Strings;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("integration-test")
public abstract class BaseIntegrationTest
{
	private static final String SERVER_HOST = "http://localhost";
	private static final String USER_USERNAME = "user";
	private static final String USER_PASSWORD = "password";
	private static final String ADMIN_PASSWORD = "password";
	private static final String ADMIN_USERNAME = "admin";

	@Value("${server.context-path}")
	private String contextPath;

	@LocalServerPort
	private int port;

	@Autowired
	private SimpleHeaderTokenAuthenticationService authenticationService;

	@Autowired
	private JacksonConfiguration jacksonConfiguration;

	@Autowired
	private UserService userService;

	protected String validUserTokenWithPrefix;
	protected String validAdminTokenWithPrefix;

	@Before
	public void beforeBase()
	{
		RestAssured.baseURI = SERVER_HOST;
		RestAssured.basePath = contextPath;
		RestAssured.port = port;
		RestAssured.config = RestAssuredConfig.config().
			objectMapperConfig(new ObjectMapperConfig().jackson2ObjectMapperFactory((aClass, s) -> jacksonConfiguration
				.jackson2ObjectMapperBuilder()
				.build()));

		validUserTokenWithPrefix = Strings.join(AuthenticationConstants.TOKEN_PREFIX,
		                                        authenticationService.authenticate(USER_USERNAME, USER_PASSWORD)
		                                                             .getCurrentToken()).with(" ");

		validAdminTokenWithPrefix = Strings.join(AuthenticationConstants.TOKEN_PREFIX,
		                                         authenticationService.authenticate(ADMIN_USERNAME,
		                                                                            ADMIN_PASSWORD)
		                                                              .getCurrentToken()).with(" ");
	}
}
