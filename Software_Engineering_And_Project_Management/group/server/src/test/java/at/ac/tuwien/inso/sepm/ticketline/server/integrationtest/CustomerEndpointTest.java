package at.ac.tuwien.inso.sepm.ticketline.server.integrationtest;

import at.ac.tuwien.inso.sepm.ticketline.rest.customer.CustomerDTO;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Customer;
import at.ac.tuwien.inso.sepm.ticketline.server.integrationtest.base.BaseIntegrationTest;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.CustomerRepository;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class CustomerEndpointTest extends BaseIntegrationTest
{
	private static final String ENDPOINT = "/customers";
	private static final String CUSTOMER_PATH = "/{id}";

	private static final long INVALID_ID = 42;
	private static final long ID = 1;
	private static final String FIRST_NAME = "tester";
	private static final String LAST_NAME = "testmaster";
	private static final String EMAIL = "tester@testing.com";

	@MockBean
	private CustomerRepository repository;

	@Test
	public void findsAllCustomers()
	{
		Customer customer = new Customer();
		customer.setId(ID);
		customer.setFirstName(FIRST_NAME);
		customer.setLastName(LAST_NAME);
		customer.setEmail(EMAIL);

		BDDMockito.given(repository.findAll()).willReturn(Collections.singletonList(customer));

		Response response = RestAssured.given()
		                               .contentType(ContentType.JSON)
		                               .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
		                               .when()
		                               .get(ENDPOINT)
		                               .then()
		                               .extract()
		                               .response();

		Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK.value());

		CustomerDTO dto = new CustomerDTO();
		dto.setId(ID);
		dto.setFirstName(FIRST_NAME);
		dto.setLastName(LAST_NAME);
		dto.setEmail(EMAIL);

		List<CustomerDTO> expected = Collections.singletonList(dto);
		List<CustomerDTO> actual = Arrays.asList(response.getBody().as(CustomerDTO[].class));
		Assertions.assertThat(actual).isEqualTo(expected);
	}

	@Test
	public void findsSpecificCustomer()
	{
		Customer customer = new Customer();
		customer.setId(ID);
		customer.setFirstName(FIRST_NAME);
		customer.setLastName(LAST_NAME);
		customer.setEmail(EMAIL);

		BDDMockito.given(repository.findOne(ID)).willReturn(customer);

		Response response = RestAssured.given()
		                               .contentType(ContentType.JSON)
		                               .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
		                               .when()
		                               .get(ENDPOINT + CUSTOMER_PATH, ID)
		                               .then()
		                               .extract()
		                               .response();

		Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK.value());

		CustomerDTO dto = new CustomerDTO();
		dto.setId(ID);
		dto.setFirstName(FIRST_NAME);
		dto.setLastName(LAST_NAME);
		dto.setEmail(EMAIL);

		Assertions.assertThat(response.getBody().as(CustomerDTO.class)).isEqualTo(dto);
	}

	@Test
	public void returnsErrorOnNonExistentCustomer()
	{
		Customer customer = new Customer();
		customer.setId(ID);
		customer.setFirstName(FIRST_NAME);
		customer.setLastName(LAST_NAME);
		customer.setEmail(EMAIL);

		BDDMockito.given(repository.findOne(ID)).willReturn(customer);
		BDDMockito.given(repository.findOne(INVALID_ID)).willReturn(null);

		Response response = RestAssured.given()
		                               .contentType(ContentType.JSON)
		                               .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
		                               .when()
		                               .get(ENDPOINT + CUSTOMER_PATH, INVALID_ID)
		                               .then()
		                               .extract()
		                               .response();

		Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND.value());
	}

	@Test
	public void createsNewCustomerSuccessfully()
	{
		Customer customer = new Customer();
		customer.setFirstName(FIRST_NAME);
		customer.setLastName(LAST_NAME);
		customer.setEmail(EMAIL);

		Response response = RestAssured.given()
		                               .contentType(ContentType.JSON)
		                               .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
		                               .body(customer)
		                               .when()
		                               .post(ENDPOINT)
		                               .then()
		                               .extract()
		                               .response();

		Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK.value());
	}

	/*@Test
	public void rejectsCreatingInvalidCustomer()
	{
		CustomerDTO customer = new CustomerDTO();
		customer.setFirstName(FIRST_NAME);
		customer.setLastName(LAST_NAME);
		// no email

		Response response = RestAssured.given()
		                               .contentType(ContentType.JSON)
		                               .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
		                               .body(customer)
		                               .when()
		                               .post(ENDPOINT)
		                               .then()
		                               .extract()
		                               .response();

		Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST.value());
	}*/

	@Test
	public void updateUserSuccessfully()
	{
		Customer customer = new Customer();
		customer.setId(ID);
		customer.setFirstName(FIRST_NAME);
		customer.setLastName(LAST_NAME);
		customer.setEmail(EMAIL);

		Response response = RestAssured.given()
		                               .contentType(ContentType.JSON)
		                               .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
		                               .body(customer)
		                               .when().put(ENDPOINT + CUSTOMER_PATH, ID)
		                               .then()
		                               .extract()
		                               .response();

		Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK.value());
	}

	@Test
	public void updateUserFail()
	{
		Customer customer = new Customer();
		customer.setId(ID);
		customer.setFirstName(FIRST_NAME);
		customer.setLastName(LAST_NAME);

		Response response = RestAssured.given()
		                               .contentType(ContentType.JSON)
		                               .header(HttpHeaders.AUTHORIZATION, validUserTokenWithPrefix)
		                               .body(customer)
		                               .when().put(ENDPOINT + CUSTOMER_PATH, ID)
		                               .then()
		                               .extract()
		                               .response();

		Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST.value());
	}
}
