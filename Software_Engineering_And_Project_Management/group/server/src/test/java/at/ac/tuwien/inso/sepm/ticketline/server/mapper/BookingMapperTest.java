package at.ac.tuwien.inso.sepm.ticketline.server.mapper;

import at.ac.tuwien.inso.sepm.ticketline.rest.booking.BookingDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.booking.SeatBookingDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.booking.SectionBookingDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.booking.SectionBookingEntryDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.location.SeatDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.location.SectionDTO;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.*;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.booking.BookingMapper;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.SeatRepository;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

@RunWith(SpringJUnit4ClassRunner.class)
public class BookingMapperTest
{
	@Configuration
	@ComponentScan(basePackages = "at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper")
	public static class BookingMapperTestContextConfiguration
	{
	}

	@MockBean
	SeatRepository seatRepository;

	@Autowired
	@SuppressWarnings("SpringJavaAutowiredMembersInspection")
	BookingMapper mapper;

	private List<Booking> getSectionBookings()
	{
		SectionBooking b1 = new SectionBooking();
		b1.setId(1L);

		Set<SectionBookingEntry> sections = new HashSet<>();

		Section section = Section.builder().id(10L).capacity(100).build();

		SectionBookingEntry entry = new SectionBookingEntry();
		entry.setSection(section);
		entry.setAmount(5L);
		entry.setBooking(b1);
		sections.add(entry);

		b1.setEntries(sections);

		List<Booking> bookings = new LinkedList<>();
		bookings.add(b1);
		return bookings;
	}

	private List<BookingDTO> getSectionBookingsDTOs()
	{
		SectionBookingDTO b1 = new SectionBookingDTO();
		b1.setId(1L);

		Set<SectionBookingEntryDTO> sections = new HashSet<>();

		SectionDTO section = SectionDTO.builder().id(10L).capacity(100).build();

		SectionBookingEntryDTO entry = new SectionBookingEntryDTO();
		entry.setSection(section);
		entry.setAmount(5L);
		sections.add(entry);

		b1.setSections(sections);

		List<BookingDTO> bookings = new LinkedList<>();
		bookings.add(b1);
		return bookings;
	}

	private List<Booking> getSeatBookings()
	{
		SeatBooking b1 = new SeatBooking();
		b1.setId(1L);

		Set<Seat> seats = new HashSet<>();

		Seat seat = Seat.builder().id(10L).name("test").build();

		seats.add(seat);

		b1.setSeats(seats);

		List<Booking> bookings = new LinkedList<>();
		bookings.add(b1);
		return bookings;
	}

	private List<BookingDTO> getSeatBookingsDTOs()
	{
		SeatBookingDTO b1 = new SeatBookingDTO();
		b1.setId(1L);

		Set<SeatDTO> seats = new HashSet<>();

		SeatDTO seat = SeatDTO.builder().id(10L).name("test").build();

		seats.add(seat);

		b1.setSeats(seats);

		List<BookingDTO> bookings = new LinkedList<>();
		bookings.add(b1);
		return bookings;
	}

	@Test
	public void shouldMapSeatBookingListToSeatBookingDTOList()
	{
		List<Booking> bookings = getSeatBookings();

		List<BookingDTO> bookingDTOS = mapper.bookingsToBookingDTOs(bookings);

		Assertions.assertThat(bookingDTOS).isNotNull();
		Assertions.assertThat(bookingDTOS.size()).isEqualTo(bookings.size());
		Assertions.assertThat(bookingDTOS.get(0)).isInstanceOf(SeatBookingDTO.class);

		SeatBookingDTO seatBookingDTO = (SeatBookingDTO)bookingDTOS.get(0);
		Assertions.assertThat(seatBookingDTO.getSeats().size()).isEqualTo(1);
	}

	@Test
	public void shouldMapSectionBookingListToSectionBookingDTOList()
	{
		List<Booking> bookings = getSectionBookings();

		List<BookingDTO> bookingDTOS = mapper.bookingsToBookingDTOs(bookings);

		Assertions.assertThat(bookingDTOS).isNotNull();
		Assertions.assertThat(bookingDTOS.size()).isEqualTo(bookings.size());
		Assertions.assertThat(bookingDTOS.get(0)).isInstanceOf(SectionBookingDTO.class);

		SectionBookingDTO seatBookingDTO = (SectionBookingDTO)bookingDTOS.get(0);
		Assertions.assertThat(seatBookingDTO.getSections().size()).isEqualTo(1);
	}

	@Test
	public void shouldMapSeatBookingDTOListToSeatBookingList()
	{
		List<BookingDTO> bookingsDTOs = getSeatBookingsDTOs();

		List<Booking> bookings = mapper.bookingDTOsToBookings(bookingsDTOs);

		Assertions.assertThat(bookings).isNotNull();
		Assertions.assertThat(bookings.size()).isEqualTo(bookings.size());
		Assertions.assertThat(bookings.get(0)).isInstanceOf(SeatBooking.class);

		SeatBooking seatBooking = (SeatBooking)bookings.get(0);
		Assertions.assertThat(seatBooking.getSeats().size()).isEqualTo(1);
	}

	@Test
	public void shouldMapSectionBookingDTOListToSectionBookingList()
	{
		List<BookingDTO> bookingsDTOs = getSectionBookingsDTOs();

		List<Booking> bookings = mapper.bookingDTOsToBookings(bookingsDTOs);

		Assertions.assertThat(bookings).isNotNull();
		Assertions.assertThat(bookings.size()).isEqualTo(bookings.size());
		Assertions.assertThat(bookings.get(0)).isInstanceOf(SectionBooking.class);

		SectionBooking seatBooking = (SectionBooking)bookings.get(0);
		Assertions.assertThat(seatBooking.getEntries().size()).isEqualTo(1);
	}
}
