package at.ac.tuwien.inso.sepm.ticketline.server.service;

import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventFilterDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.enums.Category;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.*;
import at.ac.tuwien.inso.sepm.ticketline.server.exception.NotFoundException;
import at.ac.tuwien.inso.sepm.ticketline.server.exception.ValidationException;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.EventRepository;
import at.ac.tuwien.inso.sepm.ticketline.server.service.implementation.SimpleEventService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SimpleEventServiceTest
{
	@MockBean
	private EventRepository repository;

	@Autowired
	private SimpleEventService service;

	private static final long EVENT_ID = 1L;
	private static final String EVENT_NAME = "Test Event";
	private static final String EVENT_DESCRIPTION = "Test Description";
	private static final String EVENT_ARTIST = "Madonna Donna";
	private static final String EVENT_ARTIST_FIRSTNAME = "Madonna";
	private static final String EVENT_ARTIST_LASTNAME = "Donna";
	private static final LocalDateTime EVENT_TIME = LocalDateTime.of(2017, 1, 1, 1, 1, 0, 0);
	private static final String LOCATION_NAME = "Test Location";
	private static final String LOCATION_STREET = "Test Street";
	private static final String LOCATION_CITY = "Test City";
	private static final String LOCATION_ZIP = "1234";
	private static final String LOCATION_COUNTRY = "Test Country";
	private static final Category EVENT_CATEGORY = Category.CONCERT;
	private static final int EVENT_DURATION = 120;
	private static final Category OTHER_EVENT_CATEGORY = Category.MUSICAL;

	@Test
	public void retrieveEvent_success()
	{
		Event event = Event.builder()
		                   .id(EVENT_ID)
		                   .name(EVENT_NAME)
		                   .artist(EVENT_ARTIST)
		                   .category(EVENT_CATEGORY)
		                   .time(EVENT_TIME)
		                   .build();

		List<Event> list = new ArrayList<>();
		list.add(event);

		given(repository.findAll()).willReturn(list);
		List<Event> result = service.findAll();
		assertThat(result).isEqualTo(list);
	}

	@Test
	public void retrieveEmpty_success()
	{
		given(repository.findAll()).willReturn(new ArrayList<>());
		List<Event> result = service.findAll();
		assertThat(result).isEmpty();
	}

	@Test
	public void retrieveEventById_success() throws ValidationException
	{
		Event event = Event.builder()
		                   .id(EVENT_ID)
		                   .name(EVENT_NAME)
		                   .artist(EVENT_ARTIST)
		                   .category(EVENT_CATEGORY)
		                   .time(EVENT_TIME)
		                   .build();

		given(repository.findOne(EVENT_ID)).willReturn(event);
		Event result = service.findOne(EVENT_ID);
		assertThat(result).isEqualTo(event);
	}

	@Test(expected = NotFoundException.class)
	public void retrieveEventById_notFound() throws ValidationException
	{
		Event event = Event.builder()
		                   .id(EVENT_ID)
		                   .name(EVENT_NAME)
		                   .artist(EVENT_ARTIST)
		                   .category(EVENT_CATEGORY)
		                   .time(EVENT_TIME)
		                   .build();

		given(repository.findOne(EVENT_ID)).willReturn(event);
		service.findOne(33L);
	}

	@Test(expected = ValidationException.class)
	public void retrieveEventById_failure() throws ValidationException
	{
		service.findOne(null);
	}

	@Test
	public void retrieveFilteredEvent_success()
	{
		Location location = Location.builder()
		                            .name(LOCATION_NAME)
		                            .street(LOCATION_STREET)
		                            .city(LOCATION_CITY)
		                            .zip(LOCATION_ZIP)
		                            .country(LOCATION_COUNTRY)
		                            .build();

		Event event = Event.builder()
		                   .id(EVENT_ID)
		                   .name(EVENT_NAME)
		                   .artist(EVENT_ARTIST)
		                   .category(EVENT_CATEGORY)
		                   .time(EVENT_TIME)
		                   .location(location)
		                   .build();

		List<Event> list = new ArrayList<>();
		list.add(event);

		given(repository.findByFilter(EVENT_NAME,
		                              EVENT_DESCRIPTION,
		                              EVENT_CATEGORY,
		                              EVENT_DURATION,
		                              EVENT_ARTIST_FIRSTNAME,
		                              EVENT_ARTIST_LASTNAME,
		                              LOCATION_NAME,
		                              LOCATION_STREET,
		                              LOCATION_CITY,
		                              LOCATION_ZIP,
		                              LOCATION_COUNTRY,
		                              EVENT_TIME,
		                              null)).willReturn(list);

		EventFilterDTO filter = new EventFilterDTO();
		filter.setArtistFirstname(EVENT_ARTIST_FIRSTNAME);
		filter.setArtistLastname(EVENT_ARTIST_LASTNAME);
		filter.setLocationName(LOCATION_NAME);
		filter.setCity(LOCATION_CITY);
		filter.setStreet(LOCATION_STREET);
		filter.setZip(LOCATION_ZIP);
		filter.setCountry(LOCATION_COUNTRY);
		filter.setCategory(EVENT_CATEGORY);
		filter.setDuration(EVENT_DURATION);
		filter.setName(EVENT_NAME);
		filter.setDescription(EVENT_DESCRIPTION);
		filter.setTime(EVENT_TIME);

		List<Event> result = service.findByFilter(filter);
		assertThat(result).isEqualTo(list);
	}

	@Test
	public void retrieveFilteredEmpty_success()
	{
		Location location = Location.builder()
		                            .name(LOCATION_NAME)
		                            .street(LOCATION_STREET)
		                            .city(LOCATION_CITY)
		                            .zip(LOCATION_ZIP)
		                            .country(LOCATION_COUNTRY)
		                            .build();

		Event event = Event.builder()
		                   .id(EVENT_ID)
		                   .name(EVENT_NAME)
		                   .artist(EVENT_ARTIST)
		                   .category(EVENT_CATEGORY)
		                   .time(EVENT_TIME)
		                   .location(location)
		                   .build();

		given(repository.findByFilter(EVENT_NAME,
		                              EVENT_DESCRIPTION,
		                              EVENT_CATEGORY,
		                              EVENT_DURATION,
		                              EVENT_ARTIST_FIRSTNAME,
		                              EVENT_ARTIST_LASTNAME,
		                              LOCATION_NAME,
		                              LOCATION_STREET,
		                              LOCATION_CITY,
		                              LOCATION_ZIP,
		                              LOCATION_COUNTRY,
		                              EVENT_TIME,
		                              null)).willReturn(Collections.singletonList(event));

		EventFilterDTO filter = new EventFilterDTO();
		filter.setArtistFirstname("another");
		filter.setCountry("another");

		List<Event> result = service.findByFilter(filter);
		assertThat(result).isEmpty();
	}

	@Test
	public void retrieveFilteredByName_success()
	{
		Event event = Event.builder()
		                   .id(EVENT_ID)
		                   .name(EVENT_NAME)
		                   .artist(EVENT_ARTIST)
		                   .category(EVENT_CATEGORY)
		                   .time(EVENT_TIME)
		                   .build();

		List<Event> list = new ArrayList<>();
		list.add(event);

		given(repository.findByFilter(Mockito.eq(EVENT_NAME),
		                              Mockito.anyString(),
		                              Mockito.any(),
		                              Mockito.anyInt(),
		                              Mockito.anyString(),
		                              Mockito.anyString(),
		                              Mockito.anyString(),
		                              Mockito.anyString(),
		                              Mockito.anyString(),
		                              Mockito.anyString(),
		                              Mockito.anyString(),
		                              Mockito.anyObject(),
		                              Mockito.anyObject())).willReturn(list);

		EventFilterDTO filter = new EventFilterDTO();
		filter.setName(EVENT_NAME);

		List<Event> result = service.findByFilter(filter);
		assertThat(result).isEqualTo(list);
	}

	@Test
	public void retrieveEmptyFilteredByName_success()
	{
		Event event = Event.builder()
		                   .id(EVENT_ID)
		                   .name(EVENT_NAME)
		                   .artist(EVENT_ARTIST)
		                   .category(EVENT_CATEGORY)
		                   .time(EVENT_TIME)
		                   .build();

		List<Event> list = new ArrayList<>();
		list.add(event);

		given(repository.findByFilter(Mockito.eq(EVENT_NAME),
		                              Mockito.anyString(),
		                              Mockito.any(),
		                              Mockito.anyInt(),
		                              Mockito.anyString(),
		                              Mockito.anyString(),
		                              Mockito.anyString(),
		                              Mockito.anyString(),
		                              Mockito.anyString(),
		                              Mockito.anyString(),
		                              Mockito.anyString(),
		                              Mockito.anyObject(),
		                              Mockito.anyObject())).willReturn(list);

		EventFilterDTO filter = new EventFilterDTO();
		filter.setName("another");

		List<Event> result = service.findByFilter(filter);
		assertThat(result).isEmpty();
	}

	@Test
	public void retrieveFilteredByCategory_success()
	{
		Event event = Event.builder()
		                   .id(EVENT_ID)
		                   .name(EVENT_NAME)
		                   .artist(EVENT_ARTIST)
		                   .category(EVENT_CATEGORY)
		                   .time(EVENT_TIME)
		                   .build();

		List<Event> list = new ArrayList<>();
		list.add(event);

		given(repository.findByFilter(Mockito.anyString(),
		                              Mockito.anyString(),
		                              Mockito.eq(EVENT_CATEGORY),
		                              Mockito.anyInt(),
		                              Mockito.anyString(),
		                              Mockito.anyString(),
		                              Mockito.anyString(),
		                              Mockito.anyString(),
		                              Mockito.anyString(),
		                              Mockito.anyString(),
		                              Mockito.anyString(),
		                              Mockito.anyObject(),
		                              Mockito.anyObject())).willReturn(list);

		EventFilterDTO filter = new EventFilterDTO();
		filter.setCategory(EVENT_CATEGORY);

		List<Event> result = service.findByFilter(filter);
		assertThat(result).isEqualTo(list);
	}

	@Test
	public void retrieveEmptyFilteredByCategory_success()
	{
		Event event = Event.builder()
		                   .id(EVENT_ID)
		                   .name(EVENT_NAME)
		                   .artist(EVENT_ARTIST)
		                   .category(EVENT_CATEGORY)
		                   .time(EVENT_TIME)
		                   .build();

		given(repository.findByFilter(Mockito.anyString(),
		                              Mockito.anyString(),
		                              Mockito.eq(EVENT_CATEGORY),
		                              Mockito.anyInt(),
		                              Mockito.anyString(),
		                              Mockito.anyString(),
		                              Mockito.anyString(),
		                              Mockito.anyString(),
		                              Mockito.anyString(),
		                              Mockito.anyString(),
		                              Mockito.anyString(),
		                              Mockito.anyObject(),
		                              Mockito.anyObject())).willReturn(Collections.singletonList(event));

		EventFilterDTO filter = new EventFilterDTO();
		filter.setCategory(OTHER_EVENT_CATEGORY);

		List<Event> result = service.findByFilter(filter);
		assertThat(result).isEmpty();
	}

	@Test
	public void retrieveFilteredByDuration_success()
	{
		Event event = Event.builder()
		                   .id(EVENT_ID)
		                   .name(EVENT_NAME)
		                   .artist(EVENT_ARTIST)
		                   .category(EVENT_CATEGORY)
		                   .time(EVENT_TIME)
		                   .duration(EVENT_DURATION)
		                   .build();

		List<Event> list = new ArrayList<>();
		list.add(event);

		given(repository.findByFilter(Mockito.anyString(),
		                              Mockito.anyString(),
		                              Mockito.any(),
		                              Mockito.eq(EVENT_DURATION),
		                              Mockito.anyString(),
		                              Mockito.anyString(),
		                              Mockito.anyString(),
		                              Mockito.anyString(),
		                              Mockito.anyString(),
		                              Mockito.anyString(),
		                              Mockito.anyString(),
		                              Mockito.anyObject(),
		                              Mockito.anyObject())).willReturn(list);

		EventFilterDTO filter = new EventFilterDTO();
		filter.setDuration(EVENT_DURATION);

		List<Event> result = service.findByFilter(filter);
		assertThat(result).isEqualTo(list);
	}

	@Test
	public void retrieveEmptyFilteredByDuration_success()
	{
		Event event = Event.builder()
		                   .id(EVENT_ID)
		                   .name(EVENT_NAME)
		                   .artist(EVENT_ARTIST)
		                   .category(EVENT_CATEGORY)
		                   .time(EVENT_TIME)
		                   .duration(EVENT_DURATION)
		                   .build();

		given(repository.findByFilter(Mockito.anyString(),
		                              Mockito.anyString(),
		                              Mockito.any(),
		                              Mockito.eq(EVENT_DURATION),
		                              Mockito.anyString(),
		                              Mockito.anyString(),
		                              Mockito.anyString(),
		                              Mockito.anyString(),
		                              Mockito.anyString(),
		                              Mockito.anyString(),
		                              Mockito.anyString(),
		                              Mockito.anyObject(),
		                              Mockito.anyObject())).willReturn(Collections.singletonList(event));

		EventFilterDTO filter = new EventFilterDTO();
		filter.setDuration(0);

		List<Event> result = service.findByFilter(filter);
		assertThat(result).isEmpty();
	}

	@Test
	public void retrievesTopEventsCorrectly() throws ValidationException
	{
		SectionBooking sectionBooking = new SectionBooking();
		SectionBookingEntry entry = new SectionBookingEntry();
		entry.setSection(Section.builder().build());
		entry.setAmount(3L);
		sectionBooking.setEntries(Set.of(entry));

		SeatBooking seatBooking = new SeatBooking();
		seatBooking.setSeats(Set.of(Seat.builder().id(1L).build(), Seat.builder().id(2L).build()));

		List<Event> events = new ArrayList<>();
		events.add(Event.builder().id(1L).bookings(Set.of(sectionBooking)).build());
		events.add(Event.builder().id(2L).bookings(Set.of(seatBooking)).build());

		given(repository.findByFilter("", "", null, null, "", "", "", "", "", "", "", null, null)).willReturn(events);

		Map<Event, Integer> bookingsByEvents = service.findTopEvents(null, null, null, 1);
		assertThat(bookingsByEvents.keySet().size()).isEqualTo(1);
		assertThat(bookingsByEvents.keySet().iterator().next().getId()).isEqualTo(1);
		assertThat(bookingsByEvents.values().iterator().next()).isEqualTo(3);
	}

	@Test
	public void returnPageOfEventDTOs_success()
	{
		Event event = Event.builder()
		                   .id(EVENT_ID)
		                   .name(EVENT_NAME)
		                   .artist(EVENT_ARTIST)
		                   .category(EVENT_CATEGORY)
		                   .time(EVENT_TIME)
		                   .duration(EVENT_DURATION)
		                   .build();

		Event event2 = Event.builder()
		                    .id(EVENT_ID + 1)
		                    .name(EVENT_NAME)
		                    .artist(EVENT_ARTIST)
		                    .category(EVENT_CATEGORY)
		                    .time(EVENT_TIME)
		                    .duration(EVENT_DURATION)
		                    .build();

		Event event3 = Event.builder()
		                    .id(EVENT_ID +1)
		                    .name(EVENT_NAME)
		                    .artist(EVENT_ARTIST)
		                    .category(EVENT_CATEGORY)
		                    .time(EVENT_TIME)
		                    .duration(EVENT_DURATION)
		                    .build();

		List<Event> eventsList = new LinkedList<>();
		eventsList.add(event);
		eventsList.add(event2);
		eventsList.add(event3);

		PageRequest pageRequest = new PageRequest(1, 1);
		Page<Event> eventsPage = new PageImpl<>(Collections.singletonList(event), pageRequest, eventsList.size());

		given(repository.findAllByOrderByIdDesc(pageRequest)).willReturn(eventsPage);

		Page<EventDTO> eventDTOPage = service.findPageFromAll(pageRequest);

		assertThat(eventDTOPage.getTotalPages()).isEqualTo(eventsPage.getTotalPages());
		assertThat(eventDTOPage.getContent().size()).isEqualTo(eventsPage.getContent().size());
		assertThat(eventDTOPage.getContent().get(0).getId()).isEqualTo(event.getId());
	}

	@Test
	public void retrieveFilteredByDurationPage_success()
	{
		Event event = Event.builder()
		                   .id(EVENT_ID)
		                   .name(EVENT_NAME)
		                   .artist(EVENT_ARTIST)
		                   .category(EVENT_CATEGORY)
		                   .time(EVENT_TIME)
		                   .duration(EVENT_DURATION)
		                   .build();

		List<Event> eventsList = new LinkedList<>();
		eventsList.add(event);

		PageRequest pageRequest = new PageRequest(1, 1);
		Page<Event> eventsPage = new PageImpl<>(eventsList, pageRequest, 2);

		given(repository.findByFilter(Mockito.anyString(),
		                              Mockito.anyString(),
		                              Mockito.any(),
		                              Mockito.eq(EVENT_DURATION),
		                              Mockito.anyString(),
		                              Mockito.anyString(),
		                              Mockito.anyString(),
		                              Mockito.anyString(),
		                              Mockito.anyString(),
		                              Mockito.anyString(),
		                              Mockito.anyString(),
		                              Mockito.anyObject(),
		                              Mockito.anyObject(),
		                              Mockito.eq(pageRequest))).willReturn(eventsPage);

		EventFilterDTO filter = new EventFilterDTO();
		filter.setDuration(EVENT_DURATION);

		Page<EventDTO> result = service.findPageByFilter(filter, pageRequest);

		assertThat(result.getTotalPages()).isEqualTo(eventsPage.getTotalPages());
		assertThat(result.getContent().get(0).getId()).isEqualTo(event.getId());
	}

	@Test(expected = NotFoundException.class)
	public void retrieveFilteredByDurationPage_notFound()
	{
		Event event = Event.builder()
		                   .id(EVENT_ID)
		                   .name(EVENT_NAME)
		                   .artist(EVENT_ARTIST)
		                   .category(EVENT_CATEGORY)
		                   .time(EVENT_TIME)
		                   .duration(EVENT_DURATION)
		                   .build();

		List<Event> eventsList = new LinkedList<>();
		eventsList.add(event);

		PageRequest pageRequest = new PageRequest(1, 1);
		Page<Event> eventsPage = new PageImpl<>(eventsList, pageRequest, 2);

		given(repository.findByFilter(Mockito.anyString(),
		                              Mockito.anyString(),
		                              Mockito.any(),
		                              Mockito.eq(EVENT_DURATION),
		                              Mockito.anyString(),
		                              Mockito.anyString(),
		                              Mockito.anyString(),
		                              Mockito.anyString(),
		                              Mockito.anyString(),
		                              Mockito.anyString(),
		                              Mockito.anyString(),
		                              Mockito.anyObject(),
		                              Mockito.anyObject(),
		                              Mockito.eq(pageRequest))).willReturn(eventsPage);

		EventFilterDTO filter = new EventFilterDTO();
		filter.setDuration(EVENT_DURATION);

		service.findPageByFilter(filter, new PageRequest(3, 1));
	}
}
