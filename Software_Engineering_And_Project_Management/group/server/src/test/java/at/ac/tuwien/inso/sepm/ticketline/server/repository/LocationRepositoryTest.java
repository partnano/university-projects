package at.ac.tuwien.inso.sepm.ticketline.server.repository;

import at.ac.tuwien.inso.sepm.ticketline.server.entity.Location;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Set;

@RunWith(SpringJUnit4ClassRunner.class)
@DataJpaTest
@Transactional
public class LocationRepositoryTest
{
	@Autowired
	private LocationRepository repository;

	private static final String LOCATION_NAME1 = "Test Location";
	private static final String LOCATION_NAME2 = "Another Test Location";
	private static final String LOCATION_STREET = "Test Street";
	private static final String LOCATION_CITY = "Test City";
	private static final String LOCATION_ZIP = "1234";
	private static final String LOCATION_COUNTRY = "Test Country";

	@Test
	public void retrieveEmptyLocations()
	{
		Assertions.assertThat(repository.findAll()).isEmpty();
	}

	@Test
	public void createLocation()
	{
		Location location = Location.builder()
		                            .name(LOCATION_NAME1)
		                            .street(LOCATION_STREET)
		                            .city(LOCATION_CITY)
		                            .zip(LOCATION_ZIP)
		                            .country(LOCATION_COUNTRY)
		                            .build();

		Assertions.assertThat(location.getId()).isNull();
		Location savedLocation = repository.save(location);
		Assertions.assertThat(savedLocation).isEqualToIgnoringGivenFields(location, "id");
		Assertions.assertThat(savedLocation.getId()).isEqualTo(1L);
	}

	@Test
	public void createLocations()
	{
		Location location1 = Location.builder()
		                             .name(LOCATION_NAME1)
		                             .street(LOCATION_STREET)
		                             .city(LOCATION_CITY)
		                             .zip(LOCATION_ZIP)
		                             .country(LOCATION_COUNTRY)
		                             .build();

		Location location2 = Location.builder()
		                             .name(LOCATION_NAME2)
		                             .street(LOCATION_STREET)
		                             .city(LOCATION_CITY)
		                             .zip(LOCATION_ZIP)
		                             .country(LOCATION_COUNTRY)
		                             .build();

		List<Location> savedLocations = repository.save(List.of(location1, location2));

		Assertions.assertThat(savedLocations.get(0)).isEqualToIgnoringGivenFields(location1, "id");
		Assertions.assertThat(savedLocations.get(0).getId()).isEqualTo(2L);
		Assertions.assertThat(savedLocations.get(1)).isEqualToIgnoringGivenFields(location2, "id");
		Assertions.assertThat(savedLocations.get(1).getId()).isEqualTo(3L);
	}
}
