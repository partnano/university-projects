package at.ac.tuwien.inso.sepm.ticketline.server.mapper;

import at.ac.tuwien.inso.sepm.ticketline.rest.location.RowDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.location.SeatDTO;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Row;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.Seat;
import at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper.location.SeatMapper;
import at.ac.tuwien.inso.sepm.ticketline.server.repository.SeatRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
public class SeatMapperTest
{
	@Configuration
	@ComponentScan(basePackages = "at.ac.tuwien.inso.sepm.ticketline.server.entity.mapper")
	public static class LocationSeatMapperTestContextConfiguration
	{
	}

	@MockBean
	SeatRepository seatRepository;

	@Autowired
	@SuppressWarnings("SpringJavaAutowiredMembersInspection")
	private SeatMapper mapper;

	private static final long SEAT_INDEX = 1L;
	private static final long SEAT_ID = 100L;
	private static final String SEAT_NAME = "Nightmare";

	private static final Row ROW = Row.builder().id(3L).orderIndex(2L).name("Elm").build();

	private static final RowDTO ROW_DTO = RowDTO.builder().id(3L).orderIndex(2L).name("Elm").build();

	@Test
	public void shouldMapLocationSeatToLocationSeatDTO()
	{
		Seat seat = Seat.builder().id(SEAT_ID).orderIndex(SEAT_INDEX).name(SEAT_NAME).row(ROW).build();

		SeatDTO seatDTO = mapper.seatToSeatDTO(seat);
		assertThat(seatDTO).isNotNull();
		assertThat(seatDTO.getId()).isEqualTo(SEAT_ID);
		assertThat(seatDTO.getOrderIndex()).isEqualTo(SEAT_INDEX);
		assertThat(seatDTO.getName()).isEqualTo(SEAT_NAME);
		assertThat(seatDTO.getRow()).isNull();
	}

	@Test
	public void shouldMapLocationSeatDTOToLocationSeat()
	{
		SeatDTO seatDTO = SeatDTO.builder()
		                         .id(SEAT_ID)
		                         .orderIndex(SEAT_INDEX)
		                         .name(SEAT_NAME)
		                         .row(ROW_DTO)
		                         .build();

		Seat seat = mapper.seatDTOtoSeat(seatDTO);
		assertThat(seat).isNotNull();
		assertThat(seat.getId()).isEqualTo(SEAT_ID);
		assertThat(seat.getOrderIndex()).isEqualTo(SEAT_INDEX);
		assertThat(seat.getName()).isEqualTo(SEAT_NAME);
		assertThat(seat.getRow()).isNull();
	}
}
