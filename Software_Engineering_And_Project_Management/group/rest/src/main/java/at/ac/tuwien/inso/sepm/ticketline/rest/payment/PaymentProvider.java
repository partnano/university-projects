package at.ac.tuwien.inso.sepm.ticketline.rest.payment;

public enum PaymentProvider
{
	STRIPE("payment.stripe"),
	CASH("payment.cash");

	private final String name;

	PaymentProvider(String name)
	{
		this.name = name;
	}

	public String getName()
	{
		return name;
	}

	@Override
	public String toString()
	{
		return getName();
	}
}

