package at.ac.tuwien.inso.sepm.ticketline.rest.location;

import java.util.Objects;

public class SeatDTO
{
	private Long id;

	private RowDTO row;

	private Long orderIndex;

	private String name;

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public RowDTO getRow()
	{
		return row;
	}

	public void setRow(RowDTO row)
	{
		this.row = row;
	}

	public Long getOrderIndex()
	{
		return orderIndex;
	}

	public void setOrderIndex(Long orderIndex)
	{
		this.orderIndex = orderIndex;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public static SeatDTOBuilder builder()
	{
		return new SeatDTOBuilder();
	}

	public static final class SeatDTOBuilder
	{
		private Long id;
		private RowDTO row;
		private Long orderIndex;
		private String name;

		public SeatDTOBuilder id(Long id)
		{
			this.id = id;
			return this;
		}

		public SeatDTOBuilder row(RowDTO row)
		{
			this.row = row;
			return this;
		}

		public SeatDTOBuilder orderIndex(Long orderIndex)
		{
			this.orderIndex = orderIndex;
			return this;
		}

		public SeatDTOBuilder name(String name)
		{
			this.name = name;
			return this;
		}

		public SeatDTO build()
		{
			SeatDTO seat = new SeatDTO();
			seat.setId(id);
			seat.setRow(row);
			seat.setOrderIndex(orderIndex);
			seat.setName(name);
			return seat;
		}
	}

	@Override
	public boolean equals(Object o)
	{
		if(this == o)
			return true;

		if(o == null || getClass() != o.getClass())
			return false;

		SeatDTO seatDTO = (SeatDTO)o;
		return Objects.equals(id, seatDTO.id);
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(id);
	}

	@Override
	public String toString()
	{
		return "SeatDTO{" + "id=" + id + ", row=" + row + ", orderIndex=" + orderIndex + ", name='" + name
		       + '\'' + '}';
	}
}
