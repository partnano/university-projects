package at.ac.tuwien.inso.sepm.ticketline.rest.booking;

import at.ac.tuwien.inso.sepm.ticketline.rest.customer.CustomerDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventAreaDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.location.AreaDTO;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import io.swagger.annotations.ApiModelProperty;

import java.time.LocalDateTime;
import java.util.Objects;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes({@JsonSubTypes.Type(value = SectionBookingDTO.class, name = "section_booking"),
               @JsonSubTypes.Type(value = SeatBookingDTO.class, name = "seat_booking")})
public abstract class BookingDTO
{
	public enum BookingStatus
	{
		BOOKED,
		RESERVED,
		CANCELED
	}

	@ApiModelProperty(required = true, name = "The id of the booking")
	private Long id;

	@ApiModelProperty(required = true, name = "The reservation number of the booking")
	private int reservationNumber;

	@ApiModelProperty(required = true, name = "Whether the booking is already paid (or a reservation)")
	private boolean isPaid;

	@ApiModelProperty(required = true, name = "Whether the booking is canceled or not")
	private boolean isCanceled;

	@ApiModelProperty(required = true, name = "Timestamp of booking creation")
	private LocalDateTime createdAt;

	@ApiModelProperty(required = true, name = "Timestamp of when booking was paid")
	private LocalDateTime bookedAt;

	@ApiModelProperty(name = "The name of the payment method")
	private String paymentMethod;

	@ApiModelProperty(name = "Event that is booked")
	private EventDTO event;

	@ApiModelProperty(name = "Customer that booked")
	private CustomerDTO customer;

	@ApiModelProperty(name = "Refences the payment receipt ID, if paid")
	private ReceiptDTO paymentReceipt;

	@ApiModelProperty(name = "Refences the cancellation receipt ID, if cancelled after paid")
	private ReceiptDTO cancellationReceipt;

	@ApiModelProperty(name = "Total price of the booking")
	private Float totalPrice;

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public int getReservationNumber()
	{
		return reservationNumber;
	}

	public void setReservationNumber(int reservationNumber)
	{
		this.reservationNumber = reservationNumber;
	}

	public boolean isPaid()
	{
		return isPaid;
	}

	public void setPaid(boolean paid)
	{
		isPaid = paid;
	}

	public boolean isCanceled()
	{
		return isCanceled;
	}

	public void setCanceled(boolean canceled)
	{
		isCanceled = canceled;
	}

	public LocalDateTime getCreatedAt()
	{
		return createdAt;
	}

	public void setCreatedAt(LocalDateTime createdAt)
	{
		this.createdAt = createdAt;
	}

	public LocalDateTime getBookedAt()
	{
		return bookedAt;
	}

	public void setBookedAt(LocalDateTime bookedAt)
	{
		this.bookedAt = bookedAt;
	}

	public String getPaymentMethod()
	{
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod)
	{
		this.paymentMethod = paymentMethod;
	}

	public EventDTO getEvent()
	{
		return event;
	}

	public void setEvent(EventDTO event)
	{
		this.event = event;
	}

	public CustomerDTO getCustomer()
	{
		return customer;
	}

	public void setCustomer(CustomerDTO customer)
	{
		this.customer = customer;
	}

	public ReceiptDTO getPaymentReceipt()
	{
		return paymentReceipt;
	}

	public void setPaymentReceipt(ReceiptDTO paymentReceipt)
	{
		this.paymentReceipt = paymentReceipt;
	}

	public ReceiptDTO getCancellationReceipt()
	{
		return cancellationReceipt;
	}

	public void setCancellationReceipt(ReceiptDTO cancellationReceipt)
	{
		this.cancellationReceipt = cancellationReceipt;
	}

	public Float getTotalPrice()
	{
		return totalPrice;
	}

	public void setTotalPrice(Float totalPrice)
	{
		this.totalPrice = totalPrice;
	}

	/**
	 * Returns the BookingStatus of this booking
	 *
	 * @return The BookingStatus of this booking
	 */
	public BookingStatus getBookingStatus()
	{
		if(isCanceled())
		{
			return BookingStatus.CANCELED;
		}
		else if(isPaid())
		{
			return BookingStatus.BOOKED;
		}
		else
		{
			return BookingStatus.RESERVED;
		}
	}

	public Float priceForArea(AreaDTO area)
	{
		return priceForArea(area, event);
	}

	public static Float priceForArea(AreaDTO area, EventDTO event)
	{
		for(EventAreaDTO eventArea : event.getEventAreas())
		{
			if(eventArea.getArea().getId().equals(area.getId()))
			{
				return eventArea.getPrice();
			}
		}

		throw new RuntimeException(
			"no price defined for area with id " + area.getId() + " in event with id " + event.getId()
			+ "\navailable event areas: " + event.getEventAreas());
	}

	public abstract Float totalPriceThroughEvent();

	@Override
	public boolean equals(Object o)
	{
		if(this == o)
		{
			return true;
		}

		if(o == null || getClass() != o.getClass())
		{
			return false;
		}

		BookingDTO that = (BookingDTO)o;
		return Objects.equals(id, that.id) && reservationNumber == that.reservationNumber
		       && isPaid == that.isPaid && isCanceled == that.isCanceled && Objects.equals(createdAt,
		                                                                                   that.createdAt)
		       && Objects.equals(bookedAt, that.bookedAt) && Objects.equals(event, that.event)
		       && Objects.equals(customer, that.customer);
	}

	@Override
	public String toString()
	{
		String eventId = "not set";
		if(event != null)
		{
			eventId = "id=" + event.getId();
		}

		String customerId = "not set";
		if(customer != null)
		{
			customerId = "id=" + customer.getId();
		}

		return "Booking{" + "id=" + id + ", event=" + eventId + ", customer=" + customerId
		       + ", reservationNumber=" + reservationNumber + ", isPaid=" + isPaid + ", isCanceled="
		       + isCanceled + ", createdAt=" + createdAt + ", bookedAt=" + bookedAt + '}';
	}
}
