package at.ac.tuwien.inso.sepm.ticketline.rest.page;

import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.List;

public class RestResponsePage<T> extends PageImpl<T>
{
	private static final long serialVersionUID = 3248189030448292002L;
	private long totalElements;
	private int totalPages;
	private int size;

	public RestResponsePage(List<T> content, Pageable pageable, long total)
	{
		super(content, pageable, total);
		// TODO Auto-generated constructor stub
	}

	public RestResponsePage(List<T> content)
	{
		super(content);
		// TODO Auto-generated constructor stub
	}

	/* PageImpl does not have an empty constructor and this was causing an issue for RestTemplate to cast the Rest API response
	 * back to Page.
	 */
	public RestResponsePage()
	{
		super(new ArrayList<>());
	}

	@Override
	public long getTotalElements()
	{
		return totalElements;
	}

	public void setTotalElements(long totalElements)
	{
		this.totalElements = totalElements;
	}

	@Override
	public int getTotalPages()
	{
		return totalPages;
	}

	public void setTotalPages(int totalPages)
	{
		this.totalPages = totalPages;
	}

	@Override
	public int getSize()
	{
		return size;
	}

	public void setSize(int size)
	{
		this.size = size;
	}
}
