package at.ac.tuwien.inso.sepm.ticketline.rest.booking;

import io.swagger.annotations.ApiModelProperty;

import java.util.Objects;
import java.util.Set;

public class SectionBookingDTO extends BookingDTO
{

	@ApiModelProperty(name = "Sections of the section booking")
	private Set<SectionBookingEntryDTO> sections;

	public Set<SectionBookingEntryDTO> getSections()
	{
		return sections;
	}

	public void setSections(Set<SectionBookingEntryDTO> sections)
	{
		this.sections = sections;
	}

	@Override
	public String toString()
	{
		return "SectionBookingDTO{" + "sections=" + sections + "} " + super.toString();
	}

	@Override
	public Float totalPriceThroughEvent()
	{
		float sum = 0.0f;

		for(SectionBookingEntryDTO sectionEntry : sections)
		{
			Float sectionPrice = priceForArea(sectionEntry.getSection());
			sum += sectionPrice * sectionEntry.getAmount();
		}

		return sum;
	}

	@Override
	public boolean equals(Object o)
	{
		if(this == o)
			return true;

		if(o == null || getClass() != o.getClass())
			return false;

		if(!super.equals(o))
			return false;

		SectionBookingDTO that = (SectionBookingDTO)o;
		return Objects.equals(sections, that.sections);
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(sections);
	}
}
