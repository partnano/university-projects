package at.ac.tuwien.inso.sepm.ticketline.rest.event;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "Top10EventDTO", description = "an event and the corresponding number of bookings")
public class Top10EventDTO
{
	@ApiModelProperty("the event")
	private EventDTO event;

	@ApiModelProperty("the number of bookings")
	private int bookings;

	public EventDTO getEvent()
	{
		return event;
	}

	public void setEvent(EventDTO event)
	{
		this.event = event;
	}

	public int getBookings()
	{
		return bookings;
	}

	public void setBookings(int bookings)
	{
		this.bookings = bookings;
	}

	@Override
	public String toString()
	{
		return "Top10EventDTO{" + "event=" + event + ", bookings=" + bookings + '}';
	}
}
