package at.ac.tuwien.inso.sepm.ticketline.rest.booking;

import java.time.LocalDateTime;

public class ReceiptDTO
{
	private Long id;

	private LocalDateTime createdAt;

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public LocalDateTime getCreatedAt()
	{
		return createdAt;
	}

	public void setCreatedAt(LocalDateTime createdAt)
	{
		this.createdAt = createdAt;
	}

	@Override
	public boolean equals(Object o)
	{
		if(this == o)
			return true;
		if(o == null || getClass() != o.getClass())
			return false;

		ReceiptDTO that = (ReceiptDTO)o;

		if(!id.equals(that.id))
			return false;
		return createdAt != null ? createdAt.equals(that.createdAt) : that.createdAt == null;
	}

	@Override
	public int hashCode()
	{
		int result = id.hashCode();
		result = 31 * result + (createdAt != null ? createdAt.hashCode() : 0);
		return result;
	}

	@Override
	public String toString()
	{
		return "ReceiptDTO{" + "id=" + id + ", createdAt=" + createdAt + '}';
	}
}
