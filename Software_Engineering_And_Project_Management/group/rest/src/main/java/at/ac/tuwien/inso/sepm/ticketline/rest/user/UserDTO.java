package at.ac.tuwien.inso.sepm.ticketline.rest.user;

public class UserDTO
{

	private Long id;

	private String email;

	private String firstName;

	private String lastName;

	private boolean isAdmin;

	private boolean isLocked;

	public String getEmail()
	{
		return email;
	}

	public void setEmail(String email)
	{
		this.email = email;
	}

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public String getFirstName()
	{
		return firstName;
	}

	public void setFirstName(String firstName)
	{
		this.firstName = firstName;
	}

	public String getLastName()
	{
		return lastName;
	}

	public void setLastName(String lastName)
	{
		this.lastName = lastName;
	}

	public boolean isAdmin()
	{
		return isAdmin;
	}

	public void setAdmin(boolean admin)
	{
		isAdmin = admin;
	}

	public boolean isLocked()
	{
		return isLocked;
	}

	public void setLocked(boolean locked)
	{
		isLocked = locked;
	}

	@Override
	public String toString()
	{
		return "UserDTO{" + "id=" + id + ", email='" + email + '\'' + ", firstName='" + firstName + '\''
		       + ", lastName='" + lastName + '\'' + ", isAdmin=" + isAdmin + ", isLocked=" + isLocked + '}';
	}

	@Override
	public boolean equals(Object o)
	{
		if(this == o)
			return true;
		if(o == null || getClass() != o.getClass())
			return false;

		UserDTO userDTO = (UserDTO)o;

		return id.equals(userDTO.id);
	}

	@Override
	public int hashCode()
	{
		return id.hashCode();
	}
}

