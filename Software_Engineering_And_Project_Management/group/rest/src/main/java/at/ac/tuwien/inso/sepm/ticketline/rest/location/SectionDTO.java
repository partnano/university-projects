package at.ac.tuwien.inso.sepm.ticketline.rest.location;

import java.util.Objects;

public class SectionDTO extends AreaDTO
{
	private int capacity;

	public int getCapacity()
	{
		return capacity;
	}

	public void setCapacity(int capacity)
	{
		this.capacity = capacity;
	}

	public static SectionDTOBuilder builder()
	{
		return new SectionDTOBuilder();
	}

	@Override
	public boolean equals(Object o)
	{
		if(this == o)
			return true;

		if(o == null || getClass() != o.getClass())
			return false;

		SectionDTO that = (SectionDTO)o;
		return Objects.equals(getId(), that.getId()) && Objects.equals(getOrderIndex(), that.getOrderIndex())
		       && Objects.equals(getName(), that.getName()) && Objects.equals(capacity, that.capacity);
	}

	@Override
	public String toString()
	{
		return "SectionDTO{" + "capacity=" + capacity + "} " + super.toString();
	}

	public static final class SectionDTOBuilder extends AreaDTOBuilder
	{
		private int capacity;

		private SectionDTOBuilder()
		{
		}

		public SectionDTOBuilder capacity(int capacity)
		{
			this.capacity = capacity;
			return this;
		}

		@Override
		public SectionDTOBuilder id(Long id)
		{
			return (SectionDTOBuilder)super.id(id);
		}

		@Override
		public SectionDTOBuilder location(LocationDTO location)
		{
			return (SectionDTOBuilder)super.location(location);
		}

		@Override
		public SectionDTOBuilder orderIndex(Long orderIndex)
		{
			return (SectionDTOBuilder)super.orderIndex(orderIndex);
		}

		@Override
		public SectionDTOBuilder name(String name)
		{
			return (SectionDTOBuilder)super.name(name);
		}

		public SectionDTO build()
		{
			SectionDTO section = new SectionDTO();
			section = (SectionDTO)super.build(section);
			section.setCapacity(capacity);
			return section;
		}
	}
}
