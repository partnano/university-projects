package at.ac.tuwien.inso.sepm.ticketline.rest.event;

import at.ac.tuwien.inso.sepm.ticketline.rest.location.SectionDTO;

public class EventSectionPriceDTO
{
	private Long id;
	private EventDTO event;
	private SectionDTO locationSection;
	private Float price;

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public EventDTO getEvent()
	{
		return event;
	}

	public void setEvent(EventDTO event)
	{
		this.event = event;
	}

	public SectionDTO getLocationSection()
	{
		return locationSection;
	}

	public void setLocationSection(SectionDTO locationSection)
	{
		this.locationSection = locationSection;
	}

	public Float getPrice()
	{
		return price;
	}

	public void setPrice(Float price)
	{
		this.price = price;
	}

	public static EventSectionPriceDTOBuilder builder()
	{
		return new EventSectionPriceDTOBuilder();
	}

	@Override
	public boolean equals(Object o)
	{
		if(this == o)
			return true;

		if(o == null || getClass() != o.getClass())
			return false;

		EventSectionPriceDTO that = (EventSectionPriceDTO)o;

		if(!id.equals(that.id))
			return false;

		if(!event.equals(that.event))
			return false;

		if(!locationSection.equals(that.locationSection))
			return false;

		return price.equals(that.price);
	}

	@Override
	public String toString()
	{
		return "EventSectionPriceDTO{" +
			"id=" + id +
			", event=" + event +
			", locationSection=" + locationSection +
			", price=" + price +
			'}';
	}

	public static final class EventSectionPriceDTOBuilder
	{
		private Long id;
		private EventDTO event;
		private SectionDTO locationSection;
		private Float price;

		private EventSectionPriceDTOBuilder()
		{
		}

		public EventSectionPriceDTOBuilder id(Long id)
		{
			this.id = id;
			return this;
		}

		public EventSectionPriceDTOBuilder event(EventDTO event)
		{
			this.event = event;
			return this;
		}

		public EventSectionPriceDTOBuilder locationSection(SectionDTO locationSection)
		{
			this.locationSection = locationSection;
			return this;
		}

		public EventSectionPriceDTOBuilder price(Float price)
		{
			this.price = price;
			return this;
		}

		public EventSectionPriceDTO build()
		{
			EventSectionPriceDTO eventSectionPrice = new EventSectionPriceDTO();
			eventSectionPrice.setId(id);
			eventSectionPrice.setEvent(event);
			eventSectionPrice.setLocationSection(locationSection);
			eventSectionPrice.setPrice(price);
			return eventSectionPrice;
		}
	}
}
