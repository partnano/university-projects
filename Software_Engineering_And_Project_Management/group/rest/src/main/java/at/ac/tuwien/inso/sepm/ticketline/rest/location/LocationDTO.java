package at.ac.tuwien.inso.sepm.ticketline.rest.location;

import java.util.Objects;
import java.util.Set;

public class LocationDTO
{
	private Long id;

	private String name;
	private String street;
	private String city;
	private String zip;
	private String country;

	private Set<AreaDTO> areas;

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getStreet()
	{
		return street;
	}

	public void setStreet(String street)
	{
		this.street = street;
	}

	public String getZip()
	{
		return zip;
	}

	public void setZip(String zip)
	{
		this.zip = zip;
	}

	public Set<AreaDTO> getAreas()
	{
		return areas;
	}

	public void setAreas(Set<AreaDTO> areas)
	{
		this.areas = areas;
	}

	public String getCity()
	{
		return city;
	}

	public void setCity(String city)
	{
		this.city = city;
	}

	public String getCountry()
	{
		return country;
	}

	public void setCountry(String country)
	{
		this.country = country;
	}

	@Override
	public String toString()
	{
		return "LocationDTO{" + "id=" + id + ", name='" + name + '\'' + ", street='" + street + '\''
		       + ", city='" + city + '\'' + ", zip='" + zip + '\'' + ", country='" + country + '\'' + ", areas="
		       + areas + '}';
	}

	@Override
	public boolean equals(Object o)
	{
		if(this == o)
			return true;

		if(o == null || getClass() != o.getClass())
			return false;

		LocationDTO that = (LocationDTO)o;
		return Objects.equals(id, that.id);
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(id);
	}

	public static LocationBuilder builder()
	{
		return new LocationBuilder();
	}

	public static final class LocationBuilder
	{
		private Long id;
		private String name;
		private String street;
		private String city;
		private String zip;
		private String country;
		private Set<AreaDTO> areas;

		private LocationBuilder()
		{
		}

		public LocationBuilder id(Long id)
		{
			this.id = id;
			return this;
		}

		public LocationBuilder name(String name)
		{
			this.name = name;
			return this;
		}

		public LocationBuilder street(String street)
		{
			this.street = street;
			return this;
		}

		public LocationBuilder city(String city)
		{
			this.city = city;
			return this;
		}

		public LocationBuilder zip(String zip)
		{
			this.zip = zip;
			return this;
		}

		public LocationBuilder country(String country)
		{
			this.country = country;
			return this;
		}

		public LocationBuilder areas(Set<AreaDTO> areas)
		{
			this.areas = areas;
			return this;
		}

		public LocationDTO build()
		{
			LocationDTO location = new LocationDTO();
			location.setId(id);
			location.setName(name);
			location.setStreet(street);
			location.setCity(city);
			location.setZip(zip);
			location.setCountry(country);
			location.setAreas(areas);
			return location;
		}
	}
}
