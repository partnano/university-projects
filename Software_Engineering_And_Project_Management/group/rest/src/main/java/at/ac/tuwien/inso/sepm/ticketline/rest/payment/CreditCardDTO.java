package at.ac.tuwien.inso.sepm.ticketline.rest.payment;

public class CreditCardDTO
{
	private Integer expirationMonth;
	private Integer expirationYear;
	private String cardNumber;
	private Integer cvc;

	public Integer getExpirationMonth()
	{
		return expirationMonth;
	}

	public void setExpirationMonth(Integer expirationMonth)
	{
		this.expirationMonth = expirationMonth;
	}

	public Integer getExpirationYear()
	{
		return expirationYear;
	}

	public void setExpirationYear(Integer expirationYear)
	{
		this.expirationYear = expirationYear;
	}

	public String getCardNumber()
	{
		return cardNumber;
	}

	public void setCardNumber(String cardNumber)
	{
		this.cardNumber = cardNumber;
	}

	public Integer getCvc()
	{
		return cvc;
	}

	public void setCvc(Integer cvc)
	{
		this.cvc = cvc;
	}
}
