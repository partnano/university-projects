package at.ac.tuwien.inso.sepm.ticketline.rest.location;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import java.util.Objects;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes({@JsonSubTypes.Type(value = SectionDTO.class, name = "section"),
               @JsonSubTypes.Type(value = RowDTO.class, name = "row")})
public class AreaDTO
{
	private Long id;

	private LocationDTO location;

	private Long orderIndex;

	private String name;

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public LocationDTO getLocation()
	{
		return location;
	}

	public void setLocation(LocationDTO location)
	{
		this.location = location;
	}

	public Long getOrderIndex()
	{
		return orderIndex;
	}

	public void setOrderIndex(Long orderIndex)
	{
		this.orderIndex = orderIndex;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	@Override
	public boolean equals(Object o)
	{
		if(this == o)
			return true;

		if(o == null || getClass() != o.getClass())
			return false;

		AreaDTO areaDTO = (AreaDTO)o;
		return Objects.equals(id, areaDTO.id) && Objects.equals(location, areaDTO.location);
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(id, location);
	}

	@Override
	public String toString()
	{
		return "\nAreaDTO{" + "id=" + id +
		       //", location=" + location.getId() +
		       ", orderIndex=" + orderIndex + ", name='" + name + '\'' + '}';
	}

	public static AreaDTOBuilder builder()
	{
		return new AreaDTOBuilder();
	}

	public static class AreaDTOBuilder
	{
		protected Long id;
		protected LocationDTO location;
		protected Long orderIndex;
		protected String name;

		public AreaDTOBuilder id(Long id)
		{
			this.id = id;
			return this;
		}

		public AreaDTOBuilder location(LocationDTO location)
		{
			this.location = location;
			return this;
		}

		public AreaDTOBuilder orderIndex(Long orderIndex)
		{
			this.orderIndex = orderIndex;
			return this;
		}

		public AreaDTOBuilder name(String name)
		{
			this.name = name;
			return this;
		}

		public AreaDTO build()
		{
			AreaDTO area = new AreaDTO();
			return build(area);
		}

		public AreaDTO build(AreaDTO area)
		{
			return applyFields(area);
		}

		private AreaDTO applyFields(AreaDTO area)
		{
			area.setId(id);
			area.setLocation(location);
			area.setOrderIndex(orderIndex);
			area.setName(name);
			return area;
		}
	}
}
