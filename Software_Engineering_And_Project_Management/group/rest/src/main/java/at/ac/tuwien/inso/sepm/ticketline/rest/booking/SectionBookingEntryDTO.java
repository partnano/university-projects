package at.ac.tuwien.inso.sepm.ticketline.rest.booking;

import at.ac.tuwien.inso.sepm.ticketline.rest.location.SectionDTO;

import java.util.Objects;

public class SectionBookingEntryDTO
{
	private Long id;
	private SectionDTO section;
	private Long amount;

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public SectionDTO getSection()
	{
		return section;
	}

	public void setSection(SectionDTO section)
	{
		this.section = section;
	}

	public Long getAmount()
	{
		return amount;
	}

	public void setAmount(Long amount)
	{
		this.amount = amount;
	}

	@Override
	public String toString()
	{
		return "SectionBookingEntryDTO{" + "id=" + id + ", section=" + section + ", amount=" + amount + '}';
	}

	@Override
	public boolean equals(Object o)
	{
		if(this == o)
			return true;

		if(o == null || getClass() != o.getClass())
			return false;

		SectionBookingEntryDTO that = (SectionBookingEntryDTO)o;
		return Objects.equals(id, that.id);
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(section, amount);
	}
}
