package at.ac.tuwien.inso.sepm.ticketline.rest.event;

import at.ac.tuwien.inso.sepm.ticketline.rest.location.RowDTO;

public class EventRowPriceDTO
{
	private Long id;
	private EventDTO event;
	private RowDTO locationRow;
	private Float price;

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public EventDTO getEvent()
	{
		return event;
	}

	public void setEvent(EventDTO event)
	{
		this.event = event;
	}

	public RowDTO getLocationRow()
	{
		return locationRow;
	}

	public void setLocationRow(RowDTO locationRow)
	{
		this.locationRow = locationRow;
	}

	public Float getPrice()
	{
		return price;
	}

	public void setPrice(Float price)
	{
		this.price = price;
	}

	public static EventRowPriceDTOBuilder builder()
	{
		return new EventRowPriceDTOBuilder();
	}

	@Override
	public boolean equals(Object o)
	{
		if(this == o)
			return true;

		if(o == null || getClass() != o.getClass())
			return false;

		EventRowPriceDTO that = (EventRowPriceDTO)o;

		if(!id.equals(that.id))
			return false;

		if(!event.equals(that.event))
			return false;

		if(!locationRow.equals(that.locationRow))
			return false;

		return price.equals(that.price);
	}

	@Override
	public String toString()
	{
		return "EventRowPriceDTO{" +
			"id=" + id +
			", event=" + event +
			", locationRow=" + locationRow +
			", price=" + price +
			'}';
	}

	public static final class EventRowPriceDTOBuilder
	{
		private Long id;
		private EventDTO event;
		private RowDTO locationRow;
		private Float price;

		private EventRowPriceDTOBuilder()
		{
		}

		public EventRowPriceDTOBuilder id(Long id)
		{
			this.id = id;
			return this;
		}

		public EventRowPriceDTOBuilder event(EventDTO event)
		{
			this.event = event;
			return this;
		}

		public EventRowPriceDTOBuilder locationRow(RowDTO locationRow)
		{
			this.locationRow = locationRow;
			return this;
		}

		public EventRowPriceDTOBuilder price(Float price)
		{
			this.price = price;
			return this;
		}

		public EventRowPriceDTO build()
		{
			EventRowPriceDTO eventRowPrice = new EventRowPriceDTO();
			eventRowPrice.setId(id);
			eventRowPrice.setEvent(event);
			eventRowPrice.setLocationRow(locationRow);
			eventRowPrice.setPrice(price);
			return eventRowPrice;
		}
	}
}
