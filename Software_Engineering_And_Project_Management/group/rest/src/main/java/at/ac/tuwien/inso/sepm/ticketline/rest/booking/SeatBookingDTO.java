package at.ac.tuwien.inso.sepm.ticketline.rest.booking;

import at.ac.tuwien.inso.sepm.ticketline.rest.event.EventAreaDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.location.RowDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.location.SeatDTO;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Objects;
import java.util.Set;

public class SeatBookingDTO extends BookingDTO
{

	private Set<SeatDTO> seats;

	public Set<SeatDTO> getSeats()
	{
		return seats;
	}

	public void setSeats(Set<SeatDTO> seats)
	{
		this.seats = seats;
	}

	public HashMap<RowDTO, LinkedList<SeatDTO>> seatsGroupedByRow()
	{
		HashMap<RowDTO, LinkedList<SeatDTO>> seatsPerRow = new HashMap<>();

		for(SeatDTO seat : getSeats())
		{
			RowDTO row = seat.getRow();

			LinkedList<SeatDTO> seatList;
			if(seatsPerRow.containsKey(row))
			{
				seatList = seatsPerRow.get(row);
			}
			else
			{
				seatList = new LinkedList<>();
				seatsPerRow.put(row, seatList);
			}

			seatList.add(seat);
		}

		return seatsPerRow;
	}

	public Float totalPriceThroughEvent()
	{
		float sum = 0.0f;
		for(EventAreaDTO eventAreaDTO : this.getEvent().getEventAreas())
		{
			RowDTO row = (RowDTO)eventAreaDTO.getArea();
			for(SeatDTO s : row.getSeats())
			{
				for(SeatDTO otherS : this.getSeats())
				{
					if(s.getId().equals(otherS.getId()))
					{
						sum += (eventAreaDTO.getPrice());
					}
				}
			}
		}
		return sum;
	}

	@Override
	public boolean equals(Object o)
	{
		if(this == o)
			return true;

		if(o == null || getClass() != o.getClass())
			return false;

		if(!super.equals(o))
			return false;

		SeatBookingDTO that = (SeatBookingDTO)o;
		return Objects.equals(seats, that.seats);
	}

	@Override
	public String toString()
	{
		return "SeatBookingDTO{" + "seats=" + seats + "} " + super.toString();
	}
}
