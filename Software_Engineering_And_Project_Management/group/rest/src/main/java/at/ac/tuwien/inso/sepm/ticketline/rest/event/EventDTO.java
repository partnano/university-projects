package at.ac.tuwien.inso.sepm.ticketline.rest.event;

import at.ac.tuwien.inso.sepm.ticketline.rest.booking.BookingDTO;
import at.ac.tuwien.inso.sepm.ticketline.rest.event.enums.Category;
import at.ac.tuwien.inso.sepm.ticketline.rest.location.LocationDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.Set;

@ApiModel(value = "EventDTO", description = "A DTO for information of an event")
public class EventDTO
{
	@ApiModelProperty(readOnly = true, name = "The automatically generated event id")
	private Long id;

	@ApiModelProperty(name = "The event name.")
	private String name;

	@ApiModelProperty(name = "The event description.")
	private String description;

	@ApiModelProperty(name = "The event time.")
	private LocalDateTime time;

	@ApiModelProperty(name = "The event artist.")
	private String artist;

	@ApiModelProperty(name = "The event category.")
	private Category category;

	@ApiModelProperty(name = "The event duration.")
	private Integer duration;

	@ApiModelProperty(name = "Location name of the event.")
	private LocationDTO location;

	@ApiModelProperty(name = "Areas (rows or sections) of the event and their respective prices.")
	private Set<EventAreaDTO> eventAreas;

	@ApiModelProperty(name = "Bookings (seats or sections) of the event")
	private Set<BookingDTO> bookings;

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public LocalDateTime getTime()
	{
		return time;
	}

	public void setTime(LocalDateTime time)
	{
		this.time = time;
	}

	public String getArtist()
	{
		return artist;
	}

	public void setArtist(String artist)
	{
		this.artist = artist;
	}

	public Category getCategory()
	{
		return category;
	}

	public void setCategory(Category category)
	{
		this.category = category;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public Integer getDuration()
	{
		return duration;
	}

	public void setDuration(Integer duration)
	{
		this.duration = duration;
	}

	public LocationDTO getLocation()
	{
		return location;
	}

	public void setLocation(LocationDTO location)
	{
		this.location = location;
	}

	public Set<BookingDTO> getBookings()
	{
		return bookings;
	}

	public void setBookings(Set<BookingDTO> bookings)
	{
		this.bookings = bookings;
	}

	public static EventDTOBuilder builder()
	{
		return new EventDTOBuilder();
	}

	public Set<EventAreaDTO> getEventAreas()
	{
		return eventAreas;
	}

	public void setEventAreas(Set<EventAreaDTO> eventAreas)
	{
		this.eventAreas = eventAreas;
	}

	@Override
	public boolean equals(Object o)
	{
		if(this == o)
			return true;

		if(o == null || getClass() != o.getClass())
			return false;

		EventDTO eventDTO = (EventDTO)o;

		return Objects.equals(id, eventDTO.id)
		       && Objects.equals(name, eventDTO.name)
		       && Objects.equals(description, eventDTO.description)
		       && Objects.equals(time, eventDTO.time)
		       && Objects.equals(artist, eventDTO.artist)
		       && category == eventDTO.category
		       && Objects.equals(duration, eventDTO.duration)
		       && Objects.equals(location, eventDTO.location)
		       && Objects.equals(eventAreas, eventDTO.eventAreas)
		       && Objects.equals(bookings, eventDTO.bookings);
	}

	@Override
	public String toString()
	{
		return "EventDTO{" + "id=" + id + ", name='" + name + '\'' + ", description='" + description + '\''
		       + ", time=" + time + ", artist='" + artist + '\'' + ", category=" + category + ", duration="
		       + duration + ", location=" + location + ", eventAreas=" + eventAreas + ", bookings=" + bookings
		       + '}';
	}

	public static final class EventDTOBuilder
	{
		private Long id;
		private String name;
		private String description;
		private LocalDateTime time;
		private String artist;
		private Category category;
		private Integer duration;
		private LocationDTO location;
		private Set<EventAreaDTO> eventAreas;
		private Set<BookingDTO> bookings;

		public EventDTOBuilder id(Long id)
		{
			this.id = id;
			return this;
		}

		public EventDTOBuilder name(String name)
		{
			this.name = name;
			return this;
		}

		public EventDTOBuilder description(String description)
		{
			this.description = description;
			return this;
		}

		public EventDTOBuilder time(LocalDateTime time)
		{
			this.time = time;
			return this;
		}

		public EventDTOBuilder artist(String artist)
		{
			this.artist = artist;
			return this;
		}

		public EventDTOBuilder category(Category category)
		{
			this.category = category;
			return this;
		}

		public EventDTOBuilder duration(Integer duration)
		{
			this.duration = duration;
			return this;
		}

		public EventDTOBuilder location(LocationDTO location)
		{
			this.location = location;
			return this;
		}

		public EventDTOBuilder areas(Set<EventAreaDTO> areas)
		{
			this.eventAreas = areas;
			return this;
		}

		public EventDTOBuilder bookings(Set<BookingDTO> bookings)
		{
			this.bookings = bookings;
			return this;
		}

		public EventDTO build()
		{
			EventDTO eventDTO = new EventDTO();
			eventDTO.setId(id);
			eventDTO.setName(name);
			eventDTO.setDescription(description);
			eventDTO.setTime(time);
			eventDTO.setArtist(artist);
			eventDTO.setCategory(category);
			eventDTO.setDuration(duration);
			eventDTO.setLocation(location);
			eventDTO.setEventAreas(eventAreas);
			eventDTO.setBookings(bookings);
			return eventDTO;
		}
	}
}
