package at.ac.tuwien.inso.sepm.ticketline.rest.location;

import java.util.Objects;
import java.util.Set;

public class RowDTO extends AreaDTO
{
	private Set<SeatDTO> seats;

	public Set<SeatDTO> getSeats()
	{
		return seats;
	}

	public RowDTO setSeats(Set<SeatDTO> seats)
	{
		this.seats = seats;
		return this;
	}

	@Override
	public boolean equals(Object o)
	{
		if(this == o)
			return true;

		if(o == null || getClass() != o.getClass())
			return false;

		RowDTO that = (RowDTO)o;
		return Objects.equals(getId(), that.getId()) && Objects.equals(getOrderIndex(), that.getOrderIndex())
		              && Objects.equals(getName(), that.getName());
	}

	@Override
	public String toString()
	{
		return "RowDTO{" + "seats=" + seats + "} " + super.toString();
	}

	public static RowDTOBuilder builder()
	{
		return new RowDTOBuilder();
	}

	public static final class RowDTOBuilder extends AreaDTOBuilder
	{
		private Set<SeatDTO> seats;

		private RowDTOBuilder()
		{
		}

		public RowDTOBuilder seats(Set<SeatDTO> seats)
		{
			this.seats = seats;
			return this;
		}

		@Override
		public RowDTOBuilder location(LocationDTO location)
		{
			return (RowDTOBuilder)super.location(location);
		}

		@Override
		public RowDTOBuilder id(Long id)
		{
			return (RowDTOBuilder)super.id(id);
		}

		@Override
		public RowDTOBuilder orderIndex(Long orderIndex)
		{
			return (RowDTOBuilder)super.orderIndex(orderIndex);
		}

		@Override
		public RowDTOBuilder name(String name)
		{
			return (RowDTOBuilder)super.name(name);
		}

		public RowDTO build()
		{
			RowDTO row = new RowDTO();
			row = (RowDTO)super.build(row);
			row.setSeats(seats);
			return row;
		}
	}
}
