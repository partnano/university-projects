package at.ac.tuwien.inso.sepm.ticketline.rest.event;

import at.ac.tuwien.inso.sepm.ticketline.rest.event.enums.Category;

import java.time.LocalDateTime;
import java.util.Objects;

public class EventFilterDTO
{
	private String artistFirstname;
	private String artistLastname;
	private LocalDateTime time;
	private Float price;
	private String locationName;
	private String street;
	private String city;
	private String zip;
	private String country;
	private String name;
	private String description;
	private Category category;
	private Integer duration;

	public String getArtistFirstname()
	{
		return artistFirstname;
	}

	public void setArtistFirstname(String artistFirstname)
	{
		this.artistFirstname = artistFirstname;
	}

	public String getArtistLastname()
	{
		return artistLastname;
	}

	public void setArtistLastname(String artistLastname)
	{
		this.artistLastname = artistLastname;
	}

	public LocalDateTime getTime()
	{
		return time;
	}

	public void setTime(LocalDateTime time)
	{
		this.time = time;
	}

	public Float getPrice()
	{
		return price;
	}

	public void setPrice(Float price)
	{
		this.price = price;
	}

	public String getLocationName()
	{
		return locationName;
	}

	public void setLocationName(String locationName)
	{
		this.locationName = locationName;
	}

	public String getStreet()
	{
		return street;
	}

	public void setStreet(String street)
	{
		this.street = street;
	}

	public String getZip()
	{
		return zip;
	}

	public void setZip(String zip)
	{
		this.zip = zip;
	}

	public String getCountry()
	{
		return country;
	}

	public void setCountry(String country)
	{
		this.country = country;
	}

	public String getCity()
	{
		return city;
	}

	public void setCity(String city)
	{
		this.city = city;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public Category getCategory()
	{
		return category;
	}

	public void setCategory(Category category)
	{
		this.category = category;
	}

	public Integer getDuration()
	{
		return duration;
	}

	public void setDuration(Integer duration)
	{
		this.duration = duration;
	}

	public EventFilterDTO()
	{
	}

	@Override
	public boolean equals(Object o)
	{
		if(this == o)
			return true;

		if(o == null || getClass() != o.getClass())
			return false;

		EventFilterDTO that = (EventFilterDTO)o;

		return Objects.equals(artistFirstname, that.artistFirstname)
		       && Objects.equals(artistLastname, that.artistLastname)
		       && Objects.equals(locationName, that.locationName)
		       && Objects.equals(street, that.street)
		       && Objects.equals(city, that.city)
		       && Objects.equals(zip, that.zip)
		       && Objects.equals(country, that.country)
		       && Objects.equals(name, that.name)
		       && Objects.equals(description, that.description)
		       && Objects.equals(duration, that.duration)
		       && Objects.equals(category, that.category)
		       && Objects.equals(time, that.time)
		       && Objects.equals(price, that.price);
	}

	@Override
	public String toString()
	{
		return "EventFilterDTO{"
		       + "artistFirstname='" + artistFirstname + '\''
		       + "artistLastname='" + artistLastname + '\''
		       + ", time=" + time
		       + ", price=" + price
		       + ", locationName='" + locationName + '\''
		       + ", street='" + street + '\''
		       + ", city='" + city + '\''
		       + ", zip='" + zip + '\''
		       + ", country='" + country + '\''
		       + ", name='" + name + '\''
		       + ", description='" + description + '\''
		       + ", category=" + category
		       + ", duration=" + duration
		       + '}';
	}
}
