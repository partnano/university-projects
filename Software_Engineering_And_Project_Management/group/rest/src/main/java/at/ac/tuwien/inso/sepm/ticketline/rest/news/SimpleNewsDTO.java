package at.ac.tuwien.inso.sepm.ticketline.rest.news;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.time.LocalDateTime;
import java.util.Objects;

@ApiModel(value = "DetailedNewsDTO", description = "A simple DTO for news entries via rest")
public class SimpleNewsDTO
{
	@ApiModelProperty(readOnly = true, name = "The automatically generated database id")
	private Long id;

	@ApiModelProperty(required = true, readOnly = true, name = "The date and time when the news was published")
	private LocalDateTime publishedAt;

	@ApiModelProperty(required = true, readOnly = true, name = "The title of the news")
	private String title;

	@ApiModelProperty(required = true, readOnly = true, name = "The summary of the news")
	private String summary;

	@ApiModelProperty(required = true, name = "Flag if news was already read")
	private boolean isRead;

	@ApiModelProperty(name = "News image")
	private byte[] byteImage;

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public LocalDateTime getPublishedAt()
	{
		return publishedAt;
	}

	public void setPublishedAt(LocalDateTime publishedAt)
	{
		this.publishedAt = publishedAt;
	}

	public String getTitle()
	{
		return title;
	}

	public void setTitle(String title)
	{
		this.title = title;
	}

	public String getSummary()
	{
		return summary;
	}

	public void setSummary(String summary)
	{
		this.summary = summary;
	}

	public boolean isRead()
	{
		return isRead;
	}

	public void setRead(boolean read)
	{
		isRead = read;
	}

	public byte[] getByteImage()
	{
		return byteImage;
	}

	public void setByteImage(byte[] byteImage)
	{
		this.byteImage = byteImage;
	}

	@Override
	public String toString()
	{
		return "SimpleNewsDTO{" + "id=" + id + ", publishedAt=" + publishedAt + ", title='" + title + '\''
		       + ", summary='" + summary + '\'' + ", isRead=" + isRead + '}';
	}

	@Override
	public boolean equals(Object o)
	{
		if(this == o)
			return true;

		if(o == null || getClass() != o.getClass())
			return false;

		SimpleNewsDTO that = (SimpleNewsDTO)o;
		return isRead == that.isRead && Objects.equals(id, that.id) && Objects.equals(publishedAt, that.publishedAt)
		       && Objects.equals(title, that.title) && Objects.equals(summary, that.summary);
	}

	@Override
	public int hashCode()
	{

		return Objects.hash(id, publishedAt, title, summary, isRead);
	}

	public static NewsDTOBuilder builder()
	{
		return new NewsDTOBuilder();
	}

	public static final class NewsDTOBuilder
	{
		private Long id;
		private LocalDateTime publishedAt;
		private String title;
		private String summary;
		private boolean isRead;
		private byte[] byteImage;

		public NewsDTOBuilder id(Long id)
		{
			this.id = id;
			return this;
		}

		public NewsDTOBuilder publishedAt(LocalDateTime publishedAt)
		{
			this.publishedAt = publishedAt;
			return this;
		}

		public NewsDTOBuilder title(String title)
		{
			this.title = title;
			return this;
		}

		public NewsDTOBuilder summary(String summary)
		{
			this.summary = summary;
			return this;
		}

		public NewsDTOBuilder isRead(boolean isRead)
		{
			this.isRead = isRead;
			return this;
		}

		public NewsDTOBuilder byteImage(byte[] byteImage)
		{
			this.byteImage = byteImage;
			return this;
		}

		public SimpleNewsDTO build()
		{
			SimpleNewsDTO result = new SimpleNewsDTO();
			result.setId(id);
			result.setPublishedAt(publishedAt);
			result.setTitle(title);
			result.setSummary(summary);
			result.setRead(isRead);
			result.setByteImage(byteImage);
			return result;
		}
	}
}
