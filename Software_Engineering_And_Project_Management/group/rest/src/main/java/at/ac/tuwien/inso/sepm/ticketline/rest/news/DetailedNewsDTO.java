package at.ac.tuwien.inso.sepm.ticketline.rest.news;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.time.LocalDateTime;
import java.util.Objects;

@ApiModel(value = "DetailedNewsDTO", description = "A detailed DTO for news entries via rest")
public class DetailedNewsDTO
{
	@ApiModelProperty(readOnly = true, name = "The automatically generated database id")
	private Long id;

	@ApiModelProperty(readOnly = true, name = "The date and time when the news was published")
	private LocalDateTime publishedAt;

	@ApiModelProperty(required = true, name = "The title of the news")
	private String title;

	@ApiModelProperty(required = true, name = "The text content of the news")
	private String text;

	@ApiModelProperty(required = true, name = "Flag if news was already read")
	private boolean isRead;

	@ApiModelProperty(name = "News image")
	private byte[] byteImage;

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public LocalDateTime getPublishedAt()
	{
		return publishedAt;
	}

	public void setPublishedAt(LocalDateTime publishedAt)
	{
		this.publishedAt = publishedAt;
	}

	public String getTitle()
	{
		return title;
	}

	public void setTitle(String title)
	{
		this.title = title;
	}

	public String getText()
	{
		return text;
	}

	public void setText(String text)
	{
		this.text = text;
	}

	public boolean isRead()
	{
		return isRead;
	}

	public void setRead(boolean read)
	{
		isRead = read;
	}

	public byte[] getByteImage()
	{
		return byteImage;
	}

	public void setByteImage(byte[] byteImage)
	{
		this.byteImage = byteImage;
	}

	@Override
	public String toString()
	{
		return "DetailedNewsDTO{" + "id=" + id + ", publishedAt=" + publishedAt + ", title='" + title + '\''
		       + ", text='" + text + '\'' + ", isRead=" + isRead + '}';
	}

	@Override
	public boolean equals(Object o)
	{
		if(this == o)
			return true;

		if(o == null || getClass() != o.getClass())
			return false;

		DetailedNewsDTO that = (DetailedNewsDTO)o;
		return isRead == that.isRead && Objects.equals(id, that.id) && Objects.equals(publishedAt, that.publishedAt)
		       && Objects.equals(title, that.title) && Objects.equals(text, that.text);
	}

	@Override
	public int hashCode()
	{

		return Objects.hash(id, publishedAt, title, text, isRead);
	}

	public static NewsDTOBuilder builder()
	{
		return new NewsDTOBuilder();
	}

	public static final class NewsDTOBuilder
	{
		private Long id;
		private LocalDateTime publishedAt;
		private String title;
		private String text;
		private boolean isRead;
		private byte[] byteImage;

		public NewsDTOBuilder id(Long id)
		{
			this.id = id;
			return this;
		}

		public NewsDTOBuilder publishedAt(LocalDateTime publishedAt)
		{
			this.publishedAt = publishedAt;
			return this;
		}

		public NewsDTOBuilder title(String title)
		{
			this.title = title;
			return this;
		}

		public NewsDTOBuilder text(String text)
		{
			this.text = text;
			return this;
		}

		public NewsDTOBuilder isRead(boolean isRead)
		{
			this.isRead = isRead;
			return this;
		}

		public NewsDTOBuilder byteImage(byte[] byteImage)
		{
			this.byteImage = byteImage;
			return this;
		}

		public DetailedNewsDTO build()
		{
			DetailedNewsDTO result = new DetailedNewsDTO();
			result.setId(id);
			result.setPublishedAt(publishedAt);
			result.setTitle(title);
			result.setText(text);
			result.setRead(isRead);
			result.setByteImage(byteImage);
			return result;
		}
	}
}
