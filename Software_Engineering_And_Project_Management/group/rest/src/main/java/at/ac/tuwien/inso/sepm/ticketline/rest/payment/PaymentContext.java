package at.ac.tuwien.inso.sepm.ticketline.rest.payment;

import at.ac.tuwien.inso.sepm.ticketline.rest.booking.BookingDTO;

public class PaymentContext
{
	private BookingDTO bookingDTO;
	private CreditCardDTO creditCardDTO;

	public BookingDTO getBookingDTO()
	{
		return bookingDTO;
	}

	public void setBookingDTO(BookingDTO bookingDTO)
	{
		this.bookingDTO = bookingDTO;
	}

	public CreditCardDTO getCreditCardDTO()
	{
		return creditCardDTO;
	}

	public void setCreditCardDTO(CreditCardDTO creditCardDTO)
	{
		this.creditCardDTO = creditCardDTO;
	}
}
