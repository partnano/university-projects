package at.ac.tuwien.inso.sepm.ticketline.rest.customer;

import at.ac.tuwien.inso.sepm.ticketline.rest.booking.BookingDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Objects;
import java.util.Set;

@ApiModel(value = "CustomerDTO", description = "A DTO for customer entries via REST")
public class CustomerDTO
{
	@ApiModelProperty(required = true, name = "The id of the customer")
	private Long id;

	@ApiModelProperty(required = true, name = "The first name of the customer")
	private String firstName;

	@ApiModelProperty(required = true, name = "The last name of the customer")
	private String lastName;

	@ApiModelProperty(required = true, name = "The email of the customer")
	private String email;

	@ApiModelProperty(required = true, name = "Customer Number of the customer")
	private long customerNumber;

	@ApiModelProperty(name = "Bookings of the customer")
	private Set<BookingDTO> bookings;

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public String getFirstName()
	{
		return firstName;
	}

	public void setFirstName(String firstName)
	{
		this.firstName = firstName;
	}

	public String getLastName()
	{
		return lastName;
	}

	public void setLastName(String lastName)
	{
		this.lastName = lastName;
	}

	public String getEmail()
	{
		return email;
	}

	public void setEmail(String email)
	{
		this.email = email;
	}

	public long getCustomerNumber()
	{
		return customerNumber;
	}

	public void setCustomerNumber(long customerNumber)
	{
		this.customerNumber = customerNumber;
	}

	public Set<BookingDTO> getBookings()
	{
		return bookings;
	}

	public void setBookings(Set<BookingDTO> bookings)
	{
		this.bookings = bookings;
	}

	@Override
	public boolean equals(Object o)
	{
		if(this == o)
			return true;

		if(o == null || getClass() != o.getClass())
			return false;

		CustomerDTO that = (CustomerDTO)o;

		return Objects.equals(id, that.id)
		       && Objects.equals(firstName, that.firstName)
		       && Objects.equals(lastName, that.lastName)
		       && Objects.equals(email, that.email)
		       && Objects.equals(customerNumber, that.customerNumber)
		       && Objects.equals(bookings, that.bookings);
	}

	@Override
	public String toString()
	{
		return "CustomerDTO{" +
				"id=" + id +
				", firstName='" + firstName + '\'' +
				", lastName='" + lastName + '\'' +
				", email='" + email + '\'' +
				", customerNumber=" + customerNumber +
				", bookings=" + bookings +
				'}';
	}
}
