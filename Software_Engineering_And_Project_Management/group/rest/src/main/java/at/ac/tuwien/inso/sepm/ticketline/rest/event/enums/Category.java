package at.ac.tuwien.inso.sepm.ticketline.rest.event.enums;

public enum Category
{
	CONCERT("events.category.concert"),
	MUSICAL("events.category.musical");

	private final String value;

	Category(String value)
	{
		this.value = value;
	}

	public String getValue()
	{
		return value;
	}

	public static Category fromString(String value)
	{
		switch(value)
		{
		case "events.category.concert":
			return Category.CONCERT;
		case "events.category.musical":
			return Category.MUSICAL;
		default:
			return null;
		}
	}

	@Override
	public String toString()
	{
		return getValue();
	}
}
