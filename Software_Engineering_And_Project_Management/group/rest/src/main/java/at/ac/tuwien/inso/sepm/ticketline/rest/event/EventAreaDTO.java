package at.ac.tuwien.inso.sepm.ticketline.rest.event;

import at.ac.tuwien.inso.sepm.ticketline.rest.location.AreaDTO;

import java.util.Objects;

public class EventAreaDTO
{
	private Long id;

	private AreaDTO area;

	private EventDTO event;

	private Float price;

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public AreaDTO getArea()
	{
		return area;
	}

	public void setArea(AreaDTO area)
	{
		this.area = area;
	}

	public EventDTO getEvent()
	{
		return event;
	}

	public void setEvent(EventDTO event)
	{
		this.event = event;
	}

	public Float getPrice()
	{
		return price;
	}

	public void setPrice(Float price)
	{
		this.price = price;
	}

	public static EventAreaDTOBuilder builder()
	{
		return new EventAreaDTOBuilder();
	}

	@Override
	public boolean equals(Object o)
	{
		if(this == o)
			return true;

		if(o == null || getClass() != o.getClass())
			return false;

		EventAreaDTO that = (EventAreaDTO)o;
		if(id == null)
			return Objects.equals(area, that.area);

		return Objects.equals(id, that.id);
	}

	@Override
	public String toString()
	{
		return "EventAreaDTO{" + "id=" + id + ", area=" + area + ", event=" + event + ", price=" + price + '}';
	}

	public static final class EventAreaDTOBuilder
	{
		private Long id;
		private AreaDTO area;
		private EventDTO event;
		private Float price;

		private EventAreaDTOBuilder()
		{
		}

		public EventAreaDTOBuilder id(Long id)
		{
			this.id = id;
			return this;
		}

		public EventAreaDTOBuilder area(AreaDTO area)
		{
			this.area = area;
			return this;
		}

		public EventAreaDTOBuilder event(EventDTO event)
		{
			this.event = event;
			return this;
		}

		public EventAreaDTOBuilder price(Float price)
		{
			this.price = price;
			return this;
		}

		public EventAreaDTO build()
		{
			EventAreaDTO eventArea = new EventAreaDTO();
			eventArea.setId(id);
			eventArea.setArea(area);
			eventArea.setEvent(event);
			eventArea.setPrice(price);
			return eventArea;
		}
	}
}
