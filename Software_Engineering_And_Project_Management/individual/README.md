# SEPM Einzelphase

## Author

Name: Wick Bernhard\
Matrikelnummer: e01525699

## Kurzanleitung

Das Programm kann mit dem Befehl:\
```mvnw compile```\
gebaut werden können.

Das Programm kann mit dem Befehl:\
```mvnw test```\
getestet werden können.

Das Programm kann mit dem Befehl:\
```mvnw exec:java```\
ausgeführt werden.