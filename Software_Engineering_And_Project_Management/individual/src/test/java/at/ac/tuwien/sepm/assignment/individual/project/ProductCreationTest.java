package at.ac.tuwien.sepm.assignment.individual.project;

import at.ac.tuwien.sepm.assignment.individual.project.entities.Product;
import at.ac.tuwien.sepm.assignment.individual.project.exceptions.*;
import at.ac.tuwien.sepm.assignment.individual.project.persistence.*;
import at.ac.tuwien.sepm.assignment.individual.project.service.ProjectService;
import at.ac.tuwien.sepm.assignment.individual.project.service.ProjectServiceImpl;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Test;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

public class ProductCreationTest {
    private final ProductDAO productDAO = new ProductDAOImpl();
    private final OrderDAO orderDAO = new OrderDAOImpl();
    private final ProjectService service = new ProjectServiceImpl(productDAO, orderDAO);

    // should be a fail in service
    @Test(expected = RequiredFieldException.class)
    public void testCreateWithoutRequired() throws
        RequiredFieldException, NegativePriceException,
        InvalidFileException, CreateErrorException
    {
        Product testProduct =
            new Product("", "", "", "12.10", Product.CATEGORIES[0], Product.TAXING[0]);

        service.createProduct(testProduct);
    }

    // should be a fail in service
    @Test(expected = NumberFormatException.class)
    public void testCreateWithInvalidPrice() throws
        NegativePriceException, RequiredFieldException,
        InvalidFileException, CreateErrorException
    {
        Product testProduct =
            new Product("product", "", "", "twelve", Product.CATEGORIES[0], Product.TAXING[0]);

        service.createProduct(testProduct);
    }

    // should be a fail in persistence
    @Test(expected = CreateErrorException.class)
    public void testCreateWithNullValue() throws CreateErrorException {
        Product testProduct =
            new Product(null, null, null, "12.10", Product.CATEGORIES[0], Product.TAXING[0]);

        productDAO.create(testProduct);
    }

    // successful creation
    @Test
    public void testCreateCorrectProduct() throws
        NegativePriceException, InvalidFileException,
        RequiredFieldException, CreateErrorException
    {
        Product testProduct =
            new Product("TestProductPleaseDelete", "description", "",
                "12.10", Product.CATEGORIES[0], Product.TAXING[0]);

        service.createProduct(testProduct);
    }

    @After
    public void deleteTestProduct() throws RetrieveErrorException, DeletionErrorException {
        ArrayList<Product> products = service.getAllProducts(true);
        ArrayList<Product> toDelete = new ArrayList<>();

        for (Product product : products) {
            if (product.getName().equals("TestProductPleaseDelete")) {
                toDelete.add(product);
            }
        }

        service.deleteProducts(toDelete);
    }
}
