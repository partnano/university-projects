package at.ac.tuwien.sepm.assignment.individual.project;

import at.ac.tuwien.sepm.assignment.individual.project.entities.Order;
import at.ac.tuwien.sepm.assignment.individual.project.exceptions.*;
import at.ac.tuwien.sepm.assignment.individual.project.persistence.*;
import at.ac.tuwien.sepm.assignment.individual.project.service.ProjectService;
import at.ac.tuwien.sepm.assignment.individual.project.service.ProjectServiceImpl;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class OrderCreationTest {
    private final OrderDAO orderDAO = new OrderDAOImpl();
    private final ProductDAO productDAO = new ProductDAOImpl();
    private final ProjectService service = new ProjectServiceImpl(productDAO, orderDAO);

    private int receiptNumber = -1;

    // ASSUMES THAT AT LEAST ONE PRODUCT IS IN DATABASE
    // persistence test
    @Test
    public void createValidOrder() throws CreateErrorException, UpdateErrorException, RetrieveErrorException {
        Order newOrder = new Order(1, false);
        orderDAO.create(newOrder);

        receiptNumber = newOrder.getReceiptNumber();
    }

    // service test
    @Test(expected = DuplicateOrderException.class)
    public void createAlreadyExistingOrder() throws
        CreateErrorException, DuplicateOrderException,
        NumberFormatException, NegativePriceException
    {
        Order newOrder = new Order("1");
        receiptNumber = newOrder.getReceiptNumber();

        service.createOrder(newOrder);
        service.createOrder(newOrder);

    }

    @Test(expected = NumberFormatException.class)
    public void createInvalidTableNumberOrder() throws
        CreateErrorException, DuplicateOrderException,
        NumberFormatException, NegativePriceException
    {
        Order newOrder = new Order("stuff");
        service.createOrder(newOrder);
    }

    @Test(expected = NegativePriceException.class)
    public void createNegativeTableNumberOrder() throws
        CreateErrorException, DuplicateOrderException,
        NumberFormatException, NegativePriceException
    {
        Order newOrder = new Order("-13");
        service.createOrder(newOrder);
    }

    @After
    public void deleteOrder() {
        if (receiptNumber == -1)
            return;

        Connection conn = H2Connection.getInstance().getConn();
        PreparedStatement stmt = null;
        String deleteSql = "delete from orders where receiptnumber = ?";

        try {
            stmt = conn.prepareStatement(deleteSql);
            stmt.setInt(1, receiptNumber);
            stmt.executeUpdate();

            receiptNumber = -1;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            H2Connection.closeStatement(stmt);
        }
    }
}
