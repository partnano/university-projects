create table if not exists products
(
  product_id  int            auto_increment primary key,
  name        varchar(255)   not null,
  description varchar(1000),
  price       int            not null,
  taxed_price int            not null,
  imagepath   varchar(1000),
  category    varchar(100)   not null,
  taxing      varchar(100)   not null,
  creation    timestamp      not null,
  last_edit   timestamp,
  deleted     boolean        default false
);

create sequence rec_seq start with 1000 increment by 1;
create table if not exists orders
(
  order_id      int       auto_increment primary key,
  tablenumber   int       not null,
  receiptnumber int       not null default rec_seq.nextval,
  creation      timestamp not null,
  finish_time   timestamp,
  payment       varchar(255),
  finished      boolean   default false
);

create table if not exists product_in_order
(
  product_id int not null,
  order_id   int not null,
  amount     int not null,
  foreign key (product_id) references products (product_id),
  foreign key (order_id)   references orders   (order_id),
  primary key (product_id, order_id)
);