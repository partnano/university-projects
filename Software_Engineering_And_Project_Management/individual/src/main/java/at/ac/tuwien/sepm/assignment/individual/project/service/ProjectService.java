package at.ac.tuwien.sepm.assignment.individual.project.service;

import at.ac.tuwien.sepm.assignment.individual.project.entities.Order;
import at.ac.tuwien.sepm.assignment.individual.project.entities.Product;
import at.ac.tuwien.sepm.assignment.individual.project.entities.ProductSearch;
import at.ac.tuwien.sepm.assignment.individual.project.exceptions.*;

import java.util.ArrayList;

/**
 * Interface for service layer. Manages calculation and validation of data.
 */
public interface ProjectService {

    /**
     * Validates input product and calls productDAO product creation.
     *
     * @param newProd Input product to be validated and handed over to persistence layer.
     * @throws NumberFormatException Thrown if input price is invalid.
     * @throws NegativePriceException Thrown if input price is negative. (Invalid as well)
     * @throws RequiredFieldException Thrown if a required field is left out.
     * @throws InvalidFileException Thrown if path to image is invalid. (Can be empty, however)
     * @throws CreateErrorException Thrown if persistence throws this.
     */
    void createProduct (Product newProd) throws
        NumberFormatException, NegativePriceException,
        RequiredFieldException, InvalidFileException,
        CreateErrorException;

    /**
     * Validates input product and calls productDAO product update.
     *
     * @param updProd Input product to be validated and handed over to persistence layer.
     * @throws NumberFormatException Thrown if input price is invalid.
     * @throws NegativePriceException Thrown if input price is negative. (Invalid as well)
     * @throws RequiredFieldException Thrown if a required field is left out.
     * @throws InvalidFileException Thrown if path to image is invalid. (Can be empty, however)
     * @throws UpdateErrorException Thrown if persistence throws this.
     */
    void updateProduct (Product updProd) throws
        NumberFormatException, NegativePriceException,
        RequiredFieldException, InvalidFileException,
        UpdateErrorException;

    /**
     * Calls persistence to retrieve all products from saving medium.
     *
     * @param deleted If deleted products should be returned as well
     * @return ArrayList of all products in saving medium.
     * @throws RetrieveErrorException Thrown if persistence throws this.
     */
    ArrayList<Product> getAllProducts (boolean deleted) throws
        RetrieveErrorException;

    /**
     * Validates search DTO and calls search function in DAO. Search logic is
     * in saving medium.
     *
     * @param search Search DTO with properties for the search
     * @return ArrayList of found products (can be empty)
     * @throws NumberFormatException Thrown if from/to prices are invalid.
     */
    ArrayList<Product> searchProducts (ProductSearch search) throws
        NumberFormatException, RetrieveErrorException;

    /**
     * Validates and hands over list of products to be deleted from saving medium.
     *
     * @param productsToDelete List of products to be deleted.
     * @throws DeletionErrorException Thrown if persistence throws this.
     */
    void deleteProducts (ArrayList<Product> productsToDelete) throws
        DeletionErrorException;

    /**
     * Validates and hands over new order to be created in saving medium.
     * This ignores product map contents!
     *
     * @param newOrder The order to be created.
     * @throws CreateErrorException Thrown if persistence throws this.
     */
    void createOrder (Order newOrder) throws
        CreateErrorException, NumberFormatException,
        NegativePriceException, DuplicateOrderException;

    /**
     * Hands over order to be updated.
     *
     * @param updOrder Order to be updated.
     * @throws UpdateErrorException Thrown if persistence throws this
     */
    void updateOrder (Order updOrder) throws
        UpdateErrorException;

    /**
     * Calls persistence to retrieve all orders from saving medium.
     *
     * @return ArrayList of all orders in saving medium.
     * @throws RetrieveErrorException Thrown if persistence throws this.
     */
    ArrayList<Order> getAllOrders () throws
        RetrieveErrorException;
}
