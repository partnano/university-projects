package at.ac.tuwien.sepm.assignment.individual.project.exceptions;

public class UpdateErrorException extends Exception {
    public UpdateErrorException() {
    }

    public UpdateErrorException(String message) {
        super(message);
    }
}
