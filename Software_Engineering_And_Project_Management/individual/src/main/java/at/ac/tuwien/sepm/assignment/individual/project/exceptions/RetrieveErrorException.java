package at.ac.tuwien.sepm.assignment.individual.project.exceptions;

public class RetrieveErrorException extends Exception {
    public RetrieveErrorException() {
    }

    public RetrieveErrorException(String message) {
        super(message);
    }
}
