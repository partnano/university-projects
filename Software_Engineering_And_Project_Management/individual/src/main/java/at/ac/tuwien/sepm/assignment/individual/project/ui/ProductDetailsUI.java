package at.ac.tuwien.sepm.assignment.individual.project.ui;

import at.ac.tuwien.sepm.assignment.individual.project.entities.Product;
import at.ac.tuwien.sepm.assignment.individual.project.exceptions.*;
import at.ac.tuwien.sepm.assignment.individual.project.service.ProjectService;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.lang.invoke.MethodHandles;
import java.net.URL;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;

public class ProductDetailsUI implements ProjectUI, Initializable {

    private static final Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    private Stage stage;
    private ProjectService service;
    private Product currentProduct = null;

    @FXML
    private TextField txtDetailsName;
    @FXML
    private TextField txtDetailsDesc;
    @FXML
    private TextField txtDetailsPrice;
    @FXML
    private TextField txtDetailsImg;
    @FXML
    private ComboBox<String> cmbDetailsCategory;
    @FXML
    private ComboBox<String> cmbDetailsTaxes;
    @FXML
    private Label lblDetailsInfo;
    @FXML
    private Label lblDetailsHead;
    @FXML
    private Label lblDetailsCreatedOn;
    @FXML
    private Label lblDetailsLastEdit;

    @FXML
    private Button btnDetailsActivate;
    @FXML
    private Button btnDetailsImg;

    public ProductDetailsUI(ProjectService service, Stage stage) {
        this.service = service;
        this.stage = stage;
    }

    public ProductDetailsUI(ProjectService service, Stage stage, Product currentProduct) {
        this.service = service;
        this.stage = stage;
        this.currentProduct = currentProduct;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        cmbDetailsCategory.getItems().removeAll(cmbDetailsCategory.getItems());
        cmbDetailsCategory.getItems().addAll(Product.CATEGORIES[0], Product.CATEGORIES[1], Product.CATEGORIES[2]);

        cmbDetailsTaxes.getItems().removeAll(cmbDetailsTaxes.getItems());
        cmbDetailsTaxes.getItems().addAll(Product.TAXING[0], Product.TAXING[1]);

        if (currentProduct != null) {
            lblDetailsHead.setText("PRODUKT BEARBEITEN");

            txtDetailsName.setText(currentProduct.getName());
            txtDetailsDesc.setText(currentProduct.getDescription());

            txtDetailsPrice.setText(currentProduct.getFormattedPrice());

            cmbDetailsCategory.getSelectionModel().select(currentProduct.getCategory());
            cmbDetailsTaxes.getSelectionModel().select(currentProduct.getTaxing());

            txtDetailsImg.setText(currentProduct.getImagePath());

            DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm");
            String output  = currentProduct.getCreatedOn().format((dateFormat));

            lblDetailsCreatedOn.setText("Erstellt am: " + output);

            if (currentProduct.getLastEdit() != null) {
                output = dateFormat.format(currentProduct.getLastEdit());
                lblDetailsLastEdit.setText("Zuletzt bearbeitet: " + output);
            }

        } else {
            cmbDetailsCategory.getSelectionModel().select(Product.CATEGORIES[1]);
            cmbDetailsTaxes.getSelectionModel().select(Product.TAXING[0]);
        }
    }

    @FXML
    private void detailsActivate () {
        LOG.trace("called createNewProduct");

        String name      = txtDetailsName.getText();
        String desc      = txtDetailsDesc.getText();
        String price     = txtDetailsPrice.getText();
        String imagePath = txtDetailsImg.getText();
        String category  = cmbDetailsCategory.getSelectionModel().getSelectedItem();
        String taxing    = cmbDetailsTaxes.getSelectionModel().getSelectedItem();

        if (imagePath == null)
            imagePath = "";

        Product newProd = new Product(name, desc, imagePath, price, category, taxing);

        if (currentProduct != null) {
            newProd.setId(currentProduct.getId());
            newProd.setOldImagePath(currentProduct.getImagePath());
            newProd.setCreatedOn(currentProduct.getCreatedOn());
            newProd.setLastEdit(currentProduct.getLastEdit());
        }

        lblDetailsInfo.setText("");

        LOG.debug(newProd.getCategory() + ", " + newProd.getTaxing());

        try {
            if (currentProduct == null)
                service.createProduct(newProd);
            else
                service.updateProduct(newProd);

            lblDetailsInfo.setText("Anlegen erfolgreich!");
            lblDetailsInfo.setTextFill(Color.GREEN);

            stage.close();

        } catch (NumberFormatException e) {
            LOG.error("NumberFormatException! " + e.getMessage());

            lblDetailsInfo.setText("Ungültiges Preisformat!");
            lblDetailsInfo.setTextFill(Color.RED);

        } catch (NegativePriceException e) {
            LOG.error("NegativePriceException! " + e.getMessage());

            lblDetailsInfo.setText("Ungültiger negativer Preis!");
            lblDetailsInfo.setTextFill(Color.RED);

        } catch (RequiredFieldException e) {
            LOG.error("RequiredFieldException! " + e.getMessage());

            lblDetailsInfo.setText("Fehlendes Pflichtfeld!");
            lblDetailsInfo.setTextFill(Color.RED);

        } catch (InvalidFileException e) {
            LOG.error("InvalidFileException! " + e.getMessage());

            lblDetailsInfo.setText(e.getMessage());
            lblDetailsInfo.setTextFill(Color.RED);

        } catch (CreateErrorException e) {
            // log in persistence

            lblDetailsInfo.setText("Fehler beim Erstellen!");
            lblDetailsInfo.setTextFill(Color.RED);

        } catch (UpdateErrorException e) {
            // log in persistence

            lblDetailsInfo.setText("Fehler beim Bearbeiten!");
            lblDetailsInfo.setTextFill(Color.RED);
        }
    }

    @FXML
    private void chooseImage() {
        LOG.trace("called chooseImage");

        FileChooser fc = new FileChooser();
        FileChooser.ExtensionFilter filter =
            new FileChooser.ExtensionFilter("Image Files", "*.png", "*.jpg", "*.jpeg");

        fc.setTitle("Bild Auswahl...");
        fc.getExtensionFilters().add(filter);
        File file = fc.showOpenDialog(stage);

        LOG.info("Opened file chooser.");

        if (file != null) {
            txtDetailsImg.setText(file.getAbsolutePath());
        }
    }
}