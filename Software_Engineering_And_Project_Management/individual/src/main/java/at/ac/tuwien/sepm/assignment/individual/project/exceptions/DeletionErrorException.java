package at.ac.tuwien.sepm.assignment.individual.project.exceptions;

public class DeletionErrorException extends Exception {
    public DeletionErrorException() {
    }

    public DeletionErrorException(String message) {
        super(message);
    }
}
