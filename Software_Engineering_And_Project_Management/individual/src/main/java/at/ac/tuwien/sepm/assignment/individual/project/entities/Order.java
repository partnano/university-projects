package at.ac.tuwien.sepm.assignment.individual.project.entities;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;

public class Order {

    private int orderId;
    private int tableNumber;
    private int receiptNumber;

    private String tableNumberTxt;
    private String payment;

    private LocalDateTime creation;
    private LocalDateTime finishTime;

    private boolean finished;

    private HashMap<Integer, Integer> orderedProducts = new HashMap<>();
    private ArrayList<Product> orderedProductsList = new ArrayList<>();

    private double totalTaxedPrice;
    private double totalTenPercent;
    private double totalTwentyPercent;

    public Order() {}
    public Order(int tableNumber, boolean finished) {
        this.tableNumber = tableNumber;
        this.finished = finished;
    }
    public Order(String tableNumberTxt) {
        this.tableNumberTxt = tableNumberTxt;
        this.finished = false;
    }

    public void addToOrder(Product product, int amount) {
        if (!orderedProducts.containsKey(product.getId()))
            orderedProductsList.add(product);

        orderedProducts.merge(product.getId(), amount, (a, b) -> a + b);
    }

    public void removeFromOrder(Product product) {
        Integer amount = orderedProducts.get(product.getId());
        if (amount != null)
            orderedProducts.put(product.getId(), Math.max(0, amount -1));
    }

    public int getOrderId() {
        return orderId;
    }
    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public int getTableNumber() {
        return tableNumber;
    }
    public void setTableNumber(int tableNumber) {
        this.tableNumber = tableNumber;
    }

    public LocalDateTime getCreation() {
        return creation;
    }
    public void setCreation(LocalDateTime creation) {
        this.creation = creation;
    }

    public boolean isFinished() {
        return finished;
    }
    public void setFinished(boolean finished) {
        this.finished = finished;
    }

    public HashMap<Integer, Integer> getOrderedProducts() {
        return orderedProducts;
    }
    public void setOrderedProducts(HashMap<Integer, Integer> orderedProducts) {
        this.orderedProducts = orderedProducts;
    }

    public String getTableNumberTxt() {
        return tableNumberTxt;
    }
    public void setTableNumberTxt(String tableNumberTxt) {
        this.tableNumberTxt = tableNumberTxt;
    }

    public ArrayList<Product> getOrderedProductsList() {
        return orderedProductsList;
    }
    public void setOrderedProductsList(ArrayList<Product> orderedProductsList) {
        this.orderedProductsList = orderedProductsList;
    }

    public LocalDateTime getFinishTime() {
        return finishTime;
    }
    public void setFinishTime(LocalDateTime finishTime) {
        this.finishTime = finishTime;
    }

    public String getPayment() {
        return payment;
    }
    public void setPayment(String payment) {
        this.payment = payment;
    }

    public double getTotalTaxedPrice() {
        return totalTaxedPrice;
    }
    public void setTotalTaxedPrice(double totalTaxedPrice) {
        this.totalTaxedPrice = totalTaxedPrice;
    }

    public double getTotalTenPercent() {
        return totalTenPercent;
    }
    public void setTotalTenPercent(double totalTenPercent) {
        this.totalTenPercent = totalTenPercent;
    }

    public double getTotalTwentyPercent() {
        return totalTwentyPercent;
    }
    public void setTotalTwentyPercent(double totalTwentyPercent) {
        this.totalTwentyPercent = totalTwentyPercent;
    }

    public int getReceiptNumber() {
        return receiptNumber;
    }
    public void setReceiptNumber(int receiptNumber) {
        this.receiptNumber = receiptNumber;
    }
}
