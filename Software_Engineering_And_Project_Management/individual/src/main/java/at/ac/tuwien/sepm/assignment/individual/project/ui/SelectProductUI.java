package at.ac.tuwien.sepm.assignment.individual.project.ui;

import at.ac.tuwien.sepm.assignment.individual.project.entities.Order;
import at.ac.tuwien.sepm.assignment.individual.project.entities.Product;
import at.ac.tuwien.sepm.assignment.individual.project.exceptions.RetrieveErrorException;
import at.ac.tuwien.sepm.assignment.individual.project.exceptions.UpdateErrorException;
import at.ac.tuwien.sepm.assignment.individual.project.service.ProjectService;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.invoke.MethodHandles;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class SelectProductUI implements ProjectUI, Initializable {

    private static final Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    private ProjectService service;
    private Stage stage;
    private Order order;
    private ArrayList<Product> products;

    @FXML
    ListView<String> lstProducts;
    @FXML
    TextField txtAmount;
    @FXML
    Button btnAddProduct;

    public SelectProductUI (ProjectService service, Stage stage, Order order) {
        this.service = service;
        this.stage = stage;
        this.order = order;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {
            products = service.getAllProducts(false);
            ObservableList<String> productsObs = FXCollections.observableArrayList();

            for (Product product : products) {
                productsObs.add(product.getName());
            }

            lstProducts.setItems(productsObs);

        } catch (RetrieveErrorException e) {
            LOG.error("Error on retrieval! (Probably persistence)");
        }
    }

    @FXML
    public void addProductToOrder() {

        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Fehler!");
        alert.setHeaderText("Fehler!");

        try {
            int index = lstProducts.getSelectionModel().getSelectedIndex();

            if (index < 0) {
                alert.setContentText("Kein Produkt ausgewählt!");
                alert.showAndWait();
                return;
            }

            Product product = products.get(index);

            try {
                int amount = Integer.parseInt(txtAmount.getText());

                order.addToOrder(product, amount);
                service.updateOrder(order);

            } catch (NumberFormatException e) {
                LOG.error("NumberFormatException! - Not a valid product amount.");

                alert.setContentText("Keine gültige Anzahl!");
                alert.showAndWait();
                return;
            }

        } catch (UpdateErrorException e) {
            alert.setContentText("Fehler beim updaten der Bestellung");
            alert.showAndWait();

        }

        stage.close();
    }
}
