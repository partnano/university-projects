package at.ac.tuwien.sepm.assignment.individual.project.persistence;

import at.ac.tuwien.sepm.assignment.individual.project.entities.Order;
import at.ac.tuwien.sepm.assignment.individual.project.exceptions.CreateErrorException;
import at.ac.tuwien.sepm.assignment.individual.project.exceptions.RetrieveErrorException;
import at.ac.tuwien.sepm.assignment.individual.project.exceptions.UpdateErrorException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.invoke.MethodHandles;
import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;

public class OrderDAOImpl implements OrderDAO {
    private static final Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    private Connection h2Conn;

    public OrderDAOImpl () {
        this.h2Conn = H2Connection.getInstance().getConn();
    }

    @Override
    public void create(Order newOrder) throws CreateErrorException {
        LOG.trace("Called create in order DAO");

        PreparedStatement stmt = null;
        String createOrderSql = "insert into orders (tablenumber, creation, finished) values (?, ?, ?);";

        try {
            stmt = h2Conn.prepareStatement(createOrderSql, Statement.RETURN_GENERATED_KEYS);

            stmt.setInt(1, newOrder.getTableNumber());
            stmt.setTimestamp(2, getCurrentTimeStamp());
            stmt.setBoolean(3, newOrder.isFinished());

            stmt.executeUpdate();

            ResultSet generatedKeys = stmt.getGeneratedKeys();
            if (generatedKeys.next())
                newOrder.setReceiptNumber(generatedKeys.getInt(1));

            LOG.info("Successfully created new order. (" + newOrder.getReceiptNumber() + ")");

        } catch (SQLException e) {
            LOG.error ("SQLException! " + e.getMessage());
            throw new CreateErrorException();
        } finally {
            H2Connection.closeStatement(stmt);
        }
    }

    @Override
    public void update(Order updOrder) throws UpdateErrorException {
        LOG.trace("Called update in order DAO");

        PreparedStatement stmt = null;
        String updateOrderSql = "update orders set finish_time = ?, payment = ?, finished = true where order_id = ?";
        // language=MySQL
        String updateProductsSql = "insert into product_in_order (product_id, order_id, amount) values (?, ?, ?) on duplicate key update amount=?;";
        String deleteProductSql = "delete from product_in_order where product_id = ? and order_id = ?;";

        try {

            for (HashMap.Entry<Integer, Integer> entry : updOrder.getOrderedProducts().entrySet()) {

                if (entry.getValue() == 0) {
                    stmt = h2Conn.prepareStatement(deleteProductSql);

                    stmt.setInt(1, entry.getKey());
                    stmt.setInt(2, updOrder.getOrderId());

                    stmt.executeUpdate();

                } else {
                    stmt = h2Conn.prepareStatement(updateProductsSql);

                    stmt.setInt(1, entry.getKey());
                    stmt.setInt(2, updOrder.getOrderId());
                    stmt.setInt(3, entry.getValue());
                    stmt.setInt(4, entry.getValue());

                    stmt.executeUpdate();
                }
            }

            LOG.info("Updated ordered products in order " + updOrder.getOrderId() + ".");

            if (updOrder.isFinished()) {
                stmt = h2Conn.prepareStatement(updateOrderSql);

                stmt.setTimestamp(1, getCurrentTimeStamp());
                stmt.setString(2, updOrder.getPayment());
                stmt.setInt(3, updOrder.getOrderId());

                stmt.executeUpdate();

                LOG.info("Updated order " + updOrder.getOrderId() + " - finished it.");
            }

        } catch (SQLException e) {
            LOG.error ("SQLException!\n" + e.getMessage());
            throw new UpdateErrorException();
        } finally {
            H2Connection.closeStatement(stmt);
        }
    }

    @Override
    public ArrayList<Order> readAll() throws RetrieveErrorException {
        LOG.trace("Called readAll in order DAO");

        ArrayList<Order> orders = new ArrayList<>();

        PreparedStatement stmt = null;
        String orderSql = "select * from orders";
        String productsSql = "select * from product_in_order where order_id = ?";

        try {

            stmt = h2Conn.prepareStatement(orderSql);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                Order newOrder = new Order();
                newOrder.setOrderId       (rs.getInt("ORDER_ID"));
                newOrder.setTableNumber   (rs.getInt("TABLENUMBER"));
                newOrder.setCreation      (rs.getTimestamp("CREATION").toLocalDateTime());
                newOrder.setFinished      (rs.getBoolean("FINISHED"));
                newOrder.setPayment       (rs.getString("PAYMENT"));
                newOrder.setReceiptNumber (rs.getInt("RECEIPTNUMBER"));

                Timestamp ts = rs.getTimestamp("FINISH_TIME");
                if (ts != null)
                    newOrder.setFinishTime(rs.getTimestamp("FINISH_TIME").toLocalDateTime());

                orders.add(newOrder);
            }

            for (Order order : orders) {
                stmt = h2Conn.prepareStatement(productsSql);

                stmt.setInt(1, order.getOrderId());

                rs = stmt.executeQuery();

                while (rs.next()) {
                    HashMap<Integer, Integer> products = order.getOrderedProducts();
                    products.put(rs.getInt("PRODUCT_ID"), rs.getInt("AMOUNT"));
                }
            }

            LOG.info("Retrieved all available orders.");
            return orders;

        } catch (SQLException e) {
            LOG.error ("SQLException! " + e.getMessage());
            throw new RetrieveErrorException();
        } finally {
            H2Connection.closeStatement(stmt);
        }
    }

    // HELPER FUNCTIONS

    private Timestamp getCurrentTimeStamp() {
        LocalDateTime now = LocalDateTime.now();
        return Timestamp.valueOf(now);
    }
}
