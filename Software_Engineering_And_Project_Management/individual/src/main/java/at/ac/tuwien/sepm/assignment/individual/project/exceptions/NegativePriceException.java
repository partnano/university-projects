package at.ac.tuwien.sepm.assignment.individual.project.exceptions;

public class NegativePriceException extends Exception {
    public NegativePriceException() {}
    public NegativePriceException(String message) { super(message); }
}
