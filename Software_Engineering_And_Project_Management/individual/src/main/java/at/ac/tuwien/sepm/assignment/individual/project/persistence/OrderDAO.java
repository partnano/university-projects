package at.ac.tuwien.sepm.assignment.individual.project.persistence;

import at.ac.tuwien.sepm.assignment.individual.project.entities.Order;
import at.ac.tuwien.sepm.assignment.individual.project.exceptions.CreateErrorException;
import at.ac.tuwien.sepm.assignment.individual.project.exceptions.RetrieveErrorException;
import at.ac.tuwien.sepm.assignment.individual.project.exceptions.UpdateErrorException;

import java.util.ArrayList;

/**
 * DAO for order management. It handles the saving, updating, retrieving and updating of
 * order objects
 */
public interface OrderDAO {
    /**
     * Create function for a new order, this should usually be an open order
     * (finished == false)
     *
     * @param newOrder New order to be created, assumes products map is empty (doesn't save to medium)
     * @throws CreateErrorException Thrown on error in saving medium
     */
    void create(Order newOrder) throws CreateErrorException;

    /**
     * Update function for an order. (only updates products lisT!)
     *
     * @param updOrder Order to be updated.
     * @throws UpdateErrorException Thrown on error in saving medium
     */
    void update(Order updOrder) throws UpdateErrorException;

    /**
     * Function to retrieve all orders.
     *
     * @return An array list of all orders.
     * @throws RetrieveErrorException Thrown on error in saving medium
     */
    ArrayList<Order> readAll() throws RetrieveErrorException;
}
