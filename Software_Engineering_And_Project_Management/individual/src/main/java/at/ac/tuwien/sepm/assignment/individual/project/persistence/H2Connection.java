package at.ac.tuwien.sepm.assignment.individual.project.persistence;

import org.h2.jdbcx.JdbcDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.invoke.MethodHandles;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class H2Connection {

    private static final Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    private Connection conn;

    private H2Connection() {

        JdbcDataSource ds = new JdbcDataSource();
        ds.setURL("jdbc:h2:./src/main/resources/database/database;mode=MySQL");
        ds.setUser("sa");
        ds.setPassword("");

        try {
            this.conn = ds.getConnection();
        } catch (SQLException e) {
            LOG.error("SQLException on creating connection! " + e.getMessage());
        }
    }

    private static class H2ConnectionHolder {
        private static final H2Connection INSTANCE = new H2Connection();
    }

    public static H2Connection getInstance () {
        return H2ConnectionHolder.INSTANCE;
    }

    public static void closeStatement (PreparedStatement stmt) {
        if (stmt != null) {
            try {
                stmt.close();
            } catch (SQLException e) {
                LOG.warn("could not close preparedStatement");
                e.printStackTrace();
            }
        }
    }

    public Connection getConn() { return conn; }
}
