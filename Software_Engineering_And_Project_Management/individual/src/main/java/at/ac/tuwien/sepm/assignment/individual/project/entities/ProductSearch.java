package at.ac.tuwien.sepm.assignment.individual.project.entities;

public class ProductSearch {

    private String name;
    private String category;

    private String txtFromTaxless;
    private String txtToTaxless;
    private String txtFromTaxed;
    private String txtToTaxed;

    private int fromTaxless;
    private int toTaxless;
    private int fromTaxed;
    private int toTaxed;

    private boolean withImage;

    public ProductSearch() {};

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }
    public void setCategory(String category) {
        this.category = category;
    }

    public String getTxtFromTaxless() {
        return txtFromTaxless;
    }
    public void setTxtFromTaxless(String txtFromTaxless) {
        this.txtFromTaxless = txtFromTaxless;
    }

    public String getTxtToTaxless() {
        return txtToTaxless;
    }
    public void setTxtToTaxless(String txtToTaxless) {
        this.txtToTaxless = txtToTaxless;
    }

    public String getTxtFromTaxed() {
        return txtFromTaxed;
    }
    public void setTxtFromTaxed(String txtFromTaxed) {
        this.txtFromTaxed = txtFromTaxed;
    }

    public String getTxtToTaxed() {
        return txtToTaxed;
    }
    public void setTxtToTaxed(String txtToTaxed) {
        this.txtToTaxed = txtToTaxed;
    }

    public int getFromTaxless() {
        return fromTaxless;
    }
    public void setFromTaxless(int fromTaxless) {
        this.fromTaxless = fromTaxless;
    }

    public int getToTaxless() {
        return toTaxless;
    }
    public void setToTaxless(int toTaxless) {
        this.toTaxless = toTaxless;
    }

    public int getFromTaxed() {
        return fromTaxed;
    }
    public void setFromTaxed(int fromTaxed) {
        this.fromTaxed = fromTaxed;
    }

    public int getToTaxed() {
        return toTaxed;
    }
    public void setToTaxed(int toTaxed) {
        this.toTaxed = toTaxed;
    }

    public boolean isWithImage() {
        return withImage;
    }
    public void setWithImage(boolean withImage) {
        this.withImage = withImage;
    }
}
