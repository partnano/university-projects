package at.ac.tuwien.sepm.assignment.individual.project.exceptions;

public class DuplicateOrderException extends Exception {
    public DuplicateOrderException() {
    }

    public DuplicateOrderException(String message) {
        super(message);
    }
}
