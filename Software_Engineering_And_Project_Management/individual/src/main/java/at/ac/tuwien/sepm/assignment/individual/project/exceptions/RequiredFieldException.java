package at.ac.tuwien.sepm.assignment.individual.project.exceptions;

public class RequiredFieldException extends Exception {
    public RequiredFieldException() {
    }

    public RequiredFieldException(String message) {
        super(message);
    }
}
