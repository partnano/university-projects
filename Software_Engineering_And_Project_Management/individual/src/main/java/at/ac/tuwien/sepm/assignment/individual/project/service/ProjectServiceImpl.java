package at.ac.tuwien.sepm.assignment.individual.project.service;

import at.ac.tuwien.sepm.assignment.individual.project.entities.Order;
import at.ac.tuwien.sepm.assignment.individual.project.entities.Product;
import at.ac.tuwien.sepm.assignment.individual.project.entities.ProductSearch;
import at.ac.tuwien.sepm.assignment.individual.project.exceptions.*;
import at.ac.tuwien.sepm.assignment.individual.project.persistence.OrderDAO;
import at.ac.tuwien.sepm.assignment.individual.project.persistence.ProductDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;

public class ProjectServiceImpl implements ProjectService {

    private static final Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    private ProductDAO productDAO;
    private OrderDAO orderDAO;

    public ProjectServiceImpl (ProductDAO productDAO, OrderDAO orderDAO) {
        this.productDAO = productDAO;
        this.orderDAO = orderDAO;
    }

    @Override
    public void createProduct(Product newProd) throws
        NumberFormatException, NegativePriceException,
        RequiredFieldException, InvalidFileException,
        CreateErrorException
    {
        LOG.trace("Called createProduct in service layer.");

        setProductImage(newProd);
        setProductPrice(newProd);
        validateProduct(newProd);

        LOG.info("Trying to create new product.");
        productDAO.create(newProd);
    }

    @Override
    public void updateProduct(Product updProd) throws
        NumberFormatException, NegativePriceException,
        RequiredFieldException, InvalidFileException,
        UpdateErrorException
    {
        LOG.trace("Called updateProduct in service layer.");

        setProductImage(updProd);
        setProductPrice(updProd);
        validateProduct(updProd);

        LOG.info("Trying to update product");
        productDAO.update(updProd);
    }

    @Override
    public ArrayList<Product> getAllProducts(boolean deleted) throws RetrieveErrorException {
        LOG.trace("Called getAllProducts in service layer.");

        // I should maybe validate that?
        ArrayList<Product> products = productDAO.readAll(deleted);
        products.sort(Comparator.comparingInt(Product::getId));

        return products;
    }

    @Override
    public ArrayList<Product> searchProducts(ProductSearch search) throws
        NumberFormatException, RetrieveErrorException
    {
        LOG.trace("Called searchProducts in service layer.");

        search.setName(search.getName().toLowerCase());

        if (search.getCategory().equals("Alle Kategorien"))
            search.setCategory("");

        if (!search.getTxtFromTaxless().equals(""))
            search.setFromTaxless((int) Math.round((Double.parseDouble(search.getTxtFromTaxless()) * 100)));

        if (!search.getTxtToTaxless().equals(""))
            search.setToTaxless((int) Math.round((Double.parseDouble(search.getTxtToTaxless()) * 100)));

        if (!search.getTxtFromTaxed().equals(""))
            search.setFromTaxed((int) Math.round((Double.parseDouble(search.getTxtFromTaxed()) * 100)));

        if (!search.getTxtToTaxed().equals(""))
            search.setToTaxed((int) Math.round((Double.parseDouble(search.getTxtToTaxed()) * 100)));

        return productDAO.read(search);
    }

    @Override
    public void deleteProducts(ArrayList<Product> productsToDelete) throws DeletionErrorException {
        LOG.trace("Called deleteProducts in service layer.");

        for (Product p : productsToDelete) {
            if (p.getId() < 0) {
                LOG.error("Found product with invalid id while trying to delete!");
                throw new DeletionErrorException();
            }
        }

        if (productsToDelete.size() > 0) {
            LOG.info("Trying to delete given products");
            productDAO.delete(productsToDelete);
        }
    }

    @Override
    public void createOrder(Order newOrder) throws
        CreateErrorException, NumberFormatException,
        NegativePriceException, DuplicateOrderException
    {
        LOG.trace("Called createOrder in service layer.");

        newOrder.setTableNumber(Integer.parseInt(newOrder.getTableNumberTxt()));
        if (newOrder.getTableNumber() < 0) {
            LOG.error("Throwing NegativePriceException!");
            throw new NegativePriceException();
        }

        try {
            ArrayList<Order> existingOrders = orderDAO.readAll();

            for (Order order : existingOrders) {
                if (!order.isFinished() && order.getTableNumber() == newOrder.getTableNumber()) {
                    LOG.error("Throwing DuplicateOrderException, trying to create order for already existing table!");
                    throw new DuplicateOrderException();
                }
            }

            LOG.trace("Trying to create new order..");
            orderDAO.create(newOrder);

        } catch (RetrieveErrorException e) {
            LOG.error("RetrieveErrorException while trying to create!");
            throw new CreateErrorException();
        }
    }

    @Override
    public void updateOrder(Order updOrder) throws UpdateErrorException {
        try {
            ArrayList<Order> existingOrders = orderDAO.readAll();

            for (Order order : existingOrders) {
                if (!order.isFinished() && order.getTableNumber() == updOrder.getTableNumber()) {
                    orderDAO.update(updOrder);
                    return;
                }
            }

            LOG.error("Could not find existing order!");
            throw new UpdateErrorException();
        } catch (RetrieveErrorException e) {
            LOG.error("RetrieveErrorException while trying to update!");
            throw new UpdateErrorException();
        }
    }

    @Override
    public ArrayList<Order> getAllOrders() throws RetrieveErrorException {
        LOG.trace("Called getAllOrders in service layer.");

        // I should probably validate this as well?
        ArrayList<Order> orders = orderDAO.readAll();
        ArrayList<Product> allProducts = productDAO.readAll(true);

        // add products to list ... sorry
        for (Order order : orders) {
            double totalTaxedPrice = 0;
            double totalTenPercent = 0;
            double totalTwentyPercent = 0;

            // unfortunately, this is a rather dumb way, but I couldn't think of a better way of
            // retrieving the map-mentioned product
            for (HashMap.Entry<Integer, Integer> entry : order.getOrderedProducts().entrySet()) {
                for (Product product : allProducts) {
                    if (entry.getKey() == product.getId()) {

                        order.getOrderedProductsList().add(product);

                        if (!order.isFinished())
                            break;

                        int amount = entry.getValue();
                        totalTaxedPrice += product.getTaxedPrice() / 100d * amount;

                        if (product.getTaxing().equals(Product.TAXING[1]))
                            totalTenPercent += product.getPrice() / 100d * 0.1 * amount;
                        else
                            totalTwentyPercent += product.getPrice() / 100d * 0.2 * amount;

                        break;
                    }
                }
            }

            order.setTotalTaxedPrice(totalTaxedPrice);
            order.setTotalTenPercent(totalTenPercent);
            order.setTotalTwentyPercent(totalTwentyPercent);
        }

        orders.sort((o1, o2) -> o2.getCreation().compareTo(o1.getCreation()));

        LOG.info("Added ordered products to retrieved orders and calculated pricing on bills.");
        return orders;
    }

    // HELPER FUNCTIONS

    private void validateProduct(Product prod) throws
        NegativePriceException, RequiredFieldException, InvalidFileException
    {
        LOG.trace("Validating product (service layer)...");

        if (prod.getName().equals("") || prod.getPriceInTxt().equals("") ||
            prod.getCategory().equals("") || prod.getTaxing().equals("")) {
            throw new RequiredFieldException();
        }

        String imgPath = prod.getImagePath();
        File image = prod.getImage();

        if (!imgPath.equals("")) {
            int dotIndex = imgPath.lastIndexOf('.');
            String ext = dotIndex == -1 ? "" : imgPath.substring(dotIndex + 1);

            // type check
            if (!(ext.equals("jpg") || ext.equals("jpeg") || ext.equals("png")))
                throw new InvalidFileException("Falsches Dateiformat! (.jpg, .jpeg, oder .png)");

            // file size check
            if (image.length() / (1024 * 1024) > 5)
                throw new InvalidFileException("Zu große Datei!");

            // image size check
            try {
                BufferedImage bufImage = ImageIO.read(image);

                if (bufImage.getWidth() < 500 || bufImage.getHeight() < 500)
                    throw new InvalidFileException("Bild zu klein! (zumindest 500*500)");

            } catch (IOException e) {
                LOG.error("IOException !" + e.getMessage());
                throw new InvalidFileException("Kein gültiges Bild!");
            }
        }

        if (prod.getPrice() < 0)
            throw new NegativePriceException();

    }

    private void setProductImage(Product prod) throws
        InvalidFileException
    {
        if (!prod.getImagePath().equals("")) {
            File image = new File(prod.getImagePath());

            if (image.exists())
                prod.setImage(image);
            else
                throw new InvalidFileException("Ungültiger Bildpfad! (" + prod.getImagePath() + ")");
        }
    }

    private void setProductPrice(Product prod) throws
        NumberFormatException
    {
        // can throw exception - that's okay
        prod.setPrice((int) Math.round((Double.parseDouble(prod.getPriceInTxt()) * 100)));

        if (prod.getTaxing().equals(Product.TAXING[1]))
            prod.setTaxedPrice((int) Math.round(prod.getPrice() * 1.1));
        else
            prod.setTaxedPrice((int) Math.round(prod.getPrice() * 1.2));
    }

}
