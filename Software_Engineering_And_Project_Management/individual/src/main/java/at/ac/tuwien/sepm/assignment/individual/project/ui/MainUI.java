package at.ac.tuwien.sepm.assignment.individual.project.ui;

import at.ac.tuwien.sepm.assignment.individual.project.entities.Order;
import at.ac.tuwien.sepm.assignment.individual.project.entities.Product;
import at.ac.tuwien.sepm.assignment.individual.project.entities.ProductSearch;
import at.ac.tuwien.sepm.assignment.individual.project.exceptions.*;
import at.ac.tuwien.sepm.assignment.individual.project.service.ProjectService;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.net.URL;
import java.text.DecimalFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

public class MainUI implements ProjectUI, Initializable {

    private static final Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    private Stage stage;
    private ProjectService service;

    private ArrayList<Order> openOrders;
    private ArrayList<Order> closedOrders; // receipts

    @FXML
    Button btnCreateNewProduct;
    @FXML
    Button btnProductDetails;
    @FXML
    Button btnProductDelete;
    @FXML
    Button btnCreateOrder;
    @FXML
    Button btnFinishOrder;

    @FXML
    ListView<String> lstOrders;
    @FXML
    ListView<String> lstReceipts;

    @FXML
    TableView<Product> tblProducts;
    @FXML
    TableColumn<Product, String> tblProductName;
    @FXML
    TableColumn<Product, String> tblProductCategory;
    @FXML
    TableColumn<Product, Double> tblProductPrice;
    @FXML
    TableColumn<Product, ImageView> tblProductImage;

    @FXML
    Label lblProductDetailsName;
    @FXML
    Label lblProductDetailsDescription;
    @FXML
    Label lblProductDetailsPrice;
    @FXML
    Label lblProductDetailsTaxedPrice;
    @FXML
    Label lblProductDetailsCategory;
    @FXML
    Label lblProductDetailsTaxing;
    @FXML
    Label lblProductDetailsTimestamps;
    @FXML
    ImageView imgProductDetails;

    @FXML
    Label lblOrderDetailsTablenumber;
    @FXML
    Label lblOrderDetailsTimestamp;
    @FXML
    ListView<String> lstOrderProducts;
    @FXML
    Button btnOrderAddProduct;
    @FXML
    Button btnOrderRemoveProduct;

    @FXML
    Label lblReceiptNumber;
    @FXML
    Label lblReceiptTable;
    @FXML
    Label lblReceiptCreation;
    @FXML
    Label lblReceiptFinish;
    @FXML
    Label lblReceiptPayment;
    @FXML
    Label lblReceipt10Percent;
    @FXML
    Label lblReceipt20Percent;
    @FXML
    Label lblReceiptSum;
    @FXML
    ListView<String> lstReceiptProducts;

    @FXML
    TextField txtSearchName;
    @FXML
    TextField txtSearchFromTaxless;
    @FXML
    TextField txtSearchToTaxless;
    @FXML
    TextField txtSearchFromTaxed;
    @FXML
    TextField txtSearchToTaxed;
    @FXML
    ComboBox<String> cmbSearchCategory;
    @FXML
    CheckBox chkSearchImage;
    @FXML
    Button btnSearch;
    @FXML
    Button btnShowAll;

    public MainUI(ProjectService service, Stage stage) {
        this.service = service;
        this.stage = stage;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        LOG.trace("Initializing MainUI...");

        propagateProductList(null);
        tblProducts.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        propagateOrderLists();

        tblProducts.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                if(mouseEvent.getButton().equals(MouseButton.PRIMARY)){
                    if(mouseEvent.getClickCount() == 1)
                        propagateProductDetails();
                    if(mouseEvent.getClickCount() == 2)
                        openExistingProductDialogue();
                }
            }
        });

        tblProducts.setPlaceholder(new Label("Keine Produkte gefunden!"));

        cmbSearchCategory.getItems().removeAll(cmbSearchCategory.getItems());
        cmbSearchCategory.getItems().addAll("Alle Kategorien", Product.CATEGORIES[0], Product.CATEGORIES[1], Product.CATEGORIES[2]);
        cmbSearchCategory.getSelectionModel().select("Alle Kategorien");
    }

    @FXML
    private void openNewProductDialogue () {
        openProductDialogue(null);
    }

    @FXML
    private void openExistingProductDialogue() {
        Product productToShow = tblProducts.getSelectionModel().getSelectedItem();

        if (productToShow != null)
            openProductDialogue(productToShow);
    }

    @FXML
    private void deleteProducts() {
        ArrayList<Product> productsToDelete = new ArrayList<>(tblProducts.getSelectionModel().getSelectedItems());

        if (productsToDelete.size() > 0) {

            // user checks if actually correct
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Produkte löschen?");
            alert.setHeaderText("Sollen die gewählten Produkte wirklich gelöscht werden?");

            String dialogueContent = "Gewählte Produkte:\n";
            for (Product p : productsToDelete) {
                dialogueContent += p.getName() + ", ";
            }
            dialogueContent = dialogueContent.substring(0, dialogueContent.length()-2);

            alert.setContentText(dialogueContent);

            Optional<ButtonType> result = alert.showAndWait();
            if (result.isPresent() && result.get() == ButtonType.OK){
                try {
                    service.deleteProducts(productsToDelete);
                } catch (DeletionErrorException e) {
                    LOG.error("DeletionErrorException!\n");
                }
            }

            alert.close();
            propagateProductList(null);
        }
    }

    @FXML
    private void createNewOrder() {
        TextInputDialog dialog = new TextInputDialog("");
        dialog.setTitle("Neue Bestellung");
        dialog.setHeaderText("Tischnummer:");
        dialog.setContentText("");

        Optional<String> result = dialog.showAndWait();
        if(!result.isPresent()) {
            LOG.error("Couldn't retrieve tablenumber from dialogue");
            return;
        }

        Order newOrder = new Order(result.get());

        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Fehler!");
        alert.setHeaderText("Fehler!");

        try {
            service.createOrder(newOrder);
        } catch (CreateErrorException e) {
            LOG.error("CreateErrorException!");

            alert.setContentText("Fehler beim erstellen!");
            alert.showAndWait();
        } catch (NegativePriceException | NumberFormatException e) {
            LOG.error("NegativePriceException // NumberFormatException - invalid number input.");

            alert.setContentText("Ungültige Tischnummer!");
            alert.showAndWait();
        } catch (DuplicateOrderException e) {
            LOG.error("DuplicateOrderException! Open order already existing.");

            alert.setContentText("Tischnummer schon belegt!");
            alert.showAndWait();
        }

        propagateOrderLists();
    }

    @FXML
    private void propagateProductDetails() {
        Product detailProduct = tblProducts.getSelectionModel().getSelectedItem();

        if (detailProduct == null)
            return;

        lblProductDetailsName.setText(detailProduct.getName());
        lblProductDetailsDescription.setText(detailProduct.getDescription());

        lblProductDetailsPrice.setText(detailProduct.getFormattedPrice() + ",-");
        lblProductDetailsTaxedPrice.setText(detailProduct.getFormattedTaxedPrice() + ",-");

        lblProductDetailsCategory.setText(detailProduct.getCategory());
        lblProductDetailsTaxing.setText(detailProduct.getTaxing());

        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm");
        String creation = "Erstellt am: " + detailProduct.getCreatedOn().format(format);

        if (detailProduct.getLastEdit() != null)
            creation += "\nZuletzt bearbeitet: " + detailProduct.getLastEdit().format(format);

        lblProductDetailsTimestamps.setText(creation);

        if (detailProduct.getImage() == null) {
            imgProductDetails.setImage(null);
            return;
        }

        Image img = new Image(detailProduct.getImage().toURI().toString());
        imgProductDetails.setImage(img);
    }

    @FXML
    private void propagateOrderDetails() {
        LOG.trace("Called propagateOrderDetails!");

        int index = lstOrders.getSelectionModel().getSelectedIndex();

        if (index < 0) {
            lblOrderDetailsTablenumber.setText("");
            lblOrderDetailsTimestamp.setText("");
            lstOrderProducts.setItems(null);
            return;
        }

        Order detailOrder = openOrders.get(index);
        lblOrderDetailsTablenumber.setText(detailOrder.getTableNumber() + "");

        DateTimeFormatter format = DateTimeFormatter.ofPattern("HH:mm, dd.MM.yyyy");
        lblOrderDetailsTimestamp.setText(detailOrder.getCreation().format(format));

        ObservableList<String> productsObs = FXCollections.observableArrayList();

        for (Product product : detailOrder.getOrderedProductsList()) {
            productsObs.add(
                product.getName() + " (" + product.getFormattedPrice() + ",- x "
                + detailOrder.getOrderedProducts().get(product.getId()) + ")"
            );
        }

        lstOrderProducts.setItems(productsObs);
    }

    @FXML
    private void addProductToOrder() {
        int index = lstOrders.getSelectionModel().getSelectedIndex();
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Fehler!");
        alert.setHeaderText("Fehler!");

        if (index < 0) {
            alert.setContentText("Keine Bestellung ausgewählt");
            alert.showAndWait();
            return;
        }

        Order detailOrder = openOrders.get(index);
        openSelectProductDialogue(detailOrder);
    }

    @FXML
    private void removeProductFromOrder() {
        int index = lstOrders.getSelectionModel().getSelectedIndex();
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Fehler!");
        alert.setHeaderText("Fehler!");

        if (index < 0) {
            alert.setContentText("Keine Bestellung ausgewählt");
            alert.showAndWait();
            return;
        }

        Order detailOrder = openOrders.get(index);

        index = lstOrderProducts.getSelectionModel().getSelectedIndex();
        if (index < 0) {
            alert.setContentText("Kein Produkt ausgewählt");
            alert.showAndWait();
            return;
        }

        Product toRemove = detailOrder.getOrderedProductsList().get(index);

        detailOrder.removeFromOrder(toRemove);
        try {
            service.updateOrder(detailOrder);
        } catch (UpdateErrorException e) {
            LOG.error("Couldn't update order!");
        }

        propagateOrderDetails();
        lstOrderProducts.getSelectionModel().select(index);
    }

    @FXML
    public void finishOrder() {
        int index = lstOrders.getSelectionModel().getSelectedIndex();
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Fehler!");
        alert.setHeaderText("Fehler!");

        if (index < 0) {
            alert.setContentText("Keine Bestellung ausgewählt");
            alert.showAndWait();
            return;
        }

        Order detailOrder = openOrders.get(index);

        List<String> choices = new ArrayList<>();
        choices.add("Bar");
        choices.add("Karte");

        ChoiceDialog<String> dialog = new ChoiceDialog<>("Bar", choices);
        dialog.setTitle("Payment Options");
        dialog.setHeaderText("Bitte Zahlungsart auswählen.");
        dialog.setContentText("Zahlunsgart:");

        Optional<String> result1 = dialog.showAndWait();
        result1.ifPresent(detailOrder::setPayment);

        alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirmation");
        alert.setHeaderText("Bestätigen");
        alert.setContentText("Wirklich abrechnen?");

        Optional<ButtonType> result2 = alert.showAndWait();
        if (result2.get() == ButtonType.OK){
            detailOrder.setFinished(true);
            try {
                service.updateOrder(detailOrder);
            } catch (UpdateErrorException e) {
                LOG.error("UpdateErrorException while trying to finish order! (Probably persistence)");
            }

        }

        propagateOrderLists();
        propagateOrderDetails();
    }

    @FXML
    public void propagateReceiptDetails() {
        int index = lstReceipts.getSelectionModel().getSelectedIndex();

        if (index < 0)
            return;

        Order detailOrder = closedOrders.get(index);

        lblReceiptNumber.setText(detailOrder.getReceiptNumber() + "");
        lblReceiptTable.setText(detailOrder.getTableNumber() + "");
        lblReceiptPayment.setText(detailOrder.getPayment());

        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm");
        lblReceiptCreation.setText(detailOrder.getCreation().format(format));
        lblReceiptFinish.setText(detailOrder.getFinishTime().format(format));

        DecimalFormat formatter = new DecimalFormat("####0.00");

        ObservableList<String> productObs = FXCollections.observableArrayList();

        for (Product product : detailOrder.getOrderedProductsList()) {
            double taxedPrice = ((double) product.getTaxedPrice()) / 100;

            int amount = detailOrder.getOrderedProducts().get(product.getId());
            String output = product.getName() + " x" + amount + ", ";

            String formattedPrice = formatter.format(taxedPrice);
            String formattedPriceSum = formatter.format(taxedPrice*amount);

            output += "Einzelpreis: " + formattedPrice + ",-, Gesamtpreis: " + formattedPriceSum + ",-";
            productObs.add(output);
        }

        String formattedTen = formatter.format(detailOrder.getTotalTenPercent());
        String formattedTwenty = formatter.format(detailOrder.getTotalTwentyPercent());
        String formattedSum = formatter.format(detailOrder.getTotalTaxedPrice());

        lstReceiptProducts.setItems(productObs);
        lblReceipt10Percent.setText(formattedTen + ",-");
        lblReceipt20Percent.setText(formattedTwenty + ",-");
        lblReceiptSum.setText(formattedSum + ",-");
    }

    @FXML
    public void searchProducts() {
        ProductSearch search = new ProductSearch();

        search.setName           (txtSearchName.getText());
        search.setCategory       (cmbSearchCategory.getSelectionModel().getSelectedItem());
        search.setTxtFromTaxed   (txtSearchFromTaxed.getText());
        search.setTxtToTaxed     (txtSearchToTaxed.getText());
        search.setTxtFromTaxless (txtSearchFromTaxless.getText());
        search.setTxtToTaxless   (txtSearchToTaxless.getText());
        search.setWithImage      (chkSearchImage.isSelected());

        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Fehler!");
        alert.setHeaderText("Fehler!");

        try {
            ArrayList<Product> products = service.searchProducts(search);

            propagateProductList(products);
        } catch (RetrieveErrorException e) {
            LOG.error("RetrieveErrorException! (Probably from persistence)");
            alert.setContentText("Es ist ein Fehler bei der Suche aufgetreten.");
            alert.showAndWait();

        } catch (NumberFormatException e) {
            LOG.error("NumberFormatException! " + e.getMessage());
            alert.setContentText("Ungültiges Preisformat!");
            alert.showAndWait();
        }
    }

    @FXML
    public void showAllProducts() {
        propagateProductList(null);
    }

    // HELPER FUNCTIONS
    private void propagateOrderLists() {
        try {
            ArrayList<Order> orders = service.getAllOrders();

            openOrders = new ArrayList<>();
            closedOrders = new ArrayList<>();

            ObservableList<String> openOrdersObs = FXCollections.observableArrayList();
            ObservableList<String> closeOrdersObs = FXCollections.observableArrayList();

            DateTimeFormatter formatBasic = DateTimeFormatter.ofPattern("dd.MM. HH:mm");
            DateTimeFormatter formatExtended = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm");

            String entry;

            for (Order order : orders) {

                if (order.isFinished()) {
                    closedOrders.add(order);
                } else {
                    entry = "Tisch " + order.getTableNumber() + " (" + order.getCreation().format(formatBasic) + " Uhr)";
                    openOrders.add(order);
                    openOrdersObs.add(entry);
                }

            }

            for (Order order : closedOrders) {
                entry = "Tisch " + order.getTableNumber() + " (" + order.getFinishTime().format(formatExtended) + ")";
                closeOrdersObs.add(entry);
            }

            lstOrders.setItems(openOrdersObs);
            lstReceipts.setItems(closeOrdersObs);
        } catch (RetrieveErrorException e) {
            LOG.error("RetrieveErrorException! (Probably from persistence)");
        }

    }

    private void propagateProductList(ArrayList<Product> products) {
        try {
            if (products == null)
                products = service.getAllProducts(false);

            ObservableList<Product> productObs = FXCollections.observableArrayList(products);

            tblProductName.setCellValueFactory(new PropertyValueFactory<>("name"));
            tblProductCategory.setCellValueFactory(new PropertyValueFactory<>("category"));
            tblProductPrice.setCellValueFactory(new PropertyValueFactory<>("formattedTaxedPrice"));
            tblProductImage.setCellValueFactory(new PropertyValueFactory<>("imageView"));

            tblProducts.setItems(productObs);

        } catch (RetrieveErrorException e) {
            LOG.error("RetrieveErrorException! (Probably from persistence)");
        }
    }

    private void openProductDialogue (Product prod) {
        Stage detailsStage = new Stage();
        detailsStage.setTitle("Product Details");
        detailsStage.centerOnScreen();
        detailsStage.setOnCloseRequest(event -> LOG.debug("Closed Detail stage"));

        ProjectUI productDetailsUI;

        if (prod == null)
            productDetailsUI = new ProductDetailsUI(service, detailsStage);
        else
            productDetailsUI = new ProductDetailsUI(service, detailsStage, prod);

        // prepare fxml loader and inject controller
        FXMLLoader detailsLoader = new FXMLLoader(getClass().getResource("/fxml/productDetails.fxml"));
        detailsLoader.setControllerFactory(param -> param.isInstance(productDetailsUI) ? productDetailsUI : null);

        try {
            detailsStage.setScene(new Scene(detailsLoader.load()));
        } catch (IOException e) {
            LOG.error("IOException!\n" + e.getMessage());
        }

        detailsStage.show();
        detailsStage.toFront();

        detailsStage.setOnHiding(event -> propagateProductList(null));
    }

    private void openSelectProductDialogue (Order order) {
        Stage selectStage = new Stage();
        selectStage.setTitle("Select Product");
        selectStage.centerOnScreen();
        selectStage.setOnCloseRequest(event -> LOG.debug("Closed Selection Stage"));

        ProjectUI selectProductUI = new SelectProductUI(service, selectStage, order);

        FXMLLoader selectionLoader = new FXMLLoader(getClass().getResource("/fxml/selectProduct.fxml"));
        selectionLoader.setControllerFactory(param -> param.isInstance(selectProductUI) ? selectProductUI : null);

        try {
            selectStage.setScene(new Scene(selectionLoader.load()));
        } catch (IOException e) {
            LOG.error("IOException!\n" + e.getMessage());
        }

        selectStage.show();
        selectStage.toFront();

        selectStage.setOnHiding(event -> propagateOrderDetails());
    }
}
