package at.ac.tuwien.sepm.assignment.individual.application;

import at.ac.tuwien.sepm.assignment.individual.project.persistence.*;
import at.ac.tuwien.sepm.assignment.individual.project.service.ProjectService;
import at.ac.tuwien.sepm.assignment.individual.project.service.ProjectServiceImpl;
import at.ac.tuwien.sepm.assignment.individual.project.ui.MainUI;
import at.ac.tuwien.sepm.assignment.individual.project.ui.ProjectUI;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.invoke.MethodHandles;
import java.sql.Connection;
import java.sql.SQLException;

public final class MainApplication extends Application {

    private static final Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Override
    public void start(Stage primaryStage) throws Exception {
        // setup application
        primaryStage.setTitle("Store Management");
        primaryStage.centerOnScreen();
        primaryStage.setOnCloseRequest(event -> LOG.debug("Application shutdown initiated"));

        // initiate controller
        ProductDAO productDAO =  new ProductDAOImpl();
        OrderDAO orderDAO =      new OrderDAOImpl();
        ProjectService service = new ProjectServiceImpl(productDAO, orderDAO);
        ProjectUI mainUI =       new MainUI(service, primaryStage);

        // prepare fxml loader and inject controller
        FXMLLoader mainLoader = new FXMLLoader(getClass().getResource("/fxml/main.fxml"));
        mainLoader.setControllerFactory(param -> param.isInstance(mainUI) ? mainUI : null);

        primaryStage.setScene(new Scene(mainLoader.load()));

        // show application
        primaryStage.show();
        primaryStage.toFront();
        LOG.debug("Application startup complete");
    }

    @Override
    public void stop() {
        LOG.trace ("closing application");

        Connection conn = H2Connection.getInstance().getConn();

        try {
            conn.close();
        } catch (SQLException e) {
            LOG.error("SQLException, couldn't close connection. \n" + e.getMessage());
        }
    }

    public static void main(String[] args) {
        LOG.debug("Application starting with arguments={}", (Object) args);
        Application.launch(MainApplication.class, args);
    }

}
