package at.ac.tuwien.sepm.assignment.individual.project.persistence;

import at.ac.tuwien.sepm.assignment.individual.project.entities.Product;
import at.ac.tuwien.sepm.assignment.individual.project.entities.ProductSearch;
import at.ac.tuwien.sepm.assignment.individual.project.exceptions.CreateErrorException;
import at.ac.tuwien.sepm.assignment.individual.project.exceptions.DeletionErrorException;
import at.ac.tuwien.sepm.assignment.individual.project.exceptions.RetrieveErrorException;
import at.ac.tuwien.sepm.assignment.individual.project.exceptions.UpdateErrorException;

import java.util.ArrayList;

/**
 * This is the data access object for products of the software,
 * it handles all of the data saving and retrieving for products,
 * orders and billing (should) have a separate DAO
 */
public interface ProductDAO {
    /**
     * Creates a new product from given input product.
     *
     * @param newProd New Product to be saved wherever.
     * @throws CreateErrorException On Error in saving medium.
     */
    void create (Product newProd) throws CreateErrorException;

    /**
     * Updates a product.
     *
     * @param updProd Existing product (with id) to be updated.
     * @throws UpdateErrorException On Error in saving medium.
     */
    void update (Product updProd) throws UpdateErrorException;

    /**
     * Retrieves all products in saving medium.
     *
     * @param deleted If deleted products should be returned as well
     * @return Returns an ArrayList of all products in saving medium.
     * @throws RetrieveErrorException On Error in saving medium.
     */
    ArrayList<Product> readAll (boolean deleted) throws RetrieveErrorException;

    /**
     * Retrieves products that are searched for.
     *
     * @param search Search DTO with search properties to be used.
     * @return ArrayList of products. (Can be empty)
     * @throws RetrieveErrorException On Error in saving medium.
     */
    ArrayList<Product> read (ProductSearch search) throws RetrieveErrorException;

    /**
     * Deletes a product.
     *
     * @param delProds ArrayList of products to be deleted.
     * @throws DeletionErrorException On Error in saving medium.
     */
    void delete (ArrayList<Product> delProds) throws DeletionErrorException;
}
