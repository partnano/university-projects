package at.ac.tuwien.sepm.assignment.individual.project.exceptions;

public class CreateErrorException extends Exception {
    public CreateErrorException() {
    }

    public CreateErrorException(String message) {
        super(message);
    }
}
