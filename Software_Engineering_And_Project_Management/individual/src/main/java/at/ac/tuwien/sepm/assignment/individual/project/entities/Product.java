package at.ac.tuwien.sepm.assignment.individual.project.entities;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.io.File;
import java.text.DecimalFormat;
import java.time.LocalDateTime;

public class Product {

    public static final String[] CATEGORIES = {"Vorspeiße", "Hauptgericht", "Dessert"};
    public static final String[] TAXING = {"20% Normal", "10% Ermäßigt"};

    private int id = -1;

    private String name;
    private String description;

    private String priceInTxt;
    private int price; // price in cents
    private int taxedPrice;

    private String formattedPrice;
    private String formattedTaxedPrice;

    private String imagePath;
    private String oldImagePath;
    private File image;
    private ImageView imageView;

    private String category;
    private String taxing;

    private LocalDateTime createdOn;
    private LocalDateTime lastEdit;

    public Product() {}
    public Product(String name, String description, String imagePath, String priceInTxt, String category, String taxing) {
        this.name = name;
        this.description = description;
        this.imagePath = imagePath;
        this.priceInTxt = priceInTxt;
        this.category = category;
        this.taxing = taxing;

        this.imageView = new ImageView();
    }

    public ImageView getImageView() {
        imageView = new ImageView();

        if (!imagePath.equals("")) {
            if (image == null)
                image = new File(imagePath);

            Image img = new Image(image.toURI().toString());
            imageView.setImage(img);
        }

        imageView.setFitHeight(50);
        imageView.setPreserveRatio(true);
        return imageView;
    }

    public String getFormattedPrice() {
        if (formattedPrice == null) {
            DecimalFormat myFormatter = new DecimalFormat("###0.00");
            formattedPrice = myFormatter.format(((double) price) / 100);
        }

        return formattedPrice;
    }

    public String getFormattedTaxedPrice() {
        if (formattedTaxedPrice == null) {
            DecimalFormat myFormatter = new DecimalFormat("###0.00");
            formattedTaxedPrice = myFormatter.format(((double) taxedPrice) / 100);
        }

        return formattedTaxedPrice;
    }

    public int getId() { return id; }

    public String getName() { return name; }
    public String getDescription() { return description; }

    public String getPriceInTxt() { return priceInTxt; }
    public int getPrice() { return price; }
    public int getTaxedPrice() {
        return taxedPrice;
    }

    public String getImagePath() { return imagePath; }
    public String getOldImagePath() { return oldImagePath; }
    public File getImage() { return image; }

    public String getCategory() { return category; }
    public String getTaxing() { return taxing; }

    public LocalDateTime getCreatedOn() { return createdOn; }
    public LocalDateTime getLastEdit() { return lastEdit; }

    public void setId(int id) { this.id = id; }
    public void setName(String n) { name = n; }

    public void setDescription(String d) { description = d; }
    public void setPriceInTxt(String p) { priceInTxt = p; }
    public void setPrice(int p) { price = p; }
    public void setTaxedPrice(int taxedPrice) {
        this.taxedPrice = taxedPrice;
    }
    public void setFormattedPrice(String formattedPrice) {
        this.formattedPrice = formattedPrice;
    }
    public void setFormattedTaxedPrice(String formattedTaxedPrice) { this.formattedTaxedPrice = formattedTaxedPrice; }

    public void setImagePath(String i) { imagePath = i; }
    public void setOldImagePath(String oldImagePath) { this.oldImagePath = oldImagePath; }
    public void setImage(File image) { this.image = image; }

    public void setCategory(String category) { this.category = category; }
    public void setTaxing(String taxing) { this.taxing = taxing; }

    public void setCreatedOn(LocalDateTime createdOn) { this.createdOn = createdOn; }
    public void setLastEdit(LocalDateTime lastEdit) { this.lastEdit = lastEdit; }

    public void setImageView(ImageView v) {
        imageView = v;
    }
}