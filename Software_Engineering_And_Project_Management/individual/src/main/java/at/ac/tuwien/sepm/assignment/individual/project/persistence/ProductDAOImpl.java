package at.ac.tuwien.sepm.assignment.individual.project.persistence;

import at.ac.tuwien.sepm.assignment.individual.project.entities.Product;
import at.ac.tuwien.sepm.assignment.individual.project.entities.ProductSearch;
import at.ac.tuwien.sepm.assignment.individual.project.exceptions.CreateErrorException;
import at.ac.tuwien.sepm.assignment.individual.project.exceptions.DeletionErrorException;
import at.ac.tuwien.sepm.assignment.individual.project.exceptions.RetrieveErrorException;
import at.ac.tuwien.sepm.assignment.individual.project.exceptions.UpdateErrorException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.nio.file.Files;
import java.sql.*;
import java.util.ArrayList;

public class ProductDAOImpl implements ProductDAO {

    private static final Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    private Connection h2Conn;

    public ProductDAOImpl () {
        this.h2Conn = H2Connection.getInstance().getConn();
    }

    @Override
    public void create(Product newProd) throws CreateErrorException {
        LOG.trace("called Product DAO create");

        // because mysql mode....
        if (newProd.getName() == null || newProd.getImagePath() == null ||
            newProd.getCategory() == null || newProd.getTaxing() == null)
            throw new CreateErrorException();

        // first trying to create file
        saveFile(newProd);

        // then write to db
        PreparedStatement stmt = null;

        String sql = "insert into products " +
            "(name, description, price, taxed_price, category, taxing, imagepath, creation) " +
            "values (?, ?, ?, ?, ?, ?, ?, ?)";

        try {
            stmt = h2Conn.prepareStatement(sql);

            stmt.setString(1, newProd.getName());
            stmt.setString(2, newProd.getDescription());
            stmt.setInt(3, newProd.getPrice());
            stmt.setInt(4, newProd.getTaxedPrice());
            stmt.setString(5, newProd.getCategory());
            stmt.setString(6, newProd.getTaxing());
            stmt.setString(7, newProd.getImagePath());
            stmt.setTimestamp(8, getCurrentTimeStamp());

            // execute insert SQL statement
            stmt.executeUpdate();

            LOG.info("Created new product and saved in h2 database. (" + newProd.getName() + ")");

        } catch (SQLException e) {
            LOG.error("SQLException ! \n" + e.getMessage());
            throw new CreateErrorException();

        } finally {
            H2Connection.closeStatement(stmt);
        }
    }

    @Override
    public void update(Product updProd) throws UpdateErrorException {
        LOG.trace("called Product DAO update");

        // because mysql mode....
        if (updProd.getName() == null || updProd.getImagePath() == null ||
            updProd.getCategory() == null || updProd.getTaxing() == null) {
            LOG.error("Invalid DTO to update! Some fields are null.");
            throw new UpdateErrorException();
        }

        // save the new image
        if (!updProd.getOldImagePath().equals(updProd.getImagePath())) {
            try {
                saveFile(updProd);
            } catch (CreateErrorException e) {
                LOG.error("Can not save new file!");
                throw new UpdateErrorException();
            }
        }

        // then update old version (now marked as deleted)
        PreparedStatement stmt = null;

        String sqlUpdate = "update products set deleted = true where product_id = ?; ";
        String sqlCreate = "insert into products " +
            "(name, description, price, taxed_price, category, taxing, imagepath, creation, last_edit) " +
            "values (?, ?, ?, ?, ?, ?, ?, ?, ?)";

        try {

            // delete old one
            stmt = h2Conn.prepareStatement(sqlUpdate);
            stmt.setInt(1, updProd.getId());
            stmt.executeUpdate();

            stmt = h2Conn.prepareStatement(sqlCreate);

            // replace with new one
            stmt.setString(1, updProd.getName());
            stmt.setString(2, updProd.getDescription());
            stmt.setInt(3, updProd.getPrice());
            stmt.setInt(4, updProd.getTaxedPrice());
            stmt.setString(5, updProd.getCategory());
            stmt.setString(6, updProd.getTaxing());
            stmt.setString(7, updProd.getImagePath());
            stmt.setTimestamp(8, Timestamp.valueOf(updProd.getCreatedOn()));
            stmt.setTimestamp(9, getCurrentTimeStamp());

            stmt.executeUpdate();

            LOG.info("Updated product in h2 database. (" + updProd.getName() + ")");

        } catch (SQLException e) {
            LOG.error ("SQLException! " + e.getMessage());
            throw new UpdateErrorException();
        } finally {
            H2Connection.closeStatement(stmt);
        }
    }

    @Override
    public ArrayList<Product> readAll(boolean deleted) throws RetrieveErrorException {

        PreparedStatement stmt = null;
        String sql;

        if (!deleted)
            sql = "select * from products where deleted = false;";
        else
            sql = "select * from products;";

        try {
            stmt = h2Conn.prepareStatement(sql);

            ResultSet rs = stmt.executeQuery();

            LOG.info("Retrieved all products in database.");

            return fillProductList(rs);

        } catch (SQLException e) {
            LOG.error("SQLException! " + e.getMessage());
            throw new RetrieveErrorException();
        } finally {
            H2Connection.closeStatement(stmt);
        }
    }

    @Override
    public ArrayList<Product> read(ProductSearch search) throws RetrieveErrorException {

        PreparedStatement stmt = null;
        String searchSql = "select * from products where deleted = false ";

        if (!search.getName().equals(""))
            searchSql += "and lcase(name) like ? ";

        if (!search.getCategory().equals(""))
            searchSql += "and category = ? ";

        if (!search.getTxtFromTaxless().equals(""))
            searchSql += "and price >= ? ";

        if (!search.getTxtToTaxless().equals(""))
            searchSql += "and price <= ? ";

        if (!search.getTxtFromTaxed().equals(""))
            searchSql += "and taxed_price >= ? ";

        if (!search.getTxtToTaxed().equals(""))
            searchSql += "and taxed_price <= ? ";

        if (search.isWithImage())
            searchSql += "and imagepath <> '';";
        else
            searchSql += "and imagepath = '';";

        try {
            stmt = h2Conn.prepareStatement(searchSql);

            int i = 1;

            if (!search.getName().equals(""))
                stmt.setString(i++, "%" + search.getName() + "%");

            if (!search.getCategory().equals(""))
                stmt.setString(i++, search.getCategory());

            if (!search.getTxtFromTaxless().equals(""))
                stmt.setInt(i++, search.getFromTaxless());

            if (!search.getTxtToTaxless().equals(""))
                stmt.setInt(i++, search.getToTaxless());

            if (!search.getTxtFromTaxed().equals(""))
                stmt.setInt(i++, search.getFromTaxed());

            if (!search.getTxtToTaxed().equals(""))
                stmt.setInt(i, search.getToTaxed());

            ResultSet rs = stmt.executeQuery();

            LOG.info("Retrieved searched-for products.");
            return fillProductList(rs);

        } catch (SQLException e) {
            LOG.error("SQLException! " + e.getMessage());
            throw new RetrieveErrorException();
        } finally {
            H2Connection.closeStatement(stmt);
        }
    }

    @Override
    public void delete(ArrayList<Product> delProds) throws DeletionErrorException {

        PreparedStatement stmt = null;
        String sql = "update products set deleted = true where product_id = ?; ";

        try {
            for (Product product : delProds) {
                stmt = h2Conn.prepareStatement(sql);
                stmt.setInt(1, product.getId());
                stmt.executeUpdate();

                LOG.info("'Deleted' product " + product.getId() + " from database.");
            }
        } catch (SQLException e) {
            LOG.error("SQLException! " + e.getMessage());
            throw new DeletionErrorException();

        } finally {
            H2Connection.closeStatement(stmt);
        }

    }

    // HELPER FUNCTIONS
    private void saveFile (Product product) throws CreateErrorException {
        if (product.getImage() != null && product.getImage().exists()) {
            File dir = new File ("./src/main/resources/images");

            try {
                File tmp = File.createTempFile("img_", ".png", dir);
                String path = tmp.getAbsolutePath();

                if (!tmp.delete())
                    LOG.warn("Something went wrong with deleting temp file.");

                File dest = new File(path);
                Files.copy(product.getImage().toPath(), dest.toPath());

                product.setImagePath(dest.getAbsolutePath());

            } catch (IOException e) {
                LOG.error ("IOException! (Could not create tempfile in resources) " + e.getMessage());
                throw new CreateErrorException();
            }
        }
    }

    private ArrayList<Product> fillProductList (ResultSet rs) throws SQLException {
        ArrayList<Product> products = new ArrayList<>();

        while (rs.next()) {
            Product product = new Product();

            product.setId          (rs.getInt("PRODUCT_ID"));
            product.setPrice       (rs.getInt("PRICE"));
            product.setTaxedPrice  (rs.getInt("TAXED_PRICE"));
            product.setName        (rs.getString("NAME"));
            product.setDescription (rs.getString("DESCRIPTION"));
            product.setCategory    (rs.getString("CATEGORY"));
            product.setTaxing      (rs.getString("TAXING"));
            product.setImagePath   (rs.getString("IMAGEPATH"));
            product.setCreatedOn   (rs.getTimestamp("CREATION").toLocalDateTime());

            Timestamp ts = rs.getTimestamp("LAST_EDIT");
            if (ts != null)
                product.setLastEdit(ts.toLocalDateTime());

            products.add(product);
        }

        return products;
    }

    private Timestamp getCurrentTimeStamp() {
        java.util.Date today = new java.util.Date();
        return new Timestamp(today.getTime());
    }
}
