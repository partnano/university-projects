package at.ac.tuwien.sepm.assignment.individual.project.exceptions;

public class InvalidFileException extends Exception {
    public InvalidFileException() {
    }

    public InvalidFileException(String message) {
        super(message);
    }
}
