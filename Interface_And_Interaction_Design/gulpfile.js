const gulp = require('gulp');
const htmlPartial = require('gulp-html-partial');
const connect = require('gulp-connect');
 
gulp.task('html', function () {
    gulp.src(['src/**/*', '!src/*.html'])
        .pipe(gulp.dest('build'));
    gulp.src(['src/**/*.html'])
        .pipe(htmlPartial({
            basePath: 'src/partials/'
        }))
        .pipe(gulp.dest('build'))
        .pipe(connect.reload());
});

gulp.task('connect', function() {
    connect.server({
        livereload: true,
        root: 'build'
    })
});

gulp.task('watch', function() {
    gulp.watch('src/**/*', ['html']);
})

gulp.task('default', ['html', 'connect', 'watch']);