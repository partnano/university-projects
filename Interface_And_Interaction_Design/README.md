Developed and tested only in Google Chrome.

Estimated time worked on it:

Jirout Thomas: 24h
Schwarz Verena: 18h
Wick Bernhard: 18h
Weber Marco: 9h

DEVELOPER NOTES:

first, run:

npm install

then, start:

gulp

and open

http://localhost:8080

in your browser.

For livereload, install chrome extension:

https://chrome.google.com/webstore/detail/livereload/jnihajbhpnppcggbcgedagnkighmdlei
