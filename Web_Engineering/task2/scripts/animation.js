/*
  TODO
 Implementieren Sie die folgenden Funktionen um die SVG-Grafiken der Geräte anzuzeigen und verändern.

 Hinweise zur Implementierung:
 - Verwenden Sie das SVG-Plugin für jQuery, welches bereits für Sie eingebunden ist (Referenz: http://keith-wood.name/svg.html)
 - Sie dürfen das Bild bei jedem Funktionenaufruf neu laden und Ihre Veränderungen vornehmen;
 Tipp: Durch Überschreiben der OnLoad Funktion von svg.load() können Sie die Grafik noch bevor diese zum ersten Mal angezeigt wird verändern
 - In allen bereit gestellten SVG-Grafiken sind alle für Sie relevanten Stellen mit Labels markiert.
 - Da Ihre Grafiken nur beim Laden der Übersichtsseite neu gezeichnet werden müssen, bietet es ich an die draw_image Funktionen nach dem vollständigen Laden dieser Seite auszuführen.
 Rufen Sie dazu mit draw_image(id, src, min, max, current, values) die zugrunde liegende und hier definierte Funktione auf.
 */


function drawThermometer(id, src, min, max, current, values) {
  /* TODO
   Passen Sie die Höhe des Temperaturstandes entsprechend dem aktuellen Wert an.
   Beachten Sie weiters, dass auch die Beschriftung des Thermometers (max, min Temperatur) angepasst werden soll.
   */
  load(id, src, function(svg){
      $(svg.root()).addClass('device-image');

      $('tspan', svg.root()).eq(0).html(min);
      $('tspan', svg.root()).eq(1).html(max);
      $('path', svg.root()).eq(0).attr('d', 'M 323.577 408.949 V ' + (322.5 - ((current-min)*(322.5-42.5)/(max-min) + 42.5) + 42.5) + ' H 254.64 L 253.93289 406.35969 C 229.95989 418.59369 214.201 444.10305 214.201 472.81205 214.201 513.56405 247.356 546.71805 288.109 546.71805 328.862 546.71805 362.017 513.56405 362.017 472.81205 362.017 444.10405 345.552 421.18405 321.577 408.94905 Z M 318.681 482.906 C 308.363 482.906 299.995 474.539 299.995 464.22 299.995 453.901 308.352 445.534 318.681 445.534 328.999 445.534 337.365 453.901 337.365 464.22 337.365 474.539 328.999 482.906 318.681 482.906 Z');
  });
}


function drawBulb(id, src, min, max, current, values) {
    // TODO
    load(id, src, function(svg){
        $(svg.root()).addClass('device-image');

        var color;
        if(!current) {
            color = 'black';
        } else {
            color = '#FFA500';
        }
        $('path', svg.root()).attr('fill', color);
    });
}

function drawCam(id, src, min, max, current, values) {
    /* TODO
    Verändern Sie die Darstellung der Webcam entsprechend den Vorgaben aus der Angabe.
    Dabei soll jedoch nicht nur einfach die Farbe der Elemente verändert werden, sondern es soll eine Kopie der zu verändernden Elemente erstellt
     und anschließend die aktuellen durch die angepassten Kopien ersetzt werden.
     */
    load(id, src, function(svg){
        $(svg.root()).addClass('device-image');
        if(!current) {
            var lens = $('circle', svg.root()).eq(1);
            var newLens = svg.clone(lens)[0];
            lens.remove();
            svg.change(newLens, {style: "fill:black"});
            var ref = $('path', svg.root()).eq(2);
            var newRef = svg.clone(ref)[0];
            ref.remove();
            svg.change(newRef, {style: "fill:white"});
        }
    });
}

function drawShutter(id, src, min, max, current, values) {
    // TODO
    load(id, src, function(svg){
        $(svg.root()).addClass('device-image');

        if(current === 0){
            $('path', svg.root()).slice(2).remove();
        } else if (current === 1){
            $('path', svg.root()).slice(3).remove();
        }
    });
}

function load(id, src, callback) {
    $(id).svg({
        loadURL: src,
        settings: {stroke: 'yellow'},
        onLoad: callback
    });
    return $(id).svg('get');
}