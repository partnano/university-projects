import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Title } from "@angular/platform-browser";

import { ControlType } from '../model/controlType';
import { DeviceService } from '../services/device.service';
import { Device } from '../model/device';

@Component({
    selector: 'details-page',
    templateUrl: '/app/views/details.component.html',
    providers: [ DeviceService ]
})
export class DetailsComponent implements OnInit {
    id: string;
    device: Device;
    ct: any = ControlType;

    constructor(
        private deviceService: DeviceService,
        private route: ActivatedRoute,
        private titleService: Title
    ) {}

    getDevice(): void {
	    this.deviceService.getDevice (this.id).then (d => this.device = d);
    }
    
    ngOnInit(): void {
        // get id from route
        this.route.params.subscribe (p => this.id = p['id']);

        // get current device
        this.getDevice();

        //set page title
        this.titleService.setTitle( 'BIG Smart Home - Optionen' );
    }
}
