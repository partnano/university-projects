import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Title } from "@angular/platform-browser";

@Component({
    selector: 'login-page',
    templateUrl: '/app/views/login.component.html'
})
export class LoginComponent implements OnInit{
    constructor(
        private router: Router,
        private titleService: Title
    ) {}

    username = '';
    password = '';

    login(): void {
        if (this.username.trim().length == 0 ||
            this.password.trim().length == 0) {
            return;
        }

        this.router.navigate(['/overview']);
    }

    ngOnInit() :void {
        this.titleService.setTitle( 'BIG Smart Home - Anmelden' );
    }
}
