import { Component, OnInit } from '@angular/core';
import { Title } from "@angular/platform-browser";

import { Device } from '../model/device';
import { DeviceService } from '../services/device.service';

@Component({
    selector: 'overview-page',
    templateUrl: '/app/views/overview.component.html',
    providers: [ DeviceService ]
})
export class OverviewComponent implements OnInit{
    devices: Device[];

    constructor(
        private titleService: Title,
        private deviceService: DeviceService
    ) {}

    getDevices(): void {
        this.deviceService.getDevices().then( devices => this.devices = devices );
    }

    ngOnInit() :void {
       this.titleService.setTitle( 'BIG Smart Home - Geräte' );
       this.getDevices();
    }
}
