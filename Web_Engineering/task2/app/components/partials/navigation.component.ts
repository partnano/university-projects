import { Component, Input } from '@angular/core';

@Component({
    selector: 'navigation',
    templateUrl: '/app/views/partials/navigation.component.html'
})
export class NavigationComponent {
    @Input() jumplink = '#formheadline';
    @Input() routerlink = '/overview';
    @Input() optionslink = true;
    @Input() logoutlink = true;
}
