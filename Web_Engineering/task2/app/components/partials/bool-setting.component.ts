import {Component, Input, ViewChild, OnInit} from '@angular/core';
import {NgForm} from '@angular/forms';
import {BaseChartDirective} from 'ng2-charts'

import {Device} from '../../model/device';

@Component({
    selector: 'bool-setting',
    templateUrl: '/app/views/partials/detail-boolsetting.component.html'
})
export class BoolSettingComponent implements OnInit {
    @Input() device: Device;
    @Input() unit: any;
    int_log: number[] = new Array(); // past values
    str_log: string = '';

    dough_labels: string[] = ['Aus', 'An'];
    dough_data: number[] = [0, 0];

    @ViewChild(BaseChartDirective) private chart: BaseChartDirective;

    ngOnInit() {
        this.dough_data[this.unit.current] = 1;
    }

    onSubmit(f: NgForm) {

        this.int_log.push(this.unit.current);
        this.unit.current = f.value['new-value'] ? 1 : 0;

        var oldv = this.int_log[this.int_log.length - 1] ? 'An' : 'Aus';
        var newv = this.unit.current ? 'An' : 'Aus';

        var date = new Date().toLocaleString('de-AT');

        this.str_log += date + ': ' + oldv + ' -> ' + newv + '\n';

        // chart
        this.dough_data[this.unit.current]++;
        this.chart.chart.update();
    }
}
