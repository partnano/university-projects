import {Component, Input, ViewChild, OnInit} from '@angular/core';
import {NgForm} from '@angular/forms';
import {BaseChartDirective} from 'ng2-charts'

import {Device} from '../../model/device';

@Component({
    selector: 'enum-setting',
    templateUrl: '/app/views/partials/detail-enumsetting.component.html'
})
export class EnumSettingComponent implements OnInit {
    @Input() device: Device;
    @Input() unit: any;
    int_log: number[] = new Array(); // past values
    str_log: string = '';

    polar_data: number[] = new Array();
    polar_labels: string[];
    polar_legend: boolean = true;

    @ViewChild(BaseChartDirective) private chart: BaseChartDirective;

    ngOnInit() {
        this.polar_labels = this.unit.values;
        for (let _ of this.unit.values) {
            this.polar_data.push(0);
        }

        this.polar_data[this.unit.current] = 1;
    }

    onSubmit(f: NgForm) {
        this.int_log.push(this.unit.current);
        this.unit.current = +f.value['new-value'];

        var oldv = this.unit.values[this.int_log[this.int_log.length - 1]];
        var newv = this.unit.values[this.unit.current];

        var date = new Date().toLocaleString('de-AT');

        this.str_log += date + ': ' + oldv + ' -> ' + newv + '\n';

        this.polar_data[this.unit.current]++;
        this.chart.chart.update();
    }
}
