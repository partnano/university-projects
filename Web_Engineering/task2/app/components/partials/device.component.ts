import { Component, Input, AfterViewInit } from '@angular/core';

import { Device } from '../../model/device';

@Component({
    selector: 'device',
    templateUrl: '/app/views/partials/device.component.html'
})
export class DeviceComponent implements AfterViewInit {
    @Input() device: Device;
    private currentlyEditing: boolean = false;

    tempTitle: string;

    ngAfterViewInit (): void {
        this.device.draw_image('#'+this.device.id, '../../'+this.device.image, this.device.control_units[0].min, this.device.control_units[0].max, this.device.control_units[0].current, this.device.control_units.values());
    }

    deviceClicked() {
        this.abortEditingTitle();
    }

    inputClicked(event: Event) {
        // prevent other click events to be triggered, specifically deviceClicked()
        event.stopPropagation();
        event.preventDefault();
    }

    startEditingTitle() {
        this.tempTitle = this.device.display_name;
        this.currentlyEditing = true;

        return false;
    }

    abortEditingTitle() {
        this.currentlyEditing = false;
        this.tempTitle = this.device.display_name;
    }

    saveTitle() {
        this.device.display_name = this.tempTitle;
        this.currentlyEditing = false;

        return false;
    }
}