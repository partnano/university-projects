import { Component } from '@angular/core';

@Component({
    selector: 'sidebar',
    templateUrl: '/app/views/partials/sidebar.component.html'
})
export class SidebarComponent {}