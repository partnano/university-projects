import {Component, Input, OnInit} from '@angular/core';
import {NgForm} from '@angular/forms';

import {Device} from '../../model/device';

@Component({
    selector: 'cont-setting',
    templateUrl: '/app/views/partials/detail-contsetting.component.html'
})
export class ContSettingComponent implements OnInit {
    @Input() device: Device;
    @Input() unit: any;
    int_log: number[] = new Array(); // past values
    str_log: string = '';

    label: string = 'Temperatur Verlauf';
    line_data: Array<any>;
    line_labels: string[] = [];
    line_legend: boolean = true;

    ngOnInit() {
        this.int_log.push(this.unit.current);

        var date = new Date().toLocaleString('de-AT');
        this.line_labels.push(date);

        this.line_data = [{data: this.int_log, label: this.label}];
    }

    onSubmit(f: NgForm) {
        this.unit.current = f.value['new-value'];

        var oldv = this.int_log[this.int_log.length - 1];
        var newv = this.unit.current;

        var date = new Date().toLocaleString('de-AT');

        this.str_log += date + ': ' + oldv + ' -> ' + newv + '\n';

        this.int_log.push(this.unit.current);
        this.line_labels.push(date);
        this.line_data = [{data: this.int_log, label: this.label}];
    }
}
