import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Title } from "@angular/platform-browser";

@Component({
    selector: 'options-page',
    templateUrl: '/app/views/options.component.html'
})
export class OptionsComponent implements OnInit{
    oldPassword = '';
    newPassword1 = '';
    newPassword2 = '';

    isValid = false;

    constructor(
        private titleService: Title
    ) {}

    valueChanged() {
        if (this.oldPassword.trim().length != 0 &&
            this.newPassword1.length != 0 &&
            this.newPassword1 == this.newPassword2) {
            this.isValid = true;
        } else {
            this.isValid = false;
        }
    }

    changePassword() {
        if (this.isValid) {
            this.oldPassword = '';
            this.newPassword1 = '';
            this.newPassword2 = '';
            this.valueChanged();
        }
    }

    ngOnInit() :void {
        this.titleService.setTitle( 'BIG Smart Home - Optionen' );
    }
}
