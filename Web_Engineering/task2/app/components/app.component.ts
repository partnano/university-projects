import { Component } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'my-app',
    template: `
            <router-outlet></router-outlet>
            <footer>
                © 2017 BIG Smart Home
            </footer>
    `
})
export class AppComponent {}