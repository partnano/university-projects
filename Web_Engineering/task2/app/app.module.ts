import { NgModule } from '@angular/core';
import { BrowserModule, Title } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './components/app.component';
import { LoginComponent } from './components/login.component';
import { OverviewComponent } from './components/overview.component';
import { DeviceComponent } from './components/partials/device.component';
import { NavigationComponent } from './components/partials/navigation.component';
import { DetailsComponent } from './components/details.component';
import { ContSettingComponent } from './components/partials/cont-setting.component';
import { BoolSettingComponent } from './components/partials/bool-setting.component';
import { EnumSettingComponent } from './components/partials/enum-setting.component';
import { OptionsComponent } from './components/options.component';
import { SidebarComponent } from './components/partials/sidebar.component';
import { FocusOnInitDirective } from './focus-on-init.directive';

@NgModule({
  imports: [
      BrowserModule,
      FormsModule,
      ChartsModule,
      AppRoutingModule
  ],
  declarations: [
      AppComponent,
      DeviceComponent,
      NavigationComponent,
      LoginComponent,
      OverviewComponent,
      DetailsComponent,
      ContSettingComponent,
      BoolSettingComponent,
      EnumSettingComponent,
      OptionsComponent,
      SidebarComponent,
      FocusOnInitDirective
  ],
  providers: [
      Title
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
