/*jslint node: true */
/*jslint esversion: 6*/
/*jslint eqeqeq: true */

var express = require('express');
var app = express();
var fs = require("fs");
var expressWs = require('express-ws')(app);
var http = require('http');

var simulation = require('./simulation.js');
var bodyParser = require('body-parser');
var jwt = require('jsonwebtoken');
var cors = require('cors');
var uuid = require('uuid').v4;

// key: username
// value: password
let users = readUser();

// array of devices
let devices = readDevices();

let activeTokens = new Set();
let secretKey = 'super secret key';

let authorizedSessions = new Array();

let server_start = new Date();
let failed_logins = 0;

function createToken(username) {
    let id = uuid();
	let token = jwt.sign({
		id: id,
		username: username
	}, secretKey);
	return token;
}

function getTokenFromRequest(req) {
    let authHeader = req.get('Authorization');

    if (typeof authHeader === "string") {
        let headerSplitted = authHeader.split(" ");

        if (headerSplitted.length === 2 && headerSplitted[0] === "Bearer") {
            token = headerSplitted[1];

            return token;
        }
    }

    return '';
}

// Middleware for router that requires an active token before passing on
// to next handler, ends request with 401 if invalid (or no) token was provided
function validTokenRequired(req, res, next) {
    let token = getTokenFromRequest(req);

    if (token !== '' && activeTokens.has(token)) {
        return next();
    }

    res.status(401).end();
}

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(cors());

//TODO Implementieren Sie hier Ihre REST-Schnittstelle
/* Ermöglichen Sie wie in der Angabe beschrieben folgende Funktionen:
 *  Abrufen aller Geräte als Liste
 *  Hinzufügen eines neuen Gerätes
 *  Löschen eines vorhandenen Gerätes
 *  Bearbeiten eines vorhandenen Gerätes (Verändern des Gerätezustandes und Anpassen des Anzeigenamens)
 *  Log-in und Log-out des Benutzers
 *  Ändern des Passworts
 *  Abrufen des Serverstatus (Startdatum, fehlgeschlagene Log-ins).
 *
 *  BITTE BEACHTEN!
 *      Verwenden Sie dabei passende Bezeichnungen für die einzelnen Funktionen.
 *      Achten Sie bei Ihrer Implementierung auch darauf, dass der Zugriff nur nach einem erfolgreichem Log-In erlaubt sein soll.
 *      Vergessen Sie auch nicht, dass jeder Client mit aktiver Verbindung über alle Aktionen via Websocket zu informieren ist.
 *      Bei der Anlage neuer Geräte wird eine neue ID benötigt. Verwenden Sie dafür eine uuid (https://www.npmjs.com/package/uuid, Bibliothek ist bereits eingebunden).
 */

app.post("/updateCurrent", validTokenRequired, function (req, res) {
    "use strict";
    //TODO Vervollständigen Sie diese Funktion, welche den aktuellen Wert eines Gerätes ändern soll
    /*
     * Damit die Daten korrekt in die Simulation übernommen werden können, verwenden Sie bitte die nachfolgende Funktion.
     *      simulation.updatedDeviceValue(device, control_unit, Number(new_value));
     * Diese Funktion verändert gleichzeitig auch den aktuellen Wert des Gerätes, Sie müssen diese daher nur mit den korrekten Werten aufrufen.
     */

    let device = req.body[0];
    let control_unit = req.body[1];
    let new_val = control_unit.current;

    let index = getDeviceIndexForDeviceId(device.id, devices);
    let native_device = devices[index];
    let native_control_unit = null;

    for (let unit_key in native_device.control_units) {
        if (native_device.control_units[unit_key].name === control_unit.name) {
            native_control_unit = native_device.control_units[unit_key];
            break;
        }
    }

    simulation.updatedDeviceValue(native_device, native_control_unit, Number(new_val));

    //refreshConnected();
});

app.post("/sessions", function(req, res) {
    let user = req.body.username;
    let pass = req.body.password;

    // Check if provided credentials are incomplete or incorrect
    if (typeof user !== "string"
        || typeof pass !== "string"
        || !users.hasOwnProperty(user)
        || users[user] !== pass) {
            res.status(401).end();
            failed_logins++;
            return;
        }
    else {
        let token = createToken(user);
        activeTokens.add(token);

        res.status(200).json({token: token});
    }
});

app.delete("/sessions", function(req, res) {
    let token = getTokenFromRequest(req);
    console.log('logging out user, token:', token);

    if (token !== '' && activeTokens.has(token)) {
        activeTokens.delete(token);

        res.status(200).end();
        console.log('user logged out.');
        return;
    }

    console.log('logout failed');
    res.status(401).end();
});

app.get("/devices", validTokenRequired, function(req, res) {
    let json = {
        devices: (devices || [])
    };

    res.json(json);
});

app.delete("/devices/:id", validTokenRequired, function(req, res) {
    let id = req.params.id;
    if (typeof id !== "string") {
        return res.status(400).end(); // Bad Request
    } 
    
    let deviceIndex = getDeviceIndexForDeviceId(id, devices);
    if (deviceIndex > -1) {
        devices.splice(deviceIndex, 1);

        refreshConnected();

        return res.status(200).end();
    } else {
        return res.status(404).end(); // Not found
    }
});

app.post("/devices", validTokenRequired, function(req, res) {
    let display_name = req.body.display_name;
    let type = req.body.type;
    let type_name = req.body.type_name;
    let control_units = req.body.control_units;

    if (!checkCompleteness(req.body)) {
        return res.status(400).json({success: false}).end();
    }

    //set image
    let image = "images/placeholder.svg";
    if (type == "Heizkörperthermostat") {
        image = "images/thermometer.svg";
    } else if (type == "Beleuchtung") {
        image = "images/bulb.svg";
    } else if (type == "Überwachungskamera" || type == "Webcam") {
        image = "images/webcam.svg";
    } else if (type == "Rollladen") {
        image = "images/roller_shutter.svg";
    }

    let lastIndex = devices.push({
        "id": uuid(),
        "description": "",
        "display_name": display_name,
        "type": type,
        "type_name": type_name,
        "image": image,
        "image_alt": "alt for img",
        "control_units": control_units
    }) - 1;

    refreshConnectedAdd(devices[lastIndex]);

    return res.json({success: true, device: devices[lastIndex]});
});

function checkCompleteness(data) {
    console.log("completeness", data);
    if ("control_units" in data) {
        let cu = data.control_units[0];
        if ("type" in cu) {
            switch (cu.type) {
                case "boolean":
                    return (cu.name !== "");
                    break;
                case "enum":
                    return (cu.name !== "" && cu.values !== []);
                    break;
                case "continuous":
                    console.log("type: ", typeof cu.min);
                    return (cu.name !== "" && typeof cu.min === "number" && typeof cu.max === "number");
                    break;
            }
        }
        return false;
    }
    return false;
}

app.put("/devices/:id", validTokenRequired, function(req, res) {
    let id = req.params.id;
    if (typeof  id !== "string") {
        return res.status(400).end();
    }

    let deviceIndex = getDeviceIndexForDeviceId(id, devices);
    if (deviceIndex < 0) {
        return res.status(404).end(); // Not found
    }
    devices[deviceIndex].display_name = req.body.display_name;

    refreshConnectedUpdate(devices[deviceIndex]);

    return res.status(200).end();
});

app.put("/options", validTokenRequired, function(req, res) {

    let token = getTokenFromRequest(req);
    let username = jwt.verify(token, secretKey).username;

    if(req.body.old_password !== users[username]){
        return res.status(401).end();
    }

    if(req.body.new_password !== req.body.new_password_conf){
        return res.status(400).end();
    }

    users[username] = req.body.new_password;

    let jsonUsers = [];
    for(let user in users) {
        jsonUsers.push({username: user, password: users[user]});
    }
    fs.writeFile("resources/login.config", JSON.stringify(jsonUsers), function(err) {
        if (err) {
            console.log("ERROR: " + err);
        }
    });

    return res.status(200).end();
});

app.get("/status", validTokenRequired, function(req, res) {
    return res.json({
        server_start: server_start.toJSON(),
        failed_logins: failed_logins
    }).end();
});



function readUser() {
    "use strict";
    //TODO Lesen Sie die Benutzerdaten aus dem login.config File ein.

    let jsonUsers = JSON.parse(fs.readFileSync("resources/login.config"));
    let nativeUsers = {};

    for (let user in jsonUsers) {
        nativeUsers[jsonUsers[user].username] = jsonUsers[user].password;
    }


    return nativeUsers;
}

function readDevices() {
    "use strict";
    //TODO Lesen Sie die Gerätedaten aus der devices.json Datei ein.
    /*
     * Damit die Simulation korrekt funktioniert, müssen Sie diese mit nachfolgender Funktion starten
     *      simulation.simulateSmartHome(devices.devices, refreshConnected);
     * Der zweite Parameter ist dabei eine callback-Funktion, welche zum Updaten aller verbundenen Clients dienen soll.
     */

    let fileContents = fs.readFileSync("resources/devices.json");
    let devices = JSON.parse(fileContents).devices;

    return devices;
}


/**
 * precondition: devices is an array of devices, each containing field `id`
 * 
 * @returns the array index of the searched device in the passed `devices` array, or -1 if not found
 */
function getDeviceIndexForDeviceId(id, devices) {
    return devices.findIndex(function(device) {
        if (device.id == id) {
            return true;
        }
        return false;
    });
}

app.ws('/', function (ws, req) {
    ws.on('message', function(msg) {
        if (activeTokens.has(msg)) {
            console.log("User valid!");
            authorizedSessions.push(ws);
            ws.send('auth');
        }
    });

    ws.on('close', function() {
        console.log("WS connection closing!");
        let i = authorizedSessions.indexOf(ws);
        if (i >= 0)
            authorizedSessions.splice(i, 1);
    });
})

function refreshConnected() {
    "use strict";
    //TODO Übermitteln Sie jedem verbundenen Client die aktuellen Gerätedaten über das Websocket
    /*
     * Jedem Client mit aktiver Verbindung zum Websocket sollen die aktuellen Daten der Geräte übermittelt werden.
     * Dabei soll jeder Client die aktuellen Werte aller Steuerungselemente von allen Geräte erhalten.
     * Stellen Sie jedoch auch sicher, dass nur Clients die eingeloggt sind entsprechende Daten erhalten.
     *
     * Bitte beachten Sie, dass diese Funktion von der Simulation genutzt wird um periodisch die simulierten Daten an alle Clients zu übertragen.
     */

    //console.log('In refreshConnected!');

    authorizedSessions.forEach(function (client) {
        client.send(JSON.stringify({"type": "all", "data": devices}));
    });
}

function refreshConnectedAdd(device) {
    authorizedSessions.forEach(function (client) {
        client.send(JSON.stringify({"type": "add", "data": device}));
    });
}

function refreshConnectedUpdate(device) {
    authorizedSessions.forEach(function (client) {
        client.send(JSON.stringify({"type": "update", "data": device}));
    })
}

var server = app.listen(8081, function () {
    "use strict";
    readUser();
    readDevices();

    simulation.simulateSmartHome(devices, refreshConnected);

    var host = server.address().address;
    var port = server.address().port;
    console.log("Big Smart Home Server listening at http://%s:%s", host, port);
});
