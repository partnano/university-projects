"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var device_service_1 = require("../services/device.service");
var auth_service_1 = require("../services/auth.service");
var device_parser_service_1 = require("../services/device-parser.service");
var OverviewComponent = (function () {
    function OverviewComponent(deviceService, authService, deviceParser) {
        this.deviceService = deviceService;
        this.authService = authService;
        this.deviceParser = deviceParser;
        this.device_num = 0;
        this.update = true;
        this.isAddDevice = false;
    }
    OverviewComponent.prototype.ngOnInit = function () {
        this.update = true;
        this.listDevices();
        if (!this.ws_conn) {
            this.ws_conn = this.authService.getWebsocket();
        }
        if (this.ws_conn) {
            var _this_1 = this;
            this.ws_conn.onmessage = function (msg) {
                if (msg.data !== "auth") {
                    var data = JSON.parse(msg.data);
                    if (data.type === "all") {
                        var devs = data.data;
                        devs.forEach(function (dev) { return _this_1.deviceParser.parseDevice((dev)); });
                        _this_1.devices = devs;
                    }
                    else if (data.type === "add") {
                        var dev = _this_1.deviceParser.parseDevice(data.data);
                        _this_1.devices.push(dev);
                    }
                    else if (data.type === "update") {
                        var dev = _this_1.deviceParser.parseDevice(data.data);
                        for (var i = 0; i < _this_1.devices.length; ++i) {
                            if (_this_1.devices[i].id === dev.id) {
                                _this_1.devices[i] = dev;
                            }
                        }
                    }
                    _this_1.update = true;
                }
            };
        }
    };
    OverviewComponent.prototype.ngAfterViewChecked = function () {
        if (this.devices != null && this.device_num != this.devices.length && this.device_num < this.devices.length) {
            this.update = true;
            this.device_num = this.devices.length;
        }
        if (this.devices != null && this.device_num > this.devices.length) {
            this.device_num = this.devices.length;
        }
        if (this.devices == null || !this.update) {
            return;
        }
        this.update = false;
        for (var _i = 0, _a = this.devices; _i < _a.length; _i++) {
            var device = _a[_i];
            if (device.draw_image == null) {
                continue;
            }
            for (var _b = 0, _c = device.control_units; _b < _c.length; _b++) {
                var control_unit = _c[_b];
                if (control_unit.primary) {
                    device.draw_image(device.id, device.image, control_unit.min, control_unit.max, control_unit.current, control_unit.values);
                }
            }
        }
    };
    OverviewComponent.prototype.listDevices = function () {
        var _this = this;
        this.deviceService.getDevices().then(function (devices) {
            _this.devices = devices;
            _this.edit = new Array(_this.devices.length);
            for (var i = 0; i < _this.devices.length; i++) {
                _this.edit[i] = { id: _this.devices[i].id, value: false };
            }
            _this.device_num = devices.length;
        });
    };
    OverviewComponent.prototype.addDevice = function () {
        this.isAddDevice = true;
    };
    OverviewComponent.prototype.closeAddDeviceWindow = function () {
        this.isAddDevice = false;
    };
    OverviewComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'my-overview',
            templateUrl: '../views/overview.html'
        }), 
        __metadata('design:paramtypes', [device_service_1.DeviceService, auth_service_1.AuthService, device_parser_service_1.DeviceParserService])
    ], OverviewComponent);
    return OverviewComponent;
}());
exports.OverviewComponent = OverviewComponent;
//# sourceMappingURL=overview.component.js.map