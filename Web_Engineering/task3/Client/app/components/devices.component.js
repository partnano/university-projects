"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var device_service_1 = require("../services/device.service");
var overview_component_1 = require("./overview.component");
var DevicesComponent = (function () {
    function DevicesComponent(deviceService) {
        this.deviceService = deviceService;
        this.overViewComponent = null;
    }
    /**
     * Liest alle Geräte aus und initialisiert ein Flag zum Editierungs-Status dieses Gerätes
     */
    /**
     * Liest aus ob ein Gerät derzeit bearbeitet wird
     * @param device
     * @returns {boolean}
     */
    DevicesComponent.prototype.isEdited = function (device) {
        var index = this.findStatus(device);
        if (index < 0) {
            return false;
        }
        return this.overViewComponent.edit[index].value;
    };
    /**
     * Liefert den index des gewünschten Gerätes innerhalb des Arrays für den Editierungs-Status zurück
     * @param device
     * @returns {number}
     */
    DevicesComponent.prototype.findStatus = function (device) {
        for (var i = 0; i < this.overViewComponent.edit.length; i++) {
            if (device.id === this.overViewComponent.edit[i].id) {
                return i;
            }
        }
        return -1;
    };
    /**
     * Ersetzt das Geräte-Label durch ein Input-Field und ermöglicht so ein Ändern des Anzeigenamens
     * @param device
     */
    DevicesComponent.prototype.editDevice = function (device) {
        var index = this.findStatus(device);
        if (index >= 0) {
            this.overViewComponent.edit[index].value = true;
        }
        var device_outer = $(".device-outer[data-device-id=" + device.id + "]");
        var edit = device_outer.find(".device-edit");
        edit.hide();
        var remove = device_outer.find(".device-remove");
        remove.attr("src", "../images/ok.png");
    };
    /**
     * Speichert die Änderungen welche am Gerät durchgeführt wurden
     * @param device
     */
    DevicesComponent.prototype.finishEdit = function (device) {
        this.showLabel(device);
        //TODO Lesen Sie den geänderten Anzeigenamen aus und speichern Sie diesen über die REST-Schnittstelle
        this.deviceService.editDeviceName(device.id, device.display_name);
    };
    /**
     * Entfernt das angegebene Gerät
     * @param device
     */
    DevicesComponent.prototype.removeDevice = function (device) {
        //TODO Löschen Sie das angegebene Geräte über die REST-Schnittstelle
        var _this = this;
        console.log('removeDevice called');
        this.deviceService.removeDevice(device.id)
            .then(function (wasSuccess) {
            if (wasSuccess === true) {
                var index = _this.getDeviceIndexForDeviceId(device.id);
                if (index > -1) {
                    _this.overViewComponent.devices.splice(index, 1);
                }
            }
        });
    };
    /**
     * @returns the array index of the searched device or -1 if not found
     */
    DevicesComponent.prototype.getDeviceIndexForDeviceId = function (id) {
        return this.overViewComponent.devices.findIndex(function (device) {
            if (device.id == id) {
                return true;
            }
            return false;
        });
    };
    /**
     * Setz das Input-Feld wieder auf ein Label zurück und beendet so das Bearbeiten
     * @param device
     */
    DevicesComponent.prototype.showLabel = function (device) {
        var index = this.findStatus(device);
        if (index >= 0) {
            this.overViewComponent.edit[index].value = false;
        }
        var device_outer = $(".device-outer[data-device-id=" + device.id + "]");
        var edit = device_outer.find(".device-edit");
        edit.show();
        var remove = device_outer.find(".device-remove");
        remove.attr("src", "../images/remove.png");
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', overview_component_1.OverviewComponent)
    ], DevicesComponent.prototype, "overViewComponent", void 0);
    DevicesComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'my-devices',
            templateUrl: '../views/devices.component.html'
        }), 
        __metadata('design:paramtypes', [device_service_1.DeviceService])
    ], DevicesComponent);
    return DevicesComponent;
}());
exports.DevicesComponent = DevicesComponent;
//# sourceMappingURL=devices.component.js.map