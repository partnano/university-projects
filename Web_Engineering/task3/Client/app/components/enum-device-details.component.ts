import {Component, Input, OnInit} from '@angular/core';
import {Device} from "../model/device";
import {ControlUnit} from "../model/controlUnit";
import {DeviceService} from "../services/device.service";

import {AuthService} from '../services/auth.service';
import * as config from '../config';


@Component({
    moduleId: module.id,
    selector: 'enum-details',
    templateUrl: '../views/enum-device-details.component.html'
})
export class EnumDeviceDetailsComponent implements OnInit {
    @Input()
    device: Device;

    @Input()
    controlUnit: ControlUnit;

    constructor(private deviceService: DeviceService,
                private auth: AuthService) {
    };

    storage_id: string;

    labels: string[];
    new_value: string;
    log_message: string;

    ws_conn: WebSocket;

    ngOnInit(): void {
        this.new_value = this.controlUnit.values[this.controlUnit.current];

        this.storage_id = this.device.id + "-enum";

        if (!sessionStorage.getItem(this.storage_id)) {
            for (let val of this.controlUnit.values) {
                this.polarChartLabels.push(val);
                this.polarChartData.push(0);
            }

            let new_data = [this.polarChartLabels, this.polarChartData, ""];

            sessionStorage.setItem( this.storage_id, JSON.stringify(new_data) );
        }

        let data = JSON.parse( sessionStorage.getItem(this.storage_id) );
        this.polarChartLabels = data[0];

        for (let key in data[1]) {
            this.polarChartData.push(data[1][key]);
        }

        this.log_message = data[2];
        this.controlUnit.log = this.log_message;
    
        //console.log (this.polarChartLabels, this.polarChartData, this.log_message);

         // WEBSOCKET STUFF
        if (!this.ws_conn)
            this.ws_conn = this.auth.getWebsocket();

        let current_component = this;

        // precondition: websocket only sends device lists
        if (this.ws_conn) {
            this.ws_conn.onmessage = function (msg: any) {
                if (msg.data === 'auth')
                    return;
                if (JSON.parse(msg.data).type === "all") {
                    let server_devices: any = JSON.parse(msg.data).data;
                    let current_server_device: Device;

                    server_devices.forEach(function (device: Device) {
                        if (device.id === current_component.device.id)
                            current_server_device = device;
                    });

                    let current_control_unit: ControlUnit;
                    let new_value: string = null;

                    current_server_device.control_units.forEach(function (control_unit: ControlUnit) {
                        if (control_unit.name === current_component.controlUnit.name)
                            if (control_unit.current != current_component.controlUnit.current)
                                new_value = control_unit.values[control_unit.current];
                    });

                    if (new_value != null) {
                        current_component.new_value = new_value;
                        current_component.onSubmit();
                    }
                }
            }
        }
    }

    //TODO Überarbeiten Sie diese Klasse. Lesen Sie die Daten für das Diagramm aus dem SessionStorage aus und passen Sie die onSubmit Funktion an.

    /**
     * Liest den neuen Wert des Steuerungselements aus und leitet diesen an die REST-Schnittstelle weiter
     */
    onSubmit(): void {
        //TODO Lesen Sie die eingebenen Daten aus und verarbeiten Sie diese über die REST-Schnittstelle

        let _polarChartData: Array<any> = Object.assign({}, this.polarChartData);
        let index = this.controlUnit.values.indexOf(this.new_value);
        _polarChartData[index]++;

        if (this.log_message.length != 0) {
            this.log_message += "\n";
        } else {
            this.log_message = "";
        }
        this.log_message += new Date().toLocaleString() + ": " + this.controlUnit.values[this.controlUnit.current] + " -> " + this.new_value;

        this.controlUnit.log = this.log_message;
        this.polarChartData = _polarChartData;
        this.controlUnit.current = index;

        sessionStorage.setItem( this.storage_id, JSON.stringify([this.polarChartLabels, this.polarChartData, this.log_message]));

        //AJAX STUFF
        let req = new XMLHttpRequest();
        req.open( "POST", config.api.url + "/updateCurrent", true);

        let token_header = this.auth.getHttpAuthHeader();
        req.setRequestHeader("Authorization", token_header.Authorization);
        req.setRequestHeader("Content-Type", "application/json;charset=UTF-8");

        let send_data = [this.device, this.controlUnit]
        req.send( JSON.stringify(send_data) );
    }

    isSelected(val: string): boolean {
        return val == this.controlUnit.values[this.controlUnit.current];
    }

    public polarChartLabels: string[] = [];

    public polarChartData: number[] = [];
    public polarChartType: string = 'polarArea';
    public polarChartOptions: any = {
        responsive: true,
        maintainAspectRatio: false
    };
    public polarChartLegend: boolean = true;

}
