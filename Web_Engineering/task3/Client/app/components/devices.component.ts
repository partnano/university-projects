import {Component, OnInit, AfterViewChecked, Input, OnChanges} from '@angular/core';
import {DeviceService} from "../services/device.service";
import {Device} from "../model/device";
import {OverviewComponent} from "./overview.component";

declare var $: any;

@Component({
    moduleId: module.id,
    selector: 'my-devices',
    templateUrl: '../views/devices.component.html'
})
export class DevicesComponent {

    @Input()
    overViewComponent: OverviewComponent = null;





    constructor(private deviceService: DeviceService) {
    }





    /**
     * Liest alle Geräte aus und initialisiert ein Flag zum Editierungs-Status dieses Gerätes
     */


    /**
     * Liest aus ob ein Gerät derzeit bearbeitet wird
     * @param device
     * @returns {boolean}
     */
    isEdited(device: Device): boolean {
        let index = this.findStatus(device);
        if (index < 0) {
            return false;
        }
        return this.overViewComponent.edit[index].value;
    }

    /**
     * Liefert den index des gewünschten Gerätes innerhalb des Arrays für den Editierungs-Status zurück
     * @param device
     * @returns {number}
     */
    findStatus(device: Device): number {
        for (let i = 0; i < this.overViewComponent.edit.length; i++) {
            if (device.id === this.overViewComponent.edit[i].id) {
                return i;
            }
        }
        return -1;
    }

    /**
     * Ersetzt das Geräte-Label durch ein Input-Field und ermöglicht so ein Ändern des Anzeigenamens
     * @param device
     */
    editDevice(device: Device): void {

        let index = this.findStatus(device);
        if (index >= 0) {
            this.overViewComponent.edit[index].value = true;
        }

        var device_outer = $(".device-outer[data-device-id=" + device.id + "]");

        var edit = device_outer.find(".device-edit");
        edit.hide();

        var remove = device_outer.find(".device-remove");
        remove.attr("src", "../images/ok.png");

    }

    /**
     * Speichert die Änderungen welche am Gerät durchgeführt wurden
     * @param device
     */
    finishEdit(device: Device): void {
        this.showLabel(device);
        //TODO Lesen Sie den geänderten Anzeigenamen aus und speichern Sie diesen über die REST-Schnittstelle
        this.deviceService.editDeviceName(device.id, device.display_name);
    }

    /**
     * Entfernt das angegebene Gerät
     * @param device
     */
    removeDevice(device: Device): void {
        //TODO Löschen Sie das angegebene Geräte über die REST-Schnittstelle

        console.log('removeDevice called');

        this.deviceService.removeDevice(device.id)
            .then(wasSuccess => {
                if (wasSuccess === true) {
                    let index = this.getDeviceIndexForDeviceId(device.id);
                    if (index > -1) {
                        this.overViewComponent.devices.splice(index, 1);
                    }
                }
            });
    }

    /**
     * @returns the array index of the searched device or -1 if not found
     */
    private getDeviceIndexForDeviceId(id: string): number {
        return this.overViewComponent.devices.findIndex((device) => {
            if (device.id == id) {
                return true;
            }
            return false;
        });
    }

    /**
     * Setz das Input-Feld wieder auf ein Label zurück und beendet so das Bearbeiten
     * @param device
     */
    showLabel(device: Device): void {

        let index = this.findStatus(device);
        if (index >= 0) {
            this.overViewComponent.edit[index].value = false;
        }

        var device_outer = $(".device-outer[data-device-id=" + device.id + "]");

        var edit = device_outer.find(".device-edit");
        edit.show();

        var remove = device_outer.find(".device-remove");
        remove.attr("src", "../images/remove.png");
    }


}
