"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var overview_component_1 = require("./overview.component");
var device_service_1 = require("../services/device.service");
var OverlayComponent = (function () {
    function OverlayComponent(deviceService) {
        this.deviceService = deviceService;
        this.overviewComponent = null;
        this.selected_type = null;
        this.controlUnitType_selected = null;
        this.addError = false;
        this.createError = false;
    }
    OverlayComponent.prototype.ngOnInit = function () {
        this.device_types = ["Beleuchtung", "Heizkörperthermostat", "Rollladen", "Überwachungskamera", "Webcam"];
        this.controlUnit_types = ["Ein/Auschalter", "Diskrete Werte", "Kontinuierlicher Wert"];
        this.selected_type = this.device_types[0];
        this.controlUnitType_selected = this.controlUnit_types[0];
    };
    OverlayComponent.prototype.doClose = function () {
        if (this.overviewComponent != null) {
            this.overviewComponent.closeAddDeviceWindow();
        }
    };
    /**
     * Liest die Daten des neuen Gerätes aus der Form aus und leitet diese an die REST-Schnittstelle weiter
     * @param form
     */
    OverlayComponent.prototype.onSubmit = function (form) {
        var _this = this;
        //TODO Lesen Sie Daten aus der Form aus und übertragen Sie diese an Ihre REST-Schnittstelle
        var input = form.value;
        console.log(input);
        var device = {
            display_name: input.displayname,
            type: input["type-input"],
            type_name: input.typename,
            control_units: [this.fillControlUnit(input)]
        };
        console.log(device);
        this.deviceService.createDevice(device).then(function (device) {
            _this.addError = false;
            _this.createError = false;
            _this.overviewComponent.devices.push(device);
            form.reset();
            _this.overviewComponent.closeAddDeviceWindow();
        }).catch(function (response) {
            if (response === "incomplete") {
                _this.addError = true;
            }
            else {
                _this.createError = true;
            }
        });
    };
    OverlayComponent.prototype.fillControlUnit = function (input) {
        var control_unit = { name: '', type: '', values: [''], current: 0, min: 0, max: 0, primary: true };
        control_unit.name = input.elementname;
        switch (input["elementtype-input"]) {
            case "Ein/Auschalter":
                control_unit.type = "boolean";
                control_unit.values = [""];
                control_unit.current = 0;
                break;
            case "Diskrete Werte":
                control_unit.type = "enum";
                control_unit.values = input["discrete-values"].split(',').map(function (e) { return e.trim(); });
                control_unit.current = 0;
                break;
            case "Kontinuierlicher Wert":
                control_unit.type = "continuous";
                control_unit.min = input["minimum-value"];
                control_unit.max = input["maximum-value"];
                control_unit.current = control_unit.min;
                break;
        }
        control_unit.primary = true;
        return control_unit;
    };
    OverlayComponent.prototype.isSelected = function (type) {
        return type == this.device_types[0];
    };
    OverlayComponent.prototype.isBooleanSelected = function () {
        return this.controlUnitType_selected === this.controlUnit_types[0];
    };
    OverlayComponent.prototype.isEnumSelected = function () {
        return this.controlUnitType_selected === this.controlUnit_types[1];
    };
    OverlayComponent.prototype.isContinuousSelected = function () {
        return this.controlUnitType_selected === this.controlUnit_types[2];
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', overview_component_1.OverviewComponent)
    ], OverlayComponent.prototype, "overviewComponent", void 0);
    OverlayComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'my-overlay',
            templateUrl: '../views/overlay.component.html'
        }), 
        __metadata('design:paramtypes', [device_service_1.DeviceService])
    ], OverlayComponent);
    return OverlayComponent;
}());
exports.OverlayComponent = OverlayComponent;
//# sourceMappingURL=overlay.component.js.map