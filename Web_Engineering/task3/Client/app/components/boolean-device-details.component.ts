import {Component, Input, OnInit} from '@angular/core';
import {Device} from "../model/device";
import {ControlUnit} from "../model/controlUnit";
import {DeviceService} from "../services/device.service";

import {AuthService} from '../services/auth.service';
import * as config from '../config';

@Component({
    moduleId: module.id,
    selector: 'boolean-details',
    templateUrl: '../views/boolean-device-details.component.html'
})
export class BooleanDeviceDetailsComponent implements OnInit {
    @Input()
    device: Device;

    @Input()
    controlUnit: ControlUnit;

    new_value: boolean;
    values: number[] = [0, 0]
    log_message: string = null;
    storage_id: string = null;

    ws_conn: WebSocket;

    constructor(private deviceService: DeviceService, 
                private auth: AuthService) {
    }

    ngOnInit(): void {
        this.new_value = this.controlUnit.current == 1;

        this.storage_id = this.device.id + "-bool";

        if (!sessionStorage.getItem(this.storage_id)) {
            let new_data = [[0, 0], ""]

            sessionStorage.setItem(this.storage_id, JSON.stringify(new_data));
        }    

        let data = JSON.parse(sessionStorage.getItem(this.storage_id));

        this.values = data[0];
        this.doughnutChartData = this.values;
        this.log_message = data[1];
        this.controlUnit.log = this.log_message;

        //console.log(this.values, this.log_message);

        // WEBSOCKET STUFF
        if (!this.ws_conn)
            this.ws_conn = this.auth.getWebsocket();

        let client_device = this.device;
        let client_control_unit: ControlUnit = this.controlUnit;
        let current_component = this;

        // precondition: msg is a list of devices
        if (this.ws_conn) {
            this.ws_conn.onmessage = function (msg: any) {
                if (msg.data === 'auth')
                    return;
                if (JSON.parse(msg.data).type === "all") {
                    let server_devices: any = JSON.parse(msg.data).data;
                    let current_server_device: Device;

                    // get current device
                    server_devices.forEach(function (device: Device) {
                        if (device.id === client_device.id)
                            current_server_device = device;
                    });

                    if (current_server_device) {
                        let new_value: number = null;

                        // get current control_unit
                        current_server_device.control_units.forEach(function (control_unit: ControlUnit) {

                            // check if value changed on correct unit
                            if (control_unit.name === client_control_unit.name)
                                if (control_unit.current != client_control_unit.current)
                                    new_value = control_unit.current;

                        });

                        // if value changed, do new onSubmit
                        if (new_value != null) {
                            current_component.new_value = new_value === 0 ? false : true;
                            current_component.onSubmit();
                        }
                    }
                }
            }
        }
     }

    //TODO Überarbeiten Sie diese Klasse. Lesen Sie die Daten für das Diagramm aus dem SessionStorage aus und passen Sie die onSubmit Funktion an.

    /**
     * Liest den neuen Wert des Steuerungselements aus und leitet diesen an die REST-Schnittstelle weiter
     */
    onSubmit(): void {
        //TODO Lesen Sie die eingebenen Daten aus und verarbeiten Sie diese über die REST-Schnittstelle

        this.values[this.new_value ? 1 : 0]++;
        this.doughnutChartData = Object.assign({}, this.values);

        if (this.log_message.length != 0) 
            this.log_message += "\n";
        else 
            this.log_message = "";

        this.log_message += new Date().toLocaleString() + ": " + (this.controlUnit.current == 1 ? "An" : "Aus") + " -> " + (this.new_value ? "An" : "Aus");

        this.controlUnit.log = this.log_message;
        this.controlUnit.current = this.new_value ? 1 : 0;

        sessionStorage.setItem(this.storage_id, JSON.stringify([this.values, this.log_message]));

        // AJAX STUFF
        let req = new XMLHttpRequest();
        req.open( "POST", config.api.url + "/updateCurrent", true);

        let token_header = this.auth.getHttpAuthHeader();
        req.setRequestHeader("Authorization", token_header.Authorization);
        req.setRequestHeader("Content-Type", "application/json;charset=UTF-8");

        let send_data = [this.device, this.controlUnit]
        req.send( JSON.stringify(send_data) );
    }

    public doughnutChartData: number[] = [0, 0];
    public doughnutChartLabels: string[] = ['Aus', 'An'];
    public doughnutChartOptions: any = {
        responsive: true,
        maintainAspectRatio: false,
    };
    public doughnutChartLegend: boolean = true;
    public doughnutChartType: string = 'doughnut';

}
