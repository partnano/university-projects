import {Component, OnInit} from "@angular/core";
import {StatusService} from "../services/status.service";

@Component({
  moduleId: module.id,
  selector: 'my-sidebar',
  templateUrl: '../views/sidebar.component.html'
})
export class SidebarComponent implements OnInit{

  failed_logins: number = 0;
  server_start: Date;

  constructor(private statusService: StatusService){}

  ngOnInit(): void {
    //TODO Lesen Sie über die REST-Schnittstelle den Status des Servers aus und speichern Sie diesen in obigen Variablen
    this.statusService.getStatus().then(response => {
      this.server_start = new Date(response.server_start);
      this.failed_logins = response.failed_logins;
    }).catch(err => {
      console.log("error:", err);
    });
  }
}
