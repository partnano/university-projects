"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var device_1 = require("../model/device");
var controlUnit_1 = require("../model/controlUnit");
var device_service_1 = require("../services/device.service");
var auth_service_1 = require('../services/auth.service');
var config = require('../config');
var ContinuousDeviceDetailsComponent = (function () {
    function ContinuousDeviceDetailsComponent(deviceService, auth) {
        this.deviceService = deviceService;
        this.auth = auth;
        this.lineChartData = [
            { data: [], label: 'Verlauf' }
        ];
        this.lineChartLabels = [];
        this.lineChartOptions = {
            responsive: true,
            maintainAspectRatio: false
        };
        this.lineChartColors = [
            {
                backgroundColor: 'rgba(77,83,96,0.2)',
                borderColor: 'rgba(77,83,96,1)',
                pointBackgroundColor: 'rgba(77,83,96,1)',
                pointBorderColor: '#fff',
                pointHoverBackgroundColor: '#fff',
                pointHoverBorderColor: 'rgba(77,83,96,1)'
            }
        ];
        this.lineChartLegend = true;
        this.lineChartType = 'line';
    }
    ;
    ContinuousDeviceDetailsComponent.prototype.ngOnInit = function () {
        this.new_value = this.controlUnit.current;
        this.storage_id = this.device.id + "-cont";
        if (!sessionStorage.getItem(this.storage_id)) {
            var new_data = [[], [], ""];
            sessionStorage.setItem(this.storage_id, JSON.stringify(new_data));
        }
        var data = JSON.parse(sessionStorage.getItem(this.storage_id));
        this.values = data[0];
        this.lineChartData[0].data = data[0];
        this.lineChartLabels = data[1];
        this.log_message = data[2];
        this.controlUnit.log = this.log_message;
        //console.log(this.lineChartLabels, this.lineChartData);
        // WEBSOCKET STUFF
        if (!this.ws_conn)
            this.ws_conn = this.auth.getWebsocket();
        var current_component = this;
        // precondition: websocket only sends device lists
        if (this.ws_conn) {
            this.ws_conn.onmessage = function (msg) {
                if (msg.data === 'auth')
                    return;
                if (JSON.parse(msg.data).type === "all") {
                    var server_devices = JSON.parse(msg.data).data;
                    var current_server_device_1;
                    server_devices.forEach(function (device) {
                        if (device.id === current_component.device.id)
                            current_server_device_1 = device;
                    });
                    var current_control_unit = void 0;
                    var new_value_1 = null;
                    current_server_device_1.control_units.forEach(function (control_unit) {
                        if (control_unit.name === current_component.controlUnit.name)
                            if (control_unit.current != current_component.controlUnit.current)
                                new_value_1 = control_unit.current;
                    });
                    if (new_value_1 != null) {
                        current_component.new_value = new_value_1;
                        current_component.onSubmit();
                    }
                }
            };
        }
    };
    //TODO Überarbeiten Sie diese Klasse. Lesen Sie die Daten für das Diagramm aus dem SessionStorage aus und passen Sie die onSubmit Funktion an.
    /**
     * Liest den neuen Wert des Steuerungselements aus und leitet diesen an die REST-Schnittstelle weiter
     */
    ContinuousDeviceDetailsComponent.prototype.onSubmit = function () {
        //TODO Lesen Sie die eingebenen Daten aus und verarbeiten Sie diese über die REST-Schnittstelle
        var time = new Date();
        this.lineChartLabels.push(time.toLocaleDateString() + " " + time.toLocaleTimeString());
        this.values.push(this.new_value);
        this.lineChartData = [{ data: this.values, label: 'Verlauf' }];
        if (this.log_message.length != 0) {
            this.log_message += "\n";
        }
        else {
            this.log_message = "";
        }
        this.log_message += new Date().toLocaleString() + ": " + this.controlUnit.current + " -> " + this.new_value;
        this.controlUnit.log = this.log_message;
        this.controlUnit.current = this.new_value;
        sessionStorage.setItem(this.storage_id, JSON.stringify([this.values, this.lineChartLabels, this.log_message]));
        //AJAX STUFF
        var req = new XMLHttpRequest();
        req.open("POST", config.api.url + "/updateCurrent", true);
        var token_header = this.auth.getHttpAuthHeader();
        req.setRequestHeader("Authorization", token_header.Authorization);
        req.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        var send_data = [this.device, this.controlUnit];
        req.send(JSON.stringify(send_data));
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', device_1.Device)
    ], ContinuousDeviceDetailsComponent.prototype, "device", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', controlUnit_1.ControlUnit)
    ], ContinuousDeviceDetailsComponent.prototype, "controlUnit", void 0);
    ContinuousDeviceDetailsComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'continuous-details',
            templateUrl: '../views/continuous-device-details.component.html'
        }), 
        __metadata('design:paramtypes', [device_service_1.DeviceService, auth_service_1.AuthService])
    ], ContinuousDeviceDetailsComponent);
    return ContinuousDeviceDetailsComponent;
}());
exports.ContinuousDeviceDetailsComponent = ContinuousDeviceDetailsComponent;
//# sourceMappingURL=continuous-device-details.component.js.map