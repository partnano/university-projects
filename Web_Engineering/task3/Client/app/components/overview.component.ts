import {AfterViewChecked, Component, OnInit} from '@angular/core';
import {Device} from "../model/device";
import {DeviceService} from "../services/device.service";
import {AuthService} from "../services/auth.service";
import {DeviceParserService} from "../services/device-parser.service";

@Component({
  moduleId: module.id,
  selector: 'my-overview',
  templateUrl: '../views/overview.html'
})
export class OverviewComponent implements OnInit, AfterViewChecked{

  constructor(private deviceService: DeviceService, private authService: AuthService, private deviceParser: DeviceParserService) {
  }

  devices: Device[];
  device_num: number = 0;
  update: boolean = true;
  edit: { id: string, value: boolean }[];

  isAddDevice: boolean = false;

  ws_conn: WebSocket;

  ngOnInit(): void {
    this.update = true;
    this.listDevices();

    if (!this.ws_conn) {
      this.ws_conn = this.authService.getWebsocket()
    }

    if (this.ws_conn) {
      let _this = this;
      this.ws_conn.onmessage = function(msg: any) {
        if (msg.data !== "auth") {
          let data = JSON.parse(msg.data);
          if (data.type === "all") {
            let devs = data.data;
            devs.forEach((dev: any): Device => _this.deviceParser.parseDevice((dev)));
            _this.devices = devs;

          } else if (data.type === "add") {
            let dev = _this.deviceParser.parseDevice(data.data);
            _this.devices.push(dev);
          } else if (data.type === "update") {
            let dev = _this.deviceParser.parseDevice(data.data);
            for(let i = 0; i < _this.devices.length; ++i) {
              if (_this.devices[i].id === dev.id) {
                _this.devices[i] = dev;
              }
            }
          }
          _this.update = true;
        }
      }
    }
  }

  ngAfterViewChecked(): void {

    if (this.devices != null && this.device_num != this.devices.length && this.device_num < this.devices.length) {
      this.update = true;
      this.device_num = this.devices.length
    }

    if (this.devices != null && this.device_num > this.devices.length) {
      this.device_num = this.devices.length;
    }

    if (this.devices == null || !this.update) {
      return;
    }

    this.update = false;
    for (let device of this.devices) {
      if (device.draw_image == null) {
        continue;
      }
      for (let control_unit of device.control_units) {
        if (control_unit.primary) {
          device.draw_image(device.id, device.image, control_unit.min, control_unit.max, control_unit.current, control_unit.values);
        }
      }
    }
  }

  listDevices() {
    this.deviceService.getDevices().then(devices => {
      this.devices = devices;
      this.edit = new Array(this.devices.length);
      for (let i = 0; i < this.devices.length; i++) {
        this.edit[i] = {id: this.devices[i].id, value: false};
      }
      this.device_num = devices.length;
    });
  }

  addDevice() {
    this.isAddDevice = true;
  }

  closeAddDeviceWindow(){
    this.isAddDevice = false;
  }
}
