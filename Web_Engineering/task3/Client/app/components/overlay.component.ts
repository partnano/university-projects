import {Component, Input, OnInit} from '@angular/core';
import {NgForm} from '@angular/forms';
import {OverviewComponent} from "./overview.component";
import {DevicesComponent} from "./devices.component";
import {DeviceService} from "../services/device.service";
import {Device} from "../model/device";
import {ControlUnit} from "../model/controlUnit";
import {ControlType} from "../model/controlType";

@Component({
  moduleId: module.id,
  selector: 'my-overlay',
  templateUrl: '../views/overlay.component.html'
})
export class OverlayComponent implements OnInit {

  @Input()
  overviewComponent: OverviewComponent = null;

  device_types: any;
  controlUnit_types: any;
  selected_type: string = null;
  controlUnitType_selected: string = null;

  addError: boolean = false;
  createError: boolean = false;

  constructor(private deviceService: DeviceService) {
  }


  ngOnInit(): void {
    this.device_types = ["Beleuchtung", "Heizkörperthermostat", "Rollladen", "Überwachungskamera", "Webcam"]
    this.controlUnit_types = ["Ein/Auschalter", "Diskrete Werte", "Kontinuierlicher Wert"];
    this.selected_type = this.device_types[0];
    this.controlUnitType_selected = this.controlUnit_types[0];
  }

  doClose(): void {
    if (this.overviewComponent != null) {
      this.overviewComponent.closeAddDeviceWindow();
    }
  }

  /**
   * Liest die Daten des neuen Gerätes aus der Form aus und leitet diese an die REST-Schnittstelle weiter
   * @param form
   */
  onSubmit(form: NgForm): void {



    //TODO Lesen Sie Daten aus der Form aus und übertragen Sie diese an Ihre REST-Schnittstelle
    let input = form.value;
    console.log(input);
    let device = {
      display_name: input.displayname,
      type: input["type-input"],
      type_name: input.typename,
      control_units: [this.fillControlUnit(input)]
    };
    console.log(device);
    this.deviceService.createDevice(device).then(device => {
      this.addError = false;
      this.createError = false;
      this.overviewComponent.devices.push(device);
      form.reset();
      this.overviewComponent.closeAddDeviceWindow();
    }).catch(response => {
      if (response === "incomplete") {
        this.addError = true;
      } else {
        this.createError = true;
      }
    });

  }

  fillControlUnit(input: any): any {
    let control_unit = {name:'', type:'', values:[''], current:0, min:0, max:0, primary:true};
    control_unit.name = input.elementname;
    switch (input["elementtype-input"]) {
      case "Ein/Auschalter":
        control_unit.type = "boolean";
        control_unit.values = [""];
        control_unit.current = 0;
        break;
      case "Diskrete Werte":
        control_unit.type = "enum";
        control_unit.values = input["discrete-values"].split(',').map((e: any): any => {return e.trim()});
        control_unit.current = 0;
        break;
      case "Kontinuierlicher Wert":
        control_unit.type = "continuous";
        control_unit.min = input["minimum-value"];
        control_unit.max = input["maximum-value"];
        control_unit.current = control_unit.min;
        break;
    }
    control_unit.primary = true;
    return control_unit;
  }

  isSelected(type: string): boolean {
    return type == this.device_types[0];
  }

  isBooleanSelected(): boolean {
    return this.controlUnitType_selected === this.controlUnit_types[0];
  }

  isEnumSelected(): boolean {
    return this.controlUnitType_selected === this.controlUnit_types[1];
  }

  isContinuousSelected(): boolean {
    return this.controlUnitType_selected === this.controlUnit_types[2];
  }

}
