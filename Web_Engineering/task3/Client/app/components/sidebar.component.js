"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var status_service_1 = require("../services/status.service");
var SidebarComponent = (function () {
    function SidebarComponent(statusService) {
        this.statusService = statusService;
        this.failed_logins = 0;
    }
    SidebarComponent.prototype.ngOnInit = function () {
        var _this = this;
        //TODO Lesen Sie über die REST-Schnittstelle den Status des Servers aus und speichern Sie diesen in obigen Variablen
        this.statusService.getStatus().then(function (response) {
            _this.server_start = new Date(response.server_start);
            _this.failed_logins = response.failed_logins;
        }).catch(function (err) {
            console.log("error:", err);
        });
    };
    SidebarComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'my-sidebar',
            templateUrl: '../views/sidebar.component.html'
        }), 
        __metadata('design:paramtypes', [status_service_1.StatusService])
    ], SidebarComponent);
    return SidebarComponent;
}());
exports.SidebarComponent = SidebarComponent;
//# sourceMappingURL=sidebar.component.js.map