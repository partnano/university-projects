import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { Device } from "../model/device";
import { ControlUnit } from "../model/controlUnit";
import { DeviceService } from "../services/device.service";
import { Subject } from 'rxjs/Subject';

import { AuthService } from '../services/auth.service';
import * as config from '../config';

@Component({
    moduleId: module.id,
    selector: 'continuous-details',
    templateUrl: '../views/continuous-device-details.component.html'
})
export class ContinuousDeviceDetailsComponent implements OnInit {
    @Input()
    device: Device;

    @Input()
    controlUnit: ControlUnit;

    constructor(private deviceService: DeviceService,
        private auth: AuthService) {
    };

    storage_id: string;

    new_value: number;
    values: number[];
    dates: string[];
    log_message: string;

    ws_conn: WebSocket;

    ngOnInit(): void {
        this.new_value = this.controlUnit.current;

        this.storage_id = this.device.id + "-cont";

        if (!sessionStorage.getItem(this.storage_id)) {
            let new_data: any = [[], [], ""]

            sessionStorage.setItem(this.storage_id, JSON.stringify(new_data));
        }

        let data = JSON.parse(sessionStorage.getItem(this.storage_id));

        this.values = data[0];

        this.lineChartData[0].data = data[0];
        this.lineChartLabels = data[1];

        this.log_message = data[2];
        this.controlUnit.log = this.log_message;

        //console.log(this.lineChartLabels, this.lineChartData);

        // WEBSOCKET STUFF
        if (!this.ws_conn)
            this.ws_conn = this.auth.getWebsocket();

        let current_component = this;

        // precondition: websocket only sends device lists
        if (this.ws_conn) {
            this.ws_conn.onmessage = function (msg: any) {
                if (msg.data === 'auth')
                    return;
                if (JSON.parse(msg.data).type === "all") {
                    let server_devices: any = JSON.parse(msg.data).data;
                    let current_server_device: Device;

                    server_devices.forEach(function (device: Device) {
                        if (device.id === current_component.device.id)
                            current_server_device = device;
                    });

                    let current_control_unit: ControlUnit;
                    let new_value: number = null;

                    current_server_device.control_units.forEach(function (control_unit: ControlUnit) {
                        if (control_unit.name === current_component.controlUnit.name)
                            if (control_unit.current != current_component.controlUnit.current)
                                new_value = control_unit.current;
                    });

                    if (new_value != null) {
                        current_component.new_value = new_value;
                        current_component.onSubmit();
                    }
                }
            }
        }
    }

    //TODO Überarbeiten Sie diese Klasse. Lesen Sie die Daten für das Diagramm aus dem SessionStorage aus und passen Sie die onSubmit Funktion an.

    /**
     * Liest den neuen Wert des Steuerungselements aus und leitet diesen an die REST-Schnittstelle weiter
     */
    onSubmit(): void {
        //TODO Lesen Sie die eingebenen Daten aus und verarbeiten Sie diese über die REST-Schnittstelle

        let time = new Date();

        this.lineChartLabels.push(time.toLocaleDateString() + " " + time.toLocaleTimeString());

        this.values.push(this.new_value);
        this.lineChartData = [{ data: this.values, label: 'Verlauf' }];

        if (this.log_message.length != 0) {
            this.log_message += "\n";
        } else {
            this.log_message = "";
        }
        this.log_message += new Date().toLocaleString() + ": " + this.controlUnit.current + " -> " + this.new_value;
        this.controlUnit.log = this.log_message;
        this.controlUnit.current = this.new_value;

        sessionStorage.setItem(this.storage_id, JSON.stringify([this.values, this.lineChartLabels, this.log_message]));

        //AJAX STUFF
        let req = new XMLHttpRequest();
        req.open("POST", config.api.url + "/updateCurrent", true);

        let token_header = this.auth.getHttpAuthHeader();
        req.setRequestHeader("Authorization", token_header.Authorization);
        req.setRequestHeader("Content-Type", "application/json;charset=UTF-8");

        let send_data = [this.device, this.controlUnit]
        req.send(JSON.stringify(send_data));
    }

    public lineChartData: Array<any> = [
        { data: [], label: 'Verlauf' }
    ];
    public lineChartLabels: Array<any> = [];
    public lineChartOptions: any = {
        responsive: true,
        maintainAspectRatio: false
    };
    public lineChartColors: Array<any> = [
        { // dark grey
            backgroundColor: 'rgba(77,83,96,0.2)',
            borderColor: 'rgba(77,83,96,1)',
            pointBackgroundColor: 'rgba(77,83,96,1)',
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgba(77,83,96,1)'
        }
    ];
    public lineChartLegend: boolean = true;
    public lineChartType: string = 'line';
}



