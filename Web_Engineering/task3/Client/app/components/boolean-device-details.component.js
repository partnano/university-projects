"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var device_1 = require("../model/device");
var controlUnit_1 = require("../model/controlUnit");
var device_service_1 = require("../services/device.service");
var auth_service_1 = require('../services/auth.service');
var config = require('../config');
var BooleanDeviceDetailsComponent = (function () {
    function BooleanDeviceDetailsComponent(deviceService, auth) {
        this.deviceService = deviceService;
        this.auth = auth;
        this.values = [0, 0];
        this.log_message = null;
        this.storage_id = null;
        this.doughnutChartData = [0, 0];
        this.doughnutChartLabels = ['Aus', 'An'];
        this.doughnutChartOptions = {
            responsive: true,
            maintainAspectRatio: false,
        };
        this.doughnutChartLegend = true;
        this.doughnutChartType = 'doughnut';
    }
    BooleanDeviceDetailsComponent.prototype.ngOnInit = function () {
        this.new_value = this.controlUnit.current == 1;
        this.storage_id = this.device.id + "-bool";
        if (!sessionStorage.getItem(this.storage_id)) {
            var new_data = [[0, 0], ""];
            sessionStorage.setItem(this.storage_id, JSON.stringify(new_data));
        }
        var data = JSON.parse(sessionStorage.getItem(this.storage_id));
        this.values = data[0];
        this.doughnutChartData = this.values;
        this.log_message = data[1];
        this.controlUnit.log = this.log_message;
        //console.log(this.values, this.log_message);
        // WEBSOCKET STUFF
        if (!this.ws_conn)
            this.ws_conn = this.auth.getWebsocket();
        var client_device = this.device;
        var client_control_unit = this.controlUnit;
        var current_component = this;
        // precondition: msg is a list of devices
        if (this.ws_conn) {
            this.ws_conn.onmessage = function (msg) {
                if (msg.data === 'auth')
                    return;
                if (JSON.parse(msg.data).type === "all") {
                    var server_devices = JSON.parse(msg.data).data;
                    var current_server_device_1;
                    // get current device
                    server_devices.forEach(function (device) {
                        if (device.id === client_device.id)
                            current_server_device_1 = device;
                    });
                    if (current_server_device_1) {
                        var new_value_1 = null;
                        // get current control_unit
                        current_server_device_1.control_units.forEach(function (control_unit) {
                            // check if value changed on correct unit
                            if (control_unit.name === client_control_unit.name)
                                if (control_unit.current != client_control_unit.current)
                                    new_value_1 = control_unit.current;
                        });
                        // if value changed, do new onSubmit
                        if (new_value_1 != null) {
                            current_component.new_value = new_value_1 === 0 ? false : true;
                            current_component.onSubmit();
                        }
                    }
                }
            };
        }
    };
    //TODO Überarbeiten Sie diese Klasse. Lesen Sie die Daten für das Diagramm aus dem SessionStorage aus und passen Sie die onSubmit Funktion an.
    /**
     * Liest den neuen Wert des Steuerungselements aus und leitet diesen an die REST-Schnittstelle weiter
     */
    BooleanDeviceDetailsComponent.prototype.onSubmit = function () {
        //TODO Lesen Sie die eingebenen Daten aus und verarbeiten Sie diese über die REST-Schnittstelle
        this.values[this.new_value ? 1 : 0]++;
        this.doughnutChartData = Object.assign({}, this.values);
        if (this.log_message.length != 0)
            this.log_message += "\n";
        else
            this.log_message = "";
        this.log_message += new Date().toLocaleString() + ": " + (this.controlUnit.current == 1 ? "An" : "Aus") + " -> " + (this.new_value ? "An" : "Aus");
        this.controlUnit.log = this.log_message;
        this.controlUnit.current = this.new_value ? 1 : 0;
        sessionStorage.setItem(this.storage_id, JSON.stringify([this.values, this.log_message]));
        // AJAX STUFF
        var req = new XMLHttpRequest();
        req.open("POST", config.api.url + "/updateCurrent", true);
        var token_header = this.auth.getHttpAuthHeader();
        req.setRequestHeader("Authorization", token_header.Authorization);
        req.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        var send_data = [this.device, this.controlUnit];
        req.send(JSON.stringify(send_data));
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', device_1.Device)
    ], BooleanDeviceDetailsComponent.prototype, "device", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', controlUnit_1.ControlUnit)
    ], BooleanDeviceDetailsComponent.prototype, "controlUnit", void 0);
    BooleanDeviceDetailsComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'boolean-details',
            templateUrl: '../views/boolean-device-details.component.html'
        }), 
        __metadata('design:paramtypes', [device_service_1.DeviceService, auth_service_1.AuthService])
    ], BooleanDeviceDetailsComponent);
    return BooleanDeviceDetailsComponent;
}());
exports.BooleanDeviceDetailsComponent = BooleanDeviceDetailsComponent;
//# sourceMappingURL=boolean-device-details.component.js.map