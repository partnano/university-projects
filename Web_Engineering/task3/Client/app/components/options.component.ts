import {Component, OnInit} from '@angular/core';
import {Headers, Http} from '@angular/http';
import 'rxjs/add/operator/toPromise';
import {NgForm} from '@angular/forms';
import {OptionsService} from "../services/options.service";


@Component({
    moduleId: module.id,
    selector: 'my-options',
    templateUrl: '../views/options.html'
})
export class OptionsComponent implements OnInit {

    updateError: boolean;
    oldPwError: boolean;
    newPwError: boolean;

    constructor(private http: Http, private optionsService: OptionsService) {
    };

    ngOnInit(): void {
        this.updateError = false;
        this.oldPwError = false;
        this.newPwError = false;
    }

    public equalsPW(form: NgForm): boolean {
        if (!form || !form.value || !form.value["repeat-password"] || !form.value["new-password"]) {
            return false;
        }
        return form.value["repeat-password"] === form.value["new-password"];
    }


    /**
     * Liest das alte Passwort, das neue Passwort und dessen Wiederholung ein und übertraegt diese an die REST-Schnittstelle
     * @param form
     */
    onSubmit(form: NgForm): void {

        //TODO Lesen Sie Daten aus der Form aus und übertragen Sie diese an Ihre REST-Schnittstelle
        if (!form) {
            return;
        }

        this.optionsService.changePassword({
            old_password: form.value["old-password"],
            new_password: form.value["new-password"],
            new_password_conf: form.value["repeat-password"]
        }).then(status => {
            if (status === 200) {
                this.oldPwError = false;
                this.newPwError = false;
            }
        }).catch(res => {
            if (res.status === 400) {
                this.newPwError = true;
            } else if (res.status === 401) {
                this.oldPwError = true;
            }
        });

        form.resetForm();

    }

}
