import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {NgForm} from '@angular/forms';

import {AuthService} from '../services/auth.service';

@Component({
    moduleId: module.id,
    selector: 'my-login',
    templateUrl: '../views/login.html'
})
export class LoginComponent {
    loginError: boolean = false;

    constructor(private auth: AuthService, private router: Router) {
    }

    onSubmit(form: NgForm): void {
        //TODO Überprüfen Sie die Login-Daten über die REST-Schnittstelle und leiten Sie den Benutzer bei Erfolg auf die Overview-Seite weiter

        let user = form.value.username;
        let pass = form.value.password;

        console.log('testing from login...');
        this.auth.login(user, pass).then(wasSuccessful => {
            if (wasSuccessful) {
                this.loginError = false;
                console.log('logged in successfully');
                this.router.navigate(['/overview']);
            } else {
                this.loginError = true;
                console.log('login not successful');
            }
        }).catch(msg => {
            console.log('error caught', msg);
        });

    }
}
