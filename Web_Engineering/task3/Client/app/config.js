// this file contains global config information
// import using import * as config from 'path/to/this/file'
// access to variables then works like in this example: config.api
"use strict";
exports.api = {
    url: 'http://localhost:8081',
    ws: 'ws://localhost:8081'
};
//# sourceMappingURL=config.js.map