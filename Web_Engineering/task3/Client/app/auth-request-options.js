"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var http_1 = require('@angular/http');
var AuthRequestOptions = (function (_super) {
    __extends(AuthRequestOptions, _super);
    function AuthRequestOptions(auth) {
        _super.call(this);
        this.headers = new http_1.Headers(auth.getHttpAuthHeader());
    }
    return AuthRequestOptions;
}(http_1.BaseRequestOptions));
exports.AuthRequestOptions = AuthRequestOptions;
//# sourceMappingURL=auth-request-options.js.map