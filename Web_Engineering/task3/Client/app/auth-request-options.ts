import {Http, BaseRequestOptions, RequestOptions, RequestOptionsArgs, Headers} from '@angular/http';
import { AuthService } from './services/auth.service';

export class AuthRequestOptions extends BaseRequestOptions {
    constructor(auth: AuthService) {
        super();
        this.headers = new Headers(auth.getHttpAuthHeader());
    }
}