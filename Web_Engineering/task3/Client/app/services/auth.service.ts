import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';

import { AuthRequestOptions } from '../auth-request-options';

import * as config from '../config';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class AuthService {
	tokenIdentifier: string = 'bigsmarthome_jwt';
	ws_conn: WebSocket = null;

	constructor(private http: Http) { }

	isLoggedin() {
		if (this.getToken() !== '') {
			return true;
		} else {
			return false;
		}
	}

	getHttpAuthHeader(): any {
		if (!this.isLoggedin()) return {};

		return {
			'Authorization': 'Bearer ' + this.getToken()
		};
	}

	login(username: string, password: string): Promise<any> {
		console.log('trying to login!');
		return this.http.post(config.api.url + "/sessions", {
			username, password
		})
		.toPromise()
		.then(res => (res.json() || {}))
		.then(this.completeLogin.bind(this))
		.catch(this.handleError);
	}

	logout() {
		console.log('logging out user...');
		if (this.isLoggedin()) {
			let options = new AuthRequestOptions(this);

			this.http.delete(config.api.url + "/sessions", options).toPromise();
		}
		
		if (this.ws_conn)
			this.ws_conn.close();
		
		this.deleteToken();

	}

	getWebsocket() {
		if (!this.isLoggedin()) 
			return null;

		if (!this.ws_conn) {
			this.openWebsocket();
		}

		return this.ws_conn;
	}

	// Returns the current jwt, or an empty string if none is set
	private getToken() {
		return localStorage.getItem(this.tokenIdentifier) || '';
	}

	private writeToken(token: string) {
		localStorage.setItem(this.tokenIdentifier, token);
	}

	private deleteToken() {
		localStorage.removeItem(this.tokenIdentifier);
	}

	private completeLogin(response: any) {
		console.log('completing login');
		this.writeToken(response.token);
		return Promise.resolve(true);
	}

	private openWebsocket() {
		this.ws_conn = new WebSocket (config.api.ws);
		let token: string = this.getToken();


		this.ws_conn.onopen = function () {
            console.log('Connected to websocket, sending token');
			console.log("Token to send: " + token);
            this.send(token);
        };

        this.ws_conn.onerror = function (error: any) {
            console.log('Websocket Error: ' + error);
        }

		this.ws_conn.onmessage = function (msg: any) {
			if (msg.data === 'auth')
				console.log("Websocket authenticated.");
		}

		return Promise.resolve(true);
	}

	private handleError(error: Response | any) {
		if (error instanceof Response && error.status === 401) {
			return Promise.resolve(false);
		} else {
			let msg = `Could not log in. (${error.status}, ${error.statusText})`;
			console.error(error);
			return Promise.reject(msg);
		}	
	}
}
