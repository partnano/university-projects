import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';

import { AuthService } from './auth.service';

@Injectable()
export class AuthGuard implements CanActivate {
	constructor(private auth: AuthService, private router: Router) {}

	canActivate() {
		if (this.auth.isLoggedin()) {
			console.log('in AuthGuard, user is logged in.');
			return true;
		} else {
			console.log('in AuthGuard, user is NOT logged in.');
			this.router.navigate(['/login']);
			return false;
		}
	}
}
