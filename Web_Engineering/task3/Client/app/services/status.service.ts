import {Injectable} from '@angular/core';

import { Http, Response } from '@angular/http';
import { AuthService } from './auth.service';
import { AuthRequestOptions } from '../auth-request-options';

import * as config from '../config';

import 'rxjs/add/operator/toPromise';

@Injectable()
export class StatusService {

    constructor(private http: Http, private auth: AuthService) {
    }

    getStatus(): Promise<any> {
        return this.http.get(config.api.url + "/status", new AuthRequestOptions(this.auth))
            .toPromise()
            .then(response => {
                if (response instanceof Response && response.status === 200) {
                    return Promise.resolve(response.json());
                } else {
                    return Promise.reject(false);
                }
            });
    }

}