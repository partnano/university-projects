"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
var auth_request_options_1 = require('../auth-request-options');
var config = require('../config');
require('rxjs/add/operator/map');
require('rxjs/add/operator/toPromise');
var AuthService = (function () {
    function AuthService(http) {
        this.http = http;
        this.tokenIdentifier = 'bigsmarthome_jwt';
        this.ws_conn = null;
    }
    AuthService.prototype.isLoggedin = function () {
        if (this.getToken() !== '') {
            return true;
        }
        else {
            return false;
        }
    };
    AuthService.prototype.getHttpAuthHeader = function () {
        if (!this.isLoggedin())
            return {};
        return {
            'Authorization': 'Bearer ' + this.getToken()
        };
    };
    AuthService.prototype.login = function (username, password) {
        console.log('trying to login!');
        return this.http.post(config.api.url + "/sessions", {
            username: username, password: password
        })
            .toPromise()
            .then(function (res) { return (res.json() || {}); })
            .then(this.completeLogin.bind(this))
            .catch(this.handleError);
    };
    AuthService.prototype.logout = function () {
        console.log('logging out user...');
        if (this.isLoggedin()) {
            var options = new auth_request_options_1.AuthRequestOptions(this);
            this.http.delete(config.api.url + "/sessions", options).toPromise();
        }
        if (this.ws_conn)
            this.ws_conn.close();
        this.deleteToken();
    };
    AuthService.prototype.getWebsocket = function () {
        if (!this.isLoggedin())
            return null;
        if (!this.ws_conn) {
            this.openWebsocket();
        }
        return this.ws_conn;
    };
    // Returns the current jwt, or an empty string if none is set
    AuthService.prototype.getToken = function () {
        return localStorage.getItem(this.tokenIdentifier) || '';
    };
    AuthService.prototype.writeToken = function (token) {
        localStorage.setItem(this.tokenIdentifier, token);
    };
    AuthService.prototype.deleteToken = function () {
        localStorage.removeItem(this.tokenIdentifier);
    };
    AuthService.prototype.completeLogin = function (response) {
        console.log('completing login');
        this.writeToken(response.token);
        return Promise.resolve(true);
    };
    AuthService.prototype.openWebsocket = function () {
        this.ws_conn = new WebSocket(config.api.ws);
        var token = this.getToken();
        this.ws_conn.onopen = function () {
            console.log('Connected to websocket, sending token');
            console.log("Token to send: " + token);
            this.send(token);
        };
        this.ws_conn.onerror = function (error) {
            console.log('Websocket Error: ' + error);
        };
        this.ws_conn.onmessage = function (msg) {
            if (msg.data === 'auth')
                console.log("Websocket authenticated.");
        };
        return Promise.resolve(true);
    };
    AuthService.prototype.handleError = function (error) {
        if (error instanceof http_1.Response && error.status === 401) {
            return Promise.resolve(false);
        }
        else {
            var msg = "Could not log in. (" + error.status + ", " + error.statusText + ")";
            console.error(error);
            return Promise.reject(msg);
        }
    };
    AuthService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], AuthService);
    return AuthService;
}());
exports.AuthService = AuthService;
//# sourceMappingURL=auth.service.js.map