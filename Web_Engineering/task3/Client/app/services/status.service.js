"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
var auth_service_1 = require('./auth.service');
var auth_request_options_1 = require('../auth-request-options');
var config = require('../config');
require('rxjs/add/operator/toPromise');
var StatusService = (function () {
    function StatusService(http, auth) {
        this.http = http;
        this.auth = auth;
    }
    StatusService.prototype.getStatus = function () {
        return this.http.get(config.api.url + "/status", new auth_request_options_1.AuthRequestOptions(this.auth))
            .toPromise()
            .then(function (response) {
            if (response instanceof http_1.Response && response.status === 200) {
                return Promise.resolve(response.json());
            }
            else {
                return Promise.reject(false);
            }
        });
    };
    StatusService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http, auth_service_1.AuthService])
    ], StatusService);
    return StatusService;
}());
exports.StatusService = StatusService;
//# sourceMappingURL=status.service.js.map