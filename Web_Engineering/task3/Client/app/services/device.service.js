"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
var auth_service_1 = require('./auth.service');
var auth_request_options_1 = require('../auth-request-options');
var config = require('../config');
var device_parser_service_1 = require('./device-parser.service');
require('rxjs/add/operator/toPromise');
var DeviceService = (function () {
    function DeviceService(parserService, http, auth) {
        this.parserService = parserService;
        this.http = http;
        this.auth = auth;
    }
    //TODO Sie können dieses Service benutzen, um alle REST-Funktionen für die Smart-Devices zu implementieren
    DeviceService.prototype.getDevices = function () {
        //TODO Lesen Sie die Geräte über die REST-Schnittstelle aus
        /*
         * Verwenden Sie das DeviceParserService um die via REST ausgelesenen Geräte umzuwandeln.
         * Das Service ist dabei bereits vollständig implementiert und kann wie unten demonstriert eingesetzt werden.
         */
        var _this = this;
        return this.http.get(config.api.url + '/devices', new auth_request_options_1.AuthRequestOptions(this.auth))
            .toPromise()
            .then(function (response) {
            var jsonResponse = response.json();
            var parsedDevices = [];
            if ('devices' in jsonResponse && typeof jsonResponse.devices.length === 'number') {
                var devices = jsonResponse.devices;
                for (var i = 0; i < devices.length; i++) {
                    parsedDevices[i] = _this.parserService.parseDevice(devices[i]);
                }
            }
            return parsedDevices;
        });
    };
    DeviceService.prototype.getDevice = function (id) {
        return this.getDevices()
            .then(function (devices) { return devices.find(function (device) { return device.id === id; }); });
    };
    DeviceService.prototype.createDevice = function (device) {
        var _this = this;
        return this.http.post(config.api.url + '/devices', device, new auth_request_options_1.AuthRequestOptions(this.auth))
            .toPromise()
            .then(function (response) {
            var json = response.json();
            if ("success" in json) {
                var newDev = void 0;
                if ('device' in json) {
                    console.log(json);
                    newDev = _this.parserService.parseDevice(json.device);
                }
                return Promise.resolve(newDev);
            }
        })
            .catch(function (response) {
            if ("success" in response.json()) {
                return Promise.reject("incomplete");
            }
            return Promise.reject("server");
        });
    };
    DeviceService.prototype.removeDevice = function (id) {
        return this.http.delete(config.api.url + '/devices/' + id, new auth_request_options_1.AuthRequestOptions(this.auth))
            .toPromise()
            .then(function (response) {
            if (response instanceof http_1.Response
                && response.status == 200) {
                return true;
            }
            else {
                return false;
            }
        })
            .catch(function (error) {
            if (error instanceof http_1.Response && error.status === 404) {
                return Promise.resolve(true);
            }
            else {
                var msg = "Could not log in. (" + error.status + ", " + error.statusText + ")";
                console.error(error);
                return Promise.reject(msg);
            }
        });
    };
    DeviceService.prototype.editDeviceName = function (id, display_name) {
        return this.http.put(config.api.url + '/devices/' + id, { display_name: display_name }, new auth_request_options_1.AuthRequestOptions(this.auth))
            .toPromise()
            .then(function (response) {
            if (response instanceof http_1.Response && response.status === 200) {
                return Promise.resolve(true);
            }
            else {
                return Promise.resolve(false);
            }
        });
    };
    DeviceService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [device_parser_service_1.DeviceParserService, http_1.Http, auth_service_1.AuthService])
    ], DeviceService);
    return DeviceService;
}());
exports.DeviceService = DeviceService;
//# sourceMappingURL=device.service.js.map