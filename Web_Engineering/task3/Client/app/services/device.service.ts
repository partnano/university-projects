import {Device} from '../model/device';
import {Injectable} from '@angular/core';

import { Http, Response } from '@angular/http';
import { AuthService } from './auth.service';
import { AuthRequestOptions } from '../auth-request-options';

import * as config from '../config';

import {DEVICES} from '../resources/mock-device';
import {DeviceParserService} from './device-parser.service';

import 'rxjs/add/operator/toPromise';

import {DevicesComponent} from '../components/devices.component';


@Injectable()
export class DeviceService {

    constructor(private parserService: DeviceParserService, private http: Http, private auth: AuthService) {
    }

    //TODO Sie können dieses Service benutzen, um alle REST-Funktionen für die Smart-Devices zu implementieren

    getDevices(): Promise<Device[]> {
        //TODO Lesen Sie die Geräte über die REST-Schnittstelle aus
        /*
         * Verwenden Sie das DeviceParserService um die via REST ausgelesenen Geräte umzuwandeln.
         * Das Service ist dabei bereits vollständig implementiert und kann wie unten demonstriert eingesetzt werden.
         */

        return this.http.get(config.api.url + '/devices', new AuthRequestOptions(this.auth))
                .toPromise()
                .then(response => {
                    let jsonResponse = response.json();
                    let parsedDevices: Device[] = [];

                    if ('devices' in jsonResponse && typeof jsonResponse.devices.length === 'number') {
                        let devices = jsonResponse.devices;
                        for (let i = 0; i < devices.length; i++) {
                            parsedDevices[i] = this.parserService.parseDevice(devices[i]);
                        }
                    }

                    return parsedDevices;
                });
    }

    getDevice(id: string): Promise<Device> {
        return this.getDevices()
            .then(devices => devices.find(device => device.id === id));
    }

    createDevice(device: any): Promise<Device> {
        return this.http.post(config.api.url + '/devices', device, new AuthRequestOptions(this.auth))
            .toPromise()
            .then(response => {
                let json = response.json();
                if ("success" in json) {
                    let newDev: Device;
                    if ('device' in json) {
                        console.log(json);
                        newDev = this.parserService.parseDevice(json.device);
                    }
                    return Promise.resolve(newDev);
                }

            })
            .catch(response => {
                if ("success" in response.json()) {
                    return Promise.reject("incomplete");
                }
                return Promise.reject("server");
            });
    }

    removeDevice(id: string): Promise<Boolean | any> {
        return this.http.delete(config.api.url + '/devices/' + id, new AuthRequestOptions(this.auth))
            .toPromise()
            .then(response => {
                if (response instanceof Response 
                && response.status == 200) {
                    return true;
                } else {
                    return false;
                }
            })
            .catch(error => {
                if (error instanceof Response && error.status === 404) {
                    return Promise.resolve(true);
                } else {
                    let msg = `Could not log in. (${error.status}, ${error.statusText})`;
                    console.error(error);
                    return Promise.reject(msg);
                }
            });
    }

    editDeviceName(id: string, display_name: string): Promise<Boolean | any> {
        return this.http.put(config.api.url + '/devices/' + id, { display_name }, new AuthRequestOptions(this.auth))
            .toPromise()
            .then(response => {
               if (response instanceof Response && response.status === 200) {
                   return Promise.resolve(true);
               } else {
                   return Promise.resolve(false);
               }
            });
    }

}
