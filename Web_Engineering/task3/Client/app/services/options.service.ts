import {Injectable} from '@angular/core';

import { Http, Response } from '@angular/http';
import { AuthService } from './auth.service';
import { AuthRequestOptions } from '../auth-request-options';

import * as config from '../config';

import 'rxjs/add/operator/toPromise';

@Injectable()
export class OptionsService {

    constructor(private http: Http, private auth: AuthService) {
    }

    changePassword(input: any): Promise<number> {
        return this.http.put(config.api.url + "/options", input, new AuthRequestOptions(this.auth))
            .toPromise()
            .then(response => {
                if (response instanceof Response) {
                    return Promise.resolve(response.status);
                }
            });
    }

}