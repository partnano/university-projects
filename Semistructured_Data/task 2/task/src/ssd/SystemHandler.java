package ssd;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.*;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.HashMap;

/**
 * TODO: Implement this content handler.
 */
public class SystemHandler extends DefaultHandler {
	/**
	 * Use this xPath variable to create xPath expression that can be
	 * evaluated over the XML document.
	 */
	private static XPath xPath = XPathFactory.newInstance().newXPath();
	
	/**
	 * Store and manipulate the system XML document here.
	 */
	private Document systemDoc;
	
	/**
	 * This variable stores the text content of XML Elements.
	 */
	private String eleText;

	/**
	 * Insert local variables here
	 */
	private HashMap<String, String> current_data = new HashMap<>(4);

    public SystemHandler(Document doc) {
    	systemDoc = doc;
    }

    /**
     * 
     * Return the current stored system document
     * 
     * @return XML Document
     */
	public Document getDocument() {
		return systemDoc;
	}
    
    //***TODO***
	//Specify additional methods to parse the bid document and modify the systemDoc

    @Override
	public void startElement(String namespaceURI, String localName,
							 String qualifiedName, Attributes atts) throws SAXException
	{
		System.out.println("start: " + qualifiedName);
        System.out.println("  attributes: ");

        if (qualifiedName.equals("mod"))
            return;

        current_data.put("element", qualifiedName);

        for (int i = 0; i < atts.getLength(); i++)
        {
            System.out.println("    " + atts.getQName(i) + ": " + atts.getValue(i));
            current_data.put(atts.getQName(i), atts.getValue(i));
        }
	}

    @Override
    public void characters(char[] text, int start, int length) throws SAXException
    {
        eleText = new String(text, start, length);
        System.out.println("char: " + eleText);

        current_data.put("value", eleText);
    }

    // fun fun fun

    @Override
    public void endElement(String namespaceURI, String localName,
						   String qualifiedName) throws SAXException
	{
		System.out.println("end: " + qualifiedName);

		if (qualifiedName.equals("mod"))
		    return;

		if (!qualifiedName.equals(current_data.get("element")))
		    throw new SAXException("StartElement != EndElement");

		// manipulate document

        // get slot to work in
        NodeList slots = systemDoc.getElementsByTagName("slot");
        Element slot = null;

        for (int i = 0; i < slots.getLength(); i++)
        {
            Element test = (Element) slots.item(i);

            if (test.getAttribute("id").equals(current_data.get("slot")))
                slot = test;
        }

        if (slot == null)
            throw new SAXException("Slot to modify does not exist! (" + current_data.get("slot") + ")");

        // find and manipulate

        NodeList slot_children = slot.getChildNodes();
        Element ioElement = null;

        for (int i = 0; i < slot_children.getLength(); i++)
        {
            if (slot_children.item(i).getNodeName().equals(qualifiedName))
                ioElement = (Element) slot_children.item(i);
        }

        if (ioElement == null)
            createNewIONode(slot, qualifiedName);

        else
        {
            NodeList io_children = ioElement.getChildNodes();
            Element item = null;

            for (int i = 0; i < io_children.getLength(); i++)
            {
                if (io_children.item(i).getNodeName().equals("item"))
                {
                    Element test = (Element) io_children.item(i);
                    if (test.getAttribute("id").equals(current_data.get("item")))
                        item = (Element) io_children.item(i);
                }
            }

            if (item == null)
            {
                createNewItem(ioElement);
                return;
            }

            if (current_data.containsKey("store"))
                item.setAttribute("store", current_data.get("store"));

            item.getChildNodes().item(0).setNodeValue(current_data.get("value"));
        }

        // done
	}

	// PRIVATE HELPERS

	private void createNewIONode (Element slot, String qualifiedName)
    {
        // create new input / output node
        Element ioElement = systemDoc.createElement(qualifiedName);

        createNewItem(ioElement);

        // and append to slot
        if (qualifiedName.equals("input"))
            slot.insertBefore(ioElement, slot.getFirstChild());

        else // output
            slot.appendChild(ioElement);
    }

    private void createNewItem (Element io)
    {
        Element item = systemDoc.createElement("item");
        item.setAttribute("id", current_data.get("item"));

        if (current_data.containsKey("store"))
            item.setAttribute("store", current_data.get("store"));

        Text itemText = systemDoc.createTextNode(current_data.get("value"));

        item.appendChild(itemText);
        io.appendChild(item);
    }
}

