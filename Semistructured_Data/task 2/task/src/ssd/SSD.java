package ssd;

import java.io.File;
import java.io.FileInputStream;
import java.net.URL;
import java.util.List;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

public class SSD {
    private static DocumentBuilderFactory documentBuilderFactory;
    private static DocumentBuilder documentBuilder;

	public static void main(String[] args) throws Exception {
		if (args.length != 3) {
            System.err.println("Usage: java SSD <input.xml> <mod-slot.xml> <output.xml>");
            System.exit(1);
        }
	
		String inputPath = args[0];
		String modPath = args[1];
		String outputPath = args[2];
    
       initialize();
       transform(inputPath, modPath, outputPath);
    }
	
	private static void initialize() throws Exception {    
       documentBuilderFactory = DocumentBuilderFactory.newInstance();
       documentBuilder = documentBuilderFactory.newDocumentBuilder();
    }
	
	/**
     * Use this method to encapsulate the main logic for this example. 
     * 
     * First read in the system document. 
     * Second create a SystemHandler and an XMLReader (SAX parser)
     * Third parse the mod-slot document
     * Last get the Document from the SystemHandler and use a
     *    Transformer to print the document to the output path.
     * 
     * @param inputPath Path to the xml file to get read in.
     * @param modPath Path to the mod-slot xml file
     * @param outputPath Path to the output xml file.
     */
    private static void transform(String inputPath, String modPath, String outputPath) throws Exception
    {
        // Read in the data from the system xml document, created in example 1
        Document doc = documentBuilder.parse(inputPath);

        // Create an input source for the mod-slot document
        SystemHandler sys_handler = new SystemHandler(doc);
        XMLReader parser = XMLReaderFactory.createXMLReader();
        parser.setContentHandler(sys_handler);

        // start the actual parsing
        parser.parse(modPath);

        // Validate file before storing
        File schemaFile = new File (System.getProperty("user.dir") + "/resources/system.xsd");
        SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        Schema schema = factory.newSchema(schemaFile);
        Validator validator = schema.newValidator();

        // get the document from the SystemHandler
        Document new_doc = sys_handler.getDocument();

        // validate
        DOMSource dom_source = new DOMSource(new_doc);
        validator.validate(dom_source);

        //store the document
        TransformerFactory tfac = TransformerFactory.newInstance();
        Transformer transformer = tfac.newTransformer();

        StreamResult result = new StreamResult(new File(outputPath));
        transformer.transform(dom_source, result);

        System.out.println("File saved.");
    }

    /**
     * Prints an error message and exits with return code 1
     * 
     * @param message The error message to be printed
     */
    public static void exit(String message) {
    	System.err.println(message);
    	System.exit(1);
    }
}
