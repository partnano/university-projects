<bigareas> &#xa;
{
    for $x in system/area
    where count($x/*) > 2
    order by $x/@name
    return 
        <area name = "{$x/@name}"> &#xa;
        {
            for $y in $x/slot
            where sum($y//*/cost) > 10
            return
                <slot id = "{$y/@id}"> &#xa;
                        {
                            if ($y/@parallel="true") 
                            then <time> {max($y//*/time)} </time>
                            else <time> {sum($y//*/time)} </time>
                        } &#xa; 
                    <cost> {sum($y//*/cost)} </cost> &#xa;
                </slot>
        }
        &#xa;</area>
}
&#xa;</bigareas>