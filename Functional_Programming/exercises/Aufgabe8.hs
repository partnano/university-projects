import Data.List

-- [ EX 1 ] --

data GeladenerGast = A | B | C | D | E | F | G | H | I | J | K | L | M
                   | N | O | P | Q | R | S | T deriving (Eq,Ord,Enum,Show)

type Schickeria = [GeladenerGast]
type Adabeis    = [GeladenerGast]
type Nidabeis   = [GeladenerGast]
type NimmtTeil  = GeladenerGast -> Bool
type Kennt      = GeladenerGast -> GeladenerGast -> Bool

istSchickeriaEvent :: NimmtTeil -> Kennt -> Bool
istSchickeriaEvent atts knows = length (schickeria atts knows) > 0

istSuperSchick :: NimmtTeil -> Kennt -> Bool
istSuperSchick atts knows = length (adabeis atts knows) == 0

istVollProllig :: NimmtTeil -> Kennt -> Bool
istVollProllig atts knows = length (schickeria atts knows) == 0

schickeria :: NimmtTeil -> Kennt -> Schickeria
schickeria atts knows = _schickeria atts knows $ _knownAtts atts knows

_knownAtts :: NimmtTeil -> Kennt -> [GeladenerGast]
_knownAtts atts knows = [x | x <- [A ..], atts x, all (\(a, b) -> knows b a) ([(x, y) | y <- [A ..], atts y])]

_schickeria :: NimmtTeil -> Kennt -> [GeladenerGast] -> [GeladenerGast]
_schickeria atts knows peeps = [x | x <- peeps, all (\(a, b) -> not $ knows a b) ([(x, y) | y <- [A ..] \\ peeps, atts y])]

adabeis :: NimmtTeil -> Kennt -> Adabeis
adabeis atts knows = [x | x <- [A ..], atts x] \\ (schickeria atts knows)

nidabeis :: NimmtTeil -> Kennt -> Nidabeis
nidabeis atts knows = [x | x <- [A ..], not $ atts x]

-- [ EX 2 ] -- 

stream :: [Integer]
stream = 1 : _stream [2, 3, 4, 5]

_stream :: [Integer] -> [Integer]
_stream (l : ls) = l : _stream (sort $ nub ([l*2, l*3, l*5] ++ ls))

-- [ EX 3 ] --

type Quadrupel = (Integer,Integer,Integer,Integer)

quadrupel :: Integer -> [Quadrupel]
quadrupel n
  | n <= 0 = []
  | otherwise = fil $ gen n

gen :: Integer -> [Quadrupel]
gen n = [(a, b, c, d) | a <- [1..n], b <- [1..n], c <- [1..n], d <- [1..n], a <= b, a < c, c <= d]

fil :: [Quadrupel] -> [Quadrupel]
fil ls = [(a, b, c, d) | (a, b, c, d) <- ls, (a^3) + (b^3) == (c^3) + (d^3)]

sel :: Int -> [Quadrupel] -> Quadrupel
sel n ls
  | (abs n) >= (length ls) = error "bad index"
  | otherwise = ls !! (abs n)

main = do
  print ("'Don't fall asleep yet. Contrary to popular belief, that's not where dreams get accomplished.' - George Watsky")
