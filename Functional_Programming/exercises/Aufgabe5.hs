-- Exercise 5

data Digit = Zero | One | Two
type Digits = [Digit]
data Sign = Pos | Neg
newtype Numeral = Num (Sign, Digits)

----- [ Begin Ex3 ] -----

canonize :: Numeral -> Numeral
canonize (Num (s, ds)) =
  let ds_wo0 = dropWhile (== Zero) ds in 
  if null ds_wo0 then Num (Pos, [Zero])
  else Num (s, ds_wo0)
  
int2num :: Integer -> Numeral
int2num i =
  let sig = if (i < 0) then Neg else Pos
  in canonize (Num (sig, (transTo3 (abs i) [])))

transTo3 :: Integer -> Digits -> Digits
transTo3 0 [] = [Zero]
transTo3 i ds
  | i == 0    = ds
  | otherwise = do let nd = (case (mod i 3) of
                               0 -> Zero
                               1 -> One
                               2 -> Two) in
                     transTo3 (div i 3) ([nd] ++ ds)

num2int :: Numeral -> Integer
num2Int (Num (_, [])) = error "Invalid Argument"
num2int (Num (s, ds))
  | s == Pos  = transFrom3 0 0 ds
  | otherwise = (-1) * (transFrom3 0 0 ds)

transFrom3 :: Integer -> Integer -> Digits -> Integer
transFrom3 i k ds
  | length ds == 0 = i
  | otherwise      =
      let lds = (digit2int (last ds))
          sds = (init ds)
      in
        transFrom3 (i +((lds) *(3^k))) (k +1) sds
                
digit2int :: Digit -> Integer
digit2int d
  | d == Zero = 0
  | d == One  = 1
  | otherwise = 2

----- [ End Ex3 ] -----

-- a) Eq

instance Eq Digit where
  Zero == Zero = True
  One  == One  = True
  Two  == Two  = True
  _    == _    = False

instance Eq Sign where
  Pos == Pos = True
  Neg == Neg = True
  _   == _   = False

instance Eq Numeral where
  n1 == n2 = compare_nums (canonize n1) (canonize n2)

compare_nums :: Numeral -> Numeral -> Bool
compare_nums (Num (s1, d1)) (Num (s2, d2))
  | s1 == s2 && d1 == d2 = True
  | otherwise            = False

-- b) Show

instance Show Digit where
  show Zero = "0"
  show One  = "1"
  show Two  = "2"

instance Show Sign where
  show Pos = "+"
  show Neg = "-"

instance Show Numeral where
  show = showNum
  
showDigs :: Digits -> String
showDigs [] = ""
showDigs (dig:digs) = (show dig) ++ (showDigs digs)
  
showNum :: Numeral -> String
showNum _t =
  let t@(Num (sig, digs)) = canonize _t
  in (show sig) ++ (showDigs digs)
    
-- c) Ord

instance Ord Digit where
  Zero <= Zero = True
  Zero <= One  = True
  Zero <= Two  = True
  One  <= One  = True
  One  <= Two  = True
  Two  <= Two  = True
  _    <= _    = False

instance Ord Numeral where
  compare n1 n2 = compNums (canonize n1) (canonize n2)

compNums :: Numeral -> Numeral -> Ordering
compNums (Num (Pos, _)) (Num (Neg, _)) = GT
compNums (Num (Neg, _)) (Num (Pos, _)) = LT
compNums n1@(Num (s1, d1)) n2@(Num (s2, d2)) =
    if      n1 == n2               then EQ
    else if s1 == Pos && s2 == Pos then compDigs d1 d2
    else if (compDigs d1 d2) == LT then GT else LT
    
compDigs :: Digits -> Digits -> Ordering
compDigs d1 d2
  | length d1 < length d2 = LT
  | length d2 < length d1 = GT
  | head d1   < head d2   = LT
  | head d2   < head d1   = GT
  | otherwise             = compDigs (tail d1) (tail d2)

-- d) Num implementations

instance Num Numeral where
  n1 + n2           = int2num ((num2int n1) + (num2int n2))
  n1 - n2           = int2num ((num2int n1) - (num2int n2)) 
  n1 * n2           = int2num ((num2int n1) * (num2int n2))
  abs (Num (_, ds)) = canonize (Num (Pos, ds))
  signum _n         = let n@(Num (s, d)) = canonize _n
                      in if s == Pos && d == [Zero] then (Num (Pos, [Zero]))
                         else if s == Pos then (Num (Pos, [One])) else (Num (Neg, [One]))
  fromInteger n     = int2num n
  negate _n         = let n@(Num (s, ds)) = canonize _n
                      in if s == Pos then (Num (Neg, ds)) else (Num (Pos, ds))

-- testing (not in final file)

main = do
  print ("You made me swallow my gum! That's going to be in my digestive tract for seven years!")
