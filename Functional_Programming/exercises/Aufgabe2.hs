-- 1
-- factorial list

-- base functions, takes initial argument an checks for any mischievings
facLst :: Integer -> [Integer]
facLst n
  | n < 0    = []
  | n == 0   = [1]
  | otherwise = facLstCalc n 1 [1]

-- calc factorials and put them one by one into the list,
-- while increasing counter variable (k)
facLstCalc :: Integer -> Integer -> [Integer] -> [Integer]
facLstCalc n k lst
  | n == k -1 = lst
  | otherwise = facLstCalc n (k +1) (lst ++ [last lst *k])

-- take a facLst and reverse it
factsL :: Integer -> [Integer]
factsL n = reverse (facLst n)

-- 2
-- extract numerals
-- using the span and break functions
-- via span get the part that is a number and add to list
-- via break break away the nonnumber part and call extractNumerals anew
extractNumerals :: String -> [String]
extractNumerals s
  | length s >= 1 =
    let
      brk = span (checkDigit) s
    in
      filter (not . null) ([fst brk]) ++ extractNumerals (snd (break (checkDigit) (snd brk)))

  | otherwise = []

checkDigit :: Char -> Bool
checkDigit c = elem c "0123456789"

-- 3
-- is power of 2?

-- if 0 or not divisible by 2 -> -1
-- otherwise call isPowOf2Calc
isPowOf2 :: Int -> (Bool, Int)
isPowOf2 n
  | n == 0       = (False, (-1))
  | n == 1       = (True, 0)
  | mod n 2 == 1 = (False, (-1))
  | otherwise    = isPowOf2Calc (div n 2) 1

-- if n == 1, then the function is finished
-- if n is at some point not divisble by 2 anymore -> -1
isPowOf2Calc :: Int -> Int -> (Bool, Int)
isPowOf2Calc n m
  | n == 1       = (True, m)
  | mod n 2 == 1 = (False, (-1))
  | otherwise    = isPowOf2Calc (div n 2) (m +1)

-- maps sL2pO2 on every list element
sL2pO2 :: [String] -> [Int]
sL2pO2 strL = map (sL2pO2Calc) strL

-- calls isPowOf2 on list element
-- checks beforehand if it's valid
sL2pO2Calc :: String -> Int
sL2pO2Calc s = snd (isPowOf2 (checkStrToInt s))

-- checks every digit for validity (is number?)
checkStrToInt :: String -> Int
checkStrToInt s
  | all checkDigit s = read s :: Int
  | otherwise        = -1

-- 4
-- currying and uncurrying, yaya
curry3 :: ((a, b, c) -> d) -> a -> b -> c -> d
curry3 f x y z = f (x, y, z)

uncurry3 :: (a -> b -> c -> d) -> (a, b, c) -> d
uncurry3 f (x, y, z) = f x y z

curry4 :: ((a, b, c, d) -> e) -> a -> b -> c -> d -> e
curry4 f w x y z = f (w, x, y, z)

uncurry4 :: (a -> b -> c -> d -> e) -> (a, b, c, d) -> e
uncurry4 f (w, x, y, z) = f w x y z

main = do
  print (facLst 6)
  print (factsL 5)
  print (extractNumerals "a123")
  print (isPowOf2 8)
  print (sL2pO2 ["-32", "5", "2", "64"])
