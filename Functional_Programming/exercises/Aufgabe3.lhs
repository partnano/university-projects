Defining the datatypes given for this exercise.

\begin{code}

data Digit  = Zero | One | Two deriving (Eq, Enum, Show)
type Digits = [Digit]
data Sign   = Pos | Neg deriving (Eq, Show)

newtype Numeral = Num (Sign, Digits) deriving (Eq, Show)

\end{code}

--[ 1 ]--

Canonicalizing a number (no front zeroes and zero displayed as +0)

\begin{code}

canonize :: Numeral -> Numeral
canonize (Num (s, ds)) =
  let ds_wo0 = dropWhile (== Zero) ds in 
  if null ds_wo0 then Num (Pos, [Zero])
  else Num (s, ds_wo0)

\end{code}

Converting integers to a numerals.
First detects the sign and then continues with transforming into the tertiary system.
Transforming is basically diving the number by 3 until it's zero, while
detecting the division rest every step.

\begin{code}

int2num :: Integer -> Numeral
int2num i =
  let sig = if (i < 0) then Neg else Pos
  in canonize (Num (sig, (transTo3 (abs i) [])))

transTo3 :: Integer -> Digits -> Digits
transTo3 0 [] = [Zero]
transTo3 i ds
  | i == 0    = ds
  | otherwise = do let nd = (case (mod i 3) of
                               0 -> Zero
                               1 -> One
                               2 -> Two) in
                     transTo3 (div i 3) ([nd] ++ ds)

\end{code}

Converting numerals to integers.
Distinguishes between positive and negative and then transforms [digits]
into a number one by one. 

\begin{code}

num2int :: Numeral -> Integer
num2Int (Num (_, [])) = error "Invalid Argument"
num2int (Num (s, ds))
  | s == Pos  = transFrom3 0 0 ds
  | otherwise = (-1) * (transFrom3 0 0 ds)

transFrom3 :: Integer -> Integer -> Digits -> Integer
transFrom3 i k ds
  | length ds == 0 = i
  | otherwise      =
      let lds = (digit2int (last ds))
          sds = (init ds)
      in
        transFrom3 (i +((lds) *(3^k))) (k +1) sds
                
digit2int :: Digit -> Integer
digit2int d
  | d == Zero = 0
  | d == One  = 1
  | otherwise = 2
                
\end{code}

--[ 2 ]--

Incrementing a numeral

\begin{code}

inc :: Numeral -> Numeral
inc (Num (_, [])) = error "Invalid Argument"
inc (Num (is, ids)) =
  let (Num (s, ds)) = canonize (Num (is, ids))
  in
    if s == Pos then (Num (s, (incCalc ds [] True)))
    else             canonize (Num (s, (decCalc ds [] True))) 

incCalc :: Digits -> Digits -> Bool -> Digits
incCalc ds nds add
  | length ds == 0 && add == False = nds
  | length ds == 0 && add == True = [One] ++ nds
  | otherwise =
      let
        lds = (last ds)
        sds = (init ds)
      in
        if      lds == Zero && add == True  then incCalc sds ([One]  ++ nds) False
        else if lds == One  && add == True  then incCalc sds ([Two]  ++ nds) False
        else if lds == Two  && add == True  then incCalc sds ([Zero] ++ nds) True
        else if lds == Zero && add == False then incCalc sds ([Zero] ++ nds) False
        else if lds == One  && add == False then incCalc sds ([One]  ++ nds) False
        else                                     incCalc sds ([Two]  ++ nds) False


\end{code}

Decrementing a numeral

\begin{code}

dec :: Numeral -> Numeral
dec (Num (_, [])) = error "Invalid Argument"
dec (Num (is, ids)) =
  let (Num (s, ds)) = canonize (Num (is, ids))
  in
    if s == Pos && ds == [Zero] then (Num (Neg, [One]))
    else if s == Pos            then canonize (Num (s, (decCalc ds [] True)))
    else                             (Num (s, (incCalc ds [] True)))

decCalc :: Digits -> Digits -> Bool -> Digits
decCalc ds nds rem
  | length ds == 0 && rem == False = nds
  | otherwise =
      let
        lds = (last ds)
        sds = (init ds)
      in
        if      lds == Zero && rem == True  then decCalc sds ([Two]  ++ nds) True
        else if lds == One  && rem == True  then decCalc sds ([Zero] ++ nds) False
        else if lds == Two  && rem == True  then decCalc sds ([One]  ++ nds) False
        else if lds == Zero && rem == False then decCalc sds ([Zero] ++ nds) False
        else if lds == One  && rem == False then decCalc sds ([One]  ++ nds) False
        else                                     decCalc sds ([Two]  ++ nds) False


\end{code}

--[ 3 ]--

Addition and Multiplication of numerals.
Basically playing around with inc/dec'ing the right numeral
while diminishing the second numeral to zero to finish the recursion

Multiplication does mostly the same thing.
It adds the first numeral with each other until the second one is zero.

\begin{code}

numAdd :: Numeral -> Numeral -> Numeral
numAdd (Num (_, [])) _ = error "Invalid Argument"
numAdd _ (Num (_, [])) = error "Invalid Argument"
numAdd (Num (is1, ids1)) (Num (is2, ids2)) =
  let
    (Num (s1, ds1)) = canonize (Num (is1, ids1))
    (Num (s2, ds2)) = canonize (Num (is2, ids2))
  in
    if      ds2 == [Zero]  then (Num (s1, ds1))
    else if s2 == Pos      then numAdd (inc (Num (s1, ds1))) (dec (Num (s2, ds2)))
    else                        numAdd (dec (Num (s1, ds1))) (inc (Num (s2, ds2)))

numMult :: Numeral -> Numeral -> Numeral
numMult (Num (_, [])) _ = error "Invalid Argument"
numMult _ (Num (_, [])) = error "Invalid Argument"
numMult (Num (is1, ids1)) (Num (is2, ids2)) =
  let
    (Num (s1, ds1)) = canonize (Num (is1, ids1))
    (Num (s2, ds2)) = canonize (Num (is2, ids2))
    sig = if s1 /= s2 then Neg else Pos
  in
    if ds1 == [Zero] || ds2 == [Zero]
         then (Num (Pos, [Zero]))
    else if ds2 == [One]
         then (Num (sig, ds1))
    else if ds1 == [One]
         then (Num (sig, ds2))
    else if s2 == Pos
         then numMult (numAdd (Num (s1, ds1)) (Num (s1, ds1))) (dec (Num (s2, ds2)))
    else
              numMult (numAdd (Num (s1, ds1)) (Num (s1, ds1))) (inc (Num (s2, ds2)))
  
\end{code}

--[ 4 ]--

\begin{code}

curryFlip :: ((a,b) -> c) -> b -> a -> c
curryFlip f x y = f (y,x)

uncurryFlip :: (a -> b -> c) -> (b,a) -> c
uncurryFlip f (x,y) = f y x

pairFlip :: ((a,b) -> c) -> (b,a) -> c
pairFlip f (x,y) = f (y,x)

\end{code}

Testing 4...

\begin{code}

curryFlipTest :: (Integer, String) -> Bool
curryFlipTest (i, s) = True

uncurryFlipTest :: Integer -> (String -> Bool)
uncurryFlipTest i s = True

pairFlipTest :: (Integer, String) -> Bool
pairFlipTest (i, s) = True

main = do
  let cf = (curryFlip curryFlipTest)
      uf = (uncurryFlip uncurryFlipTest)
      pf = (pairFlip pairFlipTest) in do
          print (cf "Str"   10)
          print (uf ("Str", 10))
          print (pf ("Str", 10))

\end{code}
