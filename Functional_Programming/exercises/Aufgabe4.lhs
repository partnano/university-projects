--[ 1 ]--

A simple tree system!

\begin{code}

data Tree a = Nil | Node a (Tree a) (Tree a) deriving (Eq, Ord, Show)
data Order  = Up | Down deriving (Eq, Show)

nil :: Tree a
nil = Nil

isNilTree :: Tree a -> Bool
isNilTree Nil = True
isNilTree _   = False

isNodeTree :: Tree a -> Bool
isNodeTree tree = not (isNilTree tree)

msg_nil = "Empty Tree as Argument"

leftSubTree :: Tree a -> Tree a
leftSubTree Nil = error msg_nil
leftSubTree (Node _ tl _) = tl

rightSubTree :: Tree a -> Tree a
rightSubTree Nil = error msg_nil
rightSubTree (Node _ _ tr) = tr

treeValue :: Tree a -> a
treeValue Nil = error msg_nil
treeValue (Node v _ _) = v

isValueOf :: Eq a => a -> Tree a -> Bool
isValueOf _ Nil = False
isValueOf searched_v (Node v tl tr) = (searched_v == v)
                                      || (isValueOf searched_v tl)
                                      || (isValueOf searched_v tr)

isOrderedTree :: Ord a => Tree a -> Bool
isOrderedTree Nil = True
isOrderedTree (Node v tl tr) = (iOTMax v tl) && (iOTMin v tr)

iOTMax :: Ord a => a -> Tree a -> Bool
iOTMax _ Nil = True
iOTMax max (Node v tl tr) = (v < max) && (iOTRange v max tr) && (iOTMax v tl)

iOTMin :: Ord a => a -> Tree a -> Bool
iOTMin _ Nil = True
iOTMin min (Node v tl tr) = (v > min) && (iOTRange min v tl) && (iOTMin v tr)

iOTRange :: Ord a => a -> a -> Tree a -> Bool
iOTRange _ _ Nil = True
iOTRange min max (Node v tl tr) = (min < v && v < max) && (iOTRange v min tl) && (iOTRange v max tr)

msg_order = "Argument Tree not Ordered"

insert :: Ord a => a -> Tree a -> Tree a
insert ins Nil = (Node ins Nil Nil)
insert ins tree@(Node v tl tr)
  | not (isOrderedTree tree) = error msg_order
  | ins == v                 = (Node v tl tr)
  | ins  > v                 = (Node v tl (insert ins tr))
  | ins  < v                 = (Node v (insert ins tl) tr)

delete :: Ord a => a -> Tree a -> Tree a
delete _ Nil = Nil
delete del tree@(Node v tl tr)
  | not (isOrderedTree tree) = error msg_order
  | del < v = if isNilTree tl then tree else (Node v (delete del tl) tr)
  | del > v = if isNilTree tr then tree else (Node v tl (delete del tr))
  | del == v && isNilTree tl && isNilTree tr = Nil
  | del == v && isNilTree tl = tr
  | del == v && isNilTree tr = tl
  | otherwise = let r = maxValue tl in (Node r (delete r tl) tr)

maxValue :: Ord a => Tree a -> a
maxValue (Node v _ tr)
  | isNodeTree tr = maxValue tr
  | otherwise     = v

flatten :: Ord a => Order -> Tree a -> [a]
flatten _ Nil = []
flatten o tree@(Node v tl tr)
  | not (isOrderedTree tree) = error msg_order
  | o == Up   = (flatten o tl) ++ [v] ++ (flatten o tr)
  | o == Down = (flatten o tr) ++ [v] ++ (flatten o tl)

\end{code}

--[ 2 ]--

\begin{code}

maxLength :: Tree a -> Int
maxLength Nil = 0
maxLength tree@(Node v tl tr)
  | leafCheck tree = 0
  | otherwise      = 1 + (max (maxLength tl) (maxLength tr))

minLength :: Tree a -> Int
minLength Nil = 0
minLength tree@(Node v tl tr)
  | leafCheck tree = 0
  | otherwise      = 1 + (min (minLength tl) (minLength tr))

leafCheck :: Tree a -> Bool
leafCheck (Node _ Nil Nil) = True
leafCheck _                = False

balancedDegree :: Tree a -> Int
balancedDegree tree = (maxLength tree) - (minLength tree)

\end{code}

--[ Tests (not in final file) ]--

\begin{code}

main = do print ("When I'm around you, I kind of feel like I'm on drugs. Not that I do drugs. Unless you do drugs, in which case I do them all the time. All of them.")

\end{code}
