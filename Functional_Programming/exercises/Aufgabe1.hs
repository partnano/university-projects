-- 1
-- inverse factorial
-- m: the potential factorial
-- k: the base number
-- this basically does m/k -> k+1 until m = 1
facInvCalc :: Integer -> Integer -> Integer
facInvCalc m k
  | m == 1    = k -1
  | m >= k    = facInvCalc (div m k) (k+1)
  | otherwise = -1

facInv :: Integer -> Integer
facInv m
  | m == 1    = -1
  | otherwise = facInvCalc m 2

-- 2
-- extract numbers from string
-- takes s, returns t
extractDigits :: String -> String
extractDigits s = [t | t <- s, (elem t "0123456789")] 

-- 3
-- convert the numbers in a string to an Integer
-- uses above method to extract numbers
convert :: String -> Integer
convert s
  | s == ""   = 0
  | otherwise = read (extractDigits s) :: Integer

-- 4
-- this took me WAY too long and now is probably overly complicated

-- the check if a number is prime
primeCheck :: Integer -> Bool
primeCheck k = null [y | y <- [2..k -1], mod k y == 0] 

-- number of ints in string
numCount :: String -> Int
numCount s = length [t | t <- s, (elem t "0123456789")]

-- business logic
-- cuts out n digits from the left of the integer
-- checks if it's a prime and is the right length
-- if not, retry without leftmost number
-- flm = find left most
flmPrimeImpl :: Integer -> Int -> Int -> Integer
flmPrimeImpl s n l =
  let si = (div s (10 ^(l -n)))
      sh = (mod s (10 ^(l -1)))
  in
    if n > l then 0
    else if div (fromIntegral si) (10^(n-1)) > 0 && primeCheck si
      then si
    else flmPrimeImpl sh n (l -1)
  
-- call flmPrimeImpl
findLeftMostPrime :: String -> Int -> Integer
findLeftMostPrime s n
  | n <= 0    = 0
  | otherwise = flmPrimeImpl (convert s) n (numCount s)
    
-- 5
-- more or less the same as findLeftMostPrime, it just doesn't stop
-- on the first found prime, it only stops when n becomes bigger than l
faPrimesImpl :: Integer -> Int -> Int -> [Integer] -> [Integer]
faPrimesImpl s n l out =
  let si = (div s (10 ^(l -n)))
      sh = (mod s (10 ^(l -1)))
  in
    if n > l then out
    else if div (fromIntegral si) (10^(n-1)) > 0 && primeCheck si && si /= 1
      then faPrimesImpl sh n (l -1) (out ++ [si])
    else faPrimesImpl sh n (l -1) out

findAllPrimes :: String -> Int -> [Integer]
findAllPrimes s n
  | n <= 0    = []
  | otherwise = faPrimesImpl (convert s) n (numCount s) []

main = do
  print (facInv 0)
  print (extractDigits "123abc456ls78sjd9k0")
  print (convert "a5B007p1234L1151248cvK")
  print (findLeftMostPrime "a5B007p1234L1151248cvK" 3)
  print (findAllPrimes "a5B007p1234L1151248cvK" 1)
