-- Testing purposes
import qualified Control.Exception as Exc
import Data.Char

--- [ EX 1 ] ---

data Stack a = Stk [a] | NoStk deriving (Eq, Show)

-- That's apparently a Prelude thing
-- data Maybe a = Just a | Nothing deriving (Eq, Show)

empty :: (Eq a, Show a) => Stack a
empty = (Stk [])

isEmpty :: (Eq a, Show a) => Stack a -> Bool
isEmpty (Stk []) = True
isEmpty _        = False

arg_msg = "Invalid Argument"

top1 :: (Eq a, Show a) => Stack a -> a
top1 NoStk    = error arg_msg
top1 (Stk []) = error arg_msg
top1 (Stk (e:_)) = e

top2 :: (Eq a, Show a) => Stack a -> Maybe a
top2 NoStk    = Nothing
top2 (Stk []) = Nothing
top2 (Stk (e:_)) = Just e

pop :: (Eq a, Show a) => Stack a -> Stack a
pop NoStk    = NoStk
pop (Stk []) = NoStk
pop (Stk (e:es)) = (Stk es)

push :: (Eq a, Show a) => a -> Stack a -> Stack a
push _ NoStk = NoStk
push e (Stk es) = (Stk (e:es))

--- [ EX 2 ] ---

-- ===== older code BEGIN ===== --

data Digit  = Zero | One | Two deriving (Eq, Enum, Show)
type Digits = [Digit]
data Sign   = Pos | Neg deriving (Eq, Show)

newtype Numeral = Num (Sign, Digits) deriving (Eq, Show)

canonize :: Numeral -> Numeral
canonize (Num (s, ds)) =
  let ds_wo0 = dropWhile (== Zero) ds in 
  if null ds_wo0 then Num (Pos, [Zero])
  else Num (s, ds_wo0)

int2num :: Integer -> Numeral
int2num i =
  let sig = if (i < 0) then Neg else Pos
  in canonize (Num (sig, (transTo3 (abs i) [])))

transTo3 :: Integer -> Digits -> Digits
transTo3 0 [] = [Zero]
transTo3 i ds
  | i == 0    = ds
  | otherwise = do let nd = (case (mod i 3) of
                               0 -> Zero
                               1 -> One
                               2 -> Two) in
                     transTo3 (div i 3) ([nd] ++ ds)

num2int :: Numeral -> Integer
num2Int (Num (_, [])) = error "Invalid Argument"
num2int (Num (s, ds))
  | s == Pos  = transFrom3 0 0 ds
  | otherwise = (-1) * (transFrom3 0 0 ds)

transFrom3 :: Integer -> Integer -> Digits -> Integer
transFrom3 i k ds
  | length ds == 0 = i
  | otherwise      =
      let lds = (digit2int (last ds))
          sds = (init ds)
      in
        transFrom3 (i +((lds) *(3^k))) (k +1) sds
                
digit2int :: Digit -> Integer
digit2int d
  | d == Zero = 0
  | d == One  = 1
  | otherwise = 2
                
-- ===== older code END ===== --

data Operator = Plus | Times | Minus deriving (Eq, Show)
data Variable = A | B | C deriving (Eq, Show)
data Expression = Cst Numeral | Var Variable | Exp Expression Expression Operator deriving (Eq, Show)
type State = Variable -> Numeral

eval :: Expression -> State -> Integer
eval (Cst n) s = num2int n
eval (Var n) s = num2int $ s n
eval (Exp e1 e2 op) s
  | op == Plus  = eval e1 s + eval e2 s
  | op == Minus = eval e1 s - eval e2 s
  | op == Times = eval e1 s * eval e2 s
  | otherwise   = error "Unknown error."

--- [ EX 3 ] ---

data CVO = Cop Numeral
           | Vop Char
           | OpPlus
           | OpTimes
           | OpMinus
             deriving (Eq, Show)
type Expr = [CVO]
type State2 = Char -> Numeral

eval2 :: Expr -> State2 -> Integer
eval2 [] _ = error arg_msg
eval2 l s  = _eval2 l s empty

_evst :: Stack Integer -> Operator -> Stack Integer
_evst (Stk []) _  = error "No stack to _evst."
_evst (Stk (l:ls)) op
  | length ls /= 1 = error "Invalid stack size for _evst."
  | op == Plus     = Stk [head ls + l]
  | op == Minus    = Stk [head ls - l]
  | op == Times    = Stk [head ls * l]
  | otherwise      = error "Unknown error in _evst."

_eval2 :: Expr -> State2 -> Stack Integer -> Integer
_eval2 [] _ (Stk [v]) = v
_eval2 [] _ _ = error "Error while calculating result"
_eval2 (Cop l:ls) s r = _eval2 ls s (push (num2int l) r)
_eval2 (Vop l:ls) s r = _eval2 ls s (push (num2int (s l)) r)                        
_eval2 (l:ls) s r
  | l == OpPlus  = _eval2 ls s (_evst r Plus)
  | l == OpMinus = _eval2 ls s (_evst r Minus)
  | l == OpTimes = _eval2 ls s (_evst r Times)

--- [ TESTS (ommitted in finalised file) ] ---

-- main = do
--   print ("The future came and went in the mildly discouraging way that futures do.")
