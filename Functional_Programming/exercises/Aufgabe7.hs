import Prelude hiding (subtract)

data Tree a = Nil | Node a Int (Tree a) (Tree a) deriving (Eq, Ord, Show)
type Multiset a = Tree a
data ThreeValuedBool = TT | FF | Invalid deriving (Eq,Show)
data Order = Up | Down deriving (Eq,Show)

--- [ 1 ] ---

isMultiset :: Ord a => Tree a -> Bool
isMultiset Nil = True
isMultiset t = isOrderedTree t && _checkMS t False

-- Bool input T = canonical, F = non canonical
_checkMS :: Ord a => Tree a -> Bool -> Bool
_checkMS Nil _ = True
_checkMS t@(Node v i l r) c
  | i >  0 && c == True  = _checkMS l c && _checkMS r c
  | i >= 0 && c == False = _checkMS l c && _checkMS r c
  | otherwise            = False

--- [ 2 ] ---

isCanonicalMultiset :: Ord a => Tree a -> Bool
isCanonicalMultiset Nil = True
isCanonicalMultiset t = isOrderedTree t && _checkMS t True

--- [ 3 ] ---

mkMultiset :: (Ord a, Show a) => Tree a -> Multiset a
mkMultiset Nil = Nil
mkMultiset t   = _remove_negative (_mkMS (_simple_flatten t) Nil)

_mkMS :: Ord a => [(a, Int)] -> Multiset a -> Multiset a
_mkMS [] t = t
_mkMS ((i, c):ls) t = _mkMS ls (insert (i, c) t)

_simple_flatten :: Ord a => Tree a -> [(a, Int)]
_simple_flatten Nil = []
_simple_flatten (Node v i tl tr) = [(v, i)] ++ (_simple_flatten tl) ++ (_simple_flatten tr)

_remove_negative :: Ord a => Multiset a -> Multiset a
_remove_negative Nil = Nil
_remove_negative t@(Node v i l r)
  | i < 0     = (Node v 0 (_remove_negative l) (_remove_negative r))
  | otherwise = (Node v i (_remove_negative l) (_remove_negative r))

--- [ 4 ] ---

mkCanonicalMultiset :: (Ord a, Show a) => Tree a -> Multiset a
mkCanonicalMultiset Nil = Nil
mkCanonicalMultiset t   = _tocan (_mkMS (_simple_flatten t) Nil)

_tocan :: Ord a => Multiset a -> Multiset a
_tocan Nil = Nil
_tocan t@(Node v i l r)
  | i <= 0 = delete v (Node v i (_tocan l) (_tocan r))
  | otherwise = (Node v i (_tocan l) (_tocan r))

-- use delete here


--- [ 5 ] ---

-- modified ex4 code
flatten :: (Ord a, Show a) => Order -> Multiset a -> [(a, Int)]
flatten _ Nil = []
flatten o set@(Node v i tl tr)
  | not (isMultiset set) = []
  | o == Up   =
    if i > 0 then
      (flatten o tl) ++ [(v, i)] ++ (flatten o tr)
    else
      (flatten o tl) ++ (flatten o tr)
  | o == Down =
    if i > 0 then
      (flatten o tr) ++ [(v, i)] ++ (flatten o tl)
    else
      (flatten o tr) ++ (flatten o tl)

--- [ 6 ] ---

isElement :: Ord a => a -> Multiset a -> Int
isElement _ Nil = 0
isElement e set@(Node v i l r)
  | not (isMultiset set) = -1
  | e == v               = i
  | e > v                = isElement e r
  | e < v                = isElement e l
  | otherwise            = error "Unknown error in isElement"

--- [ 7 ] ---

isSubset :: (Ord a, Show a) => Multiset a -> Multiset a -> ThreeValuedBool
isSubset m1 m2
  | not $ (isMultiset m1) && (isMultiset m2)            = Invalid
  | (length (flatten Up m1)) > (length (flatten Up m2)) = FF
  | otherwise = if _checkTuples (_simple_flatten m1) (_simple_flatten m2) then TT else FF

-- this checks if the value of a node is in the superset, checks if the amount is <=
-- so we check every single node for a subset, and combine them via && and now know
-- if the whole multiset is a subset
_checkTuples :: Ord a => [(a, Int)] -> [(a, Int)] -> Bool
_checkTuples [] _ = True
_checkTuples ((v, c):l1) l2 = (_checkTuples l1 l2) &&
  (or (zipWith (&&) (map ((== v).fst) l2) (map ((>= c).snd) l2)))

--- [ 8 ] ---

join :: (Ord a, Show a) => Multiset a -> Multiset a -> Multiset a
join m1 m2
  | not $ (isMultiset m1) && (isMultiset m2) = Nil
  | otherwise = _tocan (_mkMS ((_simple_flatten m1) ++ (_simple_flatten m2)) Nil)

--- [ 9 ] ---

meet :: (Ord a, Show a) => Multiset a -> Multiset a -> Multiset a
meet m1 m2
  | not $ (isMultiset m1) && (isMultiset m2) = Nil
  | otherwise                                = _meet (_simple_flatten m1) m2 Nil

_meet :: Ord a => [(a, Int)] -> Multiset a -> Multiset a -> Multiset a
_meet [] m2 new = new
_meet ((v, c):m1s) m2 new =
  let
    el = isElement v m2
    nc = min el c
  in
    if nc == 0 then _meet m1s m2 new
    else            _meet m1s m2 (insert (v, nc) new)

--- [ 10 ] ---

subtract :: (Ord a, Show a) => Multiset a -> Multiset a -> Multiset a
subtract m1 m2
  | not $ (isMultiset m1) && (isMultiset m2) = Nil
  | otherwise                                = _subtract (_simple_flatten m1) m2 Nil

_subtract :: Ord a => [(a, Int)] -> Multiset a -> Multiset a -> Multiset a
_subtract [] m2 new = new
_subtract ((v, c):m1s) m2 new =
  let
    el = isElement v m2
    nc = c - el
  in
    if nc <= 0 then _subtract m1s m2 new
    else            _subtract m1s m2 (insert (v, nc) new)

-- === CODE FROM EX4 (MODIFIED) === --

nil :: Tree a
nil = Nil

isNilTree :: Tree a -> Bool
isNilTree Nil = True
isNilTree _   = False

isNodeTree :: Tree a -> Bool
isNodeTree tree = not (isNilTree tree)

msg_nil = "Empty Tree as Argument"

leftSubTree :: Tree a -> Tree a
leftSubTree Nil = error msg_nil
leftSubTree (Node _ _ tl _) = tl

rightSubTree :: Tree a -> Tree a
rightSubTree Nil = error msg_nil
rightSubTree (Node _ _ _ tr) = tr

treeValue :: Tree a -> a
treeValue Nil = error msg_nil
treeValue (Node v _ _ _) = v

isValueOf :: Eq a => a -> Tree a -> Bool
isValueOf _ Nil = False
isValueOf searched_v (Node v _ tl tr) = (searched_v == v)
                                      || (isValueOf searched_v tl)
                                      || (isValueOf searched_v tr)

isOrderedTree :: Ord a => Tree a -> Bool
isOrderedTree Nil = True
isOrderedTree (Node v _ tl tr) = (iOTMax v tl) && (iOTMin v tr)

iOTMax :: Ord a => a -> Tree a -> Bool
iOTMax _ Nil = True
iOTMax max (Node v _ tl tr) = (v < max) && (iOTRange v max tr) && (iOTMax v tl)

iOTMin :: Ord a => a -> Tree a -> Bool
iOTMin _ Nil = True
iOTMin min (Node v _ tl tr) = (v > min) && (iOTRange min v tl) && (iOTMin v tr)

iOTRange :: Ord a => a -> a -> Tree a -> Bool
iOTRange _ _ Nil = True
iOTRange min max (Node v _ tl tr) = (min < v && v < max) && (iOTRange v min tl) && (iOTRange v max tr)

msg_order = "Argument Tree not Ordered"

insert :: Ord a => (a, Int) -> Tree a -> Tree a
insert (ins, c) Nil = (Node ins c Nil Nil)
insert (ins, c) tree@(Node v i tl tr)
  | not (isOrderedTree tree) = error msg_order
  | ins == v                 = (Node v (i +c) tl tr)
  | ins  > v                 = (Node v i tl (insert (ins, c) tr))
  | ins  < v                 = (Node v i (insert (ins, c) tl) tr)

delete :: Ord a => a -> Tree a -> Tree a
delete _ Nil = Nil
delete del tree@(Node v i tl tr)
  | not (isOrderedTree tree) = error msg_order
  | del < v = if isNilTree tl then tree else (Node v i (delete del tl) tr)
  | del > v = if isNilTree tr then tree else (Node v i tl (delete del tr))
  | del == v && isNilTree tl && isNilTree tr = Nil
  | del == v && isNilTree tl = tr
  | del == v && isNilTree tr = tl
  | otherwise = let (r, ni) = maxValue tl in (Node r ni (delete r tl) tr)

maxValue :: Ord a => Tree a -> (a, Int)
maxValue (Node v i _ tr)
  | isNodeTree tr = maxValue tr
  | otherwise     = (v, i)

--- [ MAIN ] ---

-- main = do
--   print ("We are the Metatron. (The Voice of God. But not the *voice* of God. An entity in its own right. Rather like a Presidential spokesman.)")
