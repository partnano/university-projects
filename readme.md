# University Projects

This repository represents the different projects and exercises I did in the course of my studies at the University of Technology Vienna ('TU Wien', or 'Technische Universität Wien').

The course year and semester is noted for every course in this repository in the form of YYYYS.
(Y = Year, S = Semester, either Summer or Winter)

### 'Compilerbuilding' - 2018S

The subject was about, as the name implies, building compilers. Over the semester within consecutive exercises the students had to build a compiler for a small programming language from scratch using C, Assembly, and technologies like lex/flex, yacc, and burg. For more information about each and every task see the pdf (German).

### 'Semistructued Data' - 2018S

Semistructured Data was focused on XML and suitable technologies (XSLT, XQuery, etc.). Within the subjects, different tasks were to solve using those technologies. See the pdfs (German) within the task folders for more information.

### 'Software Engineering and Project Management - Individual Phase' - 2017W

This was the first phase of the subject, in which the task was to individually create a small desktop application in Java with JavaFX and H2, using the 3 layer architecture (UI - Service - Persistence).

### 'Software Engineering and Project Management - Group Phase' - 2017W

This was the second phase of the subject in which the task was again to create a desktop application. This time however in a team, using Spring and a Client-Server architecture, on top of the requirements of the individual phase. It was also required to use sufficient project management (several planning documents, time tracking, the use of redmine, etc.), which is however not in this repository.
The team consisted of Thomas Jirout, Markus Grech, Philip Bugnar, Maximilian Peinhopf and myself.

### 'Interface and Interaction Design' - 2017W

The project is a horizontal prototype of a 'social cinema experience' (people can / should find each other to go to the movies), realised in HTML, CSS and Javascript.
This was a team project together with Thomas Jirout, Verena Schwarz and Marco Weber.

### 'Distributed Systems - Individual Phase' - 2017W

This project is a simple email-like service and protocol, created in Java, using sockets and concurrency. For details, see the task description in the folder.

### 'Distributed Systems - Group Phase' - 2017W

This task is built upon the individual project, implementing a nameserver service, as well as encryption and hashing. This was done in a team together with Markus Grech and Lukas Schügerl. The codebase from the individual task used was written by Markus Grech. For details, see the task description in the folder.

### 'Web Engineering' - 2017S

This subject focused on learning to use modern web technologies. The tasks in the folder are build upon each other, creating a mocked smart-home control, using Angular and Typescript. See the task descriptions (German) for more details.
This was done in a team together with Thomas Jirout and Maximilian Peinhopf.

### 'Functional Programming' - 2016W

In this subject several tasks were to be solved using Haskell and the Hugs compiler. See the tasksheets (German) for more details.

### 'Operating Systems' - 2016W

This subject was focused on close-to-os programming, using C. See task descriptions (German) for more details.
