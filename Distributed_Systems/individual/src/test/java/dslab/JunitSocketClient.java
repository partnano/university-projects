package dslab;

import static org.hamcrest.CoreMatchers.containsString;

import java.io.BufferedReader;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import org.junit.rules.ErrorCollector;

public class JunitSocketClient implements Closeable {

    private ErrorCollector err;

    private Socket socket;
    private BufferedReader reader;
    private PrintWriter writer;

    /**
     * Creates a new Socket that connects to localhost on the given port and holds the I/O resources.
     *
     * @param port the port to connect to
     * @throws IOException if an I/O error occurred while connecting.
     */
    public JunitSocketClient(int port) throws IOException {
        this.socket = new Socket("localhost", port);
        this.reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        this.writer = new PrintWriter(socket.getOutputStream());
    }

    /**
     * Creates a new Socket that connects to localhost on the given port and holds the I/O resources.
     *
     * @param port the port to connect to
     * @param err the error collector used to verify communication
     * @throws IOException if an I/O error occurred while connecting.
     */

    public JunitSocketClient(int port, ErrorCollector err) throws IOException {
        this(port);
        this.err = err;
    }

    /**
     * Reads a line from the input stream and verifies that it contains the given string.
     *
     * @param string the partial string to match
     * @throws IOException on read errors
     */
    public void verify(String string) throws IOException {
        err.checkThat(read(), containsString(string));
    }

    /**
     * Writes the given string to the output stream, and then behaves like {@link #verify(String)}.
     *
     * @param request the request to send
     * @param response the expected response (partial string match)
     * @throws IOException on I/O errors
     */
    public void sendAndVerify(String request, String response) throws IOException {
        err.checkThat(sendAndRead(request), containsString(response));
    }

    public String read() throws IOException {
        return reader.readLine();
    }

    public String sendAndRead(String message) throws IOException {
        writer.println(message);
        writer.flush();
        return reader.readLine();
    }

    @Override
    public void close() throws IOException {
        socket.close();
    }
}
