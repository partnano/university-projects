package dslab.protocols;

import dslab.util.Config;

import java.io.*;
import java.net.Socket;
import java.net.SocketException;
import java.util.List;
import java.util.MissingResourceException;
import java.util.concurrent.ConcurrentHashMap;

public class DMAPServerThread implements Runnable {

    private Socket socket;
    private Config users;
    private ConcurrentHashMap<Integer, DMEP> stored_messages;

    private List<Socket> sockets;

    public DMAPServerThread (Socket socket, Config users, ConcurrentHashMap stored_messages, List<Socket> sockets) {
        this.socket = socket;
        this.users = users;
        this.stored_messages = stored_messages;
        this.sockets = sockets;
    }

    @Override
    public void run() {
        try {
            System.out.println("Client connected to DMAP!");

            BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            PrintWriter writer = new PrintWriter(socket.getOutputStream());

            String request;
            String response = "";
            DMAP access = new DMAP(stored_messages);

            writer.println("ok DMAP");
            writer.flush();

            while ((request = reader.readLine()) != null) {
                System.out.println("DMAP request: " + request);

                if (request.startsWith("login")) {
                    response = "";
                    String[] login_data = request.split("\\s");

                    String password = "";
                    try {
                        password = users.getString(login_data[1]);
                    } catch (MissingResourceException e) {
                        response = "error unknown user";
                    }

                    if (!password.equals("") && login_data[2] != null && !login_data[2].equals(password))
                        response = "error wrong password";

                    if (access.user != null)
                        response = "error already logged in";

                    if (response.equals("")) {
                        access.user = login_data[1];
                        response = "ok";
                    }

                } else if (request.equals("quit")) {
                    writer.println("ok bye");
                    writer.flush();
                    break;

                } else {
                    response = access.command(request);
                }

                writer.println(response);
                writer.flush();

                if (response.equals("error protocol error"))
                    break;
            }

        } catch (SocketException e) {
            System.out.println(e.getMessage());
        } catch (IOException e) {
            throw new UncheckedIOException(e);

        } finally {
            if (socket != null && !socket.isClosed()) {
                try {
                    socket.close();
                } catch (IOException e) {/*ignore*/}

                // remove closed sockets from list
                for (int i = 0; i < sockets.size(); i++) {
                    if (sockets.get(i) != null && sockets.get(i).isClosed()) {
                        sockets.remove(i);
                        break;
                    }
                }
            }
        }
    }
}
