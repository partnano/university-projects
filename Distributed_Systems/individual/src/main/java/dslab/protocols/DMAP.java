package dslab.protocols;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class DMAP {

    public String user;
    private ConcurrentHashMap<Integer, DMEP> messages;

    public DMAP (ConcurrentHashMap messages) {
        this.messages = messages;
    }

    public String command (String cmd) {

        if (user == null)
            return "error not logged in";

        if (cmd.equals("list")) {
            String message_list = "";

            for (Map.Entry<Integer, DMEP> entry : messages.entrySet()) {
                String[] recipients = entry.getValue().recipients;

                boolean in_rec = false;
                for (String r : recipients) {
                    System.out.println(r.split("@")[0]);

                    if (user.equals(r.split("@")[0])) {
                        in_rec = true;
                        break;
                    }
                }

                if (in_rec)
                    message_list += entry.getKey() + " " + entry.getValue().sender + " " + entry.getValue().subject + "\n";

            }

            if (message_list.length() == 0)
                return "";

            return message_list.substring(0, message_list.length()-1);
        }

        if (cmd.startsWith("show")) {
            int id = parse_id(cmd);

            if (id == -1)
                return "error unknown message id";

            String output = "";

            DMEP message = messages.get(id);
            output += "from " + message.sender + "\n";

            String reps = "";
            for (String r : message.recipients)
                reps += r + ",";
            reps = reps.substring(0, reps.length() -1);

            output += "to " + reps + "\n";
            output += "subject " + message.subject + "\n";
            output += "data " + message.data;

            return output;
        }

        if (cmd.startsWith("delete")) {
            int id = parse_id(cmd);

            if (id == -1)
                return "error unknown message id";

            messages.remove(id);

            return "ok";
        }

        if (cmd.equals("logout")) {
            user = null;
            return "ok";
        }

        return "error protocol error";
    }

    private int parse_id (String cmd) {
        int id;

        try {
            id = Integer.parseInt(cmd.split("\\s")[1]);
        } catch (NumberFormatException e) { return -1; }

        if (messages.containsKey(id))
            return id;

        return -1;
    }

}
