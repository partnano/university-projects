package dslab.protocols;

import dslab.util.Config;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class DMAPServer extends Thread {

    private ServerSocket server_socket;
    private ExecutorService executor;
    private Config users;

    private ConcurrentHashMap<Integer, DMEP> stored_messages;

    private List<Socket> sockets;

    public DMAPServer(ServerSocket server_socket, Config users, ConcurrentHashMap stored_messages) {
        this.server_socket = server_socket;
        this.users = users;
        this.stored_messages = stored_messages;

        sockets = new ArrayList<>();
    }

    public void run() {

        executor = Executors.newCachedThreadPool();

        while (true) {

            Socket socket = null;
            try {
                // wait for Client to connect
                socket = server_socket.accept();
                sockets.add(socket);

                DMAPServerThread server_thread = new DMAPServerThread(socket, users, stored_messages, sockets);

                executor.execute(server_thread);

            } catch (SocketException e) {
                System.out.println(e.getMessage());
                break;

            } catch (IOException e) {
                // you should properly handle all other exceptions
                throw new UncheckedIOException(e);
            }
        }

        // close all sockets
        for (Socket s : sockets) {
            try {
                s.close();
            } catch (IOException e) {/*ignore*/}
        }

        if (executor != null)
            executor.shutdown();
    }
}
