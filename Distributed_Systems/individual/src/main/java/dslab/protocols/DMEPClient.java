package dslab.protocols;

import java.io.*;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.List;

public class DMEPClient implements Runnable {

    private DMEP message;
    private String host;
    private int port;

    String error_host;
    int error_port = -1;

    List<Socket> sockets;

    public DMEPClient (DMEP message, String host, int port, List<Socket> sockets) {
        this.message = message;
        this.host = host;
        this.port = port;
        this.sockets = sockets;
    }

    @Override
    public void run() {
        Socket socket = null;

        try {
            socket = new Socket(host, port);
            sockets.add(socket);

            BufferedReader server_reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            PrintWriter server_writer = new PrintWriter(socket.getOutputStream());

            System.out.println("Transfer client is up, trying to send DMEP message ...");

            String response;
            int i = 0;

            while ((response = server_reader.readLine()) != null) {
                System.out.println("Response: " + response);

                if (!response.startsWith("ok")) {
                    System.out.println("Sending message wasn't successful, trying to send error message to sender ...");

                    if (error_host != null && error_port != -1) {
                        Socket error_socket = null;
                        try {

                            error_socket = new Socket(error_host, error_port);
                            sockets.add(error_socket);
                            BufferedReader error_reader = new BufferedReader(new InputStreamReader(error_socket.getInputStream()));
                            PrintWriter error_writer = new PrintWriter(error_socket.getOutputStream());

                            String error_response;
                            int j = 0;

                            DMEP error = new DMEP();
                            error.begin = true;
                            error.subject = "sending error";
                            error.data = "error " + response;
                            error.command("from mailer@[" + socket.getLocalAddress().toString().substring(1) + "]");
                            error.command("to " + message.sender);

                            while ((error_response = error_reader.readLine()) != null) {
                                if (!error_response.startsWith("ok"))
                                    break;

                                send_request(error_writer, error, j);
                                j++;
                                if (j == 6) break;
                            }

                        } catch (Exception e) {/*ignore*/}
                        finally {
                            if (error_socket != null && !error_socket.isClosed()) {
                                try {
                                    error_socket.close();

                                    remove_closed_sockets(sockets);
                                } catch (IOException e) {/*ignore*/}
                            }
                        }
                    }

                    break;
                }

                send_request(server_writer, message, i);

                i++;
                if (i == 6) break;
            }

            server_writer.println("quit");
            server_writer.flush();

            System.out.println("Done sending DMEP message!");

        } catch (UnknownHostException e) {
            System.out.println("Cannot connect to host: " + e.getMessage());
        } catch (SocketException e) {
            System.out.println(e.getMessage());
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        } finally {
            if (socket != null && !socket.isClosed()) {
                try {
                    socket.close();
                } catch (IOException e) {
                    // Ignored because we cannot handle it
                }
            }

            remove_closed_sockets(sockets);

        }
    }

    private void send_request (PrintWriter writer, DMEP msg, int i) {
        switch (i) {
            case 0:
                writer.println("begin");
                writer.flush();
                break;
            case 1:
                writer.println("subject " + msg.subject);
                writer.flush();
                break;
            case 2:
                writer.println("data " + msg.data);
                writer.flush();
                break;
            case 3:
                writer.println("from " + msg.sender);
                writer.flush();
                break;
            case 4:
                String to = "to ";
                for (String r : msg.recipients)
                    to += r + ",";
                to = to.substring(0, to.length()-1);

                writer.println(to);
                writer.flush();
                break;
            case 5:
                writer.println("send");
                writer.flush();
                break;
            default:
                System.out.println("Bad!");
        }
    }

    private void remove_closed_sockets(List<Socket> sockets) {
        // remove closed sockets from list
        for (int i = 0; i < sockets.size(); i++) {
            if (sockets.get(i) != null && sockets.get(i).isClosed()) {
                sockets.remove(i);
                break;
            }
        }
    }
}
