package dslab.protocols;

import dslab.mailbox.MailboxServer;
import dslab.util.Config;

import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.List;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class DMEPServerThread implements Runnable {

    private Socket socket;
    private String type = null;
    private Config config;
    private ResourceBundle domain_bundle;

    private DatagramSocket monitoring_socket;

    // null if transfer server
    Config mailbox_users;
    String mailbox_domain;
    ConcurrentHashMap<Integer, DMEP> mailbox_storage;

    String transfer_address;

    List<Socket> sockets;
    List<Socket> client_sockets;

    public DMEPServerThread(Socket socket, String type, Config config, List<Socket> sockets) {
        this.socket = socket;
        this.type = type;
        this.config = config;
        domain_bundle = ResourceBundle.getBundle("domains");
        this.sockets = sockets;

        client_sockets = new ArrayList<>();
    }

    @Override
    public void run() {

        ExecutorService executor = Executors.newCachedThreadPool();

        try {
            System.out.println("Client connected!");

            monitoring_socket = new DatagramSocket();

            BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            PrintWriter writer = new PrintWriter(socket.getOutputStream());

            String request;
            DMEP message = new DMEP();

            writer.println("ok DMEP");
            writer.flush();

            // read client requests
            while ((request = reader.readLine()) != null) {
                System.out.println("DMEP request: " + request);

                String response = message.command(request);

                if (response.equals("do check rec")) {
                    response = "ok " + message.recipients.length;
                    if (type.equals("transfer"))
                    {} // continue...
                    else {
                        System.out.println("checking users for this domain!");

                        for (String recipient : message.recipients) {
                            String[] address = recipient.split("@");

                            if (address[1].equals(mailbox_domain)) {
                                try {
                                    mailbox_users.getString(address[0]);
                                } catch (MissingResourceException e) {
                                    System.out.println("User " + address[0] + " not found!");
                                    response = "error unknown recipient " + address[0];
                                    break;
                                }
                            }
                        }
                    }
                }

                if (response.equals("do send")) {
                    if (type.equals("transfer")) {

                        // SENDING MESSAGES
                        String[] error_address = null;
                        try {
                            String loc = domain_bundle.getString(message.sender_domain);
                            error_address = loc.split(":");
                        } catch (MissingResourceException e) {
                            // ignore unknown domain
                        }

                        for (String d : message.domains) {
                            try {
                                String loc = domain_bundle.getString(d);
                                String[] ip_port = loc.split(":");

                                DMEPClient client = new DMEPClient(message, ip_port[0], Integer.parseInt(ip_port[1]), client_sockets);

                                if (error_address != null) {
                                    client.error_host = error_address[0];
                                    client.error_port = Integer.parseInt(error_address[1]);
                                }

                                executor.execute(client);

                            } catch (MissingResourceException e) {
                                // ignore unknown domains
                            }
                        }

                        // MONITORING
                        String input = socket.getLocalAddress().toString() + ":" + socket.getLocalPort() + " " + message.sender;
                        input = input.substring(1, input.length());
                        byte[] buffer = input.getBytes();

                        DatagramPacket packet = new DatagramPacket(buffer, buffer.length,
                                InetAddress.getByName(config.getString("monitoring.host")),
                                config.getInt("monitoring.port"));

                        monitoring_socket.send(packet);

                    } else if (type.equals("mailbox")) {
                        System.out.println("Saving in mailbox ...");

                        mailbox_storage.put(MailboxServer.next_id++, message);

                    } else {/*stub*/}

                    response = "ok";
                }

                // print request
                writer.println(response);
                writer.flush();

                if (response.equals("error protocol error")
                        || response.startsWith("error unknown recipient")
                        || response.equals("ok bye"))
                    break;
            }

        } catch (SocketException e) {
            System.out.println(e.getMessage());
        } catch (IOException e) {
            throw new UncheckedIOException(e);

        } finally {
            if (socket != null && !socket.isClosed()) {
                try {
                    socket.close();
                } catch (IOException e) {/*ignore*/}
            }

            // remove closed sockets from list
            for (int i = 0; i < sockets.size(); i++) {
                if (sockets.get(i) != null && sockets.get(i).isClosed()) {
                    sockets.remove(i);
                    break;
                }
            }
        }

        // close all client sockets
        for (Socket s : client_sockets) {
            try {
                s.close();
            } catch (IOException e) {/*ignore*/}
        }

        executor.shutdown();
    }
}
