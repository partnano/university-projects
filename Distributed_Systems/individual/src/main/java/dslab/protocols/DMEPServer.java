package dslab.protocols;

import dslab.util.Config;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class DMEPServer extends Thread {

    private ServerSocket server_socket;
    private String type;
    private Config config;

    // null if transfer server
    public Config mailbox_users;
    public String mailbox_domain;
    public ConcurrentHashMap<Integer, DMEP> mailbox_storage;

    private String transfer_address;

    private List<Socket> sockets;

    public DMEPServer(ServerSocket server_socket, String type, Config config) {
        this.server_socket = server_socket;
        this.type = type;
        this.config = config;

        transfer_address = server_socket.getInetAddress().toString() + ":" + server_socket.getLocalPort();
        sockets = new ArrayList<>();
    }

    public void run() {

        ExecutorService executor = Executors.newCachedThreadPool();

        while (true) {

            Socket socket = null;
            try {
                // wait for Client to connect
                socket = server_socket.accept();
                sockets.add(socket);

                DMEPServerThread server_thread = new DMEPServerThread(socket, type, config, sockets);
                server_thread.mailbox_users = mailbox_users;
                server_thread.mailbox_domain = mailbox_domain;
                server_thread.mailbox_storage = mailbox_storage;
                server_thread.transfer_address = transfer_address;

                executor.execute(server_thread);

            } catch (SocketException e) {
                System.out.println(e.getMessage());
                break;

            } catch (IOException e) {
                // you should properly handle all other exceptions
                throw new UncheckedIOException(e);

            }
        }

        // close all sockets and shutdown threads
        for (Socket s : sockets) {
            if (s != null) {
                try {
                    s.close();
                } catch (IOException e) {/*ignore*/}
            }
        }

        executor.shutdown();
    }
}