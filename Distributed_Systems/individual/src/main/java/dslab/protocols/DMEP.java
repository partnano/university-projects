package dslab.protocols;

import java.util.HashSet;
import java.util.Set;

/**
 * Helper class for DMEP protocol messages
 */
public class DMEP {

    public String sender;
    public String subject;
    public String data;
    public String[] recipients;
    public Set<String> domains;
    public String sender_domain;

    public boolean begin = false;

    public DMEP() {}

    public String command (String cmd) {
        if (cmd.equals("begin")) {
            sender = null;
            subject = null;
            data = null;
            recipients = null;
            domains = null;
            begin = true;
            return "ok";

        } else if (cmd.equals("quit"))
            return "ok bye";

        if (!begin)
            return "error protocol error";

        if (cmd.startsWith("to ")) {
            recipients = cmd.substring(3).split(",");

            domains = new HashSet<>();
            for (String r : recipients) {
                // maybe check for valid address?
                domains.add(r.split("@")[1]);
            }

            return "do check rec";

        } else if (cmd.startsWith("from ")) {
            sender = cmd.substring(5);
            String[] tmp = sender.split("@");
            sender_domain = tmp.length == 2 ? tmp[1] : "";
            return "ok";

        } else if (cmd.startsWith("subject ")) {
            subject = cmd.substring(8);
            return "ok";

        } else if (cmd.startsWith("data ")) {
            data = cmd.substring(5);
            return "ok";

        } else if (cmd.startsWith("send")) {

            if (sender == null)
                return "error no sender";
            if (subject == null)
                return "error no subject";
            if (data == null)
                return "error no data";
            if (recipients == null)
                return "error no recipients";

            begin = false;
            return "do send";
        }

        return "error protocol error";
    }
}
