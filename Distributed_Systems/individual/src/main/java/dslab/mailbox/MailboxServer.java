package dslab.mailbox;

import java.io.*;
import java.net.ServerSocket;
import java.util.concurrent.ConcurrentHashMap;

import at.ac.tuwien.dsg.orvell.Shell;
import at.ac.tuwien.dsg.orvell.StopShellException;
import at.ac.tuwien.dsg.orvell.annotation.Command;
import dslab.ComponentFactory;
import dslab.util.Config;
import dslab.protocols.DMAPServer;
import dslab.protocols.DMEP;
import dslab.protocols.DMEPServer;

public class MailboxServer implements IMailboxServer, Runnable {

    private final Config config;
    private final Config users;

    private ServerSocket dmep_server_socket;
    private ServerSocket dmap_server_socket;

    private ConcurrentHashMap<Integer, DMEP> stored_messages;
    public static volatile int next_id = 1;

    private Shell shell;

    /**
     * Creates a new server instance.
     *
     * @param componentId the id of the component that corresponds to the Config resource
     * @param config the component config
     * @param in the input stream to read console input from
     * @param out the output stream to write console output to
     */
    public MailboxServer(String componentId, Config config, InputStream in, PrintStream out) {
        this.config = config;
        this.users = new Config(config.getString("users.config"));

        stored_messages = new ConcurrentHashMap<>();

        shell = new Shell(in, out);
        shell.register(this);
        shell.setPrompt(componentId + "> ");
    }

    @Override
    public void run() {
        try {
            dmep_server_socket = new ServerSocket(config.getInt("dmep.tcp.port"));

            DMEPServer dmep_server = new DMEPServer(dmep_server_socket, "mailbox", config);
            dmep_server.mailbox_users = users;
            dmep_server.mailbox_domain = config.getString("domain");
            dmep_server.mailbox_storage = stored_messages;

            dmep_server.start();

            dmap_server_socket = new ServerSocket(config.getInt("dmap.tcp.port"));

            DMAPServer dmap_server = new DMAPServer(dmap_server_socket, users, stored_messages);
            dmap_server.start();

        } catch (IOException e) {
            throw new UncheckedIOException("Error while creating server server_socket", e);
        }

        shell.run();
        System.out.println("Bye.");
    }

    @Override
    @Command
    public void shutdown() {
        if (dmep_server_socket != null) {
            try {
                dmep_server_socket.close();
            } catch (IOException e) {/*ignore*/}
        }

        if (dmap_server_socket != null) {
            try {
                dmap_server_socket.close();
            } catch (IOException e) {/*ignore*/}
        }

        throw new StopShellException();
    }

    public static void main(String[] args) throws Exception {
        IMailboxServer server = ComponentFactory.createMailboxServer(args[0], System.in, System.out);
        server.run();
    }
}
