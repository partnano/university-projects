package dslab.monitoring;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.util.Map;

public class MonitoringThread extends Thread {

    private DatagramSocket datagram_socket;
    private Map<String, Integer> addresses;
    private Map<String, Integer> servers;

    public MonitoringThread (DatagramSocket datagram_socket,
                             Map<String, Integer> addresses,
                             Map<String, Integer> servers)
    {
        this.datagram_socket = datagram_socket;
        this.addresses = addresses;
        this.servers = servers;
    }

    public void run () {

        byte[] buffer;
        DatagramPacket packet;
        try {
            while (true) {

                buffer = new byte[1024];
                packet = new DatagramPacket(buffer, buffer.length);

                datagram_socket.receive(packet);

                String request = new String(packet.getData(), 0, packet.getLength());
                System.out.println("Monitoring Request: " + request);

                String[] data = request.split("\\s");

                if (data.length != 2)
                    continue;

                servers.put(data[0], servers.getOrDefault(data[0], 0) +1);
                addresses.put(data[1], addresses.getOrDefault(data[1], 0) +1);

            }
        } catch (SocketException e) {
            System.out.println(e.getMessage());
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        } finally {
            if (datagram_socket != null && !datagram_socket.isClosed()) {
                datagram_socket.close();
            }
        }
    }
}
