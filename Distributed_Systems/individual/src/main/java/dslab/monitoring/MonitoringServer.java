package dslab.monitoring;

import java.io.*;
import java.net.DatagramSocket;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import at.ac.tuwien.dsg.orvell.Shell;
import at.ac.tuwien.dsg.orvell.StopShellException;
import at.ac.tuwien.dsg.orvell.annotation.Command;
import dslab.ComponentFactory;
import dslab.util.Config;

public class MonitoringServer implements IMonitoringServer, Runnable {

    private Config config;

    private DatagramSocket datagram_socket;
    private LinkedHashMap<String, Integer> addresses;
    private LinkedHashMap<String, Integer> servers;

    private Shell shell;

    /**
     * Creates a new server instance.
     *
     * @param componentId the id of the component that corresponds to the Config resource
     * @param config the component config
     * @param in the input stream to read console input from
     * @param out the output stream to write console output to
     */
    public MonitoringServer(String componentId, Config config, InputStream in, PrintStream out) {
        this.config = config;

        shell = new Shell(in, out);
        shell.register(this);
        shell.setPrompt("");

        addresses = new LinkedHashMap<>();
        servers = new LinkedHashMap<>();
    }

    @Override
    public void run() {
        try {
            datagram_socket = new DatagramSocket(config.getInt("udp.port"));

            new MonitoringThread(datagram_socket, addresses, servers).start();
        } catch (IOException e) {
            throw new RuntimeException("Cannot listen on UDP port.", e);
        }

        shell.run();
        System.out.println("Bye.");
    }

    @Override
    @Command
    public void addresses() {
        for (Map.Entry<String, Integer> entry : addresses.entrySet()) {
            shell.out().println(entry.getKey() + " " + entry.getValue());
        }
    }

    @Override
    @Command
    public void servers() {
        for (Map.Entry<String, Integer> entry : servers.entrySet()) {
            shell.out().println(entry.getKey() + " " + entry.getValue());
        }
    }

    @Override
    @Command
    public void shutdown() {
        if (datagram_socket != null) {
            datagram_socket.close();
        }

        throw new StopShellException();
    }

    public static void main(String[] args) throws Exception {
        IMonitoringServer server = ComponentFactory.createMonitoringServer(args[0], System.in, System.out);
        server.run();
    }

}
