package dslab.transfer;

import java.io.*;
import java.net.ServerSocket;

import at.ac.tuwien.dsg.orvell.Shell;
import at.ac.tuwien.dsg.orvell.StopShellException;
import at.ac.tuwien.dsg.orvell.annotation.Command;
import dslab.ComponentFactory;
import dslab.util.Config;
import dslab.protocols.DMEPServer;

public class TransferServer implements ITransferServer, Runnable {

    private final Config config;
    private ServerSocket server_socket;

    private Shell shell;

    /**
     * Creates a new server instance.
     *
     * @param componentId the id of the component that corresponds to the Config resource
     * @param config the component config
     * @param in the input stream to read console input from
     * @param out the output stream to write console output to
     */
    public TransferServer(String componentId, Config config, InputStream in, PrintStream out) {
        this.config = config;

        shell = new Shell(in, out);
        shell.register(this);
        shell.setPrompt(componentId + "> ");
    }

    @Override
    public void run() {

        try {
            server_socket = new ServerSocket(config.getInt("tcp.port"));

            // handle incoming connections from client in a separate thread
            DMEPServer dmep_server = new DMEPServer(server_socket, "transfer", config);
            dmep_server.start();

        } catch (IOException e) {
            throw new UncheckedIOException("Error while creating server server_socket", e);
        }

        shell.run();
        System.out.println("Bye.");
    }

    @Override
    @Command
    public void shutdown() {
        if (server_socket != null) {
            try {
                server_socket.close();
            } catch (IOException e) {/*ignore*/}
        }

        throw new StopShellException();
    }

    public static void main(String[] args) throws Exception {
        ITransferServer server = ComponentFactory.createTransferServer(args[0], System.in, System.out);
        server.run();
    }

}
