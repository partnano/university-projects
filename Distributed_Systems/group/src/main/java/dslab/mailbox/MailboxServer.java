package dslab.mailbox;

import dslab.ComponentFactory;
import dslab.nameserver.AlreadyRegisteredException;
import dslab.nameserver.INameserverRemote;
import dslab.nameserver.InvalidDomainException;
import dslab.protocol.DmapServer;
import dslab.protocol.DmepServer;
import dslab.protocol.UserData;
import dslab.util.Config;
import dslab.util.SimpleShutdownConsole;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.function.Predicate;

public class MailboxServer implements IMailboxServer, Runnable
{
	private final SimpleShutdownConsole console;
	private final DmepServer dmepServer;
	private final DmapServer dmapServer;

	private Registry registry;
	private INameserverRemote root;
	private Config config;

	/**
	 * Creates a new server instance.
	 *
	 * @param componentId the id of the component that corresponds to the Config resource
	 * @param config the component config
	 * @param in the input stream to read console input from
	 * @param out the output stream to write console output to
	 */
	public MailboxServer(String componentId, Config users, Config config, InputStream in, PrintStream out) throws
	                                                                                                       IOException
	{
		this.console = new SimpleShutdownConsole(in, out);

		int dmapPort = config.getInt("dmap.tcp.port");
		int dmepPort = config.getInt("dmep.tcp.port");

		UserData data = new UserData(users);
		this.dmapServer = new DmapServer(componentId, data, dmapPort, console::close);

		Predicate<String> hasUsers = (name) -> data.get(name) != null;
		this.dmepServer = new DmepServer(config.getString("domain"),
		                                 hasUsers,
		                                 dmepPort,
		                                 data::deliverMessage,
		                                 console::close);

		this.config = config;
	}

	@Override
	public void run()
	{
		dmepServer.start();
		dmapServer.start();

		try
		{
			registry = LocateRegistry.getRegistry(config.getString("registry.host"), config.getInt("registry.port"));
			root = (INameserverRemote) registry.lookup(config.getString("root_id"));

			if(root != null)
			{
				String ip = dmepServer.getIp() + ":" + config.getString("dmep.tcp.port");
				root.registerMailboxServer(config.getString("domain"), ip);
			}
		}
		catch(RemoteException | InvalidDomainException | AlreadyRegisteredException | NotBoundException e)
		{
			e.printStackTrace();
		}

		console.run();
		shutdown();
	}

	@Override
	public void shutdown()
	{
		dmepServer.shutdown();
		dmapServer.shutdown();
	}

	public static void main(String[] args) throws Exception
	{
		IMailboxServer server = ComponentFactory.createMailboxServer(args[0], System.in, System.out);
		server.run();
	}
}
