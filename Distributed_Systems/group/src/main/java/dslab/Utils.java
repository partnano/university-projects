package dslab;

import java.io.Closeable;
import java.io.IOException;

public class Utils
{
	public static void close(Closeable c)
	{
		try
		{
			c.close();
		}
		catch(IOException ex)
		{
			// should never happen
			ex.printStackTrace();
		}
	}

	public static void join(Thread t)
	{
		try
		{
			t.join();
		}
		catch(InterruptedException ex)
		{
			// should never happen
			ex.printStackTrace();
		}
	}
}
