package dslab.monitoring;

import dslab.ComponentFactory;
import dslab.Utils;
import dslab.util.Config;

import java.io.*;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

public class MonitoringServer implements IMonitoringServer
{
	private static final int BUFFER_LENGTH = 1024;

	private final BufferedReader conin;
	private final PrintStream conout;

	private final DatagramSocket socket;
	private final Thread serverThread;

	private final Map<String, Integer> statisticsByUser   = new HashMap<>();
	private final Map<String, Integer> statisticsByServer = new HashMap<>();

	/**
	 * Creates a new server instance.
	 *
	 * @param componentId the id of the component that corresponds to the Config resource
	 * @param config      the component config
	 * @param in          the input stream to read console input from
	 * @param out         the output stream to write console output to
	 */
	public MonitoringServer(String componentId, Config config, InputStream in, PrintStream out) throws SocketException
	{
		this.conin = new BufferedReader(new InputStreamReader(in));
		this.conout = out;

		this.socket = new DatagramSocket(config.getInt("udp.port"));
		this.serverThread = new Thread(() ->
		{
			try
			{
				for(;;)
				{
					DatagramPacket packet = new DatagramPacket(new byte[BUFFER_LENGTH], BUFFER_LENGTH);
					socket.receive(packet);
					String data = new String(packet.getData(), 0, packet.getLength(), StandardCharsets.UTF_8);

					String server;
					String user;

					try
					{
						String[] parts = data.split(" ");
						server = parts[0];
						user = parts[1];
					}
					catch(Exception ex)
					{
						// invalid packet, ignore
						continue;
					}

					synchronized(statisticsByServer)
					{
						statisticsByServer.put(server, statisticsByServer.getOrDefault(server, 0) + 1);
					}

					synchronized(statisticsByUser)
					{
						statisticsByUser.put(user, statisticsByUser.getOrDefault(user, 0) + 1);
					}
				}
			}
			catch(IOException ex)
			{
				if(!socket.isClosed())
				{
					conout.println("error reading udp packet: " + ex.getMessage());
					Utils.close(conin);
				}
			}
		});
	}

	@Override
	public void run()
	{
		serverThread.start();

		try
		{
			for(String line; (line = conin.readLine()) != null;)
			{
				switch(line)
				{
				case "shutdown":
					return;

				case "addresses":
					addresses();
					break;

				case "servers":
					servers();
					break;

				default:
					conout.println("invalid command");
					break;
				}
			}
		}
		catch(IOException ex)
		{
			conout.println("failed to read console input");
		}
		finally
		{
			shutdown();
		}
	}

	@Override
	public void addresses()
	{
		synchronized(statisticsByUser)
		{
			for(Map.Entry<String, Integer> entry : statisticsByUser.entrySet())
				conout.println(String.format("%s %s", entry.getKey(), entry.getValue()));
		}
	}

	@Override
	public void servers()
	{
		synchronized(statisticsByServer)
		{
			for(Map.Entry<String, Integer> entry : statisticsByServer.entrySet())
				conout.println(String.format("%s %s", entry.getKey(), entry.getValue()));
		}
	}

	@Override
	public void shutdown()
	{
		socket.close();
		Utils.join(serverThread);
	}

	public static void main(String[] args) throws Exception
	{
		IMonitoringServer server = ComponentFactory.createMonitoringServer(args[0], System.in, System.out);
		server.run();
	}
}
