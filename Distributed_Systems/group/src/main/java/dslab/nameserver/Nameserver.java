package dslab.nameserver;

import at.ac.tuwien.dsg.orvell.Shell;
import at.ac.tuwien.dsg.orvell.StopShellException;
import at.ac.tuwien.dsg.orvell.annotation.Command;
import dslab.ComponentFactory;
import dslab.Utils;
import dslab.util.Config;

import java.io.InputStream;
import java.io.PrintStream;
import java.rmi.*;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.text.DateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class Nameserver implements INameserver, INameserverRemote
{
	private boolean isRoot;
	private Registry registry;
	private Config config;

	private Map<String, INameserverRemote> childNameservers;
	private Map<String, String> mailboxAddresses;

	private Shell shell;
	private InputStream in;

	/**
	 * Creates a new server instance.
	 *
	 * @param componentId the id of the component that corresponds to the Config resource
	 * @param config the component config
	 * @param in the input stream to read console input from
	 * @param out the output stream to write console output to
	 */
	public Nameserver(String componentId, Config config, InputStream in, PrintStream out)
	{
		isRoot = !config.containsKey("domain");
		this.config = config;

		childNameservers = new ConcurrentHashMap<>();
		mailboxAddresses = new ConcurrentHashMap<>();

		this.shell = new Shell(in, out);
		this.shell.register(this);
		this.shell.setPrompt(componentId + " > ");
	}

	private static final DateTimeFormatter DATE_FORMAT = DateTimeFormatter.ofPattern("HH:MM:ss");

	private void log(String format, Object... values)
	{
		String time = LocalDateTime.now().format(DATE_FORMAT);
		String message = String.format("%s : %s", time, String.format(format, values));
		shell.out().println(message);
	}

	@Override
	public void run()
	{
		if(isRoot)
		{
			try
			{
				// create and export the registry instance on localhost at the specified port
				registry = LocateRegistry.createRegistry(config.getInt("registry.port"));

				// create a remote object of this server object
				INameserverRemote remote = (INameserverRemote)UnicastRemoteObject.exportObject(this, 0);

				// bind the obtained remote object on specified binding name in the registry
				registry.bind(config.getString("root_id"), remote);
			}
			catch(RemoteException e)
			{
				throw new RuntimeException("Error while starting server.", e);
			}
			catch(AlreadyBoundException e)
			{
				e.printStackTrace();
			}
		}
		else
		{
			try
			{
				String domain = config.getString("domain");

				// obtain registry that was created by the server
				Registry registry = LocateRegistry.getRegistry(config.getInt("registry.port"));

				UnicastRemoteObject.exportObject(this, 0);

				// look for the bound server remote-object implementing the INameserverRemote interface
				Remote remote = registry.lookup(config.getString("root_id"));

				INameserverRemote server = (INameserverRemote) remote;

				// register this nameserver
				server.registerNameserver(domain, this);
			}
			catch(RemoteException e)
			{
				throw new RuntimeException("Error while obtaining registry/server-remote-object.", e);
			}
			catch(NotBoundException e)
			{
				throw new RuntimeException("Error while looking for server-remote-object.", e);
			}
			catch(InvalidDomainException | AlreadyRegisteredException e)
			{
				e.printStackTrace();
			}
		}

		shell.run();
	}

	// COMMAND LINE STUFF

	@Override
	@Command
	public void nameservers()
	{
		int i = 1;
		for(Map.Entry<String, INameserverRemote> entry : childNameservers.entrySet())
		{
			shell.out().println(i++ + ". " + entry.getKey());
		}
	}

	@Override
	@Command
	public void addresses()
	{
		int i = 1;
		for(Map.Entry<String, String> entry : mailboxAddresses.entrySet())
		{
			shell.out().println(i++ + ". " + entry.getValue());
		}
	}

	@Override
	@Command
	public void shutdown()
	{
		try
		{
			UnicastRemoteObject.unexportObject(this, true);

			if(isRoot)
			{
				registry.unbind(config.getString("root_id"));
				UnicastRemoteObject.unexportObject(registry, true);
			}
		}
		catch(NoSuchObjectException e)
		{
			// ignore
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}

		throw new StopShellException();
	}

	// NAMESERVER STUFF

	@Override
	public void registerNameserver(String domain, INameserverRemote nameserver) throws RemoteException,
	                                                                                   AlreadyRegisteredException,
	                                                                                   InvalidDomainException
	{
		log("registering ns for %s", domain);

		if(!domain.contains("."))
		{
			if(childNameservers.containsKey(domain))
				throw new AlreadyRegisteredException(domain + "already registered.");

			// leaf domain
			childNameservers.put(domain, nameserver);
		}
		else
		{
			String outerMost = domain.substring(domain.lastIndexOf(".") + 1);

			if(!childNameservers.containsKey(outerMost))
				throw new InvalidDomainException("Intermediate domain not registered!");

			String nextDomain = removeLastDomain(domain);

			childNameservers.get(outerMost).registerNameserver(nextDomain, nameserver);
		}
	}

	@Override
	public void registerMailboxServer(String domain, String address) throws RemoteException, AlreadyRegisteredException,
	                                                                        InvalidDomainException
	{
		log("registering mx for %s", domain);

		String[] splitted = domain.split("\\.");

		if(splitted.length == 2 && !isRoot)
		{
			if(mailboxAddresses.containsKey(splitted[0]))
				throw new AlreadyRegisteredException(address + "already registered.");

			// leaf domain
			mailboxAddresses.put(splitted[0], address);
		}
		else if(splitted.length > 2)
		{
			String outerMost = domain.substring(domain.lastIndexOf(".") + 1);

			if(!childNameservers.containsKey(outerMost))
				throw new InvalidDomainException("Intermediate domain not registered!");

			String nextDomain = removeLastDomain(domain);

			childNameservers.get(outerMost).registerMailboxServer(nextDomain, address);
		}
		else if(splitted.length == 2 && isRoot)
		{
			String outerMost = domain.substring(domain.lastIndexOf(".") + 1);

			if(!childNameservers.containsKey(outerMost))
				throw new InvalidDomainException("Intermediate domain not registered!");

			childNameservers.get(outerMost).registerMailboxServer(domain, address);
		}
		else
		{
			System.out.println("impossible case");
			// shouldn't be possible
		}
	}

	@Override
	public INameserverRemote getNameserver(String zone)
	{
		log("looking up ns for %s", zone);
		return childNameservers.getOrDefault(zone, null);
	}

	@Override
	public String lookup(String name)
	{
		log("looking up mx for %s", name);
		return mailboxAddresses.getOrDefault(name, null);
	}

	// HELPERS

	private String removeLastDomain(String domain)
	{
		String[] singleDomains = domain.split(".");
		String newDomain = "";
		for(int i = 0; i < singleDomains.length - 1; ++i)
		{
			newDomain += singleDomains[i] + ".";
		}

		return newDomain.substring(0, newDomain.length() - 2);
	}

	public static void main(String[] args) throws Exception
	{
		INameserver component = ComponentFactory.createNameserver(args[0], System.in, System.out);
		component.run();
	}
}
