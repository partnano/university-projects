package dslab.protocol;

import dslab.Utils;
import dslab.nameserver.INameserver;
import dslab.nameserver.INameserverRemote;
import dslab.util.Config;

import java.io.*;
import java.net.*;
import java.nio.charset.StandardCharsets;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.*;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class DmepForwarder
{
	private static final int MONITORING_BUFFER_SIZE = 1024;

	private final BlockingQueue<Message> messages = new LinkedBlockingQueue<>();
	private final Thread forwardThread;
	private final DatagramSocket monitoringSocket = new DatagramSocket();
	private final DatagramPacket monitoringPacket;
	private INameserverRemote root;

	private final int transferPort;

	public DmepForwarder(int transferPort, Config config, SocketAddress monitoringDestAddr) throws SocketException
	{
		this.forwardThread = new Thread(this::forwardLoop);
		this.monitoringPacket = new DatagramPacket(new byte[MONITORING_BUFFER_SIZE], MONITORING_BUFFER_SIZE, monitoringDestAddr);

		this.transferPort = transferPort;

		try
		{
			Registry registry = LocateRegistry.getRegistry(config.getString("registry.host"), config.getInt("registry.port"));
			this.root = (INameserverRemote)registry.lookup(config.getString("root_id"));

		}
		catch(RemoteException | NotBoundException e)
		{
			e.printStackTrace();
		}
	}

	private InetSocketAddress lookupDomain(String[] domain, INameserverRemote nameserver)
	{
		try
		{
			INameserverRemote newServer = nameserver.getNameserver(domain[domain.length - 1]);

			if(newServer == null)
				return null;

			String[] newDomain = Arrays.copyOf(domain, domain.length - 1);

			if(newDomain.length == 1)
			{
				String address = newServer.lookup(newDomain[0]);

				if(address == null)
					return null;

				String[] addressParts = address.split(":");
				return new InetSocketAddress(InetAddress.getByName(addressParts[0]), Integer.parseInt(addressParts[1]));
			}
			else
			{
				this.lookupDomain(newDomain, newServer);
			}
		}
		catch(RemoteException | UnknownHostException e)
		{
			e.printStackTrace();
		}

		return null;
	}

	private InetSocketAddress lookupDomain(String email, INameserverRemote nameserver)
	{
		String[] domainParts = email.split("@", 2)[1].split("\\.");
		return lookupDomain(domainParts, nameserver);
	}

	private boolean sendMessage(Message message, InetSocketAddress address)
	{
		System.out.println("forwarding message: " + message);

		try(Socket socket = new Socket())
		{
			socket.connect(address);

			String recipients = message.to.toString();
			recipients = recipients.replaceAll("[\\[\\] ]", "");
			BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
			writer.write("begin\n");
			writer.write("from " + message.from + "\n");
			writer.write("to " + recipients + "\n");
			writer.write("subject " + message.subject + "\n");
			writer.write("data " + message.data + "\n");
			writer.write("hash " + message.hash + "\n");
			writer.write("send\n");
			writer.write("quit\n");
			writer.flush();

			BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));

			for(String line; (line = reader.readLine()) != null;)
			{
				if(line.startsWith("error"))
					return false;
			}

			return true;
		}
		catch(IOException ex)
		{
			return false;
		}
	}

	private String localAddress()
	{
		try
		{
			return InetAddress.getLocalHost().getHostAddress();
		}
		catch(UnknownHostException ex)
		{
			return "127.0.0.1";
		}
	}

	private void sendErrorMessage(Message message, List<String> failures)
	{
		Message error = new Message();
		error.from = "mailer@" + localAddress();
		error.to = Collections.singletonList(message.from);
		error.subject = "error message delivery failed";

		String datafmt = "failed to deliver the following message: from=%s, to=%s, subject=%s, data=%s to the following recipients: %s\n";
		error.data = String.format(datafmt, message.from, String.join(",", message.to), message.subject, message.data, String.join(", ", failures));

		InetSocketAddress address = lookupDomain(message.from, root);

		if(address != null)
			sendMessage(error, address);
	}

	private void sendMonitoringInfo(String sender)
	{
		String info = String.format("%s:%s %s", localAddress(), transferPort, sender);
		monitoringPacket.setData(info.getBytes(StandardCharsets.UTF_8));

		try
		{
			monitoringSocket.send(monitoringPacket);
		}
		catch(IOException ex)
		{
			// we don't really care
			ex.printStackTrace();
		}
	}

	private void handleMessage(Message message)
	{
		Map<InetSocketAddress, List<String>> usersByServer = new HashMap<>();
		List<String> failures = new ArrayList<>();

		for(String recipient : message.to)
		{
			InetSocketAddress address = lookupDomain(recipient, root);

			if(address == null)
				failures.add(recipient);
			else
				usersByServer.computeIfAbsent(address, (a) -> new ArrayList<>()).add(recipient);
		}

		for(Map.Entry<InetSocketAddress, List<String>> entry : usersByServer.entrySet())
		{
			if(sendMessage(message, entry.getKey()))
				sendMonitoringInfo(message.from);
			else
				failures.addAll(entry.getValue());
		}

		if(!failures.isEmpty())
			sendErrorMessage(message, failures);
	}

	private void forwardLoop()
	{
		try
		{
			for(;;)
			{
				Message message = messages.take();
				handleMessage(message);
			}
		}
		catch(InterruptedException ex)
		{
			// shutdown
		}
	}

	public void queue(Message message)
	{
		messages.add(message);
	}

	public void start()
	{
		forwardThread.start();
	}

	public void shutdown()
	{
		forwardThread.interrupt();
		Utils.join(forwardThread);
	}
}
