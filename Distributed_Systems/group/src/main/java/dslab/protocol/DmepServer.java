package dslab.protocol;

import dslab.Utils;

import java.io.IOException;
import java.net.Socket;
import java.util.Collections;
import java.util.IdentityHashMap;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.function.Predicate;

public class DmepServer
{
	private final String domain;
	private final Predicate<String> userKnown;
	private final ClientAcceptor acceptor;
	private final Set<DmepServerClient> clients = Collections.newSetFromMap(new IdentityHashMap<>());
	private final ExecutorService clientExecutor = Executors.newCachedThreadPool();
	private final DmepMessageCallback messageCallback;

	public DmepServer(String domain, Predicate<String> userKnown, int port, DmepMessageCallback messageCallback, Runnable errorCallback) throws IOException
	{
		this.domain = domain;
		this.userKnown = userKnown;
		this.acceptor = new ClientAcceptor(port, this::onNewClient, errorCallback);
		this.messageCallback = messageCallback;
	}

	private void onNewClient(Socket socket)
	{
		DmepServerClient client = new DmepServerClient(domain, userKnown, socket, messageCallback, this::onClientDone);
		clientExecutor.submit(client);

		synchronized(clients)
		{
			clients.add(client);
		}
	}

	public String getIp()
	{
		return acceptor.getIp();
	}

	private void onClientDone(DmepServerClient client)
	{
		synchronized(clients)
		{
			clients.remove(client);
		}
	}

	public void start()
	{
		acceptor.start();
	}

	public void shutdown()
	{
		acceptor.shutdown();

		synchronized(clients)
		{
			for(DmepServerClient client : clients)
				Utils.close(client);
		}

		clientExecutor.shutdown();

		try
		{
			clientExecutor.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
		}
		catch(InterruptedException ex)
		{
			// should never happen
			ex.printStackTrace();
		}
	}
}
