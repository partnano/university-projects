package dslab.protocol;

import dslab.Utils;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.function.Consumer;

public class ClientAcceptor
{
	private final ServerSocket socket;
	private final Thread acceptThread;
	private final Consumer<Socket> callback;
	private final Runnable errorCallback;
	private volatile boolean shutdown = false;

	public ClientAcceptor(int port, Consumer<Socket> clientCallback, Runnable errorCallback) throws IOException
	{
		this.socket = new ServerSocket(port);
		this.acceptThread = new Thread(this::acceptLoop);
		this.callback = clientCallback;
		this.errorCallback = errorCallback;
	}

	public String getIp()
	{
		return socket.getInetAddress().getHostAddress();
	}

	private void acceptLoop()
	{
		try
		{
			for(;;)
			{
				Socket client = socket.accept();
				callback.accept(client);
			}
		}
		catch(IOException ex)
		{
			if(shutdown)
				return;

			errorCallback.run();
		}
	}

	public void start()
	{
		acceptThread.start();
	}

	public void shutdown()
	{
		shutdown = true;
		Utils.close(socket);
		Utils.join(acceptThread);
	}
}
