package dslab.protocol;

import dslab.Utils;
import dslab.util.Keys;

import java.io.File;
import java.io.IOException;
import java.net.Socket;
import java.security.Key;
import java.util.Collections;
import java.util.IdentityHashMap;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class DmapServer
{
	private final String componentId;
	private final Key serverKey;
	private final UserData data;
	private final ClientAcceptor acceptor;
	private final Set<DmapServerClient> clients = Collections.newSetFromMap(new IdentityHashMap<>());
	private final ExecutorService clientExecutor = Executors.newCachedThreadPool();

	public DmapServer(String componentId, UserData data, int port, Runnable errorCallback) throws IOException
	{
		this.componentId = componentId;
		this.serverKey = Keys.readPrivateKey(new File(String.format("keys/server/%s", componentId)));
		this.data = data;
		this.acceptor = new ClientAcceptor(port, this::onNewClient, errorCallback);
	}

	private void onNewClient(Socket socket)
	{
		DmapServerClient client = new DmapServerClient(componentId, serverKey, socket, data, this::onClientDone);
		clientExecutor.submit(client);

		synchronized(clients)
		{
			clients.add(client);
		}
	}

	private void onClientDone(DmapServerClient client)
	{
		synchronized(clients)
		{
			clients.remove(client);
		}
	}

	public void start()
	{
		acceptor.start();
	}

	public void shutdown()
	{
		acceptor.shutdown();

		synchronized(clients)
		{
			for(DmapServerClient client : clients)
				Utils.close(client);
		}

		clientExecutor.shutdown();

		try
		{
			clientExecutor.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
		}
		catch(InterruptedException ex)
		{
			// should never happen
			ex.printStackTrace();
		}
	}
}
