package dslab.protocol;

import java.util.List;

public class Message
{
	public String from;
	public List<String> to;
	public String subject;
	public String data;
	public String hash;

	@Override
	public String toString()
	{
		return "Message{" + "from='" + from + '\'' + ", to=" + to + ", subject='" + subject + '\'' + ", data='"
		       + data + '\'' + ", hash='" + hash + '\'' + '}';
	}
}
