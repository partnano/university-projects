package dslab.protocol;

import dslab.Utils;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class DmepServerClient implements Closeable, Runnable
{
	private final String domain;
	private final Predicate<String> userKnown;
	private final Socket socket;
	private final DmepMessageCallback messageCallback;
	private final Consumer<DmepServerClient> doneCallback;

	public DmepServerClient(String domain, Predicate<String> userKnown, Socket socket, DmepMessageCallback messageCallback, Consumer<DmepServerClient> doneCallback)
	{
		this.domain = domain;
		this.userKnown = userKnown;
		this.socket = socket;
		this.messageCallback = messageCallback;
		this.doneCallback = doneCallback;
	}

	private List<String> domainMatches(List<String> recipients)
	{
		if(domain == null)
			return recipients;

		List<String> result = new ArrayList<>();

		for(String recipient : recipients)
		{
			String[] parts = recipient.split("@");

			if(parts[1].equals(domain))
				result.add(parts[0]);
		}

		return result;
	}

	private List<String> unknownUsers(List<String> users)
	{
		if(userKnown == null)
			return new ArrayList<>();

		List<String> result = new ArrayList<>();

		for(String user : users)
			if(!userKnown.test(user))
				result.add(user);

		return result;
	}

	private List<String> knownUsers(List<String> users)
	{
		if(userKnown == null)
			return users;

		List<String> result = new ArrayList<>();

		for(String user : users)
			if(userKnown.test(user))
				result.add(user);

		return result;
	}

	public void run()
	{
		try(BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		    BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())))
		{
			writer.write("ok DMEP2.0\n");
			writer.flush();

			Message message = null;

			for(String line; (line = reader.readLine()) != null; writer.flush())
			{
				try
				{
					String[] args = line.split(" ", 2);
					String command = args[0];

					switch(command)
					{
					case "begin":
						message = new Message();
						break;

					case "from":
						message.from = args[1];
						break;

					case "to":
						message.to = Arrays.asList(args[1].split(","));

						List<String> matches = domainMatches(message.to);
						List<String> unknownUsers = unknownUsers(matches);

						if(unknownUsers.isEmpty())
							writer.write(String.format("ok %s\n", matches.size()));
						else
							writer.write(String.format("error unknown recipients %s\n", String.join(", ", unknownUsers)));

						continue;

					case "subject":
						message.subject = args[1];
						break;

					case "data":
						message.data = args[1];
						break;

					case "hash":
						message.hash = args[1];
						break;

					case "send":
						if(message.to == null)
						{
							writer.write("error no sender\n");
							continue;
						}

						if(message.from == null)
						{
							writer.write("error no recipient\n");
							continue;
						}

						if(message.subject == null)
						{
							writer.write("error no subject\n");
							continue;
						}

						if(message.data == null)
						{
							writer.write("error no data\n");
							continue;
						}

						messageCallback.invoke(message, knownUsers(domainMatches(message.to)));
						message = null;
						break;

					case "quit":
						writer.write("ok bye\n");
						writer.flush();
						Utils.close(socket);
						return;

					default:
						writer.write("error protocol error\n");
						continue;
					}

					writer.write("ok\n");
				}
				catch(ArrayIndexOutOfBoundsException ex)
				{
					writer.write("error invalid command format\n");
				}
				catch(NullPointerException ex)
				{
					writer.write("error begin command missing\n");
				}
			}
		}
		catch(IOException ex)
		{
			// shutdown or connection error, we don't care about either
		}
		finally
		{
			doneCallback.accept(this);
		}
	}

	@Override
	public void close() throws IOException
	{
		socket.close();
	}
}
