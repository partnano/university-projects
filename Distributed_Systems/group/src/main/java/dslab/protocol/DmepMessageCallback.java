package dslab.protocol;

import java.util.List;

public interface DmepMessageCallback
{
	void invoke(Message message, List<String> recipients);
}
