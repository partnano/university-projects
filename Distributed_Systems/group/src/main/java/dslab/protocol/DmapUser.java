package dslab.protocol;

import java.util.HashMap;
import java.util.Map;

public class DmapUser
{
	public final String name;
	public final String password;

	private int nextMessageId = 1;
	private final Map<Integer, Message> messages = new HashMap<>();

	public DmapUser(String name, String password)
	{
		this.name = name;
		this.password = password;
	}

	public void deliver(Message message)
	{
		synchronized(messages)
		{
			messages.put(nextMessageId++, message);
		}
	}

	public Map<Integer, Message> list()
	{
		Map<Integer, Message> copy = new HashMap<>();

		synchronized(messages)
		{
			copy.putAll(messages);
		}

		return copy;
	}

	public Message get(int id)
	{
		synchronized(messages)
		{
			return messages.get(id);
		}
	}

	public boolean delete(int id)
	{
		synchronized(messages)
		{
			return messages.remove(id) != null;
		}
	}
}
