package dslab.protocol;

import dslab.util.Config;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserData
{
	private final Map<String, DmapUser> users = new HashMap<>();

	public UserData(Config users)
	{
		for(String user : users.listKeys())
		{
			String password = users.getString(user);
			this.users.put(user, new DmapUser(user, password));
		}
	}

	public DmapUser get(String name)
	{
		return users.get(name);
	}

	public void deliverMessage(Message message, List<String> recipients)
	{
		for(String recipient : recipients)
			get(recipient).deliver(message);
	}
}
