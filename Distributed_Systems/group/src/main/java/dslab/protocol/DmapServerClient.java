package dslab.protocol;

import dslab.Utils;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.security.Key;
import java.util.Arrays;
import java.util.Base64;
import java.util.Map;
import java.util.function.Consumer;

public class DmapServerClient implements Closeable, Runnable
{
	private final String componentId;
	private final Key serverKey;
	private final Socket socket;
	private final UserData data;
	private final Consumer<DmapServerClient> onClientDone;

	private BufferedReader reader;
	private BufferedWriter writer;
	private Cipher aesEncrypter;
	private Cipher aesDecrypter;
	private boolean isEncrypted = false;

	public DmapServerClient(String componentId, Key serverKey, Socket socket, UserData data, Consumer<DmapServerClient> onClientDone)
	{
		this.componentId = componentId;
		this.serverKey = serverKey;
		this.socket = socket;
		this.data = data;
		this.onClientDone = onClientDone;
	}

	private String readLine() throws Exception
	{
		if(!isEncrypted)
			return reader.readLine();

		byte[] decoded = base64Decode(reader.readLine());
		byte[] decrypted = aesDecrypter.doFinal(decoded);
		return new String(decrypted, StandardCharsets.UTF_8);
	}

	private void writeLine(String message) throws Exception
	{
		if(!isEncrypted)
		{
			writer.write(message + "\r\n");
			writer.flush();
			return;
		}

		byte[] bytes = message.getBytes(StandardCharsets.UTF_8);
		byte[] encrypted = aesEncrypter.doFinal(bytes);
		writer.write(base64Encode(encrypted) + "\r\n");
		writer.flush();
	}

	private String base64Encode(byte[] data)
	{
		return new String(Base64.getEncoder().encode(data), StandardCharsets.UTF_8);
	}

	private byte[] base64Decode(String data)
	{
		return Base64.getDecoder().decode(data.getBytes(StandardCharsets.UTF_8));
	}

	@Override
	public void run()
	{
		try
		{
			reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));

			writeLine("ok DMAP2.0");

			DmapUser user = null;

			for(String line; (line = readLine()) != null;)
			{
				try
				{
					String[] args = line.split(" ");
					String command = args[0];

					switch(command)
					{
					case "startsecure":
						if(isEncrypted)
							break;

						writeLine(String.format("ok %s", componentId));

						byte[] decodedChallenge = base64Decode(readLine());
						Cipher rsa = Cipher.getInstance("RSA/NONE/OAEPWithSHA256AndMGF1Padding", "BC");
						rsa.init(Cipher.DECRYPT_MODE, serverKey);
						String decryptedChallenge = new String(rsa.doFinal(decodedChallenge), StandardCharsets.UTF_8);

						// ok <challenge> <key> <iv>
						String[] parts = decryptedChallenge.split(" ");
						String challenge = parts[1];
						byte[] key = base64Decode(parts[2]);
						byte[] iv = base64Decode(parts[3]);

						SecretKeySpec keySpec = new SecretKeySpec(key, "AES");
						IvParameterSpec ivSpec = new IvParameterSpec(iv);

						aesEncrypter = Cipher.getInstance("AES/CTR/NoPadding", "BC");
						aesEncrypter.init(Cipher.ENCRYPT_MODE, keySpec, ivSpec);

						aesDecrypter = Cipher.getInstance("AES/CTR/NoPadding", "BC");
						aesDecrypter.init(Cipher.DECRYPT_MODE, keySpec, ivSpec);

						isEncrypted = true;
						writeLine(String.format("ok %s", challenge));

						if(!"ok".equals(readLine()))
						{
							Utils.close(socket);
							return;
						}

						continue;

					case "login":
						user = data.get(args[1]);

						if(user == null)
						{
							writeLine("error unknown login");
							continue;
						}

						if(!args[2].equals(user.password))
						{
							user = null;
							writeLine("error invalid password");
							continue;
						}

						break;

					case "list":
						for(Map.Entry<Integer, Message> entry : user.list().entrySet())
						{
							int id = entry.getKey();
							Message message = entry.getValue();
							writeLine(String.format("%s %s %s", id, message.from, message.subject));
						}

						writeLine("ok");
						continue;

					case "show":
						int showId;

						try
						{
							showId = Integer.parseInt(args[1]);
						}
						catch(NumberFormatException ex)
						{
							writeLine("error invalid id");
							continue;
						}

						Message message = user.get(showId);

						if(message == null)
						{
							writeLine("error unknown message id");
							continue;
						}

						String recipients = message.to.toString();
						recipients = recipients.replaceAll("[\\[\\] ]", "");
						writeLine(String.format("from %s", message.from));
						writeLine(String.format("to %s", recipients));
						writeLine(String.format("subject %s", message.subject));
						writeLine(String.format("data %s", message.data));
						writeLine(String.format("hash %s", message.hash));
						writeLine("ok");
						continue;

					case "delete":
						int deleteId;

						try
						{
							deleteId = Integer.parseInt(args[1]);
						}
						catch(NumberFormatException ex)
						{
							writeLine("error invalid id");
							continue;
						}

						if(!user.delete(deleteId))
						{
							writeLine("error unknown message id");
							continue;
						}

						break;

					case "logout":
						if(user == null)
						{
							writeLine("error not logged in");
							continue;
						}

						user = null;
						break;

					case "quit":
						writeLine("ok bye");
						Utils.close(socket);
						return;

					default:
						writeLine("error protocol error");
						continue;
					}

					writeLine("ok");
				}
				catch(ArrayIndexOutOfBoundsException ex)
				{
					writeLine("error invalid command format");
				}
				catch(NullPointerException ex)
				{
					writeLine("error not logged in");
				}
			}
		}
		catch(Exception ex)
		{
			// shutdown or connection error, we don't care about either
		}
		finally
		{
			onClientDone.accept(this);
		}
	}

	@Override
	public void close()
	{
		Utils.close(reader);
		Utils.close(writer);
		Utils.close(socket);
	}
}
