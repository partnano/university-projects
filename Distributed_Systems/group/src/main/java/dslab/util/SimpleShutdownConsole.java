package dslab.util;

import dslab.Utils;

import java.io.*;

public class SimpleShutdownConsole
{
	private final BufferedReader conin;
	private final PrintStream conout;

	public SimpleShutdownConsole(InputStream in, PrintStream out)
	{
		this.conin = new BufferedReader(new InputStreamReader(in));
		this.conout = out;
	}

	public void run()
	{
		try
		{
			for(String line; (line = conin.readLine()) != null; )
			{
				if(line.equals("shutdown"))
					return;

				conout.println("invalid command");
			}
		}
		catch(IOException ex)
		{
			conout.println("failed to read console input");
		}
	}

	public void close()
	{
		Utils.close(conin);
	}
}
