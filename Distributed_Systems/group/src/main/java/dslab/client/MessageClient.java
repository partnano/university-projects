package dslab.client;

import at.ac.tuwien.dsg.orvell.Shell;
import at.ac.tuwien.dsg.orvell.StopShellException;
import at.ac.tuwien.dsg.orvell.annotation.Command;
import dslab.ComponentFactory;
import dslab.Utils;
import dslab.util.Config;
import dslab.util.Keys;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;

public class MessageClient implements IMessageClient, Runnable, AutoCloseable
{
	private Config config;
	private String componentId;

	private Shell shell;
	private Socket socket;
	private BufferedReader reader;
	private BufferedWriter writer;

	private InputStream in;

	private SecretKey aesSecret;
	private Cipher aesEncrypter;
	private Cipher aesDecrypter;
	private boolean isEncrypted = false;

	private Mac hmac;

	/**
	 * Creates a new client instance.
	 *
	 * @param componentId the id of the component that corresponds to the Config resource
	 * @param config the component config
	 * @param in the input stream to read console input from
	 * @param out the output stream to write console output to
	 */
	public MessageClient(String componentId, Config config, InputStream in, PrintStream out) throws Exception
	{
		this.config = config;
		this.componentId = componentId;

		this.socket = new Socket();

		this.shell = new Shell(in, out);
		this.shell.register(this);
		this.shell.setPrompt(componentId + " > ");

		this.in = in;

		Key hmacKey = Keys.readSecretKey(new File("keys/hmac.key"));
		this.hmac = Mac.getInstance("HmacSHA256", "BC");
		this.hmac.init(hmacKey);
	}

	private String readLine() throws Exception
	{
		if(!isEncrypted)
			return reader.readLine();

		byte[] decoded = base64Decode(reader.readLine());
		byte[] decrypted = aesDecrypter.doFinal(decoded);
		return new String(decrypted, StandardCharsets.UTF_8);
	}

	private void writeLine(String message) throws Exception
	{
		if(!isEncrypted)
		{
			writer.write(message + "\r\n");
			writer.flush();
			return;
		}

		byte[] bytes = message.getBytes(StandardCharsets.UTF_8);
		byte[] encrypted = aesEncrypter.doFinal(bytes);
		writer.write(base64Encode(encrypted) + "\r\n");
		writer.flush();
	}

	private String base64Encode(byte[] data)
	{
		return new String(Base64.getEncoder().encode(data), StandardCharsets.UTF_8);
	}

	private byte[] base64Decode(String data)
	{
		return Base64.getDecoder().decode(data.getBytes(StandardCharsets.UTF_8));
	}

	@Override
	public void run()
	{
		try
		{
			int mailboxPort = config.getInt("mailbox.port");
			String mailboxHost = config.getString("mailbox.host");
			socket.connect(new InetSocketAddress(mailboxHost, mailboxPort));

			writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
			reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));

			if (!"ok DMAP2.0".equals(readLine()))
			{
				shell.err().println("Protocol error");
				return;
			}

			writeLine("startsecure");

			String componentId = readLine().split(" ", 2)[1];

			SecureRandom random = new SecureRandom();

			byte[] challenge = new byte[32];
			random.nextBytes(challenge);

			byte[] iv = new byte[16];
			random.nextBytes(iv);

			KeyGenerator keygen = KeyGenerator.getInstance("AES");
			keygen.init(256);
			aesSecret = keygen.generateKey();

			Key serverKey = Keys.readPublicKey(new File(String.format("keys/client/%s.pub", componentId)));
			Cipher rsa = Cipher.getInstance("RSA/NONE/OAEPWithSHA256AndMGF1Padding", "BC");
			rsa.init(Cipher.ENCRYPT_MODE, serverKey);

			String challengeLine = String.join(" ", "ok", base64Encode(challenge), base64Encode(aesSecret.getEncoded()), base64Encode(iv));
			writeLine(base64Encode(rsa.doFinal(challengeLine.getBytes(StandardCharsets.UTF_8))));

			IvParameterSpec ivspec = new IvParameterSpec(iv);
			SecretKeySpec keyspec = new SecretKeySpec(aesSecret.getEncoded(), "AES");

			aesEncrypter = Cipher.getInstance("AES/CTR/NoPadding", "BC");
			aesEncrypter.init(Cipher.ENCRYPT_MODE, keyspec, ivspec);

			aesDecrypter = Cipher.getInstance("AES/CTR/NoPadding", "BC");
			aesDecrypter.init(Cipher.DECRYPT_MODE, keyspec, ivspec);

			isEncrypted = true;

			byte[] challengeResponse = base64Decode(readLine().split(" ")[1]);

			if(!Arrays.equals(challenge, challengeResponse))
			{
				shell.err().println("challenge doesn't match");
				return;
			}

			writeLine("ok");
			writeLine(String.format("login %s %s", config.getString("mailbox.user"), config.getString("mailbox.password")));

			if(!readLine().equals("ok"))
				shell.out().println("Not Hooray!");

			shell.run();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Override
	@Command
	public void inbox()
	{
		try
		{
			writeLine("list");

			List<String> ids = new ArrayList<>();

			for(String line; !"ok".equals(line = readLine());)
			{
				ids.add(line.split(" ", 2)[0]);
			}

			for(String id : ids)
			{
				writeLine(String.format("show %s", id));

				String from = readLine().split(" ", 2)[1];
				String to = readLine().split(" ", 2)[1];
				String subject = readLine().split(" ",2)[1];
				String message = readLine().split(" ", 2)[1];
				String[] hashLine = readLine().split(" ", 2);
				String hash = hashLine.length == 2 ? hashLine[1] : "";
				readLine(); // ignore ok

				shell.out().println(String.format("%s. From %s to %s: Subject=%s, Message=%s, Hash=%s", id, from, to, subject, message, hash));
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Override
	@Command
	public void delete(String id)
	{
		try
		{
			writeLine(String.format("delete %s", id));
			String response = readLine();

			if(response.startsWith("ok"))
				shell.out().println("ok");
			else
				shell.out().println(response);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Override
	@Command
	public void verify(String id)
	{
		try
		{
			writeLine(String.format("show %s", id));

			String fromLine = readLine();
			String from = fromLine.split(" ", 2)[1];

			if(fromLine.startsWith("error"))
			{
				shell.out().println(fromLine);
				return;
			}

			String to = readLine().split(" ", 2)[1];
			String subject = readLine().split(" ",2)[1];
			String message = readLine().split(" ", 2)[1];
			String[] hashLine = readLine().split(" ", 2);
			String hash = hashLine.length == 2 ? hashLine[1] : "";
			readLine(); // ignore ok

			String computedHash = hash(from, to, subject, message);

			if(!computedHash.equals(hash))
				shell.out().println("error invalid hash");
			else
				shell.out().println("ok");
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	private String hash(String from, String to, String subject, String message)
	{
		String data = String.join("\n", from, to, subject, message);
		byte[] hash = hmac.doFinal(data.getBytes(StandardCharsets.UTF_8));
		return base64Encode(hash);
	}

	private boolean sendMessage(String to, String subject, String message)
	{
		try(Socket socket = new Socket())
		{
			socket.connect(new InetSocketAddress(config.getString("transfer.host"), config.getInt("transfer.port")));

			String from = config.getString("transfer.email");
			BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
			writer.write("begin\n");
			writer.write("from " + from + "\n");
			writer.write("to " + to + "\n");
			writer.write("subject " + subject + "\n");
			writer.write("data " + message + "\n");
			writer.write("hash " + hash(from, to, subject, message) + "\n");
			writer.write("send\n");
			writer.write("quit\n");
			writer.flush();

			BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));

			for(String line; (line = reader.readLine()) != null;)
			{
				if(line.startsWith("error"))
					return false;
			}

			return true;
		}
		catch(IOException ex)
		{
			return false;
		}
	}

	@Override
	@Command
	public void msg(String to, String subject, String data)
	{
		if(sendMessage(to, subject, data))
			shell.out().println("ok");
		else
			shell.out().println("error");
	}

	@Override
	@Command
	public void shutdown()
	{
		throw new StopShellException();
	}

	@Override
	public void close()
	{
		Utils.close(socket);
	}

	public static void main(String[] args) throws Exception
	{
		IMessageClient client = ComponentFactory.createMessageClient(args[0], System.in, System.out);
		client.run();
	}
}
