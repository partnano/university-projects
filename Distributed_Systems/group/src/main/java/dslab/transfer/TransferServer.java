package dslab.transfer;

import dslab.ComponentFactory;
import dslab.protocol.DmepForwarder;
import dslab.protocol.DmepServer;
import dslab.util.Config;
import dslab.util.SimpleShutdownConsole;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketAddress;

public class TransferServer implements ITransferServer, Runnable
{
	private final SimpleShutdownConsole console;
	private final DmepServer server;
	private final DmepForwarder forwarder;

	/**
	 * Creates a new server instance.
	 *
	 * @param componentId the id of the component that corresponds to the Config resource
	 * @param config      the component config
	 * @param in          the input stream to read console input from
	 * @param out         the output stream to write console output to
	 */
	public TransferServer(String componentId, Config config, InputStream in, PrintStream out) throws IOException
	{
		this.console = new SimpleShutdownConsole(in, out);

		int serverPort = config.getInt("tcp.port");
		String monitoringHost = config.getString("monitoring.host");
		int monitoringPort = config.getInt("monitoring.port");
		SocketAddress monitoringAddress = new InetSocketAddress(InetAddress.getByName(monitoringHost), monitoringPort);

		this.forwarder = new DmepForwarder(serverPort, config, monitoringAddress);
		this.server = new DmepServer(null, null, serverPort, (msg, l) -> forwarder.queue(msg), console::close);
	}

	@Override
	public void run()
	{
		server.start();
		forwarder.start();
		console.run();
		shutdown();
	}

	@Override
	public void shutdown()
	{
		server.shutdown();
		forwarder.shutdown();
	}

	public static void main(String[] args) throws Exception
	{
		ITransferServer server = ComponentFactory.createTransferServer(args[0], System.in, System.out);
		server.run();
	}
}
