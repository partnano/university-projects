/**
 * @file mrna-server.c
 * @author Bernhard Wick - 1525699
 * @date 4 Jan 2017
 *
 * @brief Client module
 *
 * Client module for mrna calculation.
 */

#include <sys/types.h>
#include <ctype.h>

#include "mrna.h"

/**
 * Name of the program
 */
static char *progname;

/**
 * Process id of the program
 */
static int pid;

/**
 * Shared memory object
 */
struct mrna_shared *shared;

/**
 * Breakout flag, for signal handling
 */
int breakout = 0;

/**
 * Semaphores
 */
sem_t *sem_s;
sem_t *sem_c;
sem_t *sem_a;

/**
 * Helper function for closing file descriptor
 * @brief Helper function to close shared memory fd.
 * @param shmfd Shared memory file descriptor
 * @return 0 on success, -1 on error
 */
static
int try_close_shmfd (int shmfd) {
    if (close (shmfd) == -1) {
	(void) fprintf (stderr, "%s: Closing mmap file descriptor failed!\n", progname);
	return -1;
    }

    return 0;
}

/**
 * Helper functions to close shared resources
 * @brief Helper function to close shared memory objects and semaphores.
 * @return 0 on success, -1 on error
 */
static
int try_clean (void) {
    if (munmap (shared, sizeof *shared) == -1) {
    	(void) fprintf (stderr, "%s: Unmap shared memory failed!\n", progname);
    	return -1;
    }
    
    if (sem_close (sem_s) == -1 || sem_close (sem_c) == -1 || sem_close (sem_a)) {
	(void) fprintf (stderr, "%s: Closing semaphores failed!\n", progname);
	return -1;
    }

    return 0;
}

/**
 * Signal handler
 * @brief Signal handler to handle signals
 * @param signo Signal identifier
 */
static
void handle_signals (int signo) {
    breakout = 1;
}

/**
 * Retrive shared resources
 * @brief Function to retrieve shared resources into global vars
 * @return 0 on success, -1 on error;
 */
static
int get_shared_resources (void) {
    // get shared resources
    
    int shmfd = shm_open (SHM_NAME, O_RDWR, PERMISSION);

    if (shmfd == -1) {
	(void) fprintf (stderr,
			"%s: Opening shared memory fd failed! Memory existing? Server started?\n",
			progname);
	return -1;
    }

    if (ftruncate (shmfd, sizeof *shared) == -1) {
	(void) fprintf (stderr, "%s: Truncating failed!\n", progname);
	return -1;
    }

    shared = mmap(NULL, sizeof *shared, PROT_READ | PROT_WRITE, MAP_SHARED, shmfd, 0);

    if (shared == MAP_FAILED) {
	(void) fprintf (stderr, "%s: Mapping shared memory failed!\n", progname);
	(void) try_close_shmfd (shmfd);
	return -1;
    }

    if (try_close_shmfd (shmfd) == -1)
	return -1;

    // semaphores
    sem_s = sem_open (SEM_SERVER, 0);
    sem_c = sem_open (SEM_CLIENT, 0);
    sem_a = sem_open (SEM_ANSWER, 0);

    if (sem_s == SEM_FAILED || sem_c == SEM_FAILED || sem_a == SEM_FAILED) {
	(void) fprintf (stderr, "%s: Opening semaphores failed!\n", progname);
	(void) try_clean ();
	return -1;
    }

    return 0;
}

/**
 * Send new sequence
 * @brief Function to send new base sequence to server
 * @return 0 on success, -1 on error
 */
static
int send_new_sequence (void) {

    (void) printf ("Enter sequence (double enter to quit):\n");
	    
    // read input
    char line[256];
    char input[4096];

    memset (input, 0, sizeof (input));
	    
    int _c = 0;
    while (fgets (line, sizeof (input), stdin)) {
	if (breakout == 1)
	    break;
	
	if (strcmp (line, "\n") == 0) {
	    _c++;

	    if (_c >= 1)
		break;
	}

	char new_line[256] = "";
	int i = 0;
	int j = 0;
	while (line[i] != '\n' && line[i] != '\0') {

	    // everything to lowercase
	    char c = tolower(line[i]);

	    // only valid characters go through
	    if (c == 'a' || c == 'c' || c == 'u' || c == 'g') {
		new_line[j] = c;
		++j;
	    }

	    ++i;
	}

	strcat (input, new_line);
    }

    if (breakout == 1)
	return 0;
    
    if (sem_wait (sem_c) == -1 && errno == EINVAL) {
	(void) fprintf (stderr, "%s: No valid semaphore.", progname);
	return -1;
    }
	    
    shared -> pid = pid;
    shared -> command = 's';

    memset (shared -> sequence, 0, sizeof (shared -> sequence));
    strcpy (shared -> sequence, input);
	    
    return 0;
}

/**
 * Request calculated prot sequence.
 * @brief Function to request a calculated prot sequence from the server.
 * @return 0 on success, -1 on error
 */
static
int request_prot_sequence (void) {

    if (sem_wait (sem_c) == -1 && errno == EINVAL) {
	(void) fprintf (stderr, "%s: No valid semaphore.", progname);
	return -1;
    }
	    
    shared -> pid = pid;
    shared -> command = 'n';

    memset (shared -> sequence, 0, sizeof (shared -> sequence));

    if (sem_post (sem_s) == -1 || (sem_wait (sem_a) == -1 && errno == EINVAL)) {
	(void) fprintf (stderr, "%s: No valid semaphore.", progname);
	return -1;
    }

    if (shared -> sequence[0] != 0)
	(void) fprintf (stdout,
			"Protein sequence found [%i/%i] to [%i/%i]: %s\n",
			shared -> prot_start, shared -> seq_length,
			shared -> prot_end, shared -> seq_length, shared -> sequence);
    else
	(void) fprintf (stdout, "End of sequence, send r to reset\n");
	    
    return 0;
}

/**
 * Reset cursor.
 * @brief Function to reset sequence cursor on the server.
 * @return 0 on success, -1 on error
 */
static
int reset_cursor (void) {

    if (sem_wait (sem_c) == -1 && errno == EINVAL) {
	(void) fprintf (stderr, "%s: No valid semaphore.", progname);
	return -1;
    }
	    
    shared -> pid = pid;
    shared -> command = 'r';

    memset (shared -> sequence, 0, sizeof (shared -> sequence));

    return 0;
}

/**
 * Quit client.
 * @brief Function to quit client, close resources and inform server.
 * @return 0 on success, -1 on error
 */
static
int quit_app (void) {
    if (sem_wait (sem_c) == -1 && errno == EINVAL) {
	(void) fprintf (stderr, "%s: No valid semaphore.", progname);
	return -1;
    }
	    
    shared -> pid = pid;
    shared -> command = 'q';

    memset (shared -> sequence, 0, sizeof (shared -> sequence));
	    
    if (sem_post (sem_s) == -1) {
	(void) fprintf (stderr, "%s: No valid semaphore.", progname);
	return -1;
    }

    if (try_clean () == -1)
	return -1;

    return 0;
}

/**
 * program entry point
 * @param argc Argument counter
 * @param argv Arguments
 * @return Returns EXIT_SUCCESS on success, EXIT_FAILURE on error
 */
int main (int argc, char **argv) {
    progname = argv[0];
    pid = getpid();

    if (argc != 1) {
	(void) fprintf (stderr, "%s: Program must not have arguments!\n", progname);
	return EXIT_FAILURE;
    }

    /* SIGNAL HANDLER */
    struct sigaction s;
    s.sa_handler = handle_signals;

    sigaction (SIGINT, &s, NULL);
    sigaction (SIGTERM, &s, NULL);

    if (get_shared_resources () == -1)
	return EXIT_FAILURE;
    
    (void) printf ("Welcome! Your pid: %i | Available commands:\n\
    s - submit a new mRNA sequence\n\
    n - show next protein sequence in active mRNA sequence\n\
    r - reset active mRNA sequence\n\
    q - close this client\n\n", pid);

    char command[128];

    while (1) {	
	
	(void) printf ("Enter new command: ");
	
	fgets (command, 128, stdin);
	
	if (command[0] == 's') {

	    if (send_new_sequence () == -1) {
		(void) try_clean ();
		return EXIT_FAILURE;
	    }
	    
	} else if (command[0] == 'n') {

	    if (request_prot_sequence () == -1) {
		(void) try_clean ();
		return EXIT_FAILURE;
	    }
	    
	} else if (command[0] == 'r') {

	    if (reset_cursor () == -1) {
		(void) try_clean ();
		return EXIT_FAILURE;
	    }
	    
	} else if (command[0] == 'q') {

	    if (quit_app () == -1)
		return EXIT_FAILURE;
	    
	    break;

	} else {
	    // debug
	    //(void) fprintf (stderr, "Unknown command\n");
	}

	if (breakout == 1) {
	    if (quit_app () == -1)
		return EXIT_FAILURE;

	    return EXIT_SUCCESS;
	}
	
	if (sem_post (sem_s) == -1) {
		(void) fprintf (stderr, "%s: No valid semaphore.", progname);
		return EXIT_FAILURE;
	}
    }
    
    return EXIT_SUCCESS;
}
