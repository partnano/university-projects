/**
 * @file mrna-server.c
 * @author Bernhard Wick - 1525699
 * @date 4 Jan 2017
 *
 * @brief server module
 *
 * Server module for mrna calculation.
 */

#include "mrna.h"

#define MAX_CLIENTS (128)

/**
 * Name of the program
 */
static char *progname; 

/**
 * Shared memory object
 */
struct mrna_shared *shared;

/**
 * Shared semaphores
 */
sem_t *sem_s;
sem_t *sem_c;
sem_t *sem_a;

/**
 * Breakout flag, for signal handling
 */
int breakout = 0;

/**
 * Server sided data struct
 */
struct mrna_data {
    int client_pid;
    int cursor;
    int seq_length;
    char sequence [MAX_DATA];
};

/**
 * Server sided data array
 */
struct mrna_data data[MAX_CLIENTS];

/**
 * Helper functions to close shared resources
 * @brief Helper function to close and unlink shared memory objects and semaphores.
 * @return 0 on success, -1 on error
 */
static
int try_clean (void) {
    if (munmap (shared, sizeof *shared) == -1) {
    	(void) fprintf (stderr, "%s: Unmap shared memory failed!\n", progname);
    	return -1;
    }

    if (shm_unlink (SHM_NAME) == -1) {
    	(void) fprintf (stderr, "%s: Unlinking shared memory failed!\n", progname);
    	return -1;
    }

    if (sem_close (sem_s) == -1 || sem_close (sem_c) == -1 || sem_close (sem_a) == -1) {
	(void) fprintf (stderr, "%s: Closing semaphores failed!\n", progname);
	return -1;
    }

    if (sem_unlink (SEM_SERVER) == -1 || sem_unlink (SEM_CLIENT) == -1 || sem_unlink (SEM_ANSWER) == -1) {
	(void) fprintf (stderr, "%s: Unlinking semaphores failed!\n", progname);
	return -1;
    }

    return 0;
}

/**
 * Helper function for closing file descriptor
 * @brief Helper function to close shared memory fd.
 * @param shmfd Shared memory file descriptor
 * @return 0 on success, -1 on error
 */
static
int try_close_shmfd (int shmfd) {
    if (close (shmfd) == -1) {
	(void) fprintf (stderr, "%s: Closing mmap file descriptor failed!", progname);
	return -1;
    }

    return 0;
}

/**
 * Helper function
 * @brief Function to reset shared memory object to default values
 */
static
void reset_shared (void) {
    shared -> pid = -1;
    shared -> command = 0;
    memset (shared -> sequence, 0, sizeof (shared -> sequence));
    shared -> prot_start = 0;
    shared -> prot_end = 0;
    shared -> seq_length = 0;
}

/**
 * Signal handler
 * @brief Signal handler to handle signals
 * @param signo Signal identifier
 */
static
void handle_signals (int signo) {
    breakout = 1;
}

/**
 * Create and retrieve shared resources
 * @brief Function to create and retrieve shared resources into global vars
 * @return 0 on success, -1 on error;
 */
static
int create_shared_objects (void) {    
    int shmfd = shm_open (SHM_NAME, O_RDWR | O_CREAT, PERMISSION);

    if (shmfd == -1) {
	(void) fprintf (stderr, "%s: Opening file descriptor for mmap failed!\n", progname);
	return -1;
    }

    if (ftruncate (shmfd, sizeof *shared) == -1) {
	(void) fprintf (stderr, "%s: Trunctating failed!\n", progname);
	(void) try_close_shmfd (shmfd);
	return -1;
    }

    shared = mmap (NULL, sizeof *shared, PROT_READ | PROT_WRITE, MAP_SHARED, shmfd, 0);

    if (shared == MAP_FAILED) {
	(void) fprintf (stderr, "%s: Making shared memory failed!\n", progname);
	(void) try_close_shmfd (shmfd);
	return -1;
    }

    if (try_close_shmfd (shmfd) == -1)
	return -1;

    // create semaphores
    sem_s = sem_open (SEM_SERVER, O_CREAT | O_EXCL, 0600, 0);
    sem_c = sem_open (SEM_CLIENT, O_CREAT | O_EXCL, 0600, 1);
    sem_a = sem_open (SEM_ANSWER, O_CREAT | O_EXCL, 0600, 0);

    if (sem_s == SEM_FAILED || sem_c == SEM_FAILED || sem_a == SEM_FAILED) {
	(void) fprintf (stderr, "%s: Opening semaphores failed!\n", progname);
	(void) try_clean ();
	return -1;
    }

    return 0;
}

/**
 * Save new sequence
 * @brief Function to save new sequence sent by client.
 */
static
void save_new_sequence (void) {
    // check if client pid already exists, or use empty entry
    int id;
    for (int i = 0; i < MAX_CLIENTS; i++) {
	if (shared -> pid == data[i].client_pid || data[i].client_pid == -1) {
	    id = i;
	    break;
	}
    }
	    
    // overwrite / create new sequence entry in struct array
    data[id].client_pid = shared -> pid;
    data[id].cursor = 0;

    memset (data[id].sequence, 0, sizeof(data[id].sequence));
    strcpy (data[id].sequence, shared -> sequence);

    // determine sequence length
    for (int i = 0; i < MAX_DATA; i++) {
	if (data[id].sequence[i] == 0) {
	    data[id].seq_length = i;
	    break;
	}
    }

    //debug
    (void) fprintf (stdout, "Received sequence from client %i:\n%s [0/%i]\n",
		    data[id].client_pid, data[id].sequence, data[id].seq_length);

    reset_shared ();
}

/**
 * Calculate protein sequence
 * @brief Function to calculate protein sequence from client provided base.
 * @return 0 on success, -1 on error, -2 on continue case.
 */
static
int prot_sequence (void) {
    // suppress double entry
    if (shared -> pid == -1) {
	if (sem_post (sem_c) == -1) {
	    (void) fprintf (stderr, "%s: Something went wrong with client semaphore.", progname);
	    try_clean ();
	    return -1;
	}
	return -2;
    }
	    
    int id = -1;
    for (int i = 0; i < MAX_CLIENTS; i++) {
	if (data[i].client_pid == shared -> pid) {
	    id = i;
	    break;
	}
    }

    if (id == -1)
	return -2;

    int record = 0;
    char out[MAX_DATA];
    for (int i = data[id].cursor, j = -1; i < MAX_DATA -2;) {

	if (data[id].sequence[i +2] == '\0')
	    break;
	
	int codon[3];
	for (int k = 0; k < 3; k++) {
	    switch (data[id].sequence[i +k]) {
		case 'u':
		    codon[k] = U;
		    break;
		case 'c':
		    codon[k] = C;
		    break;
		case 'a':
		    codon[k] = A;
		    break;
		case 'g':
		    codon[k] = G;
		    break;
		default:
		    (void) fprintf (stderr,
				    "%s: Unknown case in protein calc. This shouldn't be possible.",
				    progname);
	    }
	}

	char cod = gen_code[codon[0]][codon[1]][codon[2]];

	if (record == 1) {
	    out[j] = cod;
	}

	if (cod == 'M' && record == 0) {
	    shared -> prot_start = i +3;
	    record = 1;
	}

	if (cod == 0 && record == 1) {
	    data[id].cursor = i +3;
	    strcpy (shared -> sequence, out);
	    break;
	}

	if (record == 1) {
	    i += 3;
	    j++;
	} else
	    i++;
    }
	    
    (void) fprintf (stdout, "Sharing calculated protein sequence to client %i\n", shared -> pid);
	    
    shared -> pid = -1;
    shared -> prot_end = data[id].cursor -3;
    shared -> seq_length = data[id].seq_length;
	    
    if (sem_post (sem_a) == -1) {
	(void) fprintf (stderr, "%s: Something went wrong with answer semaphore.", progname);
	try_clean ();
	return -1;	
    }

    return 0;
}

/**
 * Reset client cursor.
 * @brief Function to reset client cursor to 0.
 */
static
void reset_cursor (void) {

    // search for correct client and reset
    for (int i = 0; i < MAX_CLIENTS; i++) {
	if (data[i].client_pid == shared -> pid) {

	    data[i].cursor = 0;

	    // debug
	    (void) fprintf (stdout, "Reset cursor of client %i\n", data[i].client_pid);
		    
	    break;
	}
    }
	    
    reset_shared();	
}

/**
 * Remove client.
 * @brief Function to remove client from server data.
 */
static
void remove_client (void) {
    for (int i = 0; i < MAX_CLIENTS; i++) {
	if (data[i].client_pid == shared -> pid) {

	    data[i].client_pid = -1;
	    data[i].cursor = 0;
	    data[i].seq_length = 0;

	    memset (data[i].sequence, 0, sizeof (data[i].sequence));

	    // debug
	    (void) fprintf (stdout, "Removed client %i from server data\n", shared -> pid);

	    break;
	}
    }
	    
    reset_shared();
}

/**
 * program entry point
 * @param argc Argument counter
 * @param argv Arguments
 * @return Returns EXIT_SUCCESS on success, EXIT_FAILURE on error
 */
int main (int argc, char **argv) {
    progname = argv[0];

    if (argc != 1) {
	(void) fprintf (stderr, "%s: This program must not have arguments!\n", progname);
	return EXIT_FAILURE;
    }

    /* SIGNAL HANDLER */
    struct sigaction s;
    s.sa_handler = handle_signals;

    sigaction (SIGINT, &s, NULL);
    sigaction (SIGTERM, &s, NULL);

    if (create_shared_objects () == -1)
	return EXIT_FAILURE;
    
    // GAME LOGIC
    for (int i = 0; i < MAX_CLIENTS; i++)
	data[i].client_pid = -1;
    
    // GAME LOOP
    while (1) {
	if (breakout == 1)
	    break;
	
	if (sem_wait (sem_s) == -1 && errno == EINVAL) {
	    (void) fprintf (stderr, "%s: Something went wrong with server semaphore.", progname);
	    (void) try_clean ();
	    return EXIT_FAILURE;
	}

	if (shared -> command == 's')
	    save_new_sequence ();
	    
	else if (shared -> command == 'n') {
	    int _i = prot_sequence ();
	    
	    if (_i == -2)
		continue;
	    else if (_i == -1)
		return EXIT_FAILURE;
	    
	} else if (shared -> command == 'r')
	    reset_cursor ();
	    
	else if (shared -> command == 'q')
	    remove_client ();
	
	else {
	    // debug
	    //(void) fprintf (stderr, "Unknown command\n");
	    reset_shared();
	}
	
	if (sem_post (sem_c) == -1) {
	    (void) fprintf (stderr, "%s: Something went wrong with client semaphore.", progname);
	    (void) try_clean ();
	    return EXIT_FAILURE;
	}
    }
    
    // clean resources
    if (try_clean () == -1)
	return EXIT_FAILURE;

    return EXIT_SUCCESS;
}
