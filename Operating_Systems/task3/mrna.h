// mrna header

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <semaphore.h>
#include <fcntl.h>

#define SHM_NAME "/1525699_res"
#define MAX_DATA (256)
#define PERMISSION (0600)

#define SEM_SERVER "/1525699_sem_server"
#define SEM_CLIENT "/1525699_sem_client"
#define SEM_ANSWER "/1525699_sem_answer"

/**
 * Shared object struct.
 */
struct mrna_shared {
    int pid;
    char command;
    char sequence [MAX_DATA];
    int prot_end;
    int prot_start;
    int seq_length;
};

/**
 * mRNA code definition
 */
const int U = 0;
const int C = 1;
const int A = 2;
const int G = 3;
char gen_code[4][4][4] =
    {
	{
	    { 'F', 'F', 'L', 'L' }, // UU
	    { 'S', 'S', 'S', 'S' }, // UC
	    { 'Y', 'Y',  0,   0  }, // UA
	    { 'C', 'C',  0,  'W' }  // UG
	}, 
	{
	    { 'L', 'L', 'L', 'L' }, // CU
	    { 'P', 'P', 'P', 'P' }, // CC
	    { 'H', 'H', 'Q', 'Q' }, // CA
	    { 'R', 'R', 'R', 'R' }  // CG
	},
	{
	    { 'I', 'I', 'I', 'M' }, // AU
	    { 'T', 'T', 'T', 'T' }, // AC
	    { 'N', 'N', 'K', 'K' }, // AA
	    { 'S', 'S', 'R', 'R' }  // AG
	},
	{
	    { 'V', 'V', 'V', 'V' }, // GU
	    { 'A', 'A', 'A', 'A' }, // GC
	    { 'D', 'D', 'E', 'E' }, // GA
	    { 'G', 'G', 'G', 'G' }  // GG
	},
    };
	
