/*! 
 * mastermind client
 * to communicate and play with the server
 *
 * @author Bernhard Wick - 1525699
 * @brief the client accompanying the server, playing mastermind with it
 * SYNOPSIS: client hostname port
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include <unistd.h>

enum color {
    beige, darkblue,
    green, orange,
    red, black,
    violet, white
};

/**
 * @brief Function to calculate and embed the parity bit into the outgoing packet.
 * @param input The input packet to calculate parity bit on
 * @return new packet with calculated parity bit
 */
uint16_t calc_parity (uint16_t input) {
    uint16_t parity = 0;
    for (size_t i = 0; i < 15; i++) {
	parity ^= (input >> i) & 1;	
    }

    input = (parity << 15) | input;
    return input;
}

// TODO: algorithm of knuth?
/**
 * @brief Function to build outgoing packet, depending on incoming packet
 * @param incoming From server coming packet
 * @return fully built packet, ready to send to server
 */
uint16_t build_msg (uint8_t incoming) {    

    // debug
    //(void) printf ("%i\n", incoming);
    
    uint16_t msg = 0;
    // lets build wwrgb! -> DUMMY
    msg = beige << 12;
    msg = (green << 9) | msg;
    msg = (red << 6) | msg;
    msg = (white << 3) | msg;
    msg = white | msg;

    // calc parity into msg
    msg = calc_parity (msg);
    return msg;
}

/**
 * @brief Function to create and return socket
 * @param progname Program name, for messages
 * @param hostname Hostname to connect to
 * @param port_str Port number in string format
 * @return Connected socket or error number
 */
int create_socket (char *progname, char *hostname, char *port_str) {

    // socket logic
    int sockfd = -1;
    struct addrinfo hints;
    struct addrinfo *ai;

    hints.ai_flags = 0;
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = IPPROTO_TCP;

    int err = getaddrinfo (hostname, port_str, &hints, &ai);
    if (err != 0) {
	(void) fprintf (stderr, "%s, ERROR:  %s\n", progname, gai_strerror(err));
        return EXIT_FAILURE;
    }

    if (ai == NULL) {
	(void) fprintf (stderr, "%s, ERROR: could not resolve hostname. '%s'\n",
			progname, hostname);
	return EXIT_FAILURE;
    }

    sockfd = socket (ai -> ai_family, ai -> ai_socktype, ai -> ai_protocol);
    if (sockfd < 0) {
	(void) fprintf (stderr, "%s, ERROR: could not create socket.\n", progname);
	freeaddrinfo (ai);
	return EXIT_FAILURE;
    }

    if (connect (sockfd, ai -> ai_addr, ai -> ai_addrlen) < 0) {
	(void) close (sockfd);
	freeaddrinfo (ai);
	(void) fprintf (stderr, "%s, ERROR: connection failed.\n", progname);
	return EXIT_FAILURE;
    }

    freeaddrinfo (ai);

    return sockfd;
}

/**
 * @brief Function that manages communication (sending / receving packets).
 * @param sockfd With server connected socket, ready to send packets on
 * @return exit code
 */
int communicate (int sockfd) {

    int j = 1;
    uint8_t incoming;
    uint16_t outgoing = build_msg (0);
    for (int i = 0; i <= 35; ++i) {

	int s1 = send (sockfd, &outgoing, sizeof (outgoing), 0);
	int s2 = 3;
	if (s1 <= 0 && s2 <= 0) {
	    (void) fprintf (stderr, "error while sending, %i, %i\n", s1, s2);
	    return EXIT_FAILURE;
	}
	
	int r = recv (sockfd, &incoming, sizeof (incoming), 0);
	if (r <= 0 || incoming >= 64) {
	    if (incoming >= 192) {
		(void) fprintf (stderr, "max tries and parity error, %i, %i, %i\n", r, j, incoming);
		return 4;
	    } else if (incoming >= 128) {
		(void) fprintf (stderr, "max tries, %i, %i, %i\n", r, j, incoming);
		return 3;
	    } else {
		(void) fprintf (stderr, "parity error, %i, %i, %i\n", r, j, incoming);
		return 2;
	    }

	    // shouldn't happen
	    return EXIT_FAILURE;
	} else if (incoming == 5) {
	    (void) fprintf (stdout, "WON in %i rounds\n", j);
	    return EXIT_SUCCESS;
	}
	
	outgoing = build_msg (incoming);

	j++;
    }

    // this realistically shouldn't happen
    return EXIT_FAILURE;
}

/**
 * @brief Program entry point
 * @param argc The argument counter
 * @param argv The argument vector
 * @return EXIT_SUCCESS on success, EXIT_FAILURE on error.
 */
int main (int argc, char *argv[]) {
    
    if (argc < 3 || argc > 3) {
	(void) fprintf (stderr, "%s: not the correct amount of arguments!", argv[0]);
	return EXIT_FAILURE;
    }

    char *progname = argv[0];
    char *hostname = argv[1];
    char *port_str = argv[2];
    
    int sockfd = create_socket (progname, hostname, port_str);
    if (sockfd == EXIT_FAILURE)
	return EXIT_FAILURE;

    int ret = communicate (sockfd);
    
    (void) close (sockfd);
    
    return ret;
}
