/*! stegit - an application to obscure and retrieve messages 
 *
 * @author Bernhard Wick - 1525699
 * @brief A small application to obscure and retrieve messages
 *        stegit -f|-h [-o outputfile]
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <getopt.h>
#include <ctype.h>

// Letters and Words used for en- and decryption
char const *LETTERS = "abcdefghijklmnopqrstuvwxyz. ";
char const *WORDS[] = {
    "lorem", "ipsum", "dolor",
    "sit", "amet", "consectetur",
    "adipiscing", "elit", "phasellus",
    "molestie", "arcu", "a", "massa",
    "posuere", "tincidunt", "Praesent",
    "dignissim", "nisl", "id",
    "blandit", "maximus", "Nunc",
    "quis", "mauris", "eget",
    "felis", "bibendum", "egestas"
};

/**
 * @brief Function to transform message into "sentence" of predefined words.
 * @param msg The message to be encrypted.
 * @param filename The filename for the file to write it in.
 */
static
void encrypt (char *msg, char const *filename) {

    // change the message to complete lowercase
    // to match with *LETTERS
    for (int i = 0; msg[i]; i++)
	msg[i] = tolower (msg[i]);

    // file to write output in, should a filename be provided
    FILE *outf = stdout;
    if (filename != NULL) {
	outf = fopen (filename, "w");
	if (outf == NULL)
	    (void) fprintf (stderr, "Error with opening the file!");
    }	

    // loop to match letters in string to equivalent encoding word
    for (size_t i = 0, n = 0; i < strlen (msg); ++i, ++n) {
    
	char *tmp_char = strchr (LETTERS, msg[i]);
	int char_index = (int)(tmp_char - LETTERS);
    
	if (char_index < 0)
	    (void) printf ("Invalid character %c ... Will be removed from message.", msg[i]);

	else {
	    int r = rand() % 10;
	    char *filler;

	    // random chance to fill with a ".", if enough words written
	    if ((n < 15) && (n < 5 || r > 4))
		filler = " ";
	    else {
		filler = ". ";
		n = 0;
	    }

	    // print to file / output
	    fprintf (outf, "%s", WORDS[char_index]);
	    fprintf (outf, "%s", filler);
	}
    }

    if (outf != stdout)
	fclose (outf);	
}

/**
 * @brief Function to turn a hidden message (sentence of predefined words) to original message.
 * @param msg The message to be decrypted.
 * @param filename The filename for the file to write in.
 */
static
void decrypt (char *msg, char const *filename) {

    // file to write output in, if provided
    FILE *outf = stdout;
    if (filename != NULL) {
	outf = fopen (filename, "w");

	if (filename == NULL)
	    (void) fprintf (stderr, "Error with opening the file!");
    }

    char *token  = strtok (msg, " .");
    int tok_count = 0; // number of words to translate
  
    while (token != NULL) {
	for (int j = 0; j < 28; ++j) {
	    if (strcmp (WORDS[j], token) == 0) {
		(void) fprintf (outf, "%c", LETTERS[j]);
		break;
	    }
	}
	
	token = strtok (NULL, " .");
	tok_count++;
    }
    
    if (outf != stdout)
	(void) fclose (outf);
}

/**
 * @brief Program entry point
 * @param argc The argument counter
 * @param argv The argument vector
 * @return EXIT_SUCCESS on success, EXIT_FAILURE on failure
 */
int main (int argc, char **argv) {
  
    if (argc == 1) {
	(void) printf ("%s: Please provide an option. (-f or -h)\n", argv[0]);
	return EXIT_FAILURE;
    } else if (argc > 4) {
	(void) printf ("%s: Too many options!\n", argv[0]);
	return EXIT_FAILURE;
    } else if (argc > 1) {

	int find_mode = 0;
	int hide_mode = 0;
	char *output = NULL;

	int c;
	while ((c = getopt (argc, argv, "fho:")) != -1) {

	    // ./stegit -f|-h [-o filename]
	    switch (c) {
		case 'f': // find mode
		    find_mode = 1;
		    break;

		case 'h': // hide mode
		    hide_mode = 1;
		    break;

		case 'o': // file input / output
		    output = optarg;
		    break;

		case '?': // invalid argument
		    (void) fprintf (stderr, "%s: Invalid option! (Valid options: -f, -h, -o [arg])\n", argv[0]);
		    return EXIT_FAILURE;
	
		default: // not possible
		    (void) fprintf (stderr, "%s: getopt() unknown parameter failed.\n", argv[0]);
		    return EXIT_FAILURE;
	    }      
	}

	if (find_mode == hide_mode) {
	    (void) fprintf (stderr, "%s: Can't proceed! Choose one of either hide (-h) or find (-h) mode.\n", argv[0]);
	    return EXIT_FAILURE;
	} else {

	    char msg[1024];

	    if (find_mode)
		(void) printf ("Input message to decrypt:\n");
	    else
		(void) printf ("Input message to encrypt:\n");
	    
	    (void) fgets (msg, 1024, stdin);

	    // remove trailing new line
	    // and replace it with null term
	    int _i = strlen(msg)-1;
	    if (msg[_i] == '\n') 
		msg[_i] = '\0';

	    // decrypt or encrypt message
	    if (find_mode)
		(void) decrypt (msg, output);
	    
	    else if (hide_mode) {
		// rand init for encrypted message realism
		srand ((unsigned) time(NULL));
		(void) encrypt (msg, output);
	    }
	}
    }
  
    return EXIT_SUCCESS;
}
