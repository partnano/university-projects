/**
 * @file websh.c
 * @author Bernhard Wick - 1525699
 * @date 17 Nov 2016
 *
 * @brief main module
 *
 * Multi-process program to format commandline output to html.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <assert.h>
#include <string.h>
#include <sys/wait.h>

/* Global Variables (program arguments, will be filled in parse_arguments()) */
/**
 * Name of the program
 */
static char *progname;

/**
 * If this mode is set, the output will be
 * encapsuled in html headings (html, head, body)
 */
static int e_mode = -1;

/**
 * If this mode is set, the initial command
 * will be put before the respective outputs,
 * encapsuled in <h1>
 */
static int h_mode = -1;

/**
 * If this mode is set, the output will be
 * searched line by line for a keyword. If found
 * the line will be encapsuled with a given tag
 */
static int s_mode = -1;

/**
 * Keyword for -s mode
 */
static char *keyword = "";

/**
 * Tag for -s mode
 */
static char *tag = "";
/* End of global variables */

/**
 * Function to format output.
 * @brief This function takes in arrays of output and their commands.
 * Depending on set flags, html formatted output will be generated and
 * printed to stdout.
 * 
 * @param outs Array of command outputs to format
 * @param cmds Array of executed commands, in case of -h
 */
static
void format_output (char *outs[], char *cmds[]) {
    
    (void) printf ("\n===== FORMATTED OUTPUT =====\n\n");
    
    if (e_mode == 1)
	(void) printf ("<html><head></head><body>\n\n");

    for (int i = 0; outs[i] != NULL; ++i) {

	if (h_mode == 1) {
	    (void) printf ("<h1> %s </h1>\n", cmds[i]);
	}

	char *_outs;
	char *line = strtok_r (outs[i], "\n", &_outs);

	while (line != NULL) {
	    if (s_mode == 1 && strstr (line, keyword)) {
		(void) printf ("<%s> %s </%s><br />\n", tag, line, tag);
	    } else {
		(void) printf ("%s <br />\n", line);
	    }

	    line = strtok_r (NULL, "\n", &_outs);
	}

	(void) printf ("\n");
    }

    if (e_mode == 1)
    	(void) printf ("</body></html>\n");

    (void) printf ("\n===== FORMATTED OUTPUT =====\n\n");

    return;
}

/**
 * Helper function for synopsis print.
 * @brief This function prints the program synopsis.
 */
static
void usage_msg (void) {
    (void) printf ("SYNOPSIS: %s [-e] [-h] [-s WORD:TAG]\n", progname);
}

/**
 * Function to parse arguments
 * @brief This function parses arguments via getopt(3)
 * @param argc Argument counter
 * @param argv Arguments
 * @param optstring Allowed options, see getopt(3)
 * @return Returns either EXIT_SUCCESS on success or EXIT_FAILURE on error
 */
static
int parse_arguments (int argc, char *argv[], char *optstring) {

    if (argc > 5) {
	(void) fprintf (stderr, "%s, Too many arguments!\n", progname);
	return EXIT_FAILURE;
    }
    
    int c;
    while ((c = getopt (argc, argv, optstring)) != -1) {

	switch (c) {
	    case 'e':
		if (e_mode != -1) {
		    (void) fprintf (stderr, "%s: -e cannot be used multiple times.\n", progname);
		    (void) usage_msg ();
		    return EXIT_FAILURE;
		}

		e_mode = 1;
		break;

	    case 'h':
		if (h_mode != -1) {
		    (void) fprintf (stderr, "%s: -h cannot be used multiple times.\n", progname);
		    (void) usage_msg ();
		    return EXIT_FAILURE;
		}
		
		h_mode = 1;
		break;

	    case 's':
		if (s_mode != -1) {
		    (void) fprintf (stderr, "%s: -s cannot be used multiple times.\n", progname);
		    (void) usage_msg ();
		    return EXIT_FAILURE;
		}

		s_mode = 1;
		keyword = strtok (optarg, ":");
		tag = strtok (NULL, ":");
		
		break;

	    case '?':
		return EXIT_FAILURE;

	    default:
		// shouldn't be possible
		assert (0);
	}
    }

    return EXIT_SUCCESS;
}

/**
 * program entry point
 * @param argc Argument counter
 * @param argv Arguments
 * @return Returns EXIT_SUCCESS on success, EXIT_FAILURE on error
 */
int main (int argc, char *argv[]) {

    progname = argv[0];

    int ret = parse_arguments (argc, argv, "ehs:");
    if (ret == EXIT_FAILURE)
	return ret; // error output should already have happened.
    
    // read input
    char line[256];
    char input[4096];
    while (fgets (line, sizeof (input), stdin)) {
	strcat (input, line);
    }
    
    // start initial fork
    pid_t pid = fork();

    switch (pid) {
	case -1:
	    (void) fprintf (stderr, "%s: Cannot fork!\n", progname);
	    (void) exit (EXIT_FAILURE);

	case 0: ; 
	    char *line = strtok (input, "\n");
	    char *args[128];
	    int arg_count = 0;

	    char *outs[256];
	    char *cmds[256];
	    int cmd_cnt = 0;

	    while (line != NULL) {
		//inner pipe
		int inner_fd[2];
		pipe(inner_fd);

		pid_t ppid = fork();

		switch (ppid) {
		    case -1:
			(void) fprintf (stderr, "%s fork: Cannot fork!\n", progname);
			exit (EXIT_FAILURE);

		    case 0:
			close (inner_fd[0]); // close reading end

			int _d1 = dup2 (inner_fd[1], 1); // send stdout through pipe
			int _d2 = dup2 (inner_fd[1], 2); // send stderr through pipe

			if (_d1 == -1 || _d2 == -1) {
			    (void) fprintf (stderr, "%s: error on duplicating stdout / stderr to pipe (inner child)", progname);
			    (void) exit (EXIT_FAILURE);
			}
			
			args[arg_count] = strtok (line, " ");
			while (args[arg_count] != NULL) {
			    args[++arg_count] = strtok (NULL, " ");
			}
			
			(void) execvp (args[0], args);

			(void) close (inner_fd[1]);
			(void) exit (EXIT_SUCCESS);

		    default:
			close (inner_fd[1]); // close writing end
			char output[1024];
			
			while (read (inner_fd[0], output, sizeof (output)) != 0) {}

			outs[cmd_cnt] = malloc (sizeof (output));
			(void) strcpy (outs[cmd_cnt], output);
			(void) memset (output, 0, sizeof (output));

			(void) close (inner_fd[0]);

			int status;
			waitpid (ppid, &status, 0);

			if (status == -1)
			    exit (EXIT_FAILURE);
		}
		
		cmds[cmd_cnt] = line;

		cmd_cnt++;
		line = strtok (NULL, "\n");
	    }

	    // format & print html-formatted output 
	    (void) format_output (outs, cmds);

	    // free malloced pointers
	    for (int i = 0; outs[i] != NULL; ++i)
		(void) free (outs[i]);

	    (void) exit (EXIT_SUCCESS);

	default: ;
	    int status;
	    waitpid (pid, &status, 0);
	    exit (status);
    }
    
    return 0;
}
